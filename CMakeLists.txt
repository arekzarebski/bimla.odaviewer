#
#  ODA Viewer xcodeproj
#
# cmake -DRootPath=/Users/path to main/ -DODA_PLATFORM=iphone_12.0 -DCMAKE_TOOLCHAIN_FILE=./ios.cmake -      
# DIOS_PLATFORM=OS -H. -Bbuild.ios -GXcode
#
cmake_minimum_required (VERSION 3.7)

set (ODA_BIN_DIR ${RootPath}/bin/${ODA_PLATFORM})
link_directories(${ODA_BIN_DIR})
set (ODA_LIB_DIR  ${RootPath}/lib/${ODA_PLATFORM})
link_directories(${ODA_LIB_DIR})
	
message( ${ODA_PLATFORM} )
message( ${ODA_LIB_DIR} )

include (${RootPath}/build/oda.cmake)

set (TH_ROOT ${RootPath}/ThirdParty)
set (TKERNEL_ROOT ${RootPath}/Kernel)
set (TDRAWING_ROOT  ${RootPath}/Drawing)
set (VISUALIZE_ROOT  ${RootPath}/Visualize)
set (COMPONENTS_ROOT  ${RootPath}/Components)  
set (PRC_ROOT  ${RootPath}/Prc)  
set (DGN_ROOT  ${RootPath}/Dgn) 

set(TCOMPONENTS_BREPMODELER_TESTS 1)
set(TCOMPONENTS_BREPMODELER_BUILD 1)
set(VISUALIZE 1)
set(VISUALIZE_BUILD 1)
set(VISUALIZE_EXAMPLES 1)
set(VISUALIZE_EXTENSIONS 1)
set(VISUALIZE_IMPORTS 1)
set(VISUALIZE_BUILD 1)
set(TCOMPONENTS_IMPORTS 1)
set(TP_CORE 1)
set(TG_BUILD 1)

if(TP_CORE)
add_definitions(-DPRC_ENABLED)
endif(TP_CORE)

if(TG_BUILD)
add_definitions(-DDGN_ENABLED)
endif(TG_BUILD)

if(TCOMPONENTS_IMPORTS)
add_definitions(-DCOMPONENTS_IMPORT_ENABLED)
endif(TCOMPONENTS_IMPORTS)

include(${RootPath}/build/th.cmake)
include(${RootPath}/build/th.components)
include(${RootPath}/build/tkernel.cmake)
include(${RootPath}/build/tkernel.components)
include(${RootPath}/build/tdrawing.cmake)
include(${RootPath}/build/tdrawing.components)
include(${RootPath}/build/tcomponents.cmake)
include(${RootPath}/build/tcomponents.components)
include(${RootPath}/build/tvisualize.cmake)
include(${RootPath}/build/tvisualize.components)
include(${RootPath}/build/tp.cmake)
include(${RootPath}/build/tp.components)
include(${RootPath}/build/tg.cmake)
include(${RootPath}/build/tg.components)

set(DEVELOPMENT_PROJECT_NAME "ODAViewer")                     # e.g. project.xcodeproj
set(DEVELOPMENT_TEAM_ID "4SVB9F72E4")                       
set(APP_NAME "ODAViewer")                                     
set(APP_BUNDLE_IDENTIFIER "com.OpenDesignAlliance.ODAViewer")                

set(CODE_SIGN_IDENTITY "iPhone Developer")                 
                                                            # /usr/bin/env xcrun security find-identity -v -p codesigning
set(DEPLOYMENT_TARGET 12.0)                                  # <== Set your deployment target version of iOS
set(DEVICE_FAMILY "1,2")                                      # <== Set to "1" to target iPhone, set to "2" to target iPad, set to "1,2" to target both

project(${DEVELOPMENT_PROJECT_NAME})

set(PRODUCT_NAME ${APP_NAME})
set(EXECUTABLE_NAME ${APP_NAME})
set(MACOSX_BUNDLE_EXECUTABLE_NAME ${APP_NAME})
set(MACOSX_BUNDLE_INFO_STRING ${APP_BUNDLE_IDENTIFIER})
set(MACOSX_BUNDLE_GUI_IDENTIFIER ${APP_BUNDLE_IDENTIFIER})
set(MACOSX_BUNDLE_BUNDLE_NAME ${APP_BUNDLE_IDENTIFIER})
set(MACOSX_BUNDLE_ICON_FILE "")
set(MACOSX_BUNDLE_LONG_VERSION_STRING "1.0")
set(MACOSX_BUNDLE_SHORT_VERSION_STRING "1.0")
set(MACOSX_BUNDLE_BUNDLE_VERSION "1.0")
set(MACOSX_BUNDLE_COPYRIGHT "Copyright YOU")
set(MACOSX_DEPLOYMENT_TARGET ${DEPLOYMENT_TARGET})


set(APP_HEADER_FILES
	./AppDelegate.h
	./FilesViewController.h
    ./BaseViewController.h
	./RenderViewController.h
	./MetalViewController.h
	
	./AppCore/TviActivator.hpp
	./AppCore/TviAxisControl.hpp
	./AppCore/TviCore.hpp
	./AppCore/TviDatabaseInfo.h
	./AppCore/TviDraggers.hpp
	./AppCore/TviGlobalParameters.hpp
	./AppCore/TviImportParameters.hpp
	./AppCore/TviLimitManager.hpp
	./AppCore/TviMemoryStatus.h
	./AppCore/TviProgressMeter.hpp
	./AppCore/TviTools.h
	
	./AppUITools/Markups/MarkupsDialogsViewController.h
	./AppUITools/ParamsControllers/BaseImportParamsViewController.h
	./AppUITools/ParamsControllers/OptionsDlgViewController.h
	./AppUITools/BaseDialogViewController.h
	./AppUITools/SaveFileViewController.h
	./AppUITools/TviProgressControl.h
	./AppUITools/TviRectangle.h
)

set(APP_SOURCE_FILES
	./AppDelegate.mm
    ./main.m
	./FilesViewController.mm
    ./BaseViewController.mm
	./RenderViewController.mm
	./MetalViewController.mm
	
	./AppCore/TviActivator.cpp
	./AppCore/TviAxisControl.cpp
	./AppCore/TviCore.mm
	./AppCore/TviDraggers.mm
	./AppCore/TviGlobalParameters.mm
	./AppCore/TviImportParameters.mm
	./AppCore/TviLimitManager.cpp
	./AppCore/TviMemoryStatus.mm
	./AppCore/TviProgressMeter.mm
	./AppCore/TviTools.mm
	
	./AppUITools/Markups/MarkupsDialogsViewController.mm
	./AppUITools/ParamsControllers/BaseImportParamsViewController.mm
	./AppUITools/ParamsControllers/OptionsDlgViewController.mm
	./AppUITools/BaseDialogViewController.mm
	./AppUITools/SaveFileViewController.mm
	./AppUITools/TviProgressControl.m
	./AppUITools/TviRectangle.m
)

file(GLOB_RECURSE STORYB_FILES *.storyboard)
set_source_files_properties(${STORYB_FILES} PROPERTIES MACOSX_PACKAGE_LOCATION Resources)

file(GLOB_RECURSE XIB_FILES *.xib)
set_source_files_properties(${XIB_FILES} PROPERTIES MACOSX_PACKAGE_LOCATION Resources)

set(SCHEMA_FILES
    ${RootPath}/bin/${ODA_PLATFORM}/TD.tsbf
    ${RootPath}/bin/${ODA_PLATFORM}/TDV.tsbf
)

set( VISUALIZEVIEWER_IOS_RESOURCES 
    ./Assets.xcassets
	${XIB_FILES}
	${SCHEMA_FILES}
	#${TKERNEL_ROOT}/Source/Tr/render/metal/shaders.metal
 )

add_executable(
   	${APP_NAME}
   	MACOSX_BUNDLE
   	${APP_HEADER_FILES}
   	${APP_SOURCE_FILES}
   	${STORYB_FILES}
	${VISUALIZEVIEWER_IOS_RESOURCES}
)

target_include_directories( ${APP_NAME} PUBLIC ${TKERNEL_ROOT}/Include )
target_include_directories( ${APP_NAME} PUBLIC ${TDRAWING_ROOT}//Include )
target_include_directories( ${APP_NAME} PUBLIC ${TKERNEL_ROOT}/Extensions/ExServices )
target_include_directories( ${APP_NAME} PUBLIC ${TH_ROOT}/activation )
target_include_directories( ${APP_NAME} PUBLIC ${VISUALIZE_ROOT}/Include )
target_include_directories( ${APP_NAME} PUBLIC ${PRC_ROOT}/Include )
target_include_directories( ${APP_NAME} PUBLIC ${DGN_ROOT}/Include )
target_include_directories( ${APP_NAME}	PUBLIC ${TCOMPONENTS_ROOT}/BrepModeler/Include )
target_include_directories( ${APP_NAME}	PUBLIC ${RootPath}/Components/BrepModeler/Include )

find_library(GLKIT GLKit)
find_library(FOUNDATION Foundation)
find_library(CF QuartzCore)
find_library(OPENGLES OpenGLES)
find_library(UIKIT UIKit)
find_library(CG CoreGraphics)
find_library(MTL Metal)
find_library(MTLKIT MetalKit)

target_link_libraries(${APP_NAME} PUBLIC 
    ${TV_VISUALIZE_LIB}
    ${TV_VISUALIZETOOLS_LIB}
    ${TV_DWG2VISUALIZE_LIB}
    ${EX_VISUALIZEDEVICE_LIB}

    ${TV_PRC2VISUALIZE_LIB}
    ${TP_PRC_LIB}
    ${PRC_LIBS}

    ${TV_DGN2VISUALIZE_LIB}
    ${TG_DB_LIB} 
    ${TG_DGN7IO_LIB}
    ${TH_OLESS_LIB}

    ${TV_OBJ2VISUALIZE_LIB}
    ${OBJToolkit_LIB}
    ${TV_STL2VISUALIZE_LIB}
    ${TD_STL_IMPORT_LIB}

    ${TD_RDIMBLK_LIB}
    ${TV_RCS2VISUALIZE_LIB}

	${TD_EXLIB} 
	${TD_DR_EXLIB}
    ${TD_RASTER_LIB}
    ${TD_RASTERPROC_LIB}
    ${TD_DbCryptModule_LIB}
    ${TD_TXV_GLES2_LIBS}
    ${ODA_OPENGL_LIBS}
    ${TD_DB_LIB}
    ${TD_DBIO_LIB}
    ${TD_ISM_LIB}
    ${TD_SCENEOE_LIB} 
    ${TD_DBROOT_LIB} 
	${TD_DYNBLOCKS_LIB} 
	${TD_GS_LIB} 
	${TD_KEY_LIB}
    ${TD_SPATIALINDEX_LIB}
    ${TD_MODELER_LIB}
    ${TD_DBENT_LIB} 
	${TD_DBCORE_LIB} 
	${TD_BREPRENDERER_LIB} 
	${TD_BREPBUILDERFILLER_LIB} 
    ${TD_BREPBUILDER_LIB} 
    ${TD_ACISBLDR_LIB} 
    ${TCOMPONENTS_BREPMODELER_LIB} 
    ${TD_GI_LIB}
    ${TD_GE_LIB}
    ${TD_ROOT_LIB}
    ${TD_ALLOC_LIB}
    ${TH_FT_LIB}
    ${TH_THIRDPARTYRASTER_LIB}
    ${TH_ZLIB_LIB}
    ${TH_CONDITIONAL_LIBCRYPTO}
    ${TD_BR_LIB}
    ${TD_TF_LIB}
    ${TH_TINYXML_LIB}
	${TH_WCHAR_LIB}
	OdDbPartialViewing
    ${gles2_platform_libraries}
    ${u3d_libs}
	${TD_RX_CDA_LIB}
	${TD_THREADPOOL_LIB}
	${TD_WIPEOUT_LIB}
	${TD_ACCAMERA_LIB}
	${TD_ATEXT_LIB}
	${TD_RTEXT_LIB}
	${TD_MPOLYGON_LIB}
	${TD_DB_POINTCLOUDOBJ_LIB}
	${TD_EXPOINTCLOUDHOST_LIB}
	${TD_RCSFILESERVICES_LIB}
	${TD_FIELDEVAL_LIB}
	${TH_libXML}
	${TH_MINIZIP_MEM_LIB}
	${TH_UTF_LIB}
	${TR_TXR_METAL_LIB}
	${TD_TXV_METAL_LIB}
	${GLKIT} ${FOUNDATION} ${OPENGLES} ${UIKIT} ${CF} ${CG}
	${MTL} ${MTLKIT}
)

# Turn on ARC
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fobjc-arc")

# Create the app target
set_target_properties(${APP_NAME} PROPERTIES
		    XCODE_ATTRIBUTE_PRODUCT_BUNDLE_IDENTIFIER ${APP_BUNDLE_IDENTIFIER}                   
 		    XCODE_ATTRIBUTE_DEBUG_INFORMATION_FORMAT "dwarf-with-dsym"
            RESOURCE "${RESOURCES}"
            XCODE_ATTRIBUTE_GCC_PRECOMPILE_PREFIX_HEADER "YES"
            XCODE_ATTRIBUTE_IPHONEOS_DEPLOYMENT_TARGET ${DEPLOYMENT_TARGET}
            XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY ${CODE_SIGN_IDENTITY}
            XCODE_ATTRIBUTE_DEVELOPMENT_TEAM ${DEVELOPMENT_TEAM_ID}
            XCODE_ATTRIBUTE_TARGETED_DEVICE_FAMILY ${DEVICE_FAMILY}
            MACOSX_BUNDLE_INFO_PLIST ${CMAKE_CURRENT_SOURCE_DIR}/Info.plist
            XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_ARC YES
            XCODE_ATTRIBUTE_COMBINE_HIDPI_IMAGES NO
            XCODE_ATTRIBUTE_INSTALL_PATH "$(LOCAL_APPS_DIR)"
            XCODE_ATTRIBUTE_ENABLE_TESTABILITY YES
            XCODE_ATTRIBUTE_GCC_SYMBOLS_PRIVATE_EXTERN YES
            XCODE_ATTRIBUTE_ASSETCATALOG_COMPILER_APPICON_NAME AppIcon
		    MACOS_BUNDLE TRUE
            RESOURCE "${VISUALIZEVIEWER_IOS_RESOURCES}"
)

# Set the app's linker search path to the default location on iOS
set_target_properties(
   	${APP_NAME}
   	PROPERTIES
   	XCODE_ATTRIBUTE_LD_RUNPATH_SEARCH_PATHS
   	"@executable_path/Frameworks"
)

#file(COPY ./Assets.xcassets/ DESTINATION ./Assets.xcassets)
