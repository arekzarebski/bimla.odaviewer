/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __FMMDL_ITERATORS_H__
#define __FMMDL_ITERATORS_H__

#include "Modeler/FMMdlBody.h"
#include "Modeler/FMMdlVertex.h"
#include "Modeler/FMMdlEdge.h"
#include "Modeler/FMMdlFace.h"

namespace FacetModeler
{

  /** \details
     Body vertex iterator.
  */
  class FMGEOMETRY_API VertexIterator
  {
  public:
    /** \details
       Constructor. Creates a vertex iterator for the given body.
      
       \param pBody [in] Pointer to the body to be iterated.
    */
    VertexIterator(const Body* pBody);

    /** \details
       Checks whether the iteration is finished.

       \returns true if the iteration is finished, false otherwise.
    */
    bool done();

    /** \details
       Sets the iterator to the next item.
    */
    void next();

    /** \details
       Gets the current item.

       \returns Pointer to the current item.
    */
    Vertex* get();

  private:
    Body* m_pOwner;
    Vertex* m_pCurrent;
    Vertex* m_pNext;
    Vertex* m_pFirst;
  };

  /** \details
     Body edge iterator.
  */
  class FMGEOMETRY_API EdgeBodyIterator
  {
  public:
    /** \details
       Constructor. Creates an edge iterator for the given body.

       \param pBody [in] Pointer to the body to be iterated.
    */
    explicit EdgeBodyIterator(const Body* pBody);

    /** \details
       Checks whether the iteration is finished.

       \returns true if the iteration is finished, false otherwise.
    */
    bool done();

    /** \details
       Sets the iterator to the next item.

       \param bNextLoop [out] If this parameter is passed, it's set to true if the pointer to the current loop has been changed to the next one; false otherwise.
    */
    void next(bool* bNextLoop = NULL);

    /** \details
       Gets the current item.

       \returns Pointer to the current item.
    */
    Edge* get();

  private:
    Body* m_pOwner;
    Face* m_pCurrentFace;
    Face* m_pFirstFace;
    OdUInt32 m_iCurrentLoop;
    Edge* m_pFirst;
    Edge* m_pCurrent;
    Edge* m_pNext;
  };

  /** \details
     Face edge iterator.
  */
  class FMGEOMETRY_API EdgeFaceIterator
  {
  public:
    /** \details
       Constructor. Creates an edge iterator for the given face.

       \param pFace [in] Pointer to the face to be iterated.
    */
    explicit EdgeFaceIterator(const Face* pFace);

    /** \details
       Checks whether the iteration is finished.

       \returns true if the iteration is finished, false otherwise.
    */
    bool done();

    /** \details
       Sets the iterator to the next item.

       \param bNextLoop [out] If this parameter is passed, it's set to true if the pointer to the current loop has been changed to the next one; false otherwise.
    */
    void next(bool* bNextLoop = NULL);

    /** \details
       Gets the current item.

       \returns Pointer to the current item.
    */
    Edge* get();

  private:
    Face* m_pFace;
    OdUInt32 m_iCurrentLoop;
    Edge* m_pFirst;
    Edge* m_pCurrent;
    Edge* m_pNext;
  };

  /** \details
     Body face iterator.
  */
  class FMGEOMETRY_API FaceIterator
  {
  public:
    /** \details
       Constructor. Creates a face iterator for the given body.

       \param pBody [in] Pointer to the body to be iterated.
    */
    explicit FaceIterator(const Body* pBody);

    /** \details
       Checks whether the iteration is finished.

       \returns true if the iteration is finished, false otherwise.
    */
    bool done();

    /** \details
       Sets the iterator to the next item.
    */
    void next();

    /** \details
       Gets the current item.

       \returns Pointer to the current item.
    */
    Face* get();

  private:
    Body* m_pOwner;
    Face* m_pFirst;
    Face* m_pCurrent;
    Face* m_pNext;
  };

}

#endif //__FMMDL_ITERATORS_H__
