tf_sources(FMCreate
    BodyExample.h
    BodyExample.cpp
    ContourExample.h
    ContourExample.cpp
    FMCreateStart.h
    FMCreateStart.cpp
    FMCreate.cpp
    ExampleCases.h
  )

if(MSVC)
tf_sources(FMCreate
    FMCreate.rc
  )
endif(MSVC)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}
    ${TKERNEL_ROOT}/Extensions/ExServices
    ../../include
    #../../examples/FMCreate
    )

if(ODA_SHARED)
set ( FMCreate_libs 
     ${TD_EXLIB} ${TD_GE_LIB} ${TD_ROOT_LIB}
)
else(ODA_SHARED)
set ( FMCreate_libs
    ${TD_EXLIB}
    ${TD_GS_LIB} ${TD_GI_LIB} ${TD_SPATIALINDEX_LIB} ${TD_GE_LIB}
    ${TD_ROOT_LIB} ${TH_CONDITIONAL_LIBCRYPTO}
)
endif(ODA_SHARED)

tf_executable(FMCreate ${FACET_MODELER_LIB} ${FMCreate_libs} ${TD_ALLOC_LIB} )

tf_project_group(FMCreate "Examples")
