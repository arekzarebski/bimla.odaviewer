/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_FRAGMENT_H__
#define __TNW_FRAGMENT_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "Ge/GeMatrix3d.h"

class OdNwGeometryEllipticalShape;
class OdNwGeometryLineSet;
class OdNwGeometryMesh;
class OdNwGeometryPointSet;
class OdNwGeometryText;
class OdNwGeometryTube;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryEllipticalShape object pointers.
*/
typedef OdSmartPtr<OdNwGeometryEllipticalShape> OdNwGeometryEllipticalShapePtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryLineSet object pointers.
*/
typedef OdSmartPtr<OdNwGeometryLineSet> OdNwGeometryLineSetPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryMesh object pointers.
*/
typedef OdSmartPtr<OdNwGeometryMesh> OdNwGeometryMeshPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryPointSet object pointers.
*/
typedef OdSmartPtr<OdNwGeometryPointSet> OdNwGeometryPointSetPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryText object pointers.
*/
typedef OdSmartPtr<OdNwGeometryText> OdNwGeometryTextPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryTube object pointers.
*/
typedef OdSmartPtr<OdNwGeometryTube> OdNwGeometryTubePtr;

/** \details
  This class represents fragment geometry of geometry component.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwFragment : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwFragment);

  OdNwFragment();

public:

  virtual ~OdNwFragment();

public:

  /** \details
    Returns low bound of geometry data

    \returns Returns Object of OdGePoint3d with low bound of geometry data.
  */
  OdGePoint3d getLowBound() const;

  /** \details
    Returns high bound of geometry data

    \returns Returns Object of OdGePoint3d with high bound of geometry data.
  */
  OdGePoint3d getHighBound() const;

  /** \details
    Returns transform matrix of geometry data

    \returns Returns Object of OdGeMatrix3d with transform matrix of geometry data.
  */
  OdGeMatrix3d getTransformation() const;

  /** \details
    Returns flag of ellipse type of geometry data

    \returns Returns true if geometry data's type is ellipse, in other false.
  */
  bool isEllipse() const;

  /** \details
    Returns flag of line set type of geometry data

    \returns Returns true if geometry data's type is line set, in other false.
  */
  bool isLineSet() const;

  /** \details
    Returns flag of mesh type of geometry data

    \returns Returns true if geometry data's type is mesh, in other false.
  */
  bool isMesh() const;

  /** \details
    Returns flag of point set type of geometry data

    \returns Returns true if geometry data's type is point set, in other false.
  */
  bool isPointSet() const;

  /** \details
    Returns flag of text type of geometry data

    \returns Returns true if geometry data's type is text, in other false.
  */
  bool isText() const;

  /** \details
    Returns flag of tube type of geometry data

    \returns Returns true if geometry data's type is tube, in other false.
  */
  bool isTube() const;

  /** \details
    Returns a smart pointer to ellipse geometry

    \returns Returns a smart pointer to OdNwGeometryEllipticalShape object with geometry data. If geometry type is not ellipse, the returned smart pointer is NULL.
  */
  OdNwGeometryEllipticalShapePtr getEllipse() const;

  /** \details
    Returns a smart pointer to line set geometry

    \returns Returns a smart pointer to OdNwGeometryLineSet object with geometry data. If geometry type is not line set, the returned smart pointer is NULL.
  */
  OdNwGeometryLineSetPtr getLineSet() const;

  /** \details
    Returns a smart pointer to mesh geometry

    \returns Returns a smart pointer to OdNwGeometryMesh object with geometry data. If geometry type is not mesh, the returned smart pointer is NULL.
  */
  OdNwGeometryMeshPtr getMesh() const;

  /** \details
    Returns a smart pointer to point set geometry

    \returns Returns a smart pointer to OdNwGeometryPointSet object with geometry data. If geometry type is not point set, the returned smart pointer is NULL.
  */
  OdNwGeometryPointSetPtr getPointSet() const;

  /** \details
    Returns a smart pointer to text geometry

    \returns Returns a smart pointer to OdNwGeometryText object with geometry data. If geometry type is not text, the returned smart pointer is NULL.
  */
  OdNwGeometryTextPtr getText() const;

  /** \details
    Returns a smart pointer to tube geometry

    \returns Returns a smart pointer to OdNwGeometryTube object with geometry data. If geometry type is not tube, the returned smart pointer is NULL.
  */
  OdNwGeometryTubePtr getTube() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwFragment object pointers.
*/
typedef OdSmartPtr<OdNwFragment> OdNwFragmentPtr;

#endif //__TNW_FRAGMENT_H__
