/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_GEOMETRYMESH_H__
#define __TNW_GEOMETRYMESH_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "Ge/GeVector3d.h"

/** \details
  This class represents geometry with mesh type.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwGeometryMesh : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwGeometryMesh);

  OdNwGeometryMesh();

public:

  virtual ~OdNwGeometryMesh();

public:

  /** \details
    Returns array with vertexes count per face.

    \returns Returns OdArray objects with vertexes count per line.
  */
  OdArray<OdUInt16> getVertexCountPerFace() const;

  /** \details
    Returns array with indexes.

    \returns Returns OdArray objects with indexes.
  */
  OdArray<OdUInt16> getIndexes() const;

  /** \details
    Returns array with normals.

    \returns Returns OdArray objects with normales vectors.
  */
  OdArray<OdGeVector3d> getNormales() const;

  /** \details
    Returns array with uv parameters.

    \returns Returns OdArray objects with points of uv parameters.
  */
  OdArray<OdGePoint2d> getUVParameters() const;

  /** \details
    Returns array with faces.

    \returns Returns OdArray objects with faces.
  */
  OdArray<OdArray<OdInt32> >& getFaces();

  /** \details
    Returns array with vertexes.

    \returns Returns OdArray objects with vertexes.
  */
  OdGePoint3dArray getVertexes() const;

  /** \details
    Returns array with colors.

    \returns Returns OdArray objects with colors.
  */
  OdArray<OdUInt32> getColors() const;

  /** \details
    Returns flag of normales skipping

    \returns Returns true if normales are skipped, in other false.
  */
  bool getNormalesAreSkipped() const;

  /** \details
    Processes of faces before using and return array of faces

    \param faces [out] Object of OdArray with faces.

    \sa
      deprecated
  */
  void prepareFaces(OdArray<OdArray<OdInt32> >& faces);
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryMesh object pointers.
*/
typedef OdSmartPtr<OdNwGeometryMesh> OdNwGeometryMeshPtr;

#endif //__TNW_GEOMETRYMESH_H__
