/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_GEOMETRYPOINTSET_H__
#define __TNW_GEOMETRYPOINTSET_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "Ge/GePoint3dArray.h"

/** \details
  This class represents geometry with point set type.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwGeometryPointSet : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwGeometryPointSet);
  OdNwGeometryPointSet();

public:

  virtual ~OdNwGeometryPointSet();

public:

  /** \details
    Returns array with vertexes.

    \returns Returns OdArray objects with vertexes.
  */
  OdGePoint3dArray getVertexes() const;

  /** \details
    Returns array with colors.

    \returns Returns OdArray objects with colors.
  */
  OdArray<OdUInt32> getColors() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryPointSet object pointers.
*/
typedef OdSmartPtr<OdNwGeometryPointSet> OdNwGeometryPointSetPtr;

#endif //__TNW_GEOMETRYPOINTSET_H__
