/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2018, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2018 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef _NWOBJECT_H_
#define _NWOBJECT_H_
#include "DbHandle.h"
#include "Gi/GiDrawable.h"
#include "NwExport.h"

class OdNwObject;
class OdNwDatabase;
class OdNwObjectReactor;
class OdNwObjectImpl;

/** \details
  This class represents wrapper around the OdDbStub-type database's id of object.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwObjectId
{
public: 
  ODRX_HEAP_OPERATORS();
  OdNwObjectId() : m_Id(NULL) {}
  OdNwObjectId(OdDbStub* id) : m_Id(id) {}
  ~OdNwObjectId();

  NWDBEXPORT_STATIC static const OdNwObjectId kNull;
  OdNwObjectId& operator=(OdDbStub* id) { m_Id = id; return *this; }
  bool operator!() const { return isNull(); }
  operator OdDbStub*() const { return m_Id; }
  OdDbStub* operator->() const { return reinterpret_cast<OdDbStub*>(m_Id); }

  /** \details
     Returns true if and only if id is empty.

    \return boolean value of id's emptiness
  */
  bool isNull() const { return m_Id == NULL; }

  /** \details
     Resets id value.
  */
  void setNull() { m_Id = 0; }

  /** \details
     Returns true if and only if id is not empty.

    \return boolean value of id's not emptiness
  */
  bool isValid() const { return m_Id != NULL; }

  /** \details
    Returns the database with which this NwObjectId object is associated..

    \return pointer to OdNwDatabase object
  */
  OdNwDatabase* database() const;

  /** \details
     Returns the database handle of the object referenced by this ObjectId object

    \return OdDbHandle object with handle of database's id
  */
  OdDbHandle getHandle() const;

  /** \details
     Returns pointer to object of database's id.

    \return smart pointer to OdRxObject object
  */
  OdRxObjectPtr getObject();

  /** \details
     Sets object of database's id.

    \param pObj [in]  Pointer to object.
  */
  void setObject(OdRxObject* pObj);

  /** \details
     Returns pointer to object's owner database's id.

    \return pointer to OdDbStub object
  */
  OdDbStub* owner();

  /** \details
     Returns pointer to object's owner database's id.

    \return const pointer to OdDbStub object
  */
  const OdDbStub* owner() const;

  /** \details
     Sets object's owner by database's id.

    \param pOwner [in]  Pointer to object's owner.
  */
  void setOwner(OdDbStub* pOwner);

  /** \details
    Opens the database object associated with this NwObjectId object.

    \returns Returns a SmartPointer to the opened object if successful, otherwise a null SmartPointer.
  */
  OdSmartPtr<OdNwObject> openObject() const;

  /** \details
    Opens the database object associated with this NwObjectId object, or throws and exception if unsucessful.

    \returns Returns a SmartPointer to the opened object.
    \remarks

    Throws:
    
    <table>
    Exception           Cause
    eInvalidOpenState   Invalid open state
    </table>
  */
  OdSmartPtr<OdNwObject> safeOpenObject() const;

  /** \details
    Returns true if and only if the object associated with this NwObjectId object is erased.
  */
  bool isErased() const;

protected:
  OdDbStub* id() const { return m_Id; }
  class OdDbStub* m_Id;
};

/** \details
  This template class is a specialization of the OdArray class for OdNwObjectId's collection.
*/
typedef OdArray<OdNwObjectId, OdMemoryAllocator<OdNwObjectId> > OdNwObjectIdArray;

/** \details
  This class represents GI interface for database's object.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwObject : public OdGiDrawable
{
public:
  ODRX_DECLARE_MEMBERS(OdNwObject);
  OdNwObject();
  virtual ~OdNwObject();

  /** \details
     Returns object's id from database

    \return OdNwObjectId object with object's id
  */
  virtual OdNwObjectId objectId() const;

  /** \details
     Returns pointer to object's owner database's id.

    \return pointer to OdDbStub object
  */
  virtual OdDbStub* ownerId() const;

   /** \details
    Adds the specified reactor to this object's reactor list.

    \param pReactor [in]  Pointer to the reactor object.
   */
  virtual void addReactor(OdNwObjectReactor* pReactor);

  /** \details
    Removes the specified reactor from this object's reactor list.

    \param pReactor [in]  Pointer to the reactor object.
  */
  virtual void removeReactor(OdNwObjectReactor* pReactor);

   /** \details
    Sets the vectorization attributes of object, and returns its attribute flags.

    \param pTraits [in]  Pointer to OdGiDrawableTraits object from and to which the attributes are to be set.

    \remarks overridden method from class OdGiDrawable

    \sa
    SetAttributesFlags 
  */
  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const;

  /** \details
    Creates a viewport-independent geometric representation of this object.

    \param pWd [in]  Pointer to the OdGiWorldDraw interface.

    \remarks overridden method from class OdGiDrawable
  */
  virtual bool subWorldDraw(OdGiWorldDraw* pWd) const;

  /** \details
    Creates a viewport-dependent geometric representation of object.

    \param pVd [in]  Pointer to the OdGiViewportDraw interface.

    \remarks overridden method from class OdGiDrawable
  */
  virtual void subViewportDraw(OdGiViewportDraw* pVd) const;

  /** \details
    \returns Returns true if and only if object is persistent (stored in a database).

    \remarks overridden method from class OdGiDrawable
  */
  virtual bool isPersistent() const;

  /** \details
    Returns the database ID of object.

    \return pointer to OdDbStub with database ID of object

    \remarks Returns a null pointer if object is not persistent.

    overridden method from class OdGiDrawable
  */
  virtual OdDbStub* id() const;

  /** \details
    Assigns the specified OdGsCache to object.
    
    \param pGsNode [in]  Pointer to the OdGsCache to be assigned.

    \remarks overridden method from class OdGiDrawable
  */
  virtual void setGsNode(OdGsCache* pGsNode);

  /** \details
    Returns the OdGsCache associated with object.

    \return pointer to OdGsCache associated with object

    \remarks overridden method from class OdGiDrawable
  */
  virtual OdGsCache* gsNode() const;

  /** \details
    Returns drawble type that should be used in rendering with GI

    \returns
      Return DrawableType value with drawble type of object.

    \remarks
      type must be one of the following:

    <table>
    Name                                        Value     Description 
    DrawableType::kGeometry                     0
    DrawableType::kDistantLight                 1
    DrawableType::kPointLight                   2
    DrawableType::kSpotLight                    3
    DrawableType::kAmbientLight                 4
    DrawableType::kSolidBackground              5
    DrawableType::kGradientBackground           6
    DrawableType::kImageBackground              7
    DrawableType::kGroundPlaneBackground        8
    DrawableType::kViewport                     9
    DrawableType::kWebLight                     10
    DrawableType::kSkyBackground                11
    DrawableType::kImageBasedLightingBackground 12
    </table>
  */
  virtual DrawableType drawableType() const;

protected:
  friend class OdNwObjectImpl;
  OdSmartPtr<OdNwObjectImpl> m_pImpl;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwObject object pointers.
*/
typedef OdSmartPtr<OdNwObject> OdNwObjectPtr;

#endif // _NWOBJECT_H_
