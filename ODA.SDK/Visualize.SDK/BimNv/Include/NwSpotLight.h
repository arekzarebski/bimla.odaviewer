/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_SPOTLIGHT_H__
#define __TNW_SPOTLIGHT_H__

#include "NwPositionLight.h"

/** \details
  This class represents spot light of model.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwSpotLight : public OdNwPositionLight
{
  ODRX_DECLARE_MEMBERS(OdNwSpotLight);

  OdNwSpotLight();

public:

  virtual ~OdNwSpotLight();

public:

  /** \details
    Returns direction vector of light

    \returns Return OdGeVector3d value with direction vector of light.
  */
  OdGeVector3d getDirection() const;

  /** \details
    Returns dropoff rate of light

    \returns Return double value with dropoff rate of light.
  */
  double getDropoffRate() const;

  /** \details
    Returns cutoff angle of light

    \returns Return double value with cutoff angle of light.
  */
  double getCutoffAngle() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwSpotLight object pointers.
*/
typedef OdSmartPtr<OdNwSpotLight> OdNwSpotLightPtr;

#endif //__TNW_SPOTLIGHT_H__
