/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef TNW_LIGHT_VALUE_TYPE_H_
#define TNW_LIGHT_VALUE_TYPE_H_

/** \details
  bit masks for type of getting value from texture

  <group TNW_Namespaces>
*/
namespace NwLightValueType
{
  enum Enum
  {
    /**OdNwColor value with filter color of common light*/
    light_filter_color           = 0,
    /**OdNwColor value with tint color of common light*/
    light_tint_color             = 1,
    /**OdNwColor value with rgb light color of common light*/
    light_rgb_color              = 2,
    /**OdNwColor value with shadow color of common light*/
    light_shadow_color           = 3,
    /**double value with intensity of common light*/
    light_intensity_value        = 4,
    /**bool value with status of targeted of position light*/
    light_position_is_targeted   = 5,
    /**matrix value with web rotation, scale and translate of spot light*/
    light_spot_web_matrix        = 6,
    /**bool value with status of ambient only of common light*/
    light_ambient_only_flag      = 7,
    /**bool value with status of tint's using of common light*/
    light_use_tint_flag          = 8,
    /**bool value with status of shadow's enabling of common light*/
    light_shadow_on_flag         = 9
  };
}
#endif  // TNW_LIGHT_VALUE_TYPE_H_

