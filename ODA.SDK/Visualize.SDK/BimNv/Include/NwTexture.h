/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_TEXTURE_H__
#define __TNW_TEXTURE_H__

#include "NwMaterial.h"
#include "NwTextureValueType.h"
#include "NwModelUnits.h"

class OdGiRasterImage;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdGiRasterImage object pointers.
*/
typedef OdSmartPtr<OdGiRasterImage> OdGiRasterImagePtr;

/** \details
  This class represents geometry texture within the model hierarchy.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwTexture : public OdNwMaterial
{
  ODRX_DECLARE_MEMBERS(OdNwTexture);

  OdNwTexture();

public:

  virtual ~OdNwTexture();

public:

  /** \details
    Returns string param value depending from value of mask

    \param
      type [in] param with type NwTextureValueType::Enum.

    \returns
      Return OdString value depending from value of type.

    \remarks
      type must be one of the following:

    <table>
    Name                                       Value     Description 
    NwTextureValueType::pattern_path           0         OdString value with path to pattern texture
    NwTextureValueType::pattern_scale_x_value  1         double value with real world scale x of pattern texture
    NwTextureValueType::pattern_scale_x_unit   2         unit value for real world scale x of pattern texture
    NwTextureValueType::pattern_scale_y_value  3         double value with real world scale y of pattern texture
    NwTextureValueType::pattern_scale_y_unit   4         unit value for real world scale y of pattern texture
    NwTextureValueType::pattern_offset_x_value 5         double value with real world offset x of pattern texture
    NwTextureValueType::pattern_offset_x_unit  6         unit value for real world offset x of pattern texture
    NwTextureValueType::pattern_offset_y_value 7         double value with real world offset y of pattern texture
    NwTextureValueType::pattern_offset_y_unit  8         unit value for real world offset y of pattern texture
    NwTextureValueType::pattern_raster_image   9         raster image of pattern texture
    NwTextureValueType::diffuse_path           10        OdString value with path to diffuse texture
    NwTextureValueType::diffuse_scale_x_value  11        double value with real world scale x of diffuse texture
    NwTextureValueType::diffuse_scale_x_unit   12        unit value for real world scale x of diffuse texture
    NwTextureValueType::diffuse_scale_y_value  13        double value with real world scale y of diffuse texture
    NwTextureValueType::diffuse_scale_y_unit   14        unit value for real world scale  of diffuse texture
    NwTextureValueType::diffuse_offset_x_value 15        double value with real world offset x of diffuse texture
    NwTextureValueType::diffuse_offset_x_unit  16        unit value for real world offset x of diffuse texture
    NwTextureValueType::diffuse_offset_y_value 17        double value with real world offset y of diffuse texture
    NwTextureValueType::diffuse_offset_y_unit  18        unit value for real world offset y of diffuse texture
    NwTextureValueType::diffuse_raster_image   19        raster image of diffuse texture
    NwTextureValueType::opacity_path           20        OdString value with path to opacity texture
    NwTextureValueType::opacity_scale_x_value  21        double value with real world scale x of opacity texture
    NwTextureValueType::opacity_scale_x_unit   22        unit value for real world scale x of opacity texture
    NwTextureValueType::opacity_scale_y_value  23        double value with real world scale y of opacity texture
    NwTextureValueType::opacity_scale_y_unit   24        unit value for real world scale y of opacity texture
    NwTextureValueType::opacity_offset_x_value 25        double value with real world offset x of opacity texture
    NwTextureValueType::opacity_offset_x_unit  26        unit value for real world offset x of opacity texture
    NwTextureValueType::opacity_offset_y_value 27        double value with real world offset y of opacity texture
    NwTextureValueType::opacity_offset_y_unit  28        unit value for real world offset y of opacity texture
    NwTextureValueType::opacity_raster_image   29        raster image of opacity texture
    NwTextureValueType::bump_path              30        OdString value with path to bump texture
    NwTextureValueType::bump_scale_x_value     31        double value with real world scale x of bump texture
    NwTextureValueType::bump_scale_x_unit      32        unit value for real world scale x of bump texture
    NwTextureValueType::bump_scale_y_value     33        double value with real world scale y of bump texture
    NwTextureValueType::bump_scale_y_unit      34        unit value for real world scale y of bump texture
    NwTextureValueType::bump_offset_x_value    35        double value with real world offset x of bump texture
    NwTextureValueType::bump_offset_x_unit     36        unit value for real world offset x of bump texture
    NwTextureValueType::bump_offset_y_value    37        double value with real world offset y of bump texture
    NwTextureValueType::bump_offset_y_unit     38        unit value for real world offset y of bump texture
    NwTextureValueType::bump_raster_image      39        raster image of bump texture
    </table>
  */
  OdString getStringValue(NwTextureValueType::Enum type) const;


  /** \details
    Returns double param value depending from value of mask

    \param
      type [in] param with type NwTextureValueType::Enum.

    \returns
      Return double value depending from value of type.

    \remarks
      type must be one of the following:

    <table>
    Name                                       Value     Description 
    NwTextureValueType::pattern_path           0         OdString value with path to pattern texture
    NwTextureValueType::pattern_scale_x_value  1         double value with real world scale x of pattern texture
    NwTextureValueType::pattern_scale_x_unit   2         unit value for real world scale x of pattern texture
    NwTextureValueType::pattern_scale_y_value  3         double value with real world scale y of pattern texture
    NwTextureValueType::pattern_scale_y_unit   4         unit value for real world scale y of pattern texture
    NwTextureValueType::pattern_offset_x_value 5         double value with real world offset x of pattern texture
    NwTextureValueType::pattern_offset_x_unit  6         unit value for real world offset x of pattern texture
    NwTextureValueType::pattern_offset_y_value 7         double value with real world offset y of pattern texture
    NwTextureValueType::pattern_offset_y_unit  8         unit value for real world offset y of pattern texture
    NwTextureValueType::pattern_raster_image   9         raster image of pattern texture
    NwTextureValueType::diffuse_path           10        OdString value with path to diffuse texture
    NwTextureValueType::diffuse_scale_x_value  11        double value with real world scale x of diffuse texture
    NwTextureValueType::diffuse_scale_x_unit   12        unit value for real world scale x of diffuse texture
    NwTextureValueType::diffuse_scale_y_value  13        double value with real world scale y of diffuse texture
    NwTextureValueType::diffuse_scale_y_unit   14        unit value for real world scale  of diffuse texture
    NwTextureValueType::diffuse_offset_x_value 15        double value with real world offset x of diffuse texture
    NwTextureValueType::diffuse_offset_x_unit  16        unit value for real world offset x of diffuse texture
    NwTextureValueType::diffuse_offset_y_value 17        double value with real world offset y of diffuse texture
    NwTextureValueType::diffuse_offset_y_unit  18        unit value for real world offset y of diffuse texture
    NwTextureValueType::diffuse_raster_image   19        raster image of diffuse texture
    NwTextureValueType::opacity_path           20        OdString value with path to opacity texture
    NwTextureValueType::opacity_scale_x_value  21        double value with real world scale x of opacity texture
    NwTextureValueType::opacity_scale_x_unit   22        unit value for real world scale x of opacity texture
    NwTextureValueType::opacity_scale_y_value  23        double value with real world scale y of opacity texture
    NwTextureValueType::opacity_scale_y_unit   24        unit value for real world scale y of opacity texture
    NwTextureValueType::opacity_offset_x_value 25        double value with real world offset x of opacity texture
    NwTextureValueType::opacity_offset_x_unit  26        unit value for real world offset x of opacity texture
    NwTextureValueType::opacity_offset_y_value 27        double value with real world offset y of opacity texture
    NwTextureValueType::opacity_offset_y_unit  28        unit value for real world offset y of opacity texture
    NwTextureValueType::opacity_raster_image   29        raster image of opacity texture
    NwTextureValueType::bump_path              30        OdString value with path to bump texture
    NwTextureValueType::bump_scale_x_value     31        double value with real world scale x of bump texture
    NwTextureValueType::bump_scale_x_unit      32        unit value for real world scale x of bump texture
    NwTextureValueType::bump_scale_y_value     33        double value with real world scale y of bump texture
    NwTextureValueType::bump_scale_y_unit      34        unit value for real world scale y of bump texture
    NwTextureValueType::bump_offset_x_value    35        double value with real world offset x of bump texture
    NwTextureValueType::bump_offset_x_unit     36        unit value for real world offset x of bump texture
    NwTextureValueType::bump_offset_y_value    37        double value with real world offset y of bump texture
    NwTextureValueType::bump_offset_y_unit     38        unit value for real world offset y of bump texture
    NwTextureValueType::bump_raster_image      39        raster image of bump texture
    </table>
  */
  double getDoubleValue(NwTextureValueType::Enum type) const;

  /** \details
    Returns units param value depending from value of mask

    \param
      type [in] param with type NwTextureValueType::Enum.

    \returns
      Return NwModelUnits::Enum value depending from type.

    \remarks
      getUnitValue() returns one of the following:

    <table>
    Name                                 Value   Description
    NwModelUnits::UNITS_METERS           0x00    SI meter. Length of the path travelled by light in vacuum in 1 299,792,458th of second.
    NwModelUnits::UNITS_CENTIMETERS      0x01    1 100th of SI meter.
    NwModelUnits::UNITS_MILLIMETERS      0x02    1 1000th of an SI meter.
    NwModelUnits::UNITS_FEET             0x03    1 3rd of a yard.
    NwModelUnits::UNITS_INCHES           0x04    1 12th of a foot, 2.54 SI centimeters.
    NwModelUnits::UNITS_YARDS            0x05    Imperial yard, 0.9144 SI meters.
    NwModelUnits::UNITS_KILOMETERS       0x06    1000 SI meters
    NwModelUnits::UNITS_MILES            0x07    1760 yards
    NwModelUnits::UNITS_MICROMETERS      0x08    1 1,000,000th of an SI meter, micron
    NwModelUnits::UNITS_MILS             0x09    1 1,000th of inch
    NwModelUnits::UNITS_MICROINCHES      0x0A    1 1,000,000th of inch
    </table>

    type must be one of the following:

    <table>
    Name                                       Value     Description 
    NwTextureValueType::pattern_path           0         OdString value with path to pattern texture
    NwTextureValueType::pattern_scale_x_value  1         double value with real world scale x of pattern texture
    NwTextureValueType::pattern_scale_x_unit   2         unit value for real world scale x of pattern texture
    NwTextureValueType::pattern_scale_y_value  3         double value with real world scale y of pattern texture
    NwTextureValueType::pattern_scale_y_unit   4         unit value for real world scale y of pattern texture
    NwTextureValueType::pattern_offset_x_value 5         double value with real world offset x of pattern texture
    NwTextureValueType::pattern_offset_x_unit  6         unit value for real world offset x of pattern texture
    NwTextureValueType::pattern_offset_y_value 7         double value with real world offset y of pattern texture
    NwTextureValueType::pattern_offset_y_unit  8         unit value for real world offset y of pattern texture
    NwTextureValueType::pattern_raster_image   9         raster image of pattern texture
    NwTextureValueType::diffuse_path           10        OdString value with path to diffuse texture
    NwTextureValueType::diffuse_scale_x_value  11        double value with real world scale x of diffuse texture
    NwTextureValueType::diffuse_scale_x_unit   12        unit value for real world scale x of diffuse texture
    NwTextureValueType::diffuse_scale_y_value  13        double value with real world scale y of diffuse texture
    NwTextureValueType::diffuse_scale_y_unit   14        unit value for real world scale  of diffuse texture
    NwTextureValueType::diffuse_offset_x_value 15        double value with real world offset x of diffuse texture
    NwTextureValueType::diffuse_offset_x_unit  16        unit value for real world offset x of diffuse texture
    NwTextureValueType::diffuse_offset_y_value 17        double value with real world offset y of diffuse texture
    NwTextureValueType::diffuse_offset_y_unit  18        unit value for real world offset y of diffuse texture
    NwTextureValueType::diffuse_raster_image   19        raster image of diffuse texture
    NwTextureValueType::opacity_path           20        OdString value with path to opacity texture
    NwTextureValueType::opacity_scale_x_value  21        double value with real world scale x of opacity texture
    NwTextureValueType::opacity_scale_x_unit   22        unit value for real world scale x of opacity texture
    NwTextureValueType::opacity_scale_y_value  23        double value with real world scale y of opacity texture
    NwTextureValueType::opacity_scale_y_unit   24        unit value for real world scale y of opacity texture
    NwTextureValueType::opacity_offset_x_value 25        double value with real world offset x of opacity texture
    NwTextureValueType::opacity_offset_x_unit  26        unit value for real world offset x of opacity texture
    NwTextureValueType::opacity_offset_y_value 27        double value with real world offset y of opacity texture
    NwTextureValueType::opacity_offset_y_unit  28        unit value for real world offset y of opacity texture
    NwTextureValueType::opacity_raster_image   29        raster image of opacity texture
    NwTextureValueType::bump_path              30        OdString value with path to bump texture
    NwTextureValueType::bump_scale_x_value     31        double value with real world scale x of bump texture
    NwTextureValueType::bump_scale_x_unit      32        unit value for real world scale x of bump texture
    NwTextureValueType::bump_scale_y_value     33        double value with real world scale y of bump texture
    NwTextureValueType::bump_scale_y_unit      34        unit value for real world scale y of bump texture
    NwTextureValueType::bump_offset_x_value    35        double value with real world offset x of bump texture
    NwTextureValueType::bump_offset_x_unit     36        unit value for real world offset x of bump texture
    NwTextureValueType::bump_offset_y_value    37        double value with real world offset y of bump texture
    NwTextureValueType::bump_offset_y_unit     38        unit value for real world offset y of bump texture
    NwTextureValueType::bump_raster_image      39        raster image of bump texture
    </table>
  */
  NwModelUnits::Enum getUnitValue(NwTextureValueType::Enum type) const;

  /** \details
    Returns raster image param value depending from value of mask

    \param
      type [in] param with type NwTextureValueType::Enum.

    \returns
      Return OdGiRasterImagePtr value depending from value of type.

    \remarks
      type must be one of the following:

    <table>
    Name                                       Value     Description 
    NwTextureValueType::pattern_path           0         OdString value with path to pattern texture
    NwTextureValueType::pattern_scale_x_value  1         double value with real world scale x of pattern texture
    NwTextureValueType::pattern_scale_x_unit   2         unit value for real world scale x of pattern texture
    NwTextureValueType::pattern_scale_y_value  3         double value with real world scale y of pattern texture
    NwTextureValueType::pattern_scale_y_unit   4         unit value for real world scale y of pattern texture
    NwTextureValueType::pattern_offset_x_value 5         double value with real world offset x of pattern texture
    NwTextureValueType::pattern_offset_x_unit  6         unit value for real world offset x of pattern texture
    NwTextureValueType::pattern_offset_y_value 7         double value with real world offset y of pattern texture
    NwTextureValueType::pattern_offset_y_unit  8         unit value for real world offset y of pattern texture
    NwTextureValueType::pattern_raster_image   9         raster image of pattern texture
    NwTextureValueType::diffuse_path           10        OdString value with path to diffuse texture
    NwTextureValueType::diffuse_scale_x_value  11        double value with real world scale x of diffuse texture
    NwTextureValueType::diffuse_scale_x_unit   12        unit value for real world scale x of diffuse texture
    NwTextureValueType::diffuse_scale_y_value  13        double value with real world scale y of diffuse texture
    NwTextureValueType::diffuse_scale_y_unit   14        unit value for real world scale  of diffuse texture
    NwTextureValueType::diffuse_offset_x_value 15        double value with real world offset x of diffuse texture
    NwTextureValueType::diffuse_offset_x_unit  16        unit value for real world offset x of diffuse texture
    NwTextureValueType::diffuse_offset_y_value 17        double value with real world offset y of diffuse texture
    NwTextureValueType::diffuse_offset_y_unit  18        unit value for real world offset y of diffuse texture
    NwTextureValueType::diffuse_raster_image   19        raster image of diffuse texture
    NwTextureValueType::opacity_path           20        OdString value with path to opacity texture
    NwTextureValueType::opacity_scale_x_value  21        double value with real world scale x of opacity texture
    NwTextureValueType::opacity_scale_x_unit   22        unit value for real world scale x of opacity texture
    NwTextureValueType::opacity_scale_y_value  23        double value with real world scale y of opacity texture
    NwTextureValueType::opacity_scale_y_unit   24        unit value for real world scale y of opacity texture
    NwTextureValueType::opacity_offset_x_value 25        double value with real world offset x of opacity texture
    NwTextureValueType::opacity_offset_x_unit  26        unit value for real world offset x of opacity texture
    NwTextureValueType::opacity_offset_y_value 27        double value with real world offset y of opacity texture
    NwTextureValueType::opacity_offset_y_unit  28        unit value for real world offset y of opacity texture
    NwTextureValueType::opacity_raster_image   29        raster image of opacity texture
    NwTextureValueType::bump_path              30        OdString value with path to bump texture
    NwTextureValueType::bump_scale_x_value     31        double value with real world scale x of bump texture
    NwTextureValueType::bump_scale_x_unit      32        unit value for real world scale x of bump texture
    NwTextureValueType::bump_scale_y_value     33        double value with real world scale y of bump texture
    NwTextureValueType::bump_scale_y_unit      34        unit value for real world scale y of bump texture
    NwTextureValueType::bump_offset_x_value    35        double value with real world offset x of bump texture
    NwTextureValueType::bump_offset_x_unit     36        unit value for real world offset x of bump texture
    NwTextureValueType::bump_offset_y_value    37        double value with real world offset y of bump texture
    NwTextureValueType::bump_offset_y_unit     38        unit value for real world offset y of bump texture
    NwTextureValueType::bump_raster_image      39        raster image of bump texture
    </table>
  */
  OdGiRasterImagePtr getRasterImageValue(NwTextureValueType::Enum type) const;

  /** \details
    Returns pattern map path of texture.

    \returns Returns OdString object with pattern map path of texture.

    \sa
      deprecated
  */
  OdString getPatternMapPath() const;

  /** \details
    Returns pattern map scale x of texture.

    \returns Returns double object with pattern map scale x of texture.

    \sa
      deprecated
  */
  double getPatternMapScaleX() const;

  /** \details
    Returns pattern map scale y of texture.

    \returns Returns double object with pattern map scale y of texture.

    \sa
      deprecated
  */
  double getPatternMapScaleY() const;

  /** \details
    Returns bump map path of texture.

    \returns Returns OdString object with bump map path of texture.

    \sa
      deprecated
  */
  OdString getBumpMapPath() const;

  /** \details
    Returns bump map scale x of texture.

    \returns Returns double object with bump map scale x of texture.

    \sa
      deprecated
  */
  double getBumpMapScaleX() const;

  /** \details
    Returns bump map scale y of texture.

    \returns Returns double object with bump map scale y of texture.

    \sa
      deprecated
  */
  double getBumpMapScaleY() const;

  /** \details
    Returns opacity map path of texture.

    \returns Returns OdString object with opacity map path of texture.

    \sa
      deprecated
  */
  OdString getOpacityMapPath() const;

  /** \details
    Returns opacity map scale x of texture.

    \returns Returns double object with opacity map scale x of texture.

    \sa
      deprecated
  */
  double getOpacityMapScaleX() const;

  /** \details
    Returns opacity map scale y of texture.

    \returns Returns double object with opacity map scale y of texture.

    \sa
      deprecated
  */
  double getOpacityMapScaleY() const;

  /** \details
    Returns diffuse map path of texture.

    \returns Returns OdString object with diffuse map path of texture.

    \sa
      deprecated
  */
  OdString getDiffuseMapPath() const;

  /** \details
    Returns diffuse map scale x of texture.

    \returns Returns double object with diffuse map scale x of texture.

    \sa
      deprecated
  */
  double getDiffuseMapScaleX() const;

  /** \details
    Returns diffuse map scale y of texture.

    \returns Returns double object with diffuse map scale y of texture.

    \sa
      deprecated
  */
  double getDiffuseMapScaleY() const;

  /** \details
    Returns diffuse intensity of texture.

    \returns Returns double object with diffuse intensity of texture.
  */
  double getDiffuseIntensity() const;

  /** \details
    Returns specular intensity of texture.

    \returns Returns double object with specular intensity of texture.
  */
  double getSpecularIntensity() const;

  /** \details
    Returns reflect intensity of texture.

    \returns Returns double object with reflect intensity of texture.
  */
  double getReflectIntensity() const;

  /** \details
    Returns tint color of this texture

    \param color [out] Object of OdNwColor with tint color of this texture.

    \returns Returns eOk if successful, or an appropriate error code if not.
  */
  OdResult getTint(OdNwColor& color) const;

  /** \details
    Returns status of using tint color

    \returns Returns true if tint color is uesd, the returned false if not.
  */
  bool IsUseTintColor() const;

  /** \details
    Returns transmit color of this texture

    \param color [out] Object of OdNwColor with transmit color of this texture.

    \returns Returns eOk if successful, or an appropriate error code if not.
  */
  OdResult getTransmitColor(OdNwColor& color) const;

  /** \details
    Returns status of coloring by object

    \returns Return true if texture's color by object, the returned false if not.

    \sa
      deprecated
  */
  bool IsColorByObject() const;

  /** \details
    Returns status of having bitmap in texture

    \returns Return true if texture have bitmap, the returned false if not.

    \sa
      deprecated
  */
  bool isHaveBitmap() const;

  /** \details
    Returns a smart pointer to bitmap of texure

    \returns Returns a smart pointer to OdGiRasterImage object with bitmap of texure. If database don't have bitmap, the returned smart pointer is NULL.

    \sa
      deprecated
  */
  OdGiRasterImagePtr getBitmap() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwTexture object pointers.
*/
typedef OdSmartPtr<OdNwTexture> OdNwTexturePtr;

#endif //__TNW_TEXTURE_H__
