/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_CATEGORY_H__
#define __TNW_CATEGORY_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#define STL_USING_MAP
#include "OdaSTL.h"

class OdNwProperty;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwProperty object pointers.
*/
typedef OdSmartPtr<OdNwProperty> OdNwPropertyPtr;

/** \details
  This class represents category of properties.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwCategory : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwCategory);

  OdNwCategory();

public:

  virtual ~OdNwCategory();

public:

  /** \details
    Returns the status of the having of stable id in the category.

    \returns Returns true if category have stable id, return false if not.
  */
  bool hasStableId() const;

  /** \details
    Returns Int64StableId of category.

    \returns Returns OdUInt64 object with Int64StableId of category.
  */
  OdUInt64 getInt64StableId() const;

  /** \details
    Returns StringStableId of category.

    \returns Returns OdString object with StringStableId of category.
  */
  OdString getStringStableId() const;

  /** \details
    Returns display name of category.

    \returns Returns display name of category.

    \remarks display name is localing name
  */
  OdString getDisplayName() const;

  /** \details
    Returns name of category.

    \returns Returns name of category.
  */
  OdString getName() const;

  /** \details
    Returns std::map with smart pointers to category's properties

    \param mProperties [out] std::map with smart pointers to property like value and string description like key.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getProperties(std::map<OdString, OdNwPropertyPtr>& mProperties) const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwCategory object pointers.
*/
typedef OdSmartPtr<OdNwCategory> OdNwCategoryPtr;

#endif //__TNW_CATEGORY_H__
