/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_MODEL_H__
#define __TNW_MODEL_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "NwModelType.h"
#define STL_USING_MAP
#include "OdaSTL.h"
#include "NwModelUnits.h"

class OdGeMatrix3d;
class OdNwProperty;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwProperty object pointers.
*/
typedef OdSmartPtr<OdNwProperty> OdNwPropertyPtr;

/** \details
  This class represents a model in database

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwModel : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwModel);

  OdNwModel();

public:

  virtual ~OdNwModel();

public:

  /** \details
    Returns the filename of the model.

    \returns Return filename of the model.
  */
  OdString getFileName() const;

  /** \details
    Returns the path of the model.

    \returns Return path of the model.
  */
  OdString getPath() const;

  /** \details
    Returns original source filename this model was first converted from.

    \returns Return original source filename this model was first converted from.
  */
  OdString getSourceFileName() const;

  /** \details
    Returns status of  model's changing after loading from original.

    \returns Return true if this model was changing after loading from original.
  */
  bool isChange() const;

  /** \details
    Returns transform matrix applied to the model.

    \returns Return object of OdGeMatrix3d with transform which applied to the model.
  */
  OdGeMatrix3d getTransform() const;

  /** \details
    Returns the type of model was used in loading. 
    
    \remarks
      getType() returns one of the following:
    
    <table>
    Name                        Value   Description
    NwModelType::undefined      0       original model is undefined.
    NwModelType::dwg_model      1       original model was dwg.
    NwModelType::dgn_model      2       original model was dgn.
    NwModelType::revit_model    3       original model was revit.
    NwModelType::nw_model       4       original model was Naviswork.
    NwModelType::nw_13_model    5       original model was Naviswork.
    </table>
  */
  NwModelType::Enum getType() const;

  /** \details
    Return units in which dimensions of this model are defined

    \remarks
      getUnits() returns one of the following:

    <table>
    Name                                 Value   Description
    NwModelUnits::UNITS_METERS           0x00    SI meter. Length of the path travelled by light in vacuum in 1 299,792,458th of second.
    NwModelUnits::UNITS_CENTIMETERS      0x01    1 100th of SI meter.
    NwModelUnits::UNITS_MILLIMETERS      0x02    1 1000th of an SI meter.
    NwModelUnits::UNITS_FEET             0x03    1 3rd of a yard.
    NwModelUnits::UNITS_INCHES           0x04    1 12th of a foot, 2.54 SI centimeters.
    NwModelUnits::UNITS_YARDS            0x05    Imperial yard, 0.9144 SI meters.
    NwModelUnits::UNITS_KILOMETERS       0x06    1000 SI meters
    NwModelUnits::UNITS_MILES            0x07    1760 yards
    NwModelUnits::UNITS_MICROMETERS      0x08    1 1,000,000th of an SI meter, micron
    NwModelUnits::UNITS_MILS             0x09    1 1,000th of inch
    NwModelUnits::UNITS_MICROINCHES      0x0A    1 1,000,000th of inch
    </table>
  */
  NwModelUnits::Enum getUnits() const;

  /** \details
    Returns std::map with smart pointers to model's properties

    \param mProperties [out] std::map with smart pointers to property like value and string description like key.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getProperties(std::map<OdString, OdNwPropertyPtr>& mProperties) const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwModel object pointers.
*/
typedef OdSmartPtr<OdNwModel> OdNwModelPtr;

#endif //__TNW_MODEL_H__
