/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef TNW_UNIT_UTILS_H_
#define TNW_UNIT_UTILS_H_

#include "NwExport.h"
#include "NwModelUnits.h"
#include "OdResult.h"

class OdGeMatrix3d;

/** \details
  This class holds methods for working with value with units

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwUnitUtils
{
public:
  /** \details
    Returns the double result of converting from current units to desired

    \param value [in] double value which need to convert.
    \param currentModelUnit [in] units from which need to convert.
    \param desiredModelUnit [in] units to which need to convert.

    \returns
      double value with converting result.
  */
  static double convert(const double& value, NwModelUnits::Enum currentModelUnit, NwModelUnits::Enum desiredModelUnit);

  /** \details
    Convert the matrix from current units to desired

    \param value [out] OdGeMatrix3d value which need to convert.
    \param currentModelUnit [in] units from which need to convert.
    \param desiredModelUnit [in] units to which need to convert.

    \returns
      Return eOk if successful
  */
  static OdResult convertMatrix3d(OdGeMatrix3d& value, NwModelUnits::Enum currentModelUnit, NwModelUnits::Enum desiredModelUnit);
};

#endif  // TNW_UNIT_UTILS_H_

