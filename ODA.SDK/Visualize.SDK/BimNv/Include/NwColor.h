/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_COLOR_H__
#define __TNW_COLOR_H__

#include "NwExport.h"
#include "NwObject.h"

class OdNwColorImpl;

/** \details
  This class represents a color

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwColor
{
public:
  OdNwColor();
  OdNwColor(float a, float r, float g, float b);
  OdNwColor(float r, float g, float b);
  OdNwColor(const OdNwColor&);
  OdNwColor& operator=(const OdNwColor&);
  ~OdNwColor();

  /** \details
    Returns the database ID of color.

    \return OdNwObjectId object with database ID of color
  */
  virtual OdNwObjectId objectId() const;

  /** \details
    Returns the database ID of color's owner.

    \return pointer to OdDbStub with database ID of color's owner
  */
  virtual OdDbStub* ownerId() const;

  /** \details
    Returns the database ID of color.

    \return pointer to OdDbStub with database ID of color

    \remarks Returns a null pointer if color is not persistent.
  */
  OdDbStub* id() const;

public:

  /** \details
    Alpha of color component

    \returns Return float value of color's alpha.
  */
  float A() const;

  /** \details
    Red color of component

    \returns Return float value of color's red.
  */
  float R() const;

  /** \details
    Green color of component

    \returns Return float value of color's green.
  */
  float G() const;

  /** \details
    Blue color of component

    \returns Return float value of color's blue.
  */
  float B() const;

public:

  /** \details
    Returns value of color in ODCOLORREF's type

    \returns Return color value in ODCOLORREF's type.
  */
  ODCOLORREF ToColor() const;

  /** \details
    Scales intensity of color by value

    \param color [in]  OdNwColor's object for scales intensity of color.

    \returns Return OdNwColor's object with value of scaling.
  */
  OdNwColor Multiply(const OdNwColor& color) const;

  /** \details
    Scales intensity of color by value

    \param color [in]  ODCOLORREF's object for scales intensity of color.

    \returns Return OdNwColor's object with value of scaling.
  */
  OdNwColor Multiply(ODCOLORREF color) const;

private:
  friend class OdNwColorImpl;
  OdSmartPtr<OdNwColorImpl> m_pImpl;
};

#endif // __TNW_COLOR_H__
