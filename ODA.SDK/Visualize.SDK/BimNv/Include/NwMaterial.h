/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_MATERIAL_H__
#define __TNW_MATERIAL_H__

#include "NwCategory.h"

class OdNwColor;

/** \details
  This class represents geometry material within the model hierarchy.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwMaterial : public OdNwCategory
{
  ODRX_DECLARE_MEMBERS(OdNwMaterial);

  OdNwMaterial();

public:

  virtual ~OdNwMaterial();

public:

  /** \details
    Returns ambient color of this material

    \param color [out] Object of OdNwColor with ambient color of this material.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getAmbient(OdNwColor& color) const;

  /** \details
    Returns diffuse color of this material

    \param color [out] Object of OdNwColor with diffuse color of this material.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks it is main color
  */
  OdResult getDiffuse(OdNwColor& color) const;

  /** \details
    Returns specular color of this material

    \param color [out] Object of OdNwColor with specular color of this material.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getSpecular(OdNwColor& color) const;

  /** \details
    Returns emissive color of this material

    \param color [out] Object of OdNwColor with emissive color of this material.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getEmissive(OdNwColor& color) const;

  /** \details
    Returns shininess of material.

    \returns Returns float object with shininess of material.
  */
  float getShininess() const;

  /** \details
    Returns transparency of material.

    \returns Returns float object with transparency of material.
  */
  float getTransparency() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwMaterial object pointers.
*/
typedef OdSmartPtr<OdNwMaterial> OdNwMaterialPtr;

#endif //__TNW_MATERIAL_H__
