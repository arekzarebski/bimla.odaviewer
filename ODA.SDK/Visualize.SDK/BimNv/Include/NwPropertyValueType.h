/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef TNW_PROPERTY_VALUE_TYPE_H_
#define TNW_PROPERTY_VALUE_TYPE_H_

/** \details
  type of property's value

  <group TNW_Namespaces>
*/
namespace NwPropertyValueType
{
  enum Enum
  {
    /**default value type.*/
    value_type_default = 0,
    /**boolean value type.*/
    value_type_bool,
    /**double value type.*/
    value_type_double,
    /**float value type.*/
    value_type_float,
    /**integer value type.*/
    value_type_OdInt32,
    /**short integer value type.*/
    value_type_OdInt8,
    /**unsigned integer value type.*/
    value_type_OdUInt32,
    /**unsigned short integer value type.*/
    value_type_OdUInt8,
    /**OdString value type.*/
    value_type_OdString,
    /**OdStringArray value type.*/
    value_type_OdStringArray,
    /**OdGeVector3d value type.*/
    value_type_OdGeVector3d,
    /**unsigned long integer value type.*/
    value_type_OdUInt64,
    /**OdNwColor value type.*/
    value_type_OdNwColor,
    /**datetime value type.*/
    value_type_tm
  };
}
#endif  // TNW_PROPERTY_VALUE_TYPE_H_

