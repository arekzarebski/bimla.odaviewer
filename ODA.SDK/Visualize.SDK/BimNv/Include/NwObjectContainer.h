/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2018, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2018 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef __NW_OBJECTCONTAINER_H__
#define __NW_OBJECTCONTAINER_H__

#include "OdArray.h"
#include "RxObjectImpl.h"
#include "NwObjectIterator.h"
#define STL_USING_ALGORITHM
#include "OdaSTL.h"

/** \details
  This class represents internal data structure for storing IDs.

  <group OdNw_Classes>
*/
typedef OdNwObjectIdArray NwIdContainer;

class NwIdContainerType
{
public:
  typedef OdNwObjectId item;
  typedef NwIdContainer container;
  typedef NwIdContainer::iterator iterator;
  typedef NwIdContainer::const_iterator const_iterator;

  static bool isDone(const container* pContainer, const const_iterator& iter)
  { 
    return (iter == pContainer->end() || iter < pContainer->begin());
  }
  static const_iterator find(const container* pContainer, const item& id)
  {
    return std::find(pContainer->begin(), pContainer->end(), id);
  }
  static bool itemIsErased(const const_iterator& iter)
  {
    return (*iter).isErased();
  }
};

#define ITER_IS_DONE() TContainerType::isDone(m_pContainer, m_iter)
#define ITER_FIND(id) TContainerType::find(m_pContainer, id)

/** \details
  This class represents Implementation of iterator's base methods.

  <group OdNw_Classes>
*/
class NwBaseIteratorDummyInterface
{};

template < class TItem,
           class TContainerType,
           class TInterface = NwBaseIteratorDummyInterface >
class NwBaseIterator : public TInterface
{
public:
  void start(bool bForward = true, bool bSkipDeleted = true)
  {
    ODA_ASSERT(m_pContainer);
    if (bForward)
    {
      m_iter = m_pContainer->begin();
    }
    else
    {
      m_iter = m_pContainer->end();
      if (m_pContainer->size())
      {
        --m_iter;
      }
    }
    if (bSkipDeleted)
    {
      skipDeleted(bForward);
    }
  }

  bool done() const
  {
    return ITER_IS_DONE();
  }

  bool seek(const TItem& id)
  {
    m_iter = ITER_FIND(id);
    return !ITER_IS_DONE();
  }

  void step(bool bForward = true, bool bSkipDeleted = true)
  {
    if (!ITER_IS_DONE())
    {
      if (bForward)
        ++m_iter;
      else
        --m_iter;

      if (bSkipDeleted)
      {
        skipDeleted(bForward);
      }
    }
  }

  TItem item() const
  {
    return ITER_IS_DONE() ? TItem(0) : *m_iter;
  }

protected:
  // Constructors
  NwBaseIterator()
    : m_pContainer((OD_TYPENAME TContainerType::container*)0)
  {}

  NwBaseIterator(const typename TContainerType::container* pContainer)
    : m_pContainer(pContainer)
  {}

  // Support method
  virtual void skipDeleted(bool bForward)
  {
    if (bForward)
    {
      while (!ITER_IS_DONE() && TContainerType::itemIsErased(m_iter))
      {
        ++m_iter;
      }
    }
    else
    {
      while (!ITER_IS_DONE() && TContainerType::itemIsErased(m_iter))
      {
        --m_iter;
      }
    }
  }

  const OD_TYPENAME TContainerType::container* m_pContainer;
  OD_TYPENAME TContainerType::const_iterator m_iter;
};

/** \details
  This class represents internal inline pared-down interface to IDs container.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwObjectContainer : public NwIdContainer
{
public:
  class iterator : public NwBaseIterator<OdNwObjectId, NwIdContainerType>
  {
  public:
    iterator(const OdNwObjectContainer* pContainer)
      : NwBaseIterator<OdNwObjectId, NwIdContainerType>(pContainer)
    {}
  };

  OdNwObjectContainer()
  {}
  void append(const OdNwObjectId& id)
  {
    push_back(id);
  }
  void remove(const OdNwObjectId& id)
  {
    NwIdContainer::remove(id);
  }
  OdUInt32 size() const
  {
    return NwIdContainer::size();
  }
  OdNwObjectId at(OdUInt32 i) const
  {
    return NwIdContainer::at(i);
  }
  void clear()
  {
    NwIdContainer::clear();
  }
  OdNwObjectIteratorPtr createIterator(bool atBeginning = true, bool skipDeleted = true) const;
};

#endif //__NW_OBJECTCONTAINER_H__
