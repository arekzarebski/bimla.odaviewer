/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_GEOMETRYTEXT_H__
#define __TNW_GEOMETRYTEXT_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "Ge/GePoint3dArray.h"

class OdNwTextFontInfo;
class OdGeVector3d;
class OdGePoint3d;

/** \details
  This class represents geometry with text type.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwGeometryText : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwGeometryText);

  OdNwGeometryText();

public:

  virtual ~OdNwGeometryText();

public:

  /** \details
    Returns left point of text geometry data.

    \returns Returns OdGePoint3d object with left point of text geometry data.
  */
  OdGePoint3d getLeftPoint() const;

  /** \details
    Returns right point of text geometry data.

    \returns Returns OdGePoint3d object with right point of text geometry data.
  */
  OdGePoint3d getRightPoint() const;

  /** \details
    Returns normal of text geometry data.

    \returns Returns OdGeVector3d object with normal of text geometry data.
  */
  OdGeVector3d getNormal() const;

  /** \details
    Returns text of text geometry data.

    \returns Returns OdString object with text of text geometry data.
  */
  OdString getText() const;

  /** \details
    Returns rotation of text geometry data.

    \returns Returns float object with rotation of text geometry data.
  */
  float getRotation() const;

  /** \details
    Returns font of text geometry data.

    \returns Returns OdNwTextFontInfo object with font of text geometry data.
  */
  OdNwTextFontInfo getFont() const;

  /** \details
    Returns scale of text geometry data.

    \returns Returns float object with scale of text geometry data.
  */
  float getScale() const;

  /** \details
    Returns array with vertexes.

    \returns Returns OdArray objects with vertexes.
  */
  OdGePoint3dArray getVertexes() const;

  /** \details
    Returns array with colors.

    \returns Returns OdArray objects with colors.
  */
  OdArray<OdUInt32> getColors() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryText object pointers.
*/
typedef OdSmartPtr<OdNwGeometryText> OdNwGeometryTextPtr;

#endif //__TNW_GEOMETRYTEXT_H__
