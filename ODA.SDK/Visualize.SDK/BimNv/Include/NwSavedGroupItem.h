/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_SAVEDGROUP_H__
#define __TNW_SAVEDGROUP_H__

#include "NwSavedItem.h"
#include "NwViewpoint.h"

/** \details
  This class represents a base class for containing arrays with saved items.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwSavedGroupItem : public OdNwSavedItem
{
  ODRX_DECLARE_MEMBERS(OdNwSavedGroupItem);

  OdNwSavedGroupItem();

public:

  virtual ~OdNwSavedGroupItem();

public:

  /** \details
    Acesses the Viewpoint ascociated with this object. 

    \returns Returns pointers to OdNwViewpoint.
  */
  OdArray<OdNwSavedItemPtr> getChildren() const;
};

typedef OdSmartPtr<OdNwSavedGroupItem> OdNwSavedGroupItemPtr;

#endif //__TNW_SAVEDVIEWPOINT_H__
