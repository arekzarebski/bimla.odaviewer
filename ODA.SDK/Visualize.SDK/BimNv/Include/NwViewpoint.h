/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_VIEWPOINT_H__
#define __TNW_VIEWPOINT_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "NwCameraMode.h"
#include "NwLightType.h"
#include "NwModeType.h"
#include "NwViewType.h"

class OdGeQuaternion;
class OdGeVector3d;
class OdGePoint3d;

/** \details
  This class represents a view to models

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwViewpoint : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwViewpoint);

  OdNwViewpoint();

public:

  virtual ~OdNwViewpoint();

public:

  /** \details
    Returns the name of the viewpoint.

    \returns Return name of the viewpoint.
  */
  OdString getName() const;

  /** \details
    Returns angular speed of the camera.

    \returns Return double value of angular speed of the camera.
  */
  double getAngularSpeed() const;

  /** \details
    Returns distance from eye point to far plane along view direction

    \returns Return double value of distance from eye point to far plane.

    \remarks it must be greater than near distance
  */
  double getFarDistance() const;

  /** \details
    Returns focal distance of the camera.

    \returns Return double value of focal distance of the camera.
  */
  double getFocalDistance() const;

  /** \details
    Returns flag of setting of AngularSpeed

    \returns Return true if AngularSpeed has been set.
  */
  bool hasAngularSpeed() const;

  /** \details
    Returns flag of setting of FocalDistance

    \returns Return true if FocalDistance has been set.
  */
  bool hasFocalDistance() const;

  /** \details
    Returns flag of setting of Lighting

    \returns Return true if Lighting has been set.
  */
  bool hasLighting() const;

  /** \details
    Returns flag of setting of LinearSpeed

    \returns Return true if LinearSpeed has been set.
  */
  bool hasLinearSpeed() const;

  /** \details
    Returns flag of setting of RenderStyle

    \returns Return true if RenderStyle has been set.
  */
  bool hasRenderStyle() const;

  /** \details
    Returns flag of setting of WorldUpVector

    \returns Return true if WorldUpVector has been set.
  */
  bool hasWorldUpVector() const;

  /** \details
    Returns defines camera field of view in combination with aspect ratio.

    \returns Return double value of defines camera field of view in combination with aspect ratio.

    \remarks
      for perspective - vertical field of view (the angle between top and bottom planes of camera view frustum)
      for orthographic - distance between top and bottom planes of the camera view frustum
  */
  double getHeightField() const;

  /** \details
    Returns required horizontal scaling of image when adapting the camera view to display window.

    \returns Return double value of required horizontal scaling of image.
  */
  double getHorizontalScale() const;

  /** \details
    Returns the type of Lighting used for this viewpoint. 
    
    \remarks
      getLighting() returns one of the following:
    
    <table>
    Name                        Value   Description
    NwLightType::NO_LIGHT       0       Turns off lighting completely.
    NwLightType::SCENE_LIGHT    1       Lights from user in model.
    NwLightType::HEAD_LIGHT     2       An automatic headlight from viewer.
    NwLightType::FULL_LIGHT     3       Advanced level of lighting from application.
    </table>
  */
  NwLightType::Enum getLighting() const;

  /** \details
    Returns Linear speed of the camera.

    \returns Return double value of Linear speed of the camera.
  */
  double getLinearSpeed() const;

  /** \details
    Returns distance from eye point to near plane along view direction

    \returns Return double value of distance from eye point to near plane.

    \remarks it must be greater than zero
  */
  double getNearDistance() const;

  /** \details
    Returns position of the camera

    \returns Return OdGePoint3d value of position of the camera.
  */
  OdGePoint3d getPosition() const;

  /** \details
    Returns projection type of the current viewpoint. 
    
    \remarks
      getProjection() returns one of the following:
    
    <table>
    Name                        Value   Description
    NwViewType::PERSPECTIVE     0       Perspective viewpoint.
    NwViewType::ORTHOGRAPHIC    1       Orthographic viewpoint .
    </table>
  */
  NwViewType::Enum getProjection() const;

  /** \details
    Returns style used for rendering. 
    
    \remarks
      getRenderStyle() returns one of the following:
    
    <table>
    Name                        Value   Description
    NwModeType::FULL_RENDER     0       Highest quality, as specified in model.
    NwModeType::SHADED          1       Smooth shaded (simple shade model, no textures).
    NwModeType::WIREFRAME       2       Wire frame.
    NwModeType::HIDDEN_LINE     3       Hidden line wire frame.
    </table>
  */
  NwModeType::Enum getRenderStyle() const;

  /** \details
    Returns rotation of camera from base orientation

    \returns Return OdGeQuaternion rotation of camera from base orientation.
  */
  OdGeQuaternion getRotation() const;

  /** \details
    Returns avatar used for this viewpoint

    \returns Return avatar used for this viewpoint.
  */
  OdString getAvatar() const;

  /** \details
    Returns type of avatar for this viewpoint. 
    
    \remarks
      getViewerCameraMode() returns one of the following:
    
    <table>
    Name                        Value   Description
    NwCameraMode::FIRST         0       First person mode.
    NwCameraMode::THIRD         1       Third person mode.
    </table>
  */
  NwCameraMode::Enum getViewerCameraMode() const;

  /** \details
    Returns up vector

    \returns Return OdGeVector3d with up vector.
  */
  OdGeVector3d getWorldUpVector() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwViewpoint object pointers.
*/
typedef OdSmartPtr<OdNwViewpoint> OdNwViewpointPtr;

#endif //__TNW_VIEWPOINT_H__
