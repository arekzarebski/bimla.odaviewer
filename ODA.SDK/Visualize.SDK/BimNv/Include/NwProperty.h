/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_PROPERTY_H__
#define __TNW_PROPERTY_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "NwPropertyValueType.h"
#include "StringArray.h"

class OdNwColor;
class OdGeVector3d;

/** \details
  This class represents a publish property

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwProperty : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwProperty);

  OdNwProperty();

public:

  virtual ~OdNwProperty();

public:

  /** \details
    Returns the display name of property.

    \returns Return display name of property.
  */
  OdString getDisplayName() const;

  /** \details
    Returns the name of property.

    \returns Return name of property.
  */
  OdString getName() const;

  /** \details
    Returns the type of property's value.
    
    \remarks
      getValueType() returns one of the following:
    
    <table>
    Name                                           Value   Description
    NwPropertyValueType::value_type_default        0       default value type.
    NwPropertyValueType::value_type_bool           1       boolean value type.
    NwPropertyValueType::value_type_double         2       double value type.
    NwPropertyValueType::value_type_float          3       float value type.
    NwPropertyValueType::value_type_OdInt32        4       integer value type.
    NwPropertyValueType::value_type_OdInt8         5       short integer value type.
    NwPropertyValueType::value_type_OdUInt32       6       unsigned integer value type.
    NwPropertyValueType::value_type_OdUInt8        7       unsigned short integer value type.
    NwPropertyValueType::value_type_OdString       8       OdString value type.
    NwPropertyValueType::value_type_OdStringArray  9       OdStringArray value type.
    NwPropertyValueType::value_type_OdGeVector3d   10      OdGeVector3d value type.
    NwPropertyValueType::value_type_OdUInt64       11      unsigned long integer value type.
    NwPropertyValueType::value_type_OdNwColor      12      OdNwColor value type.
    NwPropertyValueType::value_type_tm             13      datetime value type.
    </table>
  */
  NwPropertyValueType::Enum getValueType() const;

  /** \details
    Returns string value

    \param val [out] OdString object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is OdString
  */
  OdResult getValue(OdString& val) const;

  /** \details
    Returns double value

    \param val [out] double object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is double
  */
  OdResult getValue(double& val) const;

  /** \details
    Returns float value

    \param val [out] float object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is float
  */
  OdResult getValue(float& val) const;

  /** \details
    Returns OdInt32 value

    \param val [out] OdInt32 object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is integer
  */
  OdResult getValue(OdInt32& val) const;

  /** \details
    Returns OdInt8 value

    \param val [out] OdInt8 object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is short integer
  */
  OdResult getValue(OdInt8& val) const;

  /** \details
    Returns OdUInt32 value

    \param val [out] OdUInt32 object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is unsigned integer
  */
  OdResult getValue(OdUInt32& val) const;

  /** \details
    Returns OdUInt8 value

    \param val [out] OdUInt8 object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is unsigned short integer
  */
  OdResult getValue(OdUInt8& val) const;

  /** \details
    Returns OdStringArray value

    \param val [out] OdStringArray object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is OdStringArray
  */
  OdResult getValue(OdStringArray& val) const;

  /** \details
    Returns OdGeVector3d value

    \param val [out] OdGeVector3d object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is OdGeVector3d
  */
  OdResult getValue(OdGeVector3d& val) const;

  /** \details
    Returns OdUInt64 value

    \param val [out] OdUInt64 object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is unsigned long integer
  */
  OdResult getValue(OdUInt64& val) const;

  /** \details
    Returns OdNwColor value

    \param val [out] OdNwColor object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is OdNwColor
  */
  OdResult getValue(OdNwColor& val) const;

  /** \details
    Returns tm value

    \param val [out] tm object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is datetime
  */
  OdResult getValue(tm& val) const;

  /** \details
    Returns bool value

    \param val [out] bool object with property's value.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if property's value is boolean
  */
  OdResult getValue(bool& val) const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwProperty object pointers.
*/
typedef OdSmartPtr<OdNwProperty> OdNwPropertyPtr;

#endif //__TNW_PROPERTY_H__

