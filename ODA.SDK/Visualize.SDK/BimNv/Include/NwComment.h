/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_COMMENT_H__
#define __TNW_COMMENT_H__

#include "NwObject.h"
#include "NwExport.h"
#include "NwCommentStatus.h"

class OdNwCommentImpl;

/** \details
  This class represents a user comment.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwComment
{
public:
  OdNwComment();
  OdNwComment(const OdNwComment&);
  OdNwComment& operator=(const OdNwComment&);
  ~OdNwComment();

  /** \details
    Author (creator) of comment  .

    \returns Returns string with author.
  */
  OdString getAuthor() const;

  /** \details
    Body of comment  

    \returns Returns OdString value with body of comment.
  */
  OdString getBody() const;

  /** \details
    Date and time at which comment was created 

    \returns OdUInt32 value with create date.
  */
  OdUInt32 getCreateDate() const;

  /** \details
    Comment identifier. Unique in databse. 0 if no meaningful id. 

    \returns OdUInt32 value with comment identifier in databse.
  */
  OdUInt32 getId() const;

  /** \details
    Status of comment 

    \returns NwCommentStatus::Enum value with comment status.

    \remarks
      type must be one of the following:

    <table>
    Name                             Value     Description 
    NwCommentStatus::NEW             0x00      New comment
    NwCommentStatus::ACTIVE          0x01      Active comment
    NwCommentStatus::APPROVED        0x02      Comment was approved
    NwCommentStatus::RESOLVED        0x03      Comment was resolved
    </table>
  */
  NwCommentStatus::Enum getStatus() const;
  
private:
  friend class OdNwCommentImpl;
  OdSmartPtr<OdNwCommentImpl> m_pImpl;
};

#endif //__TNW_COMMENT_H__
