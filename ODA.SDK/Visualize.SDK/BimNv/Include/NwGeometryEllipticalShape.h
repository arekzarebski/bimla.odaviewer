/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_GEOMETRYELLIPTICALSHAPE_H__
#define __TNW_GEOMETRYELLIPTICALSHAPE_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "Ge/GeVector3d.h"

/** \details
  This class represents geometry with elliptical shape type.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwGeometryEllipticalShape : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwGeometryEllipticalShape);

  OdNwGeometryEllipticalShape();

public:

  virtual ~OdNwGeometryEllipticalShape();

public:

  /** \details
    Returns origin of geometry data.

    \returns Returns OdGePoint3d object with origin of geometry data.
  */
  OdGePoint3d getOrigin() const;

  /** \details
    Returns x radius of elliptical geometry data.

    \returns Returns double object with x radius of elliptical geometry data.
  */
  double getXRadius() const;

  /** \details
    Returns y radius of elliptical geometry data.

    \returns Returns double object with y radius of elliptical geometry data.
  */
  double getYRadius() const;

  /** \details
    Returns x vector of elliptical geometry data.

    \returns Returns OdGeVector3d object with x vector of elliptical geometry data.
  */
  OdGeVector3d getXVector() const;

  /** \details
    Returns y vector of elliptical geometry data.

    \returns Returns OdGeVector3d object with y vector of elliptical geometry data.
  */
  OdGeVector3d getYVector() const;

  /** \details
    Returns array with colors.

    \returns Returns OdArray objects with colors.
  */
  OdArray<OdUInt32> getColors() const;

  /** \details
    Returns array with vertexes.

    \returns Returns OdArray objects with vertexes.
  */
  OdGePoint3dArray getVertexes() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGeometryEllipticalShape object pointers.
*/
typedef OdSmartPtr<OdNwGeometryEllipticalShape> OdNwGeometryEllipticalShapePtr;

#endif //__TNW_GEOMETRYELLIPTICALSHAPE_H__
