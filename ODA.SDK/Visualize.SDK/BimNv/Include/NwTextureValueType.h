/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef TNW_TEXTURE_VALUE_TYPE_H_
#define TNW_TEXTURE_VALUE_TYPE_H_

/** \details
  bit masks for type of getting value from texture

  <group TNW_Namespaces>
*/
namespace NwTextureValueType
{
  enum Enum
  {
    /**OdString value with path to pattern texture*/
    pattern_path           = 0,
    /**double value with real world scale x of pattern texture*/
    pattern_scale_x_value  = 1,
    /**unit value for real world scale x of pattern texture*/
    pattern_scale_x_unit   = 2,
    /**double value with real world scale y of pattern texture*/
    pattern_scale_y_value  = 3,
    /**unit value for real world scale y of pattern texture*/
    pattern_scale_y_unit   = 4,
    /**double value with real world offset x of pattern texture*/
    pattern_offset_x_value = 5,
    /**unit value for real world offset x of pattern texture*/
    pattern_offset_x_unit  = 6,
    /**double value with real world offset y of pattern texture*/
    pattern_offset_y_value = 7,
    /**unit value for real world offset y of pattern texture*/
    pattern_offset_y_unit  = 8,
    /**raster image of pattern texture*/
    pattern_raster_image  = 9,
    /**OdString value with path to diffuse texture*/
    diffuse_path           = 10,
    /**double value with real world scale x of diffuse texture*/
    diffuse_scale_x_value  = 11,
    /**unit value for real world scale x of diffuse texture*/
    diffuse_scale_x_unit   = 12,
    /**double value with real world scale y of diffuse texture*/
    diffuse_scale_y_value  = 13,
    /**unit value for real world scale y of diffuse texture*/
    diffuse_scale_y_unit   = 14,
    /**double value with real world offset x of diffuse texture*/
    diffuse_offset_x_value = 15,
    /**unit value for real world offset x of diffuse texture*/
    diffuse_offset_x_unit  = 16,
    /**double value with real world offset y of diffuse texture*/
    diffuse_offset_y_value = 17,
    /**unit value for real world offset y of diffuse texture*/
    diffuse_offset_y_unit  = 18,
    /**raster image of diffuse texture*/
    diffuse_raster_image  = 19,
    /**OdString value with path to opacity texture*/
    opacity_path           = 20,
    /**double value with real world scale x of opacity texture*/
    opacity_scale_x_value  = 21,
    /**unit value for real world scale x of opacity texture*/
    opacity_scale_x_unit   = 22,
    /**double value with real world scale y of opacity texture*/
    opacity_scale_y_value  = 23,
    /**unit value for real world scale y of opacity texture*/
    opacity_scale_y_unit   = 24,
    /**double value with real world offset x of opacity texture*/
    opacity_offset_x_value = 25,
    /**unit value for real world offset x of opacity texture*/
    opacity_offset_x_unit  = 26,
    /**double value with real world offset y of opacity texture*/
    opacity_offset_y_value = 27,
    /**unit value for real world offset y of opacity texture*/
    opacity_offset_y_unit  = 28,
    /**raster image of opacity texture*/
    opacity_raster_image  = 29,
    /**OdString value with path to bump texture*/
    bump_path           = 30,
    /**double value with real world scale x of bump texture*/
    bump_scale_x_value  = 31,
    /**unit value for real world scale x of bump texture*/
    bump_scale_x_unit   = 32,
    /**double value with real world scale y of bump texture*/
    bump_scale_y_value  = 33,
    /**unit value for real world scale y of bump texture*/
    bump_scale_y_unit   = 34,
    /**double value with real world offset x of bump texture*/
    bump_offset_x_value = 35,
    /**unit value for real world offset x of bump texture*/
    bump_offset_x_unit  = 36,
    /**double value with real world offset y of bump texture*/
    bump_offset_y_value = 37,
    /**unit value for real world offset y of bump texture*/
    bump_offset_y_unit  = 38,
    /**raster image of bump texture*/
    bump_raster_image  = 39
  };
}
#endif  // TNW_TEXTURE_VALUE_TYPE_H_

