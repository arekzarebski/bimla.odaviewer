/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWGEOMETRYCOMPRESSSETTINGS_H__
#define __NWGEOMETRYCOMPRESSSETTINGS_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"

class OdNwColor;

/** \details
  This class represents gettings of geometry

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwGeometryCompressSettings : public OdNwObject
{

  ODRX_DECLARE_MEMBERS(OdNwGeometryCompressSettings);

  OdNwGeometryCompressSettings();

public:

  virtual ~OdNwGeometryCompressSettings();

public:

  /** \details
    Sets geometry precision
  */
  void setPrecision(double);

  /** \details
    Returns geometry precision

    \returns Return non-zero positive float if precision is reduced.
  */
  double getPrecision() const;

  /** \details
    Sets flag geometry is compressed
  */
  void setGeometryCompressed(bool);
  /** \details
    Returns geometry is compressed
  */
  bool getGeometryCompressed() const;
  /** \details
    Sets flag normal precision is reduced
  */
  void setNormalsPrecisionReduced(bool);
  /** \details
    Returns normal precision is reduced
  */
  bool getNormalsPrecisionReduced() const;
  /** \details
    Sets flag color precision is reduced
  */
  void setColorsPrecisionReduced(bool);
  /** \details
    Returns color precision is reduced
  */
  bool getColorsPrecisionReduced() const;
  /** \details
    Sets flag texture coordinates precision is reduced
  */
  void setTextureCoordinatesPecisionReduced(bool);
  /** \details
    Returns texture coordinates precision is reduced
  */
  bool getTextureCoordinatesPecisionReduced() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for GeometryCompressSettings object pointers.
*/
typedef OdSmartPtr<OdNwGeometryCompressSettings> OdNwGeometryCompressSettingsPtr;

#endif //__NWGEOMETRYCOMPRESSSETTINGS_H__
