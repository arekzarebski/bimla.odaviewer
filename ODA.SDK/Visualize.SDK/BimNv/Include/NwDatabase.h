/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_DATABASE_H__
#define __TNW_DATABASE_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#define STL_USING_MAP
#define STL_USING_LIST
#include "OdaSTL.h"
#include "NwModelUnits.h"

class OdNwHostAppServices;
class OdNwModel;
class OdNwViewpoint;
class OdNwModelItem;
class OdNwBackgroundElement;
class OdNwCommonLight;
class OdGeMatrix3d;
class OdNwSavedItem;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwModel object pointers.
*/
typedef OdSmartPtr<OdNwModel> OdNwModelPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwViewpoint object pointers.
*/
typedef OdSmartPtr<OdNwViewpoint> OdNwViewpointPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwModelItem object pointers.
*/
typedef OdSmartPtr<OdNwModelItem> OdNwModelItemPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwBackgroundElement object pointers.
*/
typedef OdSmartPtr<OdNwBackgroundElement> OdNwBackgroundElementPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwCommonLight object pointers.
*/
typedef OdSmartPtr<OdNwCommonLight> OdNwCommonLightPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwSavedItem object pointers.
*/
typedef OdSmartPtr<OdNwSavedItem> OdNwSavedItemPtr;

/** \details
  This class implements the interface for interaction with a BimNv SDK database.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwDatabase : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwDatabase);

  OdNwDatabase();

public:

  virtual ~OdNwDatabase();

  /** \details
    Returns the OdNwHostAppServices object associated with this database object.
  */
  virtual OdNwHostAppServices* appServices() const;

  /** \details
    Reads the contents of the specified StreamBuf object or file into this database object.

    \param pStreamBuf [in]  Pointer to the StreamBuf object from which the data are to be read.
    \param partialLoad [in]  Controls the partial loading of .nwd files.
    \param password [in]  Password for file.

    Throws:
    OdError if the read is unsuccessful.
  */
  virtual void readFile(OdStreamBuf* pStreamBuf,
                        bool partialLoad = true,
                        const OdString& password = OdString::kEmpty);

  /** \details
    Reads the contents of the specified file into this database object.

    \param fileName [in]  Name of the file from which the data are to be read.
    \param partialLoad [in]  Controls the partial loading of .nwd files.
    \param password [in]  Password for file.

    Throws:
    OdError if the read is unsuccessful.
  */
  virtual void readFile(const OdString& fileName,
                        bool partialLoad =  true,
                        const OdString& password = OdString::kEmpty);

  /** \details
	Writes this database object to the specific file.

	\param fileName [in]  Name of the file to be written.

	Throws:
	OdError if the write is unsuccessful.
  */
  virtual void writeFile(const OdString& fileName);

  /** \details
	Writes this database object to the specific stream.

	\param pStreamBuf [in]  Pointer to the StreamBuf object.

	Throws:
	OdError if the write is unsuccessful.
  */
  virtual void writeFile(OdStreamBuf* pStreamBuf);

  /** \details
    Returns the name of the file associated with this database object.
  */
  virtual OdString getFilename() const;

public:

  /** \details
    Sets the path to folder with textures

    \param path [in] directory's path
  */
  void setTextureFolder(const OdString& path);

public:
  /** \details
    Adds object to database.

    \param pObj [in]  Pointer to the OdNwObject which need to add to database.
    \param handle [in]  object of OdDbHandle which need to use in adding.

    \returns Returns OdNwObjectId of object which was added to database
  */
  OdNwObjectId addObject(OdNwObjectPtr pObj,
                          const OdDbHandle& handle = OdDbHandle());

  /** \details
    Return database's id of object by it handle

    \param handle [in]  object of OdDbHandle with object's handle.
    \param createIfNotFound [in]  boolean flag for object's creating if it not found.

    \returns Returns OdNwObjectId of object
  */
  OdNwObjectId getObjectId(const OdDbHandle& handle,
                            bool createIfNotFound = false);

public:

  /** \details
    Returns a smart pointer to current viewpoint of database

    \returns Returns a smart pointer to OdNwViewpoint object with current viewpoint of database. If database don't have current viewpoint, the returned smart pointer is NULL.
  */
  OdNwViewpointPtr getCurrentView() const;

  /** \details
    Returns std::list with smart pointers to database's viewpoint

    \param lSavedViews [out] list with smart pointers to viewpoint.

    \returns Return eOk if successful, or an appropriate error code if not.

    \sa
      deprecated
  */
  OdResult getSavedViews(std::list<OdNwViewpointPtr>& lSavedViews) const;

  /** \details
    Returns array with smart pointers to database's saved views elements(with viewpoints, animation and etc.)

    \param aSavedViewsElements [out] OdArray object with smart pointers to saved views elements.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getSavedViewsElements(OdArray<OdNwSavedItemPtr>& aSavedViewsElements) const;

  /** \details
    Returns a smart pointer to background element

    \returns Returns a smart pointer to OdNwBackgroundElement object with background colors of database. If database don't have background element, the returned smart pointer is NULL.
  */
  OdNwBackgroundElementPtr getBackgroundElement() const;

  /** \details
    Returns array with smart pointers to models

    \param arrModels [out] array with smart pointers to model.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if database is composite model
  */
  OdResult getModels(OdArray<OdNwModelPtr> &arrModels) const;

  /** \details
    Returns a smart pointer to root model item

    \returns Returns a smart pointer to OdNwModelItem object with root model item of database. If database don't have root model item, the returned smart pointer is NULL.

    \remarks this method is need to use only if database is not composite model
  */
  OdNwModelItemPtr getModelItemRoot() const;

  /** \details
    Returns a transformation matrix of model

    \returns Returns a OdGeMatrix3d object with transformation matrix of model.

    \remarks this method is need to use only if database is not composite model
  */
  OdGeMatrix3d getModelTransform() const;

  /** \details
    Returns a bool value of composite database's status

    \returns Return true if database contain more then one object, the returned false if not.
  */
  bool isComposite() const;

  /** \details
    Return units in which dimensions of this model are defined

    \remarks
      getUnits() returns one of the following:

    <table>
    Name                                 Value   Description
    NwModelUnits::UNITS_METERS           0x00    SI meter. Length of the path travelled by light in vacuum in 1 299,792,458th of second.
    NwModelUnits::UNITS_CENTIMETERS      0x01    1 100th of SI meter.
    NwModelUnits::UNITS_MILLIMETERS      0x02    1 1000th of an SI meter.
    NwModelUnits::UNITS_FEET             0x03    1 3rd of a yard.
    NwModelUnits::UNITS_INCHES           0x04    1 12th of a foot, 2.54 SI centimeters.
    NwModelUnits::UNITS_YARDS            0x05    Imperial yard, 0.9144 SI meters.
    NwModelUnits::UNITS_KILOMETERS       0x06    1000 SI meters
    NwModelUnits::UNITS_MILES            0x07    1760 yards
    NwModelUnits::UNITS_MICROMETERS      0x08    1 1,000,000th of an SI meter, micron
    NwModelUnits::UNITS_MILS             0x09    1 1,000th of inch
    NwModelUnits::UNITS_MICROINCHES      0x0A    1 1,000,000th of inch
    </table>
  */
  NwModelUnits::Enum getUnits() const;

  /** \details
    Returns array with smart pointers to lights

    \param arrLights [out] array with smart pointers to lights.

    \returns Return eOk if successful, or an appropriate error code if not.

    \remarks this method is need to use only if database is composite model
  */
  OdResult getLightElements(OdArray<OdNwCommonLightPtr> &arrLights) const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwDatabase object pointers.
*/
typedef OdSmartPtr<OdNwDatabase> OdNwDatabasePtr;

#endif //__TNW_DATABASE_H__
