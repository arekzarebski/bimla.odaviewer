/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_GRAPHICJSON_H__
#define __TNW_GRAPHICJSON_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"

class OdNwColor;
class OdBinaryData;

/** \details
  This class represents a background element with colors

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwGraphicJson : public OdNwObject
{

  ODRX_DECLARE_MEMBERS(OdNwGraphicJson);

  OdNwGraphicJson();

public:

  virtual ~OdNwGraphicJson();

public:
  /** \details
    Returns main json of the objects

    \returns Return OdString with json content.
  */
  OdString getJson() const;
  /** \details
    Sets main json string

    \param json [in] string with main json.
  */
  void setJson(OdString json);
  /** \details
    Sets material json string

    \param json [in] string with material json.
  */
  void setMaterialJson(OdString json);
  /** \details
    Returns material json of the objects

    \returns Return OdString with material json content.
  */
  OdString getMaterialJson() const;
  /** \details
    Sets file reference

    \param key [in] string with file's key.
    \param name [in] string with file's path.
  */
  void setFileRef(OdString key, OdString name);
  /** \details
    Returns file reference by key

    \returns Return OdString with file path is exists and empty string otherwise.
  */
  OdString getFileRef(OdString key) const;
  /** \details
    Sets temporary file

    \param key [in] string with file's key.
    \param cantent [in] OdBinaryData with file's content.
  */
  void setFileEmbedded(OdString key, const OdBinaryData& content);
  /** \details
    Returns temporary file content by key

    \returns Return OdBinaryData with temporary file content.
  */
  OdBinaryData getFileEmbedded(OdString key) const;
  /** \details
    Returns file reference keys

    \returns Return an array of strings with file reference keys.
  */
  OdArray<OdString> getFileRefKeys() const;
  /** \details
    Returns embedded file keys

    \returns Return an array of strings with embedded file keys.
  */
  OdArray<OdString> getFileEmbeddedKeys() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwGraphicJson object pointers.
*/
typedef OdSmartPtr<OdNwGraphicJson> OdNwGraphicJsonPtr;

#endif //__TNW_GRAPHICJSON_H__
