/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_BACKGROUNDELEMENT_H__
#define __TNW_BACKGROUNDELEMENT_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#include "NwCategory.h"
#include "NwBackgroundType.h"
#include "NwGraphicJson.h"

class OdNwColor;

/** \details
  This class represents a background element with colors

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwBackgroundElement : public OdNwObject
{

  ODRX_DECLARE_MEMBERS(OdNwBackgroundElement);

  OdNwBackgroundElement();

public:

  virtual ~OdNwBackgroundElement();

public:

  /** \details
    Returns flag of setting of PlainColor

    \returns Return true if PlainColor has been set.

    \sa
      deprecated
  */
  bool hasPlainColor() const;

  /** \details
    Returns flag of setting of GraduatedColor

    \returns Return true if GraduatedColor has been set.

    \sa
      deprecated
  */
  bool hasGraduatedColor() const;

  /** \details
    Returns flag of setting of HorizonColor

    \returns Return true if HorizonColor has been set.

    \sa
      deprecated
  */
  bool hasHorizonColor() const;

  /** \details
    Returns displayed background type that should be used in rendering

    \returns
      Return NwBackgroundType::Enum value with displayed background type.

    \remarks
      type must be one of the following:

    <table>
    Name                             Value     Description 
    NwBackgroundType::PLAIN          0         Plain background
    NwBackgroundType::GRADUATED      1         Graduated background
    NwBackgroundType::HORIZON        2         Horizon background
    </table>
  */
  NwBackgroundType::Enum getBackgroundType() const;

  /** \details
    Returns background plain color

    \param color [out] Object of OdNwColor with plain color.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getPlainColor(OdNwColor& color) const;

  /** \details
    Returns background graduated color

    \param top [out] Object of OdNwColor with top color.
    \param bottom [out] Object of OdNwColor with bottom color.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getGraduatedColor(OdNwColor& top, OdNwColor& bottom) const;

  /** \details
    Returns background horizon color

    \param skyColor [out] Object of OdNwColor with sky color.
    \param horizonSkyColor [out] Object of OdNwColor with horizon sky color.
    \param horizonGroundColor [out] Object of OdNwColor with horizon ground color.
    \param groundColor [out] Object of OdNwColor with ground color.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getHorizonColor(OdNwColor& skyColor, OdNwColor& horizonSkyColor, OdNwColor& horizonGroundColor, OdNwColor& groundColor) const;

  /** \details
    Returns material data for complex background

    \returns Return smart pointer to OdNwCategory object if successful, or empty smart pointer if complex background data not exists.
  */
  OdNwCategoryPtr getMaterial() const;

  /** \details
    Returns graphic json object for complex background

    \returns Return smart pointer to OdNwGraphicJsony object if successful, or empty smart pointer if object not exists.
  */
  OdNwGraphicJsonPtr getGraphicJson() const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwBackgroundElement object pointers.
*/
typedef OdSmartPtr<OdNwBackgroundElement> OdNwBackgroundElementPtr;

#endif //__TNW_BACKGROUNDELEMENT_H__
