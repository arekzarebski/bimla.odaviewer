/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODNWHOSTAPPSERVICES_H_
#define _ODNWHOSTAPPSERVICES_H_

#include "TD_PackPush.h"

#include "DbBaseHostAppServices.h"
#include "NwExport.h"

class NwDbHostAppProgressMeter : public OdDbHostAppProgressMeter
{
public:
  NwDbHostAppProgressMeter();

  // OdDbHostAppProgressMeter overridden
  virtual void start(const OdString& displayString = OdString::kEmpty);
  virtual void stop();
  virtual void meterProgress();
  virtual void setLimit(int max);

  //Controls display of this ProgressMeter.
  void disableOutput(bool disable); 
  void setPrefix(const OdString& prefix); 

protected:
  OdString m_Prefix;
  long m_MeterLimit, m_MeterCurrent, m_MeterOld;
  bool m_disableOutput;
};

// This class is the base class for platform specific operations within ODA BimNv SDK.
// Library: TNwDb

class NWDBEXPORT OdNwHostAppServices : public OdDbBaseHostAppServices
{
public:
  ODRX_DECLARE_MEMBERS(OdNwHostAppServices);

  OdNwHostAppServices();
  virtual ~OdNwHostAppServices();

  /** \details
    Returns the fully qualified path to the specified file.

    \param filename [in]  Name of the file to find.
    \param pDb [in]  Pointer to the database context.
    \param hint [in]  Hint that indicates the type of file that is required.

    \remarks
    Returns an empty string if the file is not found.

    This function is called by ODA BimNv SDK when access is needed to a
    file, such as a font file, a template file, etc.

    pDb == 0 specifies that this file search is not related to a database.

    If pDb != 0, call pDb->getFilename() to determine the path to the DWG
    file associated with the database.

    The typical search order is as follows:

    1.  The filename itself.
    2.  The current directory.
    3.  The drawing directory (for shx font, image, and xref files).
    4.  The directories listed in the "ACAD" environment variable.
    5.  The Windows fonts directory (only for TTF fonts on Windows).

    hint must be one of the following:

    <table>
    Name                    Value     Description Extension
    kDefault                0         Any file.                         any
    kFontFile               1         Can be either SHX or TTF file.    SHX or TTF
    kCompiledShapeFile      2         SHX file.                         SHX
    kTrueTypeFontFile       3         TTF file.                         TTF
    kEmbeddedImageFile      4         Image file.                       ISM
    kXRefDrawing            5         Drawing template file.            DWT
    kPatternFile            6         Pattern file (PAT)                PAT
    kTXApplication          7         ODA BimNv SDK Xtension file.      TX
    kFontMapFile            8         FontMap file                      FMP
    kUnderlayFile           9         Underlay file
    kTextureMapFile        10
    </table>

    \note
    The client application can, in fact return any non-null string, so long as
    the application can handle that string in OdDbSystemServices::createFile.
  */
  virtual OdString findFile(
    const OdString& filename,
    OdDbBaseDatabase* pDb = 0,
    FindFileHint hint = kDefault);

  /** \details
    Returns an instance of an OdDbHostAppProgressMeter.
  */
  virtual OdDbHostAppProgressMeter* newProgressMeter();

  /** \details
    Notification function called whenever ODA BimNv SDK no longer needs the specified
    ProgressMeter object.

    \param pProgressMeter [in]  Pointer to the ProgressMeter object no longer needed by ODA BimNv SDK.

    \note
    The default implementation of this function does nothing but return.
  */
  virtual void releaseProgressMeter(
    OdDbHostAppProgressMeter* pProgressMeter);

  /** \details
    Returns the static OdRxClass description object associated with the database.

    \remarks
    Returns OdNwDatabase::desc();
  */
  virtual OdRxClass* databaseClass() const;

  /** \details
    Creates an instance of an OdNwDatabase object.

    \param createDefault [in]  If and only if true, the newly created database
            will be populated with the default set of objects (all tables, ModelSpace and PaperSpace blocks
            etc.)
    \param measurement [in] Units of measurement for creating the database.

    \remarks
    Returns the newly created database.

    This HostAppServices object will be associated with the newly created database.

    measurement must be one of the following:

    <table>
    Name              Value   Description
    OdDb::kEnglish    0       English
    OdDb::kMetric     1       Metric
    </table>

  */
  virtual OdSmartPtr<class OdNwDatabase> createDatabase(bool createDefault = true) const;

  /** \details
    Loads the contents of the specified StreamBuf or .nwd file into the
    database with which this HostAppServices object is associated.

    \param pStreamBuf [in]  Pointer to the StreamBuf object from which the data are to be read.
    \param partialLoad [in]  Controls the partial loading of .dwg files.
    \param password [in]  Password for file.
    \param allowCPConversion [in]  If and only if true, allows code page conversion.

    \remarks
    Returns a SmartPointer to the database containing the contents of the file.
  */
  //virtual OdSmartPtr<OdNwDatabase> readFile(
  //  OdStreamBuf* pStreamBuf,
  //  bool allowCPConversion = false,
  //  bool partialLoad2d,
  //  const OdPassword& password = OdPassword());

  /** \details
    \param filename [in]  Path of the .nwd file to read.
  */
  virtual OdSmartPtr<OdNwDatabase> readFile(
    const OdString& filename,
    bool partialLoad = true);

  /** \details
    Returns the name of the client program.

    \remarks
    This function is typically used for populating "About" information.

    \note
    The default implementation of this function returns oddbGetLibraryInfo()->getLibName()
  */
  virtual const OdString program();

  /** \details
    Returns the name of the client product.

    \remarks
    This function is typically used for populating "About" information.

    \note
    The default implementation of this function returns program().
  */
  virtual const OdString product();

  /** \details
    Returns the name of the client company.

    \remarks
    This function is typically used for populating "About" information.

    \note
    The default implementation of this function returns oddbGetLibraryInfo()->getCompanyName().
  */
  virtual const OdString companyName();

  /** \details
    Returns the name of the client product code.

    \remarks
    This function is typically used for populating "About" information.

    prodcode() returns one of the following:

    <table>
    Name          Value   Description
    kProd_ACAD    1       Plain
    kProd_LT      2       Lite
    kProd_OEM     3       OEM
    kProd_OdDb    4       ..
    </table>

    \note
    The default implementation of this function returns kProd_OEM, and should not be overridden.
  */
  virtual ProdIdCode prodcode();

  /** \details
    Returns the release major and minor version string of the client application.

    \note
    The default implementation of this function returns oddbGetLibraryInfo()->getLibVersion().
  */
  virtual const OdString releaseMajorMinorString();

  /** \details
    Returns the release major version of the client application.

    \note
    The default implementation of this function returns TD_MAJOR_VERSION.
  */
  virtual int releaseMajorVersion();

  /** \details
    Returns the release minor version of the client application.

    \note
    The default implementation of this function returns TD_MINOR_VERSION.
  */
  virtual int releaseMinorVersion();

  /** \details
    Returns the release version string of the client application.

    \note
    The default implementation of this function returns oddbGetLibraryInfo()->getLibVersion().
  */
  virtual const OdString versionString();

  virtual OdString getAlternateFontName() const;

  /** \details
    Returns the name of the font mapping file used by the getPreferableFont function.

    \sa
    Font Handling

    \note
    The default implementation of this function does nothing but return an empty string.
    It will be fully implemented in a future release.
  */
  virtual OdString getFontMapFileName() const;

  /** \details
    Returns the preferable font name for the specified font name and type.


    \param fontName [in]  Font name.
    \param fontType [in]  Font type.

    \remarks
    This function is called as the first step in the process of resolving a font file.
    The default implementation tries to locate a font mapping file by calling
    getFontMapFileName, and substitutes the font name based on the contents of this
    file.

    fontType must be one of the following:

    <table>
    Name                    Value    Description
    kFontTypeUnknown        0        Unknown.
    kFontTypeShx            1        SHX font.
    kFontTypeTrueType       2        TrueType font.
    kFontTypeShape          3        Shape file.
    kFontTypeBig            4        BigFont file.
    </table>

    \sa
    Font Handling
  */
  virtual OdString getPreferableFont(
    const OdString& fontName,
    OdFontType fontType);

  /** \details
    Returns the font to be used when the specified font is not found.

    \param fontName [in]  Font name.
    \param fontType [in]  Font type.

    \remarks
    fontType must be one of the following:

    <table>
    Name                    Value    Description
    kFontTypeUnknown        0        Unknown.
    kFontTypeShx            1        SHX font.
    kFontTypeTrueType       2        TrueType font.
    kFontTypeShape          3        Shape file.
    kFontTypeBig            4        BigFont file.
    </table>

    The default implementation of this function calls getAlternateFontName for
    fonts that are not of type kFontTypeShape or kFontTypeBig. When they are,
    and empty string is returned.

    Client code could override this function to perform custom substitution
    for these types of fonts.

    \sa
    Font Handling
  */
  virtual OdString getSubstituteFont(
    const OdString& fontName,
    OdFontType fontType);

  /** \details
    Returns a the gsBitmapDevice associated with this HostAppServices object.

    \param pViewObj [in]  OdAbstractViewPE compatible object (OdGsView, OdDbViewport or etc.).
    \param pDb [in]  Pointer to the database context.
    \param flags [in]  Bitmap device flags.
  */
  virtual OdGsDevicePtr gsBitmapDevice(OdRxObject* pViewObj = NULL,
                                       OdDbBaseDatabase* pDb = NULL,
                                       OdUInt32 flags = 0);

  /** \details
  Returns the OdDbHostAppServices object used for reading .dwg files.
  */
  virtual OdDbBaseHostAppServices* getDbHostAppServices() const;

  void disableProgressMeterOutput(bool disable);

  /** \details
    Sets directory's path with textures.

    \param path [in] directory's path.
    \param rename_path [in] boolean flag for renaming texture directory's path.
  */
  void setTextureDirectoryPath(const OdString& path, bool rename_path);
  
  /** \details
    MtMode controls if multi-threading is used (bit-coded) See OdDb::MultiThreadedMode
  <table>
   <b>Value</b>   <b>Description</b>
   0              Multi-threaded file loading and regeneration are disabled.
   1              Multi-threaded file loading is enabled.
   2              Multi-threaded regeneration is enabled.
   3              Multi-threaded file loading and regeneration are enabled.
  </table>
  */
  virtual OdInt16 getMtMode() const;

  /** \details
    Sets MtMode controls if multi-threading is used (bit-coded) See OdDb::MultiThreadedMode

    \param nMode [in] MtMode controls.
      <table>
       <b>Value</b>   <b>Description</b>
       0              Multi-threaded file loading and regeneration are disabled.
       1              Multi-threaded file loading is enabled.
       2              Multi-threaded regeneration is enabled.
       3              Multi-threaded file loading and regeneration are enabled.
      </table>
  */
  void setMtMode(OdInt16 nMode);

protected:
  OdMutex                  m_TextureDirectoryMutex;
  OdString                 m_TextureDirectoryPath;
  NwDbHostAppProgressMeter m_progressMeter;
  OdInt16                  m_MTMode;
};

typedef OdSmartPtr<OdNwHostAppServices> OdNwHostAppServicesPtr;

#include "TD_PackPop.h"

#endif // _ODNWHOSTAPPSERVICES_H_
