/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_MODELITEM_H__
#define __TNW_MODELITEM_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"
#define STL_USING_MAP
#include "OdaSTL.h"
#include "SharedPtr.h"

class OdGePoint3d;
class OdNwModelItem;
class OdNwComponent;
class OdNwCategory;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwModelItem object pointers.
*/
typedef OdSmartPtr<OdNwModelItem> OdNwModelItemPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwComponent object pointers.
*/
typedef OdSmartPtr<OdNwComponent> OdNwComponentPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwCategory object pointers.
*/
typedef OdSmartPtr<OdNwCategory> OdNwCategoryPtr;

/** \details
  This class represents an instance node within the model hierarchy.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwModelItem : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwModelItem);

  OdNwModelItem();

public:

  virtual ~OdNwModelItem();

public:

  /** \details
    Calculates and returns low bounding box of item and its children.

    \returns Returns object of OdGePoint3d with low bounding box of item and its children.
  */
  OdGePoint3d getLowBound() const;

  /** \details
    Calculates and returns high bounding box of item and its children.

    \returns Returns object of OdGePoint3d with high bounding box of item and its children.
  */
  OdGePoint3d getHighBound() const;

  /** \details
    Returns all ancestors of this item within model hierarchy

    \param ancestors [out] OdArray with smart pointers to ancestors of model item.

    \returns Returns eOk if successful, or an appropriate error code if not.
  */
  OdResult getAncestors(OdArray<OdNwModelItemPtr>& ancestors) const;

  /** \details
    Returns all ancestors of this item and item itself within model hierarchy

    \param ancestors [out] OdArray with smart pointers to ancestors of model item and item itself.

    \returns Returns eOk if successful, or an appropriate error code if not.
  */
  OdResult getAncestorsAndSelf(OdArray<OdNwModelItemPtr>& ancestors) const;

  /** \details
    Returns children of this item within model hierarchy

    \param ancestors [out] OdArray with smart pointers to children of this item.

    \returns Returns eOk if successful, or an appropriate error code if not.
  */
  OdResult getChildren(OdArray<OdNwModelItemPtr>& children) const;

  /** \details
    Returns the display name for type of item.

    \returns Returns display name for type of item.

    \remarks display name was derrive from type in original design application
  */
  OdString getClassDisplayName() const;

  /** \details
    Returns the name for type of item.

    \returns Returns name for type of item.

    \remarks name was derrive from type in original design application
  */
  OdString getClassName() const;

  /** \details
    Returns all descendants of this item within model hierarchy

    \param descendants [out] OdArray with smart pointers to descendants of this model item.

    \returns Returns eOk if successful, or an appropriate error code if not.
  */
  OdResult getDescendants(OdArray<OdNwModelItemPtr>& descendants) const;

  /** \details
    Returns all descendants of this item and item itself within model hierarchy

    \param descendants [out] OdArray with smart pointers to descendants of this model item and item itself.

    \returns Returns eOk if successful, or an appropriate error code if not.
  */
  OdResult getDescendantsAndSelf(OdArray<OdNwModelItemPtr>& descendants) const;

  /** \details
    Returns end user supplied name for item.

    \returns Returns end user supplied name for item.

    \remarks DisplayName maybe empty
  */
  OdString getDisplayName() const;

  /** \details
    Returns the status of the having of geometry in the model item.

    \returns Returns true if model item have geometry, return false if not.
  */
  bool hasGeometry() const;

  /** \details
    Returns a smart pointer to geometry component for this item

    \returns Returns a smart pointer to OdNwComponent object with geometry component for this item. If item don't have geometry, the returned smart pointer is NULL.
  */
  OdNwComponentPtr getGeometryComponent() const;

  /** \details
    Returns guid for the instance data refered to by this item.

    \returns Returns object of OdGUID with guid for the instance data refered to by this item.
  */
  OdGUID getInstanceGuid() const;

  /** \details
    Returns a smart pointer to parent model item within model hierarchy

    \returns Returns a smart pointer to OdNwModelItem object with parent model item. If item is root of hierarchy, the returned smart pointer is NULL.
  */
  OdNwModelItemPtr getParent() const;

  /** \details
    Returns properties of this item divided into multiple categories

    \param mCategories [out] std::map with smart pointers to categories like value and string description like key.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getPropertyCategories(std::map<OdString, OdNwCategoryPtr>& mCategories) const;
};

#endif //__TNW_MODELITEM_H__
