/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_SAVEDITEM_H__
#define __TNW_SAVEDITEM_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"

class OdNwComment;

/** \details
  This class represents a base class for saved items.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwSavedItem : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwSavedItem);

  OdNwSavedItem();

public:

  virtual ~OdNwSavedItem();

public:

  /** \details
    Comments about the item .

    \returns Returns array with OdNwComment's objects with comments about item.
  */
  OdArray<OdNwComment> getCommentCollection() const;

  /** \details
    Name for item as displayed in the GUI 

    \returns Returns OdString value with displayed name.
  */
  OdString getDisplayName() const;

  /** \details
    Globally Unique Id for item

    \returns OdGUID value.
  */
  OdGUID getGuID() const;

  /** \details
    True if this item is a GroupItem 

    \returns boolean flag with group status.
  */
  bool isGroup() const;

  /** \details
    Parent of this item. 

    \returns pointer to savd item parent of this item, or Null if item has no parent.
  */
  OdNwSavedItem* getParent() const;
};

typedef OdSmartPtr<OdNwSavedItem> OdNwSavedItemPtr;

#endif //__TNW_SAVEDITEM_H__
