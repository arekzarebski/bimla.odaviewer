/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_VARIANT_H__
#define __TNW_VARIANT_H__

#include "RxVariant.h"
#include "NwExport.h"

class OdNwVariant;
class OdNwColor;
class OdGeMatrix3d;

/** \details
  A class that implements an extension of the <exref target="https://docs.opendesign.com/tkernel/OdVariant.html">OdVariant</exref> 
  class that can be used as an universal storage of typified data.
  Corresponding C++ library: Od_Nw
  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwVariant : public OdVariant
{
public:

  /** \details
  Creates a factory (if it was not created previously) for variant objects of a specified data type.
  \param type [in] A data type of objects that are created by the factory.
  \returns Returns a raw pointer to a factory object.
  */
  static const OdVariant::TypeFactory* typeFactory(int type);

  /** \details
  Data types for Nw variant objects.
  <group OdNw_Types>
  */
  typedef enum 
  {
    kColor = OdVariant::kNextType, // A color data type.
    kMatrix3d,                     // A matrix3d data type.
    kNextType                      // A system data type (for internal use only).
  } Type;


  /** \details
  Creates an empty Nw variant object. 
  */
  OdNwVariant();
  
  /** \details
  Creates a new Nw variant object that is a copy of another variant object represented with the OdVariant class instance. 
  \param value [in] An OdVariant object that is copied to a created Nw variant object.
  */
  OdNwVariant(const OdVariant& value);
  
  /** \details
  Creates a new Nw variant object that is a copy of another Tf variant object (copy constructor). 
  \param value [in] An OdNwVariant object that is copied to a created Nw variant object.
  */
  OdNwVariant(const OdNwVariant& value);

  /** \details
  Creates a new Nw variant object with a specified OdNwColor.
  \param value [in] A OdNwColor value.
  */
  OdNwVariant(const OdNwColor& value);

  /** \details
  Creates a new Nw variant object with a specified OdGeMatrix3d.
  \param value [in] A OdGeMatrix3d value.
  */
  OdNwVariant(const OdGeMatrix3d& value);

  /** \details
  The assignment operator for two OdNwVariant objects.
  \param value [in] A right operand of the assignment operation.
  \returns Returns this variant object after the assignment operation.
  */
  OdNwVariant& operator =(const OdNwVariant& value);

  /** \details
  Destroys the variant object.
  */
  virtual ~OdNwVariant();
  
  /** \details
  Retrieves the current color of the variant object.
  \returns Returns the current color of the variant object.
  */
  const OdNwColor& getColor() const;
  
  /** \details
  Retrieves the current matrix3d of the variant object.
  \returns Returns the current matrix3d of the variant object.
  */
  const OdGeMatrix3d& getMatrix3d() const;

  /** \details
  Sets a new color for the variant object.
  \param value [in] A new color value.
  \returns Returns the reference to the modified variant object.
  */
  OdNwVariant& setColor(const OdNwColor& value);

  /** \details
  Sets a new matrix3d for the variant object.
  \param value [in] A new matrix3d value.
  \returns Returns the reference to the modified variant object.
  */
  OdNwVariant& setMatrix3d(const OdGeMatrix3d& value);

  void setVarType(int newType, int& type, void* data);
};

/** \details
The comparison operator for two Nw variant data types.
\param lhs [in] A left-hand operand for the comparison operation.
\param rhs [in] A right-hand operand for the comparison operation.
\returns Returns true if the specified data types can be cast to each other; otherwise returns false.
*/
inline bool operator==(OdNwVariant::Type lhs, OdVariant::Type rhs)
{
  return lhs == static_cast<OdNwVariant::Type>(rhs);
}

/** \details
The non-equality operator for two Nw variant data types.
\param lhs [in] A left-hand operand.
\param rhs [in] A right-hand operand.
\returns Returns true if the specified data types are not equal; otherwise returns false.
*/
inline bool operator!=(OdNwVariant::Type lhs, OdVariant::Type rhs)
{
  return !(lhs == rhs);
}

/** \details
The comparison operator for two OdVariant data types.
\param lhs [in] A left-hand operand for the comparison operation.
\param rhs [in] A right-hand operand for the comparison operation.
\returns Returns true if the specified data types are equal; otherwise returns false.
*/
inline bool operator==(OdVariant::Type lhs, OdNwVariant::Type rhs)
{
  return rhs == lhs;
}

/** \details
The non-equality operator for two OdVariant data types.
\param lhs [in] A left-hand operand.
\param rhs [in] A right-hand operand.
\returns Returns true if the specified data types are not equal; otherwise returns false.
*/
inline bool operator!=(OdVariant::Type lhs, OdNwVariant::Type rhs)
{
  return rhs != lhs;
}

#endif //__TNW_VARIANT_H__
