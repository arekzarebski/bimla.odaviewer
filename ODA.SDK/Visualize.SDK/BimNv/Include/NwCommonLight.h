/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_COMMONLIGHT_H__
#define __TNW_COMMONLIGHT_H__

#include "NwCategory.h"
#include "NwLightValueType.h"

class OdNwColor;
class OdGeVector3d;
class OdGePoint3d;
class OdGeMatrix3d;
class OdNwVariant;

/** \details
  This class represents common light of model.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwCommonLight : public OdNwCategory
{
  ODRX_DECLARE_MEMBERS(OdNwCommonLight);

  OdNwCommonLight();

public:

  virtual ~OdNwCommonLight();

public:
  /** \details
    Returns status of enabling of light

    \returns Return bool value of light's enabling.
  */
  bool isLightOn() const;

  /** \details
    Returns ambient color of this light

    \returns Return object of OdNwColor with ambient color of this light.
  */
  OdNwColor getAmbient() const;

  /** \details
    Returns diffuse color of this light

    \returns Return object of OdNwColor with diffuse color of this light.

    \remarks it is main color
  */
  OdNwColor getDiffuse() const;

  /** \details
    Returns specular color of this light

    \returns Return object of OdNwColor with specular color of this light.

    \remarks it is main color
  */
  OdNwColor getSpecular() const;

  /** \details
    Returns variant param value depending from value of paramter's type

    \param
      type [in] param with type NwLightValueType::Enum.

    \returns
      Return OdNwVariant value depending from value of type.

    \remarks
      type must be one of the following:

    <table>
    Name                                         Value     Description 
    NwLightValueType::light_filter_color          0         OdNwColor value with filter color of common light
    NwLightValueType::light_tint_color            1         OdNwColor value with tint color of common light
    NwLightValueType::light_rgb_color             2         OdNwColor value with rgb light color of common light
    NwLightValueType::light_shadow_color          3         OdNwColor value with shadow color of common light
    NwLightValueType::light_intensity_value       4         double value with intensity of common light
    NwLightValueType::light_position_is_targeted  5         bool value with status of targeted of position light
    NwLightValueType::light_spot_web_matrix       6         matrix value with web rotation, scale and translate of spot light
    NwLightValueType::light_ambient_only_flag     7         bool value with status of ambient only of common light
    NwLightValueType::light_use_tint_flag         8         bool value with status of tint's using of common light
    NwLightValueType::light_shadow_on_flag        9         bool value with status of shadow's enabling of common light
    </table>
  */
  OdNwVariant getValue(NwLightValueType::Enum val_type) const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwCommonLight object pointers.
*/
typedef OdSmartPtr<OdNwCommonLight> OdNwCommonLightPtr;

#endif //__TNW_COMMONLIGHT_H__
