/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef _NWVALUETYPES_H_INCLUDED_
#define _NWVALUETYPES_H_INCLUDED_

#include "RxValue.h"
#include "NwExport.h"
#include "NwModelUnits.h"
#include "NwCameraMode.h"
#include "NwLightType.h"
#include "NwViewType.h"
#include "NwModeType.h"
#include "NwModelType.h"
#include "NwPropertyValueType.h"
#include "NwBackgroundType.h"
#include "RxAttribute.h"

class OdNwObject;
typedef OdSmartPtr<OdNwObject> OdNwObjectPtr; 
class OdNwViewpoint;
typedef OdSmartPtr<OdNwViewpoint> OdNwViewpointPtr;
class OdNwBackgroundElement;
typedef OdSmartPtr<OdNwBackgroundElement> OdNwBackgroundElementPtr;
class OdNwModelItem;
typedef OdSmartPtr<OdNwModelItem> OdNwModelItemPtr;
class OdNwCommonLight;
typedef OdSmartPtr<OdNwCommonLight> OdNwCommonLightPtr;
class OdNwModel;
typedef OdSmartPtr<OdNwModel> OdNwModelPtr;
class OdNwColor;
class OdNwCategory;
typedef OdSmartPtr<OdNwCategory> OdNwCategoryPtr;
class OdNwProperty;
typedef OdSmartPtr<OdNwProperty> OdNwPropertyPtr;
class OdNwComponent;
typedef OdSmartPtr<OdNwComponent> OdNwComponentPtr;
class OdGUID;
class OdNwFragment;
typedef OdSmartPtr<OdNwFragment> OdNwFragmentPtr;
class OdNwMaterial;
typedef OdSmartPtr<OdNwMaterial> OdNwMaterialPtr;
class OdNwDistantLight;
typedef OdSmartPtr<OdNwDistantLight> OdNwDistantLightPtr;
class OdNwGeometryEllipticalShape;
typedef OdSmartPtr<OdNwGeometryEllipticalShape> OdNwGeometryEllipticalShapePtr;
class OdNwGeometryLineSet;
typedef OdSmartPtr<OdNwGeometryLineSet> OdNwGeometryLineSetPtr;
class OdNwGeometryMesh;
typedef OdSmartPtr<OdNwGeometryMesh> OdNwGeometryMeshPtr;
class OdNwGeometryPointSet;
typedef OdSmartPtr<OdNwGeometryPointSet> OdNwGeometryPointSetPtr;
class OdNwGeometryText;
typedef OdSmartPtr<OdNwGeometryText> OdNwGeometryTextPtr;
class OdNwGeometryTube;
typedef OdSmartPtr<OdNwGeometryTube> OdNwGeometryTubePtr;
class OdNwTextFontInfo;

ODRX_DECLARE_VALUE_TYPE(OdNwObjectPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwViewpointPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwBackgroundElementPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwModelItemPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(NwModelUnits::Enum, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwCommonLightPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwModelPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(NwCameraMode::Enum, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(NwLightType::Enum, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(NwModeType::Enum, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(NwViewType::Enum, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwColor, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwCategoryPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(NwModelType::Enum, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwPropertyPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwComponentPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdGUID, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwFragmentPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwMaterialPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwDistantLightPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwGeometryEllipticalShapePtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwGeometryLineSetPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwGeometryMeshPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwGeometryPointSetPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwGeometryTextPtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwGeometryTubePtr, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(OdNwTextFontInfo, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(tm, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(NwPropertyValueType::Enum, NWDBEXPORT);
ODRX_DECLARE_VALUE_TYPE(NwBackgroundType::Enum, NWDBEXPORT);

class NWDBEXPORT OdNwDisplayAsAttribute : public OdRxDisplayAsAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdNwDisplayAsAttribute);
  static OdRxAttributePtr createObject(bool setDisplayName);
  virtual OdString getDisplayValue(OdRxValue& value);
  OdNwDisplayAsAttribute();

protected:
  bool m_bSetDisplayName;
};

class NWDBEXPORT OdNwHierarchyLevelAttribute : public OdRxHierarchyLevelAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdNwHierarchyLevelAttribute);
  virtual OdString value(OdRxValue& value);
};

#endif
