/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_COMPONENT_H__
#define __TNW_COMPONENT_H__

#include "NwObject.h"
#include "RxSystemServices.h"
#include "NwExport.h"

class OdNwFragment;
class OdNwMaterial;
class OdGePoint3d;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwFragment object pointers.
*/
typedef OdSmartPtr<OdNwFragment> OdNwFragmentPtr;

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwMaterial object pointers.
*/
typedef OdSmartPtr<OdNwMaterial> OdNwMaterialPtr;

/** \details
  This class represents geometry component within the model hierarchy.

  <group OdNw_Classes>
*/
class NWDBEXPORT OdNwComponent : public OdNwObject
{
  ODRX_DECLARE_MEMBERS(OdNwComponent);

  OdNwComponent();

public:

  virtual ~OdNwComponent();

  /** \details
    Returns array with fragments of this geometry component

    \param aFragments [out] OdArray with smart pointers to fragments of geometry component.

    \returns Returns eOk if successful, or an appropriate error code if not.
  */
  OdResult getFragments(OdArray<OdNwFragmentPtr>& aFragments) const;

  /** \details
    Returns the status of the hiddening of component in the model hierarchy.

    \returns Returns true if component is hiden, return false if not.
  */
  bool isHidden() const;

  /** \details
    Returns a smart pointer to material of this component

    \returns Returns a smart pointer to OdNwMaterial object with material of this component. If component don't have material, the returned smart pointer is NULL.
  */
  OdNwMaterialPtr getMaterial() const;

  /** \details
    Returns current (visible) transparency for this geometry component

    \param val [out] Float object with transparency.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getTransparency(float& val) const;

  /** \details
    Returns low bound of this geometry component in world space

    \param lb [out] Object of OdGePoint3d with low bound of this geometry component.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getLowBound(OdGePoint3d& lb) const;

  /** \details
    Returns high bound of this geometry component in world space

    \param hb [out] Object of OdGePoint3d with high bound of this geometry component.

    \returns Return eOk if successful, or an appropriate error code if not.
  */
  OdResult getHighBound(OdGePoint3d& hb) const;

  /** \details
    Returns the number of fragments this geometry is divided into.

    \returns Returns OdUIn32 object with number of fragments.
  */
  OdUInt32 getFragmentCount() const;

  /** \details
    Returns a smart pointer to fragment with geometry of this component by index

    \param index [in] OdUInt32 object with value of fragment's index in componet's fragments array

    \returns Returns a smart pointer to OdNwFragment object with fragment with geometry of this component by index. If there's not fragment with this index, the returned smart pointer is NULL.
  */
  OdNwFragmentPtr findGeometry(OdUInt32 index) const;
};

/** \details
  This template class is a specialization of the OdSmartPtr class for OdNwComponent object pointers.
*/
typedef OdSmartPtr<OdNwComponent> OdNwComponentPtr;

#endif //__TNW_COMPONENT_H__
