/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWFILELOADER_H__
#define __NWFILELOADER_H__

#include "NwExport.h"
#include "NwStreamSplitter.h"
#include "NwStreamType.h"
#include "NwStreamLoader.h"
#include "OdList.h"
#include "NwTextureBuf.h"
#include "NwModelUnits.h"

class OdNwDatabaseImpl;
class OdNwViewpoint;
typedef OdSmartPtr<OdNwViewpoint> OdNwViewpointPtr;
class OdNwBackgroundElement;
typedef OdSmartPtr<OdNwBackgroundElement> OdNwBackgroundElementPtr;
class OdNwModel;
typedef OdSmartPtr<OdNwModel> OdNwModelPtr;
class OdNwModelItem;
typedef OdSmartPtr<OdNwModelItem> OdNwModelItemPtr;
class OdNwCommonLight;
typedef OdSmartPtr<OdNwCommonLight> OdNwCommonLightPtr;
class OdNwSavedItem;
typedef OdSmartPtr<OdNwSavedItem> OdNwSavedItemPtr;

class OdNwFileLoader
{
public:
  OdNwFileLoader();
  virtual ~OdNwFileLoader();

  bool Load(const OdStreamBufPtr& stream, OdNwDatabaseImpl* pDbImpl);

  void setTextureFolder(const OdString& path);

  OdInt32 getVersion() const;
  bool isLoaded() const;
  OdNwStreamLoaderPtr getLoader(const NwStreamType::Enum type);
  OdResult getCurrentView(OdNwViewpointPtr& pCurView);
  OdResult getSavedViewsList(std::list<OdNwViewpointPtr>& SavedViews);//deprecated
  OdResult getSavedViewsElements(OdArray<OdNwSavedItemPtr>& SavedItems);
  OdResult getBackgroundElement(OdNwBackgroundElementPtr& pBackgroundElement);
  OdResult getModels(OdArray<OdNwModelPtr>& aModels);
  OdResult getModelItemRoot(OdNwModelItemPtr& pModelItemRoot);
  OdResult getModelUnits(NwModelUnits::Enum& modelUnits);
  OdResult getLightElements(OdArray<OdNwCommonLightPtr>& aLightElements);
  bool isComposite() const;
  void dump_StreamNames(const OdString& path);
  void reset();

  void getCachedStreams(OdArray<NwSubStreamDef>& streams);

private:
  bool                m_bLoaded;
  OdNwStreamSplitter  m_oSplitter;
  OdNwLoadersMap      m_aLoaders;
  bool                m_bIsCompositeObject;
  OdString            m_sTextureFolderPath;
  OdTextureMap        m_mTexture;
  NwModelUnits::Enum  m_ModelUnits;
};

#endif // __NWFILELOADER_H__
