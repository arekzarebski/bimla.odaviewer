  /////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2018, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_OBJECT_READER_MANAGER_H__
#define __NW_OBJECT_READER_MANAGER_H__

#include "TD_PackPush.h"
#include "OdStreamBuf.h"
#include "NwObject.h"
#include "NwObjectType.h"

class OdBinaryData;

class OdNwObjectReaderManager
{
public:
  OdNwObjectReaderManager(OdStreamBufPtr stream);
  virtual ~OdNwObjectReaderManager() {};

  OdNwObjectPtr readObject(OdResult& res, NwObjectType::Enum verifyType = NwObjectType::Unknown);
  OdString readString(OdUInt32 align = 4);
  OdUInt32 readUInt32();
  OdBinaryData readBinary(const OdUInt32 size);

protected:
  OdUInt32 m_nObjectIndex;
  OdStreamBufPtr m_pStream;
};

/** \details
    This template class is a specialization of the OdSharedPtr class for OdNwObjectReaderManager object pointers.
*/
typedef OdSharedPtr<OdNwObjectReaderManager> OdNwObjectReaderManagerPtr;

#include "TD_PackPop.h"

#endif // __NW_OBJECT_READER_MANAGER_H__
