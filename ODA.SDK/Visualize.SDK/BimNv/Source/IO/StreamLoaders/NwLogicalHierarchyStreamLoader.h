/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_LOAGICALHIERARCHYSTREAMLOADER_H__
#define __NW_LOAGICALHIERARCHYSTREAMLOADER_H__

#include "NwPropertiesStreamLoaderBase.h"
#include "NwLogicalHierarchyTreeHelper.h"

#include "TD_PackPush.h"

typedef std::pair<OdUInt64, OdUInt32> OdPartitionPropsInsertionsPair;
typedef std::map<OdUInt64, OdUInt32> OdPartitionPropsInsertionsMap;
typedef std::pair<OdUInt32, OdUInt64> OdNodesPair;
typedef std::map<OdUInt32, OdUInt64> OdNodesMap;

class OdNwPartitionPropsStreamLoader;
class OdNwPropertyOverrideElementStreamLoader;
class OdNwGuidStoreStreamLoader;
class OdNwPartitionStreamLoader;
class OdNwHyperlinksOverrideElementStreamLoader;
class OdNwFragmentsStreamLoader;

class OdNwCategory;
typedef OdSmartPtr<OdNwCategory> OdNwCategoryPtr;
class OdNwModelItem;
typedef OdSmartPtr<OdNwModelItem> OdNwModelItemPtr;
typedef std::map<OdUInt32, OdNwModelItemPtr> OdNwModelItemMap;

class OdNwLogicalHierarchyStreamLoader : public OdNwPropertiesStreamLoaderBase
{
public:
  OdNwLogicalHierarchyStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual ~OdNwLogicalHierarchyStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdNwDatabaseImpl* pDbImpl, OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  OdResult parseStream() override;

  OdNwModelItemPtr getRootNode() const;
  const OdNwModelItemMap& getNodes() const;

  virtual void setLinkLoaderGetter(const std::function<OdNwStreamLoaderPtr(NwStreamType::Enum)>& getLoader) override;

public:
  OdNwModelItemPtr readModelItem(OdInt64 offset, OdInt64 size, OdStreamBufPtr pStream);

protected:
  virtual OdResult parsePartitionPropsStream();
  virtual OdResult parseLogicalHierarchyTree();
  OdNwModelItemPtr readNextNode();

protected:
  virtual OdNwCategoryPtr getCategoryFromPartitionProps();
  OdNwCategoryPtr getCategoryFromHyperlinksOverrideElementByComponentIndex(OdUInt32 componentIndex);
  virtual OdResult processNode(OdNwModelItemPtr pNode);
  virtual OdResult setGeometryToNode(OdNwModelItemPtr pNode);
  bool insertInNodesMap(OdUInt32 key, OdNwModelItemPtr pNode);
  OdNwModelItemPtr findNodesMap(OdUInt32 key) const;
  bool addToRootNode(OdNwModelItemPtr pNode, OdNwModelItemPtr pRoot) const;
  OdResult copyNode(OdNwModelItemPtr pOriginalNode, OdNwModelItemPtr pRoot);
  OdNwModelItemPtr createCopyNode(OdNwModelItemPtr pOriginalNode, OdNwModelItemPtr pRoot, OdUInt32 newId) const;

protected:
  std::function<OdNwStreamLoaderPtr(NwStreamType::Enum)> m_getLoaderCallBack;

  OdUInt32 m_nextPartitionPropIndex;
  OdPartitionPropsInsertionsMap m_partitionPropsInsertionsMapping;
  OdNodesMap m_nodesMapping;
  bool m_bRootNodeReturned;
  OdNwLogicalHierarchyTreeHelper m_TreeHelper;

  OdNwModelItemPtr m_pRootNode;
  OdNwModelItemMap m_mNodes;

  std::weak_ptr<OdNwFragmentsStreamLoader> m_pFramentsStreamLoader;
  std::weak_ptr<OdNwPartitionPropsStreamLoader> m_pPartitionPropsLoader;
  std::weak_ptr<OdNwPartitionStreamLoader> m_pPartitionLoader;
  std::weak_ptr<OdNwPropertyOverrideElementStreamLoader> m_pPropertyOverrideElementLoader;
  std::weak_ptr<OdNwGuidStoreStreamLoader> m_pGuidStoreLoader;
  std::weak_ptr<OdNwHyperlinksOverrideElementStreamLoader> m_pHyperlinksOverrideElementLoader;
};

typedef std::shared_ptr<OdNwLogicalHierarchyStreamLoader> OdNwLogicalHierarchyStreamLoaderPtr;
typedef std::weak_ptr<OdNwLogicalHierarchyStreamLoader> OdNwLogicalHierarchyStreamLoaderWeakPtr;

#include "TD_PackPop.h"

#endif // __NW_LOAGICALHIERARCHYSTREAMLOADER_H__
