/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_SAVEDVIEWSELEMENTSTREAMLOADER_H__
#define __NW_SAVEDVIEWSELEMENTSTREAMLOADER_H__

#include "NwCurrentViewElementStreamLoaderBase.h"
#include "OdList.h"
#include "NwComment.h"
#include "NwFragmentType.h"
#include "TD_PackPush.h"

class OdNwViewpoint;
typedef OdSmartPtr<OdNwViewpoint> OdNwViewpointPtr;
typedef std::list<OdNwViewpointPtr> OdViewPtrList;
typedef std::map<OdString, OdUInt32> OdNameIndicesMap;
typedef std::pair<OdString, OdUInt32> OdNameIndicesPair;
class OdNwSavedItem;
typedef OdSmartPtr<OdNwSavedItem> OdNwSavedItemPtr;
typedef OdArray<OdNwSavedItemPtr> OdNwSavedItemArray;

class OdNwSavedViewsElementStreamLoader : public OdNwCurrentViewElementStreamLoaderBase
{
public:
  OdNwSavedViewsElementStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual ~OdNwSavedViewsElementStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual OdResult parseStream() override;

public:
  OdViewPtrList getSavedViewList();//deprecated
  const OdNwSavedItemArray& getSavedViewsElements();

private:
  //RedlineList
  void readRedlineList();

  //ElementRecordList
  void readContentImplicit_ElementRecordList();

  //ElementRecord
  void readContentsImplicit_ElementRecord();
  void readImplicit_ElementRecord();
  void readStub_ElementRecord();

  void readOwnSchemaInstanceVector();

  //object/node parse
  bool readCheckedObject(NwFragmentType::Enum type);
  bool readNode();
  OdUInt32 getObjectHeader(OdUInt32& index, NwFragmentType::Enum& type);
  bool parseObjectDef(OdUInt32 index, NwFragmentType::Enum type);

  void readPathLinks();

  //fragments/nodes
  void readAppereance();
  void readMaterial();
  void readCheckedNode();

  //coment
  void readCommentList(OdNwSavedItemPtr pSavedItem);
  OdNwComment readComment();

  //SavedItem
  OdNwSavedItemPtr readSavedViewpointAnimationCut();
  OdResult readSavedItem(OdNwSavedItemPtr pItem);
  OdNwSavedItemPtr readSavedViewpoint();
  OdNwSavedItemPtr readEntity();
  OdNwSavedItemPtr readSavedAnimation();
  OdNwSavedItemPtr readFolderItem();

private:
  OdNwSavedItemArray m_aSavedItems;
  OdUInt32 m_nextIndex;
};

typedef std::shared_ptr<OdNwSavedViewsElementStreamLoader> OdNwSavedViewsElementStreamLoaderPtr;
typedef std::weak_ptr<OdNwSavedViewsElementStreamLoader> OdNwSavedViewsElementStreamLoaderWeakPtr;

#include "TD_PackPop.h"

#endif // __NW_SAVEDVIEWSELEMENTSTREAMLOADER_H__
