/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_PROPERTIESSTREAMLOADERBASE_H__
#define __NW_PROPERTIESSTREAMLOADERBASE_H__

#include "NwStreamLoader.h"
#include "NwFormatVersion.h"
#include "NwModelUnits.h"
#include "NwPropertyBase.h"

#include "TD_PackPush.h"

class OdGeQuaternion;
class OdNwPropertyImpl;
typedef OdSmartPtr<OdNwPropertyImpl> OdNwPropertyImplPtr;
class OdNwCategoryImpl;
typedef OdSmartPtr<OdNwCategoryImpl> OdNwCategoryImplPtr;
class OdNwTextureBuf;
typedef OdSharedPtr<OdNwTextureBuf> OdNwTextureBufPtr;
typedef std::map<OdString, OdNwTextureBufPtr> OdTextureMap;
class OdNwCategory;
typedef OdSmartPtr<OdNwCategory> OdNwCategoryPtr;
class OdNwCommonLight;
typedef OdSmartPtr<OdNwCommonLight> OdNwCommonLightPtr;
typedef OdArray<OdNwCommonLightPtr> OdLightsArray;

class OdNwPropertiesStreamLoaderBase : public OdNwStreamLoader
{
public:
  OdNwPropertiesStreamLoaderBase(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion, OdNmStreamAlignment::Enum alignmentMode = OdNmStreamAlignment::EightBytes);
  virtual ~OdNwPropertiesStreamLoaderBase();

public:
  NwModelUnits::Enum getModelUnits() const;

protected:
  void readSchema(OdNwPropertyImplPtr& pProperty);
  OdUInt32 readAttribute(OdNwCategoryImplPtr pCategory, bool reusage = false);
  OdUInt32 readHyperlinks(OdNwCategoryImplPtr pCategory, bool reusage = false);
  OdUInt32 readInstanceValue(OdNwCategoryImplPtr pCategory, bool reusage = false);
  void readArbitraryPlane(OdNwCategoryImplPtr pCategory);
  OdUInt32 readTextboxes(OdNwCategoryImplPtr pCategory, bool reusage = false);
  OdUInt32 readFileInfo(OdUInt32 nextIndex, OdLightsArray* lights = NULL);
  OdUInt32 readLight(OdNwCategoryImplPtr pCategory);
  template <typename T>
  void readReusableFragment(OdSmartPtr<T>& pProp, OdUInt32 index)
  {
    if (pProp->isKindOf(OdNwPropertyBase::desc()))
    {
      OdNwPropertyBasePtr pPropBase = pProp;
      readReusableFragment(pPropBase, index);
      if (pPropBase->isKindOf(T::desc()))
        pProp = pPropBase;
    }
  }
  void readNodeType(OdNwPropertyBasePtr pProp);
  OdUInt32 readTranslationDouble(OdNwCategoryImplPtr pCategory, bool reusage = false);
  OdUInt32 readRotationDouble(OdNwCategoryImplPtr pCategory, bool reusage = false);
  OdUInt32 readTransformationDouble(OdNwCategoryImplPtr pCategory, bool reusage = false);
  OdUInt32 readCategoryCommons(OdNwCategoryImplPtr category, bool reusage, OdString defName, OdString defDispName, OdString& name);
  OdUInt32 readMaterialDouble(OdNwCategoryImplPtr pCategory, bool reusage = false);
  void readMaterialPresenter(OdNwCategoryImplPtr pCategory);
  OdUInt32 readGroup(OdNwCategoryImplPtr pCategory, bool reusage = false);
  OdUInt32 readKeyValue(OdNwPropertyImplPtr& property, bool reusage = false);
  OdUInt32 readAutodeskMaterial(OdNwCategoryImplPtr pCategory, bool reusage = false);
  OdUInt32 readAutodeskMaterialPropertiesSet(OdNwCategoryImplPtr pCategory, bool reusage = false, OdString propNamePrefix = OD_T(""), bool extractName = false);
  void readAutodeskMaterialPropertiesSetTxt(OdNwCategoryImplPtr pCategory, bool reusage = false, OdTextureMap* imagesDumpMap = NULL);
  OdUInt32 readAutodeskMaterialProperty(OdNwCategoryImplPtr pCategory, bool reusage = false, OdString propNamePrefix = OD_T(""));
  OdUInt32 readPublishOptions(OdNwCategoryImplPtr pCategory);
  OdUInt32 readXref(OdNwCategoryImplPtr pCategory, bool reusage = false);

private:
  void readReusableFragment(OdNwPropertyBasePtr& pProp, OdUInt32 index);

protected:
  void normalizeQuaternion(OdGeQuaternion& q) const;
  double getQuaternionAngle(const OdGeQuaternion& q) const;
  void rotateByQuaternion(OdGeMatrix3d& m, const OdGeQuaternion& q) const;
  void translateByVector(OdGeMatrix3d& m, const OdGeVector3d &v) const;

protected:
  tm m_UnixTimeStartEpoch;
  NwModelUnits::Enum m_ModelUnits;
  NwFormatVersion::Enum m_FormatVersion;
};

#include "TD_PackPop.h"

#endif // __NW_PROPERTIESSTREAMLOADERBASE_H__
