/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_LOAGICALHIERARCHYSTREAMLOADERMT_H__
#define __NW_LOAGICALHIERARCHYSTREAMLOADERMT_H__

#include "NwLogicalHierarchyStreamLoader.h"

#if defined(ODA_WINDOWS)
#if _MSC_VER < 1900
#define ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING
#endif
#endif // (ODA_WINDOWS)

#include <mutex>
#include <condition_variable>
#pragma push_macro("free")
#undef free
#if defined(ODA_WINDOWS)
#ifndef _TOOLKIT_IN_DLL_ 
#define __TBB_NO_IMPLICIT_LINKAGE 1
#endif //_TOOLKIT_IN_DLL_
#endif //(ODA_WINDOWS)
#include "tbb/mutex.h"
#include "tbb/atomic.h"
#include "tbb/compat/condition_variable"
#ifndef  ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING
#define TBB_PREVIEW_CONCURRENT_ORDERED_CONTAINERS 1
#include "tbb/concurrent_map.h"
#endif // ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING
#pragma pop_macro("free")

#include "TD_PackPush.h"

#ifndef  ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING
struct OdNwMTModelItem;
class OdNwCopyChildNodeTask;
#endif // ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING

class OdNwLogicalHierarchyStreamLoaderMT : public OdNwLogicalHierarchyStreamLoader
{
#ifndef  ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING
  friend class OdNwCopyChildNodeTask;
#endif // ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING

public:
  OdNwLogicalHierarchyStreamLoaderMT(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual ~OdNwLogicalHierarchyStreamLoaderMT();
  static OdNwStreamLoaderPtr createStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);

#ifndef  ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING
protected:
  OdNwCategoryPtr getCategoryFromPartitionProps() override;
  OdResult processNode(OdNwModelItemPtr pNode) override;
  OdResult setGeometryToNode(OdNwModelItemPtr pNode) override;
  OdResult parsePartitionPropsStream() override;
  OdResult parseLogicalHierarchyTree() override;

protected:
  bool insertInMTNodesMap(OdUInt32 key, OdNwModelItemPtr pNode);
  std::shared_ptr<OdNwMTModelItem> findMTNodesMap(OdUInt32 key) const;
#else
protected:
  OdResult parsePartitionPropsStream() override;
  OdNwCategoryPtr getCategoryFromPartitionProps() override;
#endif // ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING

private:
  tbb::atomic<bool> m_isParsedPartProp;
  std::shared_ptr<std::mutex> m_pPPMutex;
  std::shared_ptr<std::condition_variable> m_pPPCondVariable;
  tbb::mutex m_FragmentsMutex;
#ifndef  ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING
  tbb::concurrent_map<OdUInt32, std::shared_ptr<OdNwMTModelItem> > m_mtNodes;
#endif // ODA_LOGICAL_HIERARCHY_OLD_MT_LOADING
};
#include "TD_PackPop.h"

#endif // __NW_LOAGICALHIERARCHYSTREAMLOADERMT_H__
