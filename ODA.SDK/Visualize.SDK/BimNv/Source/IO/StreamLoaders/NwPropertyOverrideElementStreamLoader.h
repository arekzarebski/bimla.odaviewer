/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_PROPERTYOVERRIDEELEMENTSTREAMLOADER_H__
#define __NW_PROPERTYOVERRIDEELEMENTSTREAMLOADER_H__

#include "NwPropertiesStreamLoaderBase.h"
#define STL_USING_LIST
#include "OdaSTL.h"
#include "TD_PackPush.h"

class OdNwCategory;
typedef OdSmartPtr<OdNwCategory> OdNwCategoryPtr;
typedef std::list<OdNwCategoryPtr> OdCategoryList;
typedef std::pair<OdUInt32, OdCategoryList> OdComponentPropertiesPair;
typedef std::map<OdUInt32, OdCategoryList> OdComponentPropertiesMap;

class OdNwPropertyOverrideElementStreamLoader : public OdNwPropertiesStreamLoaderBase
{
public:
  OdNwPropertyOverrideElementStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual ~OdNwPropertyOverrideElementStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual OdResult parseStream() override;

  bool getCategoriesByComponentIndex(OdUInt32 componentIndex, const OdCategoryList* pCat) const;

private:
  OdNwCategoryPtr readTab(const OdString &index);

private:
  OdComponentPropertiesMap m_componentPropertiesMapping;
};

typedef std::shared_ptr<OdNwPropertyOverrideElementStreamLoader> OdNwPropertyOverrideElementStreamLoaderPtr;
typedef std::weak_ptr<OdNwPropertyOverrideElementStreamLoader> OdNwPropertyOverrideElementStreamLoaderWeakPtr;

#include "TD_PackPop.h"

#endif // __NW_PROPERTYOVERRIDEELEMENTSTREAMLOADER_H__
