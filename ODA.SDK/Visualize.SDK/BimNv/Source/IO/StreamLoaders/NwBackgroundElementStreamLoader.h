/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_BACKGROUNDSTREAMLOADER_H__
#define __NW_BACKGROUNDSTREAMLOADER_H__

#include "NwStreamLoader.h"
#include "NwFormatVersion.h"
#include "NwBackgroundElement.h"
#include "TD_PackPush.h"

class OdNwBackgroundElementStreamLoader : public OdNwStreamLoader
{
public:
  OdNwBackgroundElementStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual ~OdNwBackgroundElementStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual OdResult parseStream() override;

  OdNwBackgroundElementPtr getBackgroundElement();
  void setBackgroundElement(const OdNwBackgroundElementPtr&);

private:
  OdNwBackgroundElementPtr m_pBackgroundElement;
  NwFormatVersion::Enum    m_FormatVersion;
};

typedef std::shared_ptr<OdNwBackgroundElementStreamLoader> OdNwBackgroundElementStreamLoaderPtr;
typedef std::weak_ptr<OdNwBackgroundElementStreamLoader> OdNwBackgroundElementStreamLoaderWeakPtr;

#include "TD_PackPop.h"

#endif // __NW_BACKGROUNDSTREAMLOADER_H__
