/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_PARTITIONPROPSSTREAMLOADER_H__
#define __NW_PARTITIONPROPSSTREAMLOADER_H__

#include "NwPropertiesStreamLoaderBase.h"
#include "TD_PackPush.h"

class OdNwCategory;
typedef OdSmartPtr<OdNwCategory> OdNwCategoryPtr;
typedef OdArray<OdNwCategoryPtr> OdCategoryArray;

class OdNwPartitionPropsStreamLoader : public OdNwPropertiesStreamLoaderBase
{
public:
  OdNwPartitionPropsStreamLoader(OdStreamBufPtr streamHeader, OdStreamBufPtr streamObjects
    , OdStreamBufPtr streamTail, NwFormatVersion::Enum FormatVersion);
  virtual ~OdNwPartitionPropsStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdStreamBufPtr stream, OdStreamBufPtr streamObjects, OdStreamBufPtr streamTail, NwFormatVersion::Enum FormatVersion);
  virtual OdResult parseStream() override;

  OdNwCategoryPtr getCategoryByIndex(OdUInt32 categoryIndex) const;
  void setCategoriesCount(OdUInt32 count);

private:
  void readPartitionProps(OdNwCategoryImplPtr category);

private:
  OdUInt64 m_maxPosition;
  OdUInt32 m_nextStructIndex;
  OdInt32 m_categoriesCount;

  OdStreamBufPtr m_streamHeader;
  OdStreamBufPtr m_streamTail;

  // PartitionProps stream can has few blocks of data with start index START_INDEX.
  // Each of blocks has its own reuse mapping (for KeyValueTypes e. t. c.).
  // So it's necessary to store and use correct mapping (from base class) for each category.

  OdCategoryArray m_aCategories;
};

typedef std::shared_ptr<OdNwPartitionPropsStreamLoader> OdNwPartitionPropsStreamLoaderPtr;
typedef std::weak_ptr<OdNwPartitionPropsStreamLoader> OdNwPartitionPropsStreamLoaderWeakPtr;

#include "TD_PackPop.h"

#endif // __NW_PARTITIONPROPSSTREAMLOADER_H__
