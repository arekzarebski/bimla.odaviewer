/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_GEOMETRYSTREAMLOADER_H__
#define __NW_GEOMETRYSTREAMLOADER_H__

#include "NwStreamLoader.h"
#include "NwGeometry.h"
#include "TD_PackPush.h"

class OdNwDatabaseImpl;

struct NwGeometryObjectDef
{
  OdInt32 pos;
  OdUInt32 size;
  bool loaded;
  NwGeometryObjectDef(OdInt32 apos = 0, OdUInt32 asize = 0, bool aloaded = false) 
    : pos(apos), size(asize), loaded(aloaded) {}
};

typedef OdArray<NwGeometryObjectDef> NwGeometryObjectDefArray;

class OdNwGeometryStreamLoader : public OdNwStreamLoader
{
public:
  OdNwGeometryStreamLoader(OdStreamBufPtr stream, OdStreamBufPtr streamTable);
  virtual ~OdNwGeometryStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdNwDatabaseImpl* pDbImpl, OdStreamBufPtr stream, OdStreamBufPtr streamTable);
  virtual OdResult parseStream() override;

public:
  OdNwGeometryPtr getComponentByIndex(OdUInt32 index);
  virtual OdArray<OdNwGeometryPtr>& getGeometries();

  void setSettings(std::weak_ptr<OdNwStreamLoader> settings);
  virtual void setLinkLoaderGetter(const std::function<OdNwStreamLoaderPtr(NwStreamType::Enum)>& getLoader) override;

protected:
  OdResult readObjectsTable();
  OdNwGeometryPtr readElement(OdUInt32 nElement);
  void readNormals(OdStreamBufPtr pStream, OdArray<OdGeVector3d>& normals, OdArray<OdUInt16>& vertexCountPerFace, OdArray<OdUInt16>& indexes, OdInt64 elementEnd, OdUInt32 nextStructIndex, bool throwInvalidIndex = false);
  void readIndexTable(OdStreamBufPtr pStream, OdArray<OdInt32>& indexTable, OdInt32 count);
  void readBits(OdStreamBufPtr pStream, OdArray<OdUInt8>& bitsPerItem, OdInt32 bitSetsCount, OdArray<OdUInt32>& bitsArray, OdInt32 arraySize);
  void readSubfragmentFacesOrLines(OdStreamBufPtr pStream, OdArray<OdUInt16>& vertexCountPerItem, OdArray<OdUInt16>& indexes, OdInt64 endOfData);
  void readVertices(OdStreamBufPtr pStream, OdGePoint3dArray& vertices, OdUInt32 nextStructIndex, bool isLineSet, bool isPointSet);
  void readUVParams(OdStreamBufPtr pStream, OdArray<OdGePoint2d>& UVParameters, OdUInt32 nextStructIndex);
  void readColors(OdStreamBufPtr pStream, OdArray<OdUInt32>& colors, OdArray<OdUInt16>& vertexCountPerFace, OdArray<OdUInt16>& indexes, OdUInt64 componentEnd, bool isLineSet, bool isPointSet);

  virtual OdStreamBufPtr getStream(OdInt32& nElementPos, OdInt32& nElementEnd);

protected:
  NwGeometryObjectDefArray m_aObjectDefs;
  OdArray<OdNwGeometryPtr> m_aGeometries;
  OdStreamBufPtr           m_streamTable;

  bool m_bIsGeometryCompressionSettingsDetected;
  bool m_bIsGeometryCompressed;
  bool m_bIsNormalsPrecisionSettingsDetected;
  bool m_bIsNormalsPrecisionReduced;
};

typedef std::shared_ptr<OdNwGeometryStreamLoader> OdNwGeometryStreamLoaderPtr;
typedef std::weak_ptr<OdNwGeometryStreamLoader> OdNwGeometryStreamLoaderWeakPtr;

#include "TD_PackPop.h"

#endif // __NW_GEOMETRYSTREAMLOADER_H__
