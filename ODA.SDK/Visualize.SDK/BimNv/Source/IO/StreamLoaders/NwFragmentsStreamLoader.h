/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_FRAGMENTSSTREAMLOADER_H__
#define __NW_FRAGMENTSSTREAMLOADER_H__

#include "NwPropertiesStreamLoaderBase.h"
#include "NwFragmentType.h"
#define STL_USING_SET
#include "OdaSTL.h"
#include "TD_PackPush.h"

class OdNwComponent;
typedef OdSmartPtr<OdNwComponent> OdNwComponentPtr;
class OdNwFragment;
typedef OdSmartPtr<OdNwFragment> OdNwFragmentPtr;
class OdNwMaterial;
typedef OdSmartPtr<OdNwMaterial> OdNwMaterialPtr;

typedef std::map<OdUInt64, OdNwMaterialPtr> OdNwMaterialMap;
typedef std::map<OdString, OdInt32> OdTrfMap;
typedef std::pair<OdString, OdInt32> OdTrfPair;
typedef std::set<OdUInt32> OdIdSet;
typedef std::map<OdUInt32, OdNwComponentPtr> OdNwComponentMap;

class OdNwGeometryStreamLoader;
class OdNwSharedNodesStreamLoader;

class OdNwFragmentsStreamLoader : public OdNwPropertiesStreamLoaderBase
{
public:
  OdNwFragmentsStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion, OdTextureMap& textureMap, const OdString& texturePath);
  virtual ~OdNwFragmentsStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion, OdTextureMap& textureMap, const OdString& texturePath);
  virtual OdResult parseStream() override;

public:
  virtual void setLinkLoaderGetter(const std::function<OdNwStreamLoaderPtr(NwStreamType::Enum)>& getLoader) override;

public:
  const OdTrfMap &getTransformations() const;
  const OdIdSet &getPrimitiveIds() const;
  const OdNwMaterialMap& getMaterials() const;
  const OdNwComponentMap& getComponents() const;

private:
  void readReusableFragment(OdNwComponentPtr component, OdNwFragmentPtr fragment, OdUInt32 index);
  void ProcessComponent(OdNwComponentPtr component, OdUInt32 componentId);
  OdGeMatrix3d readFMatrix();
  OdGeMatrix3d readTransformationTRS();
  void readGeometryReference(OdNwFragmentPtr fragment);
  void readFragmentColors(OdNwComponentPtr component);
  OdUInt32 readAutodeskMaterial(NwFragmentType::Enum type, OdNwComponentPtr component);

private:
  OdTrfMap m_transformations; 
  OdIdSet m_primitiveIds;
  OdNwMaterialMap m_materials;
  OdNwComponentMap m_components;
  OdString m_sTexturesFolderPath;
  OdTextureMap& m_mTextures;

  std::function<OdNwStreamLoaderPtr(NwStreamType::Enum)> m_getLoaderCallBack;
  std::weak_ptr<OdNwGeometryStreamLoader> m_pGeometryLoader;
  std::weak_ptr<OdNwSharedNodesStreamLoader> m_pSharedNodesLoader;
};

typedef std::shared_ptr<OdNwFragmentsStreamLoader> OdNwFragmentsStreamLoaderPtr;
typedef std::weak_ptr<OdNwFragmentsStreamLoader> OdNwFragmentsStreamLoaderWeakPtr;

#include "TD_PackPop.h"

#endif // __NW_FRAGMENTSSTREAMLOADER_H__
