/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_SCENESETSTREAMLOADER_H__
#define __NW_SCENESETSTREAMLOADER_H__

#include "NwStreamLoader.h"
#include "NwFormatVersion.h"
#include "NwModelUnits.h"
#include "TD_PackPush.h"

class OdNwPropertyImpl;
typedef OdSmartPtr<OdNwPropertyImpl> OdNwPropertyImplPtr;
class OdNwModel;
typedef OdSmartPtr<OdNwModel> OdNwModelPtr;
typedef OdArray<OdNwModelPtr> OdModelArray;

class OdNwSceneSetStreamLoader : public OdNwStreamLoader
{
public:
  OdNwSceneSetStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual ~OdNwSceneSetStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual OdResult parseStream() override;

public:
  const OdModelArray& getModels() const;
  NwModelUnits::Enum getModelUnits() const;

private:
  OdNwModelPtr readScene();
  void readValidData(OdNwModelPtr pModel);
  void readPropertyByPlugin(OdNwModelPtr pModel);
  void readPropertyNW5(OdNwModelPtr pModel);
  void readProp(OdNwModelPtr pScene);
  void readValue(OdNwPropertyImplPtr& pProperty, OdNwModelPtr pScene);
  void skipTime();//VAS:: for now - it's skip data method, but maybe need to investigate skipping data

protected:
  const NwFormatVersion::Enum m_FormatVersion;
private:
  NwModelUnits::Enum m_ModelUnits;
  OdModelArray m_aModels;
};

#include "TD_PackPop.h"

#endif // __NW_SCENESETSTREAMLOADER_H__
