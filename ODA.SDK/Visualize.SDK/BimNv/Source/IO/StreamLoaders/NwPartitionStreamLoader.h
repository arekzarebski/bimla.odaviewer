/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_PARTITIONSTREAMLOADER_H__
#define __NW_PARTITIONSTREAMLOADER_H__

#include "NwPropertiesStreamLoaderBase.h"
#include "NwModelItem.h"
#include "NwCategory.h"
#include "TD_PackPush.h"

class OdNwGuidStoreStreamLoader;

class OdNwPartitionStreamLoader : public OdNwPropertiesStreamLoaderBase
{
public:
  OdNwPartitionStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual ~OdNwPartitionStreamLoader();
  static OdNwStreamLoaderPtr createStreamLoader(OdStreamBufPtr stream, NwFormatVersion::Enum FormatVersion);
  virtual OdResult parseStream() override;

public:
  OdNwModelItemPtr getModelProperties() const;
  const OdLightsArray& getLights() const;

public:
  virtual void setLinkLoaderGetter(const std::function<OdNwStreamLoaderPtr(NwStreamType::Enum)>& getLoader) override;

protected:
  OdNwModelItemPtr readModelProperties();

private:
  OdNwModelItemPtr m_pModelProperties;
  OdLightsArray m_aLights;
  std::function<OdNwStreamLoaderPtr(NwStreamType::Enum)> m_getLoaderCallBack;
};

class NotSupportedException
{
public:
  NotSupportedException(OdString msg) :m_msg(msg) {}

  OdString m_msg;
};

typedef std::shared_ptr<OdNwPartitionStreamLoader> OdNwPartitionStreamLoaderPtr;
typedef std::weak_ptr<OdNwPartitionStreamLoader> OdNwPartitionStreamLoaderWeakPtr;

#include "TD_PackPop.h"

#endif // __NW_PARTITIONSTREAMLOADER_H__
