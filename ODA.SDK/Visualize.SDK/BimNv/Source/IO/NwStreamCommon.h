/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#ifndef __NW_STREAMCOMMON_H__
#define __NW_STREAMCOMMON_H__

#include "OdPlatformStreamer.h"
#include "NwFragmentType.h"
#include "NwSpecialType.h"
#include "NwAutodeskMaterialPropertyValueType.h"
#include "NwValueType.h"
#include "Ge/GeQuaternion.h"
#include "NwModelUnits.h"
#include "NwColor.h"
#include "NwPresenterValueType.h"
#include "NwSavedviewItemType.h"
#include "NwCommentStatus.h"
#include "NwAnimationSmoothing.h"
#include "NwCameraMode.h"
#include "NwViewType.h"
#include "NwClipplanesetMode.h"
#include "NwLightType.h"
#include "NwModeType.h"
#include "NwClipplaneState.h"
#include "NwClipplaneAlignment.h"
#include "NwPlaneValueType.h"
#include "NwBackgroundType.h"

#include "TD_PackPush.h"

#ifndef NW_STARTSTREAM
#define NW_STARTSTREAM 0x00000064 
#endif

namespace NwStreamCommon
{
  inline OdUInt32 readUInt32(OdStreamBuf& stream)
  {
      OdUInt32 res;
      stream.getBytes(&res, sizeof(res));
      odSwap4BytesNumber(res);
      return res;
  }

  inline OdUInt64 readUInt64(OdStreamBuf& stream)
  {
    OdUInt64 res;
    stream.getBytes(&res, sizeof(res));
    odSwap8Bytes(res);
    return res;
  }

  inline OdInt32 readInt32(OdStreamBuf& stream)
  {
    OdInt32 res;
    stream.getBytes(&res, sizeof(res));
    odSwap4BytesNumber(res);
    return res;
  }

  inline OdInt64 readInt64(OdStreamBuf& stream)
  {
    OdInt64 res;
    stream.getBytes(&res, sizeof(res));
    odSwap8Bytes(res);
    return res;
  }

  inline OdUInt32 peekInt32(OdStreamBuf& stream)
  {
    const OdInt32 nRes = OdPlatformStreamer::rdInt32(stream);
    stream.seek(-4, OdDb::kSeekFromCurrent);
    return nRes;
  }

  inline OdUInt32 peekInt32Offset(OdStreamBuf& stream, OdInt32 offset)
  {
    OdInt64 pos = stream.tell();
    stream.seek(offset, OdDb::kSeekFromCurrent);
    OdUInt32 word = OdPlatformStreamer::rdInt32(stream);
    stream.seek(pos, OdDb::kSeekFromStart);
    return word;
  }

  inline void shiftBytes(OdStreamBuf& stream, OdInt32 offset)
  {
    stream.seek(offset, OdDb::kSeekFromCurrent);
  }

  inline void stepInt32(OdStreamBuf& stream, OdInt32 wordsCount = 1)
  {
    shiftBytes(stream, wordsCount * sizeof(OdInt32));
  }

  inline void skipSingleFillers(OdStreamBuf& stream)
  {
    OdInt64 pos = stream.tell();
    if (pos % 4 != 0)
      stream.seek(2, OdDb::kSeekFromCurrent);
  }

  inline void skipZeroFillers(OdStreamBuf& stream)
  {
    while (0 == OdPlatformStreamer::rdInt32(stream)) {}
      shiftBytes(stream, -4 /*-sizeof(OdUInt32)*/);
  }

  inline OdUInt32 readB_ABGR_Color(OdStreamBuf& stream)
  {
    OdUInt8 alpha = stream.getByte();
    OdUInt8 blue = stream.getByte();
    OdUInt8 green = stream.getByte();
    OdUInt8 red = stream.getByte();
    return ((OdUInt32)alpha << 24)
      || ((OdUInt32)red << 16)
      || ((OdUInt32)green << 8)
      || (blue);
  }

  inline OdNwColor readDRGBColor(OdStreamBuf& stream)
  {
    double red = OdPlatformStreamer::rdDouble(stream);
    double green = OdPlatformStreamer::rdDouble(stream);
    double blue = OdPlatformStreamer::rdDouble(stream);
    return OdNwColor(red, green, blue);
  }

  inline OdNwColor readDRGBAColor(OdStreamBuf& stream)
  {
    double red = OdPlatformStreamer::rdDouble(stream);
    double green = OdPlatformStreamer::rdDouble(stream);
    double blue = OdPlatformStreamer::rdDouble(stream);
    double alpha = OdPlatformStreamer::rdDouble(stream);
    return OdNwColor(alpha, red, green, blue);
  }

  inline OdNwColor readFRGBColor(OdStreamBuf& stream)
  {
    float red = OdPlatformStreamer::rdFloat(stream);
    float green = OdPlatformStreamer::rdFloat(stream);
    float blue = OdPlatformStreamer::rdFloat(stream);
    return OdNwColor(red, green, blue);
  }

  inline OdUInt32 readBRGBAColor(OdStreamBuf& stream)
  {
    OdUInt8 red = stream.getByte();
    OdUInt8 green = stream.getByte();
    OdUInt8 blue = stream.getByte();
    OdUInt8 alpha = stream.getByte();
    return ((OdUInt32)alpha << 24)
      || ((OdUInt32)red << 16)
      || ((OdUInt32)green << 8)
      || (blue);
  }

  inline OdNwColor readFRGBColorF(OdStreamBuf& stream)
  {
    float r = OdPlatformStreamer::rdFloat(stream);
    float g = OdPlatformStreamer::rdFloat(stream);
    float b = OdPlatformStreamer::rdFloat(stream);
    return OdNwColor(r, g, b);
  }

  inline OdString readString(OdStreamBuf& stream, OdUInt32 nAlignment = 4)
  {
    OdInt32 len = OdPlatformStreamer::rdInt32(stream);
    if (len > 0)
    {
      OdInt32 align = len % nAlignment;
      if (align > 0)
        len += nAlignment - align;
      OdAnsiString sBuffer; // NB: UTF-8 string
      stream.getBytes(sBuffer.getBufferSetLength(len), len);

      OdCharArray buf;
      OdCharMapper::utf8ToUnicode(sBuffer.c_str(), sBuffer.getLength(), buf);
      return OdString(buf.getPtr(), buf.size() - 1);
    }
    return OdString();
  }

  inline OdGePoint3d readFPoint3d(OdStreamBuf& stream)
  {
    float x = OdPlatformStreamer::rdFloat(stream);
    float y = OdPlatformStreamer::rdFloat(stream);
    float z = OdPlatformStreamer::rdFloat(stream);
    return (OdGePoint3d(x, y, z));
  }

  inline OdGeVector3d readFVector3d(OdStreamBuf& stream)
  {
    float x = OdPlatformStreamer::rdFloat(stream);
    float y = OdPlatformStreamer::rdFloat(stream);
    float z = OdPlatformStreamer::rdFloat(stream);
    return (OdGeVector3d(x, y, z));
  }

  inline OdGePoint3d readDPoint3d(OdStreamBuf& stream)
  {
    double x = OdPlatformStreamer::rdDouble(stream);
    double y = OdPlatformStreamer::rdDouble(stream);
    double z = OdPlatformStreamer::rdDouble(stream);
    return (OdGePoint3d(x, y, z));
  }

  inline OdGeVector3d readDVector3d(OdStreamBuf& stream)
  {
    double x = OdPlatformStreamer::rdDouble(stream);
    double y = OdPlatformStreamer::rdDouble(stream);
    double z = OdPlatformStreamer::rdDouble(stream);
    return (OdGeVector3d(x, y, z));
  }

  inline OdGeMatrix3d readDMatrix3d(OdStreamBuf& stream)
  {
    OdGeMatrix3d matrix = OdGeMatrix3d::kIdentity;
    matrix(0, 0) = OdPlatformStreamer::rdDouble(stream);
    matrix(0, 1) = OdPlatformStreamer::rdDouble(stream);
    matrix(0, 2) = OdPlatformStreamer::rdDouble(stream);
    matrix(1, 0) = OdPlatformStreamer::rdDouble(stream);
    matrix(1, 1) = OdPlatformStreamer::rdDouble(stream);
    matrix(1, 2) = OdPlatformStreamer::rdDouble(stream);
    matrix(2, 0) = OdPlatformStreamer::rdDouble(stream);
    matrix(2, 1) = OdPlatformStreamer::rdDouble(stream);
    matrix(2, 2) = OdPlatformStreamer::rdDouble(stream);
    matrix(0, 3) = OdPlatformStreamer::rdDouble(stream);
    matrix(1, 3) = OdPlatformStreamer::rdDouble(stream);
    matrix(2, 3) = OdPlatformStreamer::rdDouble(stream);
    matrix(3, 3) = 0;

    return matrix;
  }

  inline OdGeQuaternion readQuaternion(OdStreamBuf& stream)
  {
    double x = OdPlatformStreamer::rdDouble(stream);
    double y = OdPlatformStreamer::rdDouble(stream);
    double z = OdPlatformStreamer::rdDouble(stream);
    double w = OdPlatformStreamer::rdDouble(stream);
    return OdGeQuaternion(w, x, y, z);
  }

  inline NwFragmentType::Enum readFragmentType(OdStreamBuf& stream)
  {
    NwFragmentType::Enum result = static_cast<NwFragmentType::Enum> (OdPlatformStreamer::rdInt32(stream));

    // Unsupported extra data format check
    ODA_ASSERT(!(result == NwFragmentType::FRAGMENT_INTERNAL_REFERENCE ||
      result == NwFragmentType::FRAGMENT_EXTERNAL_REFERENCE ||
      result == NwFragmentType::FRAGMENT_EMBEDDED_DATA));
    return result;
  }

  inline NwSpecialType::Enum readSpecialType(OdStreamBuf& stream)
  {
    NwSpecialType::Enum result = static_cast<NwSpecialType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwAutodeskMaterialPropertyValueType::Enum readAutodeskMaterialPropertyValueType(OdStreamBuf& stream)
  {
    NwAutodeskMaterialPropertyValueType::Enum result = static_cast<NwAutodeskMaterialPropertyValueType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwValueType::Enum readValueType(OdStreamBuf& stream)
  {
    NwValueType::Enum result = static_cast<NwValueType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwModelUnits::Enum readModelUnits(OdStreamBuf& stream)
  {
    NwModelUnits::Enum result = static_cast<NwModelUnits::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwPresenterValueType::Enum readPresenterValueType(OdStreamBuf &stream)
  {
    NwPresenterValueType::Enum result = static_cast<NwPresenterValueType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwSavedviewItemType::Enum readSavedviewItemType(OdStreamBuf &stream)
  {
    NwSavedviewItemType::Enum result = static_cast<NwSavedviewItemType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwCommentStatus::Enum readCommentStatus(OdStreamBuf &stream)
  {
    NwCommentStatus::Enum result = static_cast<NwCommentStatus::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwAnimationSmoothing::Enum readAnimationSmoothing(OdStreamBuf &stream)
  {
    NwAnimationSmoothing::Enum result = static_cast<NwAnimationSmoothing::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwViewType::Enum readViewType(OdStreamBuf &stream)
  {
    NwViewType::Enum result = static_cast<NwViewType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwCameraMode::Enum readCameraMode(OdStreamBuf &stream)
  {
    NwCameraMode::Enum result = static_cast<NwCameraMode::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwClipplanesetMode::Enum readClipplanesetMode(OdStreamBuf &stream)
  {
    NwClipplanesetMode::Enum result = static_cast<NwClipplanesetMode::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwLightType::Enum readLightType(OdStreamBuf &stream)
  {
    NwLightType::Enum result = static_cast<NwLightType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwModeType::Enum readModeType(OdStreamBuf &stream)
  {
    NwModeType::Enum result = static_cast<NwModeType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwClipplaneState::Enum readClipplaneState(OdStreamBuf &stream)
  {
    NwClipplaneState::Enum result = static_cast<NwClipplaneState::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwClipplaneAlignment::Enum readClipplaneAlignment(OdStreamBuf &stream)
  {
    NwClipplaneAlignment::Enum result = static_cast<NwClipplaneAlignment::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwPlaneValueType::Enum readPlaneValueType(OdStreamBuf &stream)
  {
    NwPlaneValueType::Enum result = static_cast<NwPlaneValueType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline NwBackgroundType::Enum readBackgroundType(OdStreamBuf &stream)
  {
    NwBackgroundType::Enum result = static_cast<NwBackgroundType::Enum> (OdPlatformStreamer::rdInt32(stream));

    return result;
  }

  inline OdGUID readGuid(OdStreamBuf& stream)
  {
    OdGUID::DataType guidData;
    stream.getBytes(guidData, sizeof(guidData));
    return OdGUID(guidData);
  }
} // namespace NwStreamCommon

// Write
namespace NwStreamCommon
{
  inline void writeUInt32(OdStreamBuf& stream, OdUInt32 val)
  {
    odSwap4BytesNumber(val);
    stream.putBytes(&val, sizeof(val));
  }

  inline void writeUInt64(OdStreamBuf& stream, OdUInt64 val)
  {
    odSwap8Bytes(val);
    stream.putBytes(&val, sizeof(val));
  }

  inline void writeInt32(OdStreamBuf& stream, OdInt32 val)
  {
    odSwap4BytesNumber(res);
    stream.putBytes(&val, sizeof(val));
  }

  inline void writeInt64(OdStreamBuf& stream, OdInt64 val)
  {
    odSwap8Bytes(val);
    stream.getBytes(&val, sizeof(val));

  }
  inline void writeDRGBColor(OdStreamBuf& stream, const OdNwColor& color)
  {
    OdPlatformStreamer::wrDouble(stream, color.R());
    OdPlatformStreamer::wrDouble(stream, color.G());
    OdPlatformStreamer::wrDouble(stream, color.B());
  }
  inline void writeBackgroundType(OdStreamBuf &stream, NwBackgroundType::Enum type)
  {
    writeUInt32(stream, static_cast<OdUInt32> (type));
  }

  inline void writeGuid(OdStreamBuf& stream, const OdGUID& guid)
  {
    const OdGUID::DataType& guidData = guid.data();
    stream.putBytes(guidData, sizeof(guidData));
  }
}// namespace NwStreamCommon

#include "TD_PackPop.h"

#endif // __NW_STREAMCOMMON_H__
