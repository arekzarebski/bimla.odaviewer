/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWMODELIMPL_INCLUDED_
#define __NWMODELIMPL_INCLUDED_

#include "NwModel.h"
#include "Ge/GeMatrix3d.h"
#include "NwObjectImpl.h"

typedef std::map<OdString, OdNwPropertyPtr> OdPropertyMap;

class OdNwModelImpl : public OdNwObjectImpl
{
public:
  OdNwModelImpl();
  virtual ~OdNwModelImpl();
  ODRX_DECLARE_MEMBERS(OdNwModelImpl);

  static OdNwModelImpl* getImpl(const OdNwModel* pNwModel);
  static OdNwModelImpl* getImpl(const OdRxObject* pRxScene);

public:
  OdString getFileName() const;
  OdString getPath() const;
  OdString getSourceFileName() const;
  bool isChange() const;
  OdUInt32 getChangeBitfield() const;
  OdGeMatrix3d getTransform() const;
  NwModelType::Enum getType() const;
  NwModelUnits::Enum getUnits() const;
  OdResult getProperties(OdPropertyMap& mProps) const;
  const OdPropertyMap& getProperties() const;

public:
  void setFileName(const OdString& name);
  void setFilePath(const OdString& path);
  void setSourceFileName(const OdString& sourceName);
  void setChangeBitfield(OdUInt32 chhangeBitfield);
  void setTransform(const OdGeMatrix3d& trMtrx);
  void setType(NwModelType::Enum type);
  void setUnits(NwModelUnits::Enum units);
  OdResult addProperty(const OdString& key, OdNwPropertyPtr pProp);

private:
  OdString m_Name;
  OdString m_Path;
  OdString m_OldName;
  OdUInt32 m_changeBitfield;
  OdGeMatrix3d m_trMtrx;
  NwModelType::Enum m_PluginType;
  NwModelUnits::Enum m_ModelUnits;
  OdPropertyMap m_Props;
};

typedef OdSmartPtr<OdNwModelImpl> OdNwModelImplPtr;

#endif //__NWMODELIMPL_INCLUDED_
