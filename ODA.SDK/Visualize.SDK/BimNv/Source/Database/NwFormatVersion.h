/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef TNW_FORMAT_VERSION_H_
#define TNW_FORMAT_VERSION_H_

namespace NwFormatVersion
{
  enum Enum
  {
    NAVISWORKS1_2 = 2,
    NAVISWORKS1_3 = 3,
    NAVISWORKS1_5 = 5,
    NAVISWORKS2 = 22,
    NAVISWORKS2_34 = 34,
    NAWISWORKS2_53 = 53,
    NAWISWORKS2_62 = 62,
    NAWISWORKS2_63 = 63,
    NAWISWORKS2_64 = 64,
    NAWISWORKS4 = 72, // ???
    NAWISWORKS4_78 = 78,
    NAWISWORKS4_80 = 80,
    NAWISWORKS5 = 90,
    NAWISWORKS5_92 = 92,
    NAWISWORKS2009 = 103,
    NAWISWORKS2009_109 = 109,
    NAWISWORKS2009_110 = 110,
    NAWISWORKS2009_112 = 112,
    NAWISWORKS2010 = 114,
    NAWISWORKS2011 = 120,
    NAWISWORKS2011_214 = 214,
    NAWISWORKS2011_217 = 217,
    NAWISWORKS2012 = 238,
    NAWISWORKS2012_244 = 244,
    NAWISWORKS2012_246 = 246,
    NAWISWORKS2012_251 = 251,
    NAWISWORKS2012_252 = 252,
    NAWISWORKS2013 = 405,
    NAWISWORKS2013_410 = 410,
    NAWISWORKS2014 = 431,
    NAWISWORKS2014_438 = 438,
    NAWISWORKS2015 = 441,
    NAWISWORKS2016 = 448 // 2016 and 2017
  };

  static bool isDefined(NwFormatVersion::Enum Formatversion)
  {
    return (Formatversion == NAWISWORKS4)
        || (Formatversion == NAWISWORKS5)
        || (Formatversion == NAWISWORKS2009)
        || (Formatversion == NAWISWORKS2010)
        || (Formatversion == NAWISWORKS2011)
        || (Formatversion == NAWISWORKS2012)
        || (Formatversion == NAWISWORKS2013)
        || (Formatversion == NAWISWORKS2014)
        || (Formatversion == NAWISWORKS2015)
        || (Formatversion == NAWISWORKS2016)
      ;
  }
}

#endif  // TNW_FORMAT_VERSION_H_

