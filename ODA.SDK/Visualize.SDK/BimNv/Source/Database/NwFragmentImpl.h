/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWFRAGMENTIMPL_INCLUDED__
#define __NWFRAGMENTIMPL_INCLUDED__

#include "NwFragment.h"
#include "NwObjectImpl.h"
#include <memory>

class OdNwGeometry;
typedef OdSmartPtr<OdNwGeometry> OdNwGeometryPtr;
class OdNwGeometryStreamLoader;
class OdNwSharedNodesStreamLoader;

class OdNwFragmentImpl : public OdNwObjectImpl
{
public:
  OdNwFragmentImpl();
  virtual ~OdNwFragmentImpl();
  ODRX_DECLARE_MEMBERS(OdNwFragmentImpl);

  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const;
  virtual bool subWorldDraw(OdGiWorldDraw* pWd) const;
  virtual void subViewportDraw(OdGiViewportDraw* pVd) const;

  static OdNwFragmentImpl* getImpl(const OdNwFragment* pNwFragment);
  static OdNwFragmentImpl* getImpl(const OdRxObject* pRxFragment);

public:
  //public implementation's methods
  OdGePoint3d getLowBound() const;
  OdGePoint3d getHighBound() const;
  OdGeMatrix3d getTransformation() const;
  bool isEllipse() const;
  bool isLineSet() const;
  bool isMesh() const;
  bool isPointSet() const;
  bool isText() const;
  bool isTube() const;
  bool checkGeometryType(OdRxClass* pClass) const;
  OdNwGeometryEllipticalShapePtr getEllipse() const;
  OdNwGeometryLineSetPtr getLineSet() const;
  OdNwGeometryMeshPtr getMesh() const;
  OdNwGeometryPointSetPtr getPointSet() const;
  OdNwGeometryTextPtr getText() const;
  OdNwGeometryTubePtr getTube() const;

public:
  // Index of parent component from LcOpNwdLogicalHierarchy stream.
  //
  // Name.nwd               - index 0
  // |
  // |____0(layer)          - index 1
  // |    |
  // |    |___3D Solid      - index 2
  // |    |
  // |    |___3D Solid      - index 3
  // |
  // |____1(layer)          - index 4
  //      |
  //      |___3D Solid      - index 5
  //      |
  //      |___3D Solid      - index 6

public:
  OdUInt32 getGeometryIndex() const;
  OdUInt32 getComponentIndex() const;
  OdNwGeometryPtr getGeometry() const;

  void setLowBound(const OdGePoint3d& lb);
  void setHighBound(const OdGePoint3d& hb);
  void setTransformation(const OdGeMatrix3d& tr);
  void setGeometryIndex(OdUInt32 idx);
  void setComponentIndex(OdUInt32 idx);
  void setGeometryLoader(std::weak_ptr<OdNwGeometryStreamLoader> pGeomLoader);
  void setSharedNodesLoader(std::weak_ptr<OdNwSharedNodesStreamLoader> pSharedNodesLoader);

private:
  OdUInt32        m_nComponentIndex;

  // 1-based index of geometry item from LcOpNwdGeometry stream.
  OdUInt32        m_nGeometryIndex;
  OdGeMatrix3d    m_oTrf; // tranformation matrix
  

  // TODO: change 2 points to bounding box
  OdGePoint3d     m_oLowBound;
  OdGePoint3d     m_oHighBound;

  // Loaders
  mutable std::weak_ptr<OdNwGeometryStreamLoader>    m_pGeomLoader;
  mutable std::weak_ptr<OdNwSharedNodesStreamLoader> m_pSharedNodesLoader;
};

typedef OdSmartPtr<OdNwFragmentImpl> OdNwFragmentImplPtr;

#endif //__NWFRAGMENTIMPL_INCLUDED__
