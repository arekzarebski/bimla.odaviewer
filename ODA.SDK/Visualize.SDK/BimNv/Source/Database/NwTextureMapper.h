/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWTEXTUREMAPPER_INCLUDED__
#define __NWTEXTUREMAPPER_INCLUDED__

#include "RxObject.h"
#include "RxSystemServices.h"
#include "NwModelUnits.h"
#include "NwTextureBuf.h"

typedef std::pair<double, NwModelUnits::Enum> OdNwDoubleUnit;

class OdNwTextureMapper
{
public:
  OdNwTextureMapper();
  virtual ~OdNwTextureMapper();

public:
  const OdString& getPath() const;
  const double& getRealWorldScaleX() const;
  const double& getRealWorldScaleY() const;
  NwModelUnits::Enum getScaleXUnit() const;
  NwModelUnits::Enum getScaleYUnit() const;
  const double& getRealWorldOffsetX() const;
  const double& getRealWorldOffsetY() const;
  NwModelUnits::Enum getOffsetXUnit() const;
  NwModelUnits::Enum getOffsetYUnit() const;
  OdNwTextureBufPtr getTextureBuf() const;
  bool hasTextureBuf() const;

  void setPath(const OdString& val);
  void setRealWorldScaleX(const double& val);
  void setRealWorldScaleY(const double& val);
  void setScaleXUnit(NwModelUnits::Enum val);
  void setScaleYUnit(NwModelUnits::Enum val);
  void setRealWorldOffsetX(const double& val);
  void setRealWorldOffsetY(const double& val);
  void setOffsetXUnit(NwModelUnits::Enum val);
  void setOffsetYUnit(NwModelUnits::Enum val);
  void setTextureBuf(OdNwTextureBufPtr pTxtBuf);

  void setDefault();

protected:
  OdNwDoubleUnit createDefScale() const;
  OdNwDoubleUnit createDefOffset() const;

private:
  OdString m_Path;
  OdNwDoubleUnit m_RealWorldScaleX;
  OdNwDoubleUnit m_RealWorldScaleY;
  OdNwDoubleUnit m_RealWorldOffsetX;
  OdNwDoubleUnit m_RealWorldOffsetY;
  OdNwTextureBufPtr m_pTextureBuf;
};

#endif //__NWTEXTUREMAPPER_INCLUDED__
