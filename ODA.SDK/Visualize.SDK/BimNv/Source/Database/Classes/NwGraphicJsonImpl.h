/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NW_GRAPHIC_JSON_IMPL_H__
#define __NW_GRAPHIC_JSON_IMPL_H__

#include "NwGraphicJson.h"
#include "NwColor.h"
#include "OdArray.h"
#include "NwObjectImpl.h"
#include "NwCategory.h"

class OdNwGraphicJsonImpl : public OdNwObjectImpl
{
public:
  OdNwGraphicJsonImpl();
  virtual ~OdNwGraphicJsonImpl();
  ODRX_DECLARE_MEMBERS(OdNwGraphicJsonImpl);

  DrawableType drawableType() const override;
  OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const override;

public:
  static OdNwGraphicJsonImpl* getImpl(const OdNwGraphicJson* pNwBck);
  static OdNwGraphicJsonImpl* getImpl(const OdRxObject* pRxBck);

//public implementation's methods

public:

  OdString getJson() const;
  void setJson(OdString json);
  void setFileRef(OdString key, OdString name);
  OdString getFileRef(OdString key) const;

  void setFileEmbedded(OdString key, const OdBinaryData& content);
  OdBinaryData getFileEmbedded(OdString key) const;

  OdArray<OdString> getFileRefKeys() const;
  OdArray<OdString> getFileEmbeddedKeys() const;

  void setMaterialJson(OdString json);
  OdString getMaterialJson() const;

protected:
  OdString                        m_sJson;
  OdString                        m_sMaterialJson;
  std::map<OdString, OdString>       m_mFileRefs;
  std::map<OdString, OdBinaryData>   m_mFileEmbedded;
};

typedef OdSmartPtr<OdNwGraphicJsonImpl> OdNwGraphicJsonImplPtr;

#endif //__NW_GRAPHIC_JSON_IMPL_H__
