/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2017, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2015 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _NWOBJECTIMPL_H_
#define _NWOBJECTIMPL_H_

#include "Ge/GeMatrix3d.h"
#include "NwObject.h"
#include "NwObjectContainer.h"
#include "OdBinaryData.h"
#define STL_USING_MAP
#include "OdaSTL.h"

///////////////////////////////////////////////////////////////////////////////

class OdNwObjectImpl : public OdGiDrawable// : public OdNwObject
{
protected:
  friend class OdNwDatabase;
  friend class OdNwDatabaseImpl;
  OdNwObjectId m_id;
  OdGsCache* m_pGsNode;

  OdNwObjectImpl();
public:
  virtual ~OdNwObjectImpl();
  ODRX_DECLARE_MEMBERS(OdNwObjectImpl);

  static OdNwObjectImpl* getImpl(const OdNwObject* pNwObj);
  static OdNwObjectImpl* getImpl(const OdRxObject* pRxObj);
  static void setImpl(OdNwObject* pNwObj, const OdNwObjectImpl* pNwObjImpl);

  // OdNwObject members:
  virtual OdNwObjectId objectId() const;
  virtual OdDbStub* ownerId() const;
  virtual void addReactor(OdNwObjectReactor* pReactor);
  virtual void removeReactor(OdNwObjectReactor* pReactor);

  // OdGiDrawable members:
  virtual bool isPersistent() const;
  virtual OdDbStub* id() const;
  virtual void setGsNode(OdGsCache* pGsNode);
  virtual OdGsCache* gsNode() const;
//protected:
  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* traits) const;
  virtual bool subWorldDraw(OdGiWorldDraw* wd) const;
  virtual void subViewportDraw(OdGiViewportDraw* vd) const;
};

typedef OdSmartPtr<OdNwObjectImpl> OdNwObjectImplPtr;

class NWDBEXPORT OdNwEntity : public OdNwObjectImpl
{
protected:
  OdNwEntity();
public:
  ODRX_DECLARE_MEMBERS(OdNwEntity);

protected:
  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* traits) const;
};

typedef OdSmartPtr<OdNwEntity> OdNwEntityPtr;

class NWDBEXPORT OdNwView : public OdNwObjectImpl
{
protected:
  class OdGsView* m_pGsView;
  OdString m_sName;

  OdNwView();

  virtual DrawableType drawableType() const { return kViewport; }
public:
  ODRX_DECLARE_MEMBERS(OdNwView);
  virtual ~OdNwView() {};

  virtual bool isModelLayout() const = 0;
  virtual bool getThumbnail(OdBinaryData& data) const;

  virtual OdString getName() const;
  void getViewports(OdNwObjectIdArray& idsViewports) const;

  OdGsView* gsView() const;
  void setGsView(OdGsView* pGsView);
};

typedef OdSmartPtr<OdNwView> OdNwViewPtr;

class  OdNwModelView : public OdNwView // for 3d NW_TK_Camera
{
//protected:
public:
  friend class OdNwGsLayoutHelper;
  OdGePoint3d m_ptCameraTarget;
  OdGeVector3d m_vecCameraDir, m_vecCameraUpVector;
  double m_withField, m_heightField;
  bool m_isPerspective;
  OdGeMatrix3d m_matrix;
  OdNwObjectContainer m_idsSegments;
  OdNwObjectId m_idViewBackground;

  OdNwModelView();
public:
  ODRX_DECLARE_MEMBERS(OdNwModelView);

  virtual bool isModelLayout() const;
  virtual bool getThumbnail(OdBinaryData& data) const;

  OdResult addObjectSegment(OdNwObjectPtr pObject);
  OdResult addViewBackground(OdNwObjectPtr pObject);

//protected:
  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* traits) const;
  virtual bool subWorldDraw(OdGiWorldDraw* wd) const;
  virtual void subViewportDraw(OdGiViewportDraw* vd) const;
};

typedef OdSmartPtr<OdNwModelView> OdNwModelViewPtr;

#endif // _NWOBJECTIMPL_H_


