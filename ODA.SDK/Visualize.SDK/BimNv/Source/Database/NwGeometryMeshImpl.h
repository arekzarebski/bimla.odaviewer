/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWGEOMETRYMESHIMPL_INCLUDED__
#define __NWGEOMETRYMESHIMPL_INCLUDED__

#include "NwGeometryMesh.h"
#include "NwGeometry.h"
#include "RxObjectImpl.h"
#include "OdArray.h"

class OdNwGeometryMeshImpl;
typedef OdSmartPtr<OdNwGeometryMeshImpl> OdNwGeometryMeshImplPtr;

class OdNwGeometryMeshImpl : public OdRxObjectImpl<OdNwGeometry>
{
public:
  OdNwGeometryMeshImpl();
  virtual ~OdNwGeometryMeshImpl();
  ODRX_DECLARE_MEMBERS(OdNwGeometryMeshImpl);

  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const;
  virtual bool subWorldDraw(OdGiWorldDraw* pWd) const;
  virtual void subViewportDraw(OdGiViewportDraw* pVd) const;

public:
  static OdNwGeometryMeshImpl* getImpl(const OdNwGeometryMesh* pNwGeometry);
  static OdNwGeometryMeshImpl* getImpl(const OdRxObject* pRxGeometry);
  static OdNwGeometryMeshImplPtr createGeometry();

public:
  OdArray<OdUInt16>& getVertexCountPerFace();
  const OdArray<OdUInt16>& getVertexCountPerFace() const;
  OdArray<OdUInt16>& getIndexes();
  const OdArray<OdUInt16>& getIndexes() const;
  OdArray<OdGeVector3d>& getNormales();
  const OdArray<OdGeVector3d>& getNormales() const;
  OdArray<OdGePoint2d>& getUVParameters();
  const OdArray<OdGePoint2d>& getUVParameters() const;
  OdArray<OdArray<OdInt32> >& getFaces();
  bool getNormalesAreSkipped() const;
  
protected:
  void prepareFaces();

protected:
  OdArray<OdUInt16>         m_aVertexCountPerFace;
  OdArray<OdUInt16>         m_aIndexes;
  OdArray<OdGeVector3d>     m_aNormales;
  OdArray<OdGePoint2d>      m_aUVParameters;
  OdArray<OdArray<OdInt32> > m_aFaces;
  bool                      m_bNormalesAreSkipped;
};

#endif //__NWGEOMETRYMESHIMPL_INCLUDED__
