/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2017, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2015 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _NWDATABASEUTILS_INCLUDED_
#define _NWDATABASEUTILS_INCLUDED_

class OdNwDatabaseImpl;
class OdNwObjectId;
class OdNwBackgroundElement;
class OdNwModelItem;
class OdNwComponent;
class OdNwFragment;
//class OdNwGeometry;
//class OdNwTextFontInfo;
class OdNwMaterial;
class OdNwViewpoint;
class OdNwTexture;
class OdNwModel;
class OdNwProperty;
class OdNwCategory;
//class OdNwColor;
class OdNwCommonLight;

namespace NwDatabaseUtils
{
  OdNwObjectId addBackgroundElement(OdNwDatabaseImpl* pDbImpl, const OdNwBackgroundElement* pBackground);
  OdNwObjectId addModelItem(OdNwDatabaseImpl* pDbImpl, const OdNwModelItem* pNodeImpl, const OdNwObjectId& ownerId);
  OdNwObjectId addComponent(OdNwDatabaseImpl* pDbImpl, const OdNwComponent* pComponent, const OdNwObjectId& ownerId);
  OdNwObjectId addFragment(OdNwDatabaseImpl* pDbImpl, const OdNwFragment* pFragment, const OdNwObjectId& ownerId);
  //OdNwObjectId addGeometry(OdNwDatabaseImpl* pDbImpl, const OdNwGeometry* pGeometry, const OdNwObjectId& ownerId);
 // OdNwObjectId addTextFont(OdNwDatabaseImpl* pDbImpl, const OdNwTextFontInfo* pFont, const OdNwObjectId& ownerId);
  OdNwObjectId addMaterial(OdNwDatabaseImpl* pDbImpl, const OdNwMaterial* pMat, const OdNwObjectId& ownerId);
  OdNwObjectId addViewpoint(OdNwDatabaseImpl* pDbImpl, const OdNwViewpoint* pView);
  OdNwObjectId addTexture(OdNwDatabaseImpl* pDbImpl, const OdNwTexture* pTxt, const OdNwObjectId& ownerId);
  OdNwObjectId addModel(OdNwDatabaseImpl* pDbImpl, const OdNwModel* pModel);
  OdNwObjectId addProperty(OdNwDatabaseImpl* pDbImpl, const OdNwProperty* pProp, const OdNwObjectId& ownerId);
  OdNwObjectId addCategory(OdNwDatabaseImpl* pDbImpl, const OdNwCategory* pCat, const OdNwObjectId& ownerId);
  //OdNwObjectId addColor(OdNwDatabaseImpl* pDbImpl, const OdNwColor* pClr, const OdNwObjectId& ownerId);
  OdNwObjectId addLight(OdNwDatabaseImpl* pDbImpl, const OdNwCommonLight* pLight);
}

#endif // _NWDATABASEUTILS_INCLUDED_
