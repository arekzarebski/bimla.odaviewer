/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWMATERIALIMPL_INCLUDED__
#define __NWMATERIALIMPL_INCLUDED__

#include "NwMaterial.h"
#include "NwCategoryImpl.h"
#include "SharedPtr.h"

class OdNwTextureBuf;
typedef OdSharedPtr<OdNwTextureBuf> OdNwTextureBufPtr;
typedef std::map<OdString, OdNwTextureBufPtr> OdTextureMap;
class OdGiMaterialColor;

class OdNwMaterialImpl : public OdNwCategoryImpl
{
public:
  OdNwMaterialImpl();
  OdNwMaterialImpl(const OdUInt64& position, const OdString& name = OD_T(""), const OdString& displayName = OD_T(""));
  virtual ~OdNwMaterialImpl();
  ODRX_DECLARE_MEMBERS(OdNwMaterialImpl);

  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const;

protected:
  void setMaterialColor(OdGiMaterialColor& matColor, const OdNwColor& sourceColor) const;

public:
  static OdNwMaterialImpl* getImpl(const OdNwMaterial* pNwMaterial);
  static OdNwMaterialImpl* getImpl(const OdRxObject* pRxMaterial);

public:
  static OdResult createMaterialImpl(OdNwMaterial* pMaterial);
  static OdResult createMaterialImpl(OdNwMaterialPtr pMaterial, const OdUInt64& position, const OdString& name = OD_T(""), const OdString& displayName = OD_T(""));

public:
  virtual OdNwCategoryPtr createCopy() override;
  virtual OdRxObjectPtr createCategoryObject() override;

public:
  virtual void ParseProperties(OdString texturesSourceDirectory, OdTextureMap* pTextureMap = NULL);

public:
  //public implementation's methods
  OdResult getAmbient(OdNwColor&) const;
  // It's a main color.
  OdResult getDiffuse(OdNwColor&) const;
  OdResult getSpecular(OdNwColor&) const;
  OdResult getEmissive(OdNwColor&) const;
  float getShininess() const;
  float getTransparency() const;

public:
  OdSharedPtr<OdNwColor> getAmbient() const;
  OdSharedPtr<OdNwColor> getDiffuse() const;
  OdSharedPtr<OdNwColor> getSpecular() const;
  OdSharedPtr<OdNwColor> getEmissive() const;

public:
  void setAmbient(OdSharedPtr<OdNwColor> pAmbient);
  void setDiffuse(OdSharedPtr<OdNwColor> pDiffuse);
  void setSpecular(OdSharedPtr<OdNwColor> pSpecular);
  void setEmissive(OdSharedPtr<OdNwColor> pEmissive);
  void setShininess(float shininess);
  void setTransparency(float transparency);

protected:
  OdSharedPtr<OdNwColor> m_pAmbient;
  // It's a main color.
  OdSharedPtr<OdNwColor> m_pDiffuse;
  OdSharedPtr<OdNwColor> m_pSpecular;
  OdSharedPtr<OdNwColor> m_pEmissive;
  float m_Shininess;
  float m_Transparency;
};

typedef OdSmartPtr<OdNwMaterialImpl> OdNwMaterialImplPtr;

#endif //__NWMATERIALIMPL_INCLUDED__
