/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWSAVEDVIEWPOINTIMPL_INCLUDED__
#define __NWSAVEDVIEWPOINTIMPL_INCLUDED__

#include "NwSavedViewpoint.h"
#include "NwSavedItemImpl.h"

class OdNwSavedViewpointImpl : public OdNwSavedItemImpl
{
public:
  ODRX_DECLARE_MEMBERS(OdNwSavedViewpointImpl);

public:
  OdNwSavedViewpointImpl();
  virtual ~OdNwSavedViewpointImpl();

public:
  static OdNwSavedViewpointImpl* getImpl(const OdNwSavedViewpoint* pNwSV);
  static OdNwSavedViewpointImpl* getImpl(const OdRxObject* pRxSV);

public:
  OdNwViewpointPtr getViewpoint() const;
  void setViewpoint(OdNwViewpointPtr pView);

protected:
  OdNwViewpointPtr m_pView;
};

typedef OdSmartPtr<OdNwSavedViewpointImpl> OdNwSavedViewpointImplPtr;

#endif // __NWSAVEDVIEWPOINTIMPL_INCLUDED__
