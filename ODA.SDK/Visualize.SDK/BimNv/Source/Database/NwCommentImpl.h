/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWCOMMENTIMPL_INCLUDED__
#define __NWCOMMENTIMPL_INCLUDED__

#include "NwComment.h"
#include "RxSystemServices.h"
#include "NwObjectImpl.h"

class OdNwCommentImpl : public OdNwObjectImpl
{
  ODRX_DECLARE_MEMBERS(OdNwCommentImpl);
  OdNwCommentImpl();
public:
  virtual ~OdNwCommentImpl();

public:
  static OdNwCommentImpl* getImpl(const OdNwComment& NwComment);

public:
  OdString getAuthor() const;
  OdString getBody() const;
  OdUInt32 getCreateDate() const;
  OdUInt32 getId() const;
  NwCommentStatus::Enum getStatus() const;

  void setAuthor(const OdString& author);
  void setBody(const OdString& body);
  void setCreateDate(OdUInt32 date);
  void setId(OdUInt32 id);
  void setStatus(NwCommentStatus::Enum status);

private:
  OdString m_user;
  OdString m_body;
  OdUInt32 m_createdate;
  OdUInt32 m_id;
  NwCommentStatus::Enum m_status;
};

typedef OdSmartPtr<OdNwCommentImpl> OdNwCommentImplPtr;

#endif //__NWCOMMENTIMPL_INCLUDED__
