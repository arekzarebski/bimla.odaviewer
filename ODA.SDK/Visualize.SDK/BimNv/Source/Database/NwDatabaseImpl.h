/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _NWDATABASEIMPL_INCLUDED_
#define _NWDATABASEIMPL_INCLUDED_

#include "NwDatabase.h"
#include "NwObjectImpl.h"
#include "NwFileLoader.h"
#include "NwGeometryCompressSettings.h"

namespace tbb 
{
  class spin_mutex;
}

class OdNwDatabaseImpl : public OdNwObjectImpl
{
protected:
  OdSharedPtr<class OdHandleTree> m_pHandles; 

  friend class OdNwDatabase;
  friend class OdNwHostAppServices;

public:
  ODRX_DECLARE_MEMBERS(OdNwDatabaseImpl);
  OdNwDatabaseImpl();
  virtual ~OdNwDatabaseImpl();

  // OdNwDatabase methods :
  virtual OdNwHostAppServices* appServices() const;
  virtual void readFile(OdStreamBuf* pStreamBuf,
                        bool partialLoad2d = true,
                        const OdString& password = OdString::kEmpty);
  virtual void readFile(const OdString& fileName,
                        bool partialLoad2d = true,
                        const OdString& password = OdString::kEmpty);

  virtual void writeFile(const OdString& fileName);
  virtual void writeFile(OdStreamBuf* pFileBuff);

  virtual OdString getFilename() const;
  virtual OdString getPassword() const;

  OdNwObjectId addObject(OdNwObjectPtr pObj,
                          const OdDbHandle& handle = OdDbHandle());
  OdNwObjectId getObjectId(const OdDbHandle& handle,
                            bool createIfNotFound);
  OdNwObjectId  getNewObjectId(const OdDbHandle& handle = OdDbHandle());

  static OdNwDatabaseImpl* getImpl(const OdNwDatabase* pNwDb);
  static OdNwDatabaseImpl* getImpl(const OdRxObject* pRxDb);

  void addStreamToCache(const NwSubStreamDef& cachedStream);
  const OdArray<NwSubStreamDef>& getCachedStreams() const;

protected:
  void                     setTextureFolder(const OdString& path);
  void                     parseAllStreams();
//protected:
public:
  OdNwViewpointPtr         getCurrentView();
  OdResult                 getSavedViews(std::list<OdNwViewpointPtr> &lSavedViews);//deprecated
  OdResult                 getSavedViewsElements(OdArray<OdNwSavedItemPtr>& aSavedViewsElements);
  OdNwBackgroundElementPtr getBackgroundElement();
  OdNwGeometryCompressSettingsPtr getGeometryCompressSettings();
  void setGeometryCompressSettings(OdNwGeometryCompressSettingsPtr settings);
  OdResult                 getModels(OdArray<OdNwModelPtr>& arrModels);
  OdNwModelItemPtr         getModelItemRoot();
  OdGeMatrix3d             getModelTransform();
  bool                     isComposite() const;
  NwModelUnits::Enum       getUnits() const;
  OdResult                 getLightElements(OdArray<OdNwCommonLightPtr>& arrLights);
  bool                     isPartialLoading() const;
  OdInt16                  getMTMode() const;

private:
  OdString                          m_sFilePathName;
  OdNwHostAppServices*              m_pNwHostAppSvcs;
  OdNwLoadersMap                    m_aLoaders;
  OdNwViewpointPtr                  m_pCurView;
  std::list<OdNwViewpointPtr>       m_lSavedViews;//deprecated
  OdArray<OdNwSavedItemPtr>         m_aSavedViewsElements;
  OdNwBackgroundElementPtr          m_pBackground;
  OdNwGeometryCompressSettingsPtr   m_pGeometryCompressSettings;
  OdArray<OdNwModelPtr>             m_aModels;
  OdString                          m_sTextureFolderPath;
  OdNwModelItemPtr                  m_pModelItemRoot;
  NwModelUnits::Enum                m_ModelUnits;
  OdArray<OdNwCommonLightPtr>       m_aLights;
  OdNwFileLoader                    m_oLoader;
  bool                              m_bPartialLoading;
  OdSharedPtr<tbb::spin_mutex>      m_pMutexHandle;
  OdInt16                           m_MTMode;

  // Cached streams we can't parse or generate
  OdArray<NwSubStreamDef>           m_aCachedStreams;
};

typedef OdSmartPtr<OdNwDatabaseImpl> OdNwDatabaseImplPtr;

#endif // _NWDATABASEIMPL_INCLUDED_
