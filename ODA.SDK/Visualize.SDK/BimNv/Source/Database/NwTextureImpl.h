/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWTEXTUREIMPL_INCLUDED__
#define __NWTEXTUREIMPL_INCLUDED__

#include "NwTexture.h"
#include "NwMaterialImpl.h"
//#include "NwTextureBuf.h"
#include "NwTextureMapper.h"

class OdGiMaterialMap;

class OdNwTextureImpl : public OdNwMaterialImpl
{
public:
  OdNwTextureImpl();
  OdNwTextureImpl(const OdUInt64& position, const OdString& name, const OdString& displayName);
  OdNwTextureImpl(const OdUInt64& position, OdNwMaterialPtr materialToJoin, OdNwTexturePtr textureToJoin);
  virtual ~OdNwTextureImpl();
  ODRX_DECLARE_MEMBERS(OdNwTextureImpl);

  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const;

public:
  static OdNwTextureImpl* getImpl(const OdNwTexture* pNwTexture);
  static OdNwTextureImpl* getImpl(const OdRxObject* pRxTexture);

public:
  static OdResult createTextureImpl(OdNwTexture* pTexture);
  static OdResult createTextureImpl(OdNwTexturePtr pTexture, const OdUInt64& position, const OdString& name = OD_T(""), const OdString& displayName = OD_T(""));
  static OdResult createTextureImpl(OdNwTexturePtr pTexture, const OdUInt64& position, OdNwMaterialPtr materialToJoin, OdNwTexturePtr textureToJoin = OdNwTexturePtr());

public:
  virtual OdNwCategoryPtr createCopy() override;
  virtual OdRxObjectPtr createCategoryObject() override;

public:
  virtual void ParseProperties(OdString texturesSourceDirectory, OdTextureMap* pTextureMap);

public:
  void setTransparency(float value);
  void setRefraction(float value);
  void UpdateMaterialSpecificProperties(OdString tag, OdString type, OdString finish, OdString colorIndex, OdSharedPtr<OdNwColor> customColor);

public:
  OdString getStringValue(NwTextureValueType::Enum type) const;
  double getDoubleValue(NwTextureValueType::Enum type) const;
  NwModelUnits::Enum getUnitValue(NwTextureValueType::Enum type) const;
  OdGiRasterImagePtr getRasterImageValue(NwTextureValueType::Enum type) const;

public:
  OdString getPatternMapPath() const;
  double getPatternMapScaleX() const;
  double getPatternMapScaleY() const;
  OdString getBumpMapPath() const;
  double getBumpMapScaleX() const;
  double getBumpMapScaleY() const;
  OdString getOpacityMapPath() const;
  double getOpacityMapScaleX() const;
  double getOpacityMapScaleY() const;
  OdString getDiffuseMapPath() const;
  double getDiffuseMapScaleX() const;
  double getDiffuseMapScaleY() const;
  double getDiffuseIntensity() const;
  double getSpecularIntensity() const;
  double getReflectIntensity() const;
  OdResult getTint(OdNwColor& tint) const;
  OdSharedPtr<OdNwColor> getTint() const;
  bool IsUseTintColor() const;
  OdResult getTransmitColor(OdNwColor& transmit) const;
  OdSharedPtr<OdNwColor> getTransmitColor() const;
  bool IsColorByObject() const;
  bool isHaveBitmap() const;//deprecated
  OdGiRasterImagePtr getBitmap() const;//deprecated

public:
  OdNwTextureMapper& getBumpMapper();
  OdNwTextureMapper& getPatternMapper();
  OdNwTextureMapper& getDiffuseMapper();
  OdNwTextureMapper& getOpacityMapper();

public:
  void setPatternMapPath(const OdString& path);
  void setPatternMapScaleX(const double& scale);
  void setPatternMapScaleY(const double& scale);
  void setBumpMapPath(const OdString& path);
  void setBumpMapScaleX(const double& scale);
  void setBumpMapScaleY(const double& scale);
  void setOpacityMapPath(const OdString& path);
  void setOpacityMapScaleX(const double& scale);
  void setOpacityMapScaleY(const double& scale);
  void setDiffuseMapPath(const OdString& path);
  void setDiffuseMapScaleX(const double& scale);
  void setDiffuseMapScaleY(const double& scale);
  void setDiffuseIntensity(const double& dif_intensity);
  void setSpecularIntensity(const double& spec_intensity);
  void setReflectIntensity(const double& ref_intensity);
  void setUseTint(bool flag);
  void setTint(OdSharedPtr<OdNwColor> tint);
  void setTransmitColor(OdSharedPtr<OdNwColor> transmit);
  void setColorByObject(bool flag);

private:
  OdNwTextureMapper m_PatternMap;
  OdNwTextureMapper m_BumpMap;
  OdNwTextureMapper m_OpacityMap;
  OdNwTextureMapper m_DiffuseMap;
  double m_DiffuseIntensity;
  double m_SpecularIntensity;
  double m_ReflectIntensity;
  double m_RefractionIndex;
  OdSharedPtr<OdNwColor> m_pTint;
  bool m_bUseTintColor;
  OdSharedPtr<OdNwColor> m_pTransmitColor;
  bool m_bColorByObject;

  //OdNwTextureBufPtr m_pTextureBuf;

  bool m_bTransparencyFromAutodeskMaterial;
};

typedef OdSmartPtr<OdNwTextureImpl> OdNwTextureImplPtr;

#endif //__NWTEXTUREIMPL_INCLUDED__
