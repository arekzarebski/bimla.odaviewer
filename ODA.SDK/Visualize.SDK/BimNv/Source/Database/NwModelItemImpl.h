/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWMODELITEMIMPL_INCLUDED__
#define __NWMODELITEMIMPL_INCLUDED__

#include "NwModelItem.h"
#include "NwItemProperty.h"
#include "OdArray.h"
#define STL_USING_ALGORITHM
#include "OdaSTL.h"

class OdGeMatrix3d;
class OdNwModelItemImpl;
class OdNwDatabaseImpl;
typedef OdSmartPtr<OdNwModelItemImpl> OdNwModelItemImplPtr;
typedef std::map<OdString, OdNwCategoryPtr> OdCategoriesMap;

class OdNwModelItemImpl : public OdNwItemProperty
{
public:
  OdNwModelItemImpl();
  OdNwModelItemImpl(const OdString& name, const OdString& displayName);
  virtual ~OdNwModelItemImpl();
  ODRX_DECLARE_MEMBERS(OdNwModelItemImpl);

  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const;
  virtual bool subWorldDraw(OdGiWorldDraw* pWd) const;
  virtual void subViewportDraw(OdGiViewportDraw* pVd) const;

public:
  static OdNwModelItemImpl* getImpl(const OdNwModelItem* pNwNode);
  static OdNwModelItemImpl* getImpl(const OdRxObject* pRxNode);

public:
  static OdNwModelItemImplPtr createModelItemImpl();
  //return true if PropertiesSet have category with type Transform
  static bool getTransformMatrixCategory(OdNwModelItemImplPtr pModelItemImpl, OdGeMatrix3d& trMat);

public:
  virtual OdNwModelItemPtr createCopy();

public:
  void Add(OdNwCategoryPtr category);

private:
  struct AddRange_ForEachPredicate
  {
    AddRange_ForEachPredicate(OdNwModelItemImpl* pModelItem);
    void operator() (const OdNwCategoryPtr& cat);

    OdNwModelItemImpl* m_pModelItem;
  };

public:
  template <template <class, class> class T>
  void AddRange(const T<OdNwCategoryPtr, std::allocator<OdNwCategoryPtr> > *categories)
  {
    ODA_ASSERT(categories);
    AddRange_ForEachPredicate pred(this);
    pred = std::for_each(categories->begin(), categories->end(), pred);
  }

public:
  //public implementation's methods
  OdGePoint3d getLowBound() const;
  OdGePoint3d getHighBound() const;
  OdResult getAncestors(OdArray<OdNwModelItemPtr>& aAncestors) const;
  OdResult getAncestorsAndSelf(OdArray<OdNwModelItemPtr>& aAncestors) const;
  OdResult getChildren(OdArray<OdNwModelItemPtr>& aChildren) const;
  OdResult getDescendants(OdArray<OdNwModelItemPtr>& aDescendants) const;
  OdResult getDescendantsAndSelf(OdArray<OdNwModelItemPtr>& aDescendants) const;
  bool hasGeometry() const;
  OdNwComponentPtr getGeometryComponent() const;
  OdGUID getInstanceGuid() const;
  OdNwModelItemPtr getParent() const;
  OdResult getPropertyCategories(std::map<OdString, OdNwCategoryPtr>&) const;

public:
  const OdArray<OdNwModelItemPtr>& getChildren() const;
  const OdCategoriesMap& getPropertyCategories() const;

public:
  void setGeometryComponent(OdNwComponentPtr pGeomComp);
  void setInstanceGuid(const OdGUID& guid);
  void setImplOwner(OdNwModelItem* pOwner);
  void setParentId(OdUInt32 rootId);
  OdResult addChildren(OdNwModelItemPtr pChild);
  OdResult addCategories(const OdString& key, OdNwCategoryPtr pCategory);
  OdResult deleteCategories(const OdString& key);
  OdResult getParentId(OdUInt32& rootId) const;

public:
  void setStreamPosition(const OdUInt64 pos);
  void setStreamSize(const OdUInt64 size);

private:
  OdSharedPtr<OdUInt32> m_pParentId;
  OdNwModelItem* m_pParent;
  OdArray<OdNwModelItemPtr> m_aChild;
  OdGUID m_Guid;
  OdNwComponentPtr m_pGeometryComponent;
  OdCategoriesMap m_aCategories;
  OdNwModelItem* m_pImplOwner;

  OdInt64 m_nStreamPosition;
  OdInt64 m_nStreamSize;

};

#endif //__NWMODELITEMIMPL_INCLUDED__
