/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWGEOMETRYCOMPRESSSETTINGSIMPL_H__
#define __NWGEOMETRYCOMPRESSSETTINGSIMPL_H__

#include "NwGeometryCompressSettings.h"
#include "NwObjectImpl.h"

class OdNwGeometryCompressSettingsImpl : public OdNwObjectImpl
{
public:
  OdNwGeometryCompressSettingsImpl();
  virtual ~OdNwGeometryCompressSettingsImpl();
  ODRX_DECLARE_MEMBERS(OdNwGeometryCompressSettingsImpl);

  DrawableType drawableType() const override;
  OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const override;

public:
  static OdNwGeometryCompressSettingsImpl* getImpl(const OdNwGeometryCompressSettings* pNwBck);
  static OdNwGeometryCompressSettingsImpl* getImpl(const OdRxObject* pRxBck);

public:
  void setPrecision(double);
  double getPrecision() const;
  void setGeometryCompressed(bool);
  bool getGeometryCompressed() const;
  void setNormalsPrecisionReduced(bool);
  bool getNormalsPrecisionReduced() const;
  void setColorsPrecisionReduced(bool);
  bool getColorsPrecisionReduced() const;
  void setTextureCoordinatesPecisionReduced(bool);
  bool getTextureCoordinatesPecisionReduced() const;

protected:
  double m_fPrecision;
  bool  m_bGeometryCompressed;
  bool  m_bNormalsPrecisionReduced;
  bool  m_bColorsPrecisionReduced;
  bool  m_bTextureCoordinatesPecisionReduced;
};

typedef OdSmartPtr<OdNwGeometryCompressSettingsImpl> OdNwGeometryCompressSettingsImplPtr;

#endif //__NWGEOMETRYCOMPRESSSETTINGSIMPL_H__
