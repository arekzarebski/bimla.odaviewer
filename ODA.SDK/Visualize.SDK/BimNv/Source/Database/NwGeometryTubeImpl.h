/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWGEOMETRYTUBEIMPL_INCLUDED__
#define __NWGEOMETRYTUBEIMPL_INCLUDED__

#include "NwGeometryTube.h"
#include "NwGeometry.h"
#include "RxObjectImpl.h"
#include "Ge/GeVector3d.h"

class OdNwGeometryTubeImpl;
typedef OdSmartPtr<OdNwGeometryTubeImpl> OdNwGeometryTubeImplPtr;

class OdNwGeometryTubeImpl : public OdRxObjectImpl<OdNwGeometry>
{
public:
  OdNwGeometryTubeImpl();
  virtual ~OdNwGeometryTubeImpl();
  ODRX_DECLARE_MEMBERS(OdNwGeometryTubeImpl);

  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const;
  virtual bool subWorldDraw(OdGiWorldDraw* pWd) const;
  virtual void subViewportDraw(OdGiViewportDraw* pVd) const;

public:
  static OdNwGeometryTubeImpl* getImpl(const OdNwGeometryTube* pNwGeometry);
  static OdNwGeometryTubeImpl* getImpl(const OdRxObject* pRxGeometry);
  static OdNwGeometryTubeImplPtr createGeometry();

public:
  const double& getRadius() const;
  const OdGePoint3d& getTopCenter() const;
  const OdGePoint3d& getBottomCenter() const;
  const OdGeVector3d& getXAxis() const;
  const OdGeVector3d& getYAxis() const;

  void setRadius(const double& val);
  void setTopCenter(const OdGePoint3d& val);
  void setBottomCenter(const OdGePoint3d& val);
  void setXAxis(const OdGeVector3d& val);
  void setYAxis(const OdGeVector3d& val);

protected:
  double m_dRadius;
  OdGePoint3d m_ptTopCenter;
  OdGePoint3d m_ptBottomCenter;
  OdGeVector3d m_vXAxis;
  OdGeVector3d m_vYAxis;
};

#endif //__NWGEOMETRYTUBEIMPL_INCLUDED__
