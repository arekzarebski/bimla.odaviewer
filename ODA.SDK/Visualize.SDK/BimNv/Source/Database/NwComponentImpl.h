/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWCOMPONENTIMPL_INCLUDED__
#define __NWCOMPONENTIMPL_INCLUDED__

#include "NwComponent.h"
#include "OdArray.h"
#include "NwObjectImpl.h"

class OdNwComponentImpl : public OdNwObjectImpl
{
public:
  OdNwComponentImpl();
  virtual ~OdNwComponentImpl();
  ODRX_DECLARE_MEMBERS(OdNwComponentImpl);

  virtual OdUInt32 subSetAttributes(OdGiDrawableTraits* pTraits) const;
  virtual bool subWorldDraw(OdGiWorldDraw* pWd) const;
  virtual void subViewportDraw(OdGiViewportDraw* pVd) const;

  static OdNwComponentImpl* getImpl(const OdNwComponent* pNwComponent);
  static OdNwComponentImpl* getImpl(const OdRxObject* pRxComponent);

public:
  //public implemetation's methods
  OdResult getFragments(OdArray<OdNwFragmentPtr>& aFragments) const;
  bool isHidden() const;
  OdNwMaterialPtr getMaterial() const;
  OdResult getTransparency(float& tr) const;
  OdResult getLowBound(OdGePoint3d& lb) const;
  OdResult getHighBound(OdGePoint3d& hb) const;
  OdUInt32 getFragmentCount() const;
  OdNwFragmentPtr findGeometry(OdUInt32 fragmentIdx) const;

public:
  const OdArray<OdNwFragmentPtr>& getFragments() const;

  void setHiddenFlag(bool hidden);
  void setMaterial(OdNwMaterialPtr pMaterial);
  OdResult addFragment(OdNwFragmentPtr pFrag);

private:
  bool m_bIsHidden;
  OdArray<OdNwFragmentPtr> m_aFragments;
  OdNwMaterialPtr m_pMaterial;
};

typedef OdSmartPtr<OdNwComponentImpl> OdNwComponentImplPtr;

#endif //__NWCOMPONENTIMPL_INCLUDED__
