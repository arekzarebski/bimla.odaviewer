/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __NWVIEWPOINTIMPL_INCLUDED__
#define __NWVIEWPOINTIMPL_INCLUDED__

#include "NwViewpoint.h"
#include "Ge/GeQuaternion.h"
#include "NwObjectImpl.h"

class OdNwViewpointImpl : public OdNwModelView
{
public:
  OdNwViewpointImpl();
  virtual ~OdNwViewpointImpl();
  ODRX_DECLARE_MEMBERS(OdNwViewpointImpl);

  static OdNwViewpointImpl* getImpl(const OdNwViewpoint* pNwView);
  static OdNwViewpointImpl* getImpl(const OdRxObject* pRxView);

public:
  OdString getName() const;
  double getAngularSpeed() const;
  double getFarDistance() const;
  double getFocalDistance() const;
  bool hasAngularSpeed() const;
  bool hasFocalDistance() const;
  bool hasLighting() const;
  bool hasLinearSpeed() const;
  bool hasRenderStyle() const;
  bool hasWorldUpVector() const;
  double getHeightField() const;
  double getHorizontalScale() const;
  NwLightType::Enum getLighting() const;
  double getLinearSpeed() const;
  double getNearDistance() const;
  OdGePoint3d getPosition() const;
  NwViewType::Enum getProjection() const;
  NwModeType::Enum getRenderStyle() const;
  OdGeQuaternion getRotation() const;
  OdString getAvatar() const;
  NwCameraMode::Enum getViewerCameraMode() const;
  OdGeVector3d getWorldUpVector() const;

public:
  void setName(const OdString& name);
  void setAngularSpeed(const double& value);
  void setFarDistance(const double& value);
  void setFocalDistance(const double& value);
  void setHeightField(const double& value);
  void setHorizontalScale(const double& value);
  void setLighting(NwLightType::Enum ligh);
  void setLinearSpeed(const double& value);
  void setNearDistance(const double& value);
  void setPosition(const OdGePoint3d& pos);
  void setProjection(NwViewType::Enum projection);
  void setRenderStyle(NwModeType::Enum render);
  void setRotation(const OdGeQuaternion& rotate);
  void setAvatar(const OdString& value);
  void setViewerCameraMode(NwCameraMode::Enum camera);
  void setWorldUpVector(const OdGeVector3d& up);

public:
  OdUInt32 getFlags() const;

public:
  void setFlags(OdUInt32 value);
  void setViewAspect(const double& value);
  void setVerticalOffset(const double& value);
  void setViewerRadius(const double& value);
  void setViewerHeight(const double& value);
  void setActualHeight(const double& value);
  void setViewerEyeOffset(const double& value);
  void setFirstToThirdAngle(const double& value);
  void setFirstToThirdDistance(const double& value);
  void setFirstToThirdParam(const double& value);
  void setFirstToThirdCorrection(bool correction);
  void setCollisionDetection(bool detection);
  void setGravity(bool gravity);
  void setGravityValue(const double& value);
  void setTerminalVelocity(const double& value);
  void setAutoCrouch(bool auto_cr);
  void setTool(OdUInt32 tool);
  void setFOV(const double& value);

private:
  OdString m_Name;
  OdUInt32 m_flags;

  //CAMERA
  NwViewType::Enum m_Type;
  OdGePoint3d m_Position;
  OdGeQuaternion m_Quaternion;
  double m_VerticalFieldOfView;
  double m_VerticalOffset;
  double m_HorisontalOffset;

  // VIEWER
  double m_ViewerRadius;
  double m_ViewerHeight;
  double m_ActualHeight;
  double m_ViewerEyeOffset;

  // THIRD PERSON
  OdString m_Avatar;
  NwCameraMode::Enum m_CameraMode;
  double m_FirstToThirdAngle;
  double m_FirstToThirdDistance;
  double m_FirstToThirdParam;
  bool m_bFirstToThirdCorrection; // Auto Zoom checkbox

  bool m_bCollisionDetection;
  bool m_bGravity;
  double m_GravityValue;
  double m_TerminalVelocity;
  bool m_bAutoCrouch;

  OdGeVector3d m_Up;
  double m_FocalDistance;
  double m_ViewAspect;
  double m_Near;
  double m_Far;

  double m_LinearSpeed;
  double m_AngularSpeed;

  OdUInt32 m_Tool;
  NwLightType::Enum m_Lighting;
  NwModeType::Enum m_Mode;

  //Define the area of the scene that can be viewed throught the camera (degrees)
  double m_FOV;
};

typedef OdSmartPtr<OdNwViewpointImpl> OdNwViewpointImplPtr;

#endif //__NWVIEWPOINTIMPL_INCLUDED__
