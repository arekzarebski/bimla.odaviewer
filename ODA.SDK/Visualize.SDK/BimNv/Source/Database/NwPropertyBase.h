/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_PROPERTYBASE_H__
#define __TNW_PROPERTYBASE_H__

#include "NwObjectImpl.h"
#include "RxSystemServices.h"

class OdNwPropertyBase : public OdNwObjectImpl
{
public:
  ODRX_DECLARE_MEMBERS(OdNwPropertyBase);

public:
  OdNwPropertyBase();
  virtual ~OdNwPropertyBase();

  virtual OdString ToString() const;

public:
  OdUInt32 getId() const;
  //name for type of item, derived from types in original design application
  OdString getName() const;
  //display name for type of item, derived from types in original design application
  OdString getDisplayName() const;
  OdUInt32 getFlags() const;

public:
  void setId(OdUInt32 id);
  void setName(const OdString& name);
  void setDisplayName(const OdString& displayName);
  void setFlags(OdUInt32 flags);

protected:
  OdUInt32 m_Id;
  OdString m_Name;
  OdString m_DisplayName;
  OdUInt32 m_Flags;
};

typedef OdSmartPtr<OdNwPropertyBase> OdNwPropertyBasePtr;

#endif //__TNW_PROPERTYBASE_H__
