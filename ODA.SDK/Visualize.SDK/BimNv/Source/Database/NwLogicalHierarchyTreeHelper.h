/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __TNW_LOGICALHIERARCHYTREEHELPER_H__
#define __TNW_LOGICALHIERARCHYTREEHELPER_H__

//#include "RxSystemServices.h"
#include "OdStack.h"

struct OdNwLogicalHierarchyNodeInfo
{
  OdNwLogicalHierarchyNodeInfo(OdUInt32 nodeIndex, OdUInt32 childrenCount, OdUInt32 currentChild)
    : m_nodeIndex(nodeIndex)
    , m_childrenCount(childrenCount)
    , m_nextIndex(0)
    , m_currentChild(currentChild)
    , m_position(0)
  {
  }

  OdUInt32 m_nodeIndex;
  OdUInt32 m_childrenCount;
  OdUInt32 m_nextIndex;
  OdUInt32 m_currentChild;
  OdUInt64 m_position;
};

typedef OdStack<OdNwLogicalHierarchyNodeInfo> OdNodeInfoPtrStack;

class OdNwLogicalHierarchyTreeHelper
{
public:
  OdNwLogicalHierarchyTreeHelper();
  virtual ~OdNwLogicalHierarchyTreeHelper();

public:

  void AddDepthLevel(OdUInt32 childrenCount, OdUInt32 index);
  void NodeIsRead(OdUInt32 nextIndex);
  OdUInt32 GetParentId();
  void ReuseStarted();
  void ChildIsProcessed();
  void SetStream(OdStreamBufPtr stream);

public:
  OdUInt32 m_currentNodeIndex;
  OdUInt32 m_currentComponentIndex;
  OdUInt32 m_reusedNodeIndex;
private:
  OdStreamBufPtr m_pStream;//reader
  OdNodeInfoPtrStack m_aTreePosition;
};

#endif //__TNW_LOGICALHIERARCHYTREEHELPER_H__
