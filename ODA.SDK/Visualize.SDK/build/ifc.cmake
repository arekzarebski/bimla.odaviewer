set (IFC_CORE_INCLUDE
	${TKERNEL_CORE_INCLUDE}
)
	
MACRO (IFC_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set ( ${ODA_LIB_NAME}_VER "1" )
ENDMACRO(IFC_LIB)

MACRO (IFC_NO_VER_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
ENDMACRO(IFC_NO_VER_LIB)

MACRO (ifc_project_group TARGET_NAME GROUP)
	if(IFC_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${IFC_PROJECT_GROUP_PREFIX}/${GROUP} )
	else (IFC_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${GROUP} )
	endif(IFC_PROJECT_GROUP_PREFIX)
ENDMACRO(ifc_project_group)

MACRO (IFC_TX_LIB ODA_LIB_VAR ODA_LIB_NAME)
	IFC_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (IFC_TX_LIB)

MACRO (IFC_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
	IFC_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (IFC_COMPONENT_LIB)

MACRO (IFC_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
	IFC_NO_VER_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME} )
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (IFC_LIBRARY_LIB)

if(ODA_SHARED)
	set( IFC_DEFINITIONS -DIFC_DYNAMIC_BUILD -DADT_DYNAMIC_BUILD)
else(ODA_SHARED)
	set( IFC_DEFINITIONS -DIFC_STATIC_BUILD -DADT_STATIC_BUILD)
endif(ODA_SHARED)

MACRO(ifc_tx TARGET_NAME)
    add_definitions(${IFC_DEFINITIONS})
	oda_tx(${TARGET_NAME} ${ARGN})
ENDMACRO(ifc_tx)

MACRO(ifc_component TARGET_NAME)
    add_definitions(${IFC_DEFINITIONS})
	oda_component(${TARGET_NAME} ${ARGN})
ENDMACRO(ifc_component)

MACRO(ifc_tx_component TARGET_NAME)
    add_definitions(${IFC_DEFINITIONS})
	oda_tx_component(${TARGET_NAME} ${ARGN})
ENDMACRO(ifc_tx_component)

MACRO(ifc_library TARGET_NAME)
    add_definitions(${IFC_DEFINITIONS})
	oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(ifc_library)

MACRO(ifc_executable TARGET_NAME)
    add_definitions(${IFC_DEFINITIONS})
	oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(ifc_executable)

MACRO(ifc_sources TARGET_NAME)
	oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(ifc_sources)

MACRO(ifc_module TARGET_NAME)
    add_definitions(${IFC_DEFINITIONS})
    oda_module(${TARGET_NAME} ${ARGN})
ENDMACRO(ifc_module)

MACRO(sdai_txexp TARGET_NAME)
    add_definitions(${IFC_DEFINITIONS})
	oda_txexp(${TARGET_NAME} ${ARGN})
ENDMACRO(sdai_txexp)
