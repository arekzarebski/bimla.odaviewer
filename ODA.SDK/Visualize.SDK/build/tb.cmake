set (TB_CORE_INCLUDE
  ${TKERNEL_CORE_INCLUDE}
  ${TB_ROOT}/Include
)

MACRO (TB_LIB ODA_LIB_VAR ODA_LIB_NAME)
  set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set ( ${ODA_LIB_NAME}_VER "1" )
ENDMACRO(TB_LIB)

MACRO (TB_NO_VER ODA_LIB_VAR ODA_LIB_NAME)
  set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
ENDMACRO(TB_NO_VER)

MACRO (tb_project_group TARGET_NAME GROUP)
  if(TB_PROJECT_GROUP_PREFIX)
    set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${TB_PROJECT_GROUP_PREFIX}/${GROUP} )
  else (TB_PROJECT_GROUP_PREFIX)
    set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${GROUP} )
  endif(TB_PROJECT_GROUP_PREFIX)
ENDMACRO(tb_project_group)

MACRO (TB_TX_LIB ODA_LIB_VAR ODA_LIB_NAME)
  TB_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
  oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TB_TX_LIB)

MACRO (TB_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
  TB_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
  oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TB_COMPONENT_LIB)

MACRO (TB_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
  TB_NO_VER(${ODA_LIB_VAR} ${ODA_LIB_NAME} )
  set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
  oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TB_LIBRARY_LIB)

MACRO(tb_tx TARGET_NAME)
  oda_tx(${TARGET_NAME} ${ARGN})
ENDMACRO(tb_tx)

MACRO(tb_component TARGET_NAME)
  oda_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tb_component)

MACRO(tb_tx_component TARGET_NAME)
  oda_tx_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tb_tx_component)

MACRO(tb_library TARGET_NAME)
  oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(tb_library)

MACRO(tb_executable TARGET_NAME)
  oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(tb_executable)

MACRO(tb_sources TARGET_NAME)
  oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(tb_sources)
