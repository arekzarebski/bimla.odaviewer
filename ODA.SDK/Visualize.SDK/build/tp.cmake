set (TP_CORE_INCLUDE
	${TKERNEL_CORE_INCLUDE}
	${TP_ROOT}/include
)

MACRO (TP_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set ( ${ODA_LIB_NAME}_VER "1" )
ENDMACRO(TP_LIB)

MACRO (TP_NO_VER ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
ENDMACRO(TP_NO_VER)

MACRO (tp_project_group TARGET_NAME GROUP)
	if(TP_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${TP_PROJECT_GROUP_PREFIX}/${GROUP} )
	else (TP_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${GROUP} )
	endif(TP_PROJECT_GROUP_PREFIX)
ENDMACRO(tp_project_group)

MACRO (TP_TX_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TP_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TP_TX_LIB)

MACRO (TP_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TP_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TP_COMPONENT_LIB)

MACRO (TP_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TP_NO_VER(${ODA_LIB_VAR} ${ODA_LIB_NAME} )
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TP_LIBRARY_LIB)

MACRO(tp_tx TARGET_NAME)
	oda_tx(${TARGET_NAME} ${ARGN})
ENDMACRO(tp_tx)

MACRO(tp_component TARGET_NAME)
	oda_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tp_component)

MACRO(tp_tx_component TARGET_NAME)
	oda_tx_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tp_tx_component)

MACRO(tp_library TARGET_NAME)
	oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(tp_library)

MACRO(tp_executable TARGET_NAME)
	oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(tp_executable)

MACRO(tp_sources TARGET_NAME)
	oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(tp_sources)
