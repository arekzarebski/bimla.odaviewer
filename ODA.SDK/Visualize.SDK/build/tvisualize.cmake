set (VISUALIZE_CORE_INCLUDE
  ${TKERNEL_CORE_INCLUDE}
)

MACRO (VISUALIZE_LIB ODA_LIB_VAR ODA_LIB_NAME)
  set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set ( ${ODA_LIB_NAME}_VER "1" )
ENDMACRO(VISUALIZE_LIB)

MACRO (VISUALIZE_NO_VER ODA_LIB_VAR ODA_LIB_NAME)
  set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
ENDMACRO(VISUALIZE_NO_VER )

MACRO (visualize_project_group TARGET_NAME GROUP)
  if(VISUALIZE_PROJECT_GROUP_PREFIX)
    set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${VISUALIZE_PROJECT_GROUP_PREFIX}/${GROUP} )
  else (VISUALIZE_PROJECT_GROUP_PREFIX)
    set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${GROUP} )
  endif(VISUALIZE_PROJECT_GROUP_PREFIX)
ENDMACRO(visualize_project_group)

MACRO (VISUALIZE_TX_LIB ODA_LIB_VAR ODA_LIB_NAME)
  VISUALIZE_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set(${ODA_LIB_NAME}_TYPE .tx)
  oda_define_target_desc(${ODA_LIB_NAME} EXTENSION ${ARGN})
ENDMACRO (VISUALIZE_TX_LIB)

MACRO (VISUALIZE_TXV_LIB ODA_LIB_VAR ODA_LIB_NAME)
  VISUALIZE_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set(${ODA_LIB_NAME}_TYPE .txv)
  oda_define_target_desc(${ODA_LIB_NAME} EXTENSION ${ARGN})
ENDMACRO (VISUALIZE_TXV_LIB)

MACRO (VISUALIZE_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
  VISUALIZE_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
  oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (VISUALIZE_COMPONENT_LIB)

MACRO (VISUALIZE_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
  VISUALIZE_NO_VER(${ODA_LIB_VAR} ${ODA_LIB_NAME} )
  set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
  oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (VISUALIZE_LIBRARY_LIB)

MACRO(visualize_tx TARGET_NAME)
  oda_tx(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_tx)

MACRO(visualize_txv TARGET_NAME)
  oda_txv(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_txv)

MACRO(visualize_component TARGET_NAME)
  oda_component(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_component)

MACRO(visualize_tx_component TARGET_NAME)
  oda_tx_component(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_tx_component)

MACRO(visualize_library TARGET_NAME)
  oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_library)

MACRO(visualize_executable TARGET_NAME)
  oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_executable)

MACRO(visualize_sources TARGET_NAME)
  oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_sources)

MACRO(visualize_module TARGET_NAME)
  oda_module(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_module)

MACRO(visualize_dependencies TARGET_NAME)
  oda_add_dependencies(${TARGET_NAME} ${ARGN})
ENDMACRO(visualize_dependencies)
