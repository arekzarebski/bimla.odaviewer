set (TG_CORE_INCLUDE
	${TKERNEL_CORE_INCLUDE}
        ${TDRAWING_ROOT}/Include
	${TG_ROOT}/include
	${TG_ROOT}/include/DgModelerGeometry
	${TG_ROOT}/include/PSToolkit
	)

MACRO (TG_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set ( ${ODA_LIB_NAME}_VER "1" )
ENDMACRO(TG_LIB)

MACRO (TG_NO_VER ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
ENDMACRO(TG_NO_VER)

MACRO (tg_project_group TARGET_NAME GROUP)
	if(TG_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${TG_PROJECT_GROUP_PREFIX}/${GROUP} )
	else (TG_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${GROUP} )
	endif(TG_PROJECT_GROUP_PREFIX)
ENDMACRO(tg_project_group)

MACRO (TG_TX_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TG_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TG_TX_LIB)

MACRO (TG_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TG_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TG_COMPONENT_LIB)

MACRO (TG_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TG_NO_VER(${ODA_LIB_VAR} ${ODA_LIB_NAME} )
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TG_LIBRARY_LIB)

MACRO(tg_tx TARGET_NAME)
	oda_tx(${TARGET_NAME} ${ARGN})
ENDMACRO(tg_tx)

MACRO(tg_component TARGET_NAME)
	oda_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tg_component)

MACRO(tg_tx_component TARGET_NAME)
	oda_tx_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tg_tx_component)

MACRO(tg_library TARGET_NAME)
	oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(tg_library)

MACRO(tg_executable TARGET_NAME)
	oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(tg_executable)

MACRO(tg_sources TARGET_NAME)
	oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(tg_sources)

MACRO(tg_module TARGET_NAME)
        oda_module(${TARGET_NAME} ${ARGN})
ENDMACRO(tg_module)

