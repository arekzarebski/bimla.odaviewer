set (TCOMPONENTS_CORE_INCLUDE
  ${TKERNEL_CORE_INCLUDE}
)

MACRO (TCOMPONENTS_LIB ODA_LIB_VAR ODA_LIB_NAME)
  set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set ( ${ODA_LIB_NAME}_VER "1" )
ENDMACRO(TCOMPONENTS_LIB)

MACRO (TCOMPONENTS_NO_VER ODA_LIB_VAR ODA_LIB_NAME)
  set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
ENDMACRO(TCOMPONENTS_NO_VER )

MACRO (tcomponents_project_group TARGET_NAME GROUP)
  if(TCOMPONENTS_PROJECT_GROUP_PREFIX)
    set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${TCOMPONENTS_PROJECT_GROUP_PREFIX}/${GROUP} )
  else (TCOMPONENTS_PROJECT_GROUP_PREFIX)
    set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${GROUP} )
  endif(TCOMPONENTS_PROJECT_GROUP_PREFIX)
ENDMACRO(tcomponents_project_group)

MACRO (TCOMPONENTS_TX_LIB ODA_LIB_VAR ODA_LIB_NAME)
  TCOMPONENTS_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set(${ODA_LIB_NAME}_TYPE .tx)
  oda_define_target_desc(${ODA_LIB_NAME} EXTENSION ${ARGN})
ENDMACRO (TCOMPONENTS_TX_LIB)

MACRO (TCOMPONENTS_TXV_LIB ODA_LIB_VAR ODA_LIB_NAME)
  TCOMPONENTS_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set(${ODA_LIB_NAME}_TYPE .txv)
  oda_define_target_desc(${ODA_LIB_NAME} EXTENSION ${ARGN})
ENDMACRO (TCOMPONENTS_TXV_LIB)

MACRO (TCOMPONENTS_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
  TCOMPONENTS_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
  set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
  oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TCOMPONENTS_COMPONENT_LIB)

MACRO (TCOMPONENTS_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
  TCOMPONENTS_NO_VER(${ODA_LIB_VAR} ${ODA_LIB_NAME} )
  set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
  oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TCOMPONENTS_LIBRARY_LIB)

MACRO(tcomponents_tx TARGET_NAME)
  oda_tx(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_tx)

MACRO(tcomponents_txv TARGET_NAME)
  oda_txv(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_txv)

MACRO(tcomponents_component TARGET_NAME)
  oda_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_component)

MACRO(tcomponents_tx_component TARGET_NAME)
  oda_tx_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_tx_component)

MACRO(tcomponents_library TARGET_NAME)
  oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_library)

MACRO(tcomponents_executable TARGET_NAME)
  oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_executable)

MACRO(tcomponents_sources TARGET_NAME)
  oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_sources)

MACRO(tcomponents_module TARGET_NAME)
  oda_module(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_module)

MACRO(tcomponents_dependencies TARGET_NAME)
  oda_add_dependencies(${TARGET_NAME} ${ARGN})
ENDMACRO(tcomponents_dependencies)
