
set (TKERNEL_CORE_INCLUDE
	${TKERNEL_ROOT}/Include
)

if(ODA_PLATFORM_INCLUDE)
include_directories(${ODA_PLATFORM_INCLUDE})
endif(ODA_PLATFORM_INCLUDE)


MACRO (th_project_group TARGET_NAME)
	set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER Thirdparty/${ARGN})
ENDMACRO(th_project_group)


MACRO (TH_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TH_COMPONENT_LIB)

MACRO (TH_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
	set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TH_LIBRARY_LIB)


MACRO(th_component TARGET_NAME)
  oda_component(${TARGET_NAME} ${ARGN})
  if (LINUX_X86 OR LINUX_X64)
    set_property(TARGET ${TARGET_NAME} APPEND_STRING PROPERTY LINK_FLAGS " -Wl,--default-symver -Wl,--strip-all -Wl,--discard-all")
  endif()
ENDMACRO(th_component)

MACRO(th_tx_component TARGET_NAME)
  oda_tx_component(${TARGET_NAME} ${ARGN})
  if (LINUX_X86 OR LINUX_X64)
    set_property(TARGET ${TARGET_NAME} APPEND_STRING PROPERTY LINK_FLAGS " -Wl,--default-symver -Wl,--strip-all -Wl,--discard-all")
  endif()
ENDMACRO(th_tx_component)

MACRO(th_library TARGET_NAME)
	oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(th_library)


MACRO(th_sources TARGET_NAME)
	oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(th_sources)

MACRO(th_executable TARGET_NAME)
    add_definitions(${TH_DEFINITIONS})
    oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(th_executable)
