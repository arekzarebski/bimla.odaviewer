set (TF_CORE_INCLUDE
	${TKERNEL_CORE_INCLUDE}
	${TDRAWING_ROOT}/Include
	${TA_ROOT}/include
	${TA_ROOT}/includeimp
	${TF_ROOT}/include
	${TF_ROOT}/includeimp
	)

MACRO (TF_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set ( ${ODA_LIB_NAME}_VER "1" )
ENDMACRO(TF_LIB)

MACRO (TF_NO_VER_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
ENDMACRO(TF_NO_VER_LIB)

MACRO (tf_project_group TARGET_NAME GROUP)
	if(TF_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${TF_PROJECT_GROUP_PREFIX}/${GROUP} )
	else (TF_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${GROUP} )
	endif(TF_PROJECT_GROUP_PREFIX)
ENDMACRO(tf_project_group)

MACRO (TF_TX_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TF_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TF_TX_LIB)

MACRO (TF_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TF_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TF_COMPONENT_LIB)

MACRO (TF_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TF_NO_VER_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME} )
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TF_LIBRARY_LIB)

if(ODA_SHARED)
set( TF_DEFINITIONS -DADT_DYNAMIC_BUILD )
else(ODA_SHARED)
set( TF_DEFINITIONS -DADT_STATIC_BUILD )
endif(ODA_SHARED)

MACRO(tf_tx TARGET_NAME)
    add_definitions(${TF_DEFINITIONS})
	oda_tx(${TARGET_NAME} ${ARGN})
ENDMACRO(tf_tx)

MACRO(tf_component TARGET_NAME)
    add_definitions(${TF_DEFINITIONS})
	oda_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tf_component)

MACRO(tf_library TARGET_NAME)
    add_definitions(${TF_DEFINITIONS})
	oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(tf_library)

MACRO(tf_executable TARGET_NAME)
    add_definitions(${TF_DEFINITIONS})
	oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(tf_executable)

MACRO(tf_sources TARGET_NAME)
	oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(tf_sources)
