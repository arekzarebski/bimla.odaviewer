set (TDRAWING_CORE_INCLUDE ${TDRAWING_ROOT}/Include)

if(ODA_PLATFORM_INCLUDE)
include_directories(${ODA_PLATFORM_INCLUDE})
endif(ODA_PLATFORM_INCLUDE)

# Define manifest generator executable depending on platform type
if(WIN64 AND MSVC_VERSION GREATER 1400)
  set( MG_BIN mg64 )
else(WIN64 AND MSVC_VERSION GREATER 1400)
  set( MG_BIN mg )
endif(WIN64 AND MSVC_VERSION GREATER 1400)

MACRO (TDRAWING_LIB ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set ( ${ODA_LIB_NAME}_VER "1" )
ENDMACRO(TDRAWING_LIB)

MACRO (TDRAWING_NO_VER ODA_LIB_VAR ODA_LIB_NAME)
	set ( ${ODA_LIB_VAR} ${ODA_LIB_NAME} )
ENDMACRO(TDRAWING_NO_VER )

MACRO (tdrawing_project_group TARGET_NAME GROUP)
	if(TDRAWING_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${TDRAWING_PROJECT_GROUP_PREFIX}/${GROUP} )
	else (TDRAWING_PROJECT_GROUP_PREFIX)
		set_target_properties ( ${TARGET_NAME} PROPERTIES FOLDER ${GROUP} )
	endif(TDRAWING_PROJECT_GROUP_PREFIX)
ENDMACRO(tdrawing_project_group)

MACRO (TDRAWING_TX_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TDRAWING_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set(${ODA_LIB_NAME}_TYPE .tx)
	oda_define_target_desc(${ODA_LIB_NAME} EXTENSION ${ARGN})
ENDMACRO (TDRAWING_TX_LIB)

MACRO (TDRAWING_TXV_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TDRAWING_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set(${ODA_LIB_NAME}_TYPE .txv)
	oda_define_target_desc(${ODA_LIB_NAME} EXTENSION ${ARGN})
ENDMACRO (TDRAWING_TXV_LIB)

MACRO (TDRAWING_COMPONENT_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TDRAWING_LIB(${ODA_LIB_VAR} ${ODA_LIB_NAME})
	set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TDRAWING_COMPONENT_LIB)

MACRO (TDRAWING_LIBRARY_LIB ODA_LIB_VAR ODA_LIB_NAME)
	TDRAWING_NO_VER(${ODA_LIB_VAR} ${ODA_LIB_NAME} )
	set(${ODA_LIB_NAME}_TYPE ${CMAKE_SHARED_LIBRARY_SUFFIX})
	oda_define_target_desc(${ODA_LIB_NAME} ${ARGN})
ENDMACRO (TDRAWING_LIBRARY_LIB)

MACRO(tdrawing_tx TARGET_NAME)
        include_directories(${TDRAWING_CORE_INCLUDE})
	oda_tx(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_tx)

MACRO(tdrawing_txv TARGET_NAME)
        include_directories(${TDRAWING_CORE_INCLUDE})
	oda_txv(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_txv)

MACRO(tdrawing_component TARGET_NAME)
        include_directories(${TDRAWING_CORE_INCLUDE})
	oda_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_component)

MACRO(tdrawing_tx_component TARGET_NAME)
        include_directories(${TDRAWING_CORE_INCLUDE})
	oda_tx_component(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_tx_component)

MACRO(tdrawing_library TARGET_NAME)
        include_directories(${TDRAWING_CORE_INCLUDE})
	oda_library(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_library)

MACRO(tdrawing_executable TARGET_NAME)
        include_directories(${TDRAWING_CORE_INCLUDE})
	oda_executable(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_executable)

MACRO(tdrawing_sources TARGET_NAME)
	oda_sources(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_sources)

MACRO(tdrawing_module TARGET_NAME)
        oda_module(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_module)

MACRO(tdrawing_dependencies TARGET_NAME)
  oda_add_dependencies(${TARGET_NAME} ${ARGN})
ENDMACRO(tdrawing_dependencies)

MACRO(tdx_postbuild_event TARGET_NAME SKIP_INTERFACES)
if(MSVC)
if (${SKIP_INTERFACES})
	add_custom_command ( TARGET ${TARGET_NAME} POST_BUILD COMMAND "${TDRAWING_ROOT_W}\\..\\build\\${MG_BIN}.exe" "${ODA_BIN_DIR_W}\\${TARGET_NAME}${ODA_VER}.dll" "${ODA_BIN_DIR_W}\\OdaX${ODA_VER}.dll")
else(${SKIP_INTERFACES})
  add_custom_command ( TARGET ${TARGET_NAME} POST_BUILD COMMAND "${TDRAWING_ROOT_W}\\..\\build\\${MG_BIN}.exe" "${ODA_BIN_DIR_W}\\${TARGET_NAME}${ODA_VER}.dll")
endif(${SKIP_INTERFACES})
endif(MSVC)
ENDMACRO(tdx_postbuild_event)


MACRO(oda_qt_set_definition TARGET_NAME ADDITIONAL_DEFINITION)
if (ANDROID)
  set (QTDEF "ANDROID;ANDROID_ON;TD_OPENGL_ES;TD_CLIENT_BUILD;QT_OPENGL_ES_2;_REENTRANT;" ${ADDITIONAL_DEFINITION})
  set_target_properties (${TARGET_NAME} PROPERTIES COMPILE_DEFINITIONS "${QTDEF}")
endif (ANDROID)
ENDMACRO(oda_qt_set_definition TARGET_NAME ADDITIONAL_DEFINITION)


MACRO(oda_qt_set_cxxflags TARGET_NAME ADDITIONAL_FLAGS)
if (ANDROID)
  set (FLAGS "-std=gnu++0x -fPIC -D_REENTRANT -Wno-psabi" ${ADDITIONAL_FLAGS})
  set_target_properties (${TARGET_NAME} PROPERTIES COMPILE_FLAGS "${FLAGS}")
endif (ANDROID)
ENDMACRO(oda_qt_set_cxxflags TARGET_NAME ADDITIONAL_DEFINITION)


MACRO(oda_qt_create_apk TARGET_NAME)
#Create Android APK
if(ANDROID)
  # create json file parsed by the androiddeployqt
  if("${ANDROID_SDK}" STREQUAL "")
    message("-- ANDROID_SDK environment variable is not set. APK file build is disabled")
  else()
    if ("${ANDROID_NDK}" STREQUAL "")
      message("-- ANDROID_NDK environment variable is not set. APK file build is disabled")
    else()
      if ("${ANT_HOME}" STREQUAL "")
        message("-- ANT_HOME environment variable is not set. APK file build is disabled")
      else()
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/data/android-lib${TARGET_NAME}.so-deployment-settings.json.cmake ${CMAKE_CURRENT_BINARY_DIR}/android-lib${TARGET_NAME}.so-deployment-settings.json @ONLY)
        add_custom_command (TARGET ${TARGET_NAME} POST_BUILD COMMAND mkdir -p "${CMAKE_CURRENT_BINARY_DIR}/android-build/libs/${ODA_ANDROID_ARCH}")
        add_custom_command (TARGET ${TARGET_NAME} POST_BUILD COMMAND cp "${ODA_BIN_DIR}/lib${TARGET_NAME}.so" "${CMAKE_CURRENT_BINARY_DIR}/android-build/libs/${ODA_ANDROID_ARCH}/")
        add_custom_command (TARGET ${TARGET_NAME} POST_BUILD COMMAND ${QTDIR_PATH}/bin/androiddeployqt --input ${CMAKE_CURRENT_BINARY_DIR}/android-lib${TARGET_NAME}.so-deployment-settings.json --output ${CMAKE_CURRENT_BINARY_DIR}/android-build/ --deployment bundled --ant ${ANT_HOME}/bin/ant.bat)
        add_custom_command (TARGET ${TARGET_NAME} POST_BUILD COMMAND cp "${CMAKE_CURRENT_BINARY_DIR}/android-build/bin/QtApp-debug.apk" "${ODA_BIN_DIR}/${TARGET_NAME}.apk")
      endif()
    endif()
  endif()
endif(ANDROID)
ENDMACRO(oda_qt_create_apk TARGET_NAME)


MACRO(qt_app_postbuild_event TARGET_NAME SKIP_INTERFACES)

if (${SKIP_INTERFACES})
	add_custom_command ( TARGET ${TARGET_NAME} POST_BUILD COMMAND "${TDRAWING_ROOT_W}\\..\\build\\${MG_BIN}" "${ODA_BIN_DIR_W}\\${TARGET_NAME}${ODA_VER}.dll" "${ODA_BIN_DIR}\\OdaX${ODA_VER}.dll")
else(${SKIP_INTERFACES})
  add_custom_command ( TARGET ${TARGET_NAME} POST_BUILD COMMAND "${TDRAWING_ROOT_W}\\..\\build\\${MG_BIN}" "${ODA_BIN_DIR_W}\\${TARGET_NAME}${ODA_VER}.dll")
endif(${SKIP_INTERFACES})
	add_custom_command ( TARGET ${TARGET_NAME} POST_BUILD COMMAND copy /Y "${TDRAWING_ROOT_W}\\ActiveX\\TDXdependency.manifest" "${ODA_BIN_DIR_W}" )

ENDMACRO(qt_app_postbuild_event)

MACRO(oda_set_macos_bundle_properties TARGET_NAME ICON_FILE_NAME ICON_FILE_PATH)
  set(MACOSX_BUNDLE_ICON_FILE "${ICON_FILE_NAME}")
  set(MACOSX_BUNDLE_RESOURCES "${ODA_BIN_DIR}/${TARGET_NAME}.app/Contents/Resources")
  set(MACOSX_BUNDLE_ICON "${ICON_FILE_PATH}/${MACOSX_BUNDLE_ICON_FILE}.icns")
  execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${MACOSX_BUNDLE_RESOURCES})
  execute_process(COMMAND ${CMAKE_COMMAND} -E copy_if_different ${MACOSX_BUNDLE_ICON} ${MACOSX_BUNDLE_RESOURCES})

  set_target_properties(${TARGET_NAME} PROPERTIES
     MACOSX_BUNDLE 1
     MACOSX_BUNDLE_GUI_IDENTIFIER "${TARGET_NAME}"
     MACOSX_BUNDLE_LONG_VERSION_STRING "${TARGET_NAME} Version ${ODA_PROD_VER}"
     MACOSX_BUNDLE_BUNDLE_NAME "${TARGET_NAME}"
     MACOSX_BUNDLE_SHORT_VERSION_STRING "${ODA_PROD_VER}"
     MACOSX_BUNDLE_BUNDLE_VERSION "${ODA_PROD_VER}"
     MACOSX_BUNDLE_COPYRIGHT "(C) 2003-2015 by Open Design Alliance"
     MACOSX_BUNDLE_INFO_STRING "${TARGET_NAME} ${ODA_PROD_VER}, Copyright (C) 2003-2015 by Open Design Alliance"
   )
ENDMACRO(oda_set_macos_bundle_properties)
