/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _MODELERGEOMETRYINTERNAL_INCLUDED_
#define _MODELERGEOMETRYINTERNAL_INCLUDED_

//enum EdgeVisibility
//  {
//    kTangentOn       = 0,
//    kTangentOff      = (0x1 << 0),
//    kTangentShortens = (0x1 << 1),
//    kInterfrenceOff  = (0x1 << 2),
//    kBend            = (0x1 << 3),
//    kThread          = (0x1 << 4),
//    kPresentation    = (0x1 << 5),
//  };
inline OdModelerGeometryPtr getModelerGeometry(const OdDbEntity* pEnt)
{
  OdModelerGeometryPtr pRes;
  if (OdDb3dSolid* pSolid = OdDb3dSolid::cast(pEnt))
    pRes = (OdRxObject*)pSolid->body();
  else if (OdDbRegion* pReg = OdDbRegion::cast(pEnt))
    pRes = (OdRxObject*)pReg->body();
  else if (OdDbBody* pBody = OdDbBody::cast(pEnt))
    pRes = (OdRxObject*)pBody->body();
  else if (OdDbSurface* pSrf = OdDbSurface::cast(pEnt))
    pRes = (OdRxObject*)pSrf->body();
  return pRes;
}

inline void setModelerGeometry(OdDbEntity* pEnt, const OdModelerGeometry* pGeom)
{
  ODA_ASSERT(pEnt != NULL && pGeom != NULL);

  if (OdDb3dSolid* pSolid = OdDb3dSolid::cast(pEnt))
    pSolid->setBody(pGeom);
  else if (OdDbRegion* pReg = OdDbRegion::cast(pEnt))
    pReg->setBody(pGeom);
  else if (OdDbBody* pBody = OdDbBody::cast(pEnt))
    pBody->setBody(pGeom);
  else if (OdDbSurface* pSrf = OdDbSurface::cast(pEnt))
    pSrf->setBody(pGeom);
}

#endif //_MODELERGEOMETRYINTERNAL_INCLUDED_
