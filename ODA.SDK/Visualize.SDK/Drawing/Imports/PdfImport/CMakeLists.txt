#
# TD_PdfImport library
#
if(NOT WINCE AND NOT WINRT)

tdrawing_sources(${TD_PDF_IMPORT_LIB}
    Include/PdfImport.h
    Include/PdfImportEx.h
    Source/PdfImportImpl.cpp
    Source/PdfImportImpl.h
    Source/PdfProperties.h
    Source/DbProgressMeterGrabber.h
    Source/DbProgressMeterGrabber.cpp
    Source/DbBasePdfImportObjectGrabber.h
    Source/DbBasePdfImportObjectGrabber.cpp
    Source/DbPdfImportObjectGrabber.h
    Source/DbPdfImportObjectGrabber.cpp
    Source/BaseFitter.h
    Source/Gaussian.h
    Source/MathMatrix.h
    Source/MathVector.h
    Source/ProxyMatrix.h
    Source/BaseFitter.cpp
    Source/ShapeFitters.h
    Source/ShapeFitters.cpp
    Source/PdfImportCmd.cpp
    Source/PdfImportCmd.h
    )
			  
include_directories(
    Include
    ${TKERNEL_ROOT}/Imports/PdfImport/Include
    ${TKERNEL_ROOT}/Imports/PdfImport/Source
    ${TH_ROOT}/pdfium
    ${TH_ROOT}/pdfium/third_party
    ${TH_ROOT}/freetype/freetype-${FREETYPE_VER}/include
    ${TH_ROOT}/freetype/freetype-${FREETYPE_VER}/include/freetype
    ${TDRAWING_ROOT}/Imports/ImportBase
    )

if(ODA_SHARED AND MSVC)
tdrawing_sources(${TD_PDF_IMPORT_LIB}
				  Source/PdfImport.rc
				  )
endif(ODA_SHARED AND MSVC)

add_definitions(-DPDFEXPORT_EXPORTS)

if(ODA_SHARED)
add_definitions(-DPDFIMPORT_EXPORTS)
add_definitions(-DFPDFSDK_EXPORTS)
else(ODA_SHARED)
add_definitions()
endif(ODA_SHARED)

IF(WIN32)
add_definitions(-DNOMINMAX)
ENDIF()

# PDFium definitions
include(${TH_ROOT}/pdfium/definitions.txt)

set (pdfium_libs ${TH_FT_LIB} ${TH_THIRDPARTYRASTER_LIB} ${TH_ZLIB_LIB})

#if(ODA_SHARED)
tdrawing_tx(${TD_PDF_IMPORT_LIB} ${TD_GS_LIB} ${TD_DB_LIB} ${TD_DBROOT_LIB} ${TD_GI_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TH_PDFIUM_LIB} ${pdfium_libs} ${TD_ALLOC_LIB})
#endif(ODA_SHARED)

tdrawing_project_group(${TD_PDF_IMPORT_LIB} "Imports")

set_property(TARGET ${TD_PDF_IMPORT_LIB} APPEND_STRING PROPERTY COMPILE_FLAGS " -std=c++11")

endif(NOT WINCE AND NOT WINRT)
