/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#if defined(_MSC_VER)
// warning C4290: C++ Exception Specification ignored
#pragma warning ( disable : 4290 )
#if _MSC_VER <= 1200
// compiler stack overflows if these warning is enabled
#pragma warning ( disable : 4786)
#endif
#endif

#include "OdaCommon.h"
#include "RxDispatchImpl.h"
#include "RxVariantValue.h"
#include "DbDictionary.h"
#include "RxDynamicModule.h"
#include "Ed/EdCommandStack.h"
#include "Ed/EdCommandContext.h"
#include "Ed/EdUserIO.h"
#include "DbCommandContext.h"
#include "StaticRxObject.h"
#include "PdfProperties.h"
#include "PdfImportImpl.h"
#include "DbDatabase.h"
#include "DbHostAppServices.h"
#include "DbBlockTableRecord.h"
#include "DbUnderlayReference.h"
#include "DbUnderlayDefinition.h"
#include "Ge/GeScale3d.h"
#include "OdPath.h"
#include "PdfImportCmd.h"

#include "DbPdfImportObjectGrabber.h"
#include "DbProgressMeterGrabber.h"

#include <memory>
#include <functional>
#include "OdPlatform.h"


OdRxDictionaryPtr PdfProperties::createObject()
{
  return OdRxObjectImpl<PdfProperties, OdRxDictionary>::createObject();
}


ODRX_DECLARE_PROPERTY(PdfPath)
ODRX_DECLARE_PROPERTY(Password)
ODRX_DECLARE_PROPERTY(Database)
ODRX_DECLARE_PROPERTY(PageNumber)
ODRX_DECLARE_PROPERTY(LayersUseType)
ODRX_DECLARE_PROPERTY(ImportVectorGeometry)
ODRX_DECLARE_PROPERTY(ImportSolidFills)
ODRX_DECLARE_PROPERTY(ImportTrueTypeText)
ODRX_DECLARE_PROPERTY(ImportRasterImages)
ODRX_DECLARE_PROPERTY(ImportAsBlock)
ODRX_DECLARE_PROPERTY(JoinLineAndArcSegments)
ODRX_DECLARE_PROPERTY(ConvertSolidsToHatches)
ODRX_DECLARE_PROPERTY(ApplyLineweight)
ODRX_DECLARE_PROPERTY(InsertionPointX)
ODRX_DECLARE_PROPERTY(InsertionPointY)
ODRX_DECLARE_PROPERTY(Scaling)
ODRX_DECLARE_PROPERTY(Rotation)
ODRX_DECLARE_PROPERTY(ImportTrueTypeTextAsGeometry)
ODRX_DECLARE_PROPERTY(ImportGradientFills)
ODRX_DECLARE_PROPERTY(UseProgressMeter)
ODRX_DECLARE_PROPERTY(UseGeometryOptimization)
ODRX_DECLARE_PROPERTY(ImagePath)
ODRX_DECLARE_PROPERTY(ImportType3AsTrueTypeText)
ODRX_DECLARE_PROPERTY(UseRgbColor)


ODRX_DEFINE_PROPERTY(PdfPath, PdfProperties, getString)
ODRX_DEFINE_PROPERTY(Password, PdfProperties, getString)
ODRX_DEFINE_PROPERTY(PageNumber, PdfProperties, getInt32)
ODRX_DEFINE_PROPERTY_OBJECT(Database, PdfProperties,  get_Database, put_Database, OdDbDatabase)
ODRX_DEFINE_PROPERTY(LayersUseType, PdfProperties, getUInt8)
ODRX_DEFINE_PROPERTY(ImportVectorGeometry, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(ImportSolidFills, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(ImportTrueTypeText, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(ImportRasterImages, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(ImportAsBlock, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(JoinLineAndArcSegments, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(ConvertSolidsToHatches, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(ApplyLineweight, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(InsertionPointX, PdfProperties, getDouble)
ODRX_DEFINE_PROPERTY(InsertionPointY, PdfProperties, getDouble)
ODRX_DEFINE_PROPERTY(Scaling, PdfProperties, getDouble)
ODRX_DEFINE_PROPERTY(Rotation, PdfProperties, getDouble)
ODRX_DEFINE_PROPERTY(ImportTrueTypeTextAsGeometry, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(ImportGradientFills, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(UseProgressMeter, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(UseGeometryOptimization, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(ImagePath, PdfProperties, getString)
ODRX_DEFINE_PROPERTY(ImportType3AsTrueTypeText, PdfProperties, getBool)
ODRX_DEFINE_PROPERTY(UseRgbColor, PdfProperties, getBool)

ODRX_BEGIN_DYNAMIC_PROPERTY_MAP( PdfProperties );
  ODRX_GENERATE_PROPERTY( PdfPath )
  ODRX_GENERATE_PROPERTY( Password )
  ODRX_GENERATE_PROPERTY( Database )
  ODRX_GENERATE_PROPERTY(PageNumber)
  ODRX_GENERATE_PROPERTY(LayersUseType)
  ODRX_GENERATE_PROPERTY(ImportVectorGeometry)
  ODRX_GENERATE_PROPERTY(ImportSolidFills)
  ODRX_GENERATE_PROPERTY(ImportTrueTypeText)
  ODRX_GENERATE_PROPERTY(ImportRasterImages)
  ODRX_GENERATE_PROPERTY(ImportAsBlock)
  ODRX_GENERATE_PROPERTY(JoinLineAndArcSegments)
  ODRX_GENERATE_PROPERTY(ConvertSolidsToHatches)
  ODRX_GENERATE_PROPERTY(ApplyLineweight)
  ODRX_GENERATE_PROPERTY(InsertionPointX)
  ODRX_GENERATE_PROPERTY(InsertionPointY)
  ODRX_GENERATE_PROPERTY(Scaling)
  ODRX_GENERATE_PROPERTY(Rotation)
  ODRX_GENERATE_PROPERTY(ImportTrueTypeTextAsGeometry)
  ODRX_GENERATE_PROPERTY(ImportGradientFills)
  ODRX_GENERATE_PROPERTY(UseProgressMeter)
  ODRX_GENERATE_PROPERTY(UseGeometryOptimization)
  ODRX_GENERATE_PROPERTY(ImagePath)
  ODRX_GENERATE_PROPERTY(ImportType3AsTrueTypeText)
  ODRX_GENERATE_PROPERTY(UseRgbColor)
ODRX_END_DYNAMIC_PROPERTY_MAP( PdfProperties );

#if defined(_MSC_VER) && (_MSC_VER >= 1300)
#pragma warning(disable:4355)
#endif

PdfImporter::PdfImporter() 
  : m_pProperties( PdfProperties::createObject() )
  , m_pDocument(nullptr)
  , m_Page(nullptr)
  , m_PageNum(-1)
  , m_isClipBoundaryInverted(false)
{
}

PdfImporter::~PdfImporter()
{
  if (isPageLoaded())
  {
    FPDF_ClosePage(m_Page);
  }
  if (isDocumentLoaded())
  {
    FPDF_CloseDocument(m_pDocument);
  }
}

OdRxDictionaryPtr PdfImporter::properties() { return m_pProperties; }

#define TOCHAR(a) ((char *)OdAnsiString(OdString(a)).c_str())

double PdfImporter::getPageWidth() const
{
  return FPDF_GetPageWidth(m_Page);
}

void PdfImporter::setClipBoundary(const OdGePoint2dArray& clipBoundary, const bool is_clip_boundary_inverted)
{
  m_ClipBoundary = clipBoundary;
  m_isClipBoundaryInverted = is_clip_boundary_inverted;
}

void PdfImporter::setLayersInfo(std::map<std::wstring, bool>& layers_info)
{
  m_LayersInfo = layers_info;
}

void PdfImporter::clearUnderlayInfo()
{
  m_ClipBoundary.clear();
  m_LayersInfo.clear();
  m_isClipBoundaryInverted = false;
}

void PdfImporter::getImportedObjectsInfo(size_t& object_count, size_t& error_count)
{
  if (m_Import_info)
  {
    object_count = m_Import_info->m_ObjectCounter;
    error_count = m_Import_info->m_ErrorCounter;
  }
  else
  {
    object_count = 0;
    error_count = 0;
  }
}

double PdfImporter::getPageHeight() const
{
  return FPDF_GetPageHeight(m_Page);
}

double PdfImporter::getMeasureDictInfo()
{
  double xScale = FPDF_GetMeasureDictInfo(m_pDocument, m_PageNum);

  return (xScale == 0.) ? 1. : (xScale)*72.0;
}

OdPdfImport::ImportResult PdfImporter::fillLayersInfo(std::map<std::wstring, bool>& layers_info)
{

  FPDF_ARRAY pONLayers = 0, pOFFLayers = 0, pLayers = 0;
  if (!FPDF_LoadLayers(m_pDocument, pLayers, pONLayers, pOFFLayers))
    return OdPdfImport::fail;

  if (!FPDF_GetPageLayers(m_pDocument, m_Page, m_pProperties->get_PageNumber() - 1, m_PageInfo))
    return OdPdfImport::fail;

  for (int i = 0; i < m_PageInfo.nol; ++i)
  {
    layers_info.emplace(m_PageInfo.names[i], FPDF_IsLayerEnabled(pOFFLayers, m_PageInfo.names[i]));
  }

  return OdPdfImport::success;
}

OdPdfImport::ImportResult PdfImporter::import()
{
  OdPdfImport::ImportResult result(OdPdfImport::success);
  try
  {
    result = loadDocument();
    if (OdPdfImport::success != result)
      return result;
    result = loadPage();
    if (OdPdfImport::success != result)
      return result;

    m_Import_info = std::make_shared<PdfImportInfoHolder>(result);

    OdRxDictionaryIteratorPtr pIt = m_pProperties->newIterator();
    while (!pIt->done())
    {
      m_Import_info->m_pProperties->putAt(pIt->getKey(), pIt->object());
      pIt->next();
    }

    OdDbDatabasePtr pDb = OdDbDatabasePtr(m_Import_info->m_pProperties->get_Database());

    OdString image_path = m_Import_info->m_pProperties->get_ImagePath();
    if (image_path.isEmpty())
      image_path =  pDb->appServices()->getPDFIMPORTIMAGEPATH();
    if(image_path.isEmpty())
      image_path = pDb->appServices()->getTempPath();
    if (0 != image_path.getLength() && image_path[image_path.getLength() - 1] != pathChar)
      image_path += pathChar;

    const double measure = getMeasureDictInfo();
    m_Import_info->m_pProperties->put_Scaling(m_pProperties->get_Scaling() * measure);

    m_Import_info->m_Width = getPageWidth();
    m_Import_info->m_Height = -getPageHeight();

    std::map<std::wstring, bool> layers_info(m_LayersInfo);
    result = fillLayersInfo(layers_info);
    if (OdPdfImport::success != result)
      return result;

    OdGePoint2dArray clip_boundary(m_ClipBoundary);
    for(auto& elem : clip_boundary)
      elem /= measure;

    if(m_Import_info->m_pProperties->get_UseProgressMeter())
    {
      auto pGraber = std::make_shared<DbProgressMeterGrabber>(m_Import_info);

      FPDF_ImportPage(m_Page, pGraber.get(), m_Import_info->m_pProperties->get_ImportTrueTypeTextAsGeometry(), 
        m_Import_info->m_pProperties->get_ImportType3AsTrueTypeText());

      if (0 == m_Import_info->m_ObjectCounter)
        return OdPdfImport::no_objects_imported;

      m_Import_info->InitProgressMeter();
      m_Import_info->ProgressMeterSetLimit();
      OdString cmd_line(OD_T("Importing pdf file..."));
      m_Import_info->StartProgressMeter(cmd_line);
    }

    auto pGraber = std::make_shared<OdDbPdfImportObjectGrabber>(m_Import_info, layers_info, clip_boundary, m_isClipBoundaryInverted, image_path);

    FPDF_ImportPage(m_Page, pGraber.get(), m_Import_info->m_pProperties->get_ImportTrueTypeTextAsGeometry(),
      m_Import_info->m_pProperties->get_ImportType3AsTrueTypeText());

    if (m_Import_info->m_pProperties->get_UseProgressMeter())
    {
      m_Import_info->StopProgressMeter();
    }

    result = m_Import_info->m_ImageError;
    if (0 == m_Import_info->m_ObjectCounter || m_Import_info->m_ObjectCounter == m_Import_info->m_ErrorCounter)
      result = OdPdfImport::no_objects_imported;
  }
  catch (...)
  {
    return OdPdfImport::fail;
  }

  return result;
}

OdPdfImport::ImportResult PdfImporter::loadDocument()
{
  if (isDocumentLoaded())
    return OdPdfImport::success;

  OdString path(m_pProperties->get_PdfPath());

  OdString password = m_pProperties->get_Password();

  m_pDocument = FPDF_LoadDocument(path.c_str(), TOCHAR(password.c_str()));

  if (nullptr == m_pDocument)
  {
    unsigned long error = FPDF_GetLastError();
    if (FPDF_ERR_PASSWORD == error)
      return OdPdfImport::bad_password;
    return OdPdfImport::fail;
  }
  return OdPdfImport::success;
}

bool PdfImporter::isDocumentLoaded() const
{
  return (nullptr != m_pDocument);
}

OdPdfImport::ImportResult PdfImporter::loadPage()
{
  if (isPageLoaded())
    return OdPdfImport::success;
  if (!isDocumentLoaded())
    return OdPdfImport::fail;

  m_PageNum = m_pProperties->get_PageNumber() - 1;

  if (m_PageNum >= getPageCount())
    return OdPdfImport::invalid_page_number;

  m_Page = FPDF_LoadPage(m_pDocument, m_PageNum);

  if (nullptr == m_Page)
    return OdPdfImport::fail;

  return OdPdfImport::success;
}

bool PdfImporter::isPageLoaded() const
{
  return (nullptr != m_Page);
}

OdUInt32 PdfImporter::getPageCount() const
{
  if (!isDocumentLoaded())
    return 0;
  return FPDF_GetPageCount(m_pDocument);
}

class PdfImportModule : public OdPdfImportModule
{
  OdStaticRxObject<PdfImportCommand> m_Command;
public:
  virtual void initApp()
  {
    FPDF_InitLibrary();
    odedRegCmds()->addCommand( &m_Command );
  }
  virtual void uninitApp()
  {
    FPDF_DestroyLibrary();
    odedRegCmds()->removeCmd( m_Command.groupName(), m_Command.globalName() );
  }
  
  virtual OdPdfImportPtr create ()
  {
      return OdRxObjectImpl<PdfImporter>::createObject();
  }

};

ODRX_DEFINE_DYNAMIC_MODULE(PdfImportModule)
