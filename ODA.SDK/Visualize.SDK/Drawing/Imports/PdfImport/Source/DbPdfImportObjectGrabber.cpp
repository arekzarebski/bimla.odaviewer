/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#include "OdaCommon.h"
#include "DbPdfImportObjectGrabber.h"

#include <algorithm>
#include <memory>
#include <vector>
#include <functional>

#include "DbDatabase.h"
#include "DbBlockTableRecord.h"
#include "DbHostAppServices.h"
#include "Db2LineAngularDimension.h"
#include "Db2dPolyline.h"
#include "Db2dVertex.h"
#include "Db3PointAngularDimension.h"
#include "Db3dPolyline.h"
#include "Db3dPolylineVertex.h"
#include "Db3dSolid.h"
#include "DbAlignedDimension.h"
#include "DbArc.h"
#include "DbArcAlignedText.h"
#include "DbArcDimension.h"
#include "DbAttribute.h"
#include "DbAttributeDefinition.h"
#include "DbBlockReference.h"
#include "DbBlockTable.h"
#include "DbBody.h"
#include "DbCircle.h"
#include "DbDiametricDimension.h"
#include "DbDimAssoc.h"
#include "DbDimStyleTable.h"
#include "DbEllipse.h"
#include "DbFace.h"
#include "DbFaceRecord.h"
#include "DbFcf.h"
#include "DbField.h"
#include "DbGroup.h"
#include "DbHyperlink.h"
#include "DbLayerTable.h"
#include "DbLayout.h"
#include "DbLeader.h"
#include "DbMLeader.h"
#include "DbLine.h"
#include "DbLinetypeTable.h"
#include "DbMaterial.h"
#include "DbMInsertBlock.h"
#include "DbMline.h"
#include "DbMlineStyle.h"
#include "DbOrdinateDimension.h"
#include "DbPoint.h"
#include "DbPolyFaceMesh.h"
#include "DbPolyFaceMeshVertex.h"
#include "DbPolygonMesh.h"
#include "DbPolygonMeshVertex.h"
#include "DbPolyline.h"
#include "DbRadialDimension.h"
#include "DbRadialDimensionLarge.h"
#include "DbRasterImageDef.h"
#include "DbRasterVariables.h"
#include "DbRay.h"
#include "DbRegion.h"
#include "DbRotatedDimension.h"
#include "DbShape.h"
#include "DbSolid.h"
#include "DbSortentsTable.h"
#include "DbSpline.h"
#include "DbTable.h"
#include "DbMText.h"
#include "DbText.h"
#include "DbTextStyleTable.h"
#include "DbTrace.h"
#include "DbViewport.h"
#include "DbViewportTable.h"
#include "DbViewportTableRecord.h"
#include "DbWipeout.h"
#include "DbXline.h"
#include "DbXrecord.h"
#include "RText.h"
#include "DbOle2Frame.h"
#include "DbHelix.h"
#include "DbUnderlayDefinition.h"
#include "DbUnderlayReference.h"
#include "HatchPatternManager.h"
#include "DbLayerTableRecord.h"
#include "OdPath.h"

#include "Ge/GeCircArc2d.h"
#include "Ge/GeEllipArc2d.h"
#include "Ge/GeSplineEnt2d.h"
#include "Ge/GeScale3d.h"
#include "Ge/GeExtents3d.h"
#include "Ge/GeKnotVector.h"
#include "Ge/GeNurbCurve2d.h"
#include "Gi/GiMaterial.h"
#include "Gi/GiRasterWrappers.h"
#include "Ge/GeExtents2d.h"
#include "Ge/GeNurbCurve3d.h"
#include "Ge/GePolyline2d.h"
#include "Ge/GeLineSeg2d.h"

#include "MemoryStream.h"
#include "OdPlatformStreamer.h"
#include "RxRasterServices.h"
#include "Gs/LineWtIndex.h"
#include "OdRound.h"
#include <math.h>
#include <type_traits>
#include <utility>
#include "Ge/GeCircArc3d.h"
#include "Ge/GePolyline3d.h"
#include "Ge/GeEllipArc3d.h"
#include "SharedPtr.h"
#include "Ge/GeLine2d.h"

class PdfImportModule;

enum LayersUseType
{
  eUsePdfLayers = 0,
  eCreateObjectLayers = 1,
  eUseCurrentLayer = 2
};

OdDbPdfImportObjectGrabber::OdDbPdfImportObjectGrabber(PdfImportInfoHolderPtr& info, const std::map<std::wstring, bool>& layers_info, 
  const OdGePoint2dArray& clipBoundary, const bool is_clip_boundary_inverted, const OdString& images_path)
  :OdDbBasePdfImportObjectGrabber(info)
  , m_LayersInfo(layers_info)
  , m_ClipBoundary(clipBoundary)
  , m_isClipBoundaryInverted(is_clip_boundary_inverted)
  , m_ImagesPath(images_path)
{
    OdDbDatabasePtr pDb = OdDbDatabasePtr(m_Info->m_pProperties->get_Database());
    
    OdString pdf_path = m_Info->m_pProperties->get_PdfPath();
  
    int i1 = pdf_path.reverseFind('\\');
    int i2 = pdf_path.reverseFind('/');
    int i = odmax(i1, i2);
    m_PdfFileName = pdf_path.mid(i + 1);
    pdf_path = pdf_path.left(i + 1);
    i = m_PdfFileName.find(L'.');
    if (i != -1)
      m_PdfFileName = m_PdfFileName.left(i);
  
    if (m_Info->m_pProperties->get_ImportAsBlock() && !m_PdfFileName.isEmpty())
    {
        OdDbBlockTablePtr pTable = pDb->getBlockTableId().safeOpenObject(OdDb::kForWrite);
  
        OdDbObjectId block_id = pTable->getAt(m_PdfFileName);
  
        if (block_id.isNull())
        {
          OdDbBlockTableRecordPtr pRecord = OdDbBlockTableRecord::createObject();
          pRecord->setName(m_PdfFileName);
          block_id = pTable->add(pRecord);
  
          OdDbBlockTableRecordPtr active_space = pDb->getActiveLayoutBTRId().safeOpenObject(OdDb::kForWrite);
          OdDbBlockReferencePtr block_ref = OdDbBlockReference::createObject();
          block_ref->setBlockTableRecord(block_id);
          active_space->appendOdDbEntity(block_ref);
        }
        m_pBTR = block_id.safeOpenObject(OdDb::kForWrite);
    }
    else
    {
      m_pBTR = (OdDbBlockTableRecordPtr)
        (pDb->getActiveLayoutBTRId().safeOpenObject(OdDb::kForWrite));
    }
  
    m_TransforMatrix.setToIdentity();
    m_TransforMatrix *= OdGeMatrix3d::scaling(m_Info->m_pProperties->get_Scaling());
    m_TransforMatrix *= OdGeMatrix3d::rotation(m_Info->m_pProperties->get_Rotation(), OdGeVector3d::kZAxis);
    m_TransforMatrix.setTranslation(OdGeVector3d(m_Info->m_pProperties->get_InsertionPointX(), m_Info->m_pProperties->get_InsertionPointY(), 0.0));

    m_Info->m_ObjectCounter = 0;
}

OdDbPdfImportObjectGrabber::~OdDbPdfImportObjectGrabber()
{
  if (0 != m_LayerItems.size())
  {
    for (auto& layer_elem : m_LayerItems)
    {
      ODCOLORREF layer_col;
      std::vector<OdDbEntityPtr>::size_type max_count(0);
      for (auto& color_elem : layer_elem.second)
      {
        if (max_count < color_elem.second.size())
        {
          layer_col = color_elem.first;
          max_count = color_elem.second.size();
        }
      }
      OdDbLayerTableRecordPtr pLayer = layer_elem.first.openObject(OdDb::kForWrite);
      pLayer->setColor(toCmColor(layer_col));
      OdCmColor col;
      col.setColorIndex(OdCmEntityColor::kACIbyLayer);
      for (auto& obj_elem : layer_elem.second[layer_col])
      {
        obj_elem->setColor(col);
      }
    }

  }
}

OdCmColor OdDbPdfImportObjectGrabber::toCmColor(ODCOLORREF color)
{
  OdCmColor col;

  if (m_Info->m_pProperties->get_UseRgbColor() || 0xFFFFFF == color)
  {
    col.setRGB(ODGETRED(color), ODGETGREEN(color), ODGETBLUE(color));
  }
  else
  {
    OdInt16 idx = OdCmEntityColor::kACIWhite;
    if (0 != color)
    {
      idx = (OdInt16)OdCmEntityColor::lookUpACI(ODGETRED(color), ODGETGREEN(color), ODGETBLUE(color));
    }
    col.setColorIndex(idx);
    if (0 != color && color != ODRGB(col.red(), col.green(), col.blue()))
      col.setRGB(ODGETRED(color), ODGETGREEN(color), ODGETBLUE(color));
  }
  return col;
}

void fillKnots(const OdUInt32 size, OdGeKnotVector& knotsVec, OdGeDoubleArray& wts)
{
  OdGeDoubleArray knots;

  /* weight values */
  wts.resize(size, 1.);

  /* Knot points */
  knots.resize(size + 4, 0.);
  double kVal = 1.;
  int j = 0;
  for (j = 0; j < 4; j++)
    knots[j] = 0.;
  for (size_t i = 1; i < (size - 1) / 3; i++)
  {
    knots[j++] = kVal;
    knots[j++] = kVal;
    knots[j++] = kVal;
    kVal += 1.;
  }
  for (int i = 0; i < 4; i++)
    knots[j++] = kVal;

  knotsVec = knots;
  knotsVec.setTolerance(0.);
}

bool OdDbPdfImportObjectGrabber::pointInPolygon(const OdGePoint2d& point)
{
  const OdUInt32 points_count = m_ClipBoundary.size();
  if (0 == points_count)
    return true;
  bool res = false;

  for (OdUInt32 i = 0, j = points_count - 1; i < points_count; j = i++)
  {
    if (((m_ClipBoundary[i].y >= point.y) != (m_ClipBoundary[j].y >= point.y)) &&
      (point.x <= (m_ClipBoundary[j].x - m_ClipBoundary[i].x) * (point.y - m_ClipBoundary[i].y) /
      (m_ClipBoundary[j].y - m_ClipBoundary[i].y) + m_ClipBoundary[i].x))
      res = !res;
  }
  return m_isClipBoundaryInverted ? !res : res;
}

double curvature(const OdGePoint2d* points, const double& t)
{
  std::function<OdGeVector2d(const OdGePoint2d* points, const double& t)> tangent = [](const OdGePoint2d* points, const double& t)
  {
    const double t_2_0 = t * t;
    const double t_0_2 = (1 - t) * (1 - t);

    const double _1__4t_1_0_3t_2_0 = 1 - 4 * t + 3 * t_2_0;
    const double _2t_1_0_3t_2_0 = 2 * t - 3 * t_2_0;

    return OdGeVector2d(-3 * points[0].x * t_0_2
      + 3 * points[1].x * _1__4t_1_0_3t_2_0
      + 3 * points[2].x * _2t_1_0_3t_2_0
      + 3 * points[3].x * t_2_0,
      -3 * points[0].y * t_0_2
      + 3 * points[1].y * _1__4t_1_0_3t_2_0
      + 3 * points[2].y * _2t_1_0_3t_2_0
      + 3 * points[3].y * t_2_0);
  };

  std::function<OdGeVector2d(const OdGePoint2d* points, const double& t)> d_tangent = [](const OdGePoint2d* points, const double& t)
  {
    return OdGeVector2d(6 * ((-points[0].x + 3 * points[1].x - 3 * points[2].x + points[3].x) * t + (points[0].x - 2 * points[1].x + points[2].x)),
      6 * ((-points[0].y + 3 * points[1].y - 3 * points[2].y + points[3].y) * t + (points[0].y - 2 * points[1].y + points[2].y)));
  };

  OdGeVector2d dpp = tangent(points, t);
  dpp.set(-dpp.y, dpp.x);
  OdGeVector2d ddp = d_tangent(points, t);
  /* normal vector len squared */
  double len = dpp.length();
  if (OdZero(len))
    return 0.;
  double curvature = dpp.dotProduct(ddp) / (len * len * len);
  return curvature;

}

bool OdDbPdfImportObjectGrabber::fitSerment(const OdGePoint2dArray& samples, const OdGeCurve2dPtr& in_curve, const double& tol, const bool is_closed,
  OdGeCurve3dPtr& out_curve, BaseCurve::SegmentType& seg_type)
{

  OdGePoint2dArray out_points;

  bool is_circle =  m_CircleFitter.fit(samples, out_points, false);


  double circle_std_deviation = m_CircleFitter.getStdDeviation();

  bool is_added = false;

  double arc_area(0.);

  if (is_circle && OdPositive(m_CircleFitter.getRadius(), 1e-12))
  {
    double radius = m_CircleFitter.getRadius();
    double coef = 100. / radius * circle_std_deviation;

    if (coef < tol)
    {
      OdGeCurve3dPtr circle = m_CircleFitter.getCurve();
      if (circle.isNull())
        return false;

      seg_type = BaseCurve::kCircle;
      if (!is_closed)
      {
        double start_angle = circle->paramOf(OdGePoint3d(out_points[0].x, out_points[0].y, 0.));
        double end_angle = circle->paramOf(OdGePoint3d(out_points[out_points.size() - 1].x, out_points[out_points.size() - 1].y, 0.));

        OdGeVector2d v1(out_points[out_points.size() / 2] - out_points[0]);
        OdGeVector2d v2(out_points[out_points.size() - 1] - out_points[out_points.size() / 2]);

        if (v1.x * v2.y < v1.y * v2.x)
        { // clockwise
          std::swap(start_angle, end_angle);
        }

        static_cast<OdGeCircArc3d*>(circle.get())->setAngles(start_angle, end_angle);
        seg_type = BaseCurve::kCircleArc;
      }

      double in_len(0.);
      if (in_curve.isNull())
      {
        OdGePolyline2d poly(samples);
        in_len = poly.length();
      }
      else
      {
        in_len = in_curve->length();
      }
      double arc_len = circle->length();
      double ratio = arc_len / in_len;

      if (OdEqual(ratio, 1.0, 1e-2))
      {
        out_curve = circle;
        is_added = true;
      }
    }
  }

  bool is_elllipse = m_EllipseFitter.fit(samples, out_points);

  if (is_elllipse && OdPositive(m_EllipseFitter.getMajorRadius(), 1e-12) && OdPositive(m_EllipseFitter.getMinorRadius(), 1e-12))
  {
    double ellipse_std_deviation = m_EllipseFitter.getStdDeviation();
    if (is_added && (circle_std_deviation < ellipse_std_deviation))
      return true;

    double coef = 100. / m_EllipseFitter.getMajorRadius() * ellipse_std_deviation; 

    if (coef < tol)
    {
      OdGeCurve3dPtr ellipse = m_EllipseFitter.getCurve();
      if (ellipse.isNull())
        return false;

      seg_type = BaseCurve::kEllipse;
      if (!is_closed)
      {
        double start_angle = ellipse->paramOf(OdGePoint3d(out_points[0].x, out_points[0].y, 0.));
        double end_angle = ellipse->paramOf(OdGePoint3d(out_points[out_points.size() - 1].x, out_points[out_points.size() - 1].y, 0.));

        OdGeVector2d v1(out_points[out_points.size() / 2] - out_points[0]);
        OdGeVector2d v2(out_points[out_points.size() - 1] - out_points[out_points.size() / 2]);

        if (v1.x * v2.y < v1.y * v2.x)
        { // clockwise
          std::swap(start_angle, end_angle);
        }

        static_cast<OdGeEllipArc3d*>(ellipse.get())->setAngles(start_angle, end_angle);
        seg_type = BaseCurve::kEllipseArc;
      }

      double in_len(0.);
      if (in_curve.isNull())
      {
        OdGePolyline2d poly(samples);
        in_len = poly.length();
      }
      else
      {
        in_len = in_curve->length();
      }
      double arc_len = ellipse->length();
      double ratio = arc_len / in_len;

      if (OdEqual(ratio, 1.0, 1e-2))
      {
        out_curve = ellipse;
        return true;
      }
    }
  }

  return is_added;
}

void OdDbPdfImportObjectGrabber::OptimizePath(std::vector<BaseCurve>& paths)
{
  for (auto& fig : paths)
  {
    for (auto& seg : fig.getSegments())
    {
      if (BaseCurve::kLine == seg.getType())
      {
        if(9 > seg.getPointStorage().size() || 500 < seg.getPointStorage().size())
          continue;

        bool need_exit = false;
        for (OdGePoint2dArray::size_type i = 0; i < seg.getPointStorage().size() - 3; ++i)
        {

          if (fabs((seg.getPointStorage()[i+2].x - seg.getPointStorage()[i].x) / 
            (seg.getPointStorage()[i+1].x - seg.getPointStorage()[i].x) - (seg.getPointStorage()[i+2].y - seg.getPointStorage()[i].y) /
            (seg.getPointStorage()[i+1].y - seg.getPointStorage()[i].y)) < 1e-10)
          {
            need_exit = true;
            break;
          }
        }
        if (need_exit)
          continue;

        OdGeCurve3dPtr curve;
        BaseCurve::SegmentType seg_type = seg.getType();
        if (fitSerment(seg.getPointStorage(), nullptr, 0.5, seg.getPointStorage().first().isEqualTo(seg.getPointStorage().last()), curve, seg_type))
        {
          seg.setCurve(curve);
          seg.setType(seg_type); 
        }
      }
      else
      {
        OdUInt16 bezier_count = (seg.getPointStorage().size() - 1) / 3;
        const OdUInt8 sample_points_count = 10;
        bool curvature_changed = false;
        bool is_positive = curvature(&seg.getPointStorage()[0], 0) > 0.;
        for (OdUInt16 bez = 0; bez < bezier_count; ++bez)
        {
          for (int i = 1; i < 11; ++i)
          {
            if (is_positive != curvature(&seg.getPointStorage()[bez * 3], (double)i / 10.) > 0.)
            {
              curvature_changed = true;
              break;
            }
          }
          if(curvature_changed)
            break;
        } 
        if(curvature_changed)
          continue;

        OdGePoint2dArray ctrlPts;
        std::copy(seg.getPointStorage().begin(), seg.getPointStorage().end(), std::back_inserter(ctrlPts));

        OdGeKnotVector knotsVec;
        OdGeDoubleArray wts;

        fillKnots(ctrlPts.size(), knotsVec, wts);

        OdGeCurve2dPtr nurbs(new OdGeNurbCurve2d(3, knotsVec, ctrlPts, wts));

        OdGePoint2dArray samples;
        nurbs->getSamplePoints(sample_points_count, samples);

        OdGeCurve3dPtr curve;
        BaseCurve::SegmentType seg_type = seg.getType();

        const bool is_closed = seg.getPointStorage().first().isEqualTo(seg.getPointStorage().last());
        if (fitSerment(samples, nurbs, 0.1, is_closed, curve, seg_type))
        {
          seg.setCurve(curve);
          seg.setType(seg_type);
          if (is_closed && BaseCurve::kCircle == seg_type && bezier_count >= 4 && 0 == bezier_count % 4)
          {
            OdGeLine2d line1(seg.getPointStorage()[0], seg.getPointStorage()[3 * bezier_count / 2]);
            OdGeLine2d line2(seg.getPointStorage()[3 * bezier_count / 4], seg.getPointStorage()[9 * bezier_count / 4]);
            OdGePoint2d center;
            line1.intersectWith(line2, center);
            double radius = ((center - seg.getPointStorage()[0]).length() + (center - seg.getPointStorage()[3 * bezier_count / 2]).length() +
              (center - seg.getPointStorage()[3 * bezier_count / 4]).length() + (center - seg.getPointStorage()[9 * bezier_count / 4]).length()) / 4.0;
            OdGeCircArc3d* arc = static_cast<OdGeCircArc3d*>(seg.getCurve().get());
            arc->setCenter(OdGePoint3d(center.x, center.y, 0.));
            arc->setRadius(radius);
          }
        }
      }
    }
  }
}

void OdDbPdfImportObjectGrabber::addPath(const OdPdfImportPathData* path_data, const unsigned long path_point_count, 
  const OdPdfImportColor* fill_color, const OdPdfImportColor* stroke_color, const float* line_width, const bool is_object_visible,
  const wchar_t* cur_layer_name)
{
  ++m_Info->m_ObjectCounter;

  if (nullptr == path_data || nullptr == fill_color || nullptr == stroke_color || 0 == path_point_count)
  {
    ++m_Info->m_ErrorCounter;
    return;
  }

    try
    {
  
      OdGePoint2dArray pt_cache;
      bool fill = m_Info->m_pProperties->get_ImportSolidFills() && (0 != fill_color->alfa);
      bool stroke = (0 != stroke_color->alfa);
    
      if (!stroke && !fill)
      {
        return;
      }
  
      ODCOLORREF od_stroke_color = ODRGB(stroke_color->r, stroke_color->g, stroke_color->b);
      ODCOLORREF od_fill_color = ODRGB(fill_color->r, fill_color->g, fill_color->b);
  
      OdDbDatabasePtr pDb = OdDbDatabasePtr(m_Info->m_pProperties->get_Database());
  
      std::vector<BaseCurve> paths;
      paths.resize(1);
  
      OdGeVector3d z_axis(OdGeVector3d::kZAxis);
      double elevation = 0.0;
      OdGeMatrix2d matrix(m_TransforMatrix.convertToLocal(z_axis, elevation));
  
      bool inClipBoundary = false;
  
      for (size_t i = 0; i < path_point_count; i++)
      {
  
        OdGePoint2d point(path_data[i].m_Point.x, path_data[i].m_Point.y);
  
        if (pointInPolygon(point))
          inClipBoundary = true;
        point.transformBy(matrix);
  
        OdPdfImportPtType point_type = path_data[i].m_Type;
        if (point_type == OdPdfImportPtType::OdMoveTo)
        {
          if (pt_cache.size())
          {
            paths.back().addSeg(pt_cache, path_data[i - 1].m_Type);
            if (path_data[i - 1].m_CloseFigure || paths.back().getSegments().front().getPointStorage().first().isEqualTo(pt_cache.last()))
            {
              if (inClipBoundary)
              {
                paths.back().setClosed(path_data[i - 1].m_CloseFigure);
                paths.emplace_back(BaseCurve());
              }
              else
              {
                paths.back().clear();
                inClipBoundary = false;
              }
  
            }
          }
          pt_cache.clear();
          pt_cache.append(point);
        }
        else if (point_type == OdPdfImportPtType::OdLineTo)
        {
  
          if (OdPdfImportPtType::OdBezierTo == path_data[i - 1].m_Type)
          {
            paths.back().addSeg(pt_cache, path_data[i - 1].m_Type);
            if (path_data[i - 1].m_CloseFigure || paths.back().getSegments().front().getPointStorage().first().isEqualTo(pt_cache.last()))
            {
              if (inClipBoundary)
              {
                paths.back().setClosed(path_data[i - 1].m_CloseFigure);
                paths.emplace_back(BaseCurve());
              }
              else
              {
                paths.back().clear();
                inClipBoundary = false;
              }
  
            }
  
            OdGePoint2d ptTmp = pt_cache.last();
            pt_cache.clear();
            pt_cache.append(ptTmp);
          }
          pt_cache.append(point);
        }
        else if (point_type == OdPdfImportPtType::OdBezierTo)
        {
          if (OdPdfImportPtType::OdLineTo == path_data[i - 1].m_Type)
          {
            paths.back().addSeg(pt_cache, path_data[i - 1].m_Type);
            if (path_data[i - 1].m_CloseFigure || paths.back().getSegments().front().getPointStorage().first().isEqualTo(pt_cache.last()))
            {
              if (inClipBoundary)
              {
                paths.back().setClosed(path_data[i - 1].m_CloseFigure);
                paths.emplace_back(BaseCurve());
              }
              else
              {
                paths.back().clear();
                inClipBoundary = false;
              }
  
            }
  
            OdGePoint2d ptTmp = pt_cache.last();
            pt_cache.clear();
            pt_cache.append(ptTmp);
          }
          pt_cache.append(point);
        }
      }
      if (OdPdfImportPtType::OdMoveTo != path_data[path_point_count - 1].m_Type)
      {
        paths.back().addSeg(pt_cache, path_data[path_point_count - 1].m_Type);
        if (path_data[path_point_count - 1].m_CloseFigure || paths.back().getSegments().front().getPointStorage().first().isEqualTo(pt_cache.last()))
        {
          paths.back().setClosed(path_data[path_point_count - 1].m_CloseFigure);
        }
        if (!inClipBoundary)
          paths.back().clear();
      }
  
      if(0 == paths.size())
        --m_Info->m_ObjectCounter;

      if(m_Info->m_pProperties->get_UseGeometryOptimization())
        OptimizePath(paths);

      if (m_Info->m_pProperties->get_ImportSolidFills() && fill)
        saveSolid(paths, od_fill_color, is_object_visible, cur_layer_name);
  
      if (stroke && (!fill || od_fill_color != od_stroke_color))
        savePath(paths, od_stroke_color, line_width, is_object_visible, cur_layer_name);
  
      m_Info->MeterProgress();
    }
    catch (...)
    {
      ++m_Info->m_ErrorCounter;
    }
}

void OdDbPdfImportObjectGrabber::addCosmeticLine(const OdPdfImportPoint* point1, const OdPdfImportPoint* point2, const OdPdfImportColor* color, 
  const bool is_object_visible, const wchar_t* cur_layer_name)
{
  ++m_Info->m_ObjectCounter;

  if (nullptr == point1 || nullptr == point2 || nullptr == color)
  {
    ++m_Info->m_ErrorCounter;
    return;
  }

  try
  {
    ODCOLORREF cr_color = ODRGB(color->r, color->g, color->b);

    OdGePoint2d od_point1(point1->x, point1->y);
    OdGePoint2d od_point2(point2->x, point2->y);

    if (!pointInPolygon(od_point1) && !pointInPolygon(od_point2))
    {
      --m_Info->m_ObjectCounter;
      return;
    }

    OdDb::LineWeight line_widht(OdDb::kLnWtByLwDefault);

    OdCmColor color = toCmColor(cr_color);

    static const OdChar* layer_name = L"PDF_Geometry";
    OdDbObjectId layer_id = getLayer(layer_name, is_object_visible, cur_layer_name);
    OdDbLayerTableRecordPtr pLayer = layer_id.openObject(OdDb::kForRead);
    if (eUseCurrentLayer == m_Info->m_pProperties->get_LayersUseType() && color == pLayer->color())
      color.setColorIndex(OdCmEntityColor::kACIbyLayer);

    OdDbPolylinePtr pPolyline = OdDbPolyline::createObject();
    pPolyline->setDatabaseDefaults(m_pBTR->database());
    m_pBTR->appendOdDbEntity(pPolyline);
    pPolyline->setLayer(layer_id);

    pPolyline->addVertexAt(0, od_point1);
    pPolyline->addVertexAt(0, od_point2);

    pPolyline->setColor(color);
    if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
      m_LayerItems[layer_id][cr_color].push_back(pPolyline);
    pPolyline->setLineWeight(line_widht);

    m_Info->MeterProgress();

  }
  catch (...)
  {
    ++m_Info->m_ErrorCounter;
  }
}

void OdDbPdfImportObjectGrabber::addPixel(const OdPdfImportPoint* point, const OdPdfImportColor* color, const bool is_object_visible, 
  const wchar_t* cur_layer_name)
{
  ++m_Info->m_ObjectCounter;

  if (nullptr == point || nullptr == color)
  {
    ++m_Info->m_ErrorCounter;
    return;
  }

  try
  {

    ODCOLORREF cr_color = ODRGB(color->r, color->g, color->b);

    OdGePoint2d od_point(point->x, point->y);

    if (!pointInPolygon(od_point))
    {
      --m_Info->m_ObjectCounter;
      return;
    }

    OdCmColor color = toCmColor(cr_color);

    static const OdChar* layer_name = L"PDF_Geometry";
    OdDbObjectId layer_id = getLayer(layer_name, is_object_visible, cur_layer_name);
    OdDbLayerTableRecordPtr pLayer = layer_id.openObject(OdDb::kForRead);
    if (eUseCurrentLayer == m_Info->m_pProperties->get_LayersUseType() && color == pLayer->color())
      color.setColorIndex(OdCmEntityColor::kACIbyLayer);

    OdDbPointPtr pPoint = OdDbPoint::createObject();
    pPoint->setDatabaseDefaults(m_pBTR->database());
    m_pBTR->appendOdDbEntity(pPoint);
    pPoint->setLayer(layer_id);

    pPoint->setPosition(OdGePoint3d(od_point.x, od_point.y, 0));

    pPoint->setColor(color);
    if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
      m_LayerItems[layer_id][cr_color].push_back(pPoint);

    m_Info->MeterProgress();
  }
  catch (...)
  {
    ++m_Info->m_ErrorCounter;
  }
}

OdStreamBufPtr convertBmpToRaster(const BmiHeaderInfo* bmi_info, const unsigned char *imagedata)
{

  BITMAPINFOHEADER bmiHeader;
  memset(&bmiHeader, 0, sizeof(BITMAPINFOHEADER));
  bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  bmiHeader.biBitCount = bmi_info->bpp;
  bmiHeader.biCompression = 0L;
  bmiHeader.biHeight = bmi_info->height;
  bmiHeader.biPlanes = 1;
  bmiHeader.biWidth = bmi_info->width;
  bmiHeader.biSizeImage = bmi_info->pitch*fabs(bmi_info->height);


  OdStreamBufPtr pFileBuf = OdMemoryStream::createNew();

  OdUInt32 paletteSize = (nullptr != bmi_info->palette) ? bmi_info->palette_size *4 : 0;

  if (nullptr == bmi_info->palette && bmiHeader.biBitCount == 1)
  {
    paletteSize = 8;
  }

  OdPlatformStreamer::wrInt16(*pFileBuf, 0x4d42);
  OdUInt32 scanLinesPos = (OdUInt32)sizeof(BITMAPFILEHEADER) + (OdUInt32)sizeof(BITMAPINFOHEADER) + paletteSize;  // Below version is correct only on Windows
  OdUInt32 size = scanLinesPos + OdGiRasterImage::calcBMPScanLineSize(bmiHeader.biWidth, bmiHeader.biBitCount)*bmiHeader.biHeight;

  OdPlatformStreamer::wrInt32(*pFileBuf, size);
  OdPlatformStreamer::wrInt32(*pFileBuf, 0); // reserved
  OdPlatformStreamer::wrInt32(*pFileBuf, scanLinesPos); // offBits
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biSize);  // not portable: sizeof(BITMAPINFOHEADER));
                                                       // save BITMAPINFOHEADER
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biWidth);
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biHeight);
  OdPlatformStreamer::wrInt16(*pFileBuf, bmiHeader.biPlanes);
  OdPlatformStreamer::wrInt16(*pFileBuf, bmiHeader.biBitCount);
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biCompression);
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biSizeImage);
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biXPelsPerMeter);
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biYPelsPerMeter);
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biClrUsed);
  OdPlatformStreamer::wrInt32(*pFileBuf, bmiHeader.biClrImportant);

  if (nullptr == bmi_info->palette && bmiHeader.biBitCount == 1)
  {
    OdUInt32 tmp = 0;
    pFileBuf->putBytes(&tmp, 4);
    tmp = 0xffffff;
    pFileBuf->putBytes(&tmp, 4);
  }
  if (nullptr != bmi_info->palette)
  {
    pFileBuf->putBytes(bmi_info->palette, paletteSize);
  }

  pFileBuf->putBytes(imagedata, bmiHeader.biSizeImage);

  pFileBuf->rewind();

  return pFileBuf;
}

bool OdDbPdfImportObjectGrabber::createRasterImage(const BmiHeaderInfo* bmi_info, const unsigned char* imagedata, 
  const OdString& layer_name, const bool is_object_visible, const wchar_t* cur_layer_name, OdDbRasterImagePtr& pImage)
{
  bool ret = true;
  try
  {
    OdString image_def_name = OdString().format(L"%ls%ls", m_PdfFileName.c_str(),
      odrxSystemServices()->createOdGUID().toString(OdGUID::StringFormat::Digits).c_str());
  
    OdDbDatabasePtr pDb = OdDbDatabasePtr(m_Info->m_pProperties->get_Database());
  
    OdStreamBufPtr image_stream_buf = convertBmpToRaster(bmi_info, imagedata);
  
    OdRxRasterServicesPtr pRasSvcs = odrxDynamicLinker()->loadApp(RX_RASTER_SERVICES_APPNAME);
    if (pRasSvcs.isNull())
    {
      m_Info->m_ImageError = OdPdfImport::image_file_error;
      return false;
    }

    OdString image_file_name = m_ImagesPath + image_def_name + L".PNG";
    try
    {
      OdGiRasterImagePtr pTmp = pRasSvcs->loadRasterImage(image_stream_buf);

      OdStreamBufPtr image_file = odSystemServices()->createFile(
        image_file_name, Oda::kFileWrite, Oda::kShareDenyNo, Oda::kCreateAlways);
      ret = pRasSvcs->convertRasterImage(pTmp, OdRxRasterServices::kPNG, image_file);
    }
    catch (...)
    {
      m_Info->m_ImageError = OdPdfImport::image_file_error;
      ret = false;
    }

    OdDbObjectId imageDictId = OdDbRasterImageDef::imageDictionary(pDb);
    if (imageDictId.isNull())
    {
      imageDictId = OdDbRasterImageDef::createImageDictionary(pDb);
    }
    OdDbDictionaryPtr pImageDict = imageDictId.safeOpenObject(OdDb::kForWrite);

    OdDbRasterImageDefPtr pImageDef = OdDbRasterImageDef::createObject();
    OdDbObjectId imageDefId = pImageDict->setAt(image_def_name, pImageDef);
  
    pImageDef->setSourceFileName(image_file_name);
  
    if (pImageDef->image().isNull())
    {
      pImageDef->setImage(OdGiRasterImageDesc::createObject(bmi_info->width, bmi_info->height));
      ret = false;
    }
  
    pImage = OdDbRasterImage::createObject();
    pImage->setDatabaseDefaults(pDb);
    m_pBTR->appendOdDbEntity(pImage);
  
    pImage->setLayer(getLayer(layer_name, is_object_visible, cur_layer_name));
  
    pImage->setImageDefId(imageDefId);
  
    pImage->setDisplayOpt(OdDbRasterImage::kShow, true);
    pImage->setDisplayOpt(OdDbRasterImage::kShowUnAligned, true);
    pImage->setDisplayOpt(OdDbRasterImage::kTransparent, true);
 
  }
  catch (...)
  {
    ret = false;
  }
  return ret;
}

void OdDbPdfImportObjectGrabber::addImage(const BmiHeaderInfo* bmi, const unsigned char* imagedata, const OdPdfImportRect* rect, 
  const OdPdfImportMatrix* object_matrix, const bool is_object_visible, const wchar_t* cur_layer_name)
{
  ++m_Info->m_ObjectCounter;

  if (nullptr == bmi || nullptr == imagedata || nullptr == rect || nullptr == object_matrix)
  {
    ++m_Info->m_ErrorCounter;
    return;
  }

  try
  {
    static const OdChar* layer_name = L"PDF_Images";

    OdGePoint3d point(0.0, 0.0, 0.0);
    OdGeVector3d vW(rect->right - rect->left, 0.0, 0.0);
    OdGeVector3d vH(0.0, rect->bottom - rect->top, 0.0);

    OdGeMatrix3d od_object_matrix;
    od_object_matrix.setToIdentity();
    od_object_matrix[0][0] = object_matrix->a;
    od_object_matrix[0][1] = object_matrix->b;
    od_object_matrix[1][0] = object_matrix->c;
    od_object_matrix[1][1] = object_matrix->d;
    od_object_matrix[0][3] = object_matrix->e;
    od_object_matrix[1][3] = object_matrix->f;

    OdGeVector3d z_axis(OdGeVector3d::kZAxis);
    double elevation = 0.0;
    const OdGePoint2d point1 = point.convert2d().transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point2 = (point.convert2d() + vW.convert2d()).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point3 = (point.convert2d() + vH.convert2d()).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point4 = (point.convert2d() + vW.convert2d() + vH.convert2d()).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));

    if (!pointInPolygon(point1) && !pointInPolygon(point2) &&
      !pointInPolygon(point3) && !pointInPolygon(point4))
    {
      --m_Info->m_ObjectCounter;
      return;
    }

    OdDbRasterImagePtr pImage;
    if(!createRasterImage(bmi, imagedata, layer_name, is_object_visible, cur_layer_name, pImage))
      ++m_Info->m_ErrorCounter;
    if (pImage.isNull())
      return;

    pImage->setOrientation(point, vW, vH);

    pImage->transformBy(od_object_matrix);

    pImage->transformBy(m_TransforMatrix);

    m_Info->MeterProgress();

  }
  catch (...)
  {
    ++m_Info->m_ErrorCounter;
  }
}

void OdDbPdfImportObjectGrabber::addText(const char* in_font_face_name, const bool is_bold, const bool is_italic, const double font_height, 
  const double text_width, const wchar_t* text, const OdPdfImportColor* color, const OdPdfImportMatrix* object_matrix, 
  const bool is_object_visible, const wchar_t* cur_layer_name)
{
  ++m_Info->m_ObjectCounter;

  try
  {
    OdString font_face_name(in_font_face_name);
    
    OdString font_style_name = OdString(L"PDF ") + font_face_name;
    
    if (is_bold)
      font_style_name += L" Bold";
    
    if (is_italic)
      font_style_name += L" Italic";
    
    OdGeMatrix3d od_object_matrix;
    od_object_matrix.setToIdentity();
    od_object_matrix[0][0] = object_matrix->a;
    od_object_matrix[1][0] = object_matrix->b;
    od_object_matrix[0][1] = object_matrix->c;
    od_object_matrix[1][1] = object_matrix->d;
    od_object_matrix[0][3] = object_matrix->e;
    od_object_matrix[1][3] = object_matrix->f;
    
    OdGePoint3d origin;
    OdGeVector3d xAxis, yAxis, zAxis;
    od_object_matrix.getCoordSystem(origin, xAxis, yAxis, zAxis);

    double angleXY = xAxis.angleTo(yAxis);


    OdString content_str(text);
    
    OdDbDatabasePtr pDb = OdDbDatabasePtr(m_Info->m_pProperties->get_Database());
    
    OdDbTextStyleTablePtr pStyles = pDb->getTextStyleTableId().safeOpenObject(OdDb::kForWrite);
    
    OdDbObjectId styleId = pStyles->getAt(font_style_name);
    
    if (styleId.isNull())
    {
      OdDbTextStyleTableRecordPtr pStyle = OdDbTextStyleTableRecord::createObject();
    
      pStyle->setName(font_style_name);
    
      styleId = pStyles->add(pStyle);
    
      pStyle->setTextSize(0.);
    
      pStyle->setFont(font_face_name, is_bold, is_italic, false, 0);
    }
    
    OdGeVector3d z_axis(OdGeVector3d::kZAxis);
    double elevation = 0.0;
    const OdGePoint2d point1 = OdGePoint2d().transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point2 = OdGePoint2d(text_width, 0.).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point3 = OdGePoint2d(0., font_height).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point4 = OdGePoint2d(text_width, font_height).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    
    if (!pointInPolygon(point1) && !pointInPolygon(point2) &&
      !pointInPolygon(point3) && !pointInPolygon(point4))
    {
      --m_Info->m_ObjectCounter;
      return;
    }
    
    
    OdDbMTextPtr pMText = OdDbMText::createObject();
    pMText->setDatabaseDefaults(m_pBTR->database());
    m_pBTR->appendOdDbEntity(pMText);
    
    OdCmColor od_color = toCmColor(ODRGB(color->r, color->g, color->b));
    static const OdChar* layer_name = L"PDF_Text";
    OdDbObjectId layerId = getLayer(layer_name, is_object_visible, cur_layer_name);
    OdDbLayerTableRecordPtr pLayer = layerId.openObject(OdDb::kForRead);
    if (eUseCurrentLayer == m_Info->m_pProperties->get_LayersUseType() && od_color == pLayer->color())
      od_color.setColorIndex(OdCmEntityColor::kACIbyLayer);
    
    pMText->setLayer(layerId);
        
    pMText->setColor(od_color);
    if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
      m_LayerItems[layerId][ODRGB(color->r, color->g, color->b)].push_back(pMText);
    
    pMText->setAttachment(OdDbMText::kBottomLeft);
    
    pMText->setDirection(OdGeVector3d::kXAxis);
    
    pMText->setTextHeight(font_height);
    
    pMText->setTextStyle(styleId);
    
    pMText->setContents(content_str);

    if (!OdEqual(angleXY, OdaPI2))
    {
      content_str = OdDbMText::obliqueChange() + odDToStr(OdaToDegree(OdaPI2 - angleXY), 'f', 5) + OD_T(";") + content_str;
    }
    
    pMText->transformBy(od_object_matrix);
    
    const double coef = text_width / pMText->actualWidth();
    if (!OdEqual(coef, 1.0))
    {
      OdString format_str = OdString().format(L"\\W%f;", coef);
      format_str.replace(L',', L'.');
      content_str = format_str + content_str;
    }
    pMText->setContents(content_str);
    
    pMText->subTransformBy(m_TransforMatrix);
    
  }
  catch (...)
  {
    ++m_Info->m_ErrorCounter;
  }
}

void OdDbPdfImportObjectGrabber::addShading(const BmiHeaderInfo* bmi, const unsigned char* imagedata, const OdPdfImportRect* rect, 
  const OdPdfImportMatrix* object_matrix, const bool is_object_visible, const wchar_t* cur_layer_name)
{
  ++m_Info->m_ObjectCounter;

  if (nullptr == bmi || nullptr == imagedata || nullptr == rect || nullptr == object_matrix)
  {
    ++m_Info->m_ErrorCounter;
    return;
  }

  try
  {
    static const OdChar* layer_name = L"PDF_Gradient Fills";

    OdGePoint3d point(0, 0, 0.0);
    OdGeVector3d vW(rect->right - rect->left, 0.0, 0.0);
    OdGeVector3d vH(0.0, rect->bottom - rect->top, 0.0);

    OdGeMatrix3d od_object_matrix;
    od_object_matrix.setToIdentity();
    od_object_matrix[0][0] = object_matrix->a;
    od_object_matrix[0][1] = object_matrix->b;
    od_object_matrix[1][0] = object_matrix->c;
    od_object_matrix[1][1] = object_matrix->d;
    od_object_matrix[0][3] = object_matrix->e;
    od_object_matrix[1][3] = object_matrix->f;

    OdGeVector3d z_axis(OdGeVector3d::kZAxis);
    double elevation = 0.0;
    const OdGePoint2d point1 = point.convert2d().transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point2 = (point.convert2d() + vW.convert2d()).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point3 = (point.convert2d() + vH.convert2d()).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));
    const OdGePoint2d point4 = (point.convert2d() + vW.convert2d() + vH.convert2d()).transformBy(od_object_matrix.convertToLocal(z_axis, elevation));

    if (!pointInPolygon(point1) && !pointInPolygon(point2) &&
      !pointInPolygon(point3) && !pointInPolygon(point4))
    {
      --m_Info->m_ObjectCounter;
      return;
    }

    OdDbRasterImagePtr pImage;
    if (!createRasterImage(bmi, imagedata, layer_name, is_object_visible, cur_layer_name, pImage))
      ++m_Info->m_ErrorCounter;
    if (pImage.isNull())
      return;

    pImage->setOrientation(point, vW, vH);

    pImage->transformBy(od_object_matrix);

    pImage->transformBy(m_TransforMatrix);

    pImage->setFade(40);

    pImage->setDisplayOpt(OdDbRasterImage::kTransparent, true);

    OdDbObjectIdArray arObjId;
    arObjId.append(pImage->id());
    OdDbSortentsTablePtr pSortentsTable = m_pBTR->getSortentsTable(true);
    pSortentsTable->moveToBottom(arObjId);

    m_Info->MeterProgress();

  }
  catch (...)
  {
    ++m_Info->m_ErrorCounter;
  }
}

void OdDbPdfImportObjectGrabber::savePath(const std::vector<BaseCurve>& paths, const ODCOLORREF cr_color,
  const float* line_width, const bool is_object_visible, const wchar_t* cur_layer_name)
{
  OdDbDatabasePtr pDb = OdDbDatabasePtr(m_Info->m_pProperties->get_Database());

  static const OdChar* layer_name = L"PDF_Geometry";
  OdCmColor color = toCmColor(cr_color);
  OdDbObjectId layer_id = getLayer(layer_name, is_object_visible, cur_layer_name);
  OdDbLayerTableRecordPtr pLayer = layer_id.openObject(OdDb::kForRead);
  if (eUseCurrentLayer == m_Info->m_pProperties->get_LayersUseType() && color == pLayer->color())
    color.setColorIndex(OdCmEntityColor::kACIbyLayer);

  OdDb::LineWeight line_weight(OdDb::kLnWtByLwDefault);

  if (m_Info->m_pProperties->get_ApplyLineweight() && nullptr != line_width)
  {
    OdDb::LineWeight prev_line_widht(OdDb::kLnWt000);
    for (int i = 0; i < 24; ++i)
    {
      line_weight = ::lineWeightByIndex(i);
      if ((*line_width >= (float)prev_line_widht / 100.) && (*line_width < (float)line_weight / 100.))
      {
        if ((*line_width - (float)prev_line_widht / 100.) < ((float)line_weight / 100. - *line_width))
        {
          line_weight = prev_line_widht;
        }
        break;
      }
      prev_line_widht = line_weight;
    }
  }

  for (const auto& fig : paths)
  {
    for (const auto& seg : fig.getSegments())
    {
      switch (seg.getType())
      {
      case BaseCurve::kCircle:
      {
        OdDbCirclePtr pCircle = OdDbCircle::createObject();
        pCircle->setDatabaseDefaults(m_pBTR->database());
        m_pBTR->appendOdDbEntity(pCircle);
        pCircle->setFromOdGeCurve(*seg.getCurve().get());
        pCircle->setLayer(layer_id);
        pCircle->setColor(color);
        if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
          m_LayerItems[layer_id][cr_color].push_back(pCircle);
        pCircle->setLineWeight(line_weight);
      }
      break;
      case BaseCurve::kCircleArc:
      {
        OdDbArcPtr pCircleArc = OdDbArc::createObject();
        pCircleArc->setDatabaseDefaults(m_pBTR->database());
        m_pBTR->appendOdDbEntity(pCircleArc);
        pCircleArc->setFromOdGeCurve(*seg.getCurve().get());
        pCircleArc->setLayer(layer_id);
        pCircleArc->setColor(color);
        if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
          m_LayerItems[layer_id][cr_color].push_back(pCircleArc);
        pCircleArc->setLineWeight(line_weight);
      }
      break;
      case BaseCurve::kEllipse:
      case BaseCurve::kEllipseArc:
      {
        OdDbEllipsePtr pEllipse = OdDbEllipse::createObject();
        pEllipse->setDatabaseDefaults(m_pBTR->database());
        m_pBTR->appendOdDbEntity(pEllipse);
        pEllipse->setFromOdGeCurve(*seg.getCurve().get());
        pEllipse->setLayer(layer_id);
        pEllipse->setColor(color);
        if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
          m_LayerItems[layer_id][cr_color].push_back(pEllipse);
        pEllipse->setLineWeight(line_weight);
      }
      break;
      case BaseCurve::kLine:
        {
          OdDbPolylinePtr pPolyline = OdDbPolyline::createObject();
          pPolyline->setDatabaseDefaults(m_pBTR->database());
          m_pBTR->appendOdDbEntity(pPolyline);
          pPolyline->setLayer(layer_id);

          for (OdUInt32 i = 0; i < seg.getPointStorage().size(); ++i)
          {
            pPolyline->addVertexAt(i, seg.getPointStorage()[i]);
          }
          pPolyline->setColor(color);
        if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
          m_LayerItems[layer_id][cr_color].push_back(pPolyline);
          pPolyline->setLineWeight(line_weight);
        }
        break;
      case BaseCurve::kBezier:
        {
          OdGePoint3dArray ctrlPts;
          ctrlPts.resize(seg.getPointStorage().size());

          for (OdUInt32 i = 0; i < seg.getPointStorage().size(); i++)
          {
            ctrlPts[i] = OdGePoint3d(seg.getPointStorage()[i].x, seg.getPointStorage()[i].y, 0);
          }

          OdGeKnotVector knotsVec;
          OdGeDoubleArray wts;

          fillKnots(seg.getPointStorage().size(), knotsVec, wts);

          OdDbSplinePtr pSpline = OdDbSpline::createObject();
          pSpline->setDatabaseDefaults(m_pBTR->database());
          m_pBTR->appendOdDbEntity(pSpline);

          pSpline->setNurbsData(
            3,			/* int degree, */
            1, 			/* bool rational, */
            false, 		/* bool closed, */
            0,			/* bool periodic,*/
            ctrlPts,	        /* const OdGePoint3dArray& controlPoints, */
            knotsVec,		/* const OdGeDoubleArray& knots, */
            wts,			/* const OdGeDoubleArray& weights,*/
            0.0			/* double controlPtTol, */
          );
          pSpline->setColor(color);
          if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
            m_LayerItems[layer_id][cr_color].push_back(pSpline);
          pSpline->setLineWeight(line_weight);
          pSpline->setLayer(layer_id);
        }
        break;
      default:
        break;
      }
    }
  }
}

void OdDbPdfImportObjectGrabber::saveSolid(const std::vector<BaseCurve>& paths, const ODCOLORREF cr_color,
  const bool is_object_visible, const wchar_t* cur_layer_name)
{
  OdDbDatabasePtr pDb = OdDbDatabasePtr(m_Info->m_pProperties->get_Database());

  static const OdChar* layer_name = L"PDF_Solid Fills";
  static const OdCmTransparency transparency(0.5);

  OdCmColor color = toCmColor(cr_color);
  OdDbObjectId layerID = getLayer(layer_name, is_object_visible, cur_layer_name);
  OdDbLayerTableRecordPtr pLayer = layerID.openObject(OdDb::kForRead);
  if (eUseCurrentLayer == m_Info->m_pProperties->get_LayersUseType() && color == pLayer->color())
    color.setColorIndex(OdCmEntityColor::kACIbyLayer);

  std::function<OdDbHatchPtr()> create_hatch = [this, &color, &layerID, &cr_color]()
  {
    OdDbHatchPtr pHatch = OdDbHatch::createObject();
    pHatch->setDatabaseDefaults(m_pBTR->database());
    OdDbObjectId redHatchId = m_pBTR->appendOdDbEntity(pHatch);

    pHatch->setAssociative(false);
    pHatch->setPattern(OdDbHatch::kPreDefined, OD_T("SOLID"));
    pHatch->setHatchStyle(OdDbHatch::kNormal);
    pHatch->setColor(color);
    if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
      m_LayerItems[layerID][cr_color].push_back(pHatch);
    pHatch->setTransparency(transparency);

    pHatch->setLayer(layerID);
    return pHatch;
  };

  OdDbHatchPtr pHatch;
  if (!m_Info->m_pProperties->get_ConvertSolidsToHatches())
  {
    for (auto& fig : paths)
    {
      for (auto& seg : fig.getSegments())
      {
        if (!seg.isClosed() || BaseCurve::kLine != seg.getType() || 4 > seg.getPointStorage().size() || 5 < seg.getPointStorage().size())
        {
          pHatch = create_hatch();
          break;
        }
      }
      if(!pHatch.isNull())
        break;
    }
  }
  else
  {
    pHatch = create_hatch();
  }

  std::function<void(const BaseCurve::Segment& seg)> create_solid = [this, &color, &layerID, &cr_color](const BaseCurve::Segment& seg)
  {
    OdDbSolidPtr pSolid = OdDbSolid::createObject();
    pSolid->setDatabaseDefaults(m_pBTR->database());
    m_pBTR->appendOdDbEntity(pSolid);

    pSolid->setPointAt(0, OdGePoint3d(seg.getPointStorage()[0].x, seg.getPointStorage()[0].y, 0));
    pSolid->setPointAt(1, OdGePoint3d(seg.getPointStorage()[1].x, seg.getPointStorage()[1].y, 0));
    if(5 == seg.getPointStorage().size())
      pSolid->setPointAt(2, OdGePoint3d(seg.getPointStorage()[3].x, seg.getPointStorage()[3].y, 0));
    else
      pSolid->setPointAt(2, OdGePoint3d(seg.getPointStorage()[2].x, seg.getPointStorage()[2].y, 0));
    pSolid->setPointAt(3, OdGePoint3d(seg.getPointStorage()[2].x, seg.getPointStorage()[2].y, 0));

    pSolid->setColor(color);
    if (eUseCurrentLayer != m_Info->m_pProperties->get_LayersUseType())
      m_LayerItems[layerID][cr_color].push_back(pSolid);
    pSolid->setLayer(layerID);
    pSolid->setTransparency(transparency);
  };

  for (auto& fig : paths)
  {

    if (fig.isPolyline())
    {

      for (auto& seg : fig.getSegments())
      {
        if (!m_Info->m_pProperties->get_ConvertSolidsToHatches() && seg.isClosed() && 6 > seg.getPointStorage().size() && 3 < seg.getPointStorage().size())
        {
          create_solid(seg);
        }
        else
        {
          OdGePoint2dArray  vertexPts(seg.getPointStorage());
          OdGeDoubleArray   vertexBulges;
          pHatch->appendLoop(OdDbHatch::kExternal | OdDbHatch::kPolyline,
            vertexPts, vertexBulges);
          double area;
          pHatch->getArea(area);
          if (OdEqual(area, 0.0))
            savePath(paths, cr_color, nullptr, is_object_visible, cur_layer_name);
        }
      }
    }
    else
    {
      EdgeArray edgePtrs;
      edgePtrs.clear();
      for (auto& seg : fig.getSegments())
      {
        switch (seg.getPointsType())
        {
        case OdPdfImportPtType::OdLineTo:
        {
          if (!m_Info->m_pProperties->get_ConvertSolidsToHatches() && seg.isClosed() && 6 > seg.getPointStorage().size() && 3 < seg.getPointStorage().size())
          {
            create_solid(seg);
          }
          else
          {
            for (OdUInt32 i = 0; i < seg.getPointStorage().size() - 1; ++i)
            {
              OdGeLineSeg2d* poly = new OdGeLineSeg2d(seg.getPointStorage()[i], seg.getPointStorage()[i + 1]);
              edgePtrs.append(poly);
            }
          }
        }
          break;
        case OdPdfImportPtType::OdBezierTo:
          {
            OdGeKnotVector knotsVec;
            OdGeDoubleArray wts;

            fillKnots(seg.getPointStorage().size(), knotsVec, wts);

            OdGeNurbCurve2d *spline = new OdGeNurbCurve2d();

            spline->set(3, knotsVec, seg.getPointStorage(), wts, false);

            edgePtrs.push_back(spline);
          }
          break;
        default:
          break;
        }
      }
      if (0 != edgePtrs.size())
      {
        try
        {
          pHatch->appendLoop(OdDbHatch::kDefault, edgePtrs);
        }
        catch (...)
        {
          savePath(paths, cr_color, nullptr, is_object_visible, cur_layer_name);
        }

        edgePtrs.clear();
      }
    }
  }
  return;
}

OdDbObjectId OdDbPdfImportObjectGrabber::getLayer(const OdChar* name, const bool is_object_visible, const wchar_t* cur_layer_name)
{
  OdDbDatabasePtr pDb = OdDbDatabasePtr(m_Info->m_pProperties->get_Database());
  OdDbLayerTablePtr pLayers = pDb->getLayerTableId().safeOpenObject(OdDb::kForWrite);
  OdDbObjectId layer_id;

  std::function<void()> createObjectLayers = [&]()
  {
    OdString layer_name = is_object_visible ? OdString(name) : (OdString(name) + OdString(L"_Invisible"));
    layer_id = pLayers->getAt(layer_name);
    if (layer_id.isNull())
    {
      OdDbLayerTableRecordPtr pLayer = OdDbLayerTableRecord::createObject();
      pLayer->setName(layer_name);
      layer_id = pLayers->add(pLayer);
      pLayer->setIsOff(!is_object_visible);
    }
  };

  static const OdChar forbidden_chars [] = { L'\\', L':',L';',L'=',L'?',L'<',L'>',L'/',L'*',L'"',L'|' };

  OdString od_cur_layer_name(cur_layer_name);

  for (auto& ch : forbidden_chars)
    od_cur_layer_name.replace(ch, L'$');

  switch (m_Info->m_pProperties->get_LayersUseType())
  {
  case eUsePdfLayers: // Use PDF Layers
  {
    OdString layer_name = od_cur_layer_name.isEmpty() ? OdString(name): (OdString(L"PDF_") + od_cur_layer_name);
    layer_id = pLayers->getAt(layer_name);
    if (layer_id.isNull())
    {
      OdDbLayerTableRecordPtr pLayer = OdDbLayerTableRecord::createObject();
      pLayer->setName(layer_name);
      layer_id = pLayers->add(pLayer);
      bool state = true;
      std::map<std::wstring,bool>::const_iterator iter = m_LayersInfo.find(cur_layer_name);
      if (iter != m_LayersInfo.end())
        state = iter->second;
      pLayer->setIsOff(!state);
    }
  }
  break;
  case eCreateObjectLayers: // Create object layers
  {
    createObjectLayers();
  }
  break;
  case eUseCurrentLayer: // Use current layer
    if (is_object_visible)
      layer_id = pDb->getCLAYER();
    else
      createObjectLayers();
    break;
  default:
    break;
  }
  return layer_id;
}



