/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _PDFIMPORT_INCLUDED_
#define _PDFIMPORT_INCLUDED_

#include "RxModule.h"
#include "RxDictionary.h"
#include "DynamicLinker.h"

//////////////////////////////////////////////////////////////////////////

#include "OdPlatformSettings.h"

#ifdef PDFIMPORT_EXPORTS
  #define PDFIMPORT_TOOLKIT         OD_TOOLKIT_EXPORT
  #define PDFIMPORT_TOOLKIT_STATIC  OD_STATIC_EXPORT
#else                               
  #define PDFIMPORT_TOOLKIT         OD_TOOLKIT_IMPORT
  #define PDFIMPORT_TOOLKIT_STATIC  OD_STATIC_IMPORT
#endif

//////////////////////////////////////////////////////////////////////////

class OdDbDatabase;

//DD:EXPORT_ON

/** \details
An abstract class that the represents the interface for the import from a PDF file.
<group OdPdfImport_Classes>
*/
class OdPdfImport : public OdRxObject
{
public:
  /** \details 
  Contains the list of the PDF import operation results, which are handled by the <link OdPdfImportModule, PDF import module>.
  */
  enum ImportResult 
  {
    success,              // The import process is successfully finished.
    fail,                 // The import process is failed.
    bad_password,         // The import process is failed because of an incorrect password.
    bad_file,             // The import process is failed because of an incorrect input .pdf file.
    bad_database,         // The import process is failed because of an incorrect destination database object.
    encrypted_file,       // The import process is failed because of an encrypted input .pdf file.
    invalid_page_number,  // The import process is failed because of an invalid page number of the input .pdf file.
    image_file_error,     // The import process is failed because of an error during the image file saving (the possible reason is an invalid directory for saving).
    no_objects_imported   // The import process is failed because no objects were imported.
  };
  
  /** \details 
  Imports a .pdf file contents to a drawing database with specified properties.
  This method should be reimplemented to provide the import from a .pdf file functionality.
  \returns Returns a value of the <link OdPdfImport::ImportResult, ImportResult> enumeration that contains the result of the import process.
  \remarks 
  To specify import properties, use the <link OdPdfImport::properties, properties()> method to get access to the import properties dictionary object.
  */
  virtual ImportResult import() = 0;

  /** \details 
  Retrieves a dictionary object that contains the set of .pdf import properties.
  \returns Returns a smart pointer to the dictionary object that contains properties used for the import of a .pdf file contents to a drawing database. 
  \remarks 
  The list of supported properties for the PDF import operation is represented in the table below.
  <table>
  Property Name                 Description
  Database                      A target database object (an instance of the OdDbDatabase class). The contents of the PDF file are imported to this database.
  PdfPath                       A full path to the imported PDF file.
  Password                      A password for the input PDF file (is empty by default).
  PageNumber                    A page number of the input PDF file. Starts from 1 (by default).
  LayersUseType                 A method that determines how imported objects should be assigned to to layers in the target database.
  ImportVectorGeometry          A flag that determines whether the PDF geometry data types are imported (true by default).
  ImportSolidFills              A flag that determines whether all solid-filled areas are imported (true by default).
  ImportTrueTypeText            A flag that determines whether text objects that use TrueType fonts should be imported (true by default).
                                Some symbols exported from a PDF document may be displayed incorrectly. It happens because the PDF document uses symbols, which the origin TrueType font does not contain. 
                                For guaranteed correct displaying all symbols, please switch on the ImportTrueTypeTextAsGeometry option for the import process.
  ImportRasterImages            A flag that determines whether raster images are imported through saving them as .png files and attaching them to the current drawing (true by default).
  ImportGradientFills           A flag that determines whether shading objects are imported as images by saving them as .png files and attaching them to the current drawing (true by default). 
  ImportAsBlock                 A flag that determines whether the PDF file is imported as a block, but not as separate objects (false by default).
  JoinLineAndArcSegments        A flag that determines whether contiguous segments are joined into a polyline if it is possible (true by default).
  ConvertSolidsToHatches        A flag that determines whether 2D solid objects are converted into solid-filled hatches (true by default).
  ApplyLineweight               A flag that determines whether lineweight properties of the imported objects remain (if true) or are ignored (if false). By default the property is equal to true.
  ImportTrueTypeTextAsGeometry  A flag that determines whether text objects that use TrueType fonts are imported as polylines and solid-filled hatches (false by default).
                                To import text as geometry switch on the ImportVectorGeometry and the ImportSolidFills import options.
  Scaling                       Specifies the scaling factor for imported objects (is equal to 1.0 by default).
  Rotation                      Specified the rotation angle for imported objects (is equal to 0.0 by default).
  InsertionPointX               An X-coordinate of the imported PDF content location relative to the location of the current UCS (is equal to 0.0 by default).
  InsertionPointY               An Y-coordinate of the imported PDF content location relative to the location of the current UCS (is equal to 0.0 by default).
  UseProgressMeter              A flag that determines whether progress meter is used for the import (true be default). Using the progress meter can reduce the performance.
  UseGeometryOptimization       A flag that determines whether geometry optimization algorithm is used for the import (true be default).
  ImportType3AsTrueTypeText     A flag that determines whether text objects that use Type3 fonts are imported as objects that use TrueType fonts (false by default).
                                Some symbols exported from a PDF document may be displayed incorrectly. It happens because the PDF document uses Type3 fonts glyphs, which the origin TrueType font does not contain.
  UseRgbColor                   A flag that determines whether RGB colors are used or whether colors are selected from the palette, if possible (false by default).
  </table>
  */
  virtual OdRxDictionaryPtr properties() = 0;
};

/** \details
A data type that represents a smart pointer to an <link OdPdfImport, PDF importer> object.
*/
typedef OdSmartPtr<OdPdfImport> OdPdfImportPtr;

/** \details
The abstract class that provides an interface for the PDF import module. 
<group OdPdfImport_Classes> 
*/
class OdPdfImportModule : public OdRxModule
{
public:
    
    /** \details
    Creates an instance of the PDF import module if it was not created yet; otherwise, provides an access to the previously created module.
    \returns Returns a smart pointer to the <link OdPdfImportModule, PDF import module>. 
    */
    virtual OdPdfImportPtr create () = 0;
    
};

/** \details
A data type that represents a smart pointer to a <link OdPdfImportModule, PDF import module>.
*/
typedef OdSmartPtr<OdPdfImportModule> OdPdfImportModulePtr;


/** \details 
Creates an instance of the <link OdPdfImportModule, PDF import module> class.
\returns Returns a smart pointer to the <link OdPdfImportModule, PDF import module>. 
<group OdPdfImport_Classes>
*/
inline OdPdfImportPtr createImporter()
{
     OdPdfImportModulePtr pModule = ::odrxDynamicLinker()->loadApp(OdPdfImportModuleName);
     if ( !pModule.isNull() ) return pModule->create ();
     return (OdPdfImport*)0;
}

//DD:EXPORT_OFF

#endif // _PDFIMPORT_INCLUDED_
