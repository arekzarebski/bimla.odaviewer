/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _PDF_IMPORTER_EX_INCLUDED_ 
#define _PDF_IMPORTER_EX_INCLUDED_

#include "PdfImport.h"
#include <map>
#include "Ge/GePoint2dArray.h"

class PdfProperties;
typedef OdSmartPtr<PdfProperties> PdfPropertiesPtr;

/** \details
An abstract class that provides the interface for upper level of the PDF import manager.
<group OdPdfImport_Classes> 
*/
class PdfImporterEx : public OdPdfImport
{
public:

  /** \details
  Retrieves current set of the PDF import properties.
  \returns Returns a smart pointer to a dictionary object that contains import properties.
  */
  virtual OdRxDictionaryPtr properties() = 0;

  /** \details
  Imports the data from a specified PDF file to a drawing database.
  \returns Returns a value of the <link OdPdfImport::ImportResult, ImportResult> enumeration that contains the result of the import operation.
  */
  virtual ImportResult import() = 0;

  /** \details 
  Loads the content of a specified PDF document.
  \returns Returns a value of the <link OdPdfImport::ImportResult, ImportResult> enumeration that contains the result of the loading operation.
  */
  virtual ImportResult loadDocument() = 0;

  /** \details 
  Retrieves whether the PDF document is successfully loaded.
  \returns Returns true if the document content is loaded; otherwise, the method returns false.
  */
  virtual bool isDocumentLoaded() const = 0;

  /** \details 
  Loads the content of a specified page from the PDF document.
  \returns Returns a value of the <link OdPdfImport::ImportResult, ImportResult> enumeration that contains the result of the loading operation.
  */
  virtual ImportResult loadPage() = 0;

  /** \details 
  Retrieves whether the page from the PDF document is successfully loaded.
  \returns Returns true if the page content is loaded; otherwise, the method returns false.
  */
  virtual bool isPageLoaded() const = 0;

  /** \details 
  Retrieves the quantity of pages in the PDF document.
  \returns Returns the current page quantity.
  */
  virtual OdUInt32 getPageCount() const = 0;

  /** \details 
  Retrieves the current measuring unit of the PDF document.
  \returns Returns the current measuring unit.
  */
  virtual double getMeasureDictInfo() = 0;

  /** \details 
  Retrieves the layers information from the PDF document.
  \param layers_info [out] A placeholder for an STL map object that contains layers information.
  \returns Returns a value of the <link OdPdfImport::ImportResult, ImportResult> enumeration that contains the result of the filling operation.
  \remarks 
  The layer information contains a set of key-value pairs. Each pair contains a layer name as a key and a boolean flag that determines the visibility of the layer as a value.
  The set of key-value pairs a stored in an STL map object.
  The method fills the passed map object with current layers information retrieved from the PDF document and returns it to a calling subroutine.  
  */
  virtual OdPdfImport::ImportResult fillLayersInfo(std::map<std::wstring, bool>& layers_info) = 0;

  /** \details 
  Retrieves the current page width of the PDF document.
  \returns Returns the current page width in measure units.
  */
  virtual double getPageWidth() const = 0;

  /** \details 
  Retrieves the current page height of the PDF document.
  \returns Returns the current page height in measure units.
  */
  virtual double getPageHeight() const = 0;

  /** \details 
  Sets a new clipping boundaries for the PDF document content.
  \param clipBoundary [in] A point array that defines the boundary of clipping.
  \param is_clip_boundary_inverted [in] A boolean value that determines whether the boundary is inverted (if is equal to true) or not (if is equal to false).
  */
  virtual void setClipBoundary(const OdGePoint2dArray& clipBoundary, const bool is_clip_boundary_inverted) = 0;

  /** \details 
  Sets layers information.
  \param layers_info [in] An STL map object that contains layers information.
  \remarks 
  The layer information contains a set of key-value pairs. 
  Each pair contains a layer name as a key and a boolean flag that determines the visibility of the layer as a value.
  The set of key-value pairs a stored in an STL map object that is passed to the method. Using this map object you can change the visibility of each layer, 
  previously retrieved from the PDF document through calling the fillLayersInfo() method.  
  */
  virtual void setLayersInfo(std::map<std::wstring, bool>& layers_info) = 0;

  /** \details 
  Clears underlay information. 
  */
  virtual void clearUnderlayInfo() = 0;

  /** \details
  Retrieves information about import results .
  \param object_count [out] A placeholder for the number of PDF objects to be imported.
  \param error_count [out] A placeholder for the number of PDF objects imported with an error.
  */
  virtual void getImportedObjectsInfo(size_t& object_count, size_t& error_count) = 0;

};

/** \details 
A data type that represents a smart pointer to a <link PdfImporterEx, PDF importer> object.
*/
typedef OdSmartPtr<PdfImporterEx> PdfImporterExPtr;

#endif // _PDF_IMPORTER_EX_INCLUDED_
