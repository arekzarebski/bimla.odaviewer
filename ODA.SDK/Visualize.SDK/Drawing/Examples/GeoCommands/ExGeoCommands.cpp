/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#include "OdaCommon.h"
#include "DbUserIO.h"
#include "DbCommandContext.h"

#include "Gs/Gs.h"
#include "DbPolyline.h"
#include "DbBlockTableRecord.h"
#include "DbViewportTable.h"
#include "DbViewportTableRecord.h"
#include "DbDictionary.h"
#include "DbDictionaryVar.h"

#include "DbSortentsTable.h"
#include "DbBlockTableRecord.h"

#include "DbGeoData.h"
#include "DbGeoMap.h"


void _Geo_func(OdEdCommandContext* pCmdCtx)
{
  OdDbCommandContextPtr pDbCmdCtx(pCmdCtx);
  OdDbDatabasePtr pDb = pDbCmdCtx->database();
  OdSmartPtr<OdDbUserIO> pIO = pDbCmdCtx->userIO();
  
  OdString sCS = pIO->getString("Specify Coordinate System name <WORLD-MERCATOR>:", OdEd::kInpDefault, L"WORLD-MERCATOR");
  OdGePoint3d ptDesign = pIO->getPoint(L"Specify design point:");

  OdDbObjectId objIdGeoData;
  oddbGetGeoDataObjId(pDb, objIdGeoData);
  if (!objIdGeoData.isNull())
  {
    OdDbGeoDataPtr pOldGeoData = objIdGeoData.openObject();
    pOldGeoData->eraseFromDb();
  }

  OdDbGeoDataPtr geoData = OdDbGeoData::createObject();
  geoData->setBlockTableRecordId(pDb->getModelSpaceId());
  OdDbObjectId objId;
  ODA_VERIFY(eOk == geoData->postToDb(objId));
  ODA_VERIFY(eOk == geoData->setCoordinateSystem(sCS));
  geoData->setCoordinateType(OdDbGeoData::kCoordTypGrid);
  geoData->setDesignPoint(ptDesign);
}

void _GeoMapImageUpdate_func(OdEdCommandContext* pCmdCtx)
{
  OdDbCommandContextPtr pDbCmdCtx(pCmdCtx);
  OdDbDatabasePtr pDb = pDbCmdCtx->database();
  OdSmartPtr<OdDbUserIO> pIO = pDbCmdCtx->userIO();

  OdDbObjectId objIdGeoData;
  oddbGetGeoDataObjId(pDb, objIdGeoData);
  if (objIdGeoData.isNull())
  {
    pIO->putString("Command Error: There is no GeoData in drawing");
    return;
  }

  OdDbSelectionSetPtr pSSet = pIO->select(L"Select map image(s) to update:", OdEd::kSelDefault);
  OdUInt32 nKeyword = pIO->getKeyword(
    L"Enter action [Optimize/Reload] ",
    L"Optimize Reload", 1
    );

  bool bMod;
  switch (nKeyword)
  {
  case 0:
    bMod = true;
    break;
  case 1:
  default:
    bMod = false;
    break;
  }

  if (pSSet->numEntities())
  {
    OdDbSelectionSetIteratorPtr pIter = pSSet->newIterator();

    pDb->startTransaction();
    while (!pIter->done())
    {
      OdDbObjectId objId = pIter->objectId();
      OdDbGeoMapPtr pEnt = OdDbGeoMap::cast(objId.openObject(OdDb::kForWrite));
      if (!pEnt.isNull())
      {
        pEnt->updateMapImage(bMod);
      }
      pIter->next();
    }
    pDb->endTransaction();
  }
}

void _GeoMapImageSetResolution_func(OdEdCommandContext* pCmdCtx)
{
  OdDbCommandContextPtr pDbCmdCtx(pCmdCtx);
  OdDbDatabasePtr pDb = pDbCmdCtx->database();
  OdSmartPtr<OdDbUserIO> pIO = pDbCmdCtx->userIO();

  OdDbObjectId objIdGeoData;
  oddbGetGeoDataObjId(pDb, objIdGeoData);
  if (objIdGeoData.isNull())
  {
    pIO->putString("Command Error: There is no GeoData in drawing");
    return;
  }

  OdDbSelectionSetPtr pSSet = pIO->select(L"Select map image(s) to change their resolution:", OdEd::kSelDefault);
  OdUInt32 nKeyword = pIO->getKeyword(
    L"Enter resolution [Cource/Optimal/Fine/VeryFine] ",
    L"Cource Optimal Fine VeryFine", 1
    );

  int nResolution;
  switch(nKeyword)
  {
  case 0:
    nResolution = kCoarse;
    break;
  case 2:
    nResolution = kFine;
    break;
  case 3:
    nResolution = kFiner;
    break;
  case 1:
  default:
    nResolution = kOptimal;
    break;
  }

  if (pSSet->numEntities())
  {
    OdDbSelectionSetIteratorPtr pIter = pSSet->newIterator();

    pDb->startTransaction();
    while (!pIter->done())
    {
      OdDbObjectId objId = pIter->objectId();
      OdDbGeoMapPtr pEnt = OdDbGeoMap::cast(objId.openObject(OdDb::kForWrite));
      if (!pEnt.isNull())
      {
        pEnt->setResolution(OdGeoMapResolution(nResolution));
        pEnt->updateMapImage();
      }
      pIter->next();
    }
    pDb->endTransaction();
  }
}

void _GeoMapImageSetMapType_func(OdEdCommandContext* pCmdCtx)
{
  OdDbCommandContextPtr pDbCmdCtx(pCmdCtx);
  OdDbDatabasePtr pDb = pDbCmdCtx->database();
  OdSmartPtr<OdDbUserIO> pIO = pDbCmdCtx->userIO();

  OdDbObjectId objIdGeoData;
  oddbGetGeoDataObjId(pDb, objIdGeoData);
  if (objIdGeoData.isNull())
  {
    pIO->putString("Command Error: There is no GeoData in drawing");
    return;
  }

  OdDbSelectionSetPtr pSSet = pIO->select(L"Select map image(s) to change their type:", OdEd::kSelDefault);
  OdUInt32 nKeyword = pIO->getKeyword(
    L"Enter map type [Aerial/Road/Hybrid] ",
    L"Aerial Road Hybrid", 0
    );

  int nType;
  switch(nKeyword)
  {
  case 1:
    nType = kRoad;
    break;
  case 2:
    nType = kHybrid;
    break;
  case 0:
  default:  
    nType = kAerial;
    break;
  }

  if (pSSet->numEntities())
  {
    OdDbSelectionSetIteratorPtr pIter = pSSet->newIterator();

    pDb->startTransaction();
    while (!pIter->done())
    {
      OdDbObjectId objId = pIter->objectId();
      OdDbGeoMapPtr pEnt = OdDbGeoMap::cast(objId.openObject(OdDb::kForWrite));
      if (!pEnt.isNull())
      {
        pEnt->setMapType(OdGeoMapType(nType));
        pEnt->updateMapImage();
      }
      pIter->next();
    }
    pDb->endTransaction();
  }
}

void _GeoMapImage_func(OdEdCommandContext* pCmdCtx)
{
  OdDbCommandContextPtr pDbCmdCtx(pCmdCtx);
  OdDbDatabasePtr pDb = pDbCmdCtx->database();
  OdDbUserIO* pIO = pDbCmdCtx->dbUserIO();

  OdDbObjectId objIdGeoData;
  oddbGetGeoDataObjId(pDb, objIdGeoData);
  if (objIdGeoData.isNull())
  {
    pIO->putString("Command Error: There is no GeoData in drawing");
    return;
  }

  OdDbViewportTablePtr pVt = pDb->getViewportTableId().safeOpenObject();
  OdDbViewportTableRecordPtr pVp = pVt->getActiveViewportId().safeOpenObject();
  OdDbDictionaryPtr pExtDictionary = pVp->extensionDictionary().openObject();
  OdDbDictionaryVarPtr pDictionaryVar;
  if (pExtDictionary.get())
  {
    OdDbDictionaryPtr pExtVariableDictionary = pExtDictionary->getAt(L"AcDbVariableDictionary").openObject();
    if (pExtVariableDictionary.get())
    {
      pDictionaryVar = pExtVariableDictionary->getAt(L"GEOMAPMODE").openObject();
    }
  }
  if (pDictionaryVar.isNull())
  {
    pIO->putString("Command Error: GeoMap type is set as kNoMap, specify it with GEOMAP command");
    return;
  }

  OdGePoint3d ptStart = pIO->getPoint(L"Specify first corner:");

  struct GeoMapTracker : OdStaticRxObject<OdEdPointTracker>
  {
    OdDbPolylinePtr m_ClipBoundary;
    OdGePoint3d m_ptFirst;

    GeoMapTracker(const OdGePoint3d & ptFirst) 
    {
      m_ptFirst = ptFirst;
      m_ClipBoundary = OdDbPolyline::createObject();
      m_ClipBoundary->reset(false, 5);
      for (int i = 0; i < 5; ++i)
      {
        m_ClipBoundary->addVertexAt(i, OdGePoint2d(ptFirst.x, ptFirst.y));
      }
    }

    virtual void setValue(const OdGePoint3d & val)
    {
      m_ClipBoundary->reset(false, 5);
      m_ClipBoundary->addVertexAt(0, OdGePoint2d(m_ptFirst.x, m_ptFirst.y));
      m_ClipBoundary->addVertexAt(1, OdGePoint2d(m_ptFirst.x, val.y));
      m_ClipBoundary->addVertexAt(2, OdGePoint2d(val.x, val.y));
      m_ClipBoundary->addVertexAt(3, OdGePoint2d(val.x, m_ptFirst.y));
      m_ClipBoundary->addVertexAt(4, OdGePoint2d(m_ptFirst.x, m_ptFirst.y));
    }

    virtual int addDrawables(OdGsView* pView)
    {
      return pView->add(m_ClipBoundary, 0);
    }

    virtual void removeDrawables(OdGsView* pView)
    {
      pView->erase(m_ClipBoundary);
    }
  } GeoMapTracker(ptStart);

  OdGePoint3d ptEnd = pIO->getPoint(L"Specify opposite corner:", 0, 0, OdString::kEmpty, &GeoMapTracker);

  OdDbObjectIdArray arObjId;
  {
    OdDbGeoMapPtr pGeoMap = OdDbGeoMap::createObject();
    pGeoMap->setDatabaseDefaults(pDb);
    OdGeExtents3d ext;
    ext.addPoint(ptStart);
    ext.addPoint(ptEnd);
    pGeoMap->setOrientation(ext.minPoint(),
                            OdGeVector3d(ext.maxPoint().x - ext.minPoint().x, 0., 0.),
                            OdGeVector3d(0., ext.maxPoint().y - ext.minPoint().y, 0.));

    OdString sGeoMapType = pDictionaryVar->value();

    pGeoMap->setMapType(OdGeoMapType(Od_strtoint(sGeoMapType)));
    pGeoMap->updateMapImage(true);

    pGeoMap->setDisplayOpt(OdDbRasterImage::kShow, true);
    OdDbObjectId modelSpaceId = pDb->getModelSpaceId();
    OdDbBlockTableRecordPtr pM = modelSpaceId.openObject(OdDb::kForWrite);
    pM->appendOdDbEntity(pGeoMap);
    arObjId.append(pGeoMap->id());
  }
  
  OdDbBlockTableRecordPtr pModelSpace = pDb->getModelSpaceId().safeOpenObject();
  OdDbSortentsTablePtr pSortentsTable = pModelSpace->getSortentsTable(true);
  pSortentsTable->moveToBottom(arObjId);
}


void _GeoMap_func(OdEdCommandContext* pCmdCtx)
{
  OdDbCommandContextPtr pDbCmdCtx(pCmdCtx);
  OdDbDatabasePtr pDb = pDbCmdCtx->database();
  OdDbUserIO* pIO = pDbCmdCtx->dbUserIO();

  OdUInt32 nKeyword = pIO->getKeyword(
    L"Select map type [Aerial/Road/Hybrid/Off] ",
    L"Aerial Road Hybrid Off", 0
    );

  int nType;
  switch(nKeyword)
  {
  case 1:
    nType = kRoad;
    break;
  case 2:
    nType = kHybrid;
    break;
  case 3:
    nType = kNoMap;
    break;
  case 0:
  default:  
    nType = kAerial;
    break;
  }

  OdDbViewportTablePtr pVt = pDb->getViewportTableId().safeOpenObject();
  OdDbViewportTableRecordPtr pVp = pVt->getActiveViewportId().safeOpenObject(OdDb::kForWrite);
  OdDbDictionaryPtr pExtDictionary = pVp->extensionDictionary().openObject(OdDb::kForWrite);
  OdDbDictionaryPtr pExtVariableDictionary;

  if (pExtDictionary.isNull())
  {
    pVp->createExtensionDictionary();
    pExtDictionary = pVp->extensionDictionary().safeOpenObject(OdDb::kForWrite);
    pExtVariableDictionary = pExtDictionary->setAt(L"AcDbVariableDictionary", OdDbDictionary::createObject()).safeOpenObject(OdDb::kForWrite);
  }
  else
  {
    pExtVariableDictionary = pExtDictionary->getAt(L"AcDbVariableDictionary").safeOpenObject(OdDb::kForWrite);
  }

  if (nType != kNoMap)
  {
    OdDbDictionaryVarPtr pDictionaryVar = OdDbDictionaryVar::createObject();
    pDictionaryVar->setValue(nType);
    pExtVariableDictionary->setAt(L"GEOMAPMODE", pDictionaryVar);
  }
  else
  {
    pExtVariableDictionary->remove(L"GEOMAPMODE");
  }
}
