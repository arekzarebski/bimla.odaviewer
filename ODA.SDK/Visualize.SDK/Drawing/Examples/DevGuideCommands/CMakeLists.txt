#
#  DevGuideCommands library
#


tdrawing_sources(${TD_DEVGUIDECOMMANDS_LIB}
    StdAfx.cpp
    StdAfx.h
    DevGuideCommandsModule.h
    DevGuideCommandsModule.cpp
    DevGuideCommandsUtils.h
    DevGuideCommandsUtils.cpp
    DevGuideCmdDef.h
    ArcAlignedText.cpp
    PolygonMesh.cpp
    OdPdfUnderlayEx.cpp
    OdRasterImgEx.cpp
    DbDimensionsEx.cpp
    DbFcfEx.cpp
    OdLeaderEx.cpp
    OdSolidEx.cpp
    OdSplinesEx.cpp
)


if(ODA_SHARED AND MSVC)
tdrawing_sources(${TD_DEVGUIDECOMMANDS_LIB}
    DevGuideCommands.rc
)
endif(ODA_SHARED AND MSVC)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

tdrawing_tx(${TD_DEVGUIDECOMMANDS_LIB} ${TD_DBROOT_LIB} ${TD_DB_LIB} ${TD_GI_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB} )

tdrawing_project_group(${TD_DEVGUIDECOMMANDS_LIB} "Examples")
