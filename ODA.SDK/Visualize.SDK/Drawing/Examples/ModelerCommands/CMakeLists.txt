#
# ModelerCommands library
#


tdrawing_sources(${TD_MODELERCOMMANDS_LIB}
  PropertiesCommand.cpp
  NewTessellationCommand.cpp
  AcisInCommand.cpp
  ModelerCommands.cpp
  AcisInCommand.h
  PropertiesCommand.h
  NewTessellationCommand.h
  ModelerCommands.h
  SpaModeler/Spa_region_Command.cpp
  SpaModeler/Spa_region_Command.h
  SpaModeler/Spa_solidedit_Command.cpp
  SpaModeler/Spa_solidedit_Command.h
  SpaModeler/Spa_union_Command.cpp
  SpaModeler/Spa_union_Command.h
  SpaModeler/Spa_imprint_Command.cpp
  SpaModeler/Spa_imprint_Command.h
  SpaModeler/Brep_LineContainment_Command.cpp
  SpaModeler/Brep_LineContainment_Command.h
  AuditBrepCommand.h
  AuditBrepCommand.cpp
  CopyBrepCommand.h
  CopyBrepCommand.cpp
  ExSweepCommand.h
  ExSweepCommand.cpp
  )

include_directories(
                      ${TKERNEL_ROOT}/Extensions
                   )

if(ODA_SHARED AND MSVC)
tdrawing_sources(${TD_MODELERCOMMANDS_LIB}
                 ModelerCommands.rc
                 )
endif(ODA_SHARED AND MSVC)
add_definitions(-DODW_DDBR_EXPORTS
                -DBR_DLL_EXPORTS)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

tdrawing_tx(${TD_MODELERCOMMANDS_LIB} ${TD_DB_LIB} ${TD_ACISBLDR_LIB} ${TD_DBROOT_LIB} ${TD_DB_LIB} ${TD_GI_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_BR_LIB} ${TD_ALLOC_LIB}
            ${TD_BREPBUILDER_LIB} ${TD_BREPBUILDERFILLER_LIB})

tdrawing_project_group(${TD_MODELERCOMMANDS_LIB} "Examples")