#
#  OdWriteEx executable
#
if(MSVC)
string(REGEX REPLACE "-DUNICODE" "" CMAKE_C_FLAGS ${CMAKE_C_FLAGS})
string(REGEX REPLACE "-D_UNICODE" "" CMAKE_C_FLAGS ${CMAKE_C_FLAGS})
string(REGEX REPLACE "-DUNICODE" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
string(REGEX REPLACE "-D_UNICODE" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
endif(MSVC)

set(od_write_src
    OdWriteEx.cpp
    DbFiller.cpp
    DbFiller.h
    EntityBoxes.h
    DbSubDMeshData.cpp
	ExCreateNurbSurfaceData.h
	ExCreateNurbSurfaceData.cpp
	)

include_directories(
					${TKERNEL_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Extensions/ExServices
					${TDRAWING_ROOT}/Include/DbPointCloudObj
					${TDRAWING_ROOT}/Examples/GeolocationObj
					../Common)
					
if(ODA_SHARED)
set ( od_write_libs  ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_RASTER_LIB} ${TD_KEY_LIB} ${TD_DB_POINTCLOUDOBJ_LIB} ${TD_DB_LIB} ${TD_GS_LIB}  ${TD_GI_LIB}
                     ${TD_ROOT_LIB} ${TD_GE_LIB} ${TD_DBROOT_LIB} ${TD_GEOLOCATIONOBJ_LIB} ${TH_ZLIB_LIB} ${TD_BREPBUILDER_LIB} ${CRYPTO_LINK_LIBS}
)
else(ODA_SHARED)
set ( od_write_libs ${TD_FIELDEVAL_LIB} ${TD_RDIMBLK_LIB} ${TD_RASTERPROC_LIB} ${TD_TXV_BITMAP_LIB}
                    ${TD_MODELER_LIB} ${TCOMPONENTS_BREPMODELER_LIB} ${TD_BREPBUILDERFILLER_LIB}
                    ${TD_BREPRENDERER_LIB} ${TD_BR_LIB} ${TD_BREPBUILDER_LIB}
                    ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_RASTER_LIB} ${TD_ACISBLDR_LIB} ${TD_DB_POINTCLOUDOBJ_LIB}
                    ${TD_DbCryptModule_LIB} ${TD_DB_LIB} ${TD_GS_LIB} ${TD_GI_LIB} ${TD_DBROOT_LIB} ${TD_GE_LIB} ${TD_SPATIALINDEX_LIB}
                    ${TH_THIRDPARTYRASTER_LIB} ${TH_WCHAR_LIB} ${TD_GEOLOCATIONOBJ_LIB} ${TD_GEODATAEX_LIB} ${TD_GEOMAPPE_LIB} 
                    ${TH_CSMAP_LIB} ${TH_TINYXML_LIB} ${TH_CURL_LIB} ${TH_ZLIB_LIB} ${TD_ROOT_LIB} ${TH_CONDITIONAL_LIBCRYPTO} ${TH_UTF_LIB} ${TD_DYNBLOCKS_LIB}
)
if (NOT WINTTF)
set ( od_write_libs ${od_write_libs}
                    ${TH_FT_LIB})
endif (NOT WINTTF)

add_definitions(-DCURL_STATICLIB)

endif(ODA_SHARED)

if(MSVC)
set(od_write_src ${od_write_src}
				  OdWriteEx.rc
				 )
endif(MSVC)

tdrawing_sources(OdWriteEx ${od_write_src})

tdrawing_executable(OdWriteEx ${od_write_libs} ${TD_ALLOC_LIB} ) 
if(MSVC)
set_target_properties(OdWriteEx PROPERTIES COMPILE_FLAGS "-DUNICODE -D_UNICODE")
endif(MSVC)
tdrawing_project_group(OdWriteEx "Examples")

if(MSVC AND EN_MBSC)
#Build OdWriteEx as MBCS sample
tdrawing_sources(OdWriteExMbcs ${od_write_src})

tdrawing_executable(OdWriteExMbcs ${od_write_libs} ${TD_ALLOC_LIB} )
set_target_properties(OdWriteExMbcs PROPERTIES COMPILE_FLAGS "-D_MBCS")

tdrawing_project_group(OdWriteExMbcs "Examples")
endif(MSVC AND EN_MBSC)
