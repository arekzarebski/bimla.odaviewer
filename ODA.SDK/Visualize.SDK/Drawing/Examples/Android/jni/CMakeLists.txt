set( jni_lib teigha_jni )

# Enable GLES2 by this definition
SET (ANDROID_GLES2 1)

if(ANDROID_GLES2)
    Message ("-- GLES2 for Android Enabled")
    add_definitions(-DANDROID_GLES2)
    set( gl_libs ${TD_TXV_GLES2_LIB} ${TR_TXR_GL2_LIB} ${TR_RENDER_LIB} ${TR_VEC_LIB} ${TR_BASE_LIB})
    set( gl_sdk_libs android EGL GLESv2 )
else(ANDROID_GLES2)
    set( gl_libs ${TD_TXV_OPENGL_LIB} ${TD_OPENGL_LIB} )
    set( gl_sdk_libs GLESv1_CM )
endif(ANDROID_GLES2)

tdrawing_sources(${jni_lib}
        AndroidAppServices.h
        AndroidAppServices.cpp
        ExHostAppServices.h
        com_opendesign_android_TeighaDWGJni.cpp
        com_opendesign_android_TeighaDWGJni.h
)

include_directories(${ODA_SOURCE_ROOT}
                    ${TDRAWING_ROOT}/Include
                    ${TKERNEL_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Imports/DwfImport/Include
                    ${TDRAWING_ROOT}/Imports/ColladaImport/Include
                    ExRender)

#Uncomment/Comment to enable/disable DWF
set(DWF_MODULES ${TD_DWF_IMPORT_LIB} ${TH_DWF7_TOOLKIT_LIB} ${TH_DWF7_W3DTK_LIB} ${TH_DWF7_WHIPTK_LIB} ${TH_DWF7_CORE_LIB} )
add_definitions(-DDWF_MODULES_ENABLED)

#Uncomment/Comment to enable/disable open COLLADA
set(COLLADA_MODULES ${TD_COLLADA_IMPORT_LIB} ${TH_COLLADASaxFrameworkLoader} ${TH_COLLADAFramework}
                    ${TH_COLLADABaseUtils} ${TH_GeneratedSaxParser} ${TH_PCRE_LIB} ${TH_MathMLSolver} ${TH_libXML} ${TH_UTF_LIB})
add_definitions(-DCOLLADA_ENABLED)

#Uncomment/Comment to enable/disable open PdfUnderlay
if(TDRAWING_PDFIUM)
set(PDFIUM_MODULE_LIBS ${TD_PDFIUMMODULE_LIB} ${TH_PDFIUM_LIB} ${TH_FT_LIB})
add_definitions(-DPDFIUM_MODULE_ENABLED)
endif(TDRAWING_PDFIUM)



if(NOT ANDROID_NDK_USE_NATIVE_WCHAR)
tdrawing_module(${jni_lib}
                       ${TD_EXLIB} ${TD_DR_EXLIB} ${ta_libs} ${gl_libs}
                       ${TD_MODELER_LIB} ${TCOMPONENTS_BREPMODELER_LIB} ${TD_BREPBUILDERFILLER_LIB}
                       ${TD_BREPRENDERER_LIB} ${TD_BR_LIB} ${TD_ACISBLDR_LIB}
                       ${DWF_MODULES} ${COLLADA_MODULES} ${PDFIUM_MODULE_LIBS}
                       ${TD_DB_LIB} ${TD_DBROOT_LIB} ${TD_GS_LIB} ${TD_GI_LIB} ${TD_SPATIALINDEX_LIB} ${TD_BREPBUILDER_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB}
                       ${TD_RASTERPROC_LIB} ${TD_RASTER_LIB} ${TH_THIRDPARTYRASTER_LIB} ${TH_ZLIB_LIB} ${TH_libXML} ${TD_RDIMBLK_LIB} 
                       ${TH_FT_LIB} ${TD_ALLOC_LIB} ${TH_CONDITIONAL_LIBCRYPTO} 
                       ${gl_sdk_libs} log)

else(NOT ANDROID_NDK_USE_NATIVE_WCHAR)
tdrawing_module(${jni_lib}
                       ${TD_EXLIB} ${TD_DR_EXLIB} ${ta_libs} ${gl_libs}
                       ${TD_MODELER_LIB} ${TCOMPONENTS_BREPMODELER_LIB} ${TD_BREPBUILDERFILLER_LIB}
                       ${TD_BREPRENDERER_LIB} ${TD_BR_LIB} ${TD_ACISBLDR_LIB}
                       ${DWF_MODULES} ${COLLADA_MODULES}  ${PDFIUM_MODULE_LIBS}
                       ${TD_OLESSITEMHDR_LIB} 
${TD_DB_LIB} ${TD_DBROOT_LIB} ${TD_GS_LIB} ${TD_GI_LIB} ${TD_BREPBUILDER_LIB} ${TD_GE_LIB} ${TD_SPATIALINDEX_LIB} ${TD_ROOT_LIB}
                       ${TD_RASTERPROC_LIB} ${TD_RASTER_LIB} ${TH_THIRDPARTYRASTER_LIB} ${TH_ZLIB_LIB} ${TH_libXML} ${TD_RDIMBLK_LIB} 
                       ${TH_FT_LIB} ${TH_WCHAR_LIB} ${TD_ALLOC_LIB} ${TH_CONDITIONAL_LIBCRYPTO}  ${TH_OLESS_LIB}
                       ${gl_sdk_libs} log)
endif(NOT ANDROID_NDK_USE_NATIVE_WCHAR)

if(PDFIUM_MODULE_ENABLED)
add_definitions(-DPDFIUM_MODULE_ENABLED_LOCAL)
endif()
