cmake_minimum_required(VERSION 3.4.1)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wno-inconsistent-missing-override")

add_definitions(
  -DANDROID_GOOGLE -DANDROID -DANDROID_NDK_NATIVE -DOD_DO_NOT_DECLARE_WCHAR
 )

set(ODA_SOURCE_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/../../../../../../../../../)
set(ODA_LINK_LIBRARIES ${ODA_SOURCE_ROOT}/lib/android_${ANDROID_ABI}_r18b_mingw_x64dbg)

if(CMAKE_HOST_SYSTEM)
  if(${CMAKE_HOST_SYSTEM} MATCHES "^Darwin.*")
    set(ODA_LINK_LIBRARIES ${ODA_SOURCE_ROOT}/lib/android_${ANDROID_ABI}_r18b_macos_x64)
  else()
    if(${CMAKE_HOST_SYSTEM} MATCHES "^Linux.*")
      set(ODA_LINK_LIBRARIES ${ODA_SOURCE_ROOT}/lib/android_${ANDROID_ABI}_r15c_linux_x64dbg)
    endif()
  endif()
endif()

set(TDRAWING_ROOT ${ODA_SOURCE_ROOT}/Drawing)
set(TKERNEL_ROOT ${ODA_SOURCE_ROOT}/Kernel)
set(TH_ROOT ${ODA_SOURCE_ROOT}/ThirdParty)

add_library(teigha_jni SHARED
        ../../../../../../jni/AndroidAppServices.h
        ../../../../../../jni/AndroidAppServices.cpp
        ../../../../../../jni/ExHostAppServices.h
        ../../../../../../jni/com_opendesign_android_TeighaDWGJni.cpp
        ../../../../../../jni/com_opendesign_android_TeighaDWGJni.h
)


include_directories(${ODA_SOURCE_ROOT}
                    ${TDRAWING_ROOT}/Include
                    ${TKERNEL_ROOT}/Include
                    ${TKERNEL_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Extensions/ExServices
                    ${TH_ROOT}/activation)


#Symbolic names of libraries
set (TD_DB_LIB                TD_Db)
set (TD_DR_EXLIB              TD_DrawingsExamplesCommon)
set (TD_EXLIB                 TD_ExamplesCommon)
set (TD_MODELER_LIB           ModelerGeometry)
set (TD_BREPRENDERER_LIB      TD_BrepRenderer)
set (TD_BR_LIB                TD_Br)
set (TD_ACISBLDR_LIB          TD_AcisBuilder)
set (TD_DBROOT_LIB            TD_DbRoot)
set (TD_GS_LIB                TD_Gs)
set (TD_GI_LIB                TD_Gi)
set (TD_BREPBUILDER_LIB       TD_BrepBuilder)
set (TD_GE_LIB                TD_Ge)
set (TD_SPATIALINDEX_LIB      TD_SpatialIndex)
set (TD_ROOT_LIB              TD_Root)
set (TD_RASTERPROC_LIB        RasterProcessor)
set (TD_RASTER_LIB            RxRasterServices)
set (TH_THIRDPARTYRASTER_LIB  FreeImage)
set ( TD_OLESSITEMHDR_LIB     OdOleSsItemHandler)
set ( TH_OLESS_LIB     oless)


set (TH_ZLIB_LIB              TD_Zlib)
set (TH_libXML                libXML)
set (TD_RDIMBLK_LIB           RecomputeDimBlock)
set (TH_FT_LIB                FreeType)
set (TH_WCHAR_LIB             TH_Wchar)
set (TD_ALLOC_LIB             TD_Alloc)
set (TH_CONDITIONAL_LIBCRYPTO         libcrypto)

set(TD_TXV_GLES2_LIB WinGLES2)

set(TR_VEC_LIB TrVec)
set(TR_TXR_GL2_LIB TrGL2)
set(TR_RENDER_LIB TrRenderBase)

set(TR_BASE_LIB TrBase)

set (TD_DBENT_LIB              TD_DbEntities)
set (TD_DBCORE_LIB      TD_DbCore)
set (TD_DBIO_LIB               TD_DbIO)
set (TD_SM_LIB                 TD_Sm)
set (TD_AVE_LIB                TD_Ave)
set (TD_SCENEOE_LIB            SCENEOE)
set (TD_ACCAMERA_LIB           ACCAMERA)
set (TD_ISM_LIB                ISM)
set (TD_WIPEOUT_LIB            WipeOut)
set (TD_MPOLYGON_LIB           AcMPolygonObj15)
set (TD_ATEXT_LIB              ATEXT)
set (TD_RTEXT_LIB              RText)
set (TH_UTF_LIB              UTF)

set (TCOMPONENTS_BREPMODELER_LIB  OdBrepModeler)
set (TD_BREPBUILDERFILLER_LIB     TD_BrepBuilderFiller)

set(TD_PDFIUMMODULE_LIB PDFiumModule)
set(TH_PDFIUM_LIB pdfium)

#Enable/Disable using of PDFiumModule - just uncomment/comment out below code
#set(PDFIUM_MODULE_LIBS ${TD_PDFIUMMODULE_LIB} ${TH_PDFIUM_LIB} ${TH_FT_LIB})
#add_definitions(-DPDFIUM_MODULE_ENABLED)

message(STATUS "Build OdaAndroidApp application")

find_library( # Defines the name of the path variable that stores the
              # location of the NDK library.
              log-lib

              # Specifies the name of the NDK library that
              # CMake needs to locate.
              log )

# add lib dependencies
add_library( ${TD_DR_EXLIB}
             STATIC
             IMPORTED )
set_target_properties( # Specifies the target library.
                       ${TD_DR_EXLIB}
                       # Specifies the parameter you want to define.
                       PROPERTIES IMPORTED_LOCATION
                       # Provides the path to the library you want to import.
                       ${ODA_LINK_LIBRARIES}/lib${TD_DR_EXLIB}.a)

add_library( ${TD_EXLIB}
             STATIC
             IMPORTED )
set_target_properties( ${TD_EXLIB}
                       PROPERTIES IMPORTED_LOCATION
                       ${ODA_LINK_LIBRARIES}/lib${TD_EXLIB}.a)

add_library( ${TD_DB_LIB}
             STATIC
             IMPORTED )
set_target_properties( ${TD_DB_LIB}
                       PROPERTIES IMPORTED_LOCATION
                       ${ODA_LINK_LIBRARIES}/lib${TD_DB_LIB}.a)

add_library( ${TD_MODELER_LIB}
             STATIC
             IMPORTED )
set_target_properties( ${TD_MODELER_LIB}
                       PROPERTIES IMPORTED_LOCATION
                       ${ODA_LINK_LIBRARIES}/lib${TD_MODELER_LIB}.a)

add_library( ${TD_BREPRENDERER_LIB}
             STATIC
             IMPORTED )
set_target_properties( ${TD_BREPRENDERER_LIB}
                       PROPERTIES IMPORTED_LOCATION
                       ${ODA_LINK_LIBRARIES}/lib${TD_BREPRENDERER_LIB}.a)

add_library( ${TD_BR_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_BR_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_BR_LIB}.a)

add_library( ${TD_ACISBLDR_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_ACISBLDR_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_ACISBLDR_LIB}.a)

add_library( ${TD_DBROOT_LIB}
             STATIC
             IMPORTED )
set_target_properties( ${TD_DBROOT_LIB}
                       PROPERTIES IMPORTED_LOCATION
                       ${ODA_LINK_LIBRARIES}/lib${TD_DBROOT_LIB}.a)

add_library( ${TD_RASTER_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_RASTER_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_RASTER_LIB}.a)
add_library( ${TD_RASTERPROC_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_RASTERPROC_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_RASTERPROC_LIB}.a)
#

add_library( ${TD_GS_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_GS_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_GS_LIB}.a)

add_library( ${TD_GI_LIB}
             STATIC
             IMPORTED )
set_target_properties( ${TD_GI_LIB}
                       PROPERTIES IMPORTED_LOCATION
                       ${ODA_LINK_LIBRARIES}/lib${TD_GI_LIB}.a)

add_library( ${TCOMPONENTS_BREPMODELER_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TCOMPONENTS_BREPMODELER_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TCOMPONENTS_BREPMODELER_LIB}.a)


add_library( ${TD_BREPBUILDERFILLER_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_BREPBUILDERFILLER_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_BREPBUILDERFILLER_LIB}.a)

add_library( ${TD_BREPBUILDER_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_BREPBUILDER_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_BREPBUILDER_LIB}.a)

add_library( ${TD_SPATIALINDEX_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_SPATIALINDEX_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_SPATIALINDEX_LIB}.a)
#
add_library( ${TD_GE_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_GE_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_GE_LIB}.a)
#
add_library( ${TD_ROOT_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_ROOT_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_ROOT_LIB}.a)
#
add_library( ${TH_THIRDPARTYRASTER_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TH_THIRDPARTYRASTER_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_THIRDPARTYRASTER_LIB}.a)
#
add_library( ${TH_ZLIB_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TH_ZLIB_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_ZLIB_LIB}.a)
#
add_library( ${TH_libXML}
            STATIC
            IMPORTED )
set_target_properties(${TH_libXML}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_libXML}.a)
#
add_library( ${TD_RDIMBLK_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_RDIMBLK_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_RDIMBLK_LIB}.a)
#
add_library( ${TH_FT_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TH_FT_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_FT_LIB}.a)
#
#
add_library( ${TH_WCHAR_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TH_WCHAR_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_WCHAR_LIB}.a)
#
add_library( ${TD_ALLOC_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_ALLOC_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_ALLOC_LIB}.a)
#
add_library( ${TH_CONDITIONAL_LIBCRYPTO}
            STATIC
            IMPORTED )
set_target_properties(${TH_CONDITIONAL_LIBCRYPTO}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_CONDITIONAL_LIBCRYPTO}.a)
#

add_library( ${TD_DBENT_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_DBENT_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_DBENT_LIB}.a)
#

add_library( ${TD_DBCORE_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_DBCORE_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_DBCORE_LIB}.a)
#

add_library( ${TD_DBIO_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_DBIO_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_DBIO_LIB}.a)
#

add_library( ${TD_SM_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_SM_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_SM_LIB}.a)
#

add_library( ${TD_AVE_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_AVE_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_AVE_LIB}.a)
#

add_library( ${TD_SCENEOE_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_SCENEOE_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_SCENEOE_LIB}.a)
#

add_library( ${TD_ACCAMERA_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_ACCAMERA_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_ACCAMERA_LIB}.a)
#

add_library( ${TD_ISM_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_ISM_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_ISM_LIB}.a)
#

add_library( ${TD_WIPEOUT_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_WIPEOUT_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_WIPEOUT_LIB}.a)
#

add_library( ${TD_MPOLYGON_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_MPOLYGON_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_MPOLYGON_LIB}.a)
#

add_library( ${TD_ATEXT_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_ATEXT_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_ATEXT_LIB}.a)
#

add_library( ${TD_RTEXT_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_RTEXT_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_RTEXT_LIB}.a)
#

add_definitions(-DANDROID_GLES2)
set( gl_libs ${TD_TXV_GLES2_LIB} ${TR_TXR_GL2_LIB} ${TR_RENDER_LIB} ${TR_VEC_LIB} ${TR_BASE_LIB})
set( gl_sdk_libs android EGL GLESv2 )

add_library( ${TD_TXV_GLES2_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_TXV_GLES2_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_TXV_GLES2_LIB}.a)

add_library( ${TR_TXR_GL2_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TR_TXR_GL2_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TR_TXR_GL2_LIB}.a)

add_library( ${TR_RENDER_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TR_RENDER_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TR_RENDER_LIB}.a)

add_library( ${TR_VEC_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TR_VEC_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TR_VEC_LIB}.a)

add_library( ${TR_BASE_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TR_BASE_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TR_BASE_LIB}.a)

add_library( ${TH_UTF_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TH_UTF_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_UTF_LIB}.a)

#PDFium PART
add_library( ${TD_PDFIUMMODULE_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_PDFIUMMODULE_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_PDFIUMMODULE_LIB}.a)

add_library( ${TH_PDFIUM_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TH_PDFIUM_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_PDFIUM_LIB}.a)


add_library( ${TD_OLESSITEMHDR_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TD_OLESSITEMHDR_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TD_OLESSITEMHDR_LIB}.a)

add_library( ${TH_OLESS_LIB}
            STATIC
            IMPORTED )
set_target_properties(${TH_OLESS_LIB}
                      PROPERTIES IMPORTED_LOCATION
                      ${ODA_LINK_LIBRARIES}/lib${TH_OLESS_LIB}.a)


#
#

target_link_libraries(teigha_jni
                       ${TD_DR_EXLIB} ${TD_EXLIB} ${gl_libs}
                       ${TD_MODELER_LIB} ${TCOMPONENTS_BREPMODELER_LIB} ${TD_BREPBUILDERFILLER_LIB}
                       ${TD_BREPRENDERER_LIB} ${TD_BR_LIB} ${TD_ACISBLDR_LIB} ${TD_RASTERPROC_LIB} ${TD_RASTER_LIB}
                       ${TD_DBENT_LIB} ${TD_DBIO_LIB} ${TD_DB_LIB} ${TD_DBENT_LIB} ${TD_DBCORE_LIB} ${TD_DBIO_LIB} ${TD_SM_LIB} ${TD_AVE_LIB} ${TD_SCENEOE_LIB} ${TD_ACCAMERA_LIB}
                       ${TD_ISM_LIB} ${TD_WIPEOUT_LIB} ${TD_MPOLYGON_LIB} ${TD_ATEXT_LIB} ${TD_RTEXT_LIB}
                       ${TD_DB_LIB} ${TD_DBENT_LIB} ${PDFIUM_MODULE_LIBS}
                       ${TD_DBENT_LIB} ${TD_DBIO_LIB} ${TD_DB_LIB} ${TD_DBENT_LIB} ${TD_DBCORE_LIB} ${TD_DBIO_LIB} ${TD_SM_LIB} ${TD_AVE_LIB} ${TD_SCENEOE_LIB} ${TD_ACCAMERA_LIB}
                       ${TD_ISM_LIB} ${TD_WIPEOUT_LIB} ${TD_MPOLYGON_LIB} ${TD_ATEXT_LIB} ${TD_RTEXT_LIB}
                       ${TD_DB_LIB} ${TD_DBENT_LIB}
${TD_OLESSITEMHDR_LIB}
                       ${TD_DBROOT_LIB} ${TD_GS_LIB} ${TD_GI_LIB} ${TD_BREPBUILDER_LIB}
                       ${TD_GE_LIB} ${TD_SPATIALINDEX_LIB} ${TD_ROOT_LIB}
                        ${TH_THIRDPARTYRASTER_LIB} ${TH_ZLIB_LIB} ${TH_libXML} ${TD_RDIMBLK_LIB}
                       ${TH_FT_LIB} ${ODA_ADD_SISL} ${TH_WCHAR_LIB} ${TD_ALLOC_LIB} ${TH_OLESS_LIB}  ${TH_CONDITIONAL_LIBCRYPTO} ${TH_UTF_LIB} 
                       ${gl_sdk_libs} ${log-lib})


