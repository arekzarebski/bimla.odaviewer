#
#  ExCustObjs library
#

set (oda_excustobjs_reactors_src
    Reactors/DbgDatabaseReactor.cpp
    Reactors/cmd_setreactor.cpp
    Reactors/DbgTransactionReactor.cpp
    Reactors/DbgObjectReactor.cpp
    Reactors/DbgEventReactor.cpp
    )
SOURCE_GROUP("Source Files\\Reactors" FILES ${oda_excustobjs_reactors_src})

tdrawing_sources(${TD_EXCUSTOBJS_LIB}
    ${oda_excustobjs_reactors_src}
    ExCustObject.cpp
    ExSphere.cpp
    ExEmbeddedSolid.cpp
    ExCustEntity.cpp
    ExCustObjsModule.cpp
    DrxDebugCmds.cpp
    Reactors/DbgObjectReactor.h
    Reactors/DbgDatabaseReactor.h
    DbCmdMsgDef.h
    ExCustObjExport.h
    ExCustEntity.h
    ExCustObject.h
    DbCmdDef.h
    Reactors/DbgEventReactor.h
    ExSphere.h
    ExEmbeddedSolid.h
    Reactors/DbgTransactionReactor.h
    ExCustObjsModule.h
    Reactors/DbgReactorOut.h
    CommonTests.h
    CommonTests.cpp
    ExCreateBreps.h
    ExCreateBreps.cpp
		${TDRAWING_ROOT}/Examples/Common/ExSelectionUtils.h
		${TDRAWING_ROOT}/Examples/Common/ExSelectionUtils.cpp
    )

include_directories(
                    Reactors
                    ${TKERNEL_ROOT}/Exports/STLExport/Include
                    ${TDRAWING_ROOT}/Extensions/DbConstraints
                    ${TP_ROOT}/Include
										${TDRAWING_ROOT}/Examples/Common/
                   )

if(EXISTS ${TCOMPONENTS_ROOT}/OBJToolkit/Include/)
	include_directories(
		${TCOMPONENTS_ROOT}/OBJToolkit/Include/
	)
	add_definitions(-DOBJTOOLKIT_ENABLED)
endif(EXISTS ${TCOMPONENTS_ROOT}/OBJToolkit/Include/)

tdrawing_sources(${TD_EXCUSTOBJS_LIB}
                                StdAfx.cpp
                                ${TKERNEL_ROOT}/Include/OdaCommon.h
                )

if(ODA_SHARED AND MSVC)
tdrawing_sources(${TD_EXCUSTOBJS_LIB}
                ExCustObjs.rc
                )
endif(ODA_SHARED AND MSVC)

add_definitions(-DEXCUSTOBJS_DLL_EXPORTS)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

add_definitions(-DEXCOMMANDS_DLL_EXPORTS)

tdrawing_tx(${TD_EXCUSTOBJS_LIB}

  ${TD_DBROOT_LIB} ${TD_DB_LIB}
  ${TD_BREPBUILDER_LIB}
  ${TD_TF_LIB} ${TD_SPATIALINDEX_LIB} ${TD_GI_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_DB_CONSTRAINTS_LIB}
  ${TD_ALLOC_LIB}
)

tdrawing_project_group(${TD_EXCUSTOBJS_LIB} "Examples")
