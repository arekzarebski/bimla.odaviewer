#
#  OverrulingSample library
#

tdrawing_sources(${TD_OVERRULINGSAMPLE_LIB}
    OverrulingSample.cpp
    OverrulingSample.h
	)

include_directories()

if(ODA_SHARED AND MSVC)
tdrawing_sources(${TD_OVERRULINGSAMPLE_LIB}
				OverrulingSample.rc
				)
endif(ODA_SHARED AND MSVC)

tdrawing_tx(${TD_OVERRULINGSAMPLE_LIB} ${TD_DB_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB} )

tdrawing_project_group(OverrulingSample "Examples")
