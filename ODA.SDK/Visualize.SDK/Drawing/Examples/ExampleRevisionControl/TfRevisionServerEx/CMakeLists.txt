

if(ODA_SHARED AND MSVC)

tdrawing_sources(TfRevisionServerEx
  TfRevisionServerEx.cpp
  TfRevisionServerEx.rc
)

include_directories(${TDRAWING_ROOT}/Extensions/ZeroMQ
					${TH_ROOT}/zmq/include
					${TH_ROOT}/zmq/src
					${TDRAWING_ROOT}/Include/Gfx
					${TKERNEL_ROOT}/Extensions/ExServices
					${TDRAWING_ROOT}/Extensions/ExServices
					${TDRAWING_ROOT}/Extensions/ExServices/ExHostAppServices.h
					${TKERNEL_ROOT}/Extensions/ExServices/OdFileBuf.h
					${TDRAWING_ROOT}/Examples/ExampleRevisionControl/TfRevModule.h)


tdrawing_executable(TfRevisionServerEx ${TD_ALLOC_LIB} ${TD_DBROOT_LIB} ${TD_DB_LIB} ${TD_GE_LIB} ${TD_KEY_LIB}
      ${TD_TF_LIB} ${TD_ROOT_LIB} ${TH_ZMQ_LIB} ${TD_EXLIB} ${TD_DR_EXLIB}   )

tdrawing_project_group(TfRevisionServerEx "Examples")

endif(ODA_SHARED AND MSVC)

