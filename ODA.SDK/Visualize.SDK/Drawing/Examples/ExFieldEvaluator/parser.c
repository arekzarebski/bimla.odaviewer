/* A Bison parser, made by GNU Bison 3.0.5.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.5"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 2

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "AcExpr.y" /* yacc.c:339  */

#include "AcExpr.h"
#include "scanner.h"
#include "parser.h"
#include <math.h>

#line 73 "parser.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "parser.h".  */
#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    FLOAT_VALUE = 258,
    NUMBER = 259,
    LITERAL = 260,
    OBJ_ID = 261,
    CELL_ID = 262,
    UMINUS = 263,
    OBJECT = 264,
    TABLE = 265,
    EVALUATE = 266,
    PI = 267,
    E = 268,
    EXP10 = 269,
    PLD = 270,
    PLT = 271,
    DPL = 272,
    DPP = 273,
    ROT = 274,
    ILP = 275,
    NOR = 276,
    VEC = 277,
    VEC1 = 278,
    ILL = 279,
    DIST = 280,
    ANG = 281
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif



int yyparse (yyscan_t yyscanner, AcExprEvalResult* ptr);
/* "%code provides" blocks.  */
#line 20 "AcExpr.y" /* yacc.c:355  */

   YY_DECL;
   void yyerror (YYLTYPE* loc, yyscan_t yyscanner, AcExprEvalResult* ptr, const char* msg);

#line 165 "parser.c" /* yacc.c:355  */

#endif /* !YY_YY_PARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 171 "parser.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
             && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  51
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   470

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  41
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  10
/* YYNRULES -- Number of rules.  */
#define YYNRULES  56
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  189

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   281

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      34,    35,    29,    28,    33,    27,    32,    30,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    39,     2,
      38,     2,     2,     2,    40,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    36,     2,    37,    31,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    40,    40,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    64,    66,    68,    70,    71,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      85,    87,    89,    91,    93,    95,    99,   101,   102,   104,
     105,   107,   108,   110,   111,   112,   113
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "FLOAT_VALUE", "NUMBER", "LITERAL",
  "OBJ_ID", "CELL_ID", "UMINUS", "OBJECT", "TABLE", "EVALUATE", "PI", "E",
  "EXP10", "PLD", "PLT", "DPL", "DPP", "ROT", "ILP", "NOR", "VEC", "VEC1",
  "ILL", "DIST", "ANG", "'-'", "'+'", "'*'", "'/'", "'^'", "'.'", "','",
  "'('", "')'", "'['", "']'", "'<'", "':'", "'@'", "$accept", "start",
  "Expr", "$@1", "Vector", "TableRange", "TableSequenceElement",
  "TableSequence", "VectorComponent", "VectorFlags", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,    45,    43,    42,
      47,    94,    46,    44,    40,    41,    91,    93,    60,    58,
      64
};
# endif

#define YYPACT_NINF -100

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-100)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      55,  -100,  -100,   -23,  -100,    -9,    -6,  -100,  -100,    14,
      29,    32,    51,    58,    76,    85,    94,   107,   114,   115,
     123,   124,    55,    55,    55,   -28,    27,   439,  -100,   173,
      81,   106,    55,    55,    55,    55,    55,    55,    55,    55,
      55,    55,    55,    55,    55,  -100,  -100,   116,  -100,    92,
      55,  -100,    55,    55,    55,    55,    55,    45,   125,  -100,
    -100,   -15,    95,   127,   144,    96,   292,   299,   306,   313,
     320,   327,   334,   341,   348,   355,   -25,  -100,  -100,   439,
      -7,   108,   108,   119,   119,   119,   152,  -100,   173,  -100,
     131,   132,  -100,    55,    55,    55,    55,    55,    55,    55,
      55,    55,    55,    55,    55,  -100,    55,  -100,    55,  -100,
     439,  -100,   162,    50,   362,   369,   376,   383,   390,   397,
     175,   184,   193,   404,   202,   -14,    53,    13,  -100,  -100,
    -100,    55,    55,    55,    55,    55,    55,  -100,  -100,  -100,
      55,  -100,    55,  -100,    55,  -100,    55,  -100,    55,   147,
     211,   220,   229,   411,    78,   418,   425,    87,   171,   179,
     180,    55,  -100,  -100,  -100,    55,    55,  -100,    55,    55,
      55,  -100,  -100,  -100,  -100,   238,   247,   256,   432,   265,
     274,  -100,  -100,  -100,    55,  -100,  -100,   283,  -100
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    14,    29,     0,    28,     0,     0,    12,    13,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    53,     0,     2,    39,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    16,    17,     0,    55,    54,
      51,     1,     0,     0,     0,     0,     0,    28,    48,    47,
      49,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    18,    56,    52,
       0,    22,    21,    19,    20,    23,     0,     3,     0,    27,
       0,     0,     4,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     6,    51,    42,    51,    46,
      48,    50,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    15,    26,
      24,     0,     0,     0,     0,     0,     0,    32,    30,    31,
       0,     5,     0,     7,    51,    41,    51,    43,    51,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    33,    34,    35,     0,     0,    10,     0,     0,
       0,     8,    40,    44,    45,     0,     0,     0,     0,     0,
       0,    25,    36,    11,     0,    37,     9,     0,    38
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -100,  -100,     0,  -100,  -100,  -100,   130,  -100,   -99,  -100
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    26,    79,   149,    28,    59,    60,    61,    80,    50
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      27,    48,    52,    53,    54,    55,    56,   126,   104,   127,
     105,    29,    49,    52,    53,    54,    55,    56,    88,   142,
      89,   143,    45,    46,    47,    30,   106,    51,    31,    58,
     107,   108,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,   158,   146,   159,    32,   160,
     147,   148,    81,    82,    83,    84,    85,   129,     1,     2,
       3,   130,     4,    33,     5,     6,    34,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    86,    35,   144,    62,   110,    24,
     145,    25,    36,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,    52,    53,    54,    55,    56,
      37,   166,    63,   167,    52,    53,    54,    55,    56,    38,
     170,    78,   171,    52,    53,    54,    55,    56,    39,    93,
      90,   150,   151,   152,   153,   154,   155,    54,    55,    56,
     156,    40,   157,    52,    53,    54,    55,    56,    41,    42,
      56,    77,    52,    53,    54,    55,    56,    43,    44,   109,
      87,   175,    91,   112,   113,   176,   177,   128,   178,   179,
     180,    52,    53,    54,    55,    56,     1,     2,     3,    92,
      57,   161,     5,     6,   187,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    52,    53,    54,    55,    56,    24,   172,    25,
     137,    52,    53,    54,    55,    56,   173,   174,   111,   138,
      52,    53,    54,    55,    56,     0,     0,     0,   139,    52,
      53,    54,    55,    56,     0,     0,     0,   141,    52,    53,
      54,    55,    56,     0,     0,     0,   162,    52,    53,    54,
      55,    56,     0,     0,     0,   163,    52,    53,    54,    55,
      56,     0,     0,     0,   164,    52,    53,    54,    55,    56,
       0,     0,     0,   181,    52,    53,    54,    55,    56,     0,
       0,     0,   182,    52,    53,    54,    55,    56,     0,     0,
       0,   183,    52,    53,    54,    55,    56,     0,     0,     0,
     185,    52,    53,    54,    55,    56,     0,     0,     0,   186,
      52,    53,    54,    55,    56,     0,     0,     0,   188,    52,
      53,    54,    55,    56,     0,    94,    52,    53,    54,    55,
      56,     0,    95,    52,    53,    54,    55,    56,     0,    96,
      52,    53,    54,    55,    56,     0,    97,    52,    53,    54,
      55,    56,     0,    98,    52,    53,    54,    55,    56,     0,
      99,    52,    53,    54,    55,    56,     0,   100,    52,    53,
      54,    55,    56,     0,   101,    52,    53,    54,    55,    56,
       0,   102,    52,    53,    54,    55,    56,     0,   103,    52,
      53,    54,    55,    56,     0,   131,    52,    53,    54,    55,
      56,     0,   132,    52,    53,    54,    55,    56,     0,   133,
      52,    53,    54,    55,    56,     0,   134,    52,    53,    54,
      55,    56,     0,   135,    52,    53,    54,    55,    56,     0,
     136,    52,    53,    54,    55,    56,     0,   140,    52,    53,
      54,    55,    56,     0,   165,    52,    53,    54,    55,    56,
       0,   168,    52,    53,    54,    55,    56,     0,   169,    52,
      53,    54,    55,    56,     0,   184,    52,    53,    54,    55,
      56
};

static const yytype_int16 yycheck[] =
{
       0,    29,    27,    28,    29,    30,    31,   106,    33,   108,
      35,    34,    40,    27,    28,    29,    30,    31,    33,    33,
      35,    35,    22,    23,    24,    34,    33,     0,    34,    29,
      37,    38,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,   144,    33,   146,    34,   148,
      37,    38,    52,    53,    54,    55,    56,     7,     3,     4,
       5,    11,     7,    34,     9,    10,    34,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    39,    34,    33,     6,    88,    34,
      37,    36,    34,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,    27,    28,    29,    30,    31,
      34,    33,     6,    35,    27,    28,    29,    30,    31,    34,
      33,    29,    35,    27,    28,    29,    30,    31,    34,    33,
      35,   131,   132,   133,   134,   135,   136,    29,    30,    31,
     140,    34,   142,    27,    28,    29,    30,    31,    34,    34,
      31,    35,    27,    28,    29,    30,    31,    34,    34,     7,
      35,   161,    35,    32,    32,   165,   166,     5,   168,   169,
     170,    27,    28,    29,    30,    31,     3,     4,     5,    35,
       7,    34,     9,    10,   184,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    27,    28,    29,    30,    31,    34,    37,    36,
      35,    27,    28,    29,    30,    31,    37,    37,    88,    35,
      27,    28,    29,    30,    31,    -1,    -1,    -1,    35,    27,
      28,    29,    30,    31,    -1,    -1,    -1,    35,    27,    28,
      29,    30,    31,    -1,    -1,    -1,    35,    27,    28,    29,
      30,    31,    -1,    -1,    -1,    35,    27,    28,    29,    30,
      31,    -1,    -1,    -1,    35,    27,    28,    29,    30,    31,
      -1,    -1,    -1,    35,    27,    28,    29,    30,    31,    -1,
      -1,    -1,    35,    27,    28,    29,    30,    31,    -1,    -1,
      -1,    35,    27,    28,    29,    30,    31,    -1,    -1,    -1,
      35,    27,    28,    29,    30,    31,    -1,    -1,    -1,    35,
      27,    28,    29,    30,    31,    -1,    -1,    -1,    35,    27,
      28,    29,    30,    31,    -1,    33,    27,    28,    29,    30,
      31,    -1,    33,    27,    28,    29,    30,    31,    -1,    33,
      27,    28,    29,    30,    31,    -1,    33,    27,    28,    29,
      30,    31,    -1,    33,    27,    28,    29,    30,    31,    -1,
      33,    27,    28,    29,    30,    31,    -1,    33,    27,    28,
      29,    30,    31,    -1,    33,    27,    28,    29,    30,    31,
      -1,    33,    27,    28,    29,    30,    31,    -1,    33,    27,
      28,    29,    30,    31,    -1,    33,    27,    28,    29,    30,
      31,    -1,    33,    27,    28,    29,    30,    31,    -1,    33,
      27,    28,    29,    30,    31,    -1,    33,    27,    28,    29,
      30,    31,    -1,    33,    27,    28,    29,    30,    31,    -1,
      33,    27,    28,    29,    30,    31,    -1,    33,    27,    28,
      29,    30,    31,    -1,    33,    27,    28,    29,    30,    31,
      -1,    33,    27,    28,    29,    30,    31,    -1,    33,    27,
      28,    29,    30,    31,    -1,    33,    27,    28,    29,    30,
      31
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     7,     9,    10,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    34,    36,    42,    43,    45,    34,
      34,    34,    34,    34,    34,    34,    34,    34,    34,    34,
      34,    34,    34,    34,    34,    43,    43,    43,    29,    40,
      50,     0,    27,    28,    29,    30,    31,     7,    43,    46,
      47,    48,     6,     6,    43,    43,    43,    43,    43,    43,
      43,    43,    43,    43,    43,    43,    43,    35,    29,    43,
      49,    43,    43,    43,    43,    43,    39,    35,    33,    35,
      35,    35,    35,    33,    33,    33,    33,    33,    33,    33,
      33,    33,    33,    33,    33,    35,    33,    37,    38,     7,
      43,    47,    32,    32,    43,    43,    43,    43,    43,    43,
      43,    43,    43,    43,    43,    43,    49,    49,     5,     7,
      11,    33,    33,    33,    33,    33,    33,    35,    35,    35,
      33,    35,    33,    35,    33,    37,    33,    37,    38,    44,
      43,    43,    43,    43,    43,    43,    43,    43,    49,    49,
      49,    34,    35,    35,    35,    33,    33,    35,    33,    33,
      33,    35,    37,    37,    37,    43,    43,    43,    43,    43,
      43,    35,    35,    35,    33,    35,    35,    43,    35
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    41,    42,    43,    43,    43,    43,    43,    43,    43,
      43,    43,    43,    43,    43,    43,    43,    43,    43,    43,
      43,    43,    43,    43,    44,    43,    43,    43,    43,    43,
      43,    43,    43,    43,    43,    43,    43,    43,    43,    43,
      45,    45,    45,    45,    45,    45,    46,    47,    47,    48,
      48,    49,    49,    50,    50,    50,    50
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     4,     4,     6,     4,     6,     8,    10,
       8,    10,     1,     1,     1,     6,     2,     2,     3,     3,
       3,     3,     3,     3,     0,    10,     6,     4,     1,     1,
       6,     6,     6,     8,     8,     8,    10,    10,    12,     1,
       8,     6,     4,     6,     8,     8,     3,     1,     1,     1,
       3,     0,     1,     0,     1,     1,     2
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (&yylloc, yyscanner, ptr, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)                                \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;        \
          (Current).first_column = YYRHSLOC (Rhs, 1).first_column;      \
          (Current).last_line    = YYRHSLOC (Rhs, N).last_line;         \
          (Current).last_column  = YYRHSLOC (Rhs, N).last_column;       \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).first_line   = (Current).last_line   =              \
            YYRHSLOC (Rhs, 0).last_line;                                \
          (Current).first_column = (Current).last_column =              \
            YYRHSLOC (Rhs, 0).last_column;                              \
        }                                                               \
    while (0)
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K])


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL

/* Print *YYLOCP on YYO.  Private, do not rely on its existence. */

YY_ATTRIBUTE_UNUSED
static unsigned
yy_location_print_ (FILE *yyo, YYLTYPE const * const yylocp)
{
  unsigned res = 0;
  int end_col = 0 != yylocp->last_column ? yylocp->last_column - 1 : 0;
  if (0 <= yylocp->first_line)
    {
      res += YYFPRINTF (yyo, "%d", yylocp->first_line);
      if (0 <= yylocp->first_column)
        res += YYFPRINTF (yyo, ".%d", yylocp->first_column);
    }
  if (0 <= yylocp->last_line)
    {
      if (yylocp->first_line < yylocp->last_line)
        {
          res += YYFPRINTF (yyo, "-%d", yylocp->last_line);
          if (0 <= end_col)
            res += YYFPRINTF (yyo, ".%d", end_col);
        }
      else if (0 <= end_col && yylocp->first_column < end_col)
        res += YYFPRINTF (yyo, "-%d", end_col);
    }
  return res;
 }

#  define YY_LOCATION_PRINT(File, Loc)          \
  yy_location_print_ (File, &(Loc))

# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, Location, yyscanner, ptr); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, yyscan_t yyscanner, AcExprEvalResult* ptr)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (yylocationp);
  YYUSE (yyscanner);
  YYUSE (ptr);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, yyscan_t yyscanner, AcExprEvalResult* ptr)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, yyscanner, ptr);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule, yyscan_t yyscanner, AcExprEvalResult* ptr)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                       , &(yylsp[(yyi + 1) - (yynrhs)])                       , yyscanner, ptr);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, yylsp, Rule, yyscanner, ptr); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, yyscan_t yyscanner, AcExprEvalResult* ptr)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (yyscanner);
  YYUSE (ptr);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (yyscan_t yyscanner, AcExprEvalResult* ptr)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

/* Location data for the lookahead symbol.  */
static YYLTYPE yyloc_default
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
  = { 1, 1, 1, 1 }
# endif
;
YYLTYPE yylloc = yyloc_default;

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.
       'yyls': related to locations.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[3];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yylsp = yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  yylsp[0] = yylloc;
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;
        YYLTYPE *yyls1 = yyls;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yyls1, yysize * sizeof (*yylsp),
                    &yystacksize);

        yyls = yyls1;
        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
        YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, &yylloc, yyscanner, ptr);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location. */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  yyerror_range[1] = yyloc;
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 40 "AcExpr.y" /* yacc.c:1648  */
    { ptr->result = acexprCopyValue((yyvsp[0]), ptr); }
#line 1518 "parser.c" /* yacc.c:1648  */
    break;

  case 3:
#line 42 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Functor((yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1524 "parser.c" /* yacc.c:1648  */
    break;

  case 4:
#line 43 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Exp10((yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1530 "parser.c" /* yacc.c:1648  */
    break;

  case 5:
#line 44 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Distance((yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1536 "parser.c" /* yacc.c:1648  */
    break;

  case 6:
#line 45 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Angle1((yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1542 "parser.c" /* yacc.c:1648  */
    break;

  case 7:
#line 46 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Angle2((yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1548 "parser.c" /* yacc.c:1648  */
    break;

  case 8:
#line 47 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Angle3((yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1554 "parser.c" /* yacc.c:1648  */
    break;

  case 9:
#line 48 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Angle4((yyvsp[-7]), (yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1560 "parser.c" /* yacc.c:1648  */
    break;

  case 10:
#line 49 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Rot3((yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1566 "parser.c" /* yacc.c:1648  */
    break;

  case 11:
#line 50 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Rot4((yyvsp[-7]), (yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1572 "parser.c" /* yacc.c:1648  */
    break;

  case 12:
#line 51 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateDouble2(3.14159265358979323846, ptr); }
#line 1578 "parser.c" /* yacc.c:1648  */
    break;

  case 13:
#line 52 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateDouble2(0.0, ptr); /*E is interpreted as 0.0E+0*/}
#line 1584 "parser.c" /* yacc.c:1648  */
    break;

  case 14:
#line 53 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = (yyvsp[0]); }
#line 1590 "parser.c" /* yacc.c:1648  */
    break;

  case 15:
#line 55 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = ObjectEval((yyvsp[-3]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1596 "parser.c" /* yacc.c:1648  */
    break;

  case 16:
#line 56 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Neg((yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1602 "parser.c" /* yacc.c:1648  */
    break;

  case 17:
#line 57 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = (yyvsp[0]); }
#line 1608 "parser.c" /* yacc.c:1648  */
    break;

  case 18:
#line 58 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = (yyvsp[-1]); }
#line 1614 "parser.c" /* yacc.c:1648  */
    break;

  case 19:
#line 59 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Mul((yyvsp[-2]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1620 "parser.c" /* yacc.c:1648  */
    break;

  case 20:
#line 60 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Div((yyvsp[-2]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1626 "parser.c" /* yacc.c:1648  */
    break;

  case 21:
#line 61 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Add((yyvsp[-2]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1632 "parser.c" /* yacc.c:1648  */
    break;

  case 22:
#line 62 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Sub((yyvsp[-2]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1638 "parser.c" /* yacc.c:1648  */
    break;

  case 23:
#line 63 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Pow((yyvsp[-2]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1644 "parser.c" /* yacc.c:1648  */
    break;

  case 24:
#line 64 "AcExpr.y" /* yacc.c:1648  */
    { acexprPushImplicitTable((yyvsp[-3]), ptr); }
#line 1650 "parser.c" /* yacc.c:1648  */
    break;

  case 25:
#line 65 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = (yyvsp[-1]); acexprPopImplicitTable(ptr); if (!ptr->success) YYERROR;}
#line 1656 "parser.c" /* yacc.c:1648  */
    break;

  case 26:
#line 67 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = TableCell((yyvsp[-3]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1662 "parser.c" /* yacc.c:1648  */
    break;

  case 27:
#line 69 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = ImplicitTableEval((yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1668 "parser.c" /* yacc.c:1648  */
    break;

  case 28:
#line 70 "AcExpr.y" /* yacc.c:1648  */
    {(yyval) = ImplicitTableCell((yyvsp[0]), ptr); if (!ptr->success) YYERROR;}
#line 1674 "parser.c" /* yacc.c:1648  */
    break;

  case 29:
#line 71 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = (yyvsp[0]); }
#line 1680 "parser.c" /* yacc.c:1648  */
    break;

  case 30:
#line 73 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Vec((yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1686 "parser.c" /* yacc.c:1648  */
    break;

  case 31:
#line 74 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = UnitVec((yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1692 "parser.c" /* yacc.c:1648  */
    break;

  case 32:
#line 75 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Normal((yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1698 "parser.c" /* yacc.c:1648  */
    break;

  case 33:
#line 76 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Pld((yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1704 "parser.c" /* yacc.c:1648  */
    break;

  case 34:
#line 77 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Plt((yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1710 "parser.c" /* yacc.c:1648  */
    break;

  case 35:
#line 78 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Dpl((yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1716 "parser.c" /* yacc.c:1648  */
    break;

  case 36:
#line 79 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Dpp((yyvsp[-7]), (yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR; }
#line 1722 "parser.c" /* yacc.c:1648  */
    break;

  case 37:
#line 80 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Ill((yyvsp[-7]), (yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1728 "parser.c" /* yacc.c:1648  */
    break;

  case 38:
#line 81 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Ilp((yyvsp[-9]), (yyvsp[-7]), (yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), ptr); if (!ptr->success) YYERROR;}
#line 1734 "parser.c" /* yacc.c:1648  */
    break;

  case 39:
#line 82 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = (yyvsp[0]); }
#line 1740 "parser.c" /* yacc.c:1648  */
    break;

  case 40:
#line 86 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Vector((yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), (yyvsp[-6]), ptr); if (!ptr->success) YYERROR; }
#line 1746 "parser.c" /* yacc.c:1648  */
    break;

  case 41:
#line 88 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Vector((yyvsp[-3]), (yyvsp[-1]), acexprCreateDouble2(0.0, ptr), (yyvsp[-4]), ptr); if (!ptr->success) YYERROR; }
#line 1752 "parser.c" /* yacc.c:1648  */
    break;

  case 42:
#line 90 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = Vector((yyvsp[-1]), acexprCreateDouble2(0.0, ptr), acexprCreateDouble2(0.0, ptr), (yyvsp[-2]), ptr); }
#line 1758 "parser.c" /* yacc.c:1648  */
    break;

  case 43:
#line 92 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = PolarVector((yyvsp[-3]), (yyvsp[-1]), acexprCreateDouble2(0.0, ptr), (yyvsp[-4]), ptr); if (!ptr->success) YYERROR; }
#line 1764 "parser.c" /* yacc.c:1648  */
    break;

  case 44:
#line 94 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = PolarVector((yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), (yyvsp[-6]), ptr); if (!ptr->success) YYERROR; }
#line 1770 "parser.c" /* yacc.c:1648  */
    break;

  case 45:
#line 96 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = SphereVector((yyvsp[-5]), (yyvsp[-3]), (yyvsp[-1]), (yyvsp[-6]), ptr); if (!ptr->success) YYERROR; }
#line 1776 "parser.c" /* yacc.c:1648  */
    break;

  case 46:
#line 99 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateTableRange((yyvsp[-2]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR; }
#line 1782 "parser.c" /* yacc.c:1648  */
    break;

  case 48:
#line 102 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprConvertToRange((yyvsp[0]), ptr); }
#line 1788 "parser.c" /* yacc.c:1648  */
    break;

  case 50:
#line 105 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateTableSequence((yyvsp[-2]), (yyvsp[0]), ptr); if (!ptr->success) YYERROR; }
#line 1794 "parser.c" /* yacc.c:1648  */
    break;

  case 51:
#line 107 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateDouble2(0.0, ptr); }
#line 1800 "parser.c" /* yacc.c:1648  */
    break;

  case 52:
#line 108 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = (yyvsp[0]); }
#line 1806 "parser.c" /* yacc.c:1648  */
    break;

  case 53:
#line 110 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateInteger2(0, ptr); }
#line 1812 "parser.c" /* yacc.c:1648  */
    break;

  case 54:
#line 111 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateInteger2(1, ptr); }
#line 1818 "parser.c" /* yacc.c:1648  */
    break;

  case 55:
#line 112 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateInteger2(2, ptr); }
#line 1824 "parser.c" /* yacc.c:1648  */
    break;

  case 56:
#line 113 "AcExpr.y" /* yacc.c:1648  */
    { (yyval) = acexprCreateInteger2(3, ptr); }
#line 1830 "parser.c" /* yacc.c:1648  */
    break;


#line 1834 "parser.c" /* yacc.c:1648  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (&yylloc, yyscanner, ptr, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (&yylloc, yyscanner, ptr, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }

  yyerror_range[1] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, &yylloc, yyscanner, ptr);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;

      yyerror_range[1] = *yylsp;
      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, yylsp, yyscanner, ptr);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  yyerror_range[2] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, yyerror_range, 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (&yylloc, yyscanner, ptr, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, &yylloc, yyscanner, ptr);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, yylsp, yyscanner, ptr);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 116 "AcExpr.y" /* yacc.c:1907  */


void yyerror (YYLTYPE* loc, yyscan_t yyscanner, AcExprEvalResult* ptr, const char* msg)
{
#ifdef _DEBUG
  printf("Parse error %s, at %d", msg, loc->first_column);
#endif
}
