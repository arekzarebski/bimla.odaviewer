#
#  ExPointCloudHost module
#

tdrawing_sources(${TD_EXPOINTCLOUDHOST_LIB}    
    StdAfx.cpp
    ExPointCloudHostPE.cpp
    ExPointCloudExHostPE.cpp
    ExPointCloudHostModule.cpp

    StdAfx.h
    ExPointCloudHostPE.h
    ExPointCloudExHostPE.h
    ExPointCloudHostModule.h
  )

include_directories()

if(ODA_SHARED AND MSVC)
tdrawing_sources(${TD_EXPOINTCLOUDHOST_LIB}
        ExPointCloudHost.rc
        )
endif(ODA_SHARED AND MSVC)


if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

tdrawing_tx(${TD_EXPOINTCLOUDHOST_LIB} ${TD_DB_LIB} ${TD_DB_POINTCLOUDOBJ_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_ALLOC_LIB})

tdrawing_project_group(${TD_EXPOINTCLOUDHOST_LIB} "Examples")
