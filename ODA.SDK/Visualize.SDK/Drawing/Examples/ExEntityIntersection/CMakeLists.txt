#
#  ExDimAssocModule
#

tdrawing_sources(${EX_ENTITYINTERSECTION_LIB}
    ExEntityIntersectionModule.cpp
    ExDbCurveIntersectionPE.cpp
    ExEntityIntersectionModule.h
    ExDbCurveIntersectionPE.h
  )

include_directories()

if(MSVC)
tdrawing_sources(${EX_ENTITYINTERSECTION_LIB}
        ExEntityIntersectionModule.rc
        )
endif(MSVC)


if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

tdrawing_tx(${EX_ENTITYINTERSECTION_LIB} ${TD_DB_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB} )

tdrawing_project_group(${EX_ENTITYINTERSECTION_LIB} "Examples")
