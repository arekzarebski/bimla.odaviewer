	#
	#  iOdaApp xcodeproj
	#
	#
	cmake_minimum_required (VERSION 3.7)
	
	

	#set(ODA_PLATFORM iphone_11.0)
	set (ODA_BIN_DIR ${RootPath}/bin/${ODA_PLATFORM})
	link_directories(${ODA_BIN_DIR})
	set (ODA_LIB_DIR  ${RootPath}/lib/${ODA_PLATFORM})
	link_directories(${ODA_LIB_DIR})
	
	message( ${ODA_PLATFORM} )
	message( ${ODA_LIB_DIR} )

	include (${RootPath}/build/oda.cmake)
	set(TH_ROOT ${RootPath}/ThirdParty)
	set (TKERNEL_ROOT ${RootPath}/Kernel)
	set (TDRAWING_ROOT  ${RootPath}/Drawing)
	set (TCOMPONENTS_ROOT  ${RootPath}/Components)

	set(TCOMPONENTS_BREPMODELER_BUILD 1)

	include(${RootPath}/build/th.cmake)
	include(${RootPath}/build/th.components)
	include(${RootPath}/build/tkernel.cmake)
	include(${RootPath}/build/tkernel.components)
	include(${RootPath}/build/tdrawing.cmake)
	include(${RootPath}/build/tdrawing.components)
	include(${RootPath}/build/tcomponents.cmake)
	include(${RootPath}/build/tcomponents.components)

	set(DEVELOPMENT_PROJECT_NAME "iOdaApp")                     # e.g. project.xcodeproj
	set(DEVELOPMENT_TEAM_ID "4SVB9F72E4")                       
	set(APP_NAME "iOdaApp")                                     
	set(APP_BUNDLE_IDENTIFIER "com.opendesign.iOdaApp")                

	set(CODE_SIGN_IDENTITY "iPhone Developer")                 
                                                            # /usr/bin/env xcrun security find-identity -v -p codesigning
	set(DEPLOYMENT_TARGET 8.0)                                  # <== Set your deployment target version of iOS
	set(DEVICE_FAMILY "2")                                      # <== Set to "1" to target iPhone, set to "2" to target iPad, set to "1,2" to target both

	project(${DEVELOPMENT_PROJECT_NAME})


   # message(STATUS " OdBrepModeler = ${TCOMPONENTS_BREPMODELER_LIB}")	

	set(PRODUCT_NAME ${APP_NAME})
	set(EXECUTABLE_NAME ${APP_NAME})
	set(MACOSX_BUNDLE_EXECUTABLE_NAME ${APP_NAME})
	set(MACOSX_BUNDLE_INFO_STRING ${APP_BUNDLE_IDENTIFIER})
	set(MACOSX_BUNDLE_GUI_IDENTIFIER ${APP_BUNDLE_IDENTIFIER})
	set(MACOSX_BUNDLE_BUNDLE_NAME ${APP_BUNDLE_IDENTIFIER})
	set(MACOSX_BUNDLE_ICON_FILE "")
	set(MACOSX_BUNDLE_LONG_VERSION_STRING "1.0")
	set(MACOSX_BUNDLE_SHORT_VERSION_STRING "1.0")
	set(MACOSX_BUNDLE_BUNDLE_VERSION "1.0")
	set(MACOSX_BUNDLE_COPYRIGHT "Copyright YOU")
	set(MACOSX_DEPLOYMENT_TARGET ${DEPLOYMENT_TARGET})
	
	
	

	set(APP_HEADER_FILES
		./AppDelegate.h
		./ViewController.h
		./DwgViewController.h
		${RootPath}/Drawing/Examples/iOS/iCommon/ExHostAppServices.h
		${RootPath}/Drawing/Examples/iOS/iCommon/iOdaAppServices.h
		${RootPath}/Drawing/Examples/iOS/iCommon/iOdaApplication.h
    	${RootPath}/Drawing/Examples/iOS/iCommon/iUtils.h
    	${RootPath}/Drawing/Examples/iOS/iCommon/iStrConv.h
    	${RootPath}/Drawing/Extensions/ExServices/ExHostAppServices.h
    	${RootPath}/Drawing/Extensions/ExServices/ExDbCommandContext.h
    	${RootPath}/Drawing/Extensions/ExServices/ExPageController.h
    	${RootPath}/Drawing/Extensions/ExServices/ExFileUndoController.h
    	${RootPath}/Drawing/Extensions/ExServices/ExUndoController.h
	)

	set(APP_SOURCE_FILES
		./AppDelegate.m
    	./ViewController.mm
    	./DwgViewController.mm
    	./main.m
    	${RootPath}/Drawing/Examples/iOS/iCommon/iOdaAppServices.cpp
    	${RootPath}/Drawing/Examples/iOS/iCommon/iOdaApplication.mm
    	${RootPath}/Drawing/Examples/iOS/iCommon/iUtils.m
    	${RootPath}/Drawing/Examples/iOS/iCommon/iStrConv.mm
    	${RootPath}/Drawing/Extensions/ExServices/ExDbCommandContext.cpp
    	${RootPath}/Drawing/Extensions/ExServices/ExPageController.cpp
    	${RootPath}/Drawing/Extensions/ExServices/ExFileUndoController.cpp
    	${RootPath}/Drawing/Extensions/ExServices/ExUndoController.cpp
    	${RootPath}/Drawing/Extensions/ExServices/ExHostAppServices.cpp 
	)


		#file(GLOB_RECURSE RES_SOURCES "${SOURCE_ROOT}/data-folder/*")
		#add_executable(${PROJECT_NAME} MACOSX_BUNDLE ${SRC} ${RES_SOURCES})
		#SET_SOURCE_FILES_PROPERTIES(${RES_SOURCES} PROPERTIES MACOSX_PACKAGE_LOCATION Resources
		
		file(GLOB_RECURSE STORYB_FILES *.storyboard)
		set_source_files_properties(${STORYB_FILES} PROPERTIES MACOSX_PACKAGE_LOCATION Resources)

	add_executable(
    	${APP_NAME}
    	MACOSX_BUNDLE
    	${APP_HEADER_FILES}
    	${APP_SOURCE_FILES}
    	${STORYB_FILES}
	)

	target_include_directories(${APP_NAME}
 		PUBLIC ${RootPath}/Drawing/Examples/iOS/iCommon
	)

 
 	target_include_directories(${APP_NAME}
   		PUBLIC ${RootPath}/Kernel/Include
 	)
 
 
 	target_include_directories(${APP_NAME}
   		PUBLIC ${RootPath}/Drawing/Include
 	)
 
 	target_include_directories(${APP_NAME}
   		PUBLIC ${RootPath}/Kernel/Extensions/ExServices
 	)
 
 	target_include_directories(${APP_NAME}
   		PUBLIC ${RootPath}/ThirdParty/Activation
 	)
 	
 	target_include_directories(${APP_NAME}
   		PUBLIC ${RootPath}/Components/BrepModeler/Include
 	)
  
	find_library(GLKIT GLKit)
	find_library(FOUNDATION Foundation)
	find_library(CF QuartzCore)
	find_library(OPENGLES OpenGLES)
	find_library(UIKIT UIKit)

        set(PDFIUM_MODULE_LIBS ${TD_PDFIUMMODULE_LIB} ${TH_PDFIUM_LIB} ${TH_FT_LIB} "-framework CoreGraphics")
        #message(STATUS "PDFIUM module is enabled, ${FOUNDATION}")
        add_definitions(-DPDFIUM_MODULE_ENABLED)

	target_link_libraries(${APP_NAME} PUBLIC 
		${TH_ZLIB_LIB}
		${TH_FT_LIB}
		${TD_ATEXT_LIB}
		${TD_DBCORE_LIB}
		${TD_RTEXT_LIB}
		${TD_DB_LIB}
		${TD_GE_LIB}
		${TD_GS_LIB}
		${TD_GI_LIB}
		${TD_SPATIALINDEX_LIB}
		${TD_EXLIB}
		${TD_RDIMBLK_LIB}
		${TD_RASTERPROC_LIB}
		${TD_TXV_GLES2_LIB}
		${TD_MODELERCOMMANDS_LIB}
		${TD_RASTER_LIB}
		${TD_DBROOT_LIB}
		${TH_THIRDPARTYRASTER_LIB}
		${TD_BREPBUILDERFILLER_LIB}
		${TCOMPONENTS_BREPMODELER_LIB}
		${TD_BR_LIB}
		${TD_BREPRENDERER_LIB}
		${TD_ACISBLDR_LIB}
		${TD_DR_EXLIB}
		${TR_VEC_LIB}
		${TR_BASE_LIB}
		${TD_BREPBUILDER_LIB}
		${TD_MODELER_LIB}
		${TH_LIBCRYPTO_LIB}
		${TR_TXR_GL2_LIB}
                ${TR_RENDER_LIB}
		${TD_DBENT_LIB}
		${TD_DBIO_LIB}
		${TD_ISM_LIB}
		${TD_WIPEOUT_LIB}
		${TD_MPOLYGON_LIB}
		${TD_ACCAMERA_LIB}
		${TD_SCENEOE_LIB}
		${TH_UTF_LIB}
                ${PDFIUM_MODULE_LIBS}
		${TD_ROOT_LIB}
		${TD_ALLOC_LIB}
		${GLKIT} ${FOUNDATION} ${OPENGLES} ${UIKIT} ${CF}
                )

		# Turn on ARC
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fobjc-arc")

		# Create the app target
		set_target_properties(${APP_NAME} PROPERTIES
                      XCODE_ATTRIBUTE_DEBUG_INFORMATION_FORMAT "dwarf-with-dsym"
                      RESOURCE "${RESOURCES}"
                      XCODE_ATTRIBUTE_GCC_PRECOMPILE_PREFIX_HEADER "YES"
                      XCODE_ATTRIBUTE_IPHONEOS_DEPLOYMENT_TARGET ${DEPLOYMENT_TARGET}
                      XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY ${CODE_SIGN_IDENTITY}
                      XCODE_ATTRIBUTE_DEVELOPMENT_TEAM ${DEVELOPMENT_TEAM_ID}
                      XCODE_ATTRIBUTE_TARGETED_DEVICE_FAMILY ${DEVICE_FAMILY}
                      MACOSX_BUNDLE_INFO_PLIST ${CMAKE_CURRENT_SOURCE_DIR}/plist.in
                      XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_ARC YES
                      XCODE_ATTRIBUTE_COMBINE_HIDPI_IMAGES NO
                      XCODE_ATTRIBUTE_INSTALL_PATH "$(LOCAL_APPS_DIR)"
                      XCODE_ATTRIBUTE_ENABLE_TESTABILITY YES
                      XCODE_ATTRIBUTE_GCC_SYMBOLS_PRIVATE_EXTERN YES
		)

		# Set the app's linker search path to the default location on iOS
		set_target_properties(
    		${APP_NAME}
    		PROPERTIES
    		XCODE_ATTRIBUTE_LD_RUNPATH_SEARCH_PATHS
    		"@executable_path/Frameworks"
		)



