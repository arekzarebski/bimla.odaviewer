/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#include "OdaCommon.h"
#include "DbGeoMapFieldsPE.h"
#include "DbGeoMap.h"
#include "DbWipeout.h"
ODRX_CONS_DEFINE_MEMBERS(OdDbGeoMapFieldsPE, OdRxObject, RXIMPL_CONSTR);

//getters
OdInt8 OdDbGeoMapFieldsPE::getLOD(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_LOD;
}
bool OdDbGeoMapFieldsPE::getIsOutOfDate(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_bOutOfDate;
}
const OdBinaryData & OdDbGeoMapFieldsPE::getPixelData(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_PixelData;
}
const OdGePoint2dArray & OdDbGeoMapFieldsPE::getPtClipBnd(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_ptClipBnd;
}
const OdStringArray & OdDbGeoMapFieldsPE::getStrings(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_strings;
}
const OdGeVector3d & OdDbGeoMapFieldsPE::getvU(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_vU;
}
const OdGeVector3d & OdDbGeoMapFieldsPE::getvV(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_vV;
}
double OdDbGeoMapFieldsPE::getTextHeight(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_dTextHeight;
}

OdCmColor OdDbGeoMapFieldsPE::getTextColor(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_textColor;
}

OdInt32 OdDbGeoMapFieldsPE::getHeight(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_nHeight;
}
OdInt32 OdDbGeoMapFieldsPE::getWidth(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_nWidth;
}
double OdDbGeoMapFieldsPE::getImageWidth(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_dImageWidth;
}
double OdDbGeoMapFieldsPE::getImageHeight(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_dImageHeight;
}
const OdGePoint3d & OdDbGeoMapFieldsPE::getPtTextPosition(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_ptTextPosition;
}
const OdGePoint3d & OdDbGeoMapFieldsPE::getPtImageBottomLeft(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_ptImageBottomLeft;
}
OdInt8 OdDbGeoMapFieldsPE::getResolution(OdDbGeoMap * pGeoMap) const
{
  if (pGeoMap == NULL)
  {
    throw OdError(eNullPtr);
  }
  pGeoMap->assertReadEnabled();
  return pGeoMap->m_Resolution;
}

//setters
OdResult OdDbGeoMapFieldsPE::setLOD(OdDbGeoMap * pGeoMap, OdInt8 LOD) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_LOD = LOD;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setIsOutOfDate(OdDbGeoMap * pGeoMap, bool bIsOutOfDate) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_bOutOfDate = bIsOutOfDate;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setPixelData(OdDbGeoMap * pGeoMap, const OdBinaryData & pixelData) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_PixelData = pixelData;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setPtClipBnd(OdDbGeoMap * pGeoMap, const OdGePoint2dArray & ptClipBnd) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_ptClipBnd = ptClipBnd;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setStrings(OdDbGeoMap * pGeoMap, const OdStringArray & strings) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_strings = strings;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setvU(OdDbGeoMap * pGeoMap,const OdGeVector3d & vU) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_vU = vU;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setvV(OdDbGeoMap * pGeoMap, const OdGeVector3d & vV) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_vV = vV;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setTextHeight(OdDbGeoMap * pGeoMap, double dTextHeight) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_dTextHeight = dTextHeight;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setTextColor(OdDbGeoMap * pGeoMap, const OdCmColor& cColor) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_textColor = cColor;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setHeight(OdDbGeoMap * pGeoMap, OdInt32 height) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_nHeight = height;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setWidth(OdDbGeoMap * pGeoMap, OdInt32 width) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_nWidth = width;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setImageWidth(OdDbGeoMap * pGeoMap, double dImageWidth) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_dImageWidth = dImageWidth;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setImageHeight(OdDbGeoMap * pGeoMap, double dImageHeight) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_dImageHeight = dImageHeight;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setPtTextPosition(OdDbGeoMap * pGeoMap, const OdGePoint3d & ptTextPosition) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_ptTextPosition = ptTextPosition;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setPtImageBottomLeft(OdDbGeoMap * pGeoMap, const OdGePoint3d & ptImageBottomLeft) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_ptImageBottomLeft = ptImageBottomLeft;
  return eOk;
}
OdResult OdDbGeoMapFieldsPE::setResolution(OdDbGeoMap * pGeoMap, OdInt8 Resolution) const
{
  if (pGeoMap == NULL)
  {
    return eNullPtr;
  }
  pGeoMap->assertWriteEnabled();
  pGeoMap->m_Resolution = Resolution;
  return eOk;
}






OdResult OdDbGeoMapGripPointsPE::getGripPoints(const OdDbEntity* ent, OdGePoint3dArray& gripPoints)const
{
  OdDbRasterImagePtr pImg = ent;
  OdGeExtents3d exts;
  OdResult res = pImg->getGeomExtents(exts);
  if (eOk == res)
  {
    unsigned int size = gripPoints.size();
    if (false)//!pImg->isClipped() || !pImg->isSetDisplayOpt(OdDbRasterImage::kClip))
    {
      gripPoints.resize(size + 5);
      gripPoints[size] = exts.minPoint() + (exts.maxPoint() - exts.minPoint()) / 2.;

      OdGePoint3d origin;
      OdGeVector3d u, v;
      pImg->getOrientation(origin, u, v);

      gripPoints[size + 1] = origin;
      gripPoints[size + 2] = origin + v;
      gripPoints[size + 3] = origin + u + v;
      gripPoints[size + 4] = origin + u;
    }
    else
    {
      OdGePoint3dArray clipPoints;
      pImg->getVertices(clipPoints);

      //IO_TRACE("clipPoints:\n  %f:%f\n  %f:%f\n  %f:%f\n  %f:%f\n", clipPoints[0].x, clipPoints[0].y, clipPoints[1].x, clipPoints[1].y, clipPoints[2].x, clipPoints[2].y, clipPoints[3].x, clipPoints[3].y);


      if (clipPoints.last() == clipPoints.first())
        clipPoints.removeLast();
      gripPoints.append(clipPoints);
    }
  }
  return res;
}

#if !defined(__sun)
inline double quad(double x) { return x * x; }
#else
inline double sun_quad(double x) { return x * x; }
#endif

//static void IO_TRACE(const char* lpszFormat, ...)
//{
//
//  va_list args;
//  va_start(args, lpszFormat);
//
//  int nBuf;
//  static char szBuffer[512];
//  nBuf = vsnprintf(szBuffer, sizeof(szBuffer), lpszFormat, args);
//  OutputDebugStringA(szBuffer);
//  va_end(args);
//}
//
template <class P>
double angle(P a, P b)
{
  return OD_ATAN2(a.y - b.y, a.x - b.x);
}

OdResult OdDbGeoMapGripPointsPE::moveGripPointsAt(OdDbEntity* pEnt, const OdIntArray& indices, const OdGeVector3d& offset)
{
  unsigned size = indices.size();
  if (size == 0)
    return eOk;
  if( offset.length()==0)
    return eOk;

  OdDbGeoMapPtr pImg = pEnt;
  int capture = indices[0];
  capture = 3 & (capture - pImg->finalize());

  OdGePoint3dArray arr;
  pImg->getVertices(arr);

  double z = pImg->bottomLeftPt().z;
  OdGePoint3d& m_ptBottomLeft  = arr[0];
  OdGePoint3d& m_ptBottomRight = arr[1];
  OdGePoint3d& m_ptUpperRight  = arr[2];
  OdGePoint3d& m_ptUpperLeft   = arr[3];


  
  double alfa = angle(m_ptBottomRight, m_ptBottomLeft); 
  double alfaoffset= OD_ATAN2(offset.y, offset.x);
  double offsetlen = offset.length();
  double offsetlenright = offsetlen *sin (alfaoffset - alfa);
  double offsetlenleft  = offsetlen *cos (alfaoffset - alfa);
 
  double s = sin(alfa);
  double c = cos(alfa);
  OdGeVector3d offsetright; 
  OdGeVector3d offsetleft; 
  int pred = 3&(capture - 1);
  int next = 3&(capture + 1);
  int stable = 3&(next + 1);
  
  if (capture == 0 || capture == 2)
  {
    offsetright = OdGeVector3d(-offsetlenright * s, offsetlenright*c, z);
    offsetleft  = OdGeVector3d(offsetlenleft*c, offsetlenleft*s, z);
    //IO_TRACE("offsetright: %f %f offsetleft: %f %f \n", offsetright.x, offsetright.y, offsetleft.x, offsetleft.y);
    //IO_TRACE("offsetlenright: %f  offsetlenleft: %f w4,h4 %f %f \n", offsetlenright, offsetlenleft,w4,h4);
  }
  else
  {
     offsetleft  = OdGeVector3d(-offsetlenright * s, offsetlenright*c, z);
     offsetright = OdGeVector3d(offsetlenleft*c, offsetlenleft*s, z);
     //IO_TRACE("offsetright: %f %f offsetleft: %f %f \n", offsetright.x, offsetright.y, offsetleft.x, offsetleft.y);
     //IO_TRACE("offsetlenright: %f  offsetlenleft: %f w4,h4 %f %f \n", offsetlenright, offsetlenleft, w4, h4);
  }

  /// find quadrant where capture point in
  OdGePoint3d capt = arr[capture] + offset;
  double s1 = (arr[stable] - arr[pred]).dotProduct(arr[stable] - capt);
  double s2 = (arr[stable] - capt).dotProduct(arr[stable] - arr[next]);
  //IO_TRACE(" s1 s2 = %f %f \n",s1,s2);


  offsetleft = offsetleft + (arr[pred] - arr[stable]);
  offsetright = offsetright + (arr[next] - arr[stable]);
  
  /// emulate strange autocad behavior
  if (s1 < 0)offsetleft = -offsetleft;
  if (s2 < 0)offsetright = -offsetright;
  /// new grip point
  arr[pred] = arr[stable] + offsetleft;
  arr[next] = arr[stable] + offsetright;
  arr[capture] = arr[stable] + offsetleft + offsetright;

  double le, ri, bo, to;
  getBBrect(m_ptBottomLeft, m_ptBottomRight, m_ptUpperRight, m_ptUpperLeft, le, ri, bo, to);

  // width height BB
  double h2 = to - bo;
  double w2 = ri - le;
  ///  new value w4 h4!!!!
#if !defined(__sun)
  double w4 = sqrt(quad(m_ptBottomLeft.y - m_ptBottomRight.y) + quad(m_ptBottomLeft.x - m_ptBottomRight.x));
  double h4 = sqrt(quad(m_ptBottomLeft.y - m_ptUpperLeft.y) + quad(m_ptBottomLeft.x - m_ptUpperLeft.x));
#else
  double w4 = sqrt(sun_quad(m_ptBottomLeft.y - m_ptBottomRight.y) + sun_quad(m_ptBottomLeft.x - m_ptBottomRight.x));
  double h4 = sqrt(sun_quad(m_ptBottomLeft.y - m_ptUpperLeft.y) + sun_quad(m_ptBottomLeft.x - m_ptUpperLeft.x));
#endif
  double w5 = pImg->imageWidth() / pImg->imageSize().x;
  double h5 = pImg->imageWidth() / pImg->imageSize().y;
  ///  if w h very small application crashed.  set minimal w h
  if (w4 < w5 || h4 <h5 )
  {
    if (w4 < w5)
    {
      w4 = w5;
      m_ptBottomRight = m_ptBottomLeft + OdGeVector3d(w4 * c, w4*s, z);
      m_ptUpperRight = m_ptUpperLeft   + OdGeVector3d(w4 * c, w4*s, z);
    }
    if (h4 < h5)
    {
      h4 = h5;
      m_ptBottomLeft = m_ptUpperLeft + OdGeVector3d(-h4 * s, h4*c, z);
      m_ptBottomRight = m_ptUpperRight + OdGeVector3d(-h4 * s, h4*c, z);
    }
    getBBrect(m_ptBottomLeft, m_ptBottomRight, m_ptUpperRight, m_ptUpperLeft, le, ri, bo, to);
    h2 = to - bo;
    w2 = ri - le;
  }

  // center
  double cx = (ri + le)*0.5;
  double cy = (to + bo)*0.5;
  ///  magic 1.02
  double m_dImageWidth  = w2 * 1.02;
  double m_dImageHeight = h2 * 1.02;
  //IO_TRACE("moveGripPointsAt w=%f, h=%f\n", m_dImageWidth, m_dImageHeight);
  
  OdGePoint3d m_ptImageBottomLeft(cx- m_dImageWidth*0.5,cy- m_dImageHeight * 0.5,z);
  OdGeVector3d u(m_dImageWidth,0,0), v(0,m_dImageHeight,0);

  //IO_TRACE("before %10.0f:%10.0f\n  %10.0f:%10.0f\n  %10.0f:%10.0f\n  %10.0f:%10.0f\n  \n", m_ptBottomLeft.x, m_ptBottomLeft.y, m_ptBottomRight.x, m_ptBottomRight.y, m_ptUpperRight.x, m_ptUpperRight.y, m_ptUpperLeft.x, m_ptUpperLeft.y);
  
  
  pImg->setOrientation(m_ptImageBottomLeft, u , v);
  return eOk;
}

OdResult OdDbGeoMapGripPointsPE::getStretchPoints(const OdDbEntity* ent, OdGePoint3dArray& stretchPoints) const
{
  return getGripPoints(ent, stretchPoints);
}

OdResult OdDbGeoMapGripPointsPE::moveStretchPointsAt(OdDbEntity* ent, const OdIntArray& indices, const OdGeVector3d& offset)
{
  return moveGripPointsAt(ent, indices, offset);
}

OdResult OdDbGeoMapGripPointsPE::getOsnapPoints(const OdDbEntity* ent, OdDb::OsnapMode osnapMode, OdGsMarker gsSelectionMark,
  const OdGePoint3d& pickPoint, const OdGePoint3d& lastPoint, const OdGeMatrix3d& viewXform,
  OdGePoint3dArray& snapPoints) const
{
  switch (osnapMode)
  {
  case OdDb::kOsModeEnd:
  case OdDb::kOsModeCen:
  {
    OdGePoint3dArray gripPoints;
    getGripPoints(ent, gripPoints);
    if (gripPoints.size() > 0)
    {
      if (osnapMode == OdDb::kOsModeEnd)
        gripPoints.erase(gripPoints.begin());
      else
        gripPoints.erase(gripPoints.begin() + 1, gripPoints.end());
      snapPoints.append(gripPoints);
    }
  }
  break;
  default:
    break;
  }
  return eOk;
}
