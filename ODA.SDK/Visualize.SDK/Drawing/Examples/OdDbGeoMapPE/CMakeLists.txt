#
#  OdDbGeoMapPE module
#

tdrawing_sources(${TD_GEOMAPPE_LIB}
  DbGeoMapPEImpl.cpp
	DbGeoMapPEImpl.h
	GeoMapPEModule.cpp
	GeoMapPEModule.h
)

include_directories(
  ${TDRAWING_ROOT}/Examples/GeolocationObj
)

if(ODA_SHARED AND MSVC)
tdrawing_sources(${TD_GEOMAPPE_LIB}
				OdDbGeoMapPE.rc
				)
endif(ODA_SHARED AND MSVC)

if(NOT ODA_SHARED)
add_definitions(-DCURL_STATICLIB)
endif(NOT ODA_SHARED)

tdrawing_tx(${TD_GEOMAPPE_LIB} ${TD_DB_LIB} ${TH_CURL_LIB} ${TD_GEOLOCATIONOBJ_LIB} ${TH_TINYXML_LIB})

tdrawing_project_group(${TD_GEOMAPPE_LIB} "Examples")