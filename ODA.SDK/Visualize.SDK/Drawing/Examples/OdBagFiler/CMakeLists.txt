#
#  OdBagFilerEx executable
#

tdrawing_sources(OdBagFilerEx
	OdBagFilerEx.cpp
	)

include_directories(
					${TKERNEL_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Extensions/ExServices
					../Common)

if(ODA_SHARED)
set ( od_bag_filer_libs  ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_KEY_LIB} ${TD_DB_LIB} ${TD_GS_LIB}  ${TD_GI_LIB} 
                         ${TD_ROOT_LIB} ${TD_GE_LIB} ${TD_DBROOT_LIB}
)
else(ODA_SHARED)
set ( od_bag_filer_libs  ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_DB_LIB} ${TD_DBROOT_LIB} ${TD_GS_LIB} 
                         ${TD_GI_LIB} ${TD_GE_LIB} ${TD_SPATIALINDEX_LIB} ${TD_ROOT_LIB} ${TH_CONDITIONAL_LIBCRYPTO} ${TH_UTF_LIB})
if (NOT WINTTF)
set ( od_bag_filer_libs  ${od_bag_filer_libs}
				         ${TH_FT_LIB})
endif (NOT WINTTF)
endif(ODA_SHARED)

if(MSVC)
tdrawing_sources(OdBagFilerEx
				  OdBagFilerEx.rc
				 )
endif(MSVC)

tdrawing_executable(OdBagFilerEx ${od_bag_filer_libs} ${TD_ALLOC_LIB} ) 

tdrawing_project_group(OdBagFilerEx "Examples")
