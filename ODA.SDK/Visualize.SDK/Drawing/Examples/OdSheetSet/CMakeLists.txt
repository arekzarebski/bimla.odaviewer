#
#  OdSheetSet executable
#

tdrawing_sources(OdSheetSet
	OdSheetSet.cpp
	FormatControl.h
	${TKERNEL_ROOT}/Extensions/ExServices/ExSystemServices.h
	${TKERNEL_ROOT}/Extensions/ExServices/OdFileBuf.h
	)

include_directories(
					${TKERNEL_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Extensions/ExServices
					../Common)

if(ODA_SHARED)
set ( od_sheet_set_libs  ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_KEY_LIB} ${TD_SM_LIB} ${TD_DB_LIB} ${TD_GS_LIB}  ${TD_GI_LIB}
                         ${TD_ROOT_LIB} ${TD_GE_LIB} ${TD_DBROOT_LIB}
)
else(ODA_SHARED)
set ( od_sheet_set_libs ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_SPATIALINDEX_LIB} ${TD_GS_LIB} ${TD_SM_LIB}
                        ${TD_DB_LIB} ${TD_DBROOT_LIB} ${TD_GE_LIB} ${TD_GI_LIB} ${TD_GS_LIB} ${TD_ROOT_LIB}
                        ${TH_THIRDPARTYRASTER_LIB} ${TH_UTF_LIB} ${TH_TINYXML_LIB} ${TH_CONDITIONAL_LIBCRYPTO}
)
if (NOT WINTTF)
set ( od_sheet_set_libs  ${od_sheet_set_libs}  ${TH_FT_LIB}
)
endif (NOT WINTTF)
if(WINCE)
set ( od_sheet_set_libs Ole32.lib ${od_sheet_set_libs} )
endif(WINCE)
endif(ODA_SHARED)
if(MSVC)
tdrawing_sources(OdSheetSet
				  OdSheetSet.rc
				 )
endif(MSVC)


tdrawing_executable(OdSheetSet ${od_sheet_set_libs} ${TD_ALLOC_LIB}) 

tdrawing_project_group(OdSheetSet "Examples")
