/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
//
// BimDbCommands.cpp
//

#include "BimCommon.h"
#include "BimDbModuleImpl.h"
#include "Ed/EdCommandContext.h"
#include "Ed/EdUserIO.h"
#include "PropServices.h"
#include "RxVariantValue.h"

//#include "BmImport.h" // Drawing/Imports/BimImport
//using namespace TD_Bim_IMPORT;

void OdqCmd_BimImport::execute(OdEdCommandContext* pCmdCtx)
{
  throw OdError(eNotApplicable);

  //OdDbDatabasePtr pDwgDb = OdDbDatabase::cast(pCmdCtx->baseDatabase());
  //if (pDwgDb.isNull())
  //  throw OdError(eNoDatabase);
  //OdDbHostAppServices* pServices = pDwgDb->appServices();
  //ODA_ASSERT_ONCE(pServices);
  //bool bFileDialog = pServices->getFILEDIA();

  //OdRxModulePtr pBimImport = ::odrxDynamicLinker()->loadModule(OD_T("TD_BimImport.tx"));
  //if (pBimImport.isNull())
  //{
  //  pServices->warning(OD_T("TD_BimImport.tx was not found"));
  //  throw OdError(eNotApplicable);
  //}
  //OdPropServicesPtr pPropServices = OdPropServices::cast(pCmdCtx);
  //ODA_ASSERT_ONCE(!pPropServices.isNull());

  //OdString sPath,
  //         sMsg;
  //if (!bFileDialog || (!pPropServices.isNull() && pPropServices->isInputStringReady(pCmdCtx)))
  //{
  //  sPath = pCmdCtx->userIO()->getString(OD_T("Enter name of Bim file to import"));
  //  if (!sPath.isEmpty() && !odrxSystemServices()->accessFile(sPath, Oda::kFileRead))
  //  {
  //    sMsg.format(L"File \"%1s\" was not found", sPath.c_str());
  //    pServices->warning(sMsg);
  //    return;
  //  }
  //}

  //static OdString s_sExtension;
  //if (sPath.isEmpty())
  //{
  //  sPath = pCmdCtx->userIO()->getFilePath(OD_T("Enter name of drawing to import"), 
  //                                         OdEd::kGfpForOpen, 
  //                                         OD_T("Select File to import"), // dialog caption 
  //                                         OD_T("Bim"), // default extension
  //                                         OdString::kEmpty, // file name
  //                                         OD_T("Bim files (*.Bim)|*.Bim||"));
  //  if (sPath.isEmpty())
  //    return; // without message "Cancel by user" // throw OdEdCancel();
  //}

  //// Create importer object
  ////OdStaticRxObject<OdExBimHostAppServices> dgSvc;
  //OdBimImportPtr importer = createBimImporter();
  //if (importer.isNull())
  //{
  //  pServices->warning(OD_T("Could not create Bim importer"));
  //  return;
  //}

  //// Set the conversion parameters
  //importer->properties()->putAt(L"Services", pServices);
  //importer->properties()->putAt(L"BimPath", OdRxVariantValue(sPath));
  //appBimServices()->disableProgressMeterOutput(true);
  //importer->properties()->putAt(L"BimServices", appBimServices());
  //importer->properties()->putAt(L"Database", pDwgDb);
  //ODA_ASSERT_ONCE(pDwgDb == OdDbDatabase::cast(importer->properties()->getAt(L"Database")));

  ////Import Bim file
  //OdBimImport::ImportResult res = importer->import();
  //switch (res)
  //{
  //case OdBimImport::fail:
  //  pServices->warning(OD_T("Unknown error."));
  //  break;
  //case OdBimImport::bad_password:
  //  pServices->warning(OD_T("Invalid password."));
  //  break;
  //case OdBimImport::bad_file:
  //  pServices->warning(OD_T("Cannot open file."));
  //  break;
  //case OdBimImport::bad_database:
  //  pServices->warning(OD_T("Invalid file."));
  //  break;
  //case OdBimImport::encrypted_file:
  //  pServices->warning(OD_T("Decryption error."));
  //  break;
  //case OdBimImport::success:
  //  ODA_ASSERT_ONCE(pDwgDb == OdDbDatabase::cast(importer->properties()->getAt(L"Database")));
  //  break;
  //}
}

//////////////////////////////////////////////////////////////////////////
