/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
//
// BimDbModule.cpp
//

#include "BimCommon.h"
#include "BimDbModuleImpl.h"
#include "DynamicLinker.h"
#include "StaticRxObject.h"
#include "RxObjectImpl.h"
#include "DbUndoController.h" // fixed compiling error on mac

#include "BmModuleNames.h"
#include "BmDatabase.h"
#include "ExSystemServices.h"

#include "GiContextForBmDatabase.h"
#include "BmGsManager.h" //#include "BmGsDevice.h"
//#include "BmSSet.h"
#include "ExBimCommandContext.h"

//#include "DbUnitsFormatterImpl.h"
#include "Ge/GePlane.h"

#include "RxInit.h"
#include "BimSysVarPEImpl.h"
#include "ExAppServices.h"

// for OdBmsGsPath
////#include "GsMarkerArray.h"
//#include "BmSubentId.h"
#include "Gs/GsModel.h"
#include "Gi/GiPathNode.h"

#include "PropServices.h"
#include "OdBinaryData.h"

#include "ColorMapping.h"
#include "BmUnitsFormatter.h"

//////////////////////////////////////////////////////////////////////////

//class OdBimServicesImpl : public OdExBimSystemServices, public OdExBimHostAppServices
class OdBimServicesImpl : public OdExBimHostAppServices
{
protected:
  //ODRX_USING_HEAP_OPERATORS(OdExBimSystemServices);
  ODRX_USING_HEAP_OPERATORS(OdExBimHostAppServices);

  friend class OdBimDbModuleImpl;
  OdSmartPtr<OdDbBaseHostAppServices> m_pBaseHostAppServices;

  // ----- OdExBimHostAppServices -----

  virtual OdString findFile(const OdString& filename, OdDbBaseDatabase* pDb = 0, FindFileHint hint = kDefault)
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->findFile(filename, pDb, hint);
    return OdExBimHostAppServices::findFile(filename, pDb, hint);
  }

  virtual OdDbHostAppProgressMeter* newProgressMeter()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->newProgressMeter();
    return OdExBimHostAppServices::newProgressMeter();
  }

  virtual void releaseProgressMeter(OdDbHostAppProgressMeter* pProgressMeter)
  {
    if (!m_pBaseHostAppServices.isNull())
    {
      m_pBaseHostAppServices->releaseProgressMeter(pProgressMeter);
      return;
    }
    OdExBimHostAppServices::releaseProgressMeter(pProgressMeter);
  }

  virtual const OdString program()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->program();
    return OdExBimHostAppServices::program();
  }

  virtual const OdString product()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->product();
    return OdExBimHostAppServices::product();
  }

  virtual const OdString companyName()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->companyName();
    return OdExBimHostAppServices::companyName();
  }

  virtual ProdIdCode prodcode()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->prodcode();
    return OdExBimHostAppServices::prodcode();
  }

  virtual const OdString releaseMajorMinorString()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->releaseMajorMinorString();
    return OdExBimHostAppServices::releaseMajorMinorString();
  }

  virtual int releaseMajorVersion()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->releaseMajorVersion();
    return OdExBimHostAppServices::releaseMajorVersion();
  }

  virtual int releaseMinorVersion()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->releaseMinorVersion();
    return OdExBimHostAppServices::releaseMinorVersion();
  }

  virtual const OdString versionString()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->versionString();
    return OdExBimHostAppServices::versionString();
  }

  virtual void warning(const char* warnVisGroup, const OdString& message)
  {
    if (!m_pBaseHostAppServices.isNull())
    {
      m_pBaseHostAppServices->warning(warnVisGroup, message);
      return;
    }
    OdExBimHostAppServices::warning(warnVisGroup, message);
  }

  virtual void warning(OdWarning warningOb)
  {
    if (!m_pBaseHostAppServices.isNull())
    {
      m_pBaseHostAppServices->warning(warningOb);
      return;
    }
    OdExBimHostAppServices::warning(warningOb);
  }

  virtual OdString getErrorDescription(unsigned int errorCode)
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->getErrorDescription(errorCode);
    return OdExBimHostAppServices::getErrorDescription(errorCode);
  }

  virtual OdString formatMessage(unsigned int errorCode, va_list* argList)
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->formatMessage(errorCode, argList);
    return OdExBimHostAppServices::formatMessage(errorCode, argList);
  }

  virtual OdDbUndoControllerPtr newUndoController()
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->newUndoController();
    return OdExBimHostAppServices::newUndoController();
  }

  virtual void auditPrintReport(OdAuditInfo * pAuditInfo, const OdString& strLine, int printDest) const
  {
    if (!m_pBaseHostAppServices.isNull())
    {
      m_pBaseHostAppServices->auditPrintReport(pAuditInfo, strLine, printDest);
      return;
    }
    OdExBimHostAppServices::auditPrintReport(pAuditInfo, strLine, printDest);
  }

  virtual bool ttfFileNameByDescriptor(const OdTtfDescriptor& description, OdString& filename)
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->ttfFileNameByDescriptor(description, filename);
    return OdExBimHostAppServices::ttfFileNameByDescriptor(description, filename);
  }

  virtual OdString getAlternateFontName() const
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->getAlternateFontName();
    return OdExBimHostAppServices::getAlternateFontName();
  }

  virtual OdString getFontMapFileName() const
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->getFontMapFileName();
    return OdExBimHostAppServices::getFontMapFileName();
  }

  virtual OdString getPreferableFont(const OdString& fontName, OdFontType fontType)
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->getPreferableFont(fontName, fontType);
    return OdExBimHostAppServices::getPreferableFont(fontName, fontType);
  }

  virtual OdString getSubstituteFont(const OdString& fontName, OdFontType fontType)
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->getSubstituteFont(fontName, fontType);
    return OdExBimHostAppServices::getSubstituteFont(fontName, fontType);
  }

  virtual OdString fileDialog(int flags,
                              const OdString& dialogCaption = OdString::kEmpty,
                              const OdString& defExt = OdString::kEmpty,
                              const OdString& defFilename = OdString::kEmpty,
                              const OdString& filter = OdString::kEmpty)
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->fileDialog(flags, dialogCaption, defExt, defFilename, filter);
    return OdExBimHostAppServices::fileDialog(flags, dialogCaption, defExt, defFilename, filter);
  }

  virtual OdGsDevicePtr gsBitmapDevice(OdRxObject* pViewObj = NULL,
                                       OdDbBaseDatabase* pDb = NULL,
                                       OdUInt32 flags = 0)
  {
    if (!m_pBaseHostAppServices.isNull())
      return m_pBaseHostAppServices->gsBitmapDevice(pViewObj, pDb, flags);
    return OdExBimHostAppServices::gsBitmapDevice(pViewObj, pDb, flags);
  }

  // ----- OdBimServicesImpl -----

  void restoreRegVariables()
  {
    ExAppServicesPtr pExAppServices = ExAppServices::cast(m_pBaseHostAppServices);
    if (pExAppServices.isNull())
      return;

    OdString sBaseKey = OD_T("vars/tg/");
    #define REGVAR_DEF(type, name, def_val, unused4, unused5) \
      if (!pExAppServices->readRegistryValue(sBaseKey + OD_T(#name), \
                                             toRbType(m_##name), &m_##name)) \
        m_##name = def_val;

    #define REGVAR_DEF_ARRAY(type, name, unused3, unused4, unused5) \
      { \
        m_##name.clear(); \
        for (int index = 0; true; index++) \
        { \
          OdString sName; \
          sName.format(OD_T("%ls[%d]"), OD_T(#name), index); \
          OdString sValue; \
          if (   !pExAppServices->readRegistryValue(sBaseKey + sName, \
                                                    OdResBuf::kRtString, &sValue) \
              || sValue.isEmpty()) \
            break; \
          m_##name.push_back(sValue); \
        } \
      }

    #include "BmRegVarDefs.h"

    #undef REGVAR_DEF
    #undef REGVAR_DEF_ARRAY
  }

  void saveRegVariables()
  {
    ExAppServicesPtr pExAppServices = ExAppServices::cast(m_pBaseHostAppServices);
    if (pExAppServices.isNull())
      return;

    OdString sBaseKey = OD_T("vars/tg/");
    #define REGVAR_DEF(type, name, def_val, unused4, unused5) \
      pExAppServices->writeRegistryValue(sBaseKey + OD_T(#name), \
                                         toRbType(m_##name), &m_##name);
    #define REGVAR_DEF_ARRAY(type, name, unused3, unused4, unused5) \
      { \
        int num = m_##name.size(); \
        for (int index = 0; index <= num; index++) \
        { \
          OdString sName; \
          sName.format(OD_T("%ls[%d]"), OD_T(#name), index); \
          if (index >= num) \
          { \
            pExAppServices->writeRegistryValue(sBaseKey + sName, \
                                               OdResBuf::kRtString, &OdString::kEmpty); \
            continue; \
          } \
          pExAppServices->writeRegistryValue(sBaseKey + sName, \
                                             toRbType(m_##name.getAt(index)), &m_##name.getAt(index)); \
        } \
      }

    #include "BmRegVarDefs.h"

    #undef REGVAR_DEF
    #undef REGVAR_DEF_ARRAY
  }

  OdStringArray resetRegVariables() // set to default values
  {
    OdStringArray vars;

    #define REGVAR_DEF(type, name, def_val, unused4, unused5) \
      if (m_##name != def_val) \
      { \
        m_##name = def_val; \
        vars.push_back(OD_T(#name)); \
      }
    // are empty by default
    #define REGVAR_DEF_ARRAY(type, name, unused3, unused4, unused5) \
      if (m_##name.size()) \
      { \
        m_##name.clear(); \
        vars.push_back(OD_T(#name)); \
      }

    #include "BmRegVarDefs.h"

    #undef REGVAR_DEF
    #undef REGVAR_DEF_ARRAY

    return vars;
  }
};

//////////////////////////////////////////////////////////////////////////

static OdBimServicesImpl* appBimServicesImpl()
{
  static OdStaticRxObject<OdBimServicesImpl> g_services;
  return &g_services;
}

OdExBimHostAppServices* appBimServices()
{
  return appBimServicesImpl();
}

//////////////////////////////////////////////////////////////////////////

class OdBimDbModuleImpl : public OdExtDbModuleBaseImpl
{
public:
  OdBimDbModuleImpl();
  virtual ~OdBimDbModuleImpl();

  // OdRxModule overridden
  virtual void initApp();
  virtual void uninitApp();
  virtual bool isValid();

  // init issues
  virtual void setBaseHostAppServices(OdDbBaseHostAppServices* pServices);
  virtual void initOdExtPE();
  virtual void uninitOdExtPE();

  virtual OdStringArray resetRegVariables(); // set to default values

  virtual bool getPreviewBitmap(const OdString& fileName, 
                                OdBinaryData& dataBmp); // out

  // load / save issues
  virtual OdRxObjectPtr readFile(const OdString& fileName, 
                                 Oda::FileShareMode shareMode = Oda::kShareDenyWrite);
  virtual OdRxObjectPtr createUninitializedDatabase();
  virtual bool loadToUninitializedDatabase(OdRxObjectPtr pRxDb, 
                                           const OdString& fileName, 
                                           Oda::FileShareMode shareMode = Oda::kShareDenyWrite);
  virtual bool writeFile(const OdString& fileName, OdEdCommandContext* ctx);

  // Cloud command command context issues
  virtual OdEdBaseIO* baseIO(OdEdCommandContext* ctx);
  virtual OdEdCommandContextPtr cloneCommandContext(OdEdCommandContext* ctx,
                                                    OdEdBaseIO* pIOStream = NULL, 
                                                    OdRxObject* pRxDatabase = NULL);

  // render issues
  // for Dgn active model(group) only
  //virtual bool getVisibleLayoutViewIds(OdRxObject* pRxDatabase,
  //                                     OdDbStub*& idActiveLayoutView, // out
  //                                     OdDbStubPtrArray& idsLayoutViews, OdGsDCRectArray& rects);
  // for all models (return false to use standard way (via OdDbBaseDatabasePE & OdDbBaseLayoutPE))
  //virtual bool getLayoutViewNamesAndIds(OdRxObject* pRxDatabase,
  //                                      OdStringArray& namesLayoutView,
  //                                      OdDbStubPtrArray& idsLayoutView,
  //                                      OdString* pActiveLayoutViewName = NULL,
  //                                      bool bOneForModel = true,
  //                                      OdGsDCRectArray* pRects = NULL);

  //virtual OdGsDevicePtr createDevice(OdGsModulePtr pGs, OdRxObjectPtr pRxDatabase);
  //virtual OdGsDevicePtr createBitmapDevice(OdGsModulePtr pGs, OdRxObjectPtr pRxDatabase);
  //virtual OdGsDevicePtr takeUnderlyingDeviceAndViewports(OdGsDevicePtr pGsDevice,
  //                                                       OdArray<OdGsViewPtr>& viewportsToDeleteAfterUpdate); // out

  //virtual void enableGsModel(class OdGiDefaultContext* pCtx, bool bGsCache);

  //virtual bool getModelBackground(OdRxObject* pRxDatabase, ODCOLORREF& clrBackground,
  //                                                         ODGSPALETTE* pPalette = NULL);
  //virtual bool setModelBackground(OdRxObject* pRxDatabase, ODCOLORREF clrBackground);
  virtual bool argsAtSetupLayoutViews(OdRxObject* pRxDatabase, bool& bDoZoomExtents, OdGsView::RenderMode& rm);

  // get point, zoom etc issues
  virtual OdGsView* getActiveGsView(OdGsDevice* pGsDevice);
  virtual OdGsModel* getGsModel(OdGsDevice* pGsDevice);

  virtual OdUnitsFormatter* getFormatter(OdRxObject* pRxDatabase);

  // selection issues
  virtual OdSelectionSetPtr createSelectionSet(OdRxObject* pRxDatabase);
  virtual OdSelectionSetPtr select(OdGsView* gsView,
                                   int nPoints,
                                   const OdGePoint3d* wcsPts,
                                   OdDbVisualSelection::Mode mode = OdDbVisualSelection::kCrossing,
                                   OdDbVisualSelection::SubentSelectionMode sm = OdDbVisualSelection::kDisableSubents,
                                   const OdRxObject* pFilter = NULL);
  //virtual void getVisibleAllIds(OdRxObject* pRxDatabase, OdDbStubPtrArray& ids);
  //virtual OdDbStub* getVisibleLastId(OdRxObject* pRxDatabase);
  //virtual void highlight(OdGsView* gsView, bool bValue,
  //                       OdDbStub* id, const OdDbBaseFullSubentPath* pPath);
  //virtual bool getGripPoints(OdDbStub* id,
  //                           OdGePoint3dArray& gripPoints,
  //                           OdEdCommandContext* pCmdCtx = NULL);
  //virtual bool getGripPoints(OdDbStub* id,
  //                           OdDbGripDataPtrArray& grips,
  //                           double curViewUnitSize,
  //                           int gripSize,
  //                           const OdGeVector3d& curViewDir,
  //                           int bitFlags = 0);
  //virtual bool getGripPointsAtSubentPath(const OdDbBaseFullSubentPath& path, 
  //                                       OdDbGripDataPtrArray& grips,
  //                                       double curViewUnitSize, 
  //                                       int gripSize,
  //                                       const OdGeVector3d& curViewDir, 
  //                                       OdUInt32 bitflags = 0);
  virtual bool isErased(OdDbStub* id);
  virtual OdDbHandle getElementHandle(OdDbStub* id);
  virtual OdString getElementClassName(OdDbStub* id);

  // command issues
  virtual bool cmdErase(OdEdCommandContext* pCmdCtx);

  // filters issues
  virtual bool isResponsible(OdRxObject* pRxDatabase);
  virtual OdStringArray getExtensions(enum OdEd::GetFilePathFlags flg = OdEd::kGfpForOpen, 
                                      OdRxObject* pRxDatabase = NULL,
                                      bool bWithExtSynonyms = false);

  virtual OdString getFilterByExtension(const OdString& sExt, 
                                        int index = 0,
                                        enum OdEd::GetFilePathFlags flg = OdEd::kGfpForOpen,
                                        OdStringArray* pExtSynonyms = NULL);

  /////// Commands ///////
  #define ODRX_CMD_ENTRY(cmdName, name, impl) OdStaticRxObject<OdqCmd##name> m_cmd##name;
  #include "BimDbCommands.h"

private:
  OdRxModulePtr m_pBimModule;
  //OdStaticRxObject<OdDbUnitsFormatterImpl> m_formatter;
};

ODRX_DEFINE_DYNAMIC_MODULE(OdBimDbModuleImpl);

OdBimDbModuleImpl::OdBimDbModuleImpl()
{
  #define ODRX_CMD_ENTRY(cmdName, name, impl) m_cmd##name.m_pModule = this;
  #include "BimDbCommands.h"
}

OdBimDbModuleImpl::~OdBimDbModuleImpl()
{
}

void OdBimDbModuleImpl::initApp()
{
  // Register BIM
  ::odrxDynamicLinker()->loadModule(OdBmBaseModuleName);
  //::odrxDynamicLinker()->loadModule(OdBmLabelUtils);
  m_pBimModule = ::odrxDynamicLinker()->loadModule(OdBmLoaderModuleName, false);

  // ...::rxInit();

  //ODA_ASSERT(OdDbBaseLayerPE::desc());
  //static OdStaticRxObject<OdBimBaseLayerPEImpl> dbLayer;
  //OdBimLevelTableRecord::desc()->addX(OdDbBaseLayerPE::desc(), &dbLayer);

  // register commands
  OdEdCommandStackPtr pCommands = odedRegCmds();
  #define ODRX_CMD_ENTRY(cmdName, name, impl) pCommands->addCommand(&m_cmd##name);
  #include "BimDbCommands.h"
}

void OdBimDbModuleImpl::uninitApp()
{
  // unregister commands
  OdEdCommandStackPtr pCommands = odedRegCmds();
  #define ODRX_CMD_ENTRY(cmdName, name, impl) pCommands->removeCmd(&m_cmd##name);
  #include "BimDbCommands.h"

  // ...::rxUninit();
  //OdBimLevelTableRecord::desc()->delX(OdDbBaseLayerPE::desc());

  //::odrxDynamicLinker()->unloadModule(L"TB_ExLabelUtils.tx");
  m_pBimModule = NULL;
}

bool OdBimDbModuleImpl::isValid()
{
  return !m_pBimModule.isNull();
}

// init issues
void OdBimDbModuleImpl::setBaseHostAppServices(OdDbBaseHostAppServices* pServices)
{
  appBimServicesImpl()->m_pBaseHostAppServices = pServices;
}

void OdBimDbModuleImpl::initOdExtPE()
{
  ::initOdBmSysVarPE();
  appBimServicesImpl()->restoreRegVariables();
}

void OdBimDbModuleImpl::uninitOdExtPE()
{
  appBimServicesImpl()->saveRegVariables();
  ::uninitOdBmSysVarPE();
}

OdStringArray OdBimDbModuleImpl::resetRegVariables() // set to default values
{
  return appBimServicesImpl()->resetRegVariables();
}

bool OdBimDbModuleImpl::getPreviewBitmap(const OdString& fileName, 
                                         OdBinaryData& dataBmp) // out
{
  dataBmp.clear();
  //appBimServicesImpl()->getPreviewBitmap(fileName, dataBmp);
  OdThumbnailImage dataImage;
  appBimServicesImpl()->getPreviewImage(fileName, dataImage);
  dataBmp = dataImage.png;
  bool bRes = (dataBmp.size() != 0);
  return bRes;
}

OdRxObjectPtr OdBimDbModuleImpl::readFile(const OdString& fileName, 
                                          Oda::FileShareMode shareMode) // = Oda::kShareDenyWrite
{
  OdBmDatabasePtr pBimDb = appBimServicesImpl()->readFile(fileName, shareMode);
  OdRxObjectPtr pRxDb = OdDbBaseDatabase::cast(pBimDb);
  ODA_ASSERT_ONCE(OdBmDatabase::cast(pRxDb).get()); // check for RTTI state
  return pRxDb;
}

OdRxObjectPtr OdBimDbModuleImpl::createUninitializedDatabase()
{
  OdBmDatabasePtr pBimDb = appBimServicesImpl()->createDatabase(false);
  return OdRxObject::cast(pBimDb);
}

bool OdBimDbModuleImpl::loadToUninitializedDatabase(OdRxObjectPtr pRxDb, 
                                                    const OdString& fileName, 
                                                    Oda::FileShareMode shareMode) // = Oda::kShareDenyWrite
{
  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDb.get());
  ODA_ASSERT_ONCE(pBimDb.get());
  if (pBimDb.isNull())
    return false;

  appBimServicesImpl()->readFile(pBimDb, fileName, shareMode); // use attachment of #16631 to fix compilation error here
  return true;
}

bool OdBimDbModuleImpl::writeFile(const OdString& fileName,
                                  OdEdCommandContext* ctx)
{
  ODA_ASSERT_ONCE(ctx);
  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(ctx->baseDatabase());
  if (pBimDb.isNull())
    return false;
  //OdBmBasicFileInfoPtr pInfo = pBimDb->getBasicFileInfo();
  try
  {
    appBimServicesImpl()->writeFile(fileName, pBimDb);
    return true;
  }
  catch (...)
  {
    FileVersion vers = pBimDb->getOriginalFileVersion();
    ODA_ASSERT_ONCE(vers != fileVersion2017);
    if (vers != fileVersion2017)
      ctx->userIO()->putError(L"Writing supported only for 2017 file format.");
  }
  return false;
}

// Cloud command command context issues
// possible TODO move reset, baseIO & cloneObject into OdEdCommandContext

class OdExBimCmdContext : public ExBimCommandContext {
protected:
  OdEdCommandContext* m_pOwner; // of clone

  OdExBimCmdContext()
    : m_pOwner(NULL)
  {
  }

public:
  //~OdExBimCmdContext() 
  //{
  //  if(m_pDb)
  //    m_pDb->release();
  //}

  static OdEdCommandContextPtr createObject(OdEdBaseIO* pIOStream, 
                                            OdRxObject* pRxDb = NULL,
                                            OdEdCommandContext* pOwner = NULL)
  {
    OdEdCommandContextPtr pRes = OdRxObjectImpl<OdExBimCmdContext, OdEdCommandContext>::createObject();
    OdExBimCmdContext* pCmdCtx = static_cast<OdExBimCmdContext*>(pRes.get());
    pCmdCtx->m_pDb = pRxDb;
    pCmdCtx->m_pIoStream = pIOStream;
    pCmdCtx->m_pOwner = pOwner;
    //m_pDb->addRef();
    return pRes;
  }

  //void reset(OdEdBaseIO* pIOStream, OdRxObject* pRxDb)
  //{
  //  m_pIoStream = pIOStream;
  //  m_pDb = pRxDb;
  //}
  
  OdEdBaseIO* baseIO()
  {
    return m_pIoStream.get();
  }

  virtual OdEdCommandContextPtr cloneObject(OdEdBaseIO* pIOStream = NULL,
                                            OdRxObject* pRxDb = NULL)
  {
    OdEdCommandContextPtr pRes = OdRxObjectImpl<OdExBimCmdContext, OdEdCommandContext>::createObject();
    OdExBimCmdContext* pCmdCtx = static_cast<OdExBimCmdContext*>(pRes.get());
    return createObject(pIOStream ? pIOStream : m_pIoStream.get(), 
                        pRxDb ? pRxDb : m_pDb,
                        this);
  }
};

OdEdBaseIO* OdBimDbModuleImpl::baseIO(OdEdCommandContext* pCtx)
{
  if (!pCtx || OdBmDatabase::cast(pCtx->baseDatabase()).isNull())
    return NULL;
  OdExBimCmdContext* pCmdCtx = static_cast<OdExBimCmdContext*>(pCtx);
  return pCmdCtx->baseIO();
}

OdEdCommandContextPtr OdBimDbModuleImpl::cloneCommandContext(OdEdCommandContext* pCtx,
                                                             OdEdBaseIO* pIOStream, // = NULL
                                                             OdRxObject* pRxDb) // = NULL
{
  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDb);
  ODA_ASSERT_ONCE(pBimDb.get());
  if (pBimDb.isNull())
    return OdEdCommandContextPtr();
  
  ODA_ASSERT_ONCE(pCtx); // to check with deb next
  if (!pCtx || OdBmDatabase::cast(pCtx->baseDatabase()).isNull())
    return OdExBimCmdContext::createObject(pIOStream, pRxDb, pCtx);

  OdExBimCmdContext* pCmdCtx = static_cast<OdExBimCmdContext*>(pCtx);
  return pCmdCtx->cloneObject(pIOStream, pRxDb);
}

//

// for Dgn active model(group) only
//bool OdBimDbModuleImpl::getVisibleLayoutViewIds(OdRxObject*, // pRxDatabase
//                                                OdDbStub*&, // idActiveLayoutView
//                                                OdDbStubPtrArray&, // idsLayoutViews
//                                                OdGsDCRectArray&) // rects
//{
//  return false;
//}

// for all models (return false to use standard way (via OdDbBaseDatabasePE & OdDbBaseLayoutPE))
//bool OdBimDbModuleImpl::getLayoutViewNamesAndIds(OdRxObject*, // pRxDatabase
//                                                 OdStringArray&, // namesLayoutView
//                                                 OdDbStubPtrArray&, // idsLayoutView
//                                                 OdString*, // pActiveLayoutViewName // = NULL
//                                                 bool, //bOneForModel, // = true (only in focus)
//                                                 OdGsDCRectArray*) // pRects // = NULL
//{
//  return false; // to work with standard way (via OdDbBaseDatabasePE & OdDbBaseLayoutPE)
//}

//OdGsDevicePtr OdBimDbModuleImpl::createDevice(OdGsModulePtr pGs,
//                                              OdRxObjectPtr pRxDatabase)
//{
//  ODA_ASSERT_ONCE(pGs.get() && pRxDatabase.get());
//  OdGsDevicePtr pGsDevice;
//
//  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
//  if (pGs.isNull() || pBimDb.isNull())
//    return pGsDevice;
//
//  pGsDevice = pGs->createDevice();
//  return pGsDevice;
//}

//OdGsDevicePtr OdBimDbModuleImpl::takeUnderlyingDeviceAndViewports(OdGsDevicePtr pGsDevice,
//                                                                  OdArray<OdGsViewPtr>& viewportsToDeleteAfterUpdate) // out
//{
//  ODA_FAIL_ONCE(); // TODO
//  return OdGsDevicePtr();
//  //OdGsDeviceForBmModelPtr pLayoutHelperPrev = OdGsDeviceForBmModel::cast(pGsDevice);
//  //ODA_ASSERT_ONCE(pLayoutHelperPrev.get() && !viewportsToDeleteAfterUpdate.size());
//  //if (pLayoutHelperPrev.isNull())
//  //  return OdGsDevicePtr();
//  //OdGsDevicePtr pUnderlying = pLayoutHelperPrev->underlyingDevice();
//  //ODA_ASSERT_ONCE(pUnderlying.get())
//
//  //int numViewportsToDelete = pLayoutHelperPrev->numViews();
//  //if (numViewportsToDelete && pUnderlying.get())
//  //{
//  //  for (int idx = 0; idx < numViewportsToDelete; idx++)
//  //  {
//  //    OdGsViewPtr pView = pLayoutHelperPrev->viewAt(idx);
//  //    ODA_ASSERT_ONCE(pView.get());
//  //    viewportsToDeleteAfterUpdate.push_back(pView); // store to delete after update
//  //  }
//  //}
//  //pLayoutHelperPrev->eraseAllViews();
//
//  //return pUnderlying;
//}

//OdGsDevicePtr OdBimDbModuleImpl::createBitmapDevice(OdGsModulePtr pGs, 
//                                                    OdRxObjectPtr pRxDatabase)
//{
//  OdGsDevicePtr pGsDevice;
//  if (pGs.isNull() || pRxDatabase.isNull())
//  {
//    ODA_FAIL_ONCE();
//    return pGsDevice;
//  }
//
//  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
//  if (pBimDb.isNull()) // valid way to prevent linking with Bim
//    return pGsDevice;
//
//  pGsDevice = pGs->createBitmapDevice();
//  return pGsDevice;
//}

//void OdBimDbModuleImpl::enableGsModel(OdGiDefaultContext* pCtx,
//                                      bool) // bGsCache
//{
//  ODA_ASSERT_ONCE(pCtx);
//  if (!pCtx)
//    return;
//  // TODO (unsupported now
//  //OdGiContextForBmDatabasePtr pBmCtx = OdGiContextForBmDatabase::cast(pCtx); 
//  //if (pBmCtx.isNull())
//  //  return;
//  //pBmCtx->enableGsModel(bGsCache);
//}

//bool OdBimDbModuleImpl::getModelBackground(OdRxObject* pRxDatabase,
//                                           ODCOLORREF& clrBackground,
//                                           ODGSPALETTE* pPalette) // = NULL
//{
//  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
//  if (pBimDb.isNull())
//    return false;
//  
//  //OdBmModelPtr pModel = pBimDb->getActiveModelId().safeOpenObject();
//  //clrBackground = pModel->getBackground();
//
//  if (pPalette)
//  {
//    ODGSPALETTE& palette = *pPalette;
//    ODA_ASSERT_ONCE(!palette.size());
//    //const ODCOLORREF* refColors = OdBmColorTable::currentPalette(pBimDb);
//    const ODCOLORREF* refColors = isDarkPalette(clrBackground) ? ::odcmAcadDarkPalette()
//                                                               : ::odcmAcadLightPalette();
//
//    palette.insert(palette.begin(), refColors, refColors + 256);
//    // Color with #255 always defines background. 
//    // The background of the active model must be considered in the device palette.
//    palette[255] = clrBackground;
//    //  // Note: This method should be called to resolve "white background issue" before setting device palette
//    //  OdBmColorTable::correctPaletteForWhiteBackground(palette.asArrayPtr());
//  }
//
//  return true;
//}

//bool OdBimDbModuleImpl::setModelBackground(OdRxObject* pRxDatabase, 
//                                           ODCOLORREF clrBackground)
//{
//  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
//  if (pBimDb.isNull())
//    return false;
//
//  ODA_FAIL_ONCE(); // TODO
//  return false;
//  //OdBmModelPtr pModel = pBimDb->getActiveModelId().safeOpenObject(OdBm::kForWrite);
//  //if (clrBackground == pModel->getBackground())
//  //  return true;
//  //pModel->setUseBackgroundColorFlag(true);
//  //pModel->setBackground(clrBackground);
//  //return true;
//}

bool OdBimDbModuleImpl::argsAtSetupLayoutViews(OdRxObject* pRxDatabase, bool& bDoZoomExtents, OdGsView::RenderMode& rm)
{
  OdExtDbModuleBaseImpl::argsAtSetupLayoutViews(pRxDatabase, bDoZoomExtents, rm); // set for false case
  bDoZoomExtents = true;
  // I do not found file with non-wireframe initially getDisplayStyle in BIM
  // (debug  OdBmViewDisplayModelInternalImpl::setDisplayStyle & OdBmDBDrawingHelperImpl::setDisplayStyle to check it) 
  // It is very inconvenient in TV 
  rm = OdGsView::kGouraudShaded;
  return true;
}

OdGsView* OdBimDbModuleImpl::getActiveGsView(OdGsDevice* pGsDevice)
{
  OdGsBmDBDrawingHelperPtr pLayoutHelper = OdGsBmDBDrawingHelper::cast(pGsDevice);
  ODA_ASSERT_ONCE(pLayoutHelper.get());
  if (pLayoutHelper.isNull())
    return NULL;
  OdGsView* pGsView = pLayoutHelper->activeView().get();
  ODA_ASSERT_ONCE(pGsView);

  return pGsView;
}

OdGsModel* OdBimDbModuleImpl::getGsModel(OdGsDevice* pGsDevice)
{
  OdGsBmDBDrawingHelperPtr pLayoutHelper = OdGsBmDBDrawingHelper::cast(pGsDevice);
  ODA_ASSERT_ONCE(pLayoutHelper.get());
  OdGsModel* pGsModel = pLayoutHelper->gsModel();
  // it is NULL if GS cache off //ODA_ASSERT_ONCE(pGsModel);
  return pGsModel;
}

OdUnitsFormatter* OdBimDbModuleImpl::getFormatter(OdRxObject* pRxDatabase)
{
  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
  if (pBimDb.isNull())
    return NULL;

  return &pBimDb->formatter(); // return &m_formatter; // TODO special for each model in view (via active model)
}

// selection issues
OdSelectionSetPtr OdBimDbModuleImpl::createSelectionSet(OdRxObject* pRxDatabase)
{
  return OdSelectionSetPtr(); // TODO

  //OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
  //if (pBimDb.isNull())
  //  return OdSelectionSetPtr();
  //return OdBmSelectionSet::createObject(pBimDb);
}

OdSelectionSetPtr OdBimDbModuleImpl::select(OdGsView* gsView,
                                            int nPoints,
                                            const OdGePoint3d* wcsPts,
                                            OdDbVisualSelection::Mode mode, // = OdDbVisualSelection::kCrossing
                                            OdDbVisualSelection::SubentSelectionMode sm, // = OdDbVisualSelection::kDisableSubents
                                            const OdRxObject* pFilter) // = NULL
{
  ODA_FAIL_ONCE(); // TODO
  return OdSelectionSetPtr();

  //if (!gsView || !gsView->userGiContext())
  //{
  //  ODA_FAIL_ONCE(); // test
  //  return OdSelectionSetPtr();
  //}
  //OdBmDatabasePtr pBimDb = OdBmDatabase::cast(gsView->userGiContext()->database());
  //if (pBimDb.isNull())
  //  return OdSelectionSetPtr();
  //return OdBmSelectionSet::select(pBimDb, gsView, nPoints, wcsPts, mode, sm, pFilter);
}

//static bool isObjectLocked(OdBmObjectPtr pElem)
//{
//  if (pElem.isNull())
//    return false;
//
//  if (pElem->getLockedFlag())
//    return true;
//
//  OdBmGraphicsObjectPtr pItem = OdBmGraphicsObject::cast(pElem);
//  if (pItem.isNull())
//    return false;
//
//  if (!pItem->getLevelId().isNull())
//  {
//    OdBmLevelTableRecordPtr pLevel = pItem->getLevelId().safeOpenObject();
//    if (pLevel->getIsFrozenFlag() || pLevel->getIsReadOnlyFlag())
//      return true;
//  }
//
//  OdBmObjectIteratorPtr pItr;
//  {
//    OdBmCellHeader2dPtr pHeader2d = OdBmCellHeader2d::cast(pItem);
//    OdBmCellHeader3dPtr pHeader3d = OdBmCellHeader3d::cast(pItem);
//    if (!pHeader2d.isNull())
//      pItr = pHeader2d->createIterator();
//    if (pItr.isNull() && !(pHeader3d = OdBmCellHeader3d::cast(pItem)).isNull())
//      pItr = pHeader3d->createIterator();
//  }
//  if (pItr.isNull())
//    return false;
//  
//  for (; !pItr->done(); pItr->step())
//  {
//    pElem = OdBmObject::cast(pItr->item().safeOpenObject());
//    if (pElem.isNull() || !isObjectLocked(pElem))
//      continue;
//    return true;
//  }
//
//  return false;
//}

//void OdBimDbModuleImpl::getVisibleAllIds(OdRxObject* pRxDatabase, OdDbStubPtrArray& ids)
//{
//  ODA_FAIL_ONCE(); // TODO
//
//  //OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
//  //if (pBimDb.isNull())
//  //  return;
//  //OdBmModelPtr pModel = OdBmModel::cast(pBimDb->getActiveModelId().openObject());
//  //ODA_ASSERT_ONCE(!pModel.isNull());
//  //if (pModel.isNull())
//  //  return;
//  //int idx = 0;
//  //for (OdBmObjectIteratorPtr pItr = pModel->createGraphicsObjectsIterator();
//  //     idx < 2; pItr = pModel->createControlObjectsIterator(), idx++)
//  //{
//  //  for (; !pItr->done(); pItr->step())
//  //  {
//  //    OdBmObjectId idElem = pItr->item();
//  //    ODA_ASSERT_ONCE(!idElem.isNull());
//  //    if (idElem.isNull())
//  //      continue;
//  //    OdBmObjectPtr pElem = OdBmObject::cast(idElem.openObject());
//  //    ODA_ASSERT_ONCE(!pElem.isNull());
//  //    if (pElem.isNull() || isObjectLocked(pElem))
//  //      continue;
//  //    if (!pElem->gsNode()) // Cache
//  //      continue;
//  //    ids.push_back(idElem);
//  //  }
//  //}
//}

//OdDbStub* OdBimDbModuleImpl::getVisibleLastId(OdRxObject* pRxDatabase)
//{
//  ODA_FAIL_ONCE(); // TODO
//  return NULL;
//  //OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
//  //if (pBimDb.isNull())
//  //  return NULL;
//  //OdBmModelPtr pModel = OdBmModel::cast(pBimDb->getActiveModelId().openObject());
//  //ODA_ASSERT_ONCE(!pModel.isNull());
//  //if (pModel.isNull())
//  //  return NULL;
//  //int idx = 0;
//  //for (OdBmObjectIteratorPtr pItr = pModel->createGraphicsObjectsIterator(false);
//  //     idx < 2; pItr = pModel->createControlObjectsIterator(false), idx++)
//  //{
//  //  for (; !pItr->done(); pItr->step())
//  //  {
//  //    OdBmObjectId idElem = pItr->item();
//  //    ODA_ASSERT_ONCE(!idElem.isNull());
//  //    if (idElem.isNull())
//  //      continue;
//  //    OdBmObjectPtr pElem = OdBmObject::cast(idElem.openObject());
//  //    ODA_ASSERT_ONCE(!pElem.isNull());
//  //    if (pElem.isNull() || isObjectLocked(pElem))
//  //      continue;
//  //    if (!pElem->gsNode()) // Cache
//  //      continue;
//  //    return idElem;
//  //  }
//  //}
//  //return NULL;
//}

//class OdBmGsPath
//{
//  struct Node : OdGiPathNode
//  {
//    const Node*     m_pParent;
//    OdDbStub*       m_pId;
//    OdGiDrawablePtr m_pDrawable;
//    OdGsMarker      m_gsMarker;
//
//    const OdGiPathNode* parent() const 
//    { 
//      return m_pParent; 
//    }
//    OdDbStub* persistentDrawableId() const 
//    { 
//      return m_pId; 
//    }
//    const OdGiDrawable* transientDrawable() const 
//    { 
//      return m_pDrawable; 
//    }
//    OdGsMarker selectionMarker() const 
//    { 
//      return m_gsMarker; 
//    }
//  };
//  const Node* m_pLeaf;
//
//  void add(const OdGiDrawable* pDrawable, const OdBmObjectId& drawableId, OdGsMarker gsMarker = -1)
//  {
//    Node* pNode = new Node();
//    pNode->m_pParent    = m_pLeaf;
//    m_pLeaf = pNode;
//
//    pNode->m_pDrawable  = pDrawable;
//    pNode->m_pId        = drawableId;
//    pNode->m_gsMarker   = gsMarker;
//  }
//
//public:
//  OdBmGsPath()
//    : m_pLeaf(0)
//  {
//  }
//  ~OdBmGsPath()
//  {
//    clear();
//  }
//  OdBmGsPath(const OdBmFullSubentPath& path)
//    : m_pLeaf(0)
//  {
//    set(path);
//  }
//
//  void clear()
//  {
//    while(m_pLeaf)
//    {
//      const Node* pNode = m_pLeaf;
//      m_pLeaf = pNode->m_pParent;
//      delete pNode;
//    }
//    m_pLeaf = NULL;
//  }
//
//  void set(const OdBmFullSubentPath& path)
//  {
//    set(path, kNullSubentIndex);
//  }
//  void set(const OdBmFullSubentPath& path, OdGsMarker gsMarker)
//  {
//    clear();
//    const OdBmObjectIdArray& ids = path.objectIds();
//
//    OdBmObjectIdArray::const_iterator iter = ids.begin();
//    if (iter==ids.end())
//      throw OdError(eInvalidInput);
//
//    OdBmObjectPtr pObj = iter->safeOpenObject();
//    if (!pObj->ownerId().isNull())
//      addNode(pObj->ownerId());
//    for (; iter != ids.end()-1; ++iter)
//      addNode(*iter);
//
//    addNode(*iter, gsMarker);
//  }
//
//  void addNode(const OdBmObjectId& drawableId, OdGsMarker gsMarker = kNullSubentIndex)
//  {
//    add(0, drawableId, gsMarker);
//  }
//  void addNode(const OdGiDrawable* pDrawable, OdGsMarker gsMarker = kNullSubentIndex)
//  {
//    add(pDrawable->isPersistent() ? 0 : pDrawable, pDrawable->id(), gsMarker);
//  }
//
//  operator const OdGiPathNode&() const
//  {
//    return *m_pLeaf;
//  }
//};

//void OdBimDbModuleImpl::highlight(OdGsView* gsView, bool bValue,
//                                  OdDbStub* id, const OdDbBaseFullSubentPath* pPath)
//{
//  ODA_FAIL_ONCE(); // TODO
//
//  //OdRxObject* pRxDatabase = baseDatabaseBy(id);
//  //ODA_ASSERT_ONCE(pRxDatabase && id);
//  //if (!pRxDatabase || !id)
//  //  return;
//
//  //OdBmObjectId idEnt(id);
//  //OdBmObjectPtr pEnt = idEnt.openObject();
//  //if (pEnt.isNull())
//  //  return; // erased
//  //
//  //// TODO via pEnt->highlight(bValue, &pPath);
//  //// see OdDbEntity::subHighlight
//  ////
//  //OdGsCache* pCache = pEnt->gsNode();
//  //if (!pCache)
//  //{
//  //  pEnt->downgradeOpen();
//  //  pCache = pEnt->gsNode();
//  //  ODA_ASSERT_ONCE(pCache);
//  //  if (!pCache)
//  //    return;
//  //}
//
//  //if (!pPath)
//  //{
//  //  //if (pEnt->ownerId().isNull())
//  //  //  return;
//  //  OdBmGsPath gsPath; //OdDbGsPath gsPath;
//  //  if (!pEnt->ownerId().isNull())
//  //    gsPath.addNode(pEnt->ownerId());
//  // #ifdef _DEBUG
//  //  else
//  //    gsPath = gsPath; // brk !!!!
//  // #endif
//  //  gsPath.addNode(pEnt);
//  //  pCache->model()->highlight(gsPath, bValue);
//  //  return;
//  //}
//
//  //ODA_FAIL_ONCE(); // TODO
//  ////OdGsMarkerArray gsMarkers;
//  ////pEnt->getGsMarkersAtSubentPath(*pPath, gsMarkers);
//  ////for (OdGsMarkerArray::iterator sm = gsMarkers.begin(); sm != gsMarkers.end(); ++sm)
//  ////{
//  ////  OdBmGsPath gsPath; //OdDbGsPath gsPath;
//  ////  gsPath.set(*pPath, *sm);
//  ////  pCache->model()->highlight(gsPath, bValue);
//  ////}
//}

//bool OdBimDbModuleImpl::getGripPoints(OdDbStub* id,
//                                      OdGePoint3dArray& gripPoints,
//                                      OdEdCommandContext* pCmdCtx) // = NULL
//{
//  ODA_FAIL_ONCE(); // TODO
//  return false;
//
//  //ODA_ASSERT_ONCE(id);
//  //if (!id)
//  //  return false;
//
//  //OdBmDatabasePtr pBimDb = OdBmDatabase::cast(baseDatabaseBy(id));
//  //if (pBimDb.isNull())
//  //  return false;
//
//  //OdBmObjectPtr pElem = OdBmObject::cast(OdBmObjectId(id).openObject());
//  //ODA_ASSERT_ONCE(!pElem.isNull());
//  //if (pElem.isNull())
//  //  return false;
//
//  //OdResult res = eNotImplementedYet;
//
//  //// TODO
//  ////res = pElem->getGripPoints(gripPoints);
//  ////
//  //// TEST for Bim grips
//  //bool bTestBimGrips = false;
//  //OdSysVarPEPtr pSysVarPE = OdSysVarPE::cast(pCmdCtx);
//  //if (!pSysVarPE.isNull())
//  //{
//  //  bool bExist = false;
//  //  OdString sValue = pSysVarPE->valueToString(pCmdCtx, OD_T("BimGRIPS"), NULL, &bExist);
//  //  ODA_ASSERT_ONCE(   sValue == OD_T("0") || sValue == OD_T("1") // DWG
//  //                  || sValue == OD_T("false") || sValue == OD_T("true")); // Bim
//  //  bTestBimGrips = !(sValue.isEmpty() || sValue == OD_T("0") || sValue == OD_T("false"));
//  //}
//  //if (bTestBimGrips)
//  //{
//  //  OdGeExtents3d extents;
//  //  res = pElem->getGeomExtents(extents);
//  //  if (res == eOk)
//  //  {
//  //    if (extents.isValidExtents())
//  //    {
//  //      OdGePoint3d ptCenter = extents.center();
//  //      gripPoints.push_back(ptCenter);
//  //    }
//  //    else
//  //      res = eNotImplementedYet;
//  //  }
//  //}
//
//  //ODA_ASSERT_ONCE(res == eOk || res == eNotImplementedYet);
//  //return res == eOk;
//}

//bool OdBimDbModuleImpl::getGripPoints(OdDbStub* id,
//                                      OdDbGripDataPtrArray&, // grips
//                                      double, // curViewUnitSize
//                                      int, // gripSize
//                                      const OdGeVector3d&, // curViewDir
//                                      int) // bitFlags // = 0
//{
//  ODA_FAIL_ONCE(); // TODO
//  return false;
//
//  //ODA_ASSERT_ONCE(id);
//  //if (!id)
//  //  return false;
//
//  //OdBmDatabasePtr pBimDb = OdBmDatabase::cast(baseDatabaseBy(id));
//  //if (pBimDb.isNull())
//  //  return false;
//
//  //OdBmObjectPtr pElem = OdBmObject::cast(OdBmObjectId(id).openObject());
//  //ODA_ASSERT_ONCE(!pElem.isNull());
//  //if (pElem.isNull())
//  //  return false;
//
//  //OdResult res = eNotImplementedYet;
//  //// TODO
//  ////res = pElem->getGripPoints(aPts, 
//  ////                           grips,
//  ////                           curViewUnitSize,
//  ////                           gripSize,
//  ////                           curViewDir,
//  ////                           bitFlags);
//
//  //ODA_ASSERT_ONCE(res == eOk || res == eNotImplementedYet);
//  //return res == eOk;
//}

//bool OdBimDbModuleImpl::getGripPointsAtSubentPath(const OdDbBaseFullSubentPath& path, 
//                                                  OdDbGripDataPtrArray&, // grips
//                                                  double, // curViewUnitSize
//                                                  int, // gripSize
//                                                  const OdGeVector3d&, // curViewDir
//                                                  OdUInt32) // bitflags // = 0
//{
//  ODA_FAIL_ONCE(); // TODO
//  return false;
//
//  //if (!path.objectIds().size())
//  //  return false;
//
//  //OdBmDatabasePtr pBimDb = OdBmDatabase::cast(baseDatabaseBy(path.objectIds().first()));
//  //if (pBimDb.isNull())
//  //  return false;
//
//  //OdBmObjectPtr pElem = OdBmObject::cast(OdBmObjectId(path.objectIds().first()).openObject());
//  //ODA_ASSERT_ONCE(!pElem.isNull());
//  //if (pElem.isNull())
//  //  return false;
//
//  //OdResult res = eNotImplementedYet;
//  //// TODO
//  ////res = pElem->getGripPointsAtSubentPath(path
//  ////                                       grips,
//  ////                                       curViewUnitSize, 
//  ////                                       gripSize,
//  ////                                       curViewDir, 
//  ////                                       bitflags);
//  //ODA_ASSERT_ONCE(res == eOk || res == eNotImplementedYet);
//  //return res == eOk;
//}

bool OdBimDbModuleImpl::isErased(OdDbStub* id)
{
  /*OdBmDatabasePtr pBimDb = OdBmDatabase::cast(baseDatabaseBy(id));
  ODA_ASSERT_ONCE(id && !pBimDb.isNull());
  if (pBimDb.isNull())
    return false;
  OdBmObjectId idElem(id);

  return idElem.isErased();*/
  return false;
}

OdDbHandle OdBimDbModuleImpl::getElementHandle(OdDbStub* id)
{
  return OdBmObjectId(id).getHandle();
}

OdString OdBimDbModuleImpl::getElementClassName(OdDbStub* id)
{
  if (!id)
    return OdBmDatabase::desc()->name();
  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(baseDatabaseBy(id));
  ODA_ASSERT_ONCE(id && !pBimDb.isNull());
  if (pBimDb.isNull())
    return OdString::kEmpty;
  OdBmObjectId idElem(id);
  OdBmObjectPtr pElem = idElem.openObject();
  ODA_ASSERT_ONCE(!pElem.isNull());
  if (pElem.isNull())
    return OdString::kEmpty;

  OdString sName = pElem->isA()->name();
  return sName;
}

bool OdBimDbModuleImpl::cmdErase(OdEdCommandContext* pCmdCtx)
{
  ODA_FAIL_ONCE(); // TODO
  return false;

  //OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pCmdCtx->baseDatabase());
  //if (pBimDb.isNull())
  //  return false;

  //OdDbUserIOPtr pIO = pCmdCtx->userIO();
  //OdSelectionSetPtr pSSet = pIO->select();

  //int nLockedLayer = 0,
  //    nCount = 0;
  //for (OdBmSelectionSetIteratorPtr pItr = pSSet->newIterator();
  //     !pItr->done(); pItr->next())
  //{
  //  OdBmObjectId idElem = pItr->objectId();
  //  OdBmObjectPtr pElem = OdBmObject::cast(idElem.safeOpenObject(OdBm::kForWrite));
  //  ODA_ASSERT_ONCE(!pElem.isNull());
  //  if (pElem.isNull())
  //    continue;

  //  nCount++;
  //  if (isObjectLocked(pElem))
  //  {
  //    nLockedLayer++;
  //    continue;
  //  }
  //  // already contained via OdBmSelectionSet
  //  //OdGsCache* pCsh = pElem->gsNode();
  //  //if (pCsh)
  //  //  pCsh->model()->onErased(pElem, pElem->ownerId());
  //  
  //  pElem->erase();
  //}
  //if (!nCount)
  //  return false;

  //ODA_ASSERT_ONCE(OdPropServices::cast(pCmdCtx).get());
  //OdString sMsg;
  //sMsg.format(OdPropServices::cast(pCmdCtx)->tr("  %d found.").c_str(),
  //  nCount);
  //pIO->putString(sMsg);

  //if (nLockedLayer)
  //{
  //  sMsg.format(OdPropServices::cast(pCmdCtx)->tr( // msgNWasWereOnALockedLayer
  //              (nLockedLayer > 1) ? "  %d were on a locked layer."
  //                                 : "  %d was on a locked layer.").c_str(), 
  //              nLockedLayer);
  //  pIO->putString(sMsg);
  //}

  //return true;
}

bool OdBimDbModuleImpl::isResponsible(OdRxObject* pRxDatabase)
{
  ODA_ASSERT_ONCE(pRxDatabase); // test
  OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
  return !pBimDb.isNull();
}

inline bool isFileCouldBeResaved(OdBmDatabase* pBimDb)
{
  ODA_ASSERT_ONCE(pBimDb);
  return (pBimDb->getOriginalFileVersion() == fileVersion2017);
}

OdStringArray OdBimDbModuleImpl::getExtensions(enum OdEd::GetFilePathFlags flg, // = OdEd::kGfpForOpen
                                               OdRxObject* pRxDatabase, // = NULL
                                               bool) // bWithExtSynonyms = false
{
  OdStringArray lst;
  if (pRxDatabase && !isResponsible(pRxDatabase))
    return lst;
  if (flg == OdEd::kGfpForOpen)
    lst.push_back(L"rvt"), lst.push_back(L"rfa");
  else if (flg == OdEd::kGfpForSave)
  {
    OdBmDatabasePtr pBimDb = OdBmDatabase::cast(pRxDatabase);
    if (!isFileCouldBeResaved(pBimDb))
      return lst;
    lst.push_back(pBimDb->isModel() ? L"rvt" : L"rfa");
  }

  return lst;
}

OdString OdBimDbModuleImpl::getFilterByExtension(const OdString& sExt, 
                                                 int index, // = 0
                                                 enum OdEd::GetFilePathFlags, // flg = OdEd::kGfpForOpen
                                                 OdStringArray*) // pExtSynonyms = NULL
{
  if (index)
    return OdString::kEmpty;
  if (sExt == L"rvt")
    return L"RVT files (*.rvt)|*.rvt|";
  if (sExt == L"rfa")
    return L"RFA files (*.rfa)|*.rfa|";
  return OdString::kEmpty;
}
