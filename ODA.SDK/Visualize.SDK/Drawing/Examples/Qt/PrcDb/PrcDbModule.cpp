/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
//
// PrcDbModule.cpp
//

#include "OdaCommon.h"
#include "DbUndoController.h"
#include "RxObject.h"
#include "PrcCommon.h"
#include "ExtDbModuleBaseImpl.h"
#include "ExDynamicModule.h" // #include "RxDynamicModule.h" 

//#include "Ed/EdCommandStack.h"
//#include "ExDbCommandContext.h"


//#include "DynamicLinker.h"
//#include "StaticRxObject.h"
//#include "DbUndoController.h" // fixed compiling error on mac
//#include "DbUnitsFormatterImpl.h"
//#include "Ge/GePlane.h"
//#include "RxInit.h"
//#include "Gs/GsModel.h"
//#include "Gi/GiPathNode.h"
//#include "DbUserIO.h"
//#include "PropServices.h"
//#include "OdBinaryData.h"
#include "ColorMapping.h"

//#include "PrcFile.h"
#include "ExPrcCommandContext.h"
#include "ExPrcHostAppServices.h"

#include "GiGs/PrcGsManager.h" // OdGsPrcLayoutHelper  // ? #include "PrcGsLayoutHelper.h"

//////////////////////////////////////////////////////////////////////////

class OdPrcDbServicesImpl : public OdWrapServicesImpl<OdExPrcHostAppServices>
{
protected:
  //ODRX_USING_HEAP_OPERATORS(OdPrcDbServicesImpl);

  friend class OdPrcDbModuleImpl;

  // ----- OdPrcDbServicesImpl -----

  //void restoreRegVariables()
  //{
  //  ExAppServicesPtr pExAppServices = ExAppServices::cast(m_pBaseHostAppServices);
  //  if (pExAppServices.isNull())
  //    return;

  //  OdString sBaseKey = OD_T("vars/tg/");
  //  #define REGVAR_DEF(type, name, def_val, unused4, unused5) \
  //    if (!pExAppServices->readRegistryValue(sBaseKey + OD_T(#name), \
  //                                           toRbType(m_##name), &m_##name)) \
  //      m_##name = def_val;

  //  #define REGVAR_DEF_ARRAY(type, name, unused3, unused4, unused5) \
  //    { \
  //      m_##name.clear(); \
  //      for (int index = 0; true; index++) \
  //      { \
  //        OdString sName; \
  //        sName.format(OD_T("%ls[%d]"), OD_T(#name), index); \
  //        OdString sValue; \
  //        if (   !pExAppServices->readRegistryValue(sBaseKey + sName, \
  //                                                  OdResBuf::kRtString, &sValue) \
  //            || sValue.isEmpty()) \
  //          break; \
  //        m_##name.push_back(sValue); \
  //      } \
  //    }

  //  #include "DgRegVarDefs.h"

  //  #undef REGVAR_DEF
  //  #undef REGVAR_DEF_ARRAY
  //}

  //void saveRegVariables()
  //{
  //  ExAppServicesPtr pExAppServices = ExAppServices::cast(m_pBaseHostAppServices);
  //  if (pExAppServices.isNull())
  //    return;

  //  OdString sBaseKey = OD_T("vars/tg/");
  //  #define REGVAR_DEF(type, name, def_val, unused4, unused5) \
  //    pExAppServices->writeRegistryValue(sBaseKey + OD_T(#name), \
  //                                       toRbType(m_##name), &m_##name);
  //  #define REGVAR_DEF_ARRAY(type, name, unused3, unused4, unused5) \
  //    { \
  //      int num = m_##name.size(); \
  //      for (int index = 0; index <= num; index++) \
  //      { \
  //        OdString sName; \
  //        sName.format(OD_T("%ls[%d]"), OD_T(#name), index); \
  //        if (index >= num) \
  //        { \
  //          pExAppServices->writeRegistryValue(sBaseKey + sName, \
  //                                             OdResBuf::kRtString, &OdString::kEmpty); \
  //          continue; \
  //        } \
  //        pExAppServices->writeRegistryValue(sBaseKey + sName, \
  //                                           toRbType(m_##name.getAt(index)), &m_##name.getAt(index)); \
  //      } \
  //    }

  //  #include "DgRegVarDefs.h"

  //  #undef REGVAR_DEF
  //  #undef REGVAR_DEF_ARRAY
  //}

  //OdStringArray resetRegVariables() // set to default values
  //{
  //  OdStringArray vars;

  //  #define REGVAR_DEF(type, name, def_val, unused4, unused5) \
  //    if (m_##name != def_val) \
  //    { \
  //      m_##name = def_val; \
  //      vars.push_back(OD_T(#name)); \
  //    }
  //  // are empty by default
  //  #define REGVAR_DEF_ARRAY(type, name, unused3, unused4, unused5) \
  //    if (m_##name.size()) \
  //    { \
  //      m_##name.clear(); \
  //      vars.push_back(OD_T(#name)); \
  //    }

  //  #include "DgRegVarDefs.h"

  //  #undef REGVAR_DEF
  //  #undef REGVAR_DEF_ARRAY

  //  return vars;
  //}
};

//////////////////////////////////////////////////////////////////////////

static OdPrcDbServicesImpl* appPrcServicesImpl()
{
  static OdStaticRxObject<OdPrcDbServicesImpl> g_services;
  return &g_services;
}

OdExPrcHostAppServices* appPrcServices()
{
  return appPrcServicesImpl();
}

//////////////////////////////////////////////////////////////////////////

class OdPrcDbModuleImpl : public OdExtDbModuleBaseImpl
{
public:
  OdPrcDbModuleImpl();
  virtual ~OdPrcDbModuleImpl();

  //// OdRxModule overridden
  virtual void initApp();
  virtual void uninitApp();
  virtual bool isValid();

  //// init issues
  virtual void setBaseHostAppServices(OdDbBaseHostAppServices* pServices);
  
  //virtual void initOdExtPE();
  //virtual void uninitOdExtPE();
  //virtual OdStringArray resetRegVariables(); // set to default values
  //virtual bool getPreviewBitmap(const OdString& fileName, 
  //                              OdBinaryData& dataBmp); // out

  //// load / save issues
  //virtual OdRxObjectPtr readFile(const OdString& fileName, 
  //                               Oda::FileShareMode shareMode = Oda::kShareDenyWrite);
  virtual OdRxObjectPtr createUninitializedDatabase();
  virtual bool loadToUninitializedDatabase(OdRxObjectPtr pRxDb, 
                                           const OdString& fileName, 
                                           Oda::FileShareMode shareMode = Oda::kShareDenyWrite);
  virtual bool writeFile(const OdString& fileName, OdEdCommandContext* ctx);

  //// Cloud command command context issues
  //virtual OdEdBaseIO* baseIO(OdEdCommandContext* ctx);
  //virtual OdEdCommandContextPtr cloneCommandContext(OdEdCommandContext* ctx,
  //                                                  OdEdBaseIO* pIOStream = NULL, 
  //                                                  OdRxObject* pRxDatabase = NULL);

  // render issues
  // for all models (return false to use standard way (via OdDbBaseDatabasePE & OdDbBaseLayoutPE))
  virtual bool getLayoutViewNamesAndIds(OdRxObject* pRxDatabase,
                                        OdStringArray& namesLayoutView,
                                        OdDbStubPtrArray& idsLayoutView,
                                        OdString* pActiveLayoutViewName = NULL,
                                        bool bOneForModel = true,
                                        OdGsDCRectArray* pRects = NULL);
  virtual bool argsAtSetupLayoutViews(OdRxObject* pRxDatabase, bool& bDoZoomExtents, OdGsView::RenderMode& rm);

  //// get point, zoom etc issues
  virtual OdGsView* getActiveGsView(OdGsDevice* pGsDevice);
  virtual OdGsModel* getGsModel(OdGsDevice* pGsDevice);

  virtual OdUnitsFormatter* getFormatter(OdRxObject* pRxDatabase);

  //// selection issues
  //virtual OdSelectionSetPtr createSelectionSet(OdRxObject* pRxDatabase);
  //virtual OdSelectionSetPtr select(OdGsView* gsView,
  //                                 int nPoints,
  //                                 const OdGePoint3d* wcsPts,
  //                                 OdDbVisualSelection::Mode mode = OdDbVisualSelection::kCrossing,
  //                                 OdDbVisualSelection::SubentSelectionMode sm = OdDbVisualSelection::kDisableSubents,
  //                                 const OdRxObject* pFilter = NULL);
  //virtual void getVisibleAllIds(OdRxObject* pRxDatabase, OdDbStubPtrArray& ids);
  //virtual OdDbStub* getVisibleLastId(OdRxObject* pRxDatabase);
  //virtual void highlight(OdGsView* gsView, bool bValue,
  //                       OdDbStub* id, const OdDbBaseFullSubentPath* pPath);
  virtual bool isErased(OdDbStub* id);
  virtual OdDbHandle getElementHandle(OdDbStub* id);
  virtual OdString getElementClassName(OdDbStub* id);

  //// command issues
  //virtual bool cmdErase(OdEdCommandContext* pCmdCtx);

  // filters issues
  virtual bool isResponsible(OdRxObject* pRxDatabase);
  virtual OdStringArray getExtensions(enum OdEd::GetFilePathFlags flg = OdEd::kGfpForOpen, 
                                      OdRxObject* pRxDatabase = NULL,
                                      bool bWithExtSynonyms = false);
  virtual OdString getFilterByExtension(const OdString& sExt, 
                                        int index = 0,
                                        enum OdEd::GetFilePathFlags flg = OdEd::kGfpForOpen,
                                        OdStringArray* pExtSynonyms = NULL);

  /////// Commands ///////
  //#define ODRX_CMD_ENTRY(cmdName, name, impl) OdStaticRxObject<OdqCmd##name> m_cmd##name;
  //#include "PrcDbCommands.h"
};

ODRX_DEFINE_DYNAMIC_MODULE(OdPrcDbModuleImpl);

OdPrcDbModuleImpl::OdPrcDbModuleImpl()
{
  //#define ODRX_CMD_ENTRY(cmdName, name, impl) m_cmd##name.m_pModule = this;
  //#include "PrcDbCommands.h"
}

OdPrcDbModuleImpl::~OdPrcDbModuleImpl()
{
}

void OdPrcDbModuleImpl::initApp()
{
  m_pDbModule = ::odrxDynamicLinker()->loadApp(OdPrcModuleName);
  OdExtDbModuleBaseImpl::initApp();
  // ...::rxInit();
  // register commands
  //OdEdCommandStackPtr pCommands = odedRegCmds();
  //#define ODRX_CMD_ENTRY(cmdName, name, impl) pCommands->addCommand(&m_cmd##name);
  //#include "PrcDbCommands.h"
}

void OdPrcDbModuleImpl::uninitApp()
{
  // unregister commands
  //OdEdCommandStackPtr pCommands = odedRegCmds();
  //#define ODRX_CMD_ENTRY(cmdName, name, impl) pCommands->removeCmd(&m_cmd##name);
  //#include "PrcDbCommands.h"
  // ...::rxUninit();
  OdExtDbModuleBaseImpl::uninitApp();
}

bool OdPrcDbModuleImpl::isValid()
{
  return !m_pDbModule.isNull();
}

// init issues
void OdPrcDbModuleImpl::setBaseHostAppServices(OdDbBaseHostAppServices* pServices)
{
  appPrcServicesImpl()->m_pBaseHostAppServices = pServices;
}

//void OdPrcDbModuleImpl::initOdExtPE()
//{
//  ::initOdDgSysVarPE();
//  appPrcServicesImpl()->restoreRegVariables();
//}

//void OdPrcDbModuleImpl::uninitOdExtPE()
//{
//  appPrcServicesImpl()->saveRegVariables();
//  ::uninitOdDgSysVarPE();
//}

//OdStringArray OdPrcDbModuleImpl::resetRegVariables() // set to default values
//{
//  return OdStringArray(); //return appPrcServicesImpl()->resetRegVariables();
//}

//bool OdPrcDbModuleImpl::getPreviewBitmap(const OdString& fileName, 
//                                         OdBinaryData& dataBmp) // out
//{
//  OdPrcFilePtr pPrcDb = appPrcServices()->readFile(fileName);
//  if (pPrcDb.isNull())
//    return false;
//  OdPrcObjectId id = pPrcDb->currentLayoutId();
//  ODA_ASSERT_ONCE_X(TPrc, !id.isNull());
//  if (id.isNull())
//    return false;
//  OdPrcViewPtr pView = id.openObject();
//  return pView->getThumbnail(dataBmp);
//}

//OdRxObjectPtr OdPrcDbModuleImpl::readFile(const OdString& fileName, 
//                                          Oda::FileShareMode shareMode) // = Oda::kShareDenyWrite
//{
//  OdRxObjectPtr pRxDb = OdExtDbModuleBaseImpl::readFile(fileName, shareMode, pbDoZoomExtents);
//  return pRxDb;
//}

OdRxObjectPtr OdPrcDbModuleImpl::createUninitializedDatabase()
{
  OdPrcFilePtr pPrcDb = appPrcServicesImpl()->createDatabase();
  return OdRxObject::cast(pPrcDb);
}

bool OdPrcDbModuleImpl::loadToUninitializedDatabase(OdRxObjectPtr pRxDb, 
                                                    const OdString& fileName, 
                                                    Oda::FileShareMode) // shareMode // = Oda::kShareDenyWrite
{
  OdPrcFilePtr pPrcDb = OdPrcFile::cast(pRxDb.get());
  ODA_ASSERT_ONCE(pPrcDb.get());
  if (pPrcDb.isNull())
    return false;

  pPrcDb->readFile(fileName);
  return true;
}

bool OdPrcDbModuleImpl::writeFile(const OdString& fileName,
                                  OdEdCommandContext* ctx)
{
  ODA_ASSERT_ONCE(ctx);
  OdPrcFilePtr pPrcDb = OdPrcFile::cast(ctx->baseDatabase());
  if (pPrcDb.isNull())
    return false;

  ctx->userIO()->putError(L"Writing of Prc database is not supported.") ;
  //ODA_FAIL_ONCE(); // TODO //pPrcDb->writeFile(fileName);
  return true;
}

// Cloud command command context issues
// possible TODO move reset, baseIO & cloneObject into OdEdCommandContext

//class OdExPrcCmdContext : public ExDbCommandContext {
//protected:
//  OdEdCommandContext* m_pOwner; // of clone
//
//  OdExPrcCmdContext()
//    : m_pOwner(NULL)
//  {
//  }
//
//public:
//
//  static OdEdCommandContextPtr createObject(OdEdBaseIO* pIOStream, 
//                                            OdRxObject* pRxDb = NULL,
//                                            OdEdCommandContext* pOwner = NULL)
//  {
//    OdEdCommandContextPtr pRes = OdRxObjectImpl<OdExPrcCmdContext, ExDbCommandContext>::createObject();
//    OdExPrcCmdContext* pCmdCtx = static_cast<OdExPrcCmdContext*>(pRes.get());
//    pCmdCtx->m_pDb = OdPrcFile::cast(pRxDb).get();
//    pCmdCtx->m_pIoStream = pIOStream;
//    pCmdCtx->m_pOwner = pOwner;
//    //m_pDb->addRef();
//    return pRes;
//  }
//
//  OdEdBaseIO* baseIO()
//  {
//    return m_pIoStream.get();
//  }
//
//  virtual OdEdCommandContextPtr cloneObject(OdEdBaseIO* pIOStream = NULL,
//                                            OdRxObject* pRxDb = NULL)
//  {
//    OdEdCommandContextPtr pRes = OdRxObjectImpl<OdExPrcCmdContext, ExDbCommandContext>::createObject();
//    //OdExPrcCmdContext* pCmdCtx = static_cast<OdExPrcCmdContext*>(pRes.get());
//    return createObject(pIOStream ? pIOStream : m_pIoStream.get(), 
//                        pRxDb ? pRxDb : m_pDb,
//                        this);
//  }
//};

//OdEdBaseIO* OdPrcDbModuleImpl::baseIO(OdEdCommandContext* pCtx)
//{
//  if (!pCtx || OdPrcFile::cast(pCtx->baseDatabase()).isNull())
//    return NULL;
//  OdExPrcCmdContext* pCmdCtx = static_cast<OdExPrcCmdContext*>(pCtx);
//  return pCmdCtx->baseIO();
//}
//
//OdEdCommandContextPtr OdPrcDbModuleImpl::cloneCommandContext(OdEdCommandContext* pCtx,
//                                                             OdEdBaseIO* pIOStream, // = NULL
//                                                             OdRxObject* pRxDb) // = NULL
//{
//  OdPrcFilePtr pPrcDb = OdPrcFile::cast(pRxDb);
//  ODA_ASSERT_ONCE(pPrcDb.get());
//  if (pPrcDb.isNull())
//    return OdEdCommandContextPtr();
//  
//  ODA_ASSERT_ONCE(pCtx); // to check with deb next
//  if (!pCtx || OdPrcFile::cast(pCtx->baseDatabase()).isNull())
//    return OdExPrcCmdContext::createObject(pIOStream, pRxDb);
//
//  OdExPrcCmdContext* pCmdCtx = static_cast<OdExPrcCmdContext*>(pCtx);
//  return pCmdCtx->cloneObject(pIOStream, pRxDb);
//}

bool OdPrcDbModuleImpl::getLayoutViewNamesAndIds(OdRxObject* pRxDatabase,
                                                 OdStringArray& namesLayoutView,
                                                 OdDbStubPtrArray&, //idsLayoutView
                                                 OdString* pActiveLayoutViewName, // = NULL
                                                 bool, //bOneForModel // = true
                                                 OdGsDCRectArray*)// pRects // = NULL
{
  if (!isResponsible(pRxDatabase))
    return false;
  namesLayoutView.push_back(L"Model");
  if (pActiveLayoutViewName)
    *pActiveLayoutViewName = namesLayoutView.last();
  return true;
}

bool OdPrcDbModuleImpl::argsAtSetupLayoutViews(OdRxObject* pRxDatabase, bool& bDoZoomExtents, OdGsView::RenderMode& rm)
{
  OdExtDbModuleBaseImpl::argsAtSetupLayoutViews(pRxDatabase, bDoZoomExtents, rm); // set for false case
  rm = OdGsView::kGouraudShaded;
  return true;
}

OdGsView* OdPrcDbModuleImpl::getActiveGsView(OdGsDevice* pGsDevice)
{
  OdGsPrcLayoutHelperPtr pLayoutHelper = OdGsPrcLayoutHelper::cast(pGsDevice);
  ODA_ASSERT_ONCE(pLayoutHelper.get());
  if (pLayoutHelper.isNull())
    return NULL;
  OdGsView* pGsView = pLayoutHelper->activeView().get();
  ODA_ASSERT_ONCE(pGsView);

  return pGsView;
}

OdGsModel* OdPrcDbModuleImpl::getGsModel(OdGsDevice* pGsDevice)
{
  OdGsPrcLayoutHelperPtr pLayoutHelper = OdGsPrcLayoutHelper::cast(pGsDevice);
  ODA_ASSERT_ONCE(pLayoutHelper.get());
  OdGsModel* pGsModel = pLayoutHelper->gsModel();
  // it is NULL if GS cache off //ODA_ASSERT_ONCE(pGsModel);
  return pGsModel;
}

OdUnitsFormatter* OdPrcDbModuleImpl::getFormatter(OdRxObject* pRxDatabase)
{
  OdPrcFilePtr pPrcDb = OdPrcFile::cast(pRxDatabase);
  if (pPrcDb.isNull())
    return NULL;

  return &pPrcDb->formatter(); // return &m_formatter; // TODO special for each model in view (via active model)
}

// selection issues
//OdSelectionSetPtr OdPrcDbModuleImpl::createSelectionSet(OdRxObject* pRxDatabase)
//{
//  return OdSelectionSetPtr(); // TODO
//}

//OdSelectionSetPtr OdPrcDbModuleImpl::select(OdGsView* gsView,
//                                            int nPoints,
//                                            const OdGePoint3d* wcsPts,
//                                            OdDbVisualSelection::Mode mode, // = OdDbVisualSelection::kCrossing
//                                            OdDbVisualSelection::SubentSelectionMode sm, // = OdDbVisualSelection::kDisableSubents
//                                            const OdRxObject* pFilter) // = NULL
//{
//  ODA_FAIL_ONCE(); // TODO
//  return OdSelectionSetPtr();
//
//  //if (!gsView || !gsView->userGiContext())
//  //{
//  //  ODA_FAIL_ONCE(); // test
//  //  return OdSelectionSetPtr();
//  //}
//  //OdPrcFilePtr pPrcDb = OdPrcFile::cast(gsView->userGiContext()->database());
//  //if (pPrcDb.isNull())
//  //  return OdSelectionSetPtr();
//  //return OdPrcSelectionSet::select(pPrcDb, gsView, nPoints, wcsPts, mode, sm, pFilter);
//}

//static bool isElementLocked(OdPrcObjectPtr pElem)
//{
//  if (pElem.isNull())
//    return false;
//
//  if (pElem->getLockedFlag())
//    return true;
//
//  OdDgGraphicsElementPtr pItem = OdDgGraphicsElement::cast(pElem);
//  if (pItem.isNull())
//    return false;
//
//  if (!pItem->getLevelId().isNull())
//  {
//    OdDgLevelTableRecordPtr pLevel = pItem->getLevelId().safeOpenObject();
//    if (pLevel->getIsFrozenFlag() || pLevel->getIsReadOnlyFlag())
//      return true;
//  }
//
//  OdPrcObjectIteratorPtr pItr;
//  {
//    OdDgCellHeader2dPtr pHeader2d = OdDgCellHeader2d::cast(pItem);
//    OdDgCellHeader3dPtr pHeader3d = OdDgCellHeader3d::cast(pItem);
//    if (!pHeader2d.isNull())
//      pItr = pHeader2d->createIterator();
//    if (pItr.isNull() && !(pHeader3d = OdDgCellHeader3d::cast(pItem)).isNull())
//      pItr = pHeader3d->createIterator();
//  }
//  if (pItr.isNull())
//    return false;
//  
//  for (; !pItr->done(); pItr->step())
//  {
//    pElem = OdPrcObject::cast(pItr->item().safeOpenObject());
//    if (pElem.isNull() || !isElementLocked(pElem))
//      continue;
//    return true;
//  }
//
//  return false;
//}

//void OdPrcDbModuleImpl::getVisibleAllIds(OdRxObject* pRxDatabase, OdDbStubPtrArray& ids)
//{
//  OdPrcFilePtr pPrcDb = OdPrcFile::cast(pRxDatabase);
//  if (pPrcDb.isNull())
//    return;
//
//  ODA_FAIL_ONCE(); // TODO
//  //OdPrcModelPtr pModel = OdPrcModel::cast(pPrcDb->getActiveModelId().openObject());
//  //ODA_ASSERT_ONCE(!pModel.isNull());
//  //if (pModel.isNull())
//  //  return;
//  //int idx = 0;
//  //for (OdPrcObjectIteratorPtr pItr = pModel->createGraphicsElementsIterator();
//  //     idx < 2; pItr = pModel->createControlElementsIterator(), idx++)
//  //{
//  //  for (; !pItr->done(); pItr->step())
//  //  {
//  //    OdPrcObjectId idElem = pItr->item();
//  //    ODA_ASSERT_ONCE(!idElem.isNull());
//  //    if (idElem.isNull())
//  //      continue;
//
//  //    OdPrcObjectPtr pElem = OdPrcObject::cast(idElem.openObject());
//  //    ODA_ASSERT_ONCE(!pElem.isNull());
//  //    if (pElem.isNull() || isElementLocked(pElem))
//  //      continue;
//  //    if (!pElem->gsNode()) // Cache
//  //      continue;
//
//  //    ids.push_back(idElem);
//  //  }
//  //}
//}

//OdDbStub* OdPrcDbModuleImpl::getVisibleLastId(OdRxObject* pRxDatabase)
//{
//  OdPrcFilePtr pPrcDb = OdPrcFile::cast(pRxDatabase);
//  if (pPrcDb.isNull())
//    return NULL;
//
//  ODA_FAIL_ONCE(); // TODO
//  //OdPrcModelPtr pModel = OdPrcModel::cast(pPrcDb->getActiveModelId().openObject());
//  //ODA_ASSERT_ONCE(!pModel.isNull());
//  //if (pModel.isNull())
//  //  return NULL;
//  //int idx = 0;
//  //for (OdPrcObjectIteratorPtr pItr = pModel->createGraphicsElementsIterator(false);
//  //     idx < 2; pItr = pModel->createControlElementsIterator(false), idx++)
//  //{
//  //  for (; !pItr->done(); pItr->step())
//  //  {
//  //    OdPrcObjectId idElem = pItr->item();
//  //    ODA_ASSERT_ONCE(!idElem.isNull());
//  //    if (idElem.isNull())
//  //      continue;
//
//  //    OdPrcObjectPtr pElem = OdPrcObject::cast(idElem.openObject());
//  //    ODA_ASSERT_ONCE(!pElem.isNull());
//  //    if (pElem.isNull() || isElementLocked(pElem))
//  //      continue;
//  //    if (!pElem->gsNode()) // Cache
//  //      continue;
//
//  //    return idElem;
//  //  }
//  //}
//
//  return NULL;
//}

//class OdPrcGsPath
//{
//  struct Node : OdGiPathNode
//  {
//    const Node*     m_pParent;
//    OdDbStub*       m_pId;
//    OdGiDrawablePtr m_pDrawable;
//    OdGsMarker      m_gsMarker;
//
//    const OdGiPathNode* parent() const 
//    { 
//      return m_pParent; 
//    }
//    OdDbStub* persistentDrawableId() const 
//    { 
//      return m_pId; 
//    }
//    const OdGiDrawable* transientDrawable() const 
//    { 
//      return m_pDrawable; 
//    }
//    OdGsMarker selectionMarker() const 
//    { 
//      return m_gsMarker; 
//    }
//  };
//  const Node* m_pLeaf;
//
//  void add(const OdGiDrawable* pDrawable, const OdPrcObjectId& drawableId, OdGsMarker gsMarker = -1)
//  {
//    Node* pNode = new Node();
//    pNode->m_pParent    = m_pLeaf;
//    m_pLeaf = pNode;
//
//    pNode->m_pDrawable  = pDrawable;
//    pNode->m_pId        = drawableId;
//    pNode->m_gsMarker   = gsMarker;
//  }
//
//  //void addNode(OdPrcObjectIdArray::const_iterator& iter)
//  //{
//  //  OdPrcObjectPtr pObj = iter->safeOpenObject();
//  //  addNode(pObj);
//  //  OdDgBlockReferencePtr pInsert = OdDgBlockReference::cast(pObj);
//  //  if (pInsert.get())
//  //    addNode(pInsert->blockTableRecord());
//  //  ++iter;
//  //}
//public:
//  OdPrcGsPath()
//    : m_pLeaf(0)
//  {
//  }
//  ~OdPrcGsPath()
//  {
//    clear();
//  }
//  OdPrcGsPath(const OdDgFullSubentPath& path)
//    : m_pLeaf(0)
//  {
//    set(path);
//  }
//
//  void clear()
//  {
//    while(m_pLeaf)
//    {
//      const Node* pNode = m_pLeaf;
//      m_pLeaf = pNode->m_pParent;
//      delete pNode;
//    }
//    m_pLeaf = NULL;
//  }
//
//  void set(const OdDgFullSubentPath& path)
//  {
//    set(path, kNullSubentIndex);
//  }
//  void set(const OdDgFullSubentPath& path, OdGsMarker gsMarker)
//  {
//    clear();
//    const OdPrcObjectIdArray& ids = path.objectIds();
//
//    OdPrcObjectIdArray::const_iterator iter = ids.begin();
//    if (iter==ids.end())
//      throw OdError(eInvalidInput);
//
//    OdPrcObjectPtr pObj = iter->safeOpenObject();
//    if (!pObj->ownerId().isNull())
//      addNode(pObj->ownerId());
//    for (; iter != ids.end()-1; ++iter)
//      addNode(*iter);
//
//    addNode(*iter, gsMarker);
//  }
//
//  void addNode(const OdPrcObjectId& drawableId, OdGsMarker gsMarker = kNullSubentIndex)
//  {
//    add(0, drawableId, gsMarker);
//  }
//  void addNode(const OdGiDrawable* pDrawable, OdGsMarker gsMarker = kNullSubentIndex)
//  {
//    add(pDrawable->isPersistent() ? 0 : pDrawable, pDrawable->id(), gsMarker);
//  }
//
//  operator const OdGiPathNode&() const
//  {
//    return *m_pLeaf;
//  }
//};

//void OdPrcDbModuleImpl::highlight(OdGsView* gsView, bool bValue,
//                                  OdDbStub* id, const OdDbBaseFullSubentPath* pPath)
//{
//  OdRxObject* pRxDatabase = baseDatabaseBy(id);
//  ODA_ASSERT_ONCE(pRxDatabase && id);
//  if (!pRxDatabase || !id)
//    return;
//
//  ODA_FAIL_ONCE(); // TODO
//  //OdPrcObjectId idEnt(id);
//  //OdPrcObjectPtr pEnt = idEnt.openObject();
//  //if (pEnt.isNull())
//  //  return; // erased
//  //
//  //// TODO via pEnt->highlight(bValue, &pPath);
//  //// see OdDbEntity::subHighlight
//  ////
//  //OdGsCache* pCache = pEnt->gsNode();
//  //if (!pCache)
//  //{
//  //  pEnt->downgradeOpen();
//  //  pCache = pEnt->gsNode();
//  //  ODA_ASSERT_ONCE(pCache);
//  //  if (!pCache)
//  //    return;
//  //}
//
//  //if (!pPath)
//  //{
//  //  //if (pEnt->ownerId().isNull())
//  //  //  return;
//  //  OdPrcGsPath gsPath; //OdDbGsPath gsPath;
//  //  if (!pEnt->ownerId().isNull())
//  //    gsPath.addNode(pEnt->ownerId());
//  // #ifdef _DEBUG
//  //  else
//  //    gsPath = gsPath; // brk !!!!
//  // #endif
//  //  gsPath.addNode(pEnt);
//  //  pCache->model()->highlight(gsPath, bValue);
//  //  return;
//  //}
//
//  //ODA_FAIL_ONCE(); // TODO
//  ////OdGsMarkerArray gsMarkers;
//  ////pEnt->getGsMarkersAtSubentPath(*pPath, gsMarkers);
//  ////for (OdGsMarkerArray::iterator sm = gsMarkers.begin(); sm != gsMarkers.end(); ++sm)
//  ////{
//  ////  OdPrcGsPath gsPath; //OdDbGsPath gsPath;
//  ////  gsPath.set(*pPath, *sm);
//  ////  pCache->model()->highlight(gsPath, bValue);
//  ////}
//}

bool OdPrcDbModuleImpl::isErased(OdDbStub* id)
{
  OdPrcFilePtr pPrcDb = OdPrcFile::cast(baseDatabaseBy(id));
  ODA_ASSERT_ONCE(id && !pPrcDb.isNull());
  if (pPrcDb.isNull())
    return false;
  OdPrcObjectId idElem(id);
  return idElem.isErased();
}

OdDbHandle OdPrcDbModuleImpl::getElementHandle(OdDbStub* id)
{
  return OdPrcObjectId(id).getHandle();
}

OdString OdPrcDbModuleImpl::getElementClassName(OdDbStub* id)
{
  if (!id)
    return OdPrcFile::desc()->name();
  OdPrcFilePtr pPrcDb = OdPrcFile::cast(baseDatabaseBy(id));
  ODA_ASSERT_ONCE(id && !pPrcDb.isNull());
  if (pPrcDb.isNull())
    return OdString::kEmpty;

  OdPrcObjectId idElem(id);
  OdPrcObjectPtr pElem = idElem.openObject();
  ODA_ASSERT_ONCE(pElem.get());
  if (pElem.isNull())
    return OdString::kEmpty;
  OdString sName = pElem->isA()->name();
  return sName;
}

//bool OdPrcDbModuleImpl::cmdErase(OdEdCommandContext* pCmdCtx)
//{
//  OdPrcFilePtr pPrcDb = OdPrcFile::cast(pCmdCtx->baseDatabase());
//  if (pPrcDb.isNull())
//    return false;
//
//  ODA_FAIL_ONCE(); // TODO
//  return false;
//  //OdDbUserIOPtr pIO = pCmdCtx->userIO();
//  //OdSelectionSetPtr pSSet = pIO->select();
//
//  //int nLockedLayer = 0,
//  //    nCount = 0;
//  //for (OdPrcSelectionSetIteratorPtr pItr = pSSet->newIterator();
//  //     !pItr->done(); pItr->next())
//  //{
//  //  OdPrcObjectId idElem = pItr->objectId();
//  //  OdPrcObjectPtr pElem = OdPrcObject::cast(idElem.safeOpenObject(OdDg::kForWrite));
//  //  ODA_ASSERT_ONCE(!pElem.isNull());
//  //  if (pElem.isNull())
//  //    continue;
//
//  //  nCount++;
//  //  if (isElementLocked(pElem))
//  //  {
//  //    nLockedLayer++;
//  //    continue;
//  //  }
//  //  // already contained via OdPrcSelectionSet
//  //  //OdGsCache* pCsh = pElem->gsNode();
//  //  //if (pCsh)
//  //  //  pCsh->model()->onErased(pElem, pElem->ownerId());
//  //  
//  //  pElem->erase();
//  //}
//  //if (!nCount)
//  //  return false;
//
//  //ODA_ASSERT_ONCE(OdPropServices::cast(pCmdCtx).get());
//  //OdString sMsg;
//  //sMsg.format(OdPropServices::cast(pCmdCtx)->tr("  %d found.").c_str(),
//  //  nCount);
//  //pIO->putString(sMsg);
//
//  //if (nLockedLayer)
//  //{
//  //  sMsg.format(OdPropServices::cast(pCmdCtx)->tr( // msgNWasWereOnALockedLayer
//  //              (nLockedLayer > 1) ? "  %d were on a locked layer."
//  //                                 : "  %d was on a locked layer.").c_str(), 
//  //              nLockedLayer);
//  //  pIO->putString(sMsg);
//  //}
//
//  //return true;
//}

bool OdPrcDbModuleImpl::isResponsible(OdRxObject* pRxDatabase)
{
  ODA_ASSERT_ONCE(pRxDatabase); // test
  OdPrcFilePtr pPrcDb = OdPrcFile::cast(pRxDatabase);
  return !pPrcDb.isNull();
}

OdStringArray OdPrcDbModuleImpl::getExtensions(enum OdEd::GetFilePathFlags flg, // = OdEd::kGfpForOpen
                                               OdRxObject* pRxDatabase, // = NULL
                                               bool) // bWithExtSynonyms = false
{
  OdStringArray lst;
  if (flg == OdEd::kGfpForOpen && (!pRxDatabase || isResponsible(pRxDatabase)))
    lst.push_back(L"prc");
  return lst;
}

OdString OdPrcDbModuleImpl::getFilterByExtension(const OdString& sExt, 
                                                 int index, // = 0
                                                 enum OdEd::GetFilePathFlags, // flg = OdEd::kGfpForOpen
                                                 OdStringArray*) // pExtSynonyms = NULL
{
  if (index)
    return OdString::kEmpty;
  if (sExt == L"prc")
    return L"PRC (*.prc)|*.prc|";
  return OdString::kEmpty;
}
