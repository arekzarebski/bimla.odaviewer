TARGET = TD_DgnDb
TEMPLATE = lib

exists(../OdaQtPaths.pri): include(../OdaQtPaths.pri)

!android {

  win32:TARGET_EXT = $${ODADLLSUFFIX}.tx
  unix:!mac {
    # unixmake2.cpp: } else if(project->isEmpty("QMAKE_HPUX_SHLIB")) {

    CONFIG += plugin no_plugin_name_prefix
    QMAKE_EXTENSION_PLUGIN = tx
  }
  macx {
    # http://bugreports.qt.nokia.com/browse/QTBUG-5429
    # QtBUG 5429 : QMake Completely Ignores TARGET_EXT on Mac OS X

    # via adv params of xcodebuild call :
    CONFIG += plugin no_plugin_name_prefix
    QMAKE_EXTENSION_PLUGIN = tx

    # generated project.pbxproj should contained the next
    # to build shared dll without dylib extension and lib suffix :
    #
    #     PRODUCT_NAME = "OdaQtOpenGL";
    #     EXECUTABLE_EXTENSION = .txv;
    #     #EXECUTABLE_PREFIX = "";
    #
    # but qmake/generatoe/mac/pbuilder_pbx.cpp (ProjectBuilderMakefileGenerator::writeMakeParts(...))
    # does not contain code to set EXECUTABLE_EXTENSION (or using ODADLLSUFFIX, QMAKE_POST_LINK)
    # and include next hardcode fragment for PRODUCT_NAME :
    #
    #    QString lib = project->first("QMAKE_ORIG_TARGET");
    #    if(!project->isActiveConfig("lib_bundle") && !project->isActiveConfig("staticlib"))
    #        lib.prepend("lib"); // !!!!!!!!!!!!!!!!
    #    t << "\t\t\t\t" << writeSettings("PRODUCT_NAME", escapeFilePath(lib)) << ";" << "\n";
    #
  }

  win32 {
    DESTDIR = $${ODADIR}/exe/$${TD_CONF_NAME}
    #PRJ_DESTDIR = $${ODADIR}/$${QTMAKEPLATFORMS_FLD_NAME}/$$TD_CONF_NAME/ProjectFiles/Examples/Qt/
    PRJ_DESTDIR = ""

    # (see result at project property / C/C++ / Output Files / Program Database File Name)
    TMPVAR = $$(QMAKESPEC)
    !isEqual(TMPVAR, ""): QMAKE_CXXFLAGS += /Fd$(IntDir)/
    #QMAKE_CXXFLAGS_DEBUG and QMAKE_CXXFLAGS_RELEASE 

    OBJECTS_DIR = $${PRJ_DESTDIR}build/$$TARGET 
    MOC_DIR = $${PRJ_DESTDIR}build/$$TARGET
    UI_DIR = $${PRJ_DESTDIR}build/$$TARGET
    RCC_DIR = $${PRJ_DESTDIR}build/$$TARGET

  } else {

    #DESTDIR = $${ODADIR}/bin/$${TD_CONF_NAME}
    DESTDIR = $${ODADIR}/bin/$${TD_CONF_NAME}

    #PRJ_DESTDIR = $${ODADIR}/bin/$${TD_CONF_NAME}/Examples/Qt/
    #PRJ_DESTDIR = $${PRJDEST}/
    #
    PRJ_DESTDIR = $${ODADIR}/bin/$${TD_CONF_NAME}/Examples/Qt/

    OBJECTS_DIR = $${PRJ_DESTDIR}build
    MOC_DIR = $${PRJ_DESTDIR}build
    UI_DIR = $${PRJ_DESTDIR}build
    RCC_DIR = $${PRJ_DESTDIR}build
  }

  win32:LIBS += -L$${ODADIR}/lib/$${TD_CONF_NAME}
  !win32:LIBS += -L$${ODADIR}/bin/$${TD_CONF_NAME}
  !win32:LIBS += -L$${ODADIR}/lib/$${TD_CONF_NAME}

} else {
  CONFIG += staticlib
}

COMMONPATH = $${ODADIR}/Drawing/Examples/Qt/Common

INCLUDEPATH += $$COMMONPATH
INCLUDEPATH += $${ODADIR}/Kernel/Examples/Common
INCLUDEPATH += $${ODADIR}/Drawing/Examples/Common
INCLUDEPATH += $${ODADIR}/Kernel/Include
INCLUDEPATH += $${ODADIR}/Drawing/Include
INCLUDEPATH += $${ODADIR}/Dgn/include
INCLUDEPATH += $${ODADIR}/Dgn/source
INCLUDEPATH += $${ODADIR}/Dgn/source/Common
INCLUDEPATH += $${ODADIR}/Dgn/Extensions/ExServices
INCLUDEPATH += $${ODADIR}/Kernel/Extensions/ExServices
INCLUDEPATH += $${ODADIR}/Drawing/Imports/DgnImport
INCLUDEPATH += $${ODADIR}/ThirdParty/activation

HEADERS += $${ODADIR}/Kernel/Examples/Common/ExtDbModule.h

#HEADERS += ../../Common/SysVarPE.h
!exists( $${ODADIR}/Kernel/Include/SysVarPE.h )  {
  HEADERS += $${ODADIR}/Drawing/Examples/Common/SysVarPE.h
  DEFINES += ODA_SYS_VAR_PE_UNLINKED_RXINIT
}
else {
  HEADERS += $${ODADIR}/Kernel/Include/SysVarPE.h
}
#HEADERS += ../Common/PropServices.h
!exists( $${ODADIR}/Kernel/Include/PropServices.h )  {
  HEADERS += $${ODADIR}/Drawing/Examples/Qt/Common/PropServices.h
  DEFINES += ODA_PROP_SERVICES_UNLINKED_RXINIT
}
else {
  HEADERS += $${ODADIR}/Kernel/Include/PropServices.h
}
#HEADERS += ../Common/ExAppServices.h
!exists( $${ODADIR}/Kernel/Include/ExAppServices.h )  {
  HEADERS += $${ODADIR}/Drawing/Examples/Qt/Common/ExAppServices.h
  DEFINES += ODA_EX_APP_SERVICES_UNLINKED_RXINIT
}
else {
  HEADERS += $${ODADIR}/Kernel/Include/ExAppServices.h
}

#HEADERS += DgSysVarPEImpl.h
HEADERS += $${ODADIR}/Kernel/Examples/Common/ExtDbModule.h
HEADERS += $${ODADIR}/Drawing/Examples/Qt/Common/ExtDbModuleBaseImpl.h
HEADERS += $${ODADIR}/Drawing/Examples/Qt/DgnDb/DgSysVarPEImpl.h
HEADERS += $${ODADIR}/Drawing/Examples/Qt/DgnDb/DgnDbModuleImpl.h
HEADERS += $${ODADIR}/Drawing/Examples/Qt/DgnDb/DgnDbCommands.h

HEADERS += $${ODADIR}/Dgn/Extensions/ExServices/ExDgnServices.h 
HEADERS += $${ODADIR}/Dgn/Extensions/ExServices/ExDgnHostAppServices.h
HEADERS += $${ODADIR}/Dgn/Extensions/ExServices/ExDgCommandContext.h
HEADERS += $${ODADIR}/Kernel/Extensions/ExServices/OdFileBuf.h
HEADERS += $${ODADIR}/Drawing/Imports/DgnImport/DgnImport.h

HEADERS += DgXmlPropSources.h

SOURCES += DgnDbModule.cpp
SOURCES += DgSysVarPEImpl.cpp
SOURCES += DgnDbCommands.cpp
SOURCES += DgXmlPropSources.cpp

#
SOURCES += $${ODADIR}/Dgn/Extensions/ExServices/ExDgnServices.cpp
SOURCES += $${ODADIR}/Dgn/Extensions/ExServices/ExDgnHostAppServices.cpp
SOURCES += $${ODADIR}/Dgn/Extensions/ExServices/ExDgCommandContext.cpp
SOURCES += $${ODADIR}/Kernel/Extensions/ExServices/ExKWIndex.cpp
#SOURCES += $${ODADIR}/Dgn/Extensions/ExServices/ExDgnFileBuf.cpp
SOURCES += $${ODADIR}/Kernel/Extensions/ExServices/OdFileBuf.cpp

win32:!android: RC_FILE = data/DgnDb.rc
CONFIG -= qt

# ----- DGN set -----

#ODA_DG_EXPORTS += ODA_DG_EXPORTS

DEFINES += UNICODE
DEFINES += _UNICODE
DEFINES += _CRTDBG_MAP_ALLOC
!android {
  DEFINES += _TOOLKIT_IN_DLL_

  LIBS += -lTD_Alloc
  LIBS += -lTD_Ge
  LIBS += -lTD_Gi
  LIBS += -lTD_Gs
  LIBS += -lTD_DbRoot
  LIBS += -lTD_Root

  # reason to link with TD_Db: contains import to DWG code
  # adv reason to link with TD_Db: ExDgCommandContext uses OdResBuf
  LIBS += -lTD_Db -lTD_DbCore -lTD_DbIO -lTD_DbEntities -lSCENEOE -lACCAMERA -lRText -lATEXT -lISM -lWipeOut

  win32: LIBS += -lTG_Db
  !win32: LIBS += $${ODADIR}/bin/$${TD_CONF_NAME}/TG_Db.tx
  
  win32: isEqual(CONVERTEDBY, "") {
    LIBS += -ladvapi32 # -luser32 -lkernel32 -lgdi32 ...
  }
}

#DEPENDPATH = $$INCLUDEPATH
#sources.files = $$SOURCES $$HEADERS *.pro
