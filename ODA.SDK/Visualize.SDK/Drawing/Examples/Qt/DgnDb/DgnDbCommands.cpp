/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
//
// DgnDbCommands.cpp
//

#include "OdaCommon.h"
#include "DgnDbModuleImpl.h"
#include "DbHostAppServices.h"
#include "Ed/EdCommandContext.h"
#include "Ed/EdUserIO.h"

#if 0 // redesigned as import command in console now

#include "DgnImport.h" // Drawing/Imports/DgnImport
#include "PropServices.h"
#include "RxVariantValue.h"

using namespace TD_DGN_IMPORT;

void OdqCmd_DgnImport::execute(OdEdCommandContext* pCmdCtx)
{
  OdDbDatabasePtr pDwgDb = OdDbDatabase::cast(pCmdCtx->baseDatabase());
  if (pDwgDb.isNull())
    throw OdError(eNoDatabase);
  OdDbHostAppServices* pServices = pDwgDb->appServices();
  ODA_ASSERT_ONCE(pServices);
  bool bFileDialog = pServices->getFILEDIA();

  OdRxModulePtr pDgnImport = ::odrxDynamicLinker()->loadModule(OD_T("TD_DgnImport.tx"));
  if (pDgnImport.isNull())
  {
    pServices->warning(OD_T("TD_DgnImport.tx was not found"));
    throw OdError(eNotApplicable);
  }
  OdPropServicesPtr pPropServices = OdPropServices::cast(pCmdCtx);
  ODA_ASSERT_ONCE(!pPropServices.isNull());

  OdString sPath,
           sMsg;
  if (!bFileDialog || (!pPropServices.isNull() && pPropServices->isInputStringReady(pCmdCtx)))
  {
    sPath = pCmdCtx->userIO()->getString(OD_T("Enter name of DGN file to import"));
    if (!sPath.isEmpty() && !odrxSystemServices()->accessFile(sPath, Oda::kFileRead))
    {
      sMsg.format(L"File \"%1s\" was not found", sPath.c_str());
      pServices->warning(sMsg);
      return;
    }
  }

  static OdString s_sExtension;
  if (sPath.isEmpty())
  {
    sPath = pCmdCtx->userIO()->getFilePath(OD_T("Enter name of drawing to import"), 
                                           OdEd::kGfpForOpen, 
                                           OD_T("Select File to import"), // dialog caption 
                                           OD_T("dgn"), // default extension
                                           OdString::kEmpty, // file name
                                           OD_T("DGN files (*.dgn)|*.dgn||"));
    if (sPath.isEmpty())
      return; // without message "Cancel by user" // throw OdEdCancel();
  }

  // Create importer object
  //OdStaticRxObject<OdExDgnHostAppServices> dgSvc;
  OdDgnImportPtr importer = createDgnImporter();
  if (importer.isNull())
  {
    pServices->warning(OD_T("Could not create dgn importer"));
    return;
  }

  // Set the conversion parameters
  importer->properties()->putAt(L"Services", pServices);
  importer->properties()->putAt(L"DgnPath", OdRxVariantValue(sPath));
  appDgnServices()->disableProgressMeterOutput(true);
  importer->properties()->putAt(L"DgnServices", appDgnServices());
  importer->properties()->putAt(L"Database", pDwgDb);
  ODA_ASSERT_ONCE(pDwgDb == OdDbDatabase::cast(importer->properties()->getAt(L"Database")));

  //Import DGN file
  OdDgnImport::ImportResult res = importer->import();
  switch (res)
  {
  case OdDgnImport::fail:
    pServices->warning(OD_T("Unknown error."));
    break;
  case OdDgnImport::bad_password:
    pServices->warning(OD_T("Invalid password."));
    break;
  case OdDgnImport::bad_file:
    pServices->warning(OD_T("Cannot open file."));
    break;
  case OdDgnImport::bad_database:
    pServices->warning(OD_T("Invalid file."));
    break;
  case OdDgnImport::encrypted_file:
    pServices->warning(OD_T("Decryption error."));
    break;
  case OdDgnImport::success:
    ODA_ASSERT_ONCE(pDwgDb == OdDbDatabase::cast(importer->properties()->getAt(L"Database")));
    break;
  }
}

#endif

//////////////////////////////////////////////////////////////////////////
