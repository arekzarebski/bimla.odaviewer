/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
//
// DgXmlPropSources.cpp
//

#include "OdaCommon.h"
#include "DgXmlPropSources.h"
#include "DgDatabase.h"
#include "DgUnitsFormatter.h"
#include "DgGraphicsElement.h"
#include "DgLevelTableRecord.h"
#include "DgElementIdArray.h"

#include "RxObjectImpl.h"
#include "RxDictionary.h"

///////////////////////////////////////////////////////////////////////////////

static OdString levelToStr(OdDgElementId idLevel)
{
  //ODA_ASSERT_ONCE(!idLevel.isNull());
  if (idLevel.isNull())
    return OdString::kEmpty;
  OdString sLevel;
  //idLevel.convertToRedirectedId();
  OdDgLevelTableRecordPtr pLevel = OdDgLevelTableRecord::cast(idLevel.openObject());
  ODA_ASSERT_ONCE(pLevel.get());
  if (pLevel.get())
    sLevel = pLevel->getName();
  return sLevel;
}

static OdDgElementId strToLevel(OdRxObject* pRxDb, const OdString& sLevel, OdDgElementId idDefVal)
{
  OdDgDatabase* pDb = OdDgDatabase::cast(pRxDb).get();
  if (!pDb)
    return idDefVal;
  for (OdDgElementIteratorPtr pLevelIter = pDb->getLevelTable()->createIterator();
       !pLevelIter->done(); pLevelIter->step())
  {
    OdDgLevelTableRecordPtr pLevel = pLevelIter->item().openObject(OdDg::kForRead);
    ODA_ASSERT_ONCE(!pLevel.isNull());
    if (pLevel.isNull() || pLevel->getName() != sLevel)
      continue;
    return pLevelIter->item();
  }
  return idDefVal;
}

static bool getLevels(OdRxObject* pRxDb, OdStringArray& levels)
{
  OdDgDatabase* pDb = OdDgDatabase::cast(pRxDb).get();
  if (!pDb)
    return false;
  for (OdDgElementIteratorPtr pLevelIter = pDb->getLevelTable()->createIterator();
       !pLevelIter->done(); pLevelIter->step())
  {
    OdDgLevelTableRecordPtr pLevel = pLevelIter->item().openObject(OdDg::kForRead);
    ODA_ASSERT_ONCE(!pLevel.isNull());
    if (pLevel.isNull())
      continue;
    OdString sLevel = pLevel->getName();
    if (sLevel.isEmpty() || levels.contains(sLevel))
      continue;
    levels.push_back(sLevel);
  }
  return true;
}

static void setCmColorBaseByDgnIndex(OdDgDatabase* pDb, OdCmColorBase& dstClr, OdDgGraphicsElement* pEnt)
{
  // see also setDwgColorByDgnIndex
  ODA_ASSERT_ONCE(pEnt);

  OdUInt32 color = pEnt->getColorIndex();
  ODCOLORREF colorRef = OdDgColorTable::lookupRGB(pDb, color);

  if (color == OdDgCmEntityColor::kByLevel)
    dstClr.setColorMethod(OdCmEntityColor::kByLayer);
  else if (color == OdDgCmEntityColor::kByCell)
    dstClr.setColorMethod(OdCmEntityColor::kByBlock);
  else
  {
    ODA_ASSERT_ONCE(colorRef == pEnt->getColor());
    dstClr.setRGB(ODGETRED(colorRef), ODGETGREEN(colorRef), ODGETBLUE(colorRef));
  }
}

static OdString colorToStr(OdDgDatabase* pDgDb, OdDgGraphicsElement* pEnt)
{
  OdString sVal;
  OdPropServicesPtr pPropServices = ::odrxSysRegistry()->getAt(OD_PROP_SERVICES).get();
  if (pPropServices.get())
  {
    OdDgCmColor clr; // or OdCmColor clr;
    setCmColorBaseByDgnIndex(pDgDb, clr, pEnt); // see also setDwgColorByDgnIndex

    //sVal = pDgDb->formatter().formatColor(clr);
    //
    // OdDgUnitsFormatter::formatColor & OdDgUnitsFormatter::unformatColor have an empty
    // content. Therefore upper way is useless useless now.

    sVal = pPropServices->colorBaseToStr(pDgDb, clr);
  }
  return sVal;
}

static OdUInt32 strToColorIndex(OdDgDatabase* pDb, OdString sColor, OdUInt32 indexDef)
{
  OdPropServicesPtr pPropServices = ::odrxSysRegistry()->getAt(OD_PROP_SERVICES).get();
  if (pPropServices.isNull())
  {
    ODA_FAIL_ONCE(); // unsupported
    return indexDef;
  }
  OdDgCmColor clr;
  if (!pPropServices->colorBaseByStr(pDb, sColor, clr))
  {
    ODA_FAIL_ONCE();
    return indexDef;
  }
  if (clr.isByLayer())
    return OdDg::kColorByLevel; // 0xffffffff
  if (clr.isByBlock())
    return OdDg::kColorByCell; // 0xfffffffe
  ODCOLORREF rgbColor = ODRGB(clr.red(), clr.green(), clr.blue());
  OdUInt32 index = OdDgColorTable::getColorIndexByRGB(pDb, rgbColor);
  return index;
}

static bool getLineStyleNames(OdRxObject* pRxDb, OdStringArray& names, 
                              OdDgElementIdArray* pIds = NULL)
{
  ODA_ASSERT_ONCE(names.isEmpty() || !pIds);
  OdDgDatabase* pDb = OdDgDatabase::cast(pRxDb);
  if (!pDb)
    return false;

  for (OdInt32 entryId = 0; entryId < 8; entryId++)
  {
    OdString sStyle;
    sStyle.format(L"Internal%d", entryId);
    names.push_back(sStyle);
    if (!pIds)
      continue;
    pIds->push_back(OdDgElementId());
  }

  for (OdDgElementIteratorPtr pStyleIter = pDb->getLineStyleTable()->createIterator();
       !pStyleIter->done(); pStyleIter->step())
  {
    OdDgElementId idStyle = pStyleIter->item();
    OdDgTableRecordPtr pStyle = idStyle.openObject(OdDg::kForRead);
    ODA_ASSERT_ONCE(!pStyle.isNull());
    if (pStyle.isNull()) 
      continue;
    OdString sStyle = pStyle->getName();
    if (sStyle.isEmpty() || names.contains(sStyle))
    {
      ODA_FAIL_ONCE();
      continue;
    }
    names.push_back(sStyle);

    if (!pIds)
      continue;
    pIds->push_back(idStyle);
  }
  return names.size() != 0;
}

static OdString lineStyleToStr(OdDgGraphicsElement* pEnt)
{
  OdInt32 ls = pEnt->getLineStyleEntryId();
  if (ls == OdDg::kLineStyleByLevel) // 0x7fffffff
    return L"ByLevel";
  if (ls == OdDg::kLineStyleByCell) // 0x7ffffffe
    return L"ByCell";
  OdString sLineStyle; // = L"Default";
  OdDgElementId idLineStyle = pEnt->getLineStyleId();
  ODA_ASSERT_ONCE(!idLineStyle.isNull());
  OdDgLineStyleTableRecordPtr pLineStyle = idLineStyle.openObject();
  ODA_ASSERT_ONCE(pLineStyle.get());
  if (!pLineStyle.isNull())
  {
    sLineStyle = pLineStyle->getName();
    if (sLineStyle.isEmpty())
    {
      sLineStyle.format(L"Internal%d", pEnt->getLineStyleEntryId());
    }
  }
  return sLineStyle;
}

static bool setLineStyle(OdDgGraphicsElement* pEnt, const OdString& csValue)
{
  OdString sVal = csValue;
  if (sVal.makeLower().isEmpty())
  {
    ODA_FAIL_ONCE();
    return false;
  }
  OdInt32 ls = pEnt->getLineStyleEntryId(), ls_ = ls;
  if (sVal == L"bylevel")
  {
    ls = OdDg::kLineStyleByLevel;
    if (ls != ls_)
    {
      pEnt->setLineStyleEntryId(ls);
      return true;
    }
    return false;
  }
  if (sVal == L"bycell")
  {
    ls = OdDg::kLineStyleByCell;
    if (ls != ls_)
    {
      pEnt->setLineStyleEntryId(ls);
      return true;
    }
    return false;
  }
  if (!sVal.find(L"internal"))
  {
    OdInt32 entryId = odStrToInt(sVal = sVal.mid(8));
    pEnt->setLineStyleEntryId(entryId, false);
    return true;
  }

  OdStringArray names; OdDgElementIdArray ids;
  getLineStyleNames(pEnt->database(), names, &ids);
  unsigned int index = -1;
  if (!names.find(csValue, index) || index >= names.size())
  {
    ODA_FAIL_ONCE();
    return false;
  }
  ODA_ASSERT_ONCE(index >= 8 && !ids[index].isNull());

  pEnt->setLineStyleId(ids[index]);
  return true;
}

static OdString lineWeightToStr(OdUInt32 weight)
{
  if (weight == OdDg::kLineWeightByLevel) // 0xffffffff
    return L"ByLevel";
  if (weight == OdDg::kLineWeightByCell) // 0xfffffffe
    return L"ByCell";

  // see also OdqPropertyModel::stringByLineWeight
  ODA_ASSERT_ONCE(weight <= 31);
  if (weight > 31)
    return L"Invalid";
  //if (!weight)
  //  return L"Default";
  OdString sLineWeight;
  sLineWeight.format(L"%d", weight);
  return sLineWeight;
}

static OdUInt32 strToLineWeight(const OdString& csValue, OdUInt32 weightDef)
{
  OdString sVal = csValue;
  if (sVal.makeLower().isEmpty())
    return weightDef;
  if (sVal == L"bylevel")
    return OdDg::kLineWeightByLevel;
  if (sVal == L"bycell")
    return OdDg::kLineWeightByCell;
  //if (sVal == L"default")
  //  return 0;
  OdUInt32 weight = odStrToInt(sVal);
  ODA_ASSERT_ONCE(weight <= 31);
  if (weight > 31)
    return weightDef;
  return weight;
}

static bool getLineWeightsNames(OdRxObject*, OdStringArray& names)
{
  names.push_back(lineWeightToStr(OdDg::kLineWeightByLevel));
  names.push_back(lineWeightToStr(OdDg::kLineWeightByCell));
  for (OdUInt32 lw = 0; lw <= 31; lw++)
    names.push_back(lineWeightToStr(lw));
  return true;
}

static OdString classToStr(enum OdDgGraphicsElement::Class cls)
{
  OdString sVal;
  switch (cls)
  {
  default:
    ODA_FAIL_ONCE(); break;
  case OdDgGraphicsElement::kClassPrimary:
    sVal = L"Primary"; break;
  case OdDgGraphicsElement::kClassPatternComponent:
    sVal = L"Pattern Component"; break;
  case OdDgGraphicsElement::kClassConstruction:
    sVal = L"Construction"; break;
  case OdDgGraphicsElement::kClassDimension:
    sVal = L"Dimension"; break;
  case OdDgGraphicsElement::kClassPrimaryRule:
    sVal = L"Primary Rule"; break;
  case OdDgGraphicsElement::kClassLinearPatterned:
    sVal = L"Linear Patterned"; break;
  case OdDgGraphicsElement::kClassConstructionRule:
    sVal = L"Construction Rule"; break;
  }
  return sVal;
}

//static OdString transparencyToStr(OdDgDatabase* pDb, const OdCmTransparency& trn)
//{
//  OdCmTransparency::transparencyMethod mthd = trn.method();
//  switch (mthd)
//  {
//  case OdCmTransparency::kByLayer:
//    return L"ByLayer";
//  case OdCmTransparency::kByBlock:
//    return L"ByBlock"; // ? ByCell
//  case OdCmTransparency::kByAlpha:
//    break; // Use the Alpha-value in this object  
//  default:
//  case OdCmTransparency::kErrorValue:
//    return L"Error value";
//  }
//  OdString sVal;
//  sVal.format(L"%g", (1.0 - trn.alphaPercent()) * 100.0);
//  return sVal;
//}

//static OdCmTransparency strToTransparency(OdDgDatabase* pDb, const OdString& csVal, const OdCmTransparency& trnDef)
//{
//  OdString sVal = csVal; 
//  if (sVal.makeLower().isEmpty())
//    return trnDef;
//  OdCmTransparency trn = trnDef;
//  if (sVal == L"bylayer")
//    trn.setMethod(OdCmTransparency::kByLayer);
//  else if (sVal == L"byblock") // ? ByCell
//    trn.setMethod(OdCmTransparency::kByBlock);
//  else
//  {
//    trn.setMethod(OdCmTransparency::kByAlpha);
//    double alphaPercent = (100.0 - odStrToD(sVal)) / 100.0;
//    alphaPercent = fabs(alphaPercent);
//    trn.setAlphaPercent(alphaPercent);
//  }
//  return trn;
//}

///////////////////////////////////////////////////////////////////////////////

class OdDgXmlPropGraphicsElement : public OdPropBase
{
public:
  virtual bool getPropertyValues(OdPropContext& ctx)
  {
    OdDgGraphicsElementPtr pEnt = object();
    if (pEnt.isNull())
      return false;
    OdPropServicesPtr pPropServices = ::odrxSysRegistry()->getAt(OD_PROP_SERVICES).get();
    if (pPropServices.isNull())
      return false;

    OdPropContext ctxGen = ctx.addCategory(L"General");
    ctxGen.addProperty(L"Level", levelToStr(pEnt->getLevelId()), tCombo); //.comment(L"Specifies ...");
    ctxGen.addProperty(L"Color", colorToStr(pEnt->database(), pEnt), tOdColor);
    ctxGen.addProperty(L"Line Style", lineStyleToStr(pEnt), tCombo);
    ctxGen.addProperty(L"Weight", lineWeightToStr(pEnt->getLineWeight()), tCombo);
    ctxGen.addProperty(L"Class", classToStr(pEnt->getClass())).readOnly();
    //TIDO//ctxGen.addProperty(L"Template", ...);
    OdString sTrn = pPropServices->transparencyToStr(pEnt->database(), pEnt->getTransparency());
    ctxGen.addProperty(L"Transparency", sTrn, tTransparency);

    return true; 
  }

  virtual bool setPropertyValue(const OdString& sPath,
                                const OdString& sValue,
                                Action&) //action
  {
    OdString sName = sPath, sCat = extractFirst(sName);

    if (sCat == L"General") 
    {
      OdDgGraphicsElementPtr pEnt = object(); pEnt->upgradeOpen();
      OdPropServicesPtr pPropServices = ::odrxSysRegistry()->getAt(OD_PROP_SERVICES).get();
      if (sName == L"Level")
      {
        OdDgElementId idDefVal = pEnt->getLevelId(),
                      id = strToLevel(pEnt->database(), sValue, idDefVal);
        if (!id.isNull() && id != idDefVal)
          pEnt->setLevelId(id);
      }
      else if (sName == L"Color")
      {
        OdUInt32 indexDef = pEnt->getColorIndex(),
                 index = strToColorIndex(pEnt->database(), sValue, indexDef);
        if (index != indexDef)
          pEnt->setColorIndex(index);
      }
      else if (sName == L"Line Style")
        setLineStyle(pEnt, sValue);
      else if (sName == L"Weight")
      {
        OdUInt32 weightDef = pEnt->getLineWeight(),
                 weight = strToLineWeight(sValue, weightDef);
        if (weight != weightDef)
          pEnt->setLineWeight(weight);
      }
      else if (sName == L"Transparency" && pPropServices.get())
      {
        OdCmTransparency trnDef = pEnt->getTransparency(), trn;
        pPropServices->transparencyByStr(baseDatabase(), sValue, trn);
        if (trn != trnDef)
        {
          ODA_ASSERT_VAR(OdResult res =)
          pEnt->setTransparency(trn);
          ODA_ASSERT_ONCE(res == eOk);
        }
      }
      else
      {
        ODA_FAIL_ONCE();
        return false;
      }
      return true;
    }

    return false;
  }

  virtual bool getPropertyPossibleValues(const OdString& sPath,
                                         OdStringArray& values,
                                         OdStringArray&) // iconPaths
  {
    OdString sName = sPath, sCat = extractFirst(sName);
    if (sCat == L"General")
    {
      if (sName == L"Level")
        return getLevels(baseDatabase(), values);
      if (sName == L"Line Style")
      {
        values.push_back(L"ByLevel"); values.push_back(L"ByCell");
        return getLineStyleNames(baseDatabase(), values);
      }
      if (sName == L"Weight")
        return getLineWeightsNames(baseDatabase(), values);
    }
    return false;
  }
};

///////////////////////////////////////////////////////////////////////////////

void registerOdDgXmlPropSources(OdPropServices* pPropSrv)
{
  ODA_ASSERT_ONCE(pPropSrv);
  if (!pPropSrv)
    return;
  pPropSrv->registerXmlProp(OdDgGraphicsElement::desc()->name(), // L"OdDgGraphicsElement"
                            &OdRxObjectImpl<OdDgXmlPropGraphicsElement, OdPropBase>::createObject);
}

void unregisterOdDgXmlPropSources(OdPropServices* pPropSrv)
{
  ODA_ASSERT_ONCE(pPropSrv);
  if (!pPropSrv)
    return;
  pPropSrv->unregisterXmlProp(OdDgGraphicsElement::desc()->name()); // L"OdDgGraphicsElement"
}

///////////////////////////////////////////////////////////////////////////////
