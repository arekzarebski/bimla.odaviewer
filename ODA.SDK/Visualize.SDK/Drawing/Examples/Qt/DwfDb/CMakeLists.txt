#
#  TD_DwfDb library for ODA File Viewer
#
set(td_dwfdb_tv_sources
  ${TKERNEL_ROOT}/Examples/Common/ExtDbModule.h
  ${TDRAWING_ROOT}/Examples/Qt/Common/ExtDbModuleBaseImpl.h
  ${TDRAWING_ROOT}/Examples/Qt/DwfDb/DwfDbModuleImpl.h
  ${TDRAWING_ROOT}/Examples/Qt/DwfDb/DwfDbCommands.h
  ${TDRAWING_ROOT}/Extensions/ExServices/ExDbCommandContext.h
  ${TKERNEL_ROOT}/Extensions/ExServices/ExKWIndex.cpp
  ${TKERNEL_ROOT}/Extensions/ExServices/OdFileBuf.cpp
  ${TDRAWING_ROOT}/Extensions/ExServices/ExDbCommandContext.cpp
  DwfDbModule.cpp
  DwfDbCommands.cpp
)

if(EXISTS ${TKERNEL_ROOT}/Include/SysVarPE.h)
  set(td_dwfdb_tv_sources
      ${TKERNEL_ROOT}/Include/SysVarPE.h
      ${td_dwfdb_tv_sources}
  )
else(EXISTS ${TKERNEL_ROOT}/Include/SysVarPE.h)
  set(td_dwfdb_tv_sources
      ${TDRAWING_ROOT}/Examples/Common/SysVarPE.h
      ${td_dwfdb_tv_sources}
  )
  add_definitions(-DODA_SYS_VAR_PE_UNLINKED_RXINIT)
endif(EXISTS ${TKERNEL_ROOT}/Include/SysVarPE.h)

tcomponents_sources(${TD_DWFDB_LIB}
                    ${td_dwfdb_tv_sources}
                    )
include_directories(
                    ${TCOMPONENTS_ROOT}/Dwfdb/Include
                    ${TDRAWING_ROOT}/Examples/Qt/Common
                    ${TKERNEL_ROOT}/Examples/Common
                    ${TDRAWING_ROOT}/Examples/Common
                    ${TKERNEL_ROOT}/Include
                    ${TDRAWING_ROOT}/Include
                    ${TKERNEL_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Imports/DwfImport/Include
                    ${TKERNEL_ROOT}/Exports/DwfExport/Include
                    ${TKERNEL_ROOT}
)

if(ODA_SHARED AND MSVC)
tcomponents_sources(${TD_DWFDB_LIB} data/DwfDb.rc)
endif(ODA_SHARED AND MSVC)

# reason to link with ${TD_DB_LIB}: contains import/export code

tcomponents_tx(${TD_DWFDB_LIB} 
      ${TDWF_DB_LIB} ${TD_DB_LIB} ${TD_GE_LIB} ${TD_GI_LIB} ${TD_GS_LIB}
      ${TD_DBROOT_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB}
)

tcomponents_project_group(${TD_DWFDB_LIB} "Dwfdb/Examples")
