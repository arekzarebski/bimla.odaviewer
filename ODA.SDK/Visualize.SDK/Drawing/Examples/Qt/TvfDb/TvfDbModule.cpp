/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
//
// TvfDbModule.cpp
//

#include "OdaCommon.h"

#include "ExtDbModuleBaseImpl.h"
#include "DbHostAppServices.h"
#include "ExDynamicModule.h" // #include "RxDynamicModule.h" 
//#include "ColorMapping.h"
//#include "DbDictionary.h"
//#include "DbBlockTableRecord.h"
//#include "AbstractViewPE.h"

#include "TvFactory.h" //"TvDatabase.h"
#include "TvError.h"

#include "TvfDatabase.h"
#include "TvGiContextForDatabase.h" // Visualize/Source/TvGiContextForDatabase.h
#include "TvGsDeviceImpl.h" // Visualize/Source/TvGsDeviceImpl.h

//////////////////////////////////////////////////////////////////////////

//appTvfServicesImpl()->m_pBaseHostAppServices = pServices;

//static OdTvfDbServicesImpl* appTvfServicesImpl()
//{
//  static OdStaticRxObject<OdTvfDbServicesImpl> g_services;
//  return &g_services;
//}

static OdDbHostAppServices* s_pServices = NULL;
OdDbHostAppServices* appTvfServices()
{
  ODA_ASSERT_ONCE(s_pServices);
  return s_pServices;
}

//////////////////////////////////////////////////////////////////////////

class OdTvfDbModuleImpl : public OdExtDbModuleBaseImpl
{
  OdTvFactoryId m_idTvFactory; // TV factory Id
  // Qt dependent //OdTvViewerDeviceOptions m_deviceOptions; //global device properties
public:
  OdTvfDbModuleImpl();
  virtual ~OdTvfDbModuleImpl();

  //// OdRxModule overridden
  virtual void initApp();
  virtual void uninitApp();
  virtual bool isValid(); // to unload if using components are missing (false)

  //// init issues
  virtual void setBaseHostAppServices(OdDbBaseHostAppServices* pServices);

  //// load / save issues
  //virtual OdRxObjectPtr readFile(const OdString& fileName, 
  //                               Oda::FileShareMode shareMode = Oda::kShareDenyWrite);
  virtual OdRxObjectPtr createUninitializedDatabase();
  virtual bool loadToUninitializedDatabase(OdRxObjectPtr pRxDb, 
                                           const OdString& fileName, 
                                           Oda::FileShareMode shareMode = Oda::kShareDenyWrite);
  virtual OdRxObject* getWrappedDatabase(OdRxObject* pRxDb); // for TD_TvfDb (pRxDb is a wrapper)
  virtual OdString getFilename(OdRxObject* pRxDb);

  // render issues
  // for all models (return false to use standard way (via OdDbBaseDatabasePE & OdDbBaseLayoutPE))
  virtual bool getLayoutViewNamesAndIds(OdRxObject* pRxDb,
                                        OdStringArray& namesLayoutView,
                                        OdDbStubPtrArray& idsLayoutView,
                                        OdString* pActiveLayoutViewName = NULL,
                                        bool bOneForModel = true,
                                        OdGsDCRectArray* pRects = NULL);
  virtual OdGiDefaultContextPtr createGiContext(OdRxObject* pRxDb);
  virtual OdGsDevicePtr setupActiveLayoutViews(OdRxObject* pRxDb, OdGsDevice* pDevice, OdGiDefaultContext* pGiCtx,
                                               void* winId = NULL, long widthQWidget = 800, long heightQWidget = 600);
  //virtual bool argsAtSetupLayoutViews(OdRxObject* pRxDatabase, bool& bDoZoomExtents, OdGsView::RenderMode& rm);

  // get point, zoom etc issues
  virtual OdGsView* getActiveGsView(OdGsDevice* pGsDevice);
  //virtual OdGsModel* getGsModel(OdGsDevice* pGsDevice);
  //virtual bool getUcsPlane(OdRxObject* pRxDatabase, OdGsView* pGsView, OdGePlane& plane,
  //                         const OdGeVector3d* pvHintDirToIntersect = NULL);
  //virtual bool setGsViewToDb(OdRxObject* pRxDatabase, OdGsView* pGsView);

  //virtual OdUnitsFormatter* getFormatter(OdRxObject* pRxDatabase);

  // filters issues
  virtual bool isResponsible(OdRxObject* pRxDatabase);
  virtual OdStringArray getExtensions(enum OdEd::GetFilePathFlags flg = OdEd::kGfpForOpen, 
                                      OdRxObject* pRxDatabase = NULL,
                                      bool bWithExtSynonyms = false);
  virtual OdString getFilterByExtension(const OdString& sExt, 
                                        int index = 0,
                                        enum OdEd::GetFilePathFlags flg = OdEd::kGfpForOpen,
                                        OdStringArray* pExtSynonyms = NULL);

  /////// Commands ///////
  //#define ODRX_CMD_ENTRY(cmdName, name, impl) OdStaticRxObject<OdqCmd##name> m_cmd##name;
  //#include "TvfDbCommands.h"
};

ODRX_DEFINE_DYNAMIC_MODULE(OdTvfDbModuleImpl);

OdTvfDbModuleImpl::OdTvfDbModuleImpl()
{
  //#define ODRX_CMD_ENTRY(cmdName, name, impl) m_cmd##name.m_pModule = this;
  //#include "TvfDbCommands.h"
}

OdTvfDbModuleImpl::~OdTvfDbModuleImpl()
{
}

void OdTvfDbModuleImpl::initApp()
{
  odTvInitialize(); // initialize the Visualize SDK
  m_idTvFactory = odTvGetFactory(); 
  m_pDbModule = ::odrxDynamicLinker()->getModule(L"TV_Visualize.dll"); // for isValid() only 

  OdExtDbModuleBaseImpl::initApp();
  OdTvfDatabase::rxInit();
  //OdTvGiContextForDatabase::rxInit();
  //OdGsDeviceForTvImpl::rxInit();
  // ...::rxInit();
  // register commands
  //OdEdCommandStackPtr pCommands = odedRegCmds();
  //#define ODRX_CMD_ENTRY(cmdName, name, impl) pCommands->addCommand(&m_cmd##name);
  //#include "TvfDbCommands.h"
}

void OdTvfDbModuleImpl::uninitApp()
{
  // unregister commands
  //OdEdCommandStackPtr pCommands = odedRegCmds();
  //#define ODRX_CMD_ENTRY(cmdName, name, impl) pCommands->removeCmd(&m_cmd##name);
  //#include "TvfDbCommands.h"
  // ...::rxUninit();
  //OdGsDeviceForTvImpl::rxUninit();
  //OdTvGiContextForDatabase::rxUninit();
  OdTvfDatabase::rxUninit();
  OdExtDbModuleBaseImpl::uninitApp();

  m_pDbModule = NULL;
  m_idTvFactory.clearDatabases();
  odTvUninitialize(); // Uninitialize the Visualize SDK
}

bool OdTvfDbModuleImpl::isValid() // to unload if using components are missing (false)
{
  ODA_ASSERT_ONCE(m_pDbModule.get());
  return !m_pDbModule.isNull();
}

// init issues
void OdTvfDbModuleImpl::setBaseHostAppServices(OdDbBaseHostAppServices* pServices)
{
  ODA_ASSERT_ONCE(!s_pServices || !pServices);

  s_pServices = OdDbHostAppServices::cast(pServices).get();
}

//OdRxObjectPtr OdTvfDbModuleImpl::readFile(const OdString& fileName, 
//                                          Oda::FileShareMode shareMode) // = Oda::kShareDenyWrite
//{
//  OdRxObjectPtr pRxDb;
//  try
//  {
//    OdTvResult rc;
//    OdTvDatabaseId databaseId = m_idTvFactory.readFile(fileName.c_str(), &rc);
//    if (rc == tvOk)
//    {
//      OdTvDatabasePtr pTvDb = databaseId.openObject(OdTv::kForRead, &rc);
//      if (rc == tvOk && !pTvDb.isNull())
//      {
//        // unfortunately OdTvDatabase is not RTTI based class //pRxDb = OdRxObject::cast(pTvDb);
//        // therefore  we use OdTvfDatabase - local RTTI based wrapper of OdTvDatabase
//        OdTvfDatabasePtr pTvfDb =  OdRxObjectImpl<OdTvfDatabase>::createObject();
//        pTvfDb->m_pTvDb = pTvDb;
//        pRxDb = OdRxObject::cast(pTvfDb);
//        pTvfDb = NULL;
//        ODA_ASSERT_ONCE(pRxDb.get());
//      }
//    }
//  }
//  catch (OdTvError&) // e)
//  {
//    ODA_FAIL_ONCE();
//    //OdString descr = e.description();
//    //OdString sErr; sErr.format(L"Reading of file \"%ls\" was failed (%ls).", fileName.c_str(), descr.c_str());
//    // throw OdError(...)
//  }
//
//  return pRxDb;
//}

OdRxObjectPtr OdTvfDbModuleImpl::createUninitializedDatabase()
{
  // unfortunately OdTvDatabase is not RTTI based class (is not cated to OdRxObject and back)
  // therefore  we use OdTvfDatabase - local RTTI based wrapper of OdTvDatabase
  OdTvfDatabasePtr pTvfDb = OdRxObjectImpl<OdTvfDatabase>::createObject();
  return OdRxObject::cast(pTvfDb);
}

bool OdTvfDbModuleImpl::loadToUninitializedDatabase(OdRxObjectPtr pRxDb, 
                                                    const OdString& fileName, 
                                                    Oda::FileShareMode) // shareMode // = Oda::kShareDenyWrite
{
  if (!isResponsible(pRxDb))
    return false;
  try
  {
    OdTvResult rc;
    OdTvDatabaseId databaseId = m_idTvFactory.readFile(fileName.c_str(), &rc);
    if (rc == tvOk)
    {
      OdTvDatabasePtr pTvDb = databaseId.openObject(OdTv::kForRead, &rc);
      if (rc == tvOk && !pTvDb.isNull())
      {
        OdTvfDatabasePtr pTvfDb = OdTvfDatabase::cast(pRxDb);
        ODA_ASSERT_ONCE(pTvfDb.get() && pTvfDb->m_pTvDb.isNull() && pTvfDb->m_sFileName.isEmpty())
        pTvfDb->m_pTvDb = pTvDb;
        pTvfDb->m_sFileName = fileName;
        return true;
      }
    }
  }
  catch (OdTvError&) // e)
  {
    ODA_FAIL_ONCE();
    //OdString descr = e.description();
    //OdString sErr; sErr.format(L"Reading of file \"%ls\" was failed (%ls).", fileName.c_str(), descr.c_str());
    // throw OdError(...)
  }

  return false;
}

OdRxObject* OdTvfDbModuleImpl::getWrappedDatabase(OdRxObject* pRxDb)
{
  OdTvfDatabasePtr pTvfDb = OdTvfDatabase::cast(pRxDb);
  if (pTvfDb.isNull() || pTvfDb->m_pTvDb.isNull())
    return pRxDb;
  OdTvResult rc;
  OdDbBaseDatabase* pRxDbInt = static_cast<OdDbBaseDatabase*>(pTvfDb->m_pTvDb->getInternalDatabase(&rc));
  if (!pRxDbInt || rc != tvOk)
    return pRxDb;
  return pRxDbInt;
}

OdString OdTvfDbModuleImpl::getFilename(OdRxObject* pRxDb)
{
  OdTvfDatabasePtr pTvfDb = OdTvfDatabase::cast(pRxDb);
  if (pTvfDb.isNull() || pTvfDb->m_pTvDb.isNull())
    return OdExtDbModuleBaseImpl::getFilename(pRxDb);
  OdTvResult rc;
  OdString sFileName = pTvfDb->m_pTvDb->getFilename(&rc);
  if (rc != tvOk || sFileName.isEmpty())
    sFileName = pTvfDb->m_sFileName;
  return sFileName;
}

bool OdTvfDbModuleImpl::getLayoutViewNamesAndIds(OdRxObject* pRxDb,
                                                 OdStringArray& namesLayoutView,
                                                 OdDbStubPtrArray&, //idsLayoutView
                                                 OdString* pActiveLayoutViewName, // = NULL
                                                 bool, //bOneForModel // = true
                                                 OdGsDCRectArray*)// pRects // = NULL
{
  OdTvfDatabasePtr pTvfDb = OdTvfDatabase::cast(pRxDb);
  if (pTvfDb.isNull() || pTvfDb->m_pTvDb.isNull())
    return false;
  OdTvResult rc;
  OdTvModelsIteratorPtr pItr = pTvfDb->m_pTvDb->getModelsIterator(&rc);
  for (; !pItr->done(); pItr->step())
  {
    OdTvModelId id = pItr->getModel(&rc);
    if (rc != tvOk || id.isNull())
      continue;

    OdTvModelPtr pModel = id.openObject(OdTv::kForRead, &rc);
    if (rc != tvOk || pModel.isNull())
      continue;
    OdString sName = pModel->getName(&rc);
    if (rc != tvOk || sName.isEmpty() || sName == L"Draggers")
      continue;

    namesLayoutView.push_back(sName);
    if (pActiveLayoutViewName && pActiveLayoutViewName->isEmpty())
      *pActiveLayoutViewName = sName;
  }

  if (namesLayoutView.isEmpty())
    namesLayoutView.push_back(L"Model");
  if (pActiveLayoutViewName  && pActiveLayoutViewName->isEmpty())
    *pActiveLayoutViewName = namesLayoutView.last();
  return true;
}

OdGiDefaultContextPtr OdTvfDbModuleImpl::createGiContext(OdRxObject* pRxDb)
{
  OdTvfDatabasePtr pTvfDb = OdTvfDatabase::cast(pRxDb);
  if (pTvfDb.isNull() || pTvfDb->m_pTvDb.isNull())
    return OdExtDbModuleBaseImpl::createGiContext(pRxDb);
  //OdTvDatabase* pTvDb = pTvfDb->m_pTvDb->getInternalDatabase(OdTvResult* rc = NULL) const = 0;
  OdDbBaseDatabase* pDb = static_cast<OdDbBaseDatabase*>(pTvfDb->m_pTvDb->getInternalDatabase());
  ODA_ASSERT_ONCE(pDb != pRxDb);

  //3. Create Gi context
  OdTvGiContextForDatabase* pTvGiCtx = new OdTvGiContextForDatabase();
  pTvGiCtx->setDatabase(dynamic_cast<OdDbDatabaseTv*>(pDb));
  pTvGiCtx->setTtfPolyDrawMode(false);
  //pTvGiCtx->enableGsModel(true);
  //pTvGiCtx->setLogicalPalette(m_logPalette.asArrayPtr(), m_logPalette.size());

  OdGiDefaultContextPtr pGiCtx = OdGiDefaultContext::cast(pTvGiCtx);
  ODA_ASSERT_ONCE(pGiCtx.get());
  return pGiCtx;
}

OdGsDevicePtr OdTvfDbModuleImpl::setupActiveLayoutViews(OdRxObject* pRxDb, 
                                                        OdGsDevice* pDevice, OdGiDefaultContext* pGiCtx,
                                                        void* winId, // = NULL
                                                        long widthQWidget, // = 800
                                                        long heightQWidget) // = 600
{
  OdTvfDatabasePtr pTvfDb = OdTvfDatabase::cast(pRxDb);
  OdTvGiContextForDatabase* pTvGiCtx = OdTvGiContextForDatabase::cast(pGiCtx).get();
  //if (pTvGiCtx && !pTvfDb.isNull() && !pTvfDb->m_pTvDb.isNull())
  //{
  //  OdTvDatabase* pTvDb = static_cast<OdTvDatabase*>(pTvfDb->m_pTvDb->getInternalDatabase());

  //  //OdTvViewerRenderArea::setupDevice
  //  OdTvDCRect rect(0, widthQWidget, heightQWidget, 0);
  //  OdTvResult rc;

  //  OdString strDeviceName = L"Window_NN"; // tODO
  //  OdTvGsDeviceId m_TvDeviceId = pTvDb->createDevice(strDeviceName.c_str(), (OSWindowHandle) winId, rect, OdTvGsDevice::kOpenGLES2, NULL, &rc);
  //  ODA_ASSERT_ONCE(rc == tvOk);

  //  //OdGsDeviceForTvImplPtr pLayoutHelper = OdGsDeviceForTvImpl::createObject(pGsDevice);
  //  //ODA_ASSERT_ONCE(pLayoutHelper.get());
  //  //ODA_FAIL_ONCE(); // TODO for setUnderlyingTvDevice - alternative code does not work

  //  //5. Create the tv implementation of the GS device (generally speaking it is a wrapper over GsDevice)
  //  //OdGsDeviceForTvImplPtr pDeviceWrapper = OdGsDeviceForTvImpl::createObject(pDevice);
  //  //pDeviceWrapper->rememberUserGiContext(pGiCtx, true);

  //  // see OdTvGsDeviceImpl::setupGs
  //  //9. Remember gs device wrapper in DB
  //  //setUnderlyingTvDevice(pDeviceWrapper.get());                     // !!!

  //  //return pDeviceWrapper;
  //}
  return OdExtDbModuleBaseImpl::setupActiveLayoutViews(pRxDb, pDevice, pGiCtx);
}

//bool OdTvfDbModuleImpl::argsAtSetupLayoutViews(OdRxObject* pRxDatabase, bool& bDoZoomExtents, OdGsView::RenderMode& rm)
//{
//  OdExtDbModuleBaseImpl::argsAtSetupLayoutViews(pRxDatabase, bDoZoomExtents, rm); // set for false case
//  //bDoZoomExtents = true;
//  ////rm = OdGsView::kGouraudShaded;
//  return true;
//}

OdGsView* OdTvfDbModuleImpl::getActiveGsView(OdGsDevice* pGsDevice)
{
  OdGsDeviceForTvImplPtr pLayoutHelper = OdGsDeviceForTvImpl::createObject(pGsDevice);
  ODA_ASSERT_ONCE(pLayoutHelper.get());
  if (pLayoutHelper.isNull())
    return NULL;
  OdGsView* pGsView = pLayoutHelper->activeView().get();
  ////ODA_ASSERT_ONCE(pGsView);
  return pGsView;
}

bool OdTvfDbModuleImpl::isResponsible(OdRxObject* pRxDatabase)
{
  ODA_ASSERT_ONCE(pRxDatabase); // test
  OdTvfDatabasePtr pTvfDb = OdTvfDatabase::cast(pRxDatabase);
  return !pTvfDb.isNull();
}

OdStringArray OdTvfDbModuleImpl::getExtensions(enum OdEd::GetFilePathFlags flg, // = OdEd::kGfpForOpen
                                               OdRxObject* pRxDatabase, // = NULL
                                               bool) // bWithExtSynonyms) // = false
{
  OdStringArray lst;
  if (flg == OdEd::kGfpForOpen && (!pRxDatabase || isResponsible(pRxDatabase)))
    lst.push_back(L"tvf");
  return lst;
}

OdString OdTvfDbModuleImpl::getFilterByExtension(const OdString& sExt, 
                                                 int index, // = 0
                                                 enum OdEd::GetFilePathFlags flg, // = OdEd::kGfpForOpen
                                                 OdStringArray*) // pExtSynonyms) // = NULL
{
  if (flg != OdEd::kGfpForOpen || index)
    return OdString::kEmpty;
  if (sExt == L"tvf")
    return L"Teigha Visualize Format (*.tvf)|*.tvf|";
  return OdString::kEmpty;
}
