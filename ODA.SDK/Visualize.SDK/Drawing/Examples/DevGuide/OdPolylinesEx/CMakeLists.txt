#
#  OdPolylinesEx executable
#

tdrawing_sources(OdPolylinesEx OdPolylinesEx.cpp)

include_directories(
	${TKERNEL_ROOT}/Extensions/ExServices
    ${TDRAWING_ROOT}/Extensions/ExServices
	${TKERNEL_ROOT}/Examples/Common
)
					
if(ODA_SHARED)
	set ( od_write_libs  ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_KEY_LIB} ${TD_DB_LIB}  ${TD_ROOT_LIB} ${TD_GE_LIB} ${TD_DBROOT_LIB})
else(ODA_SHARED)
	set ( od_write_libs ${TD_FIELDEVAL_LIB} ${TD_RDIMBLK_LIB} ${TD_RASTERPROC_LIB} ${TD_TXV_BITMAP_LIB}
                    ${TD_MODELER_LIB} ${TCOMPONENTS_BREPMODELER_LIB} ${TD_BREPBUILDERFILLER_LIB}
                    ${TD_BREPRENDERER_LIB} ${TD_BR_LIB} ${TD_BREPBUILDER_LIB}
                    ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_RASTER_LIB} ${TD_ACISBLDR_LIB} ${TD_DB_POINTCLOUDOBJ_LIB}
                    ${TD_DB_LIB} ${TD_GS_LIB} ${TD_GI_LIB} ${TD_DBROOT_LIB} ${TD_GE_LIB} ${TD_SPATIALINDEX_LIB} ${TD_ROOT_LIB}
                    ${TH_THIRDPARTYRASTER_LIB} ${TH_WCHAR_LIB} ${TH_CONDITIONAL_LIBCRYPTO} ${TH_UTF_LIB})
	if (NOT WINTTF)
		set ( od_write_libs ${od_write_libs}
                    ${TH_FT_LIB})
	endif (NOT WINTTF)
endif(ODA_SHARED)					

if(MSVC)
	tdrawing_sources(OdPolylinesEx OdPolylinesEx.rc)
endif(MSVC)

tdrawing_executable(OdPolylinesEx ${od_write_libs} ${TD_ALLOC_LIB} ) 

tdrawing_project_group(OdPolylinesEx "Examples/DevGuideExamples")