#
# ODAUWPApp - Universal Windows Platform application
#

set(SOURCE_FILES
  App.xaml.cpp
  MainPage.xaml.cpp
  ViewFile.xaml.cpp
  pch.cpp
  common/SampleConfiguration.cpp
  common/SuspensionManager.cpp
  )

set(HEADER_FILES
   App.xaml.h
   MainPage.xaml.h
   ViewFile.xaml.h
   pch.h
   WinRTAppService.h
   common/SampleConfiguration.h
   common/SuspensionManager.h
  )

set(XAML_FILES
   App.xaml
   MainPage.xaml
   ViewFile.xaml
   )
 
set(ASSET_FILES
     Assets/Wide310x150Logo.scale-200.png
     Assets/StoreLogo.png
     Assets/Square44x44Logo.targetsize-24_altform-unplated.png
     Assets/Square44x44Logo.scale-200.png
     Assets/Square150x150Logo.scale-200.png
     Assets/SplashScreen.scale-200.png
     Assets/odalogo29.png
     Assets/odalogo.png
     Assets/LockScreenLogo.scale-200.png
     )
 
set(CONTENT_FILES
     Package.appxmanifest
    )

set(RESOURCE_FILES
  ${CONTENT_FILES} ${ASSET_FILES}
  ODAUWPApp_TemporaryKey.pfx)

if(TA_ROOT)
  add_definitions(-DTA_MODULES_ENABLED)
  set(TA_LIBS ${TA_COMMONEX_LIB} ${TA_AECSCHEDULE_LIB} ${TA_AECARCHDACHBASE_LIB} ${TA_AECAREACALCULATIONBASE_LIB}
              ${TA_AECSTRUCTUREBASE_LIB} ${TA_AECARCHBASE_LIB} ${TA_AECSCHEDULEDATA_LIB} ${TA_AECBASE_LIB} 
              ${TA_AECGEOMETRY_LIB} ${FACET_MODELER_LIB}
              )
endif(TA_ROOT)

if(TC_ROOT)
  add_definitions(-DTC_MODULES_ENABLED)
  set(TC_LIBS ${TC_COMMONEX_LIB} ${TC_AECCUILAND_LIB} ${TC_AECCBUILDINGSITE_LIB} ${TC_AECCIVILBASE_LIB} ${TC_AECCSURVEY_LIB} 
              ${TC_AECCROADWAY_LIB} ${TC_AECCPLANPROD_LIB} ${TC_AECCHYDROLOGY_LIB} ${TC_AECCNETWORK_LIB} ${TC_AECCPOINTCLOUD_LIB} 
              ${TC_AECCLAND_LIB} ${TH_MINIZIP_MEM_LIB} ${TH_ZLIB_LIB} ${TC_AECCVBASE_LIB} ${TC_AECCPRESSUREPIPES_LIB} )
endif(TC_ROOT)
  
include_directories(
					${CMAKE_CURRENT_SOURCE_DIR}
					${TKERNEL_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Extensions/ExServices
					${TKERNEL_ROOT}/Examples/Common
					${TKERNEL_ROOT}/Include
					${TDRAWING_ROOT}/Examples/Common
					${TDRAWING_ROOT}/Include
					${TA_ROOT}/include
					${TC_ROOT}/include
          ${TKERNEL_ROOT}/Examples/WinWUP/OdaUWPApp/OdaUWPApp
          )
set_property(SOURCE ${CONTENT_FILES} PROPERTY VS_DEPLOYMENT_CONTENT 1)
set_property(SOURCE ${ASSET_FILES} PROPERTY VS_DEPLOYMENT_CONTENT 1)
set_property(SOURCE ${ASSET_FILES} PROPERTY VS_DEPLOYMENT_LOCATION "Assets")
 
set_property(SOURCE "App.xaml" PROPERTY VS_XAML_TYPE "ApplicationDefinition")

source_group("Source Files" FILES ${SOURCE_FILES})
source_group("Header Files" FILES ${HEADER_FILES})
source_group("Resource Files" FILES ${RESOURCE_FILES})
source_group("Xaml Files" FILES ${XAML_FILES})

tdrawing_sources( ODAUWPApp ${SOURCE_FILES} ${HEADER_FILES} ${RESOURCE_FILES} ${XAML_FILES} )

set ( ODAUWPApp_libs   ${TC_LIBS} ${TA_LIBS} ${TD_KEY_LIB} ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_MODELER_LIB}
                       ${TCOMPONENTS_BREPMODELER_LIB} ${TD_BREPBUILDERFILLER_LIB}
                       ${TD_FIELDEVAL_LIB} ${TD_RDIMBLK_LIB}
                       ${TD_DYNBLOCKS_LIB} ${TD_RDIMBLK_LIB}
                       ${TD_BREPRENDERER_LIB} ${TD_BR_LIB} ${TD_TXV_BITMAP_LIB}
                       ${TD_ACISBLDR_LIB} ${TD_RASTERPROC_LIB} ${TD_RASTER_LIB} 
                       ${TD_DB_LIB} ${TD_DBROOT_LIB} ${TD_GS_LIB} ${TD_GI_LIB} ${TD_SPATIALINDEX_LIB} ${TD_BREPBUILDER_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} 
                       ${TH_THIRDPARTYRASTER_LIB} ${TH_FT_LIB} ${TH_ZLIB_LIB} ${TD_ALLOC_LIB} ${TH_CONDITIONAL_LIBCRYPTO}
)

tdrawing_executable(ODAUWPApp ${ODAUWPApp_libs})

set_target_properties (ODAUWPApp PROPERTIES COMPILE_FLAGS_REMOVE "/Zc:wchar_t- /Gy-" )
set_target_properties (ODAUWPApp PROPERTIES COMPILE_FLAGS "/sdl" )
set_target_properties (ODAUWPApp PROPERTIES WIN32_EXECUTABLE 1)
set_target_properties (ODAUWPApp PROPERTIES VS_WINDOWS_TARGET_PLATFORM_MIN_VERSION "10.0.10240.0")

set_property(TARGET ODAUWPApp PROPERTY VS_WINRT_COMPONENT TRUE)

tdrawing_project_group(ODAUWPApp "Examples")
