/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _IFC_EXPORT_H_
#define _IFC_EXPORT_H_

#include "OdPlatformSettings.h"
#include "OdModuleNames.h"
#include "RxModule.h"
#include "RxDictionary.h"
#include "DynamicLinker.h"
#include "DbObjectId.h"

#ifdef IFC2DWG_EXPORTS
  #define IFC2DWG_EXPORT          OD_TOOLKIT_EXPORT
  #define IFC2DWG_EXPORT_STATIC   OD_STATIC_EXPORT
#else                               
  #define IFC2DWG_EXPORT          OD_TOOLKIT_IMPORT
  #define IFC2DWG_EXPORT_STATIC   OD_STATIC_IMPORT
#endif

#define ODRX_DECLARE_IFC2DWG_STATIC_MODULES_ENTRY_POINTS() \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdSDAIModuleImpl); \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdIfcCoreModule); \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdIfc2x3Module); \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdIfc4Module); \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdIfc4x2Module); \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdIfcGeomModuleImpl); \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdIfcBrepBuilderModule); \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdIfcFacetModelerModule); \
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(OdIfc2DwgModuleImpl);
// end of ODRX_DECLARE_IFC2DWG_STATIC_MODULES_ENTRY_POINTS macro

#define ODRX_DEFINE_IFC2DWG_STATIC_APPMODULES() \
  ODRX_DEFINE_STATIC_APPMODULE(OdSDAIModuleName, OdSDAIModuleImpl) \
  ODRX_DEFINE_STATIC_APPMODULE(OdIfcCoreModuleName, OdIfcCoreModule) \
  ODRX_DEFINE_STATIC_APPMODULE(OdIfc2x3ModuleName, OdIfc2x3Module) \
  ODRX_DEFINE_STATIC_APPMODULE(OdIfc4ModuleName, OdIfc4Module) \
  ODRX_DEFINE_STATIC_APPMODULE(OdIfc4x2ModuleName, OdIfc4x2Module) \
  ODRX_DEFINE_STATIC_APPMODULE(OdIfcGeomModuleName, OdIfcGeomModuleImpl) \
  ODRX_DEFINE_STATIC_APPMODULE(OdIfcBrepBuilderModuleName, OdIfcBrepBuilderModule) \
  ODRX_DEFINE_STATIC_APPMODULE(OdIfcFacetModelerModuleName, OdIfcFacetModelerModule) \
  ODRX_DEFINE_STATIC_APPMODULE(OdIfc2DwgModuleName, OdIfc2DwgModuleImpl)
// end of ODRX_DEFINE_IFC2DWG_STATIC_APPMODULES macro

class OdDbDatabase;

class OdIfcFile;
typedef OdSmartPtr<OdIfcFile> OdIfcFilePtr;

namespace TD_IFC_EXPORT {

  enum ExportMode
  {
    kAsPolyFaceMesh,
    kAsSubDMesh
  };

  /** \details
    This class implements the IFC connection map.
  */
  class IFC2DWG_EXPORT OdIfcConnectionMap : public OdRxObject
  {
  public:

    /** \details
      Returns a handle of corresponding Ifc entity into the loaded model by
      the object id of the imported OdDbDatabase entity.
    */
    virtual OdDbHandle find(OdDbObjectId id) = 0;

    /**
      Returns the pointer to attached OdIfcFile.
    */
    virtual OdIfcFilePtr getIfcFile() = 0;

  };
  typedef OdSmartPtr<OdIfcConnectionMap> OdIfcConnectionMapPtr;

  /** \details
    This class implements the IFC importer.
  */
  class OdIfcExport : public OdRxObject
  {
  public:
    enum ExportResult { success, fail, bad_password, bad_file, schema_not_supported, bad_database, file_internal_error};
  
    /** \details
      Process Ifc file import into Dwg according to defined properties.
    */
    virtual ExportResult exportIfc() = 0;

    /** \details
      Passes parameters for Ifc importing.
    
      \remarks
  
      See Developer's Guide, IFC Input Parameters for the list of possible parameters.
    */
    virtual OdRxDictionaryPtr properties() = 0;
  };
  typedef OdSmartPtr<OdIfcExport> OdIfcExportPtr;

  /** \details
    This class implements the Ifc to Dwg export module.
  */
  class OdIfc2DwgModule : public OdRxModule
  {
  public:
    virtual OdIfcExportPtr create () = 0;
  };
  typedef OdSmartPtr<OdIfc2DwgModule> OdIfc2DwgModulePtr;

  inline OdIfcExportPtr createIfcExporter()
  {
    OdIfc2DwgModulePtr pModule = ::odrxDynamicLinker()->loadApp(OdIfc2DwgModuleName);
    if (!pModule.isNull())
      return pModule->create();
    return OdIfcExportPtr();
  }

}

#endif // _IFC_EXPORT_H_
