
#
#  ExDgnCloning library
#
if(MSVC)

tg_sources(${TG_EX_CLONING_LIB}
    ExDgnCloningModule.cpp
    ExDgnCreateCommands.cpp
    ExDgnCloneCommands.cpp
    ExDgnExplodeCommands.cpp
    ExDgnObject.cpp
    ExDgnEntity.cpp
    ExDgnCloningExport.h
    ExDgnObject.h
    ExDgnCmdDefs.h
    ExDgnEntity.h
    ExDgnCloningModule.h
	)

include_directories(${TG_CORE_INCLUDE}
					../../source/Common
					../../source/FileIO
					../../source/Design
					../../source/Elements
					${TG_ROOT}/Extensions/ExServices
					${TKERNEL_ROOT}/Extensions/ExServices)

add_definitions(-DEXDGNCLONING_DLL_EXPORTS)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)
				
if(ODA_SHARED AND MSVC)					
tg_sources(${TG_EX_CLONING_LIB}
    ExDgnCloning.rc
				)
endif(ODA_SHARED AND MSVC)

tg_tx(${TG_EX_CLONING_LIB} 
 ${TG_EXLIB} ${TD_EXLIB} ${TG_DGN7IO_LIB} ${TG_PS_TOOLKIT_LIB} ${TG_MODELERGEOMETRY_LIB} ${TG_DB_LIB} ${TH_TINYXML_LIB} ${TH_ZLIB_LIB} 
 ${TH_OLESS_LIB} ${TD_TXV_BITMAP_LIB} ${TD_DB_LIB} ${TD_GS_LIB} ${TD_GI_LIB} ${TD_DBROOT_LIB} ${TD_GE_LIB} ${TD_SPATIALINDEX_LIB} 
 ${TD_ROOT_LIB} ${TD_ALLOC_LIB} ${TH_THIRDPARTYRASTER_LIB} ${TH_FT_LIB} ) 

tg_project_group(${TG_EX_CLONING_LIB} "Examples")

endif(MSVC)