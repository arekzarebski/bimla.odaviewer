#
#  ExDgnElementIntersect library
#

tg_sources(${TG_INTERSECT_LIB}
    StdAfx.cpp
    OdDgIntersectModule.cpp
    DgElementIntersectPEImpl.cpp
    GiDrawObjectsForIntersect.cpp
    StdAfx.h
    OdDgIntersectModule.h
    DgElementIntersectPEImpl.h
    GiDrawObjectsForIntersect.h
  )


include_directories(${TG_CORE_INCLUDE}
					../../source/Common
					../../source/FileIO
					../../source/Design
					../../source/Elements
					${CMAKE_CURRENT_SOURCE_DIR}
					${TKERNEL_ROOT}/Extensions/ExServices
					${TG_ROOT}/Extensions/ExServices)

if(ODA_SHARED AND MSVC)
tg_sources( ${TG_INTERSECT_LIB}
        DgIntersect.rc
        )
endif(ODA_SHARED AND MSVC)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

tg_tx(${TG_INTERSECT_LIB} ${TG_DB_LIB} ${TD_GI_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB} )				

tg_project_group(${TG_INTERSECT_LIB} "Examples")