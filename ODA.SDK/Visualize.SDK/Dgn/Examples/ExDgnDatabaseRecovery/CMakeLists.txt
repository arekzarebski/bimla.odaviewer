#
#  ExDgnDatabaseRecovery library
#

tg_sources(${TG_DATABASERECOVERY_LIB}
    OdDgDatabaseRecoveryModule.cpp
    DgdatabaseRecoveryPEImpl.cpp
    StdAfx.h
    OdDgDatabaseRecoveryModule.h
    DgDatabaseRecoveryPEImpl.h
  )


include_directories(${TG_CORE_INCLUDE}
		    ${TD_CORE_INCLUDE}
		    ${CMAKE_CURRENT_SOURCE_DIR}
		    ${TG_ROOT}/Extensions/ExServices)

if(ODA_SHARED AND MSVC)
tg_sources( ${TG_DATABASERECOVERY_LIB}
        DgDatabaseRecoveryPE.rc
        )
endif(ODA_SHARED AND MSVC)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

tg_tx(${TG_DATABASERECOVERY_LIB} ${TG_DB_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB} )				

tg_project_group(${TG_DATABASERECOVERY_LIB} "Examples")
