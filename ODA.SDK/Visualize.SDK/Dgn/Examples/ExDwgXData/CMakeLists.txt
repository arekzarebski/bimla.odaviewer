#
#  ExDwgXData executable
#

tg_sources(ExDwgXData
  ExDwgXData.cpp
	)

include_directories(${TG_CORE_INCLUDE}
					../../source/Common
					../../source/FileIO
					../../source/Design
					../../source/Elements
					${CMAKE_CURRENT_SOURCE_DIR}
					${TKERNEL_ROOT}/Extensions/ExServices
					${TDRAWING_ROOT}/Extensions/ExServices
					${TG_ROOT}/Extensions/ExServices)
					
if(ODA_SHARED)
set ( ExDwgXData_libs  ${TG_EXLIB} ${TD_EXLIB} ${TD_DR_EXLIB} ${TG_DB_LIB} ${TD_KEY_LIB} ${TD_DB_LIB}  ${TD_DBROOT_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} 
)
else(ODA_SHARED)
set ( ExDwgXData_libs  ${TG_DWGDB_LIB}  ${TG_EXLIB} ${TD_DR_EXLIB} ${TD_EXLIB} ${TG_DGN7IO_LIB} ${TG_PS_TOOLKIT_LIB} ${TG_MODELERGEOMETRY_LIB} ${TG_DB_LIB} ${TH_TINYXML_LIB} ${TH_ZLIB_LIB} ${TH_OLESS_LIB}
                      ${TD_DGN_IMPORT_LIB} ${TD_BREPBUILDERFILLER_LIB} ${TD_BR_LIB} ${TD_BREPBUILDER_LIB} ${TD_DGNLS_LIB} 
                      ${TD_DB_LIB} ${TD_GS_LIB} ${TD_GI_LIB} ${TD_DBROOT_LIB} ${TD_GE_LIB} ${TD_SPATIALINDEX_LIB} ${TD_ROOT_LIB}
                      ${TH_THIRDPARTYRASTER_LIB} ${TH_FT_LIB} ${TH_CONDITIONAL_LIBCRYPTO}
)
endif(ODA_SHARED)					

if(MSVC)
tg_sources(ExDwgXData ExDwgXData.rc)
endif(MSVC)

tg_executable(ExDwgXData ${ExDwgXData_libs} ${TD_ALLOC_LIB} ) 

tg_project_group(ExDwgXData "Examples")
