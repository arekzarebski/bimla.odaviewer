/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _OD_PDFIUM_FUNCTIONS_INCLUDED_
#define _OD_PDFIUM_FUNCTIONS_INCLUDED_

#include "public/fpdfview.h"
#include "oda_files/PdfImportBaseObjectGrabber.h"

typedef void* FPDF_ARRAY;
typedef void* FPDF_PageRenderContext;

typedef struct {
  int   nol;
  wchar_t  names[1024][512];
} FPDF_PageLayerInfo;

//////////////////////////////////////////////////////////////////////////
FPDF_EXPORT double FPDF_CALLCONV FPDF_GetMeasureDictInfo(FPDF_DOCUMENT document, int page_index);

FPDF_EXPORT bool FPDF_CALLCONV FPDF_LoadLayers(FPDF_DOCUMENT document, FPDF_ARRAY& pLayers, FPDF_ARRAY& pONLayers, FPDF_ARRAY& pOFFLayers);

FPDF_EXPORT bool FPDF_CALLCONV FPDF_GetPageLayers(FPDF_DOCUMENT document, FPDF_PAGE page, int page_index, FPDF_PageLayerInfo& info);

FPDF_EXPORT bool FPDF_CALLCONV FPDF_EnableLayer(FPDF_ARRAY pLayers, FPDF_ARRAY pONLayers, FPDF_ARRAY pOFFLayers, const wchar_t* pName, bool bEnable);

FPDF_EXPORT bool FPDF_CALLCONV FPDF_IsLayerEnabled(FPDF_ARRAY pOFFLayers, const wchar_t* pName);

FPDF_EXPORT void FPDF_CALLCONV FPDF_SetPageRenderContext(FPDF_PAGE page, FPDF_PageRenderContext context);

FPDF_EXPORT void FPDF_CALLCONV FPDF_ImportPage(FPDF_PAGE page, OdPdfImportBaseObjectGrabber* grabber, bool import_TT_fonts, bool import_Type3_as_TT);

//////////////////////////////////////////////////////////////////////////

#endif // _OD_PDFIUM_FUNCTIONS_INCLUDED_
