/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODPDFDEVICEDRIVER_INCLUDED_
#define _ODPDFDEVICEDRIVER_INCLUDED_

#include "PdfImportBaseObjectGrabber.h"

#include <memory>
#include <vector>


#include "core/fxcrt/retain_ptr.h"
#include "core/fxge/cfx_pathdata.h"
#include "core/fxge/ifx_renderdevicedriver.h"


struct Type3FontInfo
{
  Type3FontInfo()
    :m_TextHeight(0.)
    ,m_TextWidth(0.) {}

  float m_TextWidth;
  float m_TextHeight;
};

class CPDF_TextObject;

class OdDeviceDriver : public IFX_RenderDeviceDriver
{
public:
  explicit OdDeviceDriver(OdPdfImportBaseObjectGrabber* grabber, const std::wstring& layer_name, bool& is_object_visible);
  ~OdDeviceDriver() override;

  // IFX_RenderDeviceDriver
  virtual int GetDeviceCaps(int caps_id) const;
  virtual void SaveState() {};
  virtual void RestoreState(bool bKeepSaved) {};
  virtual bool SetClip_PathFill(const CFX_PathData* pPathData, const CFX_Matrix* pObject2Device, int fill_mode);
  virtual bool SetClip_PathStroke(const CFX_PathData* pPathData, const CFX_Matrix* pObject2Device, const CFX_GraphStateData* pGraphState);
  virtual bool GetClipBox(FX_RECT* pRect);
  virtual bool StretchDIBits(const RetainPtr<CFX_DIBSource>& pBitmap, uint32_t color, int dest_left, int dest_top, int dest_width,
    int dest_height, const FX_RECT* pClipRect, uint32_t flags, int blend_type);
  virtual bool FillRectWithBlend(const FX_RECT* pRect, uint32_t fill_color, int blend_type);
  virtual bool DrawShading(const CPDF_ShadingPattern* pPattern, const CFX_Matrix* pMatrix, const FX_RECT& clip_rect,
    int alpha, bool bAlphaMode);
  virtual bool SetBitsWithMask(const RetainPtr<CFX_DIBSource>& pBitmap, const RetainPtr<CFX_DIBSource>& pMask,
    int left, int top, int bitmap_alpha, int blend_type);
  virtual int GetDriverType() const;

  bool DrawPath(const CFX_PathData* pPathData, const CFX_Matrix* pObject2Device, const CFX_GraphStateData* pGraphState,
    uint32_t fill_color, uint32_t stroke_color, int fill_mode, int blend_type) override;
  bool SetDIBits(const RetainPtr<CFX_DIBSource>& pBitmap, uint32_t color, const FX_RECT* pSrcRect, int left, int top, int blend_type) override;
  bool StartDIBits(const RetainPtr<CFX_DIBSource>& pBitmap, int bitmap_alpha, uint32_t color, const CFX_Matrix* pMatrix, uint32_t render_flags,
    std::unique_ptr<CFX_ImageRenderer>* handle, int blend_type) override;
  bool DrawDeviceText(int nChars, const FXTEXT_CHARPOS* pCharPos, CFX_Font* pFont, const CFX_Matrix* pObject2Device, float font_size, uint32_t color) override;
  bool DrawCosmeticLine(const CFX_PointF& ptMoveTo, const CFX_PointF& ptLineTo, uint32_t color, int blend_type);
  bool SetPixel(int x, int y, uint32_t color) override;
  bool DrawShadingAsBitmap(const RetainPtr<CFX_DIBSource>& pBitmap, uint32_t color, const FX_RECT* pSrcRect, int left, int top, int blend_type);

  bool DrawDeviceTextEx(int nChars, const FXTEXT_CHARPOS* pCharPos, CFX_Font* pFont, const CFX_Matrix* pObject2Device, float font_size, uint32_t color,
    const bool isType3Font, const WideString& unicode_str, const Type3FontInfo& type3_info);

private:
  bool DrawDeviceTextImpl(int nChars, const FXTEXT_CHARPOS* pCharPos, CFX_Font* pFont, const CFX_Matrix* pObject2Device,
    float font_size, uint32_t color);
  void fillBmiHeader(RetainPtr<CFX_DIBSource>& pBitmap, BmiHeaderInfo& bmiHeader);


  const std::wstring&                  m_CurrentLayerName;
  bool&                                m_IsObjectVisible;
  OdPdfImportBaseObjectGrabber*        m_Grabber;
  static const double                  DPI;

  
};

#endif // _ODPDFDEVICEDRIVER_INCLUDED_
