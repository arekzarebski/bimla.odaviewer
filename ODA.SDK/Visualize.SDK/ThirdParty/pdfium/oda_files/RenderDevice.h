/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef ODRENDERDEVICE_H_
#define ODRENDERDEVICE_H_

#include "core/fxge/cfx_renderdevice.h"
#include "core/fpdfapi/page/cpdf_textobject.h"
#include "OdDeviceDriver.h"

//CORE-14446 CORE-14441 PdfImport for Mac
//CORE-14782
class RenderDevice : public CFX_RenderDevice
{
public:
  RenderDevice();

  bool GetZeroAreaPath(const CFX_PathData* Path, const CFX_Matrix* pMatrix,
    bool bAdjust,
    CFX_PathData* NewPath,
    bool* bThin,
    bool* setIdentity) const;

  virtual bool DrawPathWithBlend(const CFX_PathData* pPathData,
    const CFX_Matrix* pObject2Device,
    const CFX_GraphStateData* pGraphState,
    uint32_t fill_color,
    uint32_t stroke_color,
    int fill_mode,
    int blend_type);

  bool DrawNormalTextEx(int nChars,
    const FXTEXT_CHARPOS* pCharPos,
    CFX_Font* pFont,
    float font_size,
    const CFX_Matrix* pText2Device,
    uint32_t fill_color,
    uint32_t text_flags,
    const bool isType3Font,
    const WideString& unicode_str,
    const Type3FontInfo& type3_info);

protected:
  virtual bool ShouldDrawDeviceText(const CFX_Font* pFont, uint32_t text_flags);
};

#endif // ODRENDERDEVICE_H_
