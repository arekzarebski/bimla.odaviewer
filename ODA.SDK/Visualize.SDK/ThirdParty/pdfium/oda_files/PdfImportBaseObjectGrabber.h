/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _PDFIMPORTBASEOBJECTGRABBER_INCLUDED_
#define _PDFIMPORTBASEOBJECTGRABBER_INCLUDED_

#include "public/fpdfview.h"

struct FPDF_EXPORT  OdPdfImportPoint
{
  OdPdfImportPoint();
  OdPdfImportPoint(double x_in, double y_in);

  double x;
  double y;
};

enum FPDF_EXPORT OdPdfImportPtType { OdLineTo, OdBezierTo, OdMoveTo };

struct FPDF_EXPORT OdPdfImportPathData
{
  OdPdfImportPathData();
  OdPdfImportPoint   m_Point;
  OdPdfImportPtType  m_Type;
  bool               m_CloseFigure;
};

struct FPDF_EXPORT OdPdfImportMatrix
{
  OdPdfImportMatrix();
  OdPdfImportMatrix(double a_in, double b_in, double c_in, double d_in, double e_in, double f_in);
  double a;
  double b;
  double c;
  double d;
  double e;
  double f;
};

struct FPDF_EXPORT OdPdfImportColor
{
  OdPdfImportColor(unsigned char r_in, unsigned char g_in, unsigned char b_in, unsigned char alfa_in);
  unsigned char r;
  unsigned char g;
  unsigned char b;
  unsigned char alfa;
};

struct FPDF_EXPORT OdPdfImportRect
{
  OdPdfImportRect();
  OdPdfImportRect(double l, double t, double r, double b);

  double left;
  double top;
  double right;
  double bottom;
};

struct FPDF_EXPORT BmiHeaderInfo
{
  int width;
  int height;
  int pitch;
  int bpp;
  void * palette;
  int palette_size;
};


class FPDF_EXPORT OdPdfImportBaseObjectGrabber
{
public:
  OdPdfImportBaseObjectGrabber() {}
  virtual ~OdPdfImportBaseObjectGrabber() {};
  virtual bool needToAddGrathics(bool* ret) { return false; }
  virtual void addPath(const OdPdfImportPathData* path_data, const unsigned long path_point_count, const OdPdfImportColor* fill_color, 
    const OdPdfImportColor* stroke_color, const float* line_width, const bool is_object_visible, const wchar_t* layer_name) {}
  virtual void addCosmeticLine(const OdPdfImportPoint* point1, const OdPdfImportPoint* point2, const OdPdfImportColor* color, 
    const bool is_object_visible, const wchar_t* layer_name) {}
  virtual void addPixel(const OdPdfImportPoint* point, const OdPdfImportColor* color, const bool is_object_visible, 
    const wchar_t* layer_name) {}

  virtual bool needToAddImage(bool* ret) { return false;  }
  virtual void addImage(const BmiHeaderInfo* bmi, const unsigned char* imagedata, const OdPdfImportRect* rect,
    const OdPdfImportMatrix* object_matrix, const bool is_object_visible, const wchar_t* layer_name) {}

  virtual bool needToAddText(bool* ret) { return false; }
  virtual void addText(const char* font_face_name, const bool is_bold, const bool is_italic, const double font_height, const double text_width,
    const wchar_t* text, const OdPdfImportColor* color, const OdPdfImportMatrix* object_matrix, const bool is_object_visible, 
    const wchar_t* layer_name) {}

  virtual bool needToAddShading(bool* ret) { return false; }
  virtual void addShading(const BmiHeaderInfo* bmi, const unsigned char* imagedata, const OdPdfImportRect* rect,
    const OdPdfImportMatrix* object_matrix, const bool is_object_visible, const wchar_t* layer_name) {}

  virtual int getWidth() = 0;
  virtual int getHeight() = 0;
};


#endif // _PDFIMPORTBASEOBJECTGRABBER_INCLUDED_
