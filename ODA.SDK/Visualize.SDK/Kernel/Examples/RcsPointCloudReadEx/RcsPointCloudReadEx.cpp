/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#include "OdaCommon.h"
#include "StaticRxObject.h"
#include "ExSystemServices.h"
#include "DynamicLinker.h"
#include "diagnostics.h"
#include "RxDynamicModule.h"

#include "RcsFileServices/RxRcsFileServices.h"

#define STL_USING_IOSTREAM
#include "OdaSTL.h"
#define  STD(a)  std:: a

#ifdef OD_HAVE_CONSOLE_H_FILE
#include <console.h>
#endif
#include "ExPrintConsole.h"

#ifndef _TOOLKIT_IN_DLL_
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(RcsFileServicesModule);

  ODRX_BEGIN_STATIC_MODULE_MAP()
    ODRX_DEFINE_STATIC_APPMODULE(RX_RCS_FILE_SERVICES, RcsFileServicesModule)
  ODRX_END_STATIC_MODULE_MAP()
#endif

static const char cUsage[] =
{
  "RcsPointCloudReadEx sample program. Copyright (C) " TD_COPYRIGHT_START_YEAR_S  TD_COPYRIGHT_END_S "\n"
  "Usage: RcsPointCloudReadEx <inputFileType> <inputFilePath>\n"
  "  <inputFileType> can be:\n"
  "                     rcs\n"
  "                     rcp\n"
  "  <inputFilePath>  - full path to the input file\n"  
  "\nPress ENTER to continue...\n"
};

void dumpRcsFile(OdPointCloudScanDatabase* pScanDb);
void dumpRcpFile(OdPointCloudProjectDatabase* pProjectDb);

#if defined(OD_USE_WMAIN)
int wmain(int argc, wchar_t* argv[])
#else
int main(int argc, char* argv[])
#endif
{
#ifdef OD_HAVE_CCOMMAND_FUNC
  argc = ccommand(&argv);
#endif

  /**********************************************************************/
  /* Verify the argument count and display an error message as required */
  /**********************************************************************/
  if (argc < 3)
  {
    STD(cout) << cUsage;
    STD(cin).get();
    return 1;
  }

#ifndef _TOOLKIT_IN_DLL_
  ODRX_INIT_STATIC_MODULE_MAP();
#endif

  /**********************************************************************/
  /* Initialize Runtime Extension environment                           */
  /**********************************************************************/
  OdStaticRxObject<ExSystemServices> svcs;
  odrxInitialize(&svcs);

  OdString format(argv[1]), filePath(argv[2]);

  try
  {
    OdRxRcsFileServicesPtr pModule = odrxDynamicLinker()->loadModule("RcsFileServices");

    if (format.iCompare(OD_T("rcs")) == 0)
    {
      OdPointCloudScanDatabasePtr pScanDb = pModule->readRcsFile(filePath);
      dumpRcsFile(pScanDb.get());
    }
    else if (format.iCompare(OD_T("rcp")) == 0)
    {
      OdPointCloudProjectDatabasePtr pProjectDb = pModule->readRcpFile(filePath);
      dumpRcpFile(pProjectDb.get());
    }
    else
    {
      odPrintConsoleString(OD_T("\nUnsupported file format specified!\n"));
    }

    odrxDynamicLinker()->unloadModule("RcsFileServices");
  }
  catch (OdError e)
  {
    odPrintConsoleString(e.description());
  }
  /**********************************************************************/
  /* Uninitialize Runtime Extension environment                         */
  /**********************************************************************/
  ::odrxUninitialize();

	return 0;
}

void dumpRcsFile(OdPointCloudScanDatabase* pScanDb)
{
  OdString filePath = pScanDb->getScanDatabaseFilePath();
  odPrintConsoleString(OD_T("Dump .rcs file: %ls\n\n"), filePath.c_str());

  OdGeVector3d translation = pScanDb->getTranslation();
  odPrintConsoleString(OD_T("translation = (%lf, %lf, %lf)\n"), translation.x, translation.y, translation.z);
  OdGeVector3d rotation = pScanDb->getRotation();
  odPrintConsoleString(OD_T("rotation = (%lf, %lf, %lf)\n"), rotation.x, rotation.y, rotation.z);
  OdGeVector3d scale = pScanDb->getScale();
  odPrintConsoleString(OD_T("scale = (%lf, %lf, %lf)\n\n"), scale.x, scale.y, scale.z);

  OdGeExtents3d extents = pScanDb->getExtents();
  odPrintConsoleString(OD_T("extents.minPoint = (%lf, %lf, %lf)\n"), extents.minPoint().x, extents.minPoint().y, extents.minPoint().z);
  odPrintConsoleString(OD_T("extents.maxPoint = (%lf, %lf, %lf)\n\n"), extents.maxPoint().x, extents.maxPoint().y, extents.maxPoint().z);

  OdGeExtents3d transformedExtents = pScanDb->getTransformedExtents();
  odPrintConsoleString(OD_T("transformedExtents.minPoint = (%lf, %lf, %lf)\n"), transformedExtents.minPoint().x,
                       transformedExtents.minPoint().y, transformedExtents.minPoint().z);
  odPrintConsoleString(OD_T("transformedExtents.maxPoint = (%lf, %lf, %lf)\n\n"), transformedExtents.maxPoint().x,
                       transformedExtents.maxPoint().y, transformedExtents.maxPoint().z);

  OdUInt64 totalAmountOfPoints = pScanDb->getTotalAmountOfPoints();
  odPrintConsoleString(OD_T("totalAmountOfPoints = %llu\n\n"), totalAmountOfPoints);

  bool hasRGB = pScanDb->hasRGB();
  odPrintConsoleString(OD_T("hasRGB = %d\n"), hasRGB);
  bool hasNormals = pScanDb->hasNormals();
  odPrintConsoleString(OD_T("hasNormals = %d\n"), hasNormals);
  bool hasIntensity = pScanDb->hasIntensity();
  odPrintConsoleString(OD_T("hasIntensity = %d\n\n"), hasIntensity);

  OdString scanId = pScanDb->getScanId();
  odPrintConsoleString(OD_T("scanId = %ls\n\n"), scanId.c_str());

  bool normalizeIntensity = pScanDb->getNormalizeIntensity();
  odPrintConsoleString(OD_T("normalizeIntensity = %d\n"), normalizeIntensity);

  float maxIntensity = pScanDb->getMaxIntensity();
  odPrintConsoleString(OD_T("maxIntensity = %f\n"), maxIntensity);

  float minIntensity = pScanDb->getMinIntensity();
  odPrintConsoleString(OD_T("minIntensity = %f\n"), minIntensity);

  OdUInt32 rangeImageWidth = pScanDb->getRangeImageWidth();
  odPrintConsoleString(OD_T("rangeImageWidth = %d\n"), rangeImageWidth);

  OdUInt32 rangeImageHeight = pScanDb->getRangeImageHeight();
  odPrintConsoleString(OD_T("rangeImageHeight = %d\n"), rangeImageHeight);

  bool isLidarData = pScanDb->isLidarData();
  odPrintConsoleString(OD_T("isLidarData = %d\n\n"), isLidarData);

  OdUInt64 amountOfVoxels = pScanDb->getAmountOfVoxels();
  odPrintConsoleString(OD_T("amountOfVoxels = %llu\n\n"), amountOfVoxels);

  OdRcsVoxelIteratorPtr pVoxelIt = pScanDb->getVoxelIterator();
  OdUInt64 voxelIndex = 0;
  for (; !pVoxelIt->done(); pVoxelIt->step())
  {
    OdRcsVoxelPtr pVoxel = pVoxelIt->getVoxel();

    odPrintConsoleString(OD_T("Data from Voxel[%llu] :\n\n"), voxelIndex);

    OdGeExtents3d voxelExtents = pVoxel->getExtents();
    odPrintConsoleString(OD_T("\tvoxelExtents.minPoint = (%lf, %lf, %lf)\n"), voxelExtents.minPoint().x,
                         voxelExtents.minPoint().y, voxelExtents.minPoint().z);
    odPrintConsoleString(OD_T("\tvoxelExtents.maxPoint = (%lf, %lf, %lf)\n\n"), voxelExtents.maxPoint().x,
      voxelExtents.maxPoint().y, voxelExtents.maxPoint().z);

    OdGeMatrix3d transformMatrix = pScanDb->getTransformMatrix();

    OdGeExtents3d transformedVoxelExtents = voxelExtents;
    transformedVoxelExtents.transformBy(transformMatrix);
    odPrintConsoleString(OD_T("\ttransformedVoxelExtents.minPoint = (%lf, %lf, %lf)\n"), transformedVoxelExtents.minPoint().x,
      transformedVoxelExtents.minPoint().y, transformedVoxelExtents.minPoint().z);
    odPrintConsoleString(OD_T("\ttransformedVoxelExtents.maxPoint = (%lf, %lf, %lf)\n\n"), transformedVoxelExtents.maxPoint().x,
      transformedVoxelExtents.maxPoint().y, transformedVoxelExtents.maxPoint().z);

    OdUInt32 numberOfPoints = pVoxel->getTotalNumberOfPoints();
    odPrintConsoleString(OD_T("\tnumberOfPoints = %d\n\n"), numberOfPoints);

    odPrintConsoleString(OD_T("\tPoints data (coordinates are printed as non-transformed) :\n\n"));

    OdRcsPointDataIteratorPtr pPointDataIt = pVoxel->getPointDataIterator();
    OdGePoint3dArray coords;
    OdCmEntityColorArray colors;
    OdUInt16Array normalIndexes;
    OdUInt8Array intensities;
    pPointDataIt->getPoints(coords, colors, normalIndexes, intensities, numberOfPoints);

    OdGePoint3d voxelExtentsMinPoint = voxelExtents.minPoint();
    for (OdUInt32 pointIndex = 0; pointIndex < numberOfPoints; pointIndex++)
    {
      coords[pointIndex].asVector() += voxelExtentsMinPoint.asVector();

      odPrintConsoleString(OD_T("\tcoordinates = (%lf, %lf, %lf);\n"), coords[pointIndex].x,
                                                                       coords[pointIndex].y,
                                                                       coords[pointIndex].z);

      OdUInt8 red = colors[pointIndex].red();
      OdUInt8 green = colors[pointIndex].green();
      OdUInt8 blue = colors[pointIndex].blue();
      odPrintConsoleString(OD_T("\t\tRGB = %02x %02x %02x; "), red, green, blue);

      odPrintConsoleString(OD_T("normalIndex = %d; "), normalIndexes[pointIndex]);

      odPrintConsoleString(OD_T("intensity = %d\n"), intensities[pointIndex]);
    }

    odPrintConsoleString(OD_T("\n"));
    voxelIndex++;
  }
}

void dumpRcpFile(OdPointCloudProjectDatabase* pProjectDb)
{
  OdString filePath = pProjectDb->getProjectDatabaseFilePath();
  odPrintConsoleString(OD_T("Dump .rcp file: %ls\n\n"), filePath.c_str());

  OdUInt64 totalAmountOfPoints = pProjectDb->getTotalAmountOfPoints();
  odPrintConsoleString(OD_T("totalAmountOfPoints = %llu\n\n"), totalAmountOfPoints);

  OdUInt32 totalScansCount = pProjectDb->getTotalScansCount();
  odPrintConsoleString(OD_T("totalScansCount = %d\n\n"), totalScansCount);

  OdUInt32 totalRegionsCount = pProjectDb->getTotalRegionsCount();
  odPrintConsoleString(OD_T("totalRegionsCount = %d\n\n"), totalRegionsCount);

  OdString coordSystemName = pProjectDb->getCoordinateSystemName();
  odPrintConsoleString(OD_T("coordSystemName = %ls\n\n"), coordSystemName.c_str());

  OdInt8 hasRGB = pProjectDb->hasRGB();
  odPrintConsoleString(OD_T("hasRGB = %d\n"), hasRGB);
  OdInt8 hasNormals = pProjectDb->hasNormals();
  odPrintConsoleString(OD_T("hasNormals = %d\n"), hasNormals);
  OdInt8 hasIntensity = pProjectDb->hasIntensity();
  odPrintConsoleString(OD_T("hasIntensity = %d\n\n"), hasIntensity);

  OdGeMatrix3d globalTransform = pProjectDb->getGlobalTransformation();
  OdGeVector3d globalTransform_CsXAxis = globalTransform.getCsXAxis();
  OdGeVector3d globalTransform_CsYAxis = globalTransform.getCsYAxis();
  OdGeVector3d globalTransform_CsZAxis = globalTransform.getCsZAxis();
  OdGePoint3d globalTransform_CsOrigin = globalTransform.getCsOrigin();
  odPrintConsoleString(OD_T("globalTransform :\n"));
  odPrintConsoleString(OD_T("\t%lf  %lf  %lf  0.0\n"), globalTransform_CsXAxis.x, globalTransform_CsXAxis.y, globalTransform_CsXAxis.z);
  odPrintConsoleString(OD_T("\t%lf  %lf  %lf  0.0\n"), globalTransform_CsYAxis.x, globalTransform_CsYAxis.y, globalTransform_CsYAxis.z);
  odPrintConsoleString(OD_T("\t%lf  %lf  %lf  0.0\n"), globalTransform_CsZAxis.x, globalTransform_CsZAxis.y, globalTransform_CsZAxis.z);
  odPrintConsoleString(OD_T("\t%lf  %lf  %lf  1.0\n\n"), globalTransform_CsOrigin.x, globalTransform_CsOrigin.y, globalTransform_CsOrigin.z);

  odPrintConsoleString(OD_T("---------------------------------------------------------\n\n"));

  OdPointCloudScanIteratorPtr pScanIt = pProjectDb->getScanIterator();
  OdUInt32 scanIndex = 0;
  for (; !pScanIt->done(); pScanIt->step())
  {
    odPrintConsoleString(OD_T("Data from SCAN[%d] : \n\n"), scanIndex);

    odPrintConsoleString(OD_T("Title = %ls\n"), pScanIt->getScanTitle().c_str());
    odPrintConsoleString(OD_T("Visible = %d\n\n"), pScanIt->getScanIsVisible());

    OdGeMatrix3d scanTransform = pScanIt->getScanTransform();
    OdGeVector3d scanTransform_CsXAxis = scanTransform.getCsXAxis();
    OdGeVector3d scanTransform_CsYAxis = scanTransform.getCsYAxis();
    OdGeVector3d scanTransform_CsZAxis = scanTransform.getCsZAxis();
    OdGePoint3d scanTransform_CsOrigin = scanTransform.getCsOrigin();
    odPrintConsoleString(OD_T("scanTransform in project:\n"));
    odPrintConsoleString(OD_T("\t%lf  %lf  %lf  0.0\n"), scanTransform_CsXAxis.x, scanTransform_CsXAxis.y, scanTransform_CsXAxis.z);
    odPrintConsoleString(OD_T("\t%lf  %lf  %lf  0.0\n"), scanTransform_CsYAxis.x, scanTransform_CsYAxis.y, scanTransform_CsYAxis.z);
    odPrintConsoleString(OD_T("\t%lf  %lf  %lf  0.0\n"), scanTransform_CsZAxis.x, scanTransform_CsZAxis.y, scanTransform_CsZAxis.z);
    odPrintConsoleString(OD_T("\t%lf  %lf  %lf  1.0\n\n"), scanTransform_CsOrigin.x, scanTransform_CsOrigin.y, scanTransform_CsOrigin.z);

    OdPointCloudScanDatabasePtr pScanDb = pScanIt->getScanDb();

    dumpRcsFile(pScanDb.get());

    odPrintConsoleString(OD_T("---------------------------------------------------------\n\n"));

    scanIndex++;
  }
}
