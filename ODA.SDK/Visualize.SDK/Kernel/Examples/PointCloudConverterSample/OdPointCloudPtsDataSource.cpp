/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#include "OdaCommon.h"
#include "OdPointCloudPtsDataSource.h"
#include "OdSourcePointPtsIterator.h"
#include <fstream>
#include <limits>
#include <iosfwd>

OdPointCloudPtsDataSource::OdPointCloudPtsDataSource(const OdString& filePath, Units unit)
  : m_pointsCount(0), m_unit(unit)
{
  OdResult res = open(filePath);
  if (res != eOk)
  {
    throw OdError_CantOpenFile(filePath);
  }
}

OdUInt64 OdPointCloudPtsDataSource::pointsCount() const
{
  return m_pointsCount;
}

OdPointCloudDataSource::Units OdPointCloudPtsDataSource::getUnits() const
{
  return m_unit;
}

OdSourcePointIteratorPtr OdPointCloudPtsDataSource::newSourcePointIterator() const
{
  return new OdSourcePointPtsIterator(m_pPtsFile, m_pointsOffset, m_pointsCount);
}

OdResult OdPointCloudPtsDataSource::open(const OdString& filePath)
{
  if (!odrxSystemServices()->accessFile(filePath, Oda::kFileRead))
    return eCantOpenFile;

  m_filePath = filePath;
  m_pPtsFile = new std::fstream((const char*)filePath);

  *m_pPtsFile >> m_pointsCount;
  m_pPtsFile->ignore((std::numeric_limits<std::streamsize>::max)(), '\n');

  m_pointsOffset = m_pPtsFile->tellg();

  return eOk;
}
