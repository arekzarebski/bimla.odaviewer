/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#include "OdaCommon.h"
#include "OdSourcePointPtsIterator.h"
#include <fstream>
#include <string>
#include "OdDToStr.h"

OdSourcePointPtsIterator::OdSourcePointPtsIterator(TextStreamPtr pPtsFile, std::streampos pointsOffset, OdUInt64 pointsCount)
  : m_pPtsFile(pPtsFile), m_pointsOffset(pointsOffset), m_pointsCount(pointsCount)
{
  start();
}

void OdSourcePointPtsIterator::start()
{
  m_pPtsFile->seekg(m_pointsOffset);
  m_pointsRead = 0;
}

bool OdSourcePointPtsIterator::done() const
{
  if (m_pointsRead == m_pointsCount)
    return true;

  return false;
}

namespace PointCloudConverterSampleUtils
{
  OdStringArray split(const OdString& src, const OdString& separator)
  {
    OdStringArray res;

    OdString currentStr = src;
    int pos = 0;
    while ((pos = currentStr.find(separator)) != -1)
    {
      OdString subStr = currentStr.left(pos);
      res.push_back(subStr);

      currentStr = currentStr.right(currentStr.getLength() - (pos + 1));
    }
    res.push_back(currentStr);

    return res;
  }
};

void OdSourcePointPtsIterator::getPoint(OdSourcePoint& point)
{
  std::string line;
  std::getline(*m_pPtsFile, line);

  OdString odLine(line.c_str());
  OdStringArray params = PointCloudConverterSampleUtils::split(odLine, " ");
  point.m_coord.x = odStrToD(params[0]);
  point.m_coord.y = odStrToD(params[1]);
  point.m_coord.z = odStrToD(params[2]);

  if (params.size() < 7)
  {
    if (params.size() > 3)
    {
      point.m_intensity = odStrToF(params[3]);
    }

    point.m_color = ODRGBA(255, 0, 0, 255);
  }
  else
  {
    point.m_intensity = odStrToF(params[3]);

    OdUInt16 r, g, b;
    r = odStrToInt(params[4]);
    g = odStrToInt(params[5]);
    b = odStrToInt(params[6]);

    point.m_color = ODRGBA(r, g, b, 255);
  }

  ++m_pointsRead;
}
