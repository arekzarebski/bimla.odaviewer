#
#  PointCloudConverterSample executable
#

tkernel_sources(PointCloudConverterSample
    OdPointCloudPtsDataSource.cpp
    OdPointCloudPtsDataSource.h
    OdSourcePointPtsIterator.cpp
    OdSourcePointPtsIterator.h
	
	PointCloudConverterSample.cpp
                   ${TKERNEL_ROOT}/Extensions/ExServices/ExSystemServices.h
	)

include_directories(
					${TKERNEL_ROOT}/Extensions/ExServices
					../Common)
					
if(ODA_SHARED)
  set ( PointCloudConverterSample_libs  ${TD_EXLIB} ${TD_GE_LIB} ${TD_ROOT_LIB})
else(ODA_SHARED)
  set ( PointCloudConverterSample_libs  ${TD_EXLIB} ${TD_RCSFILESERVICES_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} 
  ${TH_MINIZIP_MEM_LIB} ${TH_ZLIB_LIB} ${TH_libXML} ${TH_CONDITIONAL_LIBCRYPTO})
endif(ODA_SHARED)					

if(MSVC)
tkernel_sources(PointCloudConverterSample
				  PointCloudConverterSample.rc
				 )
endif(MSVC)

tkernel_executable(PointCloudConverterSample ${PointCloudConverterSample_libs} ${TD_ALLOC_LIB})

tkernel_project_group(PointCloudConverterSample "Examples")
