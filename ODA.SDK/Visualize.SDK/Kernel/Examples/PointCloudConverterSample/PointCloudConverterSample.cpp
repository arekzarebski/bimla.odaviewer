/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#include "OdaCommon.h"
#include "StaticRxObject.h"
#include "ExSystemServices.h"
#include "DynamicLinker.h"
#include "diagnostics.h"
#include "RxDynamicModule.h"

#include "RcsFileServices/RxRcsFileServices.h"

#include "OdPointCloudPtsDataSource.h"

#define STL_USING_IOSTREAM
#include "OdaSTL.h"
#define  STD(a)  std:: a

#ifdef OD_HAVE_CONSOLE_H_FILE
#include <console.h>
#endif
#include "ExPrintConsole.h"

#ifndef _TOOLKIT_IN_DLL_
  ODRX_DECLARE_STATIC_MODULE_ENTRY_POINT(RcsFileServicesModule);

  ODRX_BEGIN_STATIC_MODULE_MAP()
    ODRX_DEFINE_STATIC_APPMODULE(RX_RCS_FILE_SERVICES, RcsFileServicesModule)
  ODRX_END_STATIC_MODULE_MAP()
#endif

static const char cUsage[] =
{
  "PointCloudConverterSample sample program. Copyright (C) " TD_COPYRIGHT_START_YEAR_S  TD_COPYRIGHT_END_S "\n"
  "Usage: PointCloudConverterSample <inputFileType> <inputFilePath> <outputFilePath>\n"
  "  <inputFileType> can be:\n"
  "                     pts\n"
  "  <inputFilePath>  - full path to the input file\n"
  "  <outputFilePath> - full path to the output .rcs file.\n"
  "\nPress ENTER to continue...\n"
};

#if defined(OD_USE_WMAIN)
int wmain(int argc, wchar_t* argv[])
#else
int main(int argc, char* argv[])
#endif
{
#ifdef OD_HAVE_CCOMMAND_FUNC
  argc = ccommand(&argv);
#endif

  /**********************************************************************/
  /* Verify the argument count and display an error message as required */
  /**********************************************************************/
  if (argc < 4)
  {
    STD(cout) << cUsage;
    STD(cin).get();
    return 1;
  }

#ifndef _TOOLKIT_IN_DLL_
  ODRX_INIT_STATIC_MODULE_MAP();
#endif

  /**********************************************************************/
  /* Initialize Runtime Extension environment                           */
  /**********************************************************************/
  OdStaticRxObject<ExSystemServices> svcs;
  odrxInitialize(&svcs);

  OdString format(argv[1]), filePath(argv[2]), outFilePath(argv[3]);

  try
  {
    OdRxRcsFileServicesPtr pModule = odrxDynamicLinker()->loadModule("RcsFileServices");

    if (format.iCompare(OD_T("pts")) == 0)
    {
      OdPointCloudDataSourcePtr pDataSource = new OdPointCloudPtsDataSource(filePath);
      OdPointCloudConverterPtr pConverter = pModule->getPointCloudConverter(pDataSource);

      pConverter->convertToRcsFormat(outFilePath);
    }
    else
    {
      odPrintConsoleString(OD_T("\nFormat of the input file is not supported by example!\n"));
    }

    odrxDynamicLinker()->unloadModule("RcsFileServices");
  }
  catch (OdError e)
  {
    odPrintConsoleString(e.description());
  }
  /**********************************************************************/
  /* Uninitialize Runtime Extension environment                         */
  /**********************************************************************/
  ::odrxUninitialize();

	return 0;
}
