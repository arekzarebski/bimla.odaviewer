/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _OD_ODPOINTCLOUDPTSDATASOURCE_H_
#define _OD_ODPOINTCLOUDPTSDATASOURCE_H_

#include "TD_PackPush.h"
#include "RcsFileServices/RxRcsFileServices.h"

#define STL_USING_STREAM
#include "OdaSTL.h"

typedef OdSharedPtr<std::fstream> TextStreamPtr;

class OdPointCloudPtsDataSource : public OdPointCloudDataSource
{
public:

  OdPointCloudPtsDataSource(const OdString& filePath, Units unit = kMeter);

  virtual OdUInt64 pointsCount() const;

  virtual Units getUnits() const;

  virtual OdSourcePointIteratorPtr newSourcePointIterator() const;

private:
  OdResult open(const OdString& filePath);

  OdString       m_filePath;
  TextStreamPtr  m_pPtsFile;

  OdUInt64       m_pointsCount;
  std::streampos m_pointsOffset;

  Units          m_unit;
};

#include "TD_PackPop.h"

#endif //#ifndef _OD_ODPOINTCLOUDPTSDATASOURCE_H_
