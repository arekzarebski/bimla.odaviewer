#
#  XmlGLES2View executable
#

# ExClip sources (for testing purposes)

if(EXISTS ${TKERNEL_ROOT}/Source/Gi/ExClip)
set(exclip_include_files
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipChains.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipContext.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipDefs.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipGhost.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipLog.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipPCtx.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipPoly.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipSBase.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipSections.h
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipSpace.h
)
SOURCE_GROUP("Header Files\\ExClip" FILES ${exclip_include_files})
set(exclip_source_files
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipContext.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipCurve.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipGhost.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipLog.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipPoly.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipSBase.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipSections.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipShape.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipSpace.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipSPlane.cpp
    ${TKERNEL_ROOT}/Source/Gi/ExClip/ExClipSPoly.cpp
)
SOURCE_GROUP("Source Files\\ExClip" FILES ${exclip_source_files})
set(exclip_test_files
    ${exclip_include_files}
    ${exclip_source_files}
    ExClipClient.cpp
)
endif(EXISTS ${TKERNEL_ROOT}/Source/Gi/ExClip)

# Application sources

tkernel_sources(XmlGLES2View
    WinMain.cpp
    AppClient.h
    AnimationHost.h
    AnimationHost.cpp
    XmlViewClient.cpp
    ${exclip_test_files}
)

include_directories(${TKERNEL_ROOT}
                    ${TKERNEL_ROOT}/Include
                    ${TKERNEL_ROOT}/Include/Tr
                    ${TKERNEL_ROOT}/Include/Tr/render
                    ${TKERNEL_ROOT}/Include/Tr/extbar
                    ${TKERNEL_ROOT}/Extensions/ExRender
                    ${TKERNEL_ROOT}/Extensions/ExRender/TrXml
                    ${TKERNEL_ROOT}/Extensions/ExRender/TrXml/IO
                    ${TKERNEL_ROOT}/Extensions/ExServices
                    ${platform_includes}
)

if(ODA_SHARED)
set ( od_xmlgles2view_ex_libs ${TD_ROOT_LIB} ${TD_GE_LIB} ${TR_BASE_LIB} ${TR_XML_IO_LIB} )
else(ODA_SHARED)
set ( od_xmlgles2view_ex_libs ${TD_GE_LIB} ${TD_SPATIALINDEX_LIB} ${TD_ROOT_LIB} ${TR_TXR_GL2_LIB} ${TR_RENDER_LIB} ${TR_BASE_LIB}
                              ${TR_XML_IO_LIB} ${TH_CONDITIONAL_LIBCRYPTO} )
endif(ODA_SHARED)

if(MSVC)
tkernel_sources(XmlGLES2View XmlGLES2View.rc)
endif(MSVC)

tkernel_executable(XmlGLES2View ${od_xmlgles2view_ex_libs} ${TR_EXTBAR_LIB}
                           ${TD_EXLIB}
                           ${TD_ROOT_LIB}
                           ${TD_ALLOC_LIB}
                           ${TH_TINYXML_LIB}
                           ${TD_SPATIALINDEX_LIB}
)

SET_TARGET_PROPERTIES(XmlGLES2View PROPERTIES LINK_FLAGS /entry:"wWinMainCRTStartup")
set_target_properties(XmlGLES2View PROPERTIES WIN32_EXECUTABLE 1)

tkernel_project_group(XmlGLES2View "TrVis/Examples")

remove_definitions()
