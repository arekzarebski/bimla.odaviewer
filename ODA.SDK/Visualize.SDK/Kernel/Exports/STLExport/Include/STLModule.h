/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef _ODSTLMODULEIMPL_INCLUDED_
#define _ODSTLMODULEIMPL_INCLUDED_

#include "STLExportDef.h"
#include "RxDynamicModule.h"
#include "DbBaseDatabase.h"

class OdGiDrawable;
class OdStreamBuf;

/** \details
  <group OdExport_Classes> 
*/
namespace TD_STL_EXPORT
{
  /** \details
   This class implements the module for STL export.
  */
  class STLEXPORT_DLL STLModule : public OdRxModule
  {
  protected:
    /** \details
      Initializes the STL export module.
    */
    void initApp();

    /** \details
      Uninitializes the STL export module.
    */
    void uninitApp();
  public:
    /** \details
       Exports an element to STL file
       
       Input : pEntity - element to export
               bTextMode - if true, export to ASCII STL format, else to binary STL format.
               positiveOctant - if true, move STL coordinates into all positive octant
       Output: pOutStream - output stream (file stream, memory stream)
       
       Return : eOk is ok
                or OdResult error code
    */
    virtual OdResult exportSTL(OdDbBaseDatabase *pDb, const OdGiDrawable &pEntity, OdStreamBuf &pOutStream, bool bTextMode, double dDeviation, bool positiveOctant = true);

    /** \details
       Exports an element to STL file with checking of solid topology

       Input : pEntity - element to export
               bTextMode - if true, export to ASCII STL format, else to binary STL format.
               positiveOctant - if true, move STL coordinates into all positive octant
       Output: pOutStream - output stream (file stream, memory stream)

       Return : eOk is ok
                or OdResult error code
    */
    virtual OdResult exportSTLEx(OdDbBaseDatabase *pDb, const OdGiDrawable &pEntity, OdStreamBuf &pOutStream, bool bTextMode, double dDeviation, bool positiveOctant = true);
  };

  /** \details
    This template class is a specialization of the OdSmartPtr class for STLModule object pointers.
  */
  typedef OdSmartPtr<STLModule> STLModulePtr;
}
#endif //_ODSTLMODULEIMPL_INCLUDED_
