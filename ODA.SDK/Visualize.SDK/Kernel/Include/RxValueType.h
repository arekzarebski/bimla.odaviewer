/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2018, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2018 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////



#ifndef _ODRXVALUETYPE_INCLUDED_
#define _ODRXVALUETYPE_INCLUDED_

#include <memory.h>
#include "RxObject.h"
#include "OdString.h"

class OdRxProperty;
class OdRxValue;
class OdRxEnumTag;

/** \details
  <group !!RECORDS_tkernel_apiref>

  This interface is implemented by the value types having non-trivial constructor and/or destructor
*/
struct IOdRxNonBlittableType
{
  /** \details
    Called to construct a copy of the object.
    
    \param dest [in] Memory location to construct an object at.
    \param source [in] Pointer to the source object.
  */
  virtual void construct(void* dest, const void* source) const = 0;

  /** \details
    Called to assign the object.

    \param dest [in] Pointer to the destination object.
    \param source [in] Pointer to the source object.
  */
  virtual void assign(void* dest, const void* source) const = 0;

  /** \details
    Called to destroy the object.

    \param instance [in] Pointer to the object.
  */
  virtual void destruct(const void* instance) const = 0;
};

/** \details
  <group !!RECORDS_tkernel_apiref>

  This interface is implemented by the value types describing enumerations.
*/
struct IOdRxEnumeration
{
  /** \details
    Returns the number of items in the enum.
  */
  virtual int count() const = 0;
  /** \details
    Returns the descriptor of the given enum item.
  */
  virtual const OdRxEnumTag& getAt(int index) const = 0;
};

/** \details
  <group !!RECORDS_tkernel_apiref>

  This interface is implemented by the value types describing objects that can be "opened"/"closed", like OdDbObjectId in Drawings project
*/
struct IOdRxReferenceType
{
  enum OpenMode 
  {
    kForRead = 0,
    kForWrite = 1,
    kForNotify = 2
  };
  /** \details
    Open the object in the specified mode.
    
    \remark Returned object will be automatically closed when the smart pointer is released, as usual.
  */
  virtual OdRxObjectPtr dereference(const OdRxValue& value, OpenMode mode) const = 0;
};

/** \details
  <group !!RECORDS_tkernel_apiref>

  This interface is implemented by the value types describing pointers to OdRxObject descendants.
*/
struct IOdRxObjectValue
{
  /** \details
    Return contained pointer as an OdRxObject.
  */
  virtual const OdRxObject* getRxObject(const OdRxValue& value) const = 0;
};

typedef OdRxValue (*OdRxValueConstructor)();


/**
  \details 
  Metaclass (description of the contained type) of the OdRxValue. 
  Instances of this class are registered in the class dictionary.
  See RxValueTypeUtil.h for examples of the concrete value types and base helper types.
  
  <group OdRx_Classes>
*/ 
class FIRSTDLL_EXPORT OdRxValueType : public OdRxClass
{
  void addRef() {}
  void release() {}
public:
  ODRX_DECLARE_MEMBERS(OdRxValueType);

  ~OdRxValueType();

  bool operator==(const OdRxValueType& rhs) const
  {
    return this == &rhs;
  }

  bool operator!=(const OdRxValueType& rhs) const
  {
    return this != &rhs;
  }

  /** \details
    Amount of memory to store the value of the type. (sizeof(T))
  */
  unsigned int size() const { return m_size; }

  /** \details
    True for the POD types. (Value may be copied by memcpy.)
  */
  bool isBlittable() const { return nonBlittable() == 0; }

  /** \details
    True for the enumerations.
  */
  bool isEnum() const { return enumeration() != 0; }

  /** \details
    True for object handlers, like OdDbObjectId. 
  */
  bool isReference() const { return reference() != 0; }

  /** \details
    Returns the interface handling copying/destroying values for non-POD types
  */
  virtual const IOdRxNonBlittableType* nonBlittable() const { return 0; }

  /** \details
    Returns the interface describing enumeration.
  */
  virtual const IOdRxEnumeration* enumeration() const { return 0; }

  /** \details
    Returns non-null for references.
  */
  virtual const IOdRxReferenceType* reference() const { return 0; }

  /** \details
    Returns non-null for OdRxObject-descendant pointers.
  */
  virtual const IOdRxObjectValue* rxObjectValue() const { return 0; }

  /** \details
    Create new wrapped value of this type.
  */
  virtual OdRxValue createValue() const;
  
  /** \details
    Create new OdRxValue of this type wrapped in OdRxBoxedValue (if you need a smart pointer).
  */
  virtual OdRxObjectPtr create() const ODRX_OVERRIDE;

  /** \details
    Convert value to the given type from this type.

    \param vt [in] Type to convert to.
    \param instance [in] Value of "this" type.
    \param result [out] Resulting value of type vt.
    \returns Returns true if conversion is possible.
  */
  virtual bool toValueType(const OdRxValueType &vt, const OdRxValue& instance, OdRxValue &result) const
  {
    return subToValueType(vt, instance, result);
  }
  /** \details
    Convert value from the given type (type of the "from" value) to this type.

    \param from [in] Value of type vt.
    \param instance [out] Resulting value of "this" type.
    \returns Returns true if conversion is possible.
  */

  virtual bool fromValueType(const OdRxValue &from, OdRxValue& instance) const
  {
    return subFromValueType(from, instance);
  }

  virtual const OdAnsiString typePath(const OdRxValue& instance) const
  {
    return subTypePath(instance);
  }

  enum StringFormat
  {
    kStringFormatGlobal = 0,
    kStringFormatCurrent = 1,
  };

  /** \details
    Convert value to string (mostly for debug purposes).
  */
  virtual OdString toString(const void* instance, StringFormat format = kStringFormatGlobal) const
  {
    return subToString(instance, format);
  }

  /** \details
    Equality operator. Compares values of the underlying C++ type.
  */
  bool equalTo(const void* a, const void* b) const
  {
    ODA_ASSERT(a != NULL);
    if (a == NULL)
      return false;
    ODA_ASSERT(b != NULL);
    if (b == NULL)
      return false;
    return subEqualTo(a, b);
  }

  /** \details
    Specializations of this class connect C++ classes with corresponding OdRxValueType.
  */
  template<typename ValueType> struct Desc
  {
    /** \details
      Returns a singleton instance of the OdRxValueType representing ValueType.
      \remark First call of this function also registers value type in the class dictionary.
    */
    static const OdRxValueType& value() throw();
    
    /** \details
      Unregister and delete value type.
    */
    static void del();
  };
protected:
  OdRxValueType(const OdChar* name, unsigned int size, OdRxMemberCollectionConstructorPtr memberConstruct, void *userData = NULL);
private:
  virtual OdString subToString(const void* instance, StringFormat format) const = 0;
  virtual bool subEqualTo(const void* a, const void* b) const = 0;
  virtual bool subToValueType(const OdRxValueType &vt, const OdRxValue& instance, OdRxValue &in) const { return false; };
  virtual bool subFromValueType(const OdRxValue& from, OdRxValue &instance) const { return false; };
  virtual const OdAnsiString subTypePath(const OdRxValue& instance) const { return ""; };
  OdRxValueType(const OdRxValueType& rhs);
  OdRxValueType& operator=(const OdRxValueType& rhs);
  unsigned int m_size;
};

/** \details
  Smart pointer to objects of the OdRxValueType class.
*/
typedef OdSmartPtr<OdRxValueType> OdRxValueTypePtr;

/** \details
  Specialization for 'no type'.   
  
  <group OdRx_Classes>
*/
template<> struct OdRxValueType::Desc<void>
{
  FIRSTDLL_EXPORT static const OdRxValueType& value() throw();
  FIRSTDLL_EXPORT static void del();
};

class OdRxValueStorage; //this class is never defined, only used in placement new/delete
#ifdef _MSC_VER
#pragma push_macro("new")
#undef new
#endif

inline void* operator new(size_t, OdRxValueStorage* loc) { return loc; }

#ifdef _MSC_VER
#pragma pop_macro("new")
#pragma push_macro("delete")
#undef delete
#endif

inline void operator delete(void* , OdRxValueStorage* ) { }
#ifdef _MSC_VER
#pragma pop_macro("delete")
#endif

#endif // _ODRXVALUETYPE_INCLUDED_
