/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef __OD_GS_MATERIAL_CACHE__
#define __OD_GS_MATERIAL_CACHE__

// Cache for material nodes

#include "TD_PackPush.h"

#include "RxObject.h"
#include "ThreadsCounter.h"

class OdGsBaseVectorizer;
class OdGsBaseModel;
class OdGsMaterialNode;
class OdGsBaseModule;
class OdGsFiler;

/** <group OdGs_Classes> 
*/
class GS_TOOLKIT_EXPORT OdGsMaterialCache : public OdRxObject
{
  protected:
    OdUInt32           m_nMaterials;
    OdGsMaterialNode*  m_pMaterials;
    OdGsBaseModel*     m_pModel;
    mutable OdMutexPtr m_pCacheMutex;
  public:
    OdGsMaterialCache();
    ~OdGsMaterialCache();

    ODRX_DECLARE_MEMBERS(OdGsMaterialCache);

    void setBaseModel(OdGsBaseModel* pModel);

    OdGsBaseModel* baseModel() { return m_pModel; }
    const OdGsBaseModel* baseModel() const { return m_pModel; }

    static OdSmartPtr<OdGsMaterialCache> createObject(OdGsBaseModel* pModel);

    // Search for already initialized node
    OdGsMaterialNode *searchNode(OdDbStub *mtl) const;
    // Initialize new node, if not already initialized or changed and set's as current node
    OdGsMaterialNode *setMaterial(OdGsBaseVectorizer& view, OdDbStub *mtl, bool bDontReinit = false);

    // Remove material node on Erase
    bool removeNode(OdDbStub *mtl);
    bool removeNode(OdGsCache *pCsh);

    // Cache accessors
    OdUInt32 getCacheSize() const;
    OdGsMaterialNode *getCacheNode(OdUInt32 n) const;

    // Aliases
    inline void addNode(OdGsBaseVectorizer& view, OdDbStub *mtl)
    {
      setMaterial(view, mtl, true);
    }

    // Clear cache
    void clearCache();

    // Invalidate cache
    void invalidateCache(const OdGsBaseModule *pModule = NULL);

    // Save cache to filer
    bool saveMaterialCache(OdGsFiler *pFiler) const;
    // Load cache from filer
    bool loadMaterialCache(OdGsFiler *pFiler);
};

typedef OdSmartPtr<OdGsMaterialCache> OdGsMaterialCachePtr;

/** <group OdGs_Classes> 
*/
class GS_TOOLKIT_EXPORT OdGsCurrentMaterialNode
{
  protected:
    OdGsMaterialNode* m_pCurrentNode;
  public:
    OdGsCurrentMaterialNode(OdGsMaterialNode *pCurrentNode = NULL)
      : m_pCurrentNode(pCurrentNode) {}
    ~OdGsCurrentMaterialNode() {}

    // Get current node
    OdGsMaterialNode *currentNode() const
    {
      return m_pCurrentNode;
    }
    // Reset current node
    void setCurrentNode(OdGsMaterialNode *pNode = NULL)
    {
      m_pCurrentNode = pNode;
    }
    // Initialize new node, if not already initialized or changed and set's as current node
    OdGsMaterialNode *setMaterial(OdGsMaterialCache *pCache, OdGsBaseVectorizer& view, OdDbStub *mtl, bool bDontReinit = false);
};

#include "TD_PackPop.h"

#endif // __OD_GS_MATERIAL_CACHE__
