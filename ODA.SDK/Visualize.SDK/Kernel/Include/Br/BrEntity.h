/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////



#ifndef _INC_DDBRENTITY_3F819553013C_INCLUDED
#define _INC_DDBRENTITY_3F819553013C_INCLUDED


#include "OdaCommon.h"
#include "Br/BrExport.h"
#include "Br/BrEnums.h"
#include "Br/BrMassProps.h"

#include "TD_PackPush.h"
#include "SharedPtr.h"
#include "DbBaseSubentId.h"
#include "../Ge/Ge.h"

class OdBrBrep;
class OdBrHit;
class OdGePoint3d;
class OdGeLinearEnt3d;
class OdGeBoundBlock3d;


/** \details
  <group OdBr_Classes>

  This class is the interface base class for BREP topology objects.

  Corresponding C++ library: TD_Br
*/
class ODBR_TOOLKIT_EXPORT OdBrEntity
{
public:

  /** \details
    Returns true if and only if there is no ACIS model associated with this Entity object. 
  */
  bool isNull() const;

  /** \details
    Returns true if and only if the specified Entity object is equivalent to this Entity object.
    
    \remarks
    Two OdBrEntity objects are equivalent if and only if they non-null, and have the
    same subentity path.
    
    \param pOtherEntity [in]  Pointer to any OdBrEntity object.
  */
  bool isEqualTo(
    const OdBrEntity* pOtherEntity) const;
  
  /** \details
    Returns the owner of this Entity object.
  */
  bool getBrep(OdBrBrep& brep) const;

  /** \details
    Validates the element.
  */
  bool checkEntity() const;

  /** \details
  Returns the FullSubentPath of this Entity object.
  see OdDbBody::setSubentPath also.
  */
  OdBrErrorStatus getSubentPath(OdDbBaseFullSubentPath& subPath) const;

  virtual ~OdBrEntity();

  // for internal usage only. OdDbBody::setSubentPath should be used instead.
  OdBrErrorStatus setSubentPath(OdBrBrep &brep, OdDbBaseFullSubentPath& subpath);

  OdBrErrorStatus getPointContainment(const OdGePoint3d&      point,
                                      OdGe::PointContainment& containment,
                                      OdBrEntity*&            brEntContainer ) const;

  OdBrErrorStatus getLineContainment( const OdGeLinearEnt3d& line,
                                      const OdUInt32&        iNumHitsWanted,
                                      OdUInt32&              iNumHitsFound,
                                      OdBrHit*&              brHit ) const;


/*
  void* getEntity() const;
  OdBrErrorStatus setEntity(void* pData, OdDbBaseFullSubentPath& subPath = *(OdDbBaseFullSubentPath*)NULL);
*/
  /** \details
     Calculates entity geometric bounds.
    
     \param block [out] Model space bounding box as OdGeBoundBlock3d.
     \returns
     * odbrNotImplementedYet - if the method is not implemented for the current modeler;
     * odbrUninitialisedObject - if the object is uninitialized;
     * odbrInvalidInput - if the calculation was unsuccessful;
     * odbrOK - if the calculation was successful;
  */
  OdBrErrorStatus getBoundBlock(OdGeBoundBlock3d& block) const;


  // Validation
  OdBrErrorStatus setValidationLevel(const BrValidationLevel& level);
  OdBrErrorStatus getValidationLevel(BrValidationLevel& level) const;

  //BrEntityFlags
  OdUInt32 getFlags() const;

  OdBrErrorStatus getGsMarker(OdGsMarker& marker) const;
/*
  bool brepChanged() const;

  // Queries & Initialisers
  OdBrErrorStatus set(const BrValidationLevel& path);
  OdBrErrorStatus get(BrValidationLevel& path) const;
  OdBrErrorStatus set(OdBrBrepData* data);
  OdBrErrorStatus get(OdBrBrepData*& data) const;
*/
#if defined(__APPLE__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnull-dereference"
#endif
  // Geometric properties

  /** \details
    Provides the mass properties for the entity.
    The mass properties are returned as an instance of OdBrMassProps structure.
    The density is used for the mass properties scaling and is the analogue of the physical value of the density of materials.
    The mass properties are applicable for those entities, that represent topology objects having volume property.

    \param massProps [out] Mass properties of the entity.
    \param dDensity [in] Density for calculating mass properties of the entity.
    \param dTolRequired [in] Tolerance required for the result of the calculation.
    \param dTolAchieved [out] Tolerance achieved in the calculation.

    \returns A value of the OdBrErrorStatus type.

    \remarks
    If density is not specified (NULL), the default value is 1.
    If the mass properties are not applicable to the entity, odbrNotApplicable is returned.
    If an error occurs, the value of the mass properties argument remains unchanged.
  */
  OdBrErrorStatus getMassProps(OdBrMassProps& massProps,
    const double& dDensity     = *(double*)NULL,
    const double& dTolRequired = *(double*)NULL,
    double&       dTolAchieved = *(double*)NULL) const;

  /** \details
    Provides the volume of the entity as a double value.
    The volume property is applicable for most entities, that represent volumetric figures.

    \param dVolume [out] Volume of the entity.
    \param dTolRequired [in] Tolerance required for the result of the calculation.
    \param dTolAchieved [out] Tolerance achieved in the calculation.

    \returns A value of the OdBrErrorStatus type.

    \remarks
    If the volume property is not applicable to the entity, odbrNotApplicable is returned.
    If an error occurs, the value of the volume argument remains unchanged.
  */
  OdBrErrorStatus getVolume(double& dVolume,
    const double& dTolRequired = *(double*)NULL,
    double&       dTolAchieved = *(double*)NULL) const;

  /** \details
    Provides the surface area of the entity as a double value.
    The surface area property is applicable for most entities, that represent volumetric figures.

    \param dArea [out] Surface area of the entity
    \param dTolRequired [in] Tolerance required for the result of the calculation
    \param dTolAchieved [out] Tolerance achieved in the calculation

    \returns A value of the OdBrErrorStatus type.

    \remarks
    If the surface area property is not applicable to the entity, odbrNotApplicable is returned.
    If an error occurs, the value of the surface area argument remains unchanged.
  */
  OdBrErrorStatus getSurfaceArea(double& dArea,
    const double& dTolRequired = *(double*)NULL,
    double&       dTolAchieved = *(double*)NULL) const;

  /** \details
    Provides the perimeter length of the entity as a double value.
    The perimeter length property is applicable for most entities, that represent plane figures and some 3D figures.

    \param dLength [out] Perimeter length of the entity
    \param dTolRequired [in] Tolerance required for the result of the calculation
    \param dTolAchieved [out] Tolerance achieved in the calculation

    \returns A value of the OdBrErrorStatus type.

    \remarks
    If the perimeter length property is not applicable to the entity, odbrNotApplicable is returned.
    If an error occurs, the value of the perimeter length argument remains unchanged.
  */
  OdBrErrorStatus getPerimeterLength(double& dLength,
    const double& dTolRequired = *(double*)NULL,
    double&       dTolAchieved = *(double*)NULL) const;
#if defined(__APPLE__)
#pragma clang diagnostic pop
#endif

protected:
  void *m_pImp;
  OdSharedPtr<OdDbStubPtrArray> m_pFSubentPath;
  bool m_bIsValidate; //The enum has only 2 values.

  OdBrEntity();

  friend class OdBrEntityInternals;
};

#include "TD_PackPop.h"

#endif /* _INC_DDBRENTITY_3F819553013C_INCLUDED */

