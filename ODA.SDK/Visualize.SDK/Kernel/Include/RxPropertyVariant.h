/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////




#ifndef _ODRXPROPERTYVARIANT_INCLUDED_
#define _ODRXPROPERTYVARIANT_INCLUDED_

#include "TD_PackPush.h"

#include "CDAExport.h"
#include "RxVariant.h"
#include "Ge/GePoint3d.h"
#include "Ge/GePoint2d.h"
#include "CmColorBase.h"
#include "Ge/GeVector2d.h"
#include "Ge/GeVector3d.h"
#include "Ge/GeVector3dArray.h"
#include "Ge/GeVector2dArray.h"
#include "Gs/GsFiler.h"
#include "CmEntityColorArray.h"
#include "CmTransparencyArray.h"

//DOM-IGNORE-BEGIN
const int OdRxPropertyVariantDataSize = 24;
//DOM-IGNORE-END

/** \details
    This class defines objects to store property values in the property cache for the Common Data Access API.
    <group OdRx_Classes>
*/
class ODCDA_EXPORT OdRxPropertyVariant : public OdVariant
{
  //DOM-IGNORE-BEGIN
protected:
  OdUInt8 m_data2[OdRxPropertyVariantDataSize - nOdVariantDataSize];

  void setVarType(int newType, int& type, void* data);
  //DOM-IGNORE-END
public:
  
  /** \details
  Gets the pointer to the factory object for the corresponding type.

  \param type [in] Type value.
  \returns Returns a pointer to the factory object for the corresponding type.
*/
  static const OdVariant::TypeFactory* typeFactory(int type);
  
  /** \details
  Defines additional types of stored value.
*/
  typedef enum
  {

    /** The value type is OdGePoint2d*/
    kGePoint2d = OdVariant::kNextType, 

    /** The value type is OdGePoint3d*/
    kGePoint3d,

    /** The value type is OdGeVector2d*/
    kGeVector2d,

    /** The value type is OdGeVector3d*/
    kGeVector3d,

    /** The value type is OdCmEntityColor*/
    kCmEntityColor,

    /** The value type is OdCmTransparency*/
    kCmTransparency
  } Type;

  /** \details
   Default constructor for instance of OdRxPropertyVariant class.
  */
  OdRxPropertyVariant();

  /** \details
   Copy constructor from OdVariant for instance of OdRxPropertyVariant class.

   \param val [in] constant reference to OdVariant value.
  */
  OdRxPropertyVariant(const OdVariant& val);

  /** \details
   Copy constructor for instance of OdRxPropertyVariant class.

   \param val [in] constant reference to OdRxPropertyVariant value.
  */
  OdRxPropertyVariant(const OdRxPropertyVariant& val);

  /** \details
   Copy constructor from OdGePoint3d for instance of OdRxPropertyVariant class.

   \param val [in]constant reference to OdGePoint3d value.
  */
  OdRxPropertyVariant(const OdGePoint3d& val);

  /** \details
   Copy constructor from OdGePoint2d for instance of OdRxPropertyVariant class.

   \param val [in]constant reference to OdGePoint2d value.
  */
  OdRxPropertyVariant(const OdGePoint2d& val);

  /** \details
   Copy constructor from OdGeVector2d for instance of OdRxPropertyVariant class.

   \param val [in]constant reference to OdGeVector2d value.
  */
  OdRxPropertyVariant(const OdGeVector2d& val);

  /** \details
   Copy constructor from OdGeVector3d for instance of OdRxPropertyVariant class.

   \param val [in]constant reference to OdGeVector3d value.
  */
  OdRxPropertyVariant(const OdGeVector3d& val);

  /** \details
   Copy constructor from OdCmEntityColor for instance of OdRxPropertyVariant class.

   \param val [in]constant reference to OdCmEntityColor value.
  */
  OdRxPropertyVariant(const OdCmEntityColor& val);

  /** \details
   Copy constructor from OdCmTransparency for instance of OdRxPropertyVariant class.

   \param val [in]constant reference to OdCmTransparency value.
  */
  OdRxPropertyVariant(const OdCmTransparency& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdRxPropertyVariant value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdRxPropertyVariant& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] bool value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const bool& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] double value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const double& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdInt8 value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdInt8& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdUInt8 value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdUInt8& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdInt16 value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdInt16& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdUInt16 value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdUInt16& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdInt32 value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdInt32& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdUInt32 value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdUInt32& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdInt64 value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdInt64& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdUInt64 value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdUInt64& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdString value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdString& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdGePoint2d value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdGePoint2d& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdGePoint3d value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdGePoint3d& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdGeVector2d value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdGeVector2d& val);

  /** \details
   Assigns a new value to the OdRxPropertyVariant, replacing its current contents.

  \param val [in] OdGeVector3d value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdGeVector3d& val);

  /** \details
   Assigns a new value to the OdCmEntityColor, replacing its current contents.

  \param val [in] OdGeVector3d value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdCmEntityColor& val);

  /** \details
   Assigns a new value to the OdCmEntityColor, replacing its current contents.

  \param val [in] OdCmTransparency value.
  \returns Returns a reference to this object.
  */
  OdRxPropertyVariant& operator =(const OdCmTransparency& val);

  /** \details
   Destroys an instance of the OdRxPropertyVariant class.
 */
  ~OdRxPropertyVariant();

  /** \details
   Gets the value as OdGePoint3d.

  \returns Returns a constant reference OdGePoint3d.
  */
  const OdGePoint3d&      getGePoint3d()        const;

  /** \details
   Gets the value as OdGePoint2d.

  \returns Returns a constant reference OdGePoint2d.
  */
  const OdGePoint2d&      getGePoint2d()        const;

  /** \details
   Gets the value as OdGeVector2d.

  \returns Returns a constant reference OdGeVector2d.
  */
  const OdGeVector2d&     getGeVector2d()       const;

  /** \details
   Gets the value as OdGeVector3d.

  \returns Returns a constant reference OdGeVector3d.
  */
  const OdGeVector3d&     getGeVector3d()       const;

  /** \details
   Gets the value as OdCmEntityColor.

  \returns Returns a constant reference OdCmEntityColor.
  */
  const OdCmEntityColor&  getCmEntityColor()    const;

  /** \details
   Gets the value as OdCmTransparency.

  \returns Returns a constant reference OdCmTransparency.
  */
  const OdCmTransparency&  getCmTransparency()  const;

  /** \details
   Gets the value as constant array of OdGePoint3d.

  \returns Returns a constant reference array of OdGePoint3d.
  */
  const OdGePoint3dArray&      getGePoint3dArray()        const;

  /** \details
   Gets the value as constant array of OdGePoint2d.

  \returns Returns a constant reference array of OdGePoint2d.
  */
  const OdGePoint2dArray&      getGePoint2dArray()        const;

  /** \details
   Gets the value as constant array of OdGeVector2d.

  \returns Returns a constant reference array of OdGeVector2d.
  */
  const OdGeVector2dArray&     getGeVector2dArray()       const;

  /** \details
   Gets the value as constant array of OdGeVector3d.

  \returns Returns a constant reference array of OdGeVector3d.
  */
  const OdGeVector3dArray&     getGeVector3dArray()       const;

  /** \details
   Gets the value as constant array of OdCmEntityColor.

  \returns Returns a constant reference array of OdCmEntityColor.
  */
  const OdCmEntityColorArray&  getCmEntityColorArray()    const;

  /** \details
   Gets the value as constant array of OdCmEntityColor.

  \returns Returns a constant reference array of OdCmEntityColor.
  */
  const OdCmTransparencyArray& getCmTransparencyArray()   const;

  /** \details
   Gets the value as array of OdGePoint3d.

  \returns Returns a reference array of OdGePoint3d.
  */
  OdGePoint3dArray&      asGePoint3dArray();

  /** \details
   Gets the value as array of OdGePoint2d.

  \returns Returns a reference array of OdGePoint2d.
  */
  OdGePoint2dArray&      asGePoint2dArray();

  /** \details
   Gets the value as array of OdGeVector2d.

  \returns Returns a reference array of OdGeVector2d.
  */
  OdGeVector2dArray&     asGeVector2dArray();

  /** \details
   Gets the value as array of OdGeVector3d.

  \returns Returns a reference array of OdGeVector3d.
  */
  OdGeVector3dArray&     asGeVector3dArray();

  /** \details
   Gets the value as array of OdCmEntityColor.

  \returns Returns a reference array of OdCmEntityColor.
  */
  OdCmEntityColorArray&  asCmEntityColorArray();

  /** \details
   Gets the value as array of OdCmEntityColor.

  \returns Returns a reference array of OdCmEntityColor.
  */
  OdCmTransparencyArray& asCmTransparencyArray();

  /** \details
    Sets value from the OdGePoint3d object.

    \param val [in] constant reference to OdGePoint3d.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setGePoint3d(const OdGePoint3d& val);

  /** \details
    Sets value from the OdGePoint2d object.

    \param val [in] constant reference to OdGePoint2d.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setGePoint2d(const OdGePoint2d& val);

  /** \details
    Sets value from the OdGeVector2d object.

    \param val [in] constant reference to OdGeVector2d.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setGeVector2d(const OdGeVector2d& val);

  /** \details
    Sets value from the OdGeVector3d object.

    \param val [in] constant reference to OdGeVector3d.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setGeVector3d(const OdGeVector3d& val);

  /** \details
    Sets value from the OdCmEntityColor object.

    \param val [in] constant reference to OdCmEntityColor.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setCmEntityColor(const OdCmEntityColor& val);

  /** \details
    Sets value from the OdCmTransparency object.

    \param val [in] constant reference to OdCmTransparency.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setCmTransparency(const OdCmTransparency& val);

  /** \details
    Sets value from the array of OdGePoint3d objects.

    \param val [in] constant reference to array of OdGePoint3d.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setGePoint3dArray(const OdGePoint3dArray& val);

  /** \details
    Sets value from the array of OdGePoint2d objects.

    \param val [in] constant reference to array of OdGePoint2d.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setGePoint2dArray(const OdGePoint2dArray& val);

  /** \details
    Sets value from the array of OdGeVector2d objects.

    \param val [in] constant reference to array of OdGeVector2d.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setGeVector2dArray(const OdGeVector2dArray& val);

  /** \details
    Sets value from the array of OdGeVector3d objects.

    \param val [in] constant reference to array of OdGeVector3d.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setGeVector3dArray(const OdGeVector3dArray& val);

  /** \details
    Sets value from the array of OdCmEntityColor objects.

    \param val [in] constant reference to array of OdCmEntityColor.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setCmEntityColorArray(const OdCmEntityColorArray& val);

  /** \details
    Sets value from the array of OdCmTransparency objects.

    \param val [in] constant reference to array of OdCmTransparency.
    \returns Returns a reference to the OdRxPropertyVariant.
  */
  OdRxPropertyVariant& setCmTransparencyArray(const OdCmTransparencyArray& val);

};


#include "TD_PackPop.h"

#endif //_ODRXPROPERTYVARIANT_INCLUDED_


