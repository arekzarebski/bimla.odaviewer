/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
// Metal renderer local context

#ifndef ODTRMTLLOCALCONTEXT
#define ODTRMTLLOCALCONTEXT

#include "TD_PackPush.h"

#include "TrRndLocalContext.h"

#include "TrMtlGLenum.h"

/**
  <group TD_Namespaces>
*/
namespace OdaMetal {

  typedef int GLint;
  typedef OdUInt32 GLenum;
  typedef OdUInt32 GLuint;
  typedef OdUInt16 GLushort;
  typedef unsigned char GLubyte;
  typedef unsigned char GLboolean;
  typedef void GLvoid;
  typedef OdInt32 GLsizei;
  typedef float GLfloat;

  struct OdTrGL2ExtensionsRegistry
  {
    enum ExtensionName
    {
      kExtensionNonPow2 = 0,
      kExtensionNonPow2ES,
      kExtensionBGRA,
      kExtensionBGR,
      kExtensionTextureMirroredRepeat,
      kExtensionTextureEdgeClamp,
      kExtensionTextureBorderClamp,
      kExtensionDepthTexture,
      kExtensionDepth24,
      kExtensionPackedDepthStencil,
      kExtensionStencil8,
      kExtensionTextureHalfFloat,
      kExtensionTextureFloat,
      kExtensionTextureRG,
      kExtensionTextureNorm16,
      kExtensionTextureSNorm,
      kExtensionTextureSNorm16,
      kExtensionFloatBufferClamp,
      kExtensionFragDepth,
      kExtensionDrawBuffers,
      kExtensionElementIndexUInt,
      kExtensionGeometryShader,

      kNumExtensions
    };
    enum FunctionName
    {
      kPfnDrawBuffers = 0,
      kPfnProgramParameteri,
      kPfnClampColor,

      kNumFunctions
    };

    bool isExtensionSupported(ExtensionName nExt) { return true; };

    bool isFunctionAvailable(FunctionName nFunc) { return true; };

    //void glDrawBuffers(GLsizei n, const GLenum *bufs);
    //void glProgramParameteri(GLuint program, GLenum pname, GLint value);
  };

/** \details
    <group ExRender_Windows_Classes> 
*/
struct ShaderOptions
{
  OdUInt64     mBool;
  unsigned int mUInt[2];
};

class OdTrGL2LocalContext : public OdTrRndLocalContext
{
public:
  OdTrGL2LocalContext() : OdTrRndLocalContext() { }
  ~OdTrGL2LocalContext() { }

  // Each platform implementation must implement last one method
  static OdSmartPtr<OdTrGL2LocalContext> createLocalContext(OdTrVisRenderClient *pDevice);

  virtual void glViewport(GLint x, GLint y, GLsizei width, GLsizei height) {};
  virtual bool PrepareEncoder() { return false; };
  virtual void glDrawArrays(GLenum mode, GLint first, GLsizei count) {};
  virtual void SetConstantValues(const ShaderOptions &pShaderOptions) {};
  virtual void *GetMetalContext() { return NULL; };

  virtual void glGenTextures(GLsizei n, GLuint *textures) {};
  virtual void glBindTexture(GLenum target, GLuint texture) {};
  virtual void glTexParameteri(GLenum target, GLenum pname, GLint param) {};
  virtual void glPixelStorei(GLenum pname, GLint param) {};
  virtual void glTexImage2D(GLenum target, GLint level, GLint internalformat, 
                            GLsizei width, GLsizei height, GLint border, 
                            GLenum format, GLenum type, const GLvoid *data) {};
  virtual void glActiveTexture(GLenum texture) {};
  virtual void glTexParameterfv(GLenum target, GLenum pname, const GLfloat *params) {};
  virtual bool SendTextureAndSampler(int UniformName, GLenum ActiveTexture, int isVertex) { return false; };

  mutable OdTrGL2ExtensionsRegistry m_pExtensionsRegistry;
  OdTrGL2ExtensionsRegistry &extensionsRegistry() const { return m_pExtensionsRegistry; };
};

typedef OdSmartPtr<OdTrGL2LocalContext> OdTrMtlLocalContextPtr;

} //namespace OdaMetal

#include "TD_PackPop.h"

#endif // ODTRMTLLOCALCONTEXT
