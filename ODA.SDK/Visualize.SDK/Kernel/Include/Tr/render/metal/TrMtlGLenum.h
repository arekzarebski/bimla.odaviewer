/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
// Metal renderer local context

#ifndef ODTRMTLGLENUM
#define ODTRMTLGLENUM

enum
{
    GL_POINTS         =               0x0000
  , GL_LINES          =               0x0001
  , GL_LINE_LOOP      =               0x0002
  , GL_LINE_STRIP     =               0x0003
  , GL_TRIANGLES      =               0x0004
  , GL_TRIANGLE_STRIP =               0x0005
  , GL_TRIANGLE_FAN   =               0x0006
  , GL_QUADS          =               0x0007
  , GL_QUAD_STRIP     =               0x0008
  , GL_POLYGON        =               0x0009
  
  , GL_LINE_STRIP_ADJACENCY
};

enum
{
    GL_TEXTURE_1D
  , GL_TEXTURE_2D
  , GL_TEXTURE_3D
  , GL_TEXTURE_1D_ARRAY
  , GL_TEXTURE_2D_ARRAY
  , GL_TEXTURE_RECTANGLE
  , GL_TEXTURE_CUBE_MAP
  , GL_TEXTURE_CUBE_MAP_ARRAY
  , GL_TEXTURE_BUFFER
  , GL_TEXTURE_2D_MULTISAMPLE
  , GL_TEXTURE_2D_MULTISAMPLE_ARRAY
};

enum
{
    GL_DEPTH_STENCIL_TEXTURE_MODE
  , GL_TEXTURE_BASE_LEVEL
  , GL_TEXTURE_COMPARE_FUNC
  , GL_TEXTURE_COMPARE_MODE
  , GL_TEXTURE_LOD_BIAS
  , GL_TEXTURE_MIN_FILTER
  , GL_TEXTURE_MAG_FILTER
  , GL_TEXTURE_MIN_LOD
  , GL_TEXTURE_MAX_LOD
  , GL_TEXTURE_MAX_LEVEL
  , GL_TEXTURE_SWIZZLE_R
  , GL_TEXTURE_SWIZZLE_G
  , GL_TEXTURE_SWIZZLE_B
  , GL_TEXTURE_SWIZZLE_A
  , GL_TEXTURE_WRAP_S
  , GL_TEXTURE_WRAP_T
  , GL_TEXTURE_WRAP_R
  , GL_TEXTURE_BORDER_COLOR
  , GL_TEXTURE_SWIZZLE_RGBA
};

enum
{
    GL_PACK_ALIGNMENT
  , GL_UNPACK_ALIGNMENT
};

enum
{
    GL_NEAREST
  , GL_LINEAR
};

enum
{
    GL_REPEAT
  , GL_CLAMP_TO_EDGE
  , GL_CLAMP_TO_BORDER
  , GL_MIRRORED_REPEAT
  , GL_MIRROR_CLAMP_TO_EDGE
};

enum
{
    GL_COLOR_BUFFER_BIT
  , GL_DEPTH_BUFFER_BIT
  , GL_STENCIL_BUFFER_BIT

  , GL_NEVER
  , GL_LESS
  , GL_EQUAL
  , GL_LEQUAL
  , GL_GREATER
  , GL_NOTEQUAL
  , GL_GEQUAL
  , GL_ALWAYS

  , GL_KEEP
  , GL_ZERO
  , GL_REPLACE
  , GL_INCR
  , GL_INCR_WRAP
  , GL_DECR
  , GL_DECR_WRAP
  , GL_INVERT

  //, GL_ZERO
  , GL_ONE
  , GL_SRC_COLOR
  , GL_ONE_MINUS_SRC_COLOR
  , GL_DST_COLOR
  , GL_ONE_MINUS_DST_COLOR
  , GL_SRC_ALPHA
  , GL_ONE_MINUS_SRC_ALPHA
  , GL_DST_ALPHA
  , GL_ONE_MINUS_DST_ALPHA
  , GL_CONSTANT_COLOR
  , GL_ONE_MINUS_CONSTANT_COLOR
  , GL_CONSTANT_ALPHA
  , GL_ONE_MINUS_CONSTANT_ALPHA
  , GL_SRC_ALPHA_SATURATE

  , GL_FUNC_ADD
  , GL_FUNC_SUBTRACT
  , GL_FUNC_REVERSE_SUBTRACT

  , GL_FRONT
  , GL_BACK
  , GL_FRONT_AND_BACK

  , GL_BGR_EXT
  , GL_BGRA_EXT
  , GL_RGB
  , GL_RGBA
  , GL_LUMINANCE

  , GL_FALSE
  , GL_TRUE
  
  , GL_UNSIGNED_BYTE
};

enum
{
    GL_RED                    =       0x1903
  , GL_RG                     =       0x8227
  , GL_R8                     =       0x8229
  , GL_RG8                    =       0x822B
  , GL_R16                    =       0x822A
  , GL_RG16                   =       0x822C
  , GL_RED_SNORM              =       0x8F90
  , GL_RG_SNORM               =       0x8F91
  , GL_R8_SNORM               =       0x8F94
  , GL_RG8_SNORM              =       0x8F95
  , GL_R16_SNORM              =       0x8F98
  , GL_RG16_SNORM             =       0x8F99
  , GL_RGBA_FLOAT_MODE        =       0x8820
  , GL_CLAMP_VERTEX_COLOR     =       0x891A
  , GL_CLAMP_FRAGMENT_COLOR   =       0x891B
  , GL_CLAMP_READ_COLOR       =       0x891C
  , GL_FIXED_ONLY             =       0x891D

  , GL_DEPTH_COMPONENT
  , GL_DEPTH_STENCIL
};

namespace OdaMetal {

} //namespace OdaMetal

#endif // ODTRMTLGLENUM
