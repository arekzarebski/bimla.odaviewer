/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
// GLES2 device local context

#ifndef ODTRGL2LOCALCONTEXT
#define ODTRGL2LOCALCONTEXT

#include "TD_PackPush.h"

#include "TrRndLocalContext.h"

class OdTrGL2ExtensionsRegistry;

/** \details
    <group ExRender_Windows_Classes> 
*/
class OdTrGL2LocalContext : public OdTrRndLocalContext
{
  protected:
    mutable OdRxObjectPtr m_pExtensionsRegistry;
  public:
    OdTrGL2LocalContext() : OdTrRndLocalContext() { }
    ~OdTrGL2LocalContext() { }

    // Each platform implementation must implement last one method
    static OdSmartPtr<OdTrGL2LocalContext> createLocalContext(OdTrVisRenderClient *pDevice);

    // Search OpenGL extension in cross-platform way using our own extensions registry
    OdTrGL2ExtensionsRegistry &extensionsRegistry() const;
};

typedef OdSmartPtr<OdTrGL2LocalContext> OdTrGL2LocalContextPtr;

#include "TD_PackPop.h"

#endif // ODTRGL2LOCALCONTEXT
