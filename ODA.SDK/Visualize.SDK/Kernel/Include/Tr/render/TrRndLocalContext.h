/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2018, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2018 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
// Base local context interface

#ifndef ODTRRNDLOCALCONTEXT
#define ODTRRNDLOCALCONTEXT

#include "TD_PackPush.h"

#include "TrVisRenderClient.h"

/** \details
    <group ExRender_Windows_Classes> 
*/
class OdTrRndLocalContext : public OdRxObject
{
  public:
    OdTrRndLocalContext() { }
    ~OdTrRndLocalContext() { }

    virtual void createContext(OdTrVisRenderClient *pDevice) = 0;
    virtual void updateContext(OdTrVisRenderClient * /*pDevice*/) { }
    virtual void destroyContext() = 0;

    virtual bool isContextCreated() const = 0;

    virtual void makeCurrentContext() = 0;

    virtual bool pushCurrentContext() { return false; }
    virtual bool popCurrentContext() { return false; }

    virtual void presentContext() = 0;

    virtual bool isExtensionBasedEmulation() const { return false; }
    virtual bool isExtensionSupported(const char * /*pExtensionName*/) { return false; }
    virtual void *acquireExtensionFunctionPtr(const char * /*pFunctionName*/) { return NULL; }
};

typedef OdSmartPtr<OdTrRndLocalContext> OdTrRndLocalContextPtr;

/** \details
    <group ExRender_Windows_Classes> 
*/
class OdTrRndLocalContextStub : public OdTrRndLocalContext
{
  public:
    OdTrRndLocalContextStub() : OdTrRndLocalContext() { }
    ~OdTrRndLocalContextStub() { }

    virtual void createContext(OdTrVisRenderClient * /*pDevice*/) { }
    virtual void destroyContext() { }

    virtual bool isContextCreated() const { return true; }

    virtual void makeCurrentContext() { }

    virtual void presentContext() { }
};

#include "TD_PackPop.h"

#endif // ODTRRNDLOCALCONTEXT
