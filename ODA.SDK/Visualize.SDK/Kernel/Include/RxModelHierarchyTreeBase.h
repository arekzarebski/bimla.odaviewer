/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2018, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2018 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODRXHIERARCHYTREEBASE_INCLUDED_
#define _ODRXHIERARCHYTREEBASE_INCLUDED_

#include "RxModelTreeBaseNode.h"

//DOM-IGNORE-BEGIN
class OdRxModelHierarchyTreeBaseImpl;
class OdRxModelHierarchyTreeBase;
//DOM-IGNORE-END

/** \details
  A data type that represents a smart pointer to an OdRxModelHierarchyTreeBase object.
*/
typedef OdSmartPtr<OdRxModelHierarchyTreeBase> OdRxModelHierarchyTreeBasePtr;

/** \details
  A data type that represents an array of smart pointers to an OdRxModelHierarchyTreeBase object.
*/
typedef OdArray<OdRxModelHierarchyTreeBasePtr> OdRxModelHierarchyTreeBasePtrArray;

/** \details
    This class is a base class that implements a database hierarchical tree for the Common Data Access API.
	
	<group OdRx_Classes>
*/
class ODCDA_EXPORT OdRxModelHierarchyTreeBase : public OdRxObject
{
  public:
    //DOM-IGNORE-BEGIN
    ODRX_DECLARE_MEMBERS(OdRxModelHierarchyTreeBase);
    ODRX_HEAP_OPERATORS();
    //DOM-IGNORE-END

     /** \details
      Destroys an instance of the OdRxModelHierarchyTreeBase class.
    */
    virtual ~OdRxModelHierarchyTreeBase();

    /** \details
      Creates a hierarchical tree for the database.

      \param pDb [in] Pointer to the source database object.
      \param create_properties_cache [in] flag to create cache of properties. default value is true.
      \returns Returns a smart pointer to the root node of the hierarchical tree.
    */
    virtual OdRxModelTreeBaseNodePtr createDatabaseHierarchyTree(const OdRxObject* pDb, const bool create_properties_cache = true);

    /** \details
      Gets the root node of the hierarchical tree.

      \returns Returns a smart pointer to the root node of the tree.
    */
    virtual OdRxModelTreeBaseNodePtr getDatabaseNode() const;

    /** \details
      Gets the properties for a node by unique identifier.

      \param unique_source_ID [in] Unique identifier represented as OdUInt64.
      \returns Returns the array PropertyInfo.
    */
    virtual OdArray<PropertyInfo> getNodeProperties(OdUInt64 unique_source_ID);

    /** \details
      Serializes the hierarchical tree.

      \param pDb [in] Pointer to a filer object.
    */
    void serialize(OdBaseHierarchyTreeFiler* pFiler) const;

    /** \details
      Deserializes the hierarchical tree.

      \param pDb [in] Pointer to a filer object.
      \returns Returns the result of the deserialization operation.
    */
    OdResult deserialize(OdBaseHierarchyTreeFiler* pFiler);

    //DOM-IGNORE-BEGIN
  protected:
    OdRxModelHierarchyTreeBase();
    OdRxModelHierarchyTreeBase(OdRxModelHierarchyTreeBaseImpl* impl);
    friend class OdRxModelHierarchyTreeBaseImpl;
    OdRxModelHierarchyTreeBaseImpl* m_pImpl;
    //DOM-IGNORE-END
};

#endif

