/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#if !defined(OD_POINTCLOUDPROJECTDATABASE_H__D38E354C_DA17_4C07_917D_8A97547D877F__INCLUDED)
#define OD_POINTCLOUDPROJECTDATABASE_H__D38E354C_DA17_4C07_917D_8A97547D877F__INCLUDED

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TD_PackPush.h"

#include "RootExport.h"
#include "SharedPtr.h"

#include "Ge/GeExtents3d.h"
#include "StringArray.h"
#include "Ge/GeMatrix3d.h"

#include "OdPointCloudScanIterator.h"

class ODRX_ABSTRACT FIRSTDLL_EXPORT OdPointCloudProjectDatabase
{
public:
  virtual ~OdPointCloudProjectDatabase(){}

  virtual OdString getProjectDatabaseFilePath() const = 0;

  virtual OdPointCloudScanIteratorPtr getScanIterator() const = 0;

  virtual void getAllRcsFilePaths(OdStringArray &list) const = 0;

  virtual void getAllRcsRelativeFilePaths( OdStringArray &list ) const = 0;

  virtual OdGeMatrix3d getGlobalTransformation() const = 0;

  virtual OdGeMatrix3d getScanTransform(const OdString &guid) const = 0;

  virtual OdUInt32 getTotalRegionsCount() const = 0;

  virtual OdUInt32 getTotalScansCount() const = 0;

  virtual OdString getCoordinateSystemName() const = 0;

  virtual OdInt8 hasRGB() const = 0;

  virtual OdInt8 hasNormals() const = 0;

  virtual OdInt8 hasIntensity() const = 0;

  virtual OdString getRcsFilePath(const OdString &guid) const = 0;

  virtual OdString getRcsRelativeFilePath(const OdString &guid) const = 0;

  virtual OdUInt64 getTotalAmountOfPoints() const = 0;

  virtual OdGeExtents3d getExtents() const = 0;


  virtual void writeAllXmlDataToStream(OdStreamBuf* s) = 0;
};

typedef OdSharedPtr<OdPointCloudProjectDatabase> OdPointCloudProjectDatabasePtr;

#include "TD_PackPop.h"

#endif
