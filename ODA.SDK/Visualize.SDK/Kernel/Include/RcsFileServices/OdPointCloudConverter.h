/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#if !defined(OD_POINTCLOUDCONVERTER_H__1F251CD6_DB45_4534_9332_AE8CE3238D64__INCLUDED)
#define OD_POINTCLOUDCONVERTER_H__1F251CD6_DB45_4534_9332_AE8CE3238D64__INCLUDED

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TD_PackPush.h"

#include "RootExport.h"
#include "SharedPtr.h"

class OdSourcePoint
{
public:
  
  /** \details
    Constructor for the OdSourcePoint class.
    
    \remarks
    Constructs source point of fully opaque white color and full color intensity.
  */
  OdSourcePoint()
    : m_color(ODRGBA(255, 255, 255, 255)), m_intensity(255.0)
  {
  }

  /** \details
    Destructor for the OdSourcePoint class.
  */
  ~OdSourcePoint(){}

  OdGePoint3d m_coord;
  ODCOLORREF  m_color;
  float       m_intensity;
};

/** \details
  A data type that represents a smart pointer to an <link OdSourcePoint, OdSourcePoint> object.
*/
typedef OdSharedPtr<OdSourcePoint> OdSourcePointPtr;

/** \details
*/
class OdPointCloudConverterParams
{
public:
  
  /** \details
    Constructor for the OdPointCloudConverterParams class.
    
    \remarks
    Constructs point cloud converter parameters with 0.0 value for lower intensity threshold, 100.0 value for upper intensity threshold. 
    The m_isTerrestrial flag specified that input data is terrestrial if set to true, otherwise data is lidar.
  */
  OdPointCloudConverterParams()
    : m_intensityBottom(0.0), m_intensityUpper(100.0), m_isTerrestrial(true)
  {
  }

  /** \details
    Destructor for the OdPointCloudConverterParams class.
  */
  ~OdPointCloudConverterParams() {}

  float m_intensityBottom;
  float m_intensityUpper;
  bool  m_isTerrestrial;
};

/** \details
  A data type that represents a smart pointer to an <link OdPointCloudConverterParams, OdPointCloudConverterParams> object.
*/
typedef OdSharedPtr<OdPointCloudConverterParams> OdPointCloudConverterParamsPtr;

/** \details
*/
class ODRX_ABSTRACT FIRSTDLL_EXPORT OdSourcePointIterator
{
public:
  
  /** \details
    Destructor for the OdSourcePointIterator class.
  */
  virtual ~OdSourcePointIterator(){}

  virtual void start() = 0;  
  virtual bool done() const = 0;

  virtual void getPoint(OdSourcePoint& point) = 0;
};

/** \details
  A data type that represents a smart pointer to an <link OdSourcePointIterator, OdSourcePointIterator> object.
*/
typedef OdSharedPtr<OdSourcePointIterator> OdSourcePointIteratorPtr;

/** \details
*/
class ODRX_ABSTRACT FIRSTDLL_EXPORT OdPointCloudDataSource
{
public:
  enum Units
  {
    kMillimeter = 0,
    kCentimeter,
    kMeter,
    kInch,
    kFeet,
    kUSFeet
  };

  /** \details
    Destructor for the OdPointCloudDataSource class.
  */
  virtual ~OdPointCloudDataSource(){}

  virtual OdUInt64 pointsCount() const = 0;

  virtual Units getUnits() const = 0;

  virtual OdSourcePointIteratorPtr newSourcePointIterator() const = 0;
};

/** \details
  A data type that represents a smart pointer to an <link OdPointCloudDataSource, OdPointCloudDataSource> object.
*/
typedef OdSharedPtr<OdPointCloudDataSource> OdPointCloudDataSourcePtr;

/** <group OdRcs_Classes>
  \details
  This is an interface class that provides functionality to convert user point cloud to .rcs file.
*/
class ODRX_ABSTRACT FIRSTDLL_EXPORT OdPointCloudConverter
{
public:
  
  /** \details
    Destructor for the OdPointCloudConverter class.
  */
  virtual ~OdPointCloudConverter(){}
    
  virtual void convertToRcsFormat(const OdString& rcsFilePath, bool bMTMode = true) = 0;
};

/** \details
  A data type that represents a smart pointer to an <link OdPointCloudConverter, OdPointCloudConverter> object.
*/
typedef OdSharedPtr<OdPointCloudConverter> OdPointCloudConverterPtr;

#include "TD_PackPop.h"

#endif
