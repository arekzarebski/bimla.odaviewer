/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#if !defined(OD_POINTCLOUDSCANDATABASE_H__594EAF5C_A75B_4BA6_A439_251788E15302__INCLUDED)
#define OD_POINTCLOUDSCANDATABASE_H__594EAF5C_A75B_4BA6_A439_251788E15302__INCLUDED

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TD_PackPush.h"

#include "RootExport.h"
#include "SharedPtr.h"

#include "OdRcsVoxelIterator.h"

#include "Ge/GeVector3d.h"
#include "Ge/GeMatrix3d.h"

class ODRX_ABSTRACT FIRSTDLL_EXPORT OdPointCloudScanDatabase
{
public:
  virtual ~OdPointCloudScanDatabase(){}

  virtual OdString getScanDatabaseFilePath() const = 0;

  virtual OdUInt64 getTotalAmountOfPoints() const = 0;

  virtual OdString getScanId() const = 0;

  virtual bool isLidarData() const = 0;

  virtual OdRcsVoxelIteratorPtr getVoxelIterator() const = 0;

  virtual OdUInt64 getAmountOfVoxels() const = 0;

  virtual OdGeVector3d getTranslation() const = 0;

  virtual OdGeVector3d getRotation() const = 0;

  virtual OdGeVector3d getScale() const = 0;

  virtual OdGeMatrix3d getTransformMatrix() const = 0;

  virtual bool hasRGB() const = 0;

  virtual bool hasNormals() const = 0;

  virtual bool hasIntensity() const = 0;

  virtual OdGeExtents3d getExtents() const = 0;

  virtual OdGeExtents3d getTransformedExtents() const = 0;
  
  virtual bool getNormalizeIntensity() const = 0;

  virtual float getMaxIntensity() const = 0;

  virtual float getMinIntensity() const = 0;

  virtual OdUInt32 getRangeImageWidth() const = 0;

  virtual OdUInt32 getRangeImageHeight() const = 0;
};

typedef OdSharedPtr<OdPointCloudScanDatabase> OdPointCloudScanDatabasePtr;

#include "TD_PackPop.h"

#endif
