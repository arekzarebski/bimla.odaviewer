/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#if !defined(OD_RCSPOINTDATAITERATOR_H__FD54AD4F_D4D4_48E8_9BD9_7E75B75E0C9F__INCLUDED)
#define OD_RCSPOINTDATAITERATOR_H__FD54AD4F_D4D4_48E8_9BD9_7E75B75E0C9F__INCLUDED

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TD_PackPush.h"

#include "RootExport.h"
#include "SharedPtr.h"

#include "Ge/GePoint3d.h"
#include "Ge/GePoint3dArray.h"
#include "CmEntityColorArray.h"
#include "Ge/GeVector3dArray.h"
#include "UInt16Array.h"
#include "UInt8Array.h"

class ODRX_ABSTRACT FIRSTDLL_EXPORT OdRcsPointDataIterator
{
public:
  virtual ~OdRcsPointDataIterator(){}

  virtual void start() = 0;
  virtual bool done() const = 0;

  virtual OdUInt32 getPoints(OdGePoint3dArray& coordinates, OdCmEntityColorArray& colors, 
    OdUInt32 requiredNumberOfPoints) = 0;

  virtual OdUInt32 getPoints(OdGePoint3dArray& coordinates, OdCmEntityColorArray& colors, 
    OdGeVector3dArray& normals,
    OdUInt32 requiredNumberOfPoints) = 0;

  virtual OdUInt32 getPoints(OdGePoint3dArray& coordinates, OdCmEntityColorArray& colors, 
    OdUInt16Array& normalIndexes, OdUInt8Array& intensities,
    OdUInt32 requiredNumberOfPoints) = 0;
};

typedef OdSharedPtr<OdRcsPointDataIterator> OdRcsPointDataIteratorPtr;

#include "TD_PackPop.h"

#endif
