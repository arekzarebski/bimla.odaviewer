/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2018, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Teigha(R) software pursuant to a license 
//   agreement with Open Design Alliance.
//   Teigha(R) Copyright (C) 2002-2018 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODRXATTRIBUTE_INCLUDED_
#define _ODRXATTRIBUTE_INCLUDED_

#include "OdString.h"
#include "OdArray.h"
#include "RxValue.h"
#include "RxProperty.h"

  //DOM-IGNORE-BEGIN
class OdRxAttributeImpl;
class OdRxAttributeCollectionImpl;
class OdRxMember;
class OdRxPropertyBase;
//DOM-IGNORE-END

/** \details
  Base class for all attributes.

  <group OdRx_Classes>
*/
class ODRX_ABSTRACT FIRSTDLL_EXPORT OdRxAttribute : public OdRxObject
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxAttribute);
  //DOM-IGNORE-END
  
  /** \details
    Destructor.
  */
  virtual ~OdRxAttribute();
protected:
  //DOM-IGNORE-BEGIN
  OdRxAttribute(OdRxAttributeImpl*);
  friend class OdRxAttributeImpl;
  OdRxAttributeImpl* m_pImpl;
  //DOM-IGNORE-END
};

typedef OdSmartPtr<OdRxAttribute>OdRxAttributePtr;

/** \details
  Container for the attributes of a member (OdRxMember).
  Attribute collection may have only one attribute of the given class.

  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxAttributeCollection
{
  //DOM-IGNORE-BEGIN
  friend class OdRxAttributeCollectionImpl;
  OdRxAttributeCollectionImpl* m_pImpl;
  //DOM-IGNORE-END
public:
  /** \details
    Default constructor.
  */
  OdRxAttributeCollection();

  /** \details
    Destructor.
  */
  ~OdRxAttributeCollection();
  
  /** \details
    Number of elements in the collection.
  */
  int count() const;
  
  /** \details
    Retrieve the attribute at the index (constant version).
  */
  const OdRxAttribute* getAt(int index) const;
  
  /** \details
    Retrieve the attribute at the index (non-constant version).
  */
  OdRxAttribute* getAt(int index);

  /** \details
    Finds the attribute of the given class in the collection (constant version). Returns 0 if not found.
  */
  const OdRxAttribute* get(const OdRxClass* type) const;

  /** \details
    Finds the attribute of the given class in the collection. Returns 0 if not found.
  */
  OdRxAttribute* get(const OdRxClass* type);

  /** \details
    Add an attribute to the collection. If the collection already has an attribute of this class, it will not be replaced.
  */
  OdResult add(const OdRxAttribute* attribute);
  
  /** \details
    Add an attribute to the collection. If the collection already has an attribute of this class, it will be replaced.
  */
  OdResult override(const OdRxAttribute* attribute);

  /** \details
    Removes the attribute of that type from the collection.
    
    \returns eOk if successful or eKeyNotFound otherwise.
  */
  OdResult remove(const OdRxAttribute* attribute);
};

/** \details
  Protocol extension to handle loading strings from resources. (E.g. for localization.)

  <group OdRx_Classes>
*/
class ODRX_ABSTRACT FIRSTDLL_EXPORT OdRxResourceLoader : public OdRxObject
{
protected:
  /** \details
    Load the string identified by the id and hint. Implementation is up to the application.
  */
  virtual OdResult subLoadString(unsigned int id, unsigned int sourceHint, OdString& result) = 0;
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxResourceLoader);
  //DOM-IGNORE-END

  /** \details
    Load the string identified by the id and hint. Queries the class for OdRxResourceLoader and calls subLoadString() to do the actual loading.
  */
  static OdResult loadString(const OdRxObject* pClass, unsigned int id, unsigned int sourceHint, OdString& result);
};

//DOM-IGNORE-BEGIN
class OdRxLocalizedNameAttributeImpl;
//DOM-IGNORE-END

/** \details
  Provides human readable name of the member for UI.
  Uses OdRxResourceLoader to get the actual name.

  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxLocalizedNameAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxLocalizedNameAttribute);
  //DOM-IGNORE-END
  
  /** \details
    Construct the instance of the attribute to store string identifier.
  */
  static OdRxAttributePtr createObject(unsigned int id, unsigned int sourceHint = 0);
  
  /** \details
    Looks up OdRxLocalizedNameAttribute on the object if it is a member, (and in the object class attribute list if it is not), 
    and uses OdRxResourceLoader to get the string from resources identified by id and hint.
  */
  static OdString getLocalizedName(const OdRxObject* pO);
  
  /** \details
    Name string resource identifier.
  */
  unsigned int id() const;
  
  /** \details
    Name string resource identifier hint. (Zero if not set.)
  */
  unsigned int sourceHint() const;
protected:
  //DOM-IGNORE-BEGIN
  OdRxLocalizedNameAttribute(OdRxLocalizedNameAttributeImpl*);
  //DOM-IGNORE-END
};

//DOM-IGNORE-BEGIN
class OdRxAlternateLocalizedNameAttributeImpl;
//DOM-IGNORE-END

/** \details
  Same as OdRxLocalizedNameAttribute, alternative for different contexts (up to the application).
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxAlternateLocalizedNameAttribute : public OdRxLocalizedNameAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxAlternateLocalizedNameAttribute);
  //DOM-IGNORE-END

  /** \details
    Constructor.
  */
  static OdRxAttributePtr createObject(unsigned int id, unsigned int sourceHint = 0);

  //DOM-IGNORE-BEGIN
  OdRxAlternateLocalizedNameAttribute(OdRxAlternateLocalizedNameAttributeImpl* impl);
  //DOM-IGNORE-END
};

class OdRxUiPlacementAttributeImpl;

/** \details
  Used by property palettes, to group properties by category and sort by weight.
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxUiPlacementAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxUiPlacementAttribute);
  OdRxUiPlacementAttribute(OdRxUiPlacementAttributeImpl*);
  //DOM-IGNORE-END

  /** \details
    Create the attribute to store UI category and weight.
  */
  static OdRxAttributePtr createObject(const OdString& category, unsigned int weight);

  /** \details
    Returns UI category for this member.
  */
  static OdString getCategory(const OdRxObject* member);

  /** \details
    Returns UI weight for this member.
  */
  static float getWeight(const OdRxObject* member);
};

//DOM-IGNORE-BEGIN
class OdRxLMVAttributeImpl;
class FIRSTDLL_EXPORT OdRxLMVAttribute : public OdRxAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdRxLMVAttribute);
  OdRxLMVAttribute(OdRxLMVAttributeImpl*);
};
//DOM-IGNORE-END

  //DOM-IGNORE-BEGIN
class OdRxDescriptionAttributeImpl;
//DOM-IGNORE-END

/** \details
  This attribute provides human readable description of the member for UI.

  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxDescriptionAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxDescriptionAttribute);
  OdRxDescriptionAttribute(OdRxDescriptionAttributeImpl*);
  //DOM-IGNORE-END

  /** \details
    Create an attribute to store description string resource id and resource hint.
  */
  static OdRxAttributePtr createObject(const OdString& description, unsigned int id = 0, unsigned int sourceHint = 0);
  
  /** \details
    Looks up OdRxDescriptionAttribute on the object if it is a member, (and in the object class attribute list if it is not),
    and uses OdRxResourceLoader to get the string from resources identified by id and hint.
  */
  static OdString getDescription(const OdRxObject* pO);
  
  /** \details
    Description string resource identifier.
  */
  unsigned int id() const;
  /** \details
    Description string resource identifier hint. (Zero if not set.)
  */
  unsigned int sourceHint() const;
};

/** \details
  Callback type to customize property retrieval in OdRxRefersToAttribute::parseReference().
*/
typedef OdRxPropertyBasePtr (*FindPropertyCallback)(const OdRxObject* pObject, const OdString& pszPropName);

/** \details
  Callback type to optionally convert some known property values in OdRxRefersToAttribute::parseReference().
*/
typedef bool(*ConvertValueCallback)(const OdRxPropertyBase* pProperty, OdRxValue& value);

//DOM-IGNORE-BEGIN
class OdRxRefersToAttributeImpl;
//DOM-IGNORE-END

/** \details
  This attribute is used to provide additional information about properties linked with this one. Usually contained item with the container,
  like "/BlockTableId/Items" for OdDbEntity "BlockId" property.
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxRefersToAttribute : public OdRxAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdRxRefersToAttribute);

  /** \details
    Constructor. Creates an attribute containing provided path.
  */
  static OdRxAttributePtr createObject(const OdString& path);

  /** \details
    Path to the related property (starting with database).
    E.g. "/BlockTableId/Items" for OdDbEntity "BlockId" property
  */
  OdString path() const;

  /** \details
    Parses reference path returning the last property. E.g. for "/BlockTableId/Items" it would be "Items" property of the block table.
    
    \param path [in] Path to the property. Intermediate names are separated by '/'. All the properties but the last should be dereferenceable. (type().isReference() == true)
    \param pObject [in] The object to start with. Usually database, for paths starting with '/'.
    \param find [in] Optional callback to customize property retrieval. OdRxMemberQueryEngine::find is used by default.
    \param convert [in] Optional callback to convert property value to another type.
    \returns Returns the last property in the path.
  */
  static const OdRxPropertyBasePtr parseReference(const OdString& path, const OdRxObject* pObject, FindPropertyCallback find = NULL, ConvertValueCallback convert = NULL);
protected:
  //DOM-IGNORE-BEGIN
  OdRxRefersToAttribute(OdRxRefersToAttributeImpl*);
  //DOM-IGNORE-END
};

//DOM-IGNORE-BEGIN
class OdRxCOMAttributeImpl;
//DOM-IGNORE-END

/** \details
  Provides name of the corresponding COM property if it is different (like "Transparency" (portable) / "EntityTransparency" (COM))
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxCOMAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxCOMAttribute);
  //DOM-IGNORE-END
  /** \details
    Constructor.
  */
  static OdRxAttributePtr createObject(const OdString& name);
  /** \details
    Name of the corresponding COM property.
  */
  OdString name() const;
  //DOM-IGNORE-BEGIN
protected:
  OdRxCOMAttribute(OdRxCOMAttributeImpl*);
  //DOM-IGNORE-END
};

// 
class OdRxFilePathAttributeImpl;
/** \details
  Marks properties containing a file path.
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxFilePathAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxFilePathAttribute);
protected:
  OdRxFilePathAttribute(OdRxFilePathAttributeImpl*);
  //DOM-IGNORE-END
};

//DOM-IGNORE-BEGIN
class OdRxFlagsAttributeImpl;
//DOM-IGNORE-END

/** \details
  This attributes marks the properties that contain bitwise flag collection.
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxFlagsAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxFlagsAttribute);
protected:
  OdRxFlagsAttribute(OdRxFlagsAttributeImpl*);
  //DOM-IGNORE-END
};

//DOM-IGNORE-BEGIN
class OdRxUnitTypeAttributeImpl;
//DOM-IGNORE-END

/** \details
  Represents unit types for properties (value type would be just 'double')
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxUnitTypeAttribute : public OdRxAttribute
{
public:
  /** \details
    Defines possible unit types.
  */
  enum UnitType
  {
    kUnitless = 0,
    kDistance = (0x1 << 0),
    kAngle = (0x1 << 1),
    kArea = (0x1 << 2),
    kVolume = (0x1 << 3),
    kCurrency = (0x1 << 4),
    kPercentage = (0x1 << 5),
    kAngleNotTransformed = (0x1 << 16),
  };

  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxUnitTypeAttribute);
  //DOM-IGNORE-END
  
  /** \details
    Constructor.
  */
  static OdRxAttributePtr createObject(UnitType unitType);

  /** \details
    Property unit type,
  */
  UnitType unitType() const;
protected:
  //DOM-IGNORE-BEGIN
  OdRxUnitTypeAttribute(OdRxUnitTypeAttributeImpl*);
  //DOM-IGNORE-END
};

//DOM-IGNORE-BEGIN
class OdRxUseDialogForReferredCollectionAttributeImpl;

/** \details
  
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxUseDialogForReferredCollectionAttribute : public OdRxAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdRxUseDialogForReferredCollectionAttribute);
protected:
  OdRxUseDialogForReferredCollectionAttribute(OdRxUseDialogForReferredCollectionAttributeImpl*);
};

class OdRxUiCascadingContextMenuAttributeImpl;
/**   
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxUiCascadingContextMenuAttribute : public OdRxAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdRxUiCascadingContextMenuAttribute);
protected:
  OdRxUiCascadingContextMenuAttribute(OdRxUiCascadingContextMenuAttributeImpl*);
};
//DOM-IGNORE-END

//DOM-IGNORE-BEGIN
class OdRxCumulativeAttributeImpl;
//DOM-IGNORE-END

/** \details
  Marks the properties which values can be accumulated in case of multiple selection, like area, length, etc.
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxCumulativeAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxCumulativeAttribute);
  //DOM-IGNORE-END

  /** \details
    Constructor.
  */
  static OdRxAttributePtr createObject(const OdString& type);

  /** \details
    String specifying the type of cumulative value ("area", "length").
  */
  OdString type() const;
protected:
  //DOM-IGNORE-BEGIN
  OdRxCumulativeAttribute(OdRxCumulativeAttributeImpl*);
  //DOM-IGNORE-END
};

//DOM-IGNORE-BEGIN
class OdRxAffinityAttributeImpl;
//DOM-IGNORE-END

/**   
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxAffinityAttribute : public OdRxAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdRxAffinityAttribute);

  static OdRxAttributePtr createObject(const OdString& name);
  OdString name() const;
protected:
  OdRxAffinityAttribute(OdRxAffinityAttributeImpl*);
};

//DOM-IGNORE-BEGIN
class OdRxTypePromotionAttributeImpl;
//DOM-IGNORE-END
/**   
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxTypePromotionAttribute : public OdRxAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdRxTypePromotionAttribute);
  static OdRxAttributePtr createObject(const OdString& properties);
  OdString properties() const;
protected:
  OdRxTypePromotionAttribute(OdRxTypePromotionAttributeImpl*);
};

//DOM-IGNORE-BEGIN
class OdRxUnionTypeAttributeImpl;
//DOM-IGNORE-END

/**   
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxUnionTypeAttribute : public OdRxAttribute
{
public:
  ODRX_DECLARE_MEMBERS(OdRxUnionTypeAttribute);
  static OdRxAttributePtr createObject(const OdString& typenames);
  OdString typenames() const;
protected:
  OdRxUnionTypeAttribute(OdRxUnionTypeAttributeImpl*);
};

class OdRxDefaultValueAttributeImpl;

/** \details
  Default value of this property, e.g. for resetting. Optional.
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxDefaultValueAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxDefaultValueAttribute);
  //DOM-IGNORE-END

  /** \details
    Constructor.
  */
  static OdRxAttributePtr createObject(const OdRxValue& value);

  /** \details
    Default value of the property.
  */
  const OdRxValue& value() const;
protected:
  OdRxDefaultValueAttribute(OdRxDefaultValueAttributeImpl*);
};

//DOM-IGNORE-BEGIN
class OdRxHierarchyLevelAttributeImpl;
//DOM-IGNORE-END

/** \details
  Marks this property as a tree node in 'Common Data Access' API data structure.
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxHierarchyLevelAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxHierarchyLevelAttribute);
  //DOM-IGNORE-END

  /** \details
    Constructor.
  */
  static OdRxAttributePtr createObject(const OdString& value);

  /** \details
    String describing CDA hierarchy level of this node. (E.g. 'Model', 'Block')
  */
  OdString defaultValue();
  virtual OdString value(OdRxValue& value);

  //DOM-IGNORE-BEGIN
  OdRxHierarchyLevelAttribute(OdRxHierarchyLevelAttributeImpl*);
  OdRxHierarchyLevelAttribute();
  //DOM-IGNORE-END
};

class OdRxDisplayAsAttributeImpl;

/** \details
  This attribute may be used by UI to represent properties of dereferenceable types in human readable form (instead of displaying just handle).
  <group OdRx_Classes>
*/
class FIRSTDLL_EXPORT OdRxDisplayAsAttribute : public OdRxAttribute
{
public:
  //DOM-IGNORE-BEGIN
  ODRX_DECLARE_MEMBERS(OdRxDisplayAsAttribute);
  //DOM-IGNORE-END

  /** \details
    Constructor.
  */
  static OdRxAttributePtr createObject(const OdString& property_name);

  /** \details
    The name of the property of the dereferenced object to use instead of the object id.
  */
  OdString propertyName();

  /** \details
    By default this function dereferences the value and retrieves the property which name is stored in the attribute. May be overloaded in custom implementations.
    If the value type is not a reference or the referenced object has no such property, returns an empty string.
  */
  virtual OdString getDisplayValue(OdRxValue& value);

  //DOM-IGNORE-BEGIN
  OdRxDisplayAsAttribute(OdRxDisplayAsAttributeImpl* pImpl);
  OdRxDisplayAsAttribute();
  //DOM-IGNORE-END
};


#endif

