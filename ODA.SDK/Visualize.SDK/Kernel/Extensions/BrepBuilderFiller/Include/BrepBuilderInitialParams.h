/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef _BREPBUILDERFILLERPARAMS_INCLUDED_
#define _BREPBUILDERFILLERPARAMS_INCLUDED_

/** \details
	
    Corresponding C++ library: TD_BrepBuilderFiller
    <group Extension_Classes>
*/
class OdBrepBuilderFillerParams
{
  bool m_skipNullSurface;
  bool m_skipCodge2dCurve;
  bool m_skipCheckLoopType;
  bool m_generateExplicitLoops;

public:
  OdBrepBuilderFillerParams()
    : m_skipNullSurface(false)
    , m_skipCodge2dCurve(false)
    , m_skipCheckLoopType(true)
    , m_generateExplicitLoops(false)
  {}

  /** \details
  If true face without surface will not generate error.
  All topology of such face will be skipped.
  */
  void setSkipNullSurface(bool skipNullSurface)
  {
    m_skipNullSurface = skipNullSurface;
  }
  bool isSkipNullSurface() const
  {
    return m_skipNullSurface;
  }

  /** \details
  If true coedge parametric curves will be ignored.
  */
  void setSkipCoedge2dCurve(bool skipCoedge2dCurve)
  {
    m_skipCodge2dCurve = skipCoedge2dCurve;
  }
  bool isSkipCoedge2dCurve() const
  {
    return m_skipCodge2dCurve;
  }

  /** \details
  If true - filler will not check faces for 2 or more outer loops.
  This check can be used when conversion ODA BimRv to acis is performed to avoid acis errors in face loop. 
  */
  void setSkipCheckLoopType(bool skipCheckLoopType)
  {
    m_skipCheckLoopType = skipCheckLoopType;
  }
  bool isSkipCheckLoopType() const
  {
    return m_skipCheckLoopType;
  }

  /** \details
  If true - Filler will add explicit loops to faces that have no loops 
  (Except for full periodic surfaces such as sphere and torus).
  */
  void setGenerateExplicitLoops(bool val) { 
    m_generateExplicitLoops = val;
  }
  bool isGenerateExplicitLoops() const { 
    return m_generateExplicitLoops;
  }
};

#endif //_BREPBUILDERFILLERPARAMS_INCLUDED_
