/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////


#ifndef _ODBREPBUILDERFILLERMODULEIMPL_INCLUDED_
#define _ODBREPBUILDERFILLERMODULEIMPL_INCLUDED_

#include "BrepBuilderFillerDef.h"

#include "Ge/GeCurve2dPtrArray.h"
#include "Ge/GeCurve3dPtrArray.h"
#include "Ge/GeSurfacePtrArray.h"
#include "Ge/GeCurve2d.h"
#include "Ge/GeCurve3d.h"
#include "Ge/GeSurface.h"
#include "Ge/GeNurbCurve3d.h"
#include "Ge/GeNurbCurve2d.h"
#include "BrepBuilderInitialParams.h"

class OdBrepBuilder;
class OdBrBrep;
struct BrepBuilderInitialData;
class OdIMaterialAndColorHelper;

/** \details
  This class implements BrepBuilderFiller.
  Corresponding C++ library: TD_BrepBuilderFiller
  <group Extension_Classes>
*/
class BREPBUILDERFILLER_DLL OdBrepBuilderFiller
{
  OdGeCurve3dPtrArray       m_Edges;
  OdGeCurve2dPtrArray       m_Coedges;
  OdGeSurfacePtrArray       m_Surfaces;
  OdBrepBuilderFillerParams m_params;

  void clearTempArrays();
public:
  /** \details
  Return properties of filler.
  */
  OdBrepBuilderFillerParams & params() { return m_params; }

  /** \details
  Return properties of filler.
  */
  const OdBrepBuilderFillerParams & params() const { return m_params; }

  /** \details
  Get data from OdBrBrep for later initialization OdBrepBuilder.
  */
  OdResult getDataFrom(
    const OdBrBrep& brep,
    OdIMaterialAndColorHelper* pMaterialHelper,
    BrepBuilderInitialData& data);
  /** \details
  Init the OdBrepBuilder with data gotten from OdBrBrep.
  */
  OdResult initFrom(OdBrepBuilder& builder, const BrepBuilderInitialData& data);

  /** \details
  Init the OdBrepBuilder with geometries from OdBrBrep.
  \param builder [out] BrepBuilder that will be inited.
  \param brep [in] B-Rep which will be used to collect data.
  \param pMaterialHelper [inout] Helper class to receive visual information. Should be valid while calling initFrom.
  \param params [in] Optional parameters to reset default behavior.
  */
  OdResult initFrom(
    OdBrepBuilder& builder, 
    const OdBrBrep& brep,
    OdIMaterialAndColorHelper* pMaterialHelper = NULL);

  /** \details
  Obsolete method. For internal use only.
  */
  OdResult initFromNURBSingleFace(OdBrepBuilder& builder, const OdBrBrep& brep);
};

#endif //_ODBREPBUILDERFILLERMODULEIMPL_INCLUDED_
