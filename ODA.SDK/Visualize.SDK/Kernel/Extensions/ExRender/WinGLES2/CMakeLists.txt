#
#  WinGLES2 library
#

if((APPLE AND NOT MACOS_X86 AND NOT MACOS_X64) OR ANDROID OR GLES2EMUL_ENABLED OR GLES2COMPATIBILITY_ENABLED)

# Source files
tkernel_sources(${TD_TXV_GLES2_LIB}
    ../TrLocalRender/TrVecLocalRenditionGsClient.h
    ../TrLocalRender/TrVecLocalRenditionGsClient.cpp
    ../TrLocalRender/TrVecLocalSharingProvider.h
    ../TrLocalRender/TrVecLocalSharingProvider.cpp
    GLES2Module.cpp
)

if(ODA_SHARED AND MSVC)
tkernel_sources(${TD_TXV_GLES2_LIB}
    win/ExGsGLES2RSTestDialog.h
    win/ExGsGLES2RSTestDialog.cpp
    win/ExtWinRes.h
    win/WinGLES2.rc
)
set(gles2_platform_libraries ${TR_EXTBAR_LIB})
endif(ODA_SHARED AND MSVC)

include_directories(
    ${TKERNEL_ROOT}/Include/Tr
    ${TKERNEL_ROOT}/Include/Tr/vec
    ${TKERNEL_ROOT}/Include/Tr/render
    ${TKERNEL_ROOT}/Include/Tr/extbar
    ${TKERNEL_ROOT}/Extensions/ExRender
    ${TKERNEL_ROOT}/Extensions/ExRender/TrLocalRender
)

tkernel_txv(${TD_TXV_GLES2_LIB} ${TR_VEC_LIB} ${TR_TXR_GL2_LIB} ${TR_RENDER_LIB} ${TR_BASE_LIB}
            ${TD_GS_LIB} ${TD_GI_LIB} ${TD_SPATIALINDEX_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB} ${TH_FT_LIB} ${gles2_platform_libraries})

tkernel_project_group(${TD_TXV_GLES2_LIB} "Extensions")

remove_definitions()

endif((APPLE AND NOT MACOS_X86 AND NOT MACOS_X64) OR ANDROID OR GLES2EMUL_ENABLED OR GLES2COMPATIBILITY_ENABLED)
