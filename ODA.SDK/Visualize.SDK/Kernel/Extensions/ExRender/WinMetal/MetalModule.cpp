/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
// MetalModule.cpp : Defines the initialization routines for the DLL.

#include "OdaCommon.h"
#include "RxDispatchImpl.h"
#include "RxDynamicModule.h"
#include "DynamicLinker.h"
#include "Gs/GsBaseVectorizeDevice.h" // for OdSmartPtr<OdGsBaseVectorizeDevice>
#include "Gs/GsViewImpl.h"            // for OdSmartPtr<OdGsViewImpl>
#include "../vec/TrVecBaseModule.h"
#include "TrVecLocalRenditionGsClient.h"
#include "TrRndRenderModule.h"

// Base device for platform-dependent screen and bitmap devices
class OdMetalPlatformBaseDevice : public OdTrVectorizerModuleHost
{
protected:
  mutable OdTrRndLocalRenditionClientPtr m_pRenditionClient;
  OdUInt32 m_nCheckVersion;
  // @@@TODO: Here you can add your Metal-related properties
public:
  ODRX_DECLARE_PROPERTY_TRV(RenderSettings)
  ODRX_DECLARE_PROPERTY_TRV(SaveContextData)
  ODRX_DECLARE_PROPERTY_TRV(CheckRendererVersion)

  OdMetalPlatformBaseDevice() : m_nCheckVersion(0) { }

  void setRenditionClient(OdTrRndLocalRenditionClient *pRenditionClient) { m_pRenditionClient = pRenditionClient; }
  OdTrVecLocalRenditionGsClient *renditionClient() const { return static_cast<OdTrVecLocalRenditionGsClient*>(m_pRenditionClient.get()); }

  OdTrVisRenditionPtr createRendition() { return renditionClient()->renderModule()->createRendition(m_pRenditionClient); }
  OdTrRndLocalRenditionHostPtr renditionHost() const
  { return renditionClient()->renderModule()->createRenditionHost(reinterpret_cast<OdTrVisRendition*>(renderClient()->getProperty(OD_T("Rendition"))->getIntPtr())); }

  void put_RenderSettings(OdIntPtr) { }
  OdIntPtr get_RenderSettings() const { return (OdIntPtr)renditionHost()->renderSettingsManager(); }

  void put_SaveContextData(bool bSet) { renditionHost()->processContextData(bSet); }
  bool get_SaveContextData() const { return renditionHost()->hasContextData(); }

  void put_CheckRendererVersion(OdUInt32 nCheckVersion) { m_nCheckVersion = nCheckVersion; }
  OdUInt32 get_CheckRendererVersion() const { return renditionClient()->renderModule()->checkRendererVersion(renderClient(), m_nCheckVersion); }

  void generateProperties(OdRxDictionary* pInfo) const
  {
    ODRX_GENERATE_PROPERTY_TRV(RenderSettings)
    ODRX_GENERATE_PROPERTY_TRV(SaveContextData)
    ODRX_GENERATE_PROPERTY_TRV(CheckRendererVersion)
  }
};

ODRX_DEFINE_PROPERTY_PREFIX_TRV(OdMetalPlatformBaseDevice::, RenderSettings      , OdMetalPlatformBaseDevice, getIntPtr)
ODRX_DEFINE_PROPERTY_PREFIX_TRV(OdMetalPlatformBaseDevice::, SaveContextData     , OdMetalPlatformBaseDevice, getBool)
ODRX_DEFINE_PROPERTY_PREFIX_TRV(OdMetalPlatformBaseDevice::, CheckRendererVersion, OdMetalPlatformBaseDevice, getUInt32)

// Base device for platform-dependent screen devices
class _OdPlatformMetalDevice : public OdMetalPlatformBaseDevice
{
protected:
  bool m_bCreateContext;
public:
  ODRX_DECLARE_PROPERTY_TRV(CreateContext)

  _OdPlatformMetalDevice() : m_bCreateContext(true) {
  }

  void put_CreateContext(bool bSet) { m_bCreateContext = bSet; }
  bool get_CreateContext() const { return m_bCreateContext; }

  DeviceSetupType deviceSetupOverride() const { return kDtOnScreen; }

  void generateProperties(OdRxDictionary* pInfo) const
  {
    ODRX_INHERIT_PROPERTIES_TRV(OdMetalPlatformBaseDevice);
    ODRX_GENERATE_PROPERTY_TRV(CreateContext)
  }
};

ODRX_DEFINE_PROPERTY_PREFIX_TRV(_OdPlatformMetalDevice::, CreateContext , _OdPlatformMetalDevice, getBool)

#if defined(__IPHONE_OS_VERSION_MIN_REQUIRED) || defined(TARGET_IPHONE_SIMULATOR)

/* iOS platform device implementation */

class OdPlatformMetalDevice : public _OdPlatformMetalDevice
{
protected:
  OdIntPtr m_EAGLLayer;
  OdIntPtr m_EAGLContext;
public:
  ODRX_DECLARE_PROPERTY_TRV(EAGLLayer)
  ODRX_DECLARE_PROPERTY_TRV(EAGLContext)

  OdPlatformMetalDevice() : m_EAGLLayer(NULL), m_EAGLContext(NULL) { }
  void put_EAGLLayer(OdIntPtr pLayer) { m_EAGLLayer = pLayer; }
  OdIntPtr get_EAGLLayer() const { return m_EAGLLayer; }
  void put_EAGLContext(OdIntPtr pContext) { m_EAGLContext = pContext; }
  OdIntPtr get_EAGLContext() const { return m_EAGLContext; }

  void generateProperties(OdRxDictionary* pInfo) const
  {
    ODRX_INHERIT_PROPERTIES_TRV(_OdPlatformMetalDevice);
    ODRX_GENERATE_PROPERTY_TRV(EAGLLayer)
    ODRX_GENERATE_PROPERTY_TRV(EAGLContext)
  }
};

ODRX_DEFINE_PROPERTY_PREFIX_TRV(OdPlatformMetalDevice::, EAGLLayer  , OdPlatformMetalDevice, getIntPtr)
ODRX_DEFINE_PROPERTY_PREFIX_TRV(OdPlatformMetalDevice::, EAGLContext, OdPlatformMetalDevice, getIntPtr)

#else

/* MacOS platform device implementation */

class OdPlatformMetalDevice : public _OdPlatformMetalDevice
{
protected:
  OdIntPtr m_NSView;
  OdIntPtr m_NSOpenGLContext;
public:
  ODRX_DECLARE_PROPERTY_TRV(NSView)
  ODRX_DECLARE_PROPERTY_TRV(NSOpenGLContext)

  OdPlatformMetalDevice() : m_NSView(NULL), m_NSOpenGLContext(NULL) { }
  void put_NSView(OdIntPtr pWindow) { m_NSView = pWindow; }
  OdIntPtr get_NSView() const { return m_NSView; }
  void put_NSOpenGLContext(OdIntPtr pContext) { m_NSOpenGLContext = pContext; }
  OdIntPtr get_NSOpenGLContext() const { return m_NSOpenGLContext; }

  void generateProperties(OdRxDictionary* pInfo) const
  {
    ODRX_INHERIT_PROPERTIES_TRV(_OdPlatformMetalDevice);
    ODRX_GENERATE_PROPERTY_TRV(NSView)
    ODRX_GENERATE_PROPERTY_TRV(NSOpenGLContext)
  }
};

ODRX_DEFINE_PROPERTY_PREFIX_TRV(OdPlatformMetalDevice::, NSView         , OdPlatformMetalDevice, getIntPtr)
ODRX_DEFINE_PROPERTY_PREFIX_TRV(OdPlatformMetalDevice::, NSOpenGLContext, OdPlatformMetalDevice, getIntPtr)

#endif

class OdPlatformBitmapMetalDevice : public OdMetalPlatformBaseDevice
{
  protected:
    OdUInt32 m_idExternalFBO;
    // @@@TODO: Add your Metal-related properties for bitmap device here
  public:
    ODRX_DECLARE_PROPERTY_TRV(ExternalFBO)

    OdPlatformBitmapMetalDevice() : m_idExternalFBO(0) {}
    void put_ExternalFBO(OdUInt32 idExternalFBO) { m_idExternalFBO = idExternalFBO; }
    OdUInt32 get_ExternalFBO() const { return m_idExternalFBO; }

    void generateProperties(OdRxDictionary* pInfo) const
    {
      ODRX_INHERIT_PROPERTIES_TRV(OdMetalPlatformBaseDevice);
      ODRX_GENERATE_PROPERTY_TRV(ExternalFBO)
    }

    DeviceSetupType deviceSetupOverride() const
    {
      if (renderClient()->hasProperty(OD_T("ForcePartialUpdate")) &&
          renderClient()->getProperty(OD_T("ForcePartialUpdate"))->getBool())
        return kDtOnScreen;
      return kDtOffScreen;
    }
};

ODRX_DEFINE_PROPERTY_PREFIX_TRV(OdPlatformBitmapMetalDevice::, ExternalFBO, OdPlatformBitmapMetalDevice, getUInt32)

class MetalModule : public BaseTrVecModule
{
protected:
  OdTrRndLocalRenditionClientPtr m_pRenditionClient;
  OdTrVectorizerModuleHostPtr setupModuleHost(OdTrVectorizerModuleHost *pModuleHost)
  {
    if (m_pRenditionClient.isNull())
    {
      OdTrRndRenderModulePtr pRenderModule = ::odrxDynamicLinker()->loadModule(OdTrMetalModuleName);
      if (pRenderModule.isNull())
        throw OdError(eFileNotFound);
      m_pRenditionClient = OdTrVecLocalRenditionGsClient::createObject(pRenderModule);
    }
    static_cast<OdMetalPlatformBaseDevice*>(pModuleHost)->setRenditionClient(m_pRenditionClient);
    return pModuleHost;
  }
protected:
  OdSmartPtr<OdGsBaseVectorizeDevice> createDeviceObject()
  { return createVectorizeDevice(setupModuleHost(OdRxObjectImpl<OdPlatformMetalDevice, OdTrVectorizerModuleHost>::createObject()), OdTrVectorizerModuleHost::kDtOnScreen); }
  OdSmartPtr<OdGsViewImpl> createViewObject() { return createVectorizeView(); }

  OdSmartPtr<OdGsBaseVectorizeDevice> createBitmapDeviceObject()
  { return createVectorizeDevice(setupModuleHost(OdRxObjectImpl<OdPlatformBitmapMetalDevice, OdTrVectorizerModuleHost>::createObject()), OdTrVectorizerModuleHost::kDtOffScreen); }
  OdSmartPtr<OdGsViewImpl> createBitmapViewObject() { return createVectorizeView(); }

  virtual void initApp();
  virtual void uninitApp();
};

ODRX_DEFINE_DYNAMIC_MODULE(MetalModule);

void MetalModule::initApp() {
  BaseTrVecModule::initApp();
}

void MetalModule::uninitApp() {
  BaseTrVecModule::uninitApp();
}

//
