#
#  WinMetal library
#

if(IOS OR MACOS_X86 OR MACOS_X64)

# Source files
tkernel_sources(${TD_TXV_METAL_LIB}
    ../TrLocalRender/TrVecLocalRenditionGsClient.h
    ../TrLocalRender/TrVecLocalRenditionGsClient.cpp
    ../TrLocalRender/TrVecLocalSharingProvider.h
    ../TrLocalRender/TrVecLocalSharingProvider.cpp
    MetalModule.cpp
)

include_directories(
    ${TKERNEL_ROOT}/Include/Tr
    ${TKERNEL_ROOT}/Include/Tr/vec
    ${TKERNEL_ROOT}/Include/Tr/render
    ${TKERNEL_ROOT}/Extensions/ExRender
    ${TKERNEL_ROOT}/Extensions/ExRender/TrLocalRender
)

tkernel_txv(${TD_TXV_METAL_LIB} ${TR_TXR_METAL_LIB} ${TR_RENDER_LIB} ${TR_VEC_LIB} ${TR_BASE_LIB}
            ${TD_GS_LIB} ${TD_GI_LIB} ${TD_SPATIALINDEX_LIB} ${TD_GE_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB} ${TH_FT_LIB})

tkernel_project_group(${TD_TXV_METAL_LIB} "Extensions")

remove_definitions()

endif(IOS OR MACOS_X86 OR MACOS_X64)
