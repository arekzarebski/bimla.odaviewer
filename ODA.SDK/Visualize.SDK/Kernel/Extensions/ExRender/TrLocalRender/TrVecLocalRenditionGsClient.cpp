/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
// TrGL2 renderer local client

#include "OdaCommon.h"
#include "Gi/GiPlotStyleDefs.h"
#include "Gs/GsHighlightData.h"
#include "TrVecLocalSharingProvider.h"
#include "TrVecLocalRenditionGsClient.h"

// OdTrVecHlBranchGsClient

const void *OdTrVecHlBranchGsClient::hlBranchGetPtr(OdTrVisHlBranchId hlBranchId) const
{
  return OdTrVisIdToPtr(void *, hlBranchId);
}

bool OdTrVecHlBranchGsClient::hlBranchIsEmpty(const void *pHlBranchIn) const
{ const OdGsHlBranch *pHlBranch = reinterpret_cast<const OdGsHlBranch*>(pHlBranchIn);
  return pHlBranch->isEmpty();
}

bool OdTrVecHlBranchGsClient::hlBranchHasMarkers(const void *pHlBranchIn) const
{ const OdGsHlBranch *pHlBranch = reinterpret_cast<const OdGsHlBranch*>(pHlBranchIn);
  return !pHlBranch->markersEmpty();
}

const void *OdTrVecHlBranchGsClient::hlBranchGetChild(const void *pHlBranchIn, OdTrVisOwning childId) const
{ const OdGsHlBranch *pHlBranch = reinterpret_cast<const OdGsHlBranch*>(pHlBranchIn);
  return const_cast<OdGsHlBranch*>(pHlBranch)->findChild(OdTrVisIdToPtr(OdDbStub, childId));
}

bool OdTrVecHlBranchGsClient::hlBranchHasMarker(const void *pHlBranchIn, OdTrVisGsMarker gsMarker) const
{ const OdGsHlBranch *pHlBranch = reinterpret_cast<const OdGsHlBranch*>(pHlBranchIn);
  return pHlBranch->hasMarker((OdGsMarker)gsMarker);
}

OdTrRndHlBranchClientPtr OdTrVecHlBranchGsClient::createObject()
{
  return OdRxObjectImpl<OdTrVecHlBranchGsClient, OdTrRndHlBranchClient>::createObject();
}

// OdTrVecLocalRenditionGsClient

OdTrRndLocalContextPtr OdTrVecLocalRenditionGsClient::createLocalContext(OdTrVisRenderClient *pDevice)
{
  return OdTrRndLocalContextPtr();
}

OdTrRndHlBranchClientPtr OdTrVecLocalRenditionGsClient::hlBranchClient()
{
  if (m_pHlBranchClient.isNull())
    m_pHlBranchClient = OdTrVecHlBranchGsClient::createObject();
  return m_pHlBranchClient;
}

OdTrVisSharingProviderClientPtr OdTrVecLocalRenditionGsClient::createSharingProvider()
{
  return OdTrVecLocalSharingProvider::createObject(renderModule());
}

bool OdTrVecLocalRenditionGsClient::generateLinetypeTexture(OdPs::LineType ltpId, OdUInt8Array& outPixels, OdUInt8 forePixVal) const
{
  outPixels = ::odgiGetPsLinetypesManager().rasterizeLinetype(ltpId, 2, forePixVal);
  return true;
}

bool OdTrVecLocalRenditionGsClient::generateFilltypeTexture(OdPs::FillStyle fsId, OdUInt8Array& outPixels, OdUInt32 dimX, OdUInt32 dimY, OdUInt8 forePixVal) const
{
  outPixels = ::odgiGetPsFillstylesManager().rasterizeFillstyle(fsId, 0, 0, dimX, dimY, forePixVal);
  return true;
}

OdTrRndLocalRenditionClientPtr OdTrVecLocalRenditionGsClient::createObject(OdTrRndRenderModule *pRenderModule)
{
  OdSmartPtr<OdTrVecLocalRenditionGsClient> pLRC = OdRxObjectImpl<OdTrVecLocalRenditionGsClient>::createObject();
  pLRC->m_pRenderModule = pRenderModule;
  return pLRC;
}

//
