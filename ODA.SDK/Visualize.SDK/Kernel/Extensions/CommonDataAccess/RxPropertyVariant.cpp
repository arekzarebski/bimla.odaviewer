/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#undef _CRTDBG_MAP_ALLOC
#include "OdaCommon.h"
#include "RxPropertyVariant.h"

#if defined(_MSC_VER)
#pragma warning (push)
#pragma warning ( disable : 4065 ) //  switch statement contains 'default' but no 'case' labels
#pragma warning ( disable : 4100 ) //  unreferenced formal parameter
#endif


template<class T>
struct _obj_prop_factory : OdVariant::TypeFactory
{
  _obj_prop_factory() {}
  virtual void construct(void* pData) const
  {
    new ((void*)(pData)) T;
  }
  virtual void destroy(void* pData) const
  {
    ((T*)pData)->~T();
  }
};

#define TYPE_CONTAINER \
  VARTYPE(GePoint2d,      obj)\
  VARTYPE(GePoint3d,      obj)\
  VARTYPE(GeVector2d,     obj)\
  VARTYPE(GeVector3d,     obj)\
  VARTYPE(CmEntityColor,  obj)\
  VARTYPE(CmTransparency, obj)

#undef VARTYPE
#define VARTYPE(Type, storage) \
ODA_ASSUME(sizeof(Od##Type)      <= OdRxPropertyVariantDataSize)\
ODA_ASSUME(sizeof(Od##Type##Array) <= OdRxPropertyVariantDataSize)

TYPE_CONTAINER 

#undef VARTYPE
#define VARTYPE(Type, storage) \
const _##storage##_prop_factory<Od##Type> g_##Type##_factory;

TYPE_CONTAINER

#undef VARTYPE
#define VARTYPE(Type, Allocator) \
const _obj_prop_factory<Od##Type##Array> g_##Type##Array_factory;

TYPE_CONTAINER

const OdVariant::TypeFactory* OdRxPropertyVariant::typeFactory(int type)
{
  switch(type)
  {
#undef VARTYPE
#define VARTYPE(Type, storage) \
  case OdRxPropertyVariant::k##Type:\
    return &g_##Type##_factory;

  TYPE_CONTAINER

#undef VARTYPE
#define VARTYPE(Type, storage) \
  case OdRxPropertyVariant::k##Type | OdVariant::kArray:\
    return &g_##Type##Array_factory;

  TYPE_CONTAINER

  default:
    return OdVariant::typeFactory(type);
  };
}

void OdRxPropertyVariant::setVarType(int newType, int& type, void* data)
{
  const OdVariant::TypeFactory* pOldFactory = OdRxPropertyVariant::typeFactory(type);
  const OdVariant::TypeFactory* pNewFactory = OdRxPropertyVariant::typeFactory(newType);
  if(pOldFactory!=pNewFactory)
  {
    pOldFactory->destroy(data);
    pNewFactory->construct(data);
  }
  type = newType;
}

OdRxPropertyVariant::OdRxPropertyVariant(): OdVariant()
{
}

OdRxPropertyVariant::OdRxPropertyVariant(const OdVariant& val)
{
  OdVariant::operator =(val);
}

OdRxPropertyVariant::OdRxPropertyVariant(const OdRxPropertyVariant& val)
{
  OdRxPropertyVariant::operator =(val);
}

OdRxPropertyVariant& OdRxPropertyVariant::operator =(const OdRxPropertyVariant& val)
{
  
  switch (val.varType())
  {
#undef VARTYPE
#define VARTYPE(Type, storage) \
  case OdRxPropertyVariant::k##Type:\
    return set##Type(val.get##Type());\

    TYPE_CONTAINER

#undef VARTYPE
#define VARTYPE(Type, storage) \
  case OdRxPropertyVariant::k##Type | OdVariant::kArray:\
    return set##Type##Array(val.get##Type##Array());\

    TYPE_CONTAINER
  default:
    return static_cast<OdRxPropertyVariant&>(OdVariant::operator =(val));
    break;
  }
}

OdRxPropertyVariant::~OdRxPropertyVariant()
{
  setVarType(kVoid, m_type, data());
}

#undef TYPE_CONTAINER
#define TYPE_CONTAINER \
  VARTYPE(GePoint2d      )\
  VARTYPE(GePoint3d      )\
  VARTYPE(GeVector2d     )\
  VARTYPE(GeVector3d     )\
  VARTYPE(CmEntityColor  )\
  VARTYPE(CmTransparency )

#define ODTYPE(Type) Od##Type

// defining OdRxPropertyVariant explicit constructors

#undef BODY
#define BODY(Type, value) { setVarType(k##Type, m_type, data()); *((Od##Type *)data()) = value; }

#undef VARTYPE
#define VARTYPE(Type) \
OdRxPropertyVariant::OdRxPropertyVariant(const ODTYPE(Type)& val) BODY(Type, val);

TYPE_CONTAINER

// defining OdRxPropertyVariant::setXxxx()

#undef BODY
#define BODY(Type, value) { setVarType(k##Type, m_type, data()); *((Od##Type *)data()) = value; return *this; }

#undef VARTYPE
#define VARTYPE(Type) \
OdRxPropertyVariant& OdRxPropertyVariant::set##Type(const ODTYPE(Type)& val) BODY(Type, val)

TYPE_CONTAINER

// defining OdRxPropertyVariant::setXxxxArray()

#undef BODY
#define BODY(Type, value) { setVarType(k##Type | kArray, m_type, data()); *((Od##Type##Array*)data()) = value; return *this; }

#undef VARTYPE
#define VARTYPE(Type) \
OdRxPropertyVariant& OdRxPropertyVariant::set##Type##Array(const Od##Type##Array& array) BODY(Type, array)

TYPE_CONTAINER

// defining OdRxPropertyVariant::getXxxx()

inline void assertType(int to, int from)
{
  if (to != from)
    throw OdError_InvalidVariantType();
}

#undef BODY
#define BODY(Type) const { assertType(k##Type, m_type); return *((Od##Type *)data()); }

#undef VARTYPE
#define VARTYPE(Type) \
const ODTYPE(Type)& OdRxPropertyVariant::get##Type() BODY(Type)

TYPE_CONTAINER

// defining OdRxPropertyVariant::getXxxxArray()

#undef VARTYPE
#define VARTYPE(Type) \
const Od##Type##Array& OdRxPropertyVariant::get##Type##Array() const { assertType(k##Type | kArray, m_type); return *((Od##Type##Array*)data()); }

TYPE_CONTAINER

// defining OdRxPropertyVariant::asXxxxArray

#undef VARTYPE
#define VARTYPE(Type) \
Od##Type##Array& OdRxPropertyVariant::as##Type##Array()\
{ setVarType(k##Type | kArray, m_type, data()); return *((Od##Type##Array*)data()); }

TYPE_CONTAINER

#undef TYPE_CONTAINER
#define TYPE_CONTAINER \
  VARTYPE(String )\
  VARTYPE(Bool   )\
  VARTYPE(Int8   )\
  VARTYPE(Int16  )\
  VARTYPE(Int32  )\
  VARTYPE(Int64  )\
  VARTYPE(UInt8  )\
  VARTYPE(UInt16 )\
  VARTYPE(UInt32 )\
  VARTYPE(UInt64 )\
  VARTYPE(Double )\
  VARTYPE(GePoint2d      )\
  VARTYPE(GePoint3d      )\
  VARTYPE(GeVector2d     )\
  VARTYPE(GeVector3d     )\
  VARTYPE(CmEntityColor  )\
  VARTYPE(CmTransparency )

#define ODTYPE(Type) Od##Type

// defining OdRxPropertyVariant::operator =

#undef BODY
#define BODY(Type, value) { setVarType(k##Type, m_type, data()); *((Od##Type *)data()) = value; return *this;}

#undef VARTYPE
#define VARTYPE(Type) \
OdRxPropertyVariant& OdRxPropertyVariant::operator =(const ODTYPE(Type)& val) BODY(Type, val);

TYPE_CONTAINER




