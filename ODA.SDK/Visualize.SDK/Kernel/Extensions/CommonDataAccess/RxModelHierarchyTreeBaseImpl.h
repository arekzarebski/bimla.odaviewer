
#ifndef _ODRXHIERARCHYTREEBASEIMPL_H_DEFINED_
#define _ODRXHIERARCHYTREEBASEIMPL_H_DEFINED_

#include "RxModelHierarchyTreeBase.h"
#include "../Include/RxModelTreeBaseNode.h"
#include "../Include/RxProperty.h"
#include "../Include/RxAttribute.h"
#define STL_USING_MAP
#include "OdaSTL.h"


class ODCDA_EXPORT OdRxModelHierarchyTreeBaseImpl
{
public:
  ODRX_HEAP_OPERATORS();
  static OdRxModelHierarchyTreeBaseImpl* getImpl(const OdRxModelHierarchyTreeBasePtr& pObj)
  {
    return const_cast<OdRxModelHierarchyTreeBaseImpl*>(pObj->m_pImpl);
  }

  OdRxModelHierarchyTreeBaseImpl();
  virtual ~OdRxModelHierarchyTreeBaseImpl();

  virtual OdRxModelTreeBaseNodePtr createDatabaseHierarchyTree(const OdRxObject* pDb, const bool create_properties_cache = true);

  virtual OdRxModelTreeBaseNodePtr getDatabaseNode() const;

  void serialize(OdBaseHierarchyTreeFiler* pFiler) const;

  OdResult deserialize(OdBaseHierarchyTreeFiler* pFiler);

  virtual OdArray<PropertyInfo> getNodeProperties(OdUInt64 unique_source_ID);

protected:

  struct AttributesInfo
  {
    AttributesInfo()
      :pNameAttribute(NULL)
      ,pHierarchyAttribute(NULL)
      ,pUiPlacementAttribute(NULL)
      ,pLocalizedNameAttribute(NULL)
    {}
    OdRxDisplayAsAttribute* pNameAttribute;
    OdRxHierarchyLevelAttribute* pHierarchyAttribute;
    OdRxUiPlacementAttribute* pUiPlacementAttribute;
    OdRxLocalizedNameAttribute* pLocalizedNameAttribute;
  };

  virtual OdRxModelTreeBaseNodePtr createNodeObject() = 0;

  void processNode(OdRxModelTreeBaseNode* pParent, const OdRxObject* pObj, std::map<OdUInt64, OdRxModelTreeBaseNodePtr>& cache, const bool create_properties_cache);

  void processElement(OdRxModelTreeBaseNode* pParent, AttributesInfo& attributes_info, 
    OdRxValue& prop_value, std::map<OdUInt64, OdRxModelTreeBaseNodePtr>& cache, const bool create_properties_cache);
  
  void processRxProperty(OdRxModelTreeBaseNode* pParent, const OdRxObject* pObj, OdRxProperty* pProperty, PropertyInfo& info,
    AttributesInfo& attributes_info, std::map<OdUInt64, OdRxModelTreeBaseNodePtr>& cache, const bool create_properties_cache);

  void processRxCollection(OdRxModelTreeBaseNode* pParent, const OdRxObject* pObj, const OdRxCollectionProperty* pPropertyCollection, PropertyInfo& info,
    AttributesInfo& attributes_info, std::map<OdUInt64, OdRxModelTreeBaseNodePtr>& cache, const bool create_properties_cache);

  void processProperty(OdRxModelTreeBaseNode* pParent, const OdRxObject* pObj, const OdRxMember* pRxMember, PropertyInfo& info,
    std::map<OdUInt64, OdRxModelTreeBaseNodePtr>& cache, const bool create_properties_cache);

  struct NodeCache
  {
    OdRxModelTreeBaseNodePtr m_pNode;
    OdArray<OdUInt64>        m_IDs;
  };
  OdUInt64 createNodeCache(OdRxModelTreeBaseNodePtr pNode, OdArray<NodeCache>& cache) const;

  OdRxModelTreeBaseNodePtr  m_DatabaseNode;

  std::map<OdUInt64, OdRxModelTreeBaseNodePtr> m_NodeCache;

};

#endif
