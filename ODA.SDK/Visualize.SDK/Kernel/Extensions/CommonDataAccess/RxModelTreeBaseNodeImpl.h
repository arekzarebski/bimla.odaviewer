
#ifndef _ODRXBASETREENODEIMPL_H_DEFINED_
#define _ODRXBASETREENODEIMPL_H_DEFINED_

#include "RxModelTreeBaseNode.h"
#include "../Include/RxProperty.h"
#include "RxVariant.h"


class ODCDA_EXPORT OdRxModelTreeBaseNodeImpl
{
public:
  ODRX_HEAP_OPERATORS();
  static OdRxModelTreeBaseNodeImpl* getImpl(const OdRxModelTreeBaseNodePtr& pObj)
  {
    return const_cast<OdRxModelTreeBaseNodeImpl*>(pObj->m_pImpl);
  }

  OdRxModelTreeBaseNodeImpl();
  virtual ~OdRxModelTreeBaseNodeImpl();

  virtual OdString getNodeName() const;
  virtual OdUInt64 getUniqueSourceID() const;
  virtual HierarchyTreeObjectType getNodeType() const;

  virtual void setNodeName(const OdString& name);
  virtual void setUniqueSourceID(const OdUInt64 id);
  virtual void setNodeType(const HierarchyTreeObjectType type);

  virtual OdRxModelTreeBaseNodePtrArray getParents() const;
  virtual OdRxModelTreeBaseNodePtrArray getChildren() const;

  virtual bool addChild(OdRxModelTreeBaseNode* parent, OdRxModelTreeBaseNodePtr elem);

  virtual void outFields(OdBaseHierarchyTreeFiler* pFiler) const;

  virtual void serializeProprties(OdBaseHierarchyTreeFiler* pFiler, const PropertyInfo& info) const;

  virtual OdResult inFields(OdBaseHierarchyTreeFiler* pFiler);

  virtual void deserializeProprties(OdBaseHierarchyTreeFiler* pFiler, PropertyInfo& info);

  virtual OdArray<PropertyInfo> getProperties() const;

  virtual void addProperty(PropertyInfo& info);

protected:

  OdString                             m_Name;
  OdUInt64                             m_UniqueSourceID;
  OdRxModelTreeBaseNodePtrArray        m_Parents;
  OdRxModelTreeBaseNodePtrArray        m_Children;
  HierarchyTreeObjectType              m_NodeType;
  OdArray<PropertyInfo>                m_Properties;
};

#endif
