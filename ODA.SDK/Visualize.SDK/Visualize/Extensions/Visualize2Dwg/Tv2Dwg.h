/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef TVDATABASECOPIER_H
#define TVDATABASECOPIER_H

#include "TD_PackPush.h"

#include "RxObject.h"
#include "DbHostAppServices.h"

// Visualize
#include "TvDatabase.h"
#include "TvMaterialsTraits.h"

#include "DbBaseDatabase.h"
#include "DbDatabase.h"
#include "GiMaterial.h"
#include "CmColor.h"
#include "GiLightTraits.h"

/** \details
This class is service for creating database for dwg file
*/
class OdTvDwgService : public OdDbHostAppServices2
{
  virtual OdHatchPatternManager* patternManager() { return NULL; }
};

class OdTv2DwgCopier
{
public:
  OdTv2DwgCopier(const OdTvDatabaseId& tvDbId);
  OdDbDatabasePtr copyDatabase();

  OdDbObjectId getNewBdObjectIdFromOldBdObjectId(const OdDbObjectId& oldId) const;
  OdDbObjectId getRasterImageDbIdFrmPath(OdString sFilePath);

  OdDbDatabasePtr getCopiedDatabasae() const { return m_pDb; };
protected:

  void copyMaterials(OdDbDatabasePtr& pDb);
  void copyLinetypes(OdDbDatabasePtr& pDb);
  void copyRasterImages(OdDbDatabasePtr& pDb);
  void copyTextStyles(OdDbDatabasePtr& pDb);
  void copyBackground(OdDbDatabasePtr& pDb);
  void copyVisualStyles(OdDbDatabasePtr& pDb);
  void copyLayers(OdDbDatabasePtr& pDb);
  void copyViews(OdDbDatabasePtr& pDb);
  void copyBlocks(OdDbDatabasePtr& pDb);
  void copyModels(OdDbDatabasePtr& pDb);

  OdDbObjectId getNewLinetype(const OdTvLinetypeDef& linetypeTvDef);

  static OdGiMaterialColor getOdGiMaterialColorFromOdTvMaterialColor(const OdTvMaterialColor& materialColor);
  static OdGiMaterialMap getOdGiMaterialMapFromOdTvMaterialMap(const OdTvMaterialMap& materialMap);

  void copyEntity(OdTvEntityId entityTvId, OdDbBlockTableRecordPtr& pDbBlockTableRecordPtr, OdDbDatabasePtr& pDb);

  static void fillLineType(OdDbLinetypeTableRecordPtr pLinetype, OdInt32 nElements, const OdTvLinetypeElementPtr* elements);

  static OdCmEntityColor getTvCmEntityColorFromDef(const OdTvColorDef& color);
  static OdGiMapper getGiMapperFromTvMapper(const OdTvTextureMapper& tvMapper);
  static OdCmColor getCmColorFromDef(const OdTvColorDef& color);
  static OdCmEntityColor getCmEntityColorFromDef(const OdTvColorDef& color);
  static OdCmTransparency getCmTransparencyFromDef(const OdTvTransparencyDef& transparency);
  static OdDb::LineWeight getOdDbLineWeightFromDef(const OdTvLineWeightDef& lw);
  static OdCmColor getOdCmColorFromOdCmEntityColor(const OdCmEntityColor cmEntityColor);
  static OdGiDrawable::DrawableType getOdGiDrawableTypeFromOdTvLightType(OdTvLight::LightType tvType);
  static OdGiLightAttenuation getOdGiLightAttenuationFromOdTv(const OdTvLightAttenuation& lightAttenuation);
  static OdString getPredefinedLinetypeName(OdTvLinetype::Predefined lt);
  static OdDbObjectId getDbIdFromLayer(const OdTvLayerDef& layer, OdDbDatabase* pDb);
private:
  OdTvDatabaseId m_tvDbId;
  OdDbDatabasePtr m_pDb;
  //map for connection of tv database handle and database id
  std::map <OdUInt64, OdDbObjectId> m_databasesIdMap;
  // map for connection of images path and raster image db id
  std::map <OdString, OdDbObjectId> m_rasterImagesMap;
  OdStaticRxObject<OdTvDwgService> m_svcs;

};

#include "TD_PackPop.h"

#endif // TVDATABASECOPIER_H
