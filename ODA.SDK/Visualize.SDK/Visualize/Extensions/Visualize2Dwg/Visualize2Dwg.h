/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef VISUALIZE2DWG_H
#define VISUALIZE2DWG_H

#include "TD_PackPush.h"
#include "RxDispatchImpl.h"

#include "TvExport.h"
#include "DbBaseDatabase.h"

#include "GiDrawObjectForExplodeTv.h"

#define  STL_USING_STACK
#include "OdaSTL.h"

#include "Tv2Dwg.h"

class OdGiDrawObjectForExplodeGeometryTv;
class OdTvVisualize2DwgFilerModule;

/** \details
This class implements the properties of the exported to dwg
*/
class OdTvVisualize2DwgExportProperties : public OdRxDispatchImpl<>
{

public:
  OdTvVisualize2DwgExportProperties();
  virtual ~OdTvVisualize2DwgExportProperties();

  ODRX_DECLARE_DYNAMIC_PROPERTY_MAP(OdTvVisualizeDwgFilerProperties);
  static OdRxDictionaryPtr createObject();

  void setEntityForExplode(OdIntPtr pEntity);
  OdIntPtr getEntityForExplode() const;

  void setEntitySet(OdIntPtr pEntity);
  OdIntPtr getEntitySet() const;
  OdRxObjectPtrArray* getEntitySetObject() const;

  void setView(OdIntPtr view);
  OdIntPtr getView() const;
  OdTvGsViewId getViewTv() const;

  void setModel(OdIntPtr model);
  OdIntPtr getModel() const;
  OdTvModelId getModelTv() const;

  void setExportCallback(OdIntPtr pCallback);
  OdIntPtr getExportCallback() const;

protected:
  OdDbEntity* m_pEntity;                 // Entity to export
  OdRxObjectPtrArray* m_pEntitySet;      // Entity set
  OdTvGsViewId m_viewId;
  OdTvModelId m_modelId;
  OdTvExportCallbackFunction m_pCallback;// Callback for export
};
typedef OdSmartPtr<OdTvVisualize2DwgExportProperties> OdTvVisualize2DwgExportPropertiesPtr;


/** \details
This class is dwg exporter (from the Visualize database)
*/
class OdTvVisualize2DwgExport : public OdTvVisualizeExport
{
public:
  OdTvVisualize2DwgExport();
  ~OdTvVisualize2DwgExport();

  OdTvResult exportTo(const OdTvDatabaseId& tvDbId, const OdString& fileName) const;
  OdRxDictionaryPtr properties() { return m_properties; }

  //callback for calling from TV database
  static void exportCallback(OdTvVisualizeExport* pVisualizeExport);

private:
  void explode();

private:

  // dwg export properties
  OdTvVisualize2DwgExportPropertiesPtr m_properties;
  
  //draw object for the current entity
  OdGiDrawObjectForExplodeGeometryTv* m_pDrawObject;

  // copier object for database copying
  OdTv2DwgCopier* m_pDatabaseCopier;

  //stack for storing drawing objects of the parent entities
  std::stack<OdGiDrawObjectForExplodeGeometryTv*> m_parentsDrawObjects;
};


/** \details
This class is dwg exporter module implementation
*/
class OdTvVisualize2DwgFilerModule : public OdTvVisualizeExportModule
{
public:
  OdTvVisualizeExportPtr getVisualizeExporter() const;

  void initApp();
  void uninitApp();
};

#include "TD_PackPop.h"

#endif // VISUALIZE2DWG_H
