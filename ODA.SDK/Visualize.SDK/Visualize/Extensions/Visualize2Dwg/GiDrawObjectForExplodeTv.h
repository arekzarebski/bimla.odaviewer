/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef ODGIDRAWOBJECTFOREXPLODEGEOMETRY_H
#define ODGIDRAWOBJECTFOREXPLODEGEOMETRY_H

#include "TD_PackPush.h"

#include "GiDrawObjectForExplode.h"

#include "Tv2Dwg.h"

class OdGiDrawObjectForExplodeGeometryTv : public OdGiDrawObjectForExplode
{
protected:
  ODRX_USING_HEAP_OPERATORS(OdGiDrawObjectForExplode);
public:
  // OdGiDrawObjectForExplodeTv overrides
  //
  virtual OdDbObjectId getStyleForDbText(const OdGiTextStyle& textStyle);
  virtual void draw(const OdGiDrawable* pDrawable);

  virtual void polyline(OdInt32 nbPoints,
    const OdGePoint3d* pVertexList,
    const OdGeVector3d* pNormal = NULL,
    OdGsMarker lBaseSubEntMarker = -1);
  virtual void pline(const OdGiPolyline& lwBuf, OdUInt32 fromIndex = 0, OdUInt32 numSegs = 0) ODRX_OVERRIDE;
  // Explodes the text() primitive as a single OdDbMText entity
  virtual void text(
    const OdGePoint3d& position,
    const OdGeVector3d& normal,
    const OdGeVector3d& direction,
    const OdChar* msg,
    OdInt32 length,
    bool raw,
    const OdGiTextStyle* pTextStyle);
  virtual void nurbs(const OdGeNurbCurve3d& nurbsCurve);

  virtual void ellipArc(const OdGeEllipArc3d& ellipArc,
    const OdGePoint3d* endPointsOverrides = 0,
    OdGiArcType arcType = kOdGiArcSimple);

  virtual void circularArcProc(
    const OdGePoint3d& center,
    double radius,
    const OdGeVector3d& normal,
    const OdGeVector3d& startVector,
    double sweepAngle,
    OdGiArcType arcType = kOdGiArcSimple, const OdGeVector3d* pExtrusion = 0);

  virtual void meshProc(OdInt32 rows,
    OdInt32 columns,
    const OdGePoint3d* pVertexList,
    const OdGiEdgeData* pEdgeData = NULL,
    const OdGiFaceData* pFaceData = NULL,
    const OdGiVertexData* pVertexData = NULL);

  virtual void shellProc(OdInt32 nbVertex,
    const OdGePoint3d* pVertexList,
    OdInt32 faceListSize,
    const OdInt32* pFaceList,
    const OdGiEdgeData* pEdgeData = NULL,
    const OdGiFaceData* pFaceData = NULL,
    const OdGiVertexData* pVertexData = NULL);

  virtual void rasterImageProc(const OdGePoint3d& origin,
    const OdGeVector3d& u,
    const OdGeVector3d& v,
    const OdGiRasterImage* pImage,
    const OdGePoint2d* uvBoundary,
    OdUInt32 numBoundPts,
    bool transparency = false,
    double brightness = 50.0,
    double contrast = 50.0,
    double fade = 0.0);

  // #10543 : explodeGeometry() must take clip boundary into account
  virtual void pushClipBoundary(OdGiClipBoundary* pBoundary);
  virtual void pushClipBoundary(OdGiClipBoundary* pBoundary, OdGiAbstractClipBoundary* pClipInfo);
  virtual void popClipBoundary();

  // Own methods
  void applyTraits(OdGiSubEntityTraits& traits);

  void setDatabaseCopier(OdTv2DwgCopier* pDatabaseCopier) { m_pDatabaseCopier = pDatabaseCopier; };
protected:
  virtual bool isClipping() const;

  void addEntity(OdDbEntityPtr pEnt, bool needTransform = false);

  void generateFacesArray(OdInt32Array& facesList, OdInt32 rows, OdInt32 columns) const;

  void applyTraitsFromDatabasaeCopier();

  OdDbEntityPtr makePolyline(OdInt32 nbPoints, const OdGePoint3d* pVertexList, const OdGeVector3d* pNormal);

  bool setCurrTraitsTo(OdDbEntity* pEnt);

protected:
  OdTv2DwgCopier* m_pDatabaseCopier;
};


#include "TD_PackPop.h"

#endif // ODGIDRAWOBJECTFOREXPLODEGEOMETRY_H
