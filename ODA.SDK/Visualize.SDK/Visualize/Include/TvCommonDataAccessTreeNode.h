/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODTV_COMMONDATAACCESS_TREE_NODE_H_INCLUDED_ 
#define _ODTV_COMMONDATAACCESS_TREE_NODE_H_INCLUDED_

#include "Tv.h"
#include "RxModelTreeBaseNode.h"
#include "SmartPtr.h"
#include "TvEntity.h"
#include "TvSelection.h"

class OdTvCDATreeNode;

/** \details
A data type that represents a smart pointer to an <link OdTvCDATreeNode, OdTvCDATreeNode> object.
*/
typedef OdSmartPtr<OdTvCDATreeNode> OdTvCDATreeNodePtr;

/** \details
A data type that represents an array of smart pointers to an<link OdTvCDATreeNode, OdTvCDATreeNode> object.
*/
typedef OdArray<OdTvCDATreeNodePtr> OdTvCDATreeNodePtrArray;

/** \details
This class implements the hierarchy tree node for the Common Data Access mechanism specific for using with Visualize SDK
This implementation provides a link between hierarchy tree node and the appropriate Visualize entity
*/
class ODTV_EXPORT OdTvCDATreeNode : public OdRxModelTreeBaseNode
{
public:
  ODRX_DECLARE_MEMBERS(OdTvCDATreeNode);
  ODRX_HEAP_OPERATORS();

  /** \details
  Node options.
  */
  enum Options
  {
    kInvisible  = 1, // Node should be invisible.
    kExploded   = 2  // Node should be exploded.
  };

  /** \details
  Writes the .vsf file data of this object.

  \param pFiler [in]  Pointer to the filer to which data are written.
  */
  void outFields(OdBaseHierarchyTreeFiler* pFiler) const;

  /** \details
  Reads the .vsf file data of this object.

  \param pFiler [in]  Filer object from which data are read.
  */
  OdResult inFields(OdBaseHierarchyTreeFiler* pFiler);

  /** \details
  Returns the tv entity from this node.
  \returns Returns a value of <link OdTvEntityId, OdTvEntityId> Visualize entity id.
  */
  virtual OdTvEntityId getTvEntityId() const;

  /** \details
  Set the tv entity to this tree node.
  \param id  [in] A Visualize entity id.
  \remarks
  If the specified entity was successfully set, the method returns tvOk; otherwise it returns an appropriate error code.
  The node should NOT have 'HierarchyType = eGeometry', otherwise it returns an error code.
  */
  virtual OdTvResult setTvEntityId(OdTvEntityId id);

  /** \details
  Returns the selection set, which contains the visualize entities from this and all children nodes
  \param parentsPath [in] Pointer to an array that contains the parents nodes from the first parent to the top parent database node
  \returns Returns a smart pointer to the <link OdTvSelectionSet, selection set object>.
  \remarks
  parentsPath can be null in most cases since this parameter make sense in the case of blocks/insertions when one node can have multiple parents
  */
  virtual OdTvSelectionSetPtr select(const OdTvCDATreeNodePtrArray* parentsPath = NULL);

  /** \details
  Isolate node with all children.
  \returns Returns a value of <link OdTvResult, OdTvResult> type that contains the result of the operation.
  */
  virtual OdTvResult isolate();

  /** \details
  Hide node with all children.
  \returns Returns a value of <link OdTvResult, OdTvResult> type that contains the result of the operation.
  */
  virtual OdTvResult hide();

  /** \details
  Add the property to the property's cache
  \param info  [in] Property info for adding
  \remarks
  If the property was successfully added, the method returns tvOk; otherwise it returns an appropriate error code.
  */
  virtual OdTvResult addProperty(PropertyInfo& info);

  /** \details
  Set the visibility to this tree node.
  \param bInvisible  [in] A visibility flag.
  */
  virtual void setInvisible(bool bInvisible);

  /** \details
  Returns the visibility for this node.
  */
  virtual bool getInvisible() const;

  /** \details
  Set that the current node should be exploded.
  \param bIsExploded  [in] A explod flag.
  */
  virtual void setExploded(bool bExploded);

  /** \details
  Returns the explode flag for this node.
  */
  virtual bool getExploded() const;

  /** \details
  Set the name for node.
  \param name  [in] New name for the node.
  */
  virtual void setNodeName(const OdString& name);

  /** \details
  Set that the current node has viewport.
  \param viewportId  [in] A viewport id.
  */
  virtual void setViewportObject(const OdTvGsViewId& viewportId);

  /** \details
  Returns the viewport id.
  */
  virtual OdTvGsViewId getViewportObject() const;

  /** \details
  Set that the current node has sibling node.
  \param pNode  [in] A sibling node.
  */
  virtual void setSiblingNode(const OdTvCDATreeNodePtr& pNode);

  /** \details
  Returns the sibling node.
  */
  virtual OdTvCDATreeNodePtr getSiblingNode() const;

  /** \details
  Returns the tv model from this node.
  \returns Returns a value of <link OdTvModelId, OdTvModelId> Visualize model id.
  */
  virtual OdTvModelId getTvModelId() const;

  /** \details
  Set the tv model to this tree node.
  \param id  [in] A Visualize model id.
  */
  virtual void setTvModelId(const OdTvModelId& id);

protected:
  /** \details
  Constructor
  */
  OdTvCDATreeNode();
};

#endif //_ODTV_COMMONDATAACCESS_TREE_NODE_H_INCLUDED_

