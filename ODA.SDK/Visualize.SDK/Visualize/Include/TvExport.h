/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODTV_EXPORT_H_INCLUDED_
#define _ODTV_EXPORT_H_INCLUDED_

#include "RxModule.h"
#include "SharedPtr.h"

#include "TD_PackPush.h"
#include "TvDatabase.h"


/** \details
This class implements the base Visualize SDK export parameters.
*/
class ODTV_EXPORT OdTvBaseExportParams
{
public:

  /** \details
  Creates a new set of export parameters with default values.
  */
  OdTvBaseExportParams(){};

  /** \details
  Destroys the set of export parameters.
  */
  virtual ~OdTvBaseExportParams() {}

  /** \details
  Sets a new file path for the export output.
  \param filePath [in] A string that contains the output file path.
  */
  void setFilePath(const OdString& filePath) { m_strFilePath = filePath; }

  /** \details
  Retrieves the file path for export output.
  \returns Returns the output file full path.
  */
  OdString getFilePath() const { return m_strFilePath; }

//DOM-IGNORE-BEGIN
protected:
  // common properties
  OdString  m_strFilePath;            // File path for export with right extension
//DOM-IGNORE-END
};


/** \details
This class implements the Visualize SDK parameters for exporting to PDF (2D or 3D) format.
*/
class ODTV_EXPORT OdTvPdfExportParams : public OdTvBaseExportParams
{
public:

  /** \details
  Devices for the bitmap generation
  */
  enum Device
  {
    kOpenGL     = 0,    // OpenGL ES2 device.
    kOpenGLES2  = 1,    // OpenGL device.
    kBitmap     = 2     // Bitmap device.
  };

  /** \details
  Creates a new set of PDF export parameters with default values.
  */
  OdTvPdfExportParams() : m_bIs3DPdf(false)
    , m_backgroundColor(ODRGB(255, 255, 255)), m_bZoomToExtents(true), m_deviceForBitmap(kOpenGLES2) { }

  /** \details
  Destroys the set of PDF export parameters.
  */
  virtual ~OdTvPdfExportParams() {}

  /** \details
  Sets the new value of the 3D PDF export flag. The 3D flag value determines whether 3D PDF export should be performed.
  \param bIs3DPdf [in] A new value of the 3D PDF export flag.
  */
  void set3DPdf(bool bIs3DPdf) 
  { 
    m_bIs3DPdf = bIs3DPdf; 
  }

  /** \details
  Retrieves the current value of the 3D PDF export flag.
  \return The method returns true if the drawing should be exported to the 3D PDF format, otherwise returns false.
  */
  bool is3DPdf() const 
  { 
    return m_bIs3DPdf; 
  }

  /** \details
  Sets the new value of background color.
  */
  void setBackgroundColor(ODCOLORREF backgroundColor)
  {
    m_backgroundColor = backgroundColor;
  }

  /** \details
  Retrieves the current value of background color.
  */
  ODCOLORREF getBackgroundColor() const
  {
    return m_backgroundColor;
  }

  /** \details
  Sets devices to export.
  */
  void setDevices(OdStringArray devices)
  {
    m_devices = devices;
  }

  /** \details
  Retrieves array of devices for export.
  */
  OdStringArray& getDevices()
  {
    return m_devices;
  }
  OdStringArray getDevices() const
  {
    return m_devices;
  }

  /** \details
  Sets paper sizes of devices to export.
  \remarks
  Width and height are written consistently: for i device width is in 2*i position and height is on 2*i+1 position.
  */
  void setPaperSizes(OdDoubleArray paperSizes)
  {
    m_paperSizes = paperSizes;
  }

  /** \details
  Retrieves array of paper sizes of devices for export.
  \remarks
  Width and height are written consistently: for i device width is in 2*i position and height is on 2*i+1 position.
  */
  OdDoubleArray& getPaperSizes()
  {
    return m_paperSizes;
  }
  OdDoubleArray getPaperSizes() const
  {
    return m_paperSizes;
  }

  /** \details
  Sets the new flag if need zoom to extents.
  */
  void setZoomToExtents(bool bVal)
  {
    m_bZoomToExtents = bVal;
  }

  /** \details
  Retrieves flag if need zoom to extents.
  */
  bool getZoomToExtents() const
  {
    return m_bZoomToExtents;
  }

  /** \details
  Sets the device for the bitmap generation

  \param device          [in] Device which should be used for the bitmap generation
  */
  void setDeviceForBitmap(Device device)
  {
    m_deviceForBitmap = device;
  }

  /** \details
  Retrieves the device for the bitmap generation

  \returns the device which will be used for the bitmap generation
  */
  Device getDeviceForBitmap() const
  {
    return m_deviceForBitmap;
  }

//DOM-IGNORE-BEGIN
protected:
  bool                        m_bIs3DPdf;           // Flag indicates what action should be performed: export to 2D PDF or publish to 3D PDF
  ODCOLORREF                  m_backgroundColor;    // Background color. Default: white.
  OdStringArray               m_devices;            // Devices to export
  OdDoubleArray               m_paperSizes;         // Paper sizes
  bool                        m_bZoomToExtents;     // Need zoom to extents
  Device                      m_deviceForBitmap;    // Device for bitmap generation
//DOM-IGNORE-END
};

/** \details
This class implements the Visualize SDK parameters for exporting to .dwg.
*/
class ODTV_EXPORT OdTvDwgExportParams : public OdTvBaseExportParams
{
public:

  /** \details
  Creates a new set of export parameters.
  */
  OdTvDwgExportParams() { }

  /** \details
  Destroys the set of export parameters.
  */
  virtual ~OdTvDwgExportParams() {}

  /** \details
  Sets a new view to export.
  \param viewId [in] An export view's identifier.
  */
  void setView(OdTvGsViewId viewId) { m_viewId = viewId; }

  /** \details
  Retrieves the view for the export operation.
  \returns Returns the identifier of the view to export.
  */
  OdTvGsViewId getView() const { return m_viewId; }

  /** \details
  Sets a new model to export.
  \param modelId [in] An export model's identifier.
  */
  void setModel(OdTvModelId modelId) { m_modelId = modelId; }

  /** \details
  Retrieves the model for the export operation.
  \returns Returns the identifier of the model to export.
  */
  OdTvModelId getModel() const { return m_modelId; }

//DOM-IGNORE-BEGIN
protected:
  OdTvGsViewId m_viewId;
  OdTvModelId m_modelId;
//DOM-IGNORE-END
};

/** \details
This class implements the Visualize SDK parameters for exporting to .xml or .tgs.
*/
class ODTV_EXPORT OdTvXmlExportParams : public OdTvBaseExportParams
{
public:

  /** \details
  Creates a new set of export parameters.
  */
  OdTvXmlExportParams() : m_bComposite(true), m_bBinary(false) { }

  /** \details
  Destroys the set of export parameters.
  */
  virtual ~OdTvXmlExportParams() {}

  /** \details
  Sets a device for export.
  \param deviceId [in] An export device's identifier.
  */
  void setDevice(OdTvGsDeviceId deviceId) { m_deviceId = deviceId; }

  /** \details
  Retrieves the device for the export operation.
  \returns Returns the identifier of the device to export.
  */
  OdTvGsDeviceId getDevice() const { return m_deviceId; }

  /** \details
  Sets the new value of the binary flag. The binary flag value determines whether the export to the binary XML (TGS) should be performed.
  \param bIsBinary [in] A new value of the binary flag.
  */
  void setIsBinary(bool bIsBinary) { m_bBinary = bIsBinary; }

  /** \details
  Retrieves the current value of the binary flag.
  \return The method returns true if the data should be exported to the binary XML (TGS), otherwise returns false.
  */
  bool getIsBinary() const { return m_bBinary; }

  /** \details
  Sets the new value of the composite flag. The composite flag value determines whether the composite metafiles should be exported.
  \param bIsComposite [in] A new value of the composite flag.
  */
  void setIsComposite(bool bIsComposite) { m_bComposite = bIsComposite; }

  /** \details
  Retrieves the current value of the composite flag.
  \return The method returns true if the composite metafiles should be exported, otherwise returns false.
  */
  bool getIsComposite() const { return m_bComposite; }

  //DOM-IGNORE-BEGIN
protected:
  OdTvGsDeviceId m_deviceId;
  bool m_bBinary;
  bool m_bComposite;
  //DOM-IGNORE-END
};

/** \details
This class provides the common interface for the export operations.
*/
class ODTV_EXPORT OdTvVisualizeExport
{
public:

  /** \details
  Creates an instance of the Visualize SDK export functionality.
  */
  OdTvVisualizeExport() {};

  /** \details
  Destroys an instance of the Visualize SDK export functionality.
  */
  virtual ~OdTvVisualizeExport() {}

  /** \details
  Retrieves the set of export process properties.
  \returns Returns a smart pointer to the instance of the OdRxDictionary object that encapsulates export process properties.
  */
  virtual OdRxDictionaryPtr properties() = 0;

  /** \details
  Exports the Visualize database to the specified file.

  \param tvDbId   [in]  A Visualize database to export.
  \param fileName [in]  A full path to the output file.
  \returns Returns the result of the export operation represented with an <link OdTvResult, OdTvResult> value.
  */
  virtual OdTvResult exportTo(const OdTvDatabaseId& tvDbId, const OdString& fileName) const = 0;
};

/** \details
  A data type that represents a smart pointer to an <link OdTvVisualizeExport, OdTvVisualizeExport> object.
*/
typedef OdSharedPtr<OdTvVisualizeExport> OdTvVisualizeExportPtr;


/** \details
This class provides an interface for the module that exports data from a Visualize database.
*/
class ODTV_EXPORT OdTvVisualizeExportModule : public OdRxModule
{
public:

  /** \details
  Returns the object that exports a Visualize database to a specified file.
  \returns Returns a smart pointer to an <link OdTvVisualizeExport, OdTvVisualizeExport> object that implements the export functionality.
  */
  virtual OdTvVisualizeExportPtr getVisualizeExporter() const = 0;
};

/** \details
A data type that represents a smart pointer to an <link OdTvVisualizeExportModule, OdTvVisualizeExportModule> object.
*/
typedef OdSmartPtr<OdTvVisualizeExportModule> OdTvVisualizeExportModulePtr;

/** \details
  A data type that represents a pointer to the export callback function for Visualize SDK.
*/
typedef void(*OdTvExportCallbackFunction)(OdTvVisualizeExport*);

#include "TD_PackPop.h"

#endif // _ODTV_EXPORT_H_INCLUDED_
