
/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODTV_IMPORTUSERDATA_H_INCLUDED_
#define _ODTV_IMPORTUSERDATA_H_INCLUDED_

#include "TD_PackPush.h"

#include "OdaCommon.h"
#include "OdStreamBuf.h"

#include "Ge/GePoint2d.h"
#include "DbPlotSettings.h"
#include "Tv.h"

#define OdTvDWGUserData         L"Dwg2Visualize - ODA"

/** \details
The structure for storing the DWG specific layout parameters, which can be used during export through PE
*/
struct ODTV_EXPORT OdTvDwgLayoutData
{
public:

  OdString  m_strPaperName;
  OdUInt16  m_iPaperUnits;

  double    m_dPrinterWidth;
  double    m_dPrinterHeight;

  double      m_dOffsetX;
  double      m_dOffsetY;
  OdGePoint2d m_paperImageOrigin;

  double m_dLeftMargin;
  double m_dRightMargin;
  double m_dTopMargin;
  double m_dBottomMargin;

  OdDbPlotSettings::PlotType m_plotType;
  OdDbPlotSettings::PlotRotation m_plotRot;

  //scale parameters
  double  m_dStdScale;
  double  m_dCustomScaleNumerator;
  double  m_dCustomScaleDenominator;

  // Corners of the plot window area
  double m_dXmin;  // coordinate of the lower - left corner.
  double m_dYmin;  // coordinate of the lower - left corner.
  double m_dXmax;  // coordinate of the upper - right corner.
  double m_dYmax;  // coordinate of the upper - right corner.

  // shading mode for plot
  OdInt16   m_shadePlot;


  enum Properties
  {
    kModelSpace = (1 << 0),
    kScaledToFit = (1 << 1),
    kCentered = (1 << 2),
    kUseStandardScale = (1 << 3), 
    kPrintLineweights = (1 << 4),
    kPlotHidden = (1 << 5),
    kScaleLineWeights = (1 << 6),
    kPlotPlotstyles = (1 << 7)
  };

  OdUInt16    m_PlotLayoutFlags;
  OdString    m_strCurStyleSheet;

  OdTvDwgLayoutData();
  void read(OdStreamBuf* strm);
  void write(OdStreamBuf* strm);
};

/** \details
The structure for storing the DWG specific view parameters, which can be used during export through PE
*/
struct ODTV_EXPORT OdTvDwgViewData
{
  enum Type
  {
    kModel = 0,               // models space view
    kUnderlayingPaper = 1,    // underlaying view with paper
    kPaper = 2,               // main paper space view
    kViewport = 3             // paper space viewports 
  };

  Type        m_type;
  OdGePoint3d m_target;       // doesn't used for the m_type = kViewport
  OdInt16     m_shadePlot;    // shading mode for plot (used only for the m_type = kViewport)
  bool        m_bPlotHidden;  // use hidden for plot  (used only for the m_type = kViewport)

  OdTvDwgViewData();

  void read(OdStreamBuf* strm);
  void write(OdStreamBuf* strm);
};

/** \details
The structure for storing the DWG specific database parameters, which can be used during export through PE
*/
struct ODTV_EXPORT OdTvDwgDbData
{
  enum Properties
  {
    kDisplaySilhouettes = (1 << 0),
  };

  OdUInt16    m_prop;
  OdUInt16    m_iMeasurement;

  OdTvDwgDbData();

  void read(OdStreamBuf* strm);
  void write(OdStreamBuf* strm);
};



#include "TD_PackPop.h"

#endif // _ODTV_IMPORTUSERDATA_H_INCLUDED_
