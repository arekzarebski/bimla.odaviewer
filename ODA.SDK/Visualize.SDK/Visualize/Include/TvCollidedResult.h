/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODTV_COLLIDEDRESULT_H_INCLUDED_
#define _ODTV_COLLIDEDRESULT_H_INCLUDED_

#include "Tv.h"
#include "TvIObject.h"
#include "TvEntity.h"
#include "TvSelection.h"

class OdTvCollidedResult;

/** \details
A data type that represents a smart pointer to an <link OdTvCollidedResult, OdTvCollidedResult> object.
*/
typedef OdTvSmartPtr<OdTvCollidedResult> OdTvCollidedResultPtr;

/** \details
The abstract interface class for managing a set of collided Visualize SDK objects.
*/
class ODTV_EXPORT OdTvCollidedResult : public OdTvIObject
{
public:

  /** \details
  Creates a new collided objects set with specified selection level.
  \param level [in] An <link OdTvCollidedResult::Level, OdTvCollidedResult::Level> enum that contains selextion level.
  \returns Returns a smart pointer to the created collided objects set.
  */
  static OdTvCollidedResultPtr createObject(const OdTvSelectionOptions::Level level);

  /** \details
  Retrieves the iterator for getting access to items that are contained in the collided result object.
  \param rc [out] A pointer to a value of <link OdTvResult, OdTvResult> type that contains the result of the retrieve operation.
  \returns Returns a smart pointer to the <link OdTvSelectionSetIteratorPtr, iterator> object.
  \remarks
  If the rc parameter is not null and the iterator object was successfully returned, the rc parameter accepts the tvOk value; otherwise it contains an appropriate error code.
  */
  virtual OdTvSelectionSetIteratorPtr getIterator(OdTvResult* rc = NULL) const = 0;

  /** \details
  Retrieves the quantity of items that are contained in the collided result.
  \param rc [out] A pointer to a value of <link OdTvResult, OdTvResult> type that contains the result of the retrieve operation.
  \returns Returns the number of items from collided result.
  \remarks
  If the rc parameter is not null and the quantity of selected items was successfully returned, the rc parameter accepts the tvOk value; otherwise it contains an appropriate error code.
  */
  virtual OdUInt32 numItems(OdTvResult* rc = NULL) const = 0;

  /** \details
  Retrieves the current selection level for the collided result.
  \param rc [out] A pointer to a value of <link OdTvResult, OdTvResult> type that contains the result of the retrieve operation.
  \returns Returns an instance of the <link OdTvCollidedResult::Level, OdTvCollidedResult::Level> class that contains the current selection level.
  \remarks
  If the rc parameter is not null and the current selection level were successfully returned, the rc parameter accepts the tvOk value; otherwise it contains an appropriate error code.
  */
  virtual OdTvSelectionOptions::Level getLevel(OdTvResult* rc = NULL) const = 0;

  /** \details
  Removes the entity item from the collided result (all subentity pathes for this entity also will be removed).
  \param id     [in] An identifier of the removed entity.
  \returns Returns a value of <link OdTvResult, OdTvResult> type that contains the result of the operation.
  \remarks
  If the entity item was successfully removed from the collided result, the method returns the tvOk value; otherwise it returns an appropriate error code.
  */
  virtual OdTvResult removeEntity(const OdTvEntityId& id) = 0;

  /** \details
  Removes the sub-entity item (a sub-entity, geometry or sub-geometry entity) with a specified full path from the collided result.
  \param id           [in] An identifier of the removed entity.
  \param subItemPath  [in] A sub-item path object.
  \returns Returns a value of <link OdTvResult, OdTvResult> type that contains the result of the operation.
  \remarks
  If the sub-entity item was successfully removed from the selection set, the method returns the tvOk value; otherwise it returns an appropriate error code.
  */
  virtual OdTvResult removeSubEntity(const OdTvEntityId& id, const OdTvSubItemPath& subItemPath) = 0;

  /** \details
  Removes all members from this collided result.
  \returns Returns a value of <link OdTvResult, OdTvResult> type that contains the result of the operation.
  \remarks
  If the collided result was successfully cleared, the method returns the tvOk value; otherwise it returns an appropriate error code.
  */
  virtual OdTvResult clear() = 0;

};

#endif // _ODTV_COLLIDEDRESULT_H_INCLUDED_
