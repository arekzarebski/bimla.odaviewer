/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _ODTV_IMPORT_H_INCLUDED_
#define _ODTV_IMPORT_H_INCLUDED_

#include "Tv.h"
#include "TvVisualizeFiler.h"
/** \details
This class is an interface for LowMemoryImportAbort
*/
class ODTV_EXPORT OdTvLowMemoryImportAbort
{
public:
  /** \details
  Returns true if and only if low memory import should be aborted.
  */
  virtual bool abortImport() const = 0;
};

/** \details
This class implements the base ODA Visualize SDK import parameters.
*/
class ODTV_EXPORT OdTvBaseImportParams
{
public:

  /** \details
  Creates a new set of import parameters with default values.
  */
  OdTvBaseImportParams();

  /** \details
  Destroys the set of import parameters.
  */
  virtual ~OdTvBaseImportParams() {}

  /** \details
  Sets a new full path to the imported file.
  \param filePath [in] A new file path represented with an <exref target="https://docs.opendesign.com/tkernel/OdString.html">OdString object</exref>.
  */
  void setFilePath(const OdString& filePath);

  /** \details
  Retrieves tbe current full path to the imported file. 
  \returns Returns an <exref target="https://docs.opendesign.com/tkernel/OdString.html">OdString object</exref> that contains the file path for import.
  */
  OdString getFilePath() const;

  /** \details
  Sets a new data stream buffer for the import operation.
  \param pBuffer [in] A pointer to an <exref target="https://docs.opendesign.com/tkernel/OdStreamBuf.html">OdStreamBuf object</exref> that represents a new data stream buffer. 
  */
  void setStreamBuffer(OdStreamBuf* pBuffer);

  /** \details
  Retrieves current data stream buffer that is used for import operations.
  \returns Returns a smart pointer to an <exref target="https://docs.opendesign.com/tkernel/OdStreamBuf.html">OdStreamBuf object</exref> that is used for import operations.
  */
  OdStreamBufPtr getStreamBuffer() const;

  /** \details
  Sets a new object for storing the profiling information during import operations.
  \param pProfilingInfo [in] A pointer to an <link OdTvFilerTimeProfiling, OdTvFilerTimeProfiling> object that stores profiling information.
  */
  void setProfiling(OdTvFilerTimeProfiling* pProfilingInfo);

  /** \details
  Retrieves tbe current object for storing the profiling information during import operations.
  \returns Returns a raw pointer to the object that stores the profiling info.
  */
  OdTvFilerTimeProfiling* getProfiling() const;

  /** \details
  Sets a host app progress meter which will be used for direction the info about import process to the host app
  \param pProgressMeter [in] A pointer to an OdTvHostAppProgressMeter object
  \remarks
  Currently supports only for the low memory import from DWG
  */
  void setProgressMeter(OdTvHostAppProgressMeter* pProgressMeter);

  /** \details
  Retrieves the host app progress meter.
  \returns Returns a raw pointer to the host app progress meter if it was set, NULL otherwise
  */
  OdTvHostAppProgressMeter* getProgressMeter() const;

  /** \details
  Sets whether it is need to create a CDA (Common Data Access) tree during import process
  \param bNeed [in] A boolean flag which is true if the CDA tree is need 
  */
  void setNeedCDATree(bool bNeed);

  /** \details
  Retrieves the flag which indicates whether is it need to create a CDA tree during the import process
  \returns Returns true if it is need to create the CDA tree during the import process
  */
  bool getNeedCDATree() const;

  /** \details
  Sets whether it is need store native properties to CDA (Common Data Access) nodes during import process.
  \param bNeed [in] A boolean flag which is true if storing properties in CDA nodes is need.
  */
  void setNeedCollectPropertiesInCDA(bool bSet);

  /** \details
  Retrieves the flag which indicates whether is it need to store native properties in CDA nodes during the import process.
  \returns Returns true if it is need to create the CDA tree during the import process.
  */
  bool getNeedCollectPropertiesInCDA() const;
  
  /** \details
  Specifies instance of OdTvLowMemoryImportAbort
  */
  void setLowMemoryImportAbort( OdTvLowMemoryImportAbort* );
  /** \details
  Retrieves instance of OdTvLowMemoryImportAbort
  */
  OdTvLowMemoryImportAbort* getLowMemoryImportAbort() const;


//DOM-IGNORE-BEGIN
protected:

  // common properties
  OdString                  m_strFilePath;            // File path for import.
  OdStreamBuf*              m_pStreamBuf;             // Stream buffer for import. Implemented only for PRC.
  OdTvFilerTimeProfiling*   m_pProfilingInfo;         // Profiling info. If NULL, time profiling is switched off.
  OdTvHostAppProgressMeter* m_pProgressMeter;         // Progress meter from app
  bool                      m_bNeedCDATree;           // Indicates that during import it is need to create the CDA tree if it is supported.
  bool                      m_bNeedCollectPropertiesInCDA; // Indicates that during import it is need to store properties in CDA nodes if it is supported.
  OdTvLowMemoryImportAbort* m_pLowMemoryImportAbort;
//DOM-IGNORE-END
};


/** \details
This class implements the base ODA Visualize SDK append parameters.
*/
class ODTV_EXPORT OdTvBaseAppendParams
{
public:

  /** \details
  Creates a new set of append parameters with default values.
  */
  OdTvBaseAppendParams();

  /** \details
  Destroys the set of append parameters.
  */
  virtual ~OdTvBaseAppendParams() {}

  /** \details
  Sets a new transformation <link OdTvMatrix, matrix>. 
  \param matrix [in] A new transformation <link OdTvMatrix, matrix> to be set. 
  */
  void setTransform(const OdTvMatrix& matrix);

  /** \details
  Retrieves tbe current transformation <link OdTvMatrix, matrix>.
  \returns Returns tbe current transformation <link OdTvMatrix, matrix> object. 
  */
  OdTvMatrix getTransform() const;

//DOM-IGNORE-BEGIN
protected:

  // common properties
  OdTvMatrix                m_transform;            // Transform
//DOM-IGNORE-END
};


/** \details
This class implements the base ODA Visualize SDK import parameters for custom file types (non-ODA databases): .rcs, .stl, .obj, etc.
*/
class ODTV_EXPORT OdTvCustomBaseImportParams : public OdTvBaseImportParams
{
public:

  /** \details
  Creates a new set of import parameters with default values.
  */
  OdTvCustomBaseImportParams();

  /** \details
  Destroys the set of import parameters.
  */
  virtual ~OdTvCustomBaseImportParams() {}

  /** \details
  Sets a new value of the default color.
  \param defcolor [in] A new default color value.
  */
  void setDefaultColor(ODCOLORREF defcolor);

  /** \details
  Retrieves the current default color value. 
  \returns Returns the current value of the default color. 
  */
  ODCOLORREF getDefaultColor() const;

//DOM-IGNORE-BEGIN
protected:
  ODCOLORREF m_defaultColor;    // Default entity color.
//DOM-IGNORE-END
};


/** \details
This class implements the ODA Visualize SDK parameters for importing from an .rcs file.
*/
class ODTV_EXPORT OdTvRcsImportParams : public OdTvCustomBaseImportParams
{
public:

  /** \details 
  Creates a new instance of .rcs import parameters. 
  */
  OdTvRcsImportParams() : OdTvCustomBaseImportParams(), m_bIgnorePointSize(false), m_bImportAsRcsPointCloud(true) {};

  /** \details 
  Destroys the instance of .rcs import parameters. 
  */
  virtual ~OdTvRcsImportParams() {}

  /** \details
  Sets a new value of the ignore point size flag. 
  \param bSet [in] A new flag value to be set.
  */
  void setIgnorePointSize(bool bSet);

  /** \details
  Retrieves the current value of the ignore point size flag. 
  \returns Returns the current ignore point size flag value.
  */
  bool getIgnorePointSize() const;

  /** \details
  Sets a new value of the import as rcs point cloud flag.
  \param bSet [in] A new flag value to be set.
  */
  void setImportAsRcsPointCloud(bool bSet);

  /** \details
  Retrieves the current value of the import as rcs point cloud flag.
  \returns Returns the current import as rcs point cloud flag value.
  */
  bool getImportAsRcsPointCloud() const;

//DOM-IGNORE-BEGIN
private:
  bool m_bIgnorePointSize;
  bool m_bImportAsRcsPointCloud;
//DOM-IGNORE-END
};


/** \details
This class implements the ODA Visualize SDK parameters for appending contents of .rcs files.
*/
class ODTV_EXPORT OdTvRcsAppendParams : public OdTvRcsImportParams, public OdTvBaseAppendParams
{
public:

  /** \details
  Creates a new instance of parameters for appending .rcs files.
  */
  OdTvRcsAppendParams() : OdTvRcsImportParams() {};

  /** \details
  Destroys the instance of parameters for appending .rcs files.
  */
  virtual ~OdTvRcsAppendParams() {}
};

/** \details
This class implements the ODA Visualize SDK parameters for importing from an .stl file.
*/
class ODTV_EXPORT OdTvStlImportParams : public OdTvCustomBaseImportParams
{
  
  /** \details
  The .stl import flags. 
  */
  enum ParamFlags
  {
    kFixNormals = 1,                // Fix normals during the import operation.
    kUnifyDuplicatedVertices = 2,   // Unify duplicate vertices during the import operation. 
    kCalcNormalsAtVertices = 4      // Calculate normals at vertices during the import operation.
  };

public:

  /** \details
  Creates a new set of import parameters with default values.
  */
  OdTvStlImportParams();

  /** \details
  Destroys the set of import parameters.
  */
  virtual ~OdTvStlImportParams() {}

  /** \details
  Sets a new value of the fix normals flag. 
  \param bFix [in] A new flag value.
  */
  void setFixNormals(bool bFix);

  /** \details
  Retrieves the current value of the fix normals flag. 
  \returns Returns the fix normals flag value.
  */
  bool getFixNormals() const;

  /** \details
  Sets a new value of the unify duplicate vertices flag.
  \param bUnify [in] A new flag value.
  */
  void setUnifyDuplicatedVertices(bool bUnify);

  /** \details
  Retrieves the current value of the unify duplicate vertices flag.
  \returns Returns the unify duplicate vertices flag.
  */
  bool getUnifyDuplicatedVertices() const;

  /** \details
  Sets a new value of the calculate normals at vertices flag.
  \param bCalc [in] A new flag value. 
  */
  void setCalcNormalsAtVertices(bool bCalc);

  /** \details
  Retrieves the current value of the calculate normals at vertices flag.
  \returns Returns the calculate normals at vertices flag.
  */
  bool getCalcNormalsAtVertices() const;

//DOM-IGNORE-BEGIN
protected:

  OdUInt8 m_flags;
//DOM-IGNORE-END
};


/** \details
This class implements the ODA Visualize SDK parameters for appending contents of .stl files.
*/
class ODTV_EXPORT OdTvStlAppendParams : public OdTvStlImportParams, public OdTvBaseAppendParams
{
public:

  /** \details
  Creates a new set of parameters with default values for appending .stl files.
  */
  OdTvStlAppendParams() : OdTvStlImportParams() {};
  
  /** \details
  Destroys the set of parameters for appending .stl files.
  */
  virtual ~OdTvStlAppendParams() {}
};


/** \details
This class implements the ODA Visualize SDK parameters for importing .obj files.
*/
class ODTV_EXPORT OdTvObjImportParams : public OdTvCustomBaseImportParams
{
//DOM-IGNORE-BEGIN
  bool m_bBrepFlipUVCoordinates;
  bool m_bCalculateNormals;
  bool m_bImportBrepAsBrep;
//DOM-IGNORE-END
  
public:

  /** \details
  Creates a new set of .obj file import parameters with default values.
  */
  OdTvObjImportParams();

  /** \details
  Destroys the set of .obj file import parameters.
  */
  virtual ~OdTvObjImportParams() {}

  /** \details
  Retrieves the current value of the B-Rep flip UV coordinates flag.
  \returns Returns the current flag value. 
  */
  bool getBrepFlipUVCoordinates() const;

  /** \details
  Sets a new value of the B-Rep flip UV coordinates flag.
  \param bBrepFlipUVCoordinates [in] A new flag value.
  */
  void setBrepFlipUVCoordinates(bool bBrepFlipUVCoordinates);

  /** \details
  Retrieves the current value of the import brep as brep flag.
  \returns Returns the current flag value.
  */
  bool getImportBrepAsBrep() const;

  /** \details
  Sets a new value of the import brep as brep flag.
  \param bImportBrepAsBrep [in] A new flag value.
  */
  void setImportBrepAsBrep(bool bImportBrepAsBrep);

  /** \details
  Retrieves the current value of the normals calculation flag.
  \returns Returns the current flag value. 
  */
  bool getCalculateNormals() const;

  /** \details
  Sets a new value of the normals calculation flag.
  \param bCalculateNormals [in] A new flag value.
  */
  void setCalculateNormals(bool bCalculateNormals);

};


/** \details
This class implements the ODA Visualize SDK parameters for appending .obj file content.
*/
class ODTV_EXPORT OdTvObjAppendParams : public OdTvObjImportParams, public OdTvBaseAppendParams
{
public:

  /** \details
  Creates a new set of parameters with default values for appending .obj files.
  */
  OdTvObjAppendParams() : OdTvObjImportParams() {};

  /** \details
  Destroys the set of parameters for appending .obj files.
  */
  virtual ~OdTvObjAppendParams() {}
};


/** \details
This class implements the ODA Visualize SDK parameters for importing .dwg files.
*/
class ODTV_EXPORT OdTvDwgImportParams : public OdTvBaseImportParams
{
  /** \details
  Specific options for the importing from a .dwg drawing.
  */
  enum ParamFlags
  {
    kObjectNaming = 1,          // Gives the names of the Visualize SDK entities according to file object names (for example, AcDbCircle, etc).
    kStoreSource = 2,           // Stores source objects in the user data of the <link OdTvEntity, OdTvEntity> objects. 
    kClearEmptyObjects = 4,     // Cleans empty objects. 
    kMultithreading = 8,        // Enables (or disables) multi-threading mode.
    kUseAdvancedTess = 16,      // Uses facet tessellation.
    kPartialSourceOpen = 32,    // Open source .dwg file in partial mode
    kImportBrepAsBrep = 64,     // import brep as OdTvBrepData
    kConvertIndexedToRGB = 128  // Converted indexed colors to RGB during import
  };

public:

  /** \details
  Creates a new set of .dwg import parameters with default values.
  */
  OdTvDwgImportParams();

  /** \details
  Destroys the set of .dwg import parameters.
  */
  ~OdTvDwgImportParams() {}

  /** \details
  Sets a new value of the background color.
  \param background [in] A new background color.
  */
  void setBackground(ODCOLORREF background);

  /** \details
  Retrieves the current value of the background color. 
  \returns Returns the current value of the background color.
  */
  ODCOLORREF getBackground() const;

  /** \details
  Sets a new palette to be used during the import operation.
  \param pPalette [in]  A pointer to the color array that represents the new palette.
  \remarks
  If the pPalette parameter is NULL, one of two default palettes is used depending on the background color value.
  */
  void setPalette(const ODCOLORREF* pPalette);

  /** \details
  Retrieves the current palette to be used during the import operation.
  \returns Returns a pointer to the color array that represents the current palette.
  \remarks
  If no palette is set (the method returns a NULL pointer), one of two default palettes is used depending on the background color value.
  */
  const ODCOLORREF* getPalette() const;

  /** \details
  Sets a new device rectangle that is used during the import operation.
  \param rect [in]  A new <link OdTvDCRect, rectangle object> to be set.
  \remarks
  If all components of the specified rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  void setDCRect(const OdTvDCRect& rect);

  /** \details
  Retrieves the current device rectangle that is used during the import operation.
  \returns Returns the current device rectangle object.
  \remarks
  If all components of the returned rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  OdTvDCRect  getDCRect() const;

  /** \details
  Sets a new value of the object naming flag.
  \param bSet [in] A new flag value.
  */
  void setObjectNaming(bool bSet);

  /** \details
  Retrieves the current object naming flag value.
  \returns Returns the current value of the object naming flag.
  */
  bool getObjectNaming() const;
  
  /** \details
  Sets a new value of the store source objects flag.
  \param bSet [in] A new flag value.
  */
  void setStoreSourceObjects(bool bSet);

  /** \details
  Retrieves the current value of the store source objects flag.
  \returns Returns the current store source objects flag's value.
  */
  bool getStoreSourceObjects() const;

  /** \details
  Sets a new value of the flag that determines whether empty objects should be cleared.
  \param bSet [in] A new flag value.
  */
  void setClearEmptyObjects(bool bSet);

  /** \details
  Retrieves the current value of the clear empty objects flag.
  \returns Returns true if empty objects should be cleared during the import operation; otherwise the method returns false. 
  */
  bool getClearEmptyObjects() const;

  /** \details
  Sets a new callback function for filtering element selection.
  \param pCallback [in]  A pointer to the <link OdTvFeedbackForChooseCallback, selection function>.
  */
  void setFeedbackForChooseCallback(OdTvFeedbackForChooseCallback pCallback);

  /** \details
  Retrieves the current callback function for element selection.
  \returns Returns a pointer to the current <link OdTvFeedbackForChooseCallback, callback function>.
  */
  OdTvFeedbackForChooseCallback getFeedbackForChooseCallback() const;

  /** \details
  Sets a new value of the multi-threading flag. 
  \param bSet [in] A new value of the flag. 
  */
  void setMultithreading(bool bSet);

  /** \details
  Retrieves the current value of the multi-threading flag.
  \returns Returns true if multi-threading mode is enabled; otherwise the method returns false. 
  */
  bool getMultithreading() const;

  /** \details
  Sets a new quantity of threads for multi-threading mode. 
  \param nThreads [in] A new quantity of threads.
  */
  void setCountOfThreads(OdUInt16 nThreads);

  /** \details
  Retrieves the current quantity of threads for multi-threading mode.
  \returns Returns the current number of threads that can be run at the same time in multi-threading mode.
  */
  OdUInt16 getCountOfThreads() const;

  /** \details
  Sets a new value of the advanced tessellation usage flag.
  \param bSet [in] A new flag value.
  */
  void      setUseAdvancedTess(bool bSet);

  /** \details
  Retrieves the current value of the advanced tessellation usage flag. 
  \returns Returns true if advanced tessellation is used; otherwise the method returns false. 
  */
  bool      getUseAdvancedTess() const;

  /** \details
  Sets a new value of the facet resolution.
  \param dFacetRes [in] A new facet resolution value.
  \remarks 
  The facet resolution value varies from 0.1 to 10.0.
  */
  void      setFacetRes(double dFacetRes);

  /** \details
  Retrieves the current facet resolution value.
  \returns Returns the current facet resolution value between 0.1 and 10.0.
  */
  double    getFacetRes() const;

  /** \details
  Specifies that source file should be open in partial mode
  */
  void setOpenSourcePartial( bool bPartial );

  /** \details
  Returns true if and only if source file open mode is partial
  */
  bool getOpenSourcePartial() const;

  /** \details
  Sets a new value of the import Brep as Brep flag.
  \param bSet [in] A new flag value.
  */
  void setImportBrepAsBrep(bool bImportBrepAsBrep);

  /** \details
  Returns true if and only if brep should be imported as brep
  */
  bool getImportBrepAsBrep() const;

  /** \details
  Specifies that the indexed colors should be converted to RGB during import
  */
  void setConvertIndexedToRGB(bool bNeedConvertIndexedtoRGB);

  /** \details
  Returns true if and only if the indexed colors should be converted to RGB during import
  */
  bool getConvertIndexedToRGB() const;

//DOM-IGNORE-BEGIN
protected:

  ODCOLORREF                     m_background;             // Background color. Default: black.
  const ODCOLORREF*              m_pPalette;               // Palette to be used. If NULL, one of two default palettes is used depending on background color.
  OdTvDCRect                     m_importRect;             // Output rectangle. Used for correct import of some specific objects (camera, OLE images, 3D solids). The normal way to set the output window size.
  OdUInt16                       m_flags;                 // Different options.
  OdUInt16                       m_nThreads;               // Count of threads.
  double                         m_dFACETRES;              // Facet res value (between from 0.01 to 10.0).
  OdTvFeedbackForChooseCallback  m_pCallback;              // Callback for choose.
//DOM-IGNORE-END
};


/** \details
This class implements the ODA Visualize SDK parameters for appending .dwg file content.
*/
class ODTV_EXPORT OdTvDwgAppendParams : public OdTvDwgImportParams, public OdTvBaseAppendParams
{
public:

  /** \details 
  Creates a new set of parameters for appending .dwg file content. 
  */
  OdTvDwgAppendParams() : OdTvDwgImportParams() {};

  /** \details 
  Destroys the set of parameters for appending .dwg files content. 
  */
  virtual ~OdTvDwgAppendParams() {}
};


/** \details
This class implements the ODA Visualize SDK parameters for importing .dgn files.
*/
class ODTV_EXPORT OdTvDgnImportParams : public OdTvBaseImportParams
{
  /** \details
  Specific options of the .dgn import operation. 
  */
  enum ParamFlags
  {
    kObjectNaming = 1,           // Gives the names for the Visualize SDK entities according to the file object names (for example, AcDbCircle, etc).
    kStoreSource = 2,            // Stores source objects in the user data of the <link OdTvEntity, OdTvEntity> objects.
    kClearEmptyObjects = 4,      // Cleans empty objects. 
    kUseIsolinesFor3DObjects = 8 // Use isolines for 3D objects.
  };

public:

  /** \details
  Creates a new set of import parameters with default values.
  */
  OdTvDgnImportParams();

  /** \details
  Destroys the set of import parameters.
  */
  ~OdTvDgnImportParams() {}

  /** \details
  Sets a new device rectangle that is used during the import operation.
  \param rect [in]  A new rectangle object to be set.
  \remarks
  If all components of the rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  void setDCRect(const OdTvDCRect& rect);

  /** \details
  Retrieves the current device rectangle that is used during the import operation.
  \returns Returns the current device rectangle object.
  \remarks
  If all components of the rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  OdTvDCRect  getDCRect() const;

  /** \details
  Sets a new value of the objects naming flag.
  \param bSet [in] A new flag value.
  */
  void setObjectNaming(bool bSet);

  /** \details
  Retrieves the current objects naming flag value.
  \returns Returns the current value of the objects naming flag.
  */
  bool getObjectNaming() const;

  /** \details
  Sets a new value of the store source objects flag.
  \param bSet [in] A new flag value.
  */
  void setStoreSourceObjects(bool bSet);

  /** \details
  Retrieves the current value of the store source objects flag.
  \returns Returns the current store source objects flag's value.
  */
  bool getStoreSourceObjects() const;

  /** \details
  Sets a new callback function for filtering element selection.
  \param pCallback [in]  A pointer to the <link OdTvFeedbackForChooseCallback, selection function>.
  */
  void setFeedbackForChooseCallback(OdTvFeedbackForChooseCallback pCallback);

  /** \details
  Retrieves the current callback function for element selection.
  \returns Returns a pointer to the current <link OdTvFeedbackForChooseCallback, callback function>.
  */
  OdTvFeedbackForChooseCallback getFeedbackForChooseCallback() const;

  /** \details
  Sets a new value of the flag that determines whether empty objects should be cleared.
  \param bSet [in] A new flag value.
  */
  void setClearEmptyObjects(bool bSet);

  /** \details
  Retrieves the current value of the clear empty objects flag.
  \returns Returns true if empty objects should be cleared during the import operation; otherwise the method returns false. 
  */
  bool getClearEmptyObjects() const;

  /** \details
  Sets a new value of the flag that determines use isolines for 3D objects.
  \param bSet [in] A new flag value.
  */
  void setUseIsolinesFor3DObjects(bool bSet);

  /** \details
  Retrieves the current value of the use isolines for 3D objects flag.
  \returns Returns true if isolines for 3D objects used during the import operation; otherwise the method returns false.
  */
  bool getUseIsolinesFor3DObjects() const;

//DOM-IGNORE-BEGIN
protected:

  OdTvDCRect                     m_importRect;             // Output rectangle. Used for correct import of some specific objects. The normal way to set the output window size.
  OdUInt8                        m_flags;                  // Different options.
  OdTvFeedbackForChooseCallback  m_pCallback;              // Callback for choose.
//DOM-IGNORE-END
};


/** \details
This class implements the ODA Visualize SDK parameters for appending .dgn files content.
*/
class ODTV_EXPORT OdTvDgnAppendParams : public OdTvDgnImportParams, public OdTvBaseAppendParams
{
public:

  /** \details 
  Creates a new set of parameters for appending .dgn file content. 
  */
  OdTvDgnAppendParams() : OdTvDgnImportParams() {};

  /** \details 
  Destroys the set of parameters for appending .dgn file content. 
  */
  virtual ~OdTvDgnAppendParams() {}
};


/** \details
This class implements the ODA Visualize SDK parameters for importing .prc files.
*/
class ODTV_EXPORT OdTvPrcImportParams : public OdTvBaseImportParams
{
  /** \details
  Specific options for the .prc import operation.
  */
  enum ParamFlags
  {
    kObjectNaming = 1,          // Gives the names for the Visualize SDK entities according to the file object names (for example, AcDbCircle, etc).
    kStoreSource = 2,           // Stores source objects in the user data of the <link OdTvEntity, OdTvEntity> objects.
    kClearEmptyObjects = 4,     // Cleans empty objects. 
    kRearrangeObjects = 8,      // Rearranges objects.
    kImportBrepAsBrep = 16      // import brep as OdTvBrepData
  };

public:

  /** \details
  Creates a new set of .prc import parameters with default values.
  */
  OdTvPrcImportParams();

  /** \details
  Destroys the set of .prc import parameters.
  */
  ~OdTvPrcImportParams() {}

  /** \details
  Sets a new palette to be used during the import operation.
  \param pPalette [in] A new palette to be set represented with a pointer to an array of colors.
  \remarks
  If the pPalette parameter is NULL, one of two default palettes is used depending on the background color value.
  */
  void setPalette(const ODCOLORREF* pPalette);

  /** \details
  Retrieves the current palette to be used during the import operation.
  \returns Returns a pointer to an array of colors that represents the palette.
  \remarks
  If the returned pointer is NULL, one of two default palettes is used depending on the background color value.
  */
  const ODCOLORREF* getPalette() const;

  /** \details
  Sets a new device rectangle that is used during the import operation.
  \param rect [in]  A new rectangle object to be set.
  \remarks
  If all components of the rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  void setDCRect(const OdTvDCRect& rect);

  /** \details
  Retrieves the current device rectangle that is used during the import operation.
  \returns Returns the current device rectangle object.
  \remarks
  If all components of the rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  OdTvDCRect  getDCRect() const;

  /** \details
  Sets a new value of the objects naming flag.
  \param bSet [in] A new flag value.
  */
  void setObjectNaming(bool bSet);

  /** \details
  Retrieves the current objects naming flag value.
  \returns Returns the current value of the objects naming flag.
  */
  bool getObjectNaming() const;

  /** \details
  Sets a new value of the store source objects flag.
  \param bSet [in] A new flag value.
  */
  void setStoreSourceObjects(bool bSet);

  /** \details
  Retrieves the current value of the store source objects flag.
  \returns Returns the current store source objects flag's value.
  */
  bool getStoreSourceObjects() const;

  /** \details
  Sets a new value of the flag that determines whether empty objects should be cleared.
  \param bSet [in] A new flag value.
  */
  void setClearEmptyObjects(bool bSet);

  /** \details
  Retrieves the current value of the clear empty objects flag.
  \returns Returns true if empty objects should be cleared during the import operation; otherwise the method returns false. 
  */
  bool getClearEmptyObjects() const;

  /** \details
  Sets a new value of the rearrange objects flag.
  \param bSet [in] A new flag value.
  */
  void setRearrangeObjects(bool bSet);

  /** \details
  Retrieves the current value of the rearrange objects flag.
  \returns Returns true if the objects should be rearranged during the import operation; otherwise the method returns false. 
  */
  bool getRearrangeObjects() const;

  /** \details
  Sets a new value of the import Brep as Brep flag.
  \param bSet [in] A new flag value.
  */
  void setImportBrepAsBrep(bool bImportBrepAsBrep);

  /** \details
  Returns true if and only if brep should be imported as brep
  */
  bool getImportBrepAsBrep() const;

  /** \details
  Sets a new value of the facet resolution for the Breps.
  \param dFacetRes [in] A new facet resolution value.
  \remarks
  The facet resolution value varies from 0.1 to 10.0.
  */
  void setBrepFacetRes(double dFacetRes);

  /** \details
  Retrieves the current facet resolution value for the Breps.
  \returns Returns the current facet resolution value between 0.1 and 10.0.
  */
  double getBrepFacetRes() const;

//DOM-IGNORE-BEGIN
protected:

  const ODCOLORREF*       m_pPalette;               // Palette to be used. If NULL, one of two default palettes is used depending on the background color.
  OdTvDCRect              m_importRect;             // Output rectangle. Used for correct import of some specific objects (camera, OLE images). The normal way to set the output window size.
  double                  m_dFACETRES;              // Facet res value (between from 0.01 to 10.0).
  OdUInt8                 m_flags;                  // Different options.
//DOM-IGNORE-END
};


/** \details
This class implements the ODA Visualize SDK parameters for appending .prc file content.
*/
class ODTV_EXPORT OdTvPrcAppendParams : public OdTvPrcImportParams, public OdTvBaseAppendParams
{
public:

  /** \details 
  Creates a new set of parameters for appending .prc file content. 
  */
  OdTvPrcAppendParams() : OdTvPrcImportParams() {};

  /** \details 
  Destroys the set of parameters for appending .prc file content. 
  */
  virtual ~OdTvPrcAppendParams() {}
};


/** \details
This class implements the ODA Visualize SDK parameters for importing BIM files.
*/
class ODTV_EXPORT OdTvBimImportParams : public OdTvBaseImportParams
{
  /** \details
  Specific options for BIM import operations.
  */
  enum ParamFlags
  {
    kObjectNaming = 1,          // Gives the names for the Visualize SDK entities according to the file object names (for example, AcDbCircle, etc).
    kStoreSource = 2,           // Stores source objects in the user data of the <link OdTvEntity, OdTvEntity> objects.
    kThinLines = 4,             // Uses thin lines mode.
    kClearEmptyObjects = 8      // Cleans empty objects. 
  };

public:

  /** \details
  Creates a new set of BIM import parameters with default values.
  */
  OdTvBimImportParams();

  /** \details
  Destroys the set of BIM import parameters.
  */
  ~OdTvBimImportParams() {}

  /** \details
  Sets a new background value for the BIM file import operation.
  \param background [in] A new background color value.
  */
  void setBackground(ODCOLORREF background);

  /** \details
  Retrieves the current background value.
  \returns Returns the current value of the background color for the BIM file import operation.
  */
  ODCOLORREF getBackground() const;

  /** \details
  Sets a new device rectangle that is used during the import operation.
  \param rect [in]  A new rectangle object to be set.
  \remarks
  If all components of the rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  void setDCRect(const OdTvDCRect& rect);

  /** \details
  Retrieves the current device rectangle that is used during the import operation.
  \returns Returns the current device rectangle object.
  \remarks
  If all components of the rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  OdTvDCRect  getDCRect() const;

  /** \details
  Sets a new value of the objects naming flag.
  \param bSet [in] A new flag value.
  */
  void setObjectNaming(bool bSet);

  /** \details
  Retrieves the current objects naming flag value.
  \returns Returns the current value of the objects naming flag.
  */
  bool getObjectNaming() const;

  /** \details
  Sets a new value of the store source objects flag.
  \param bSet [in] A new flag value.
  */
  void setStoreSourceObjects(bool bSet);

  /** \details
  Retrieves the current value of the store source objects flag.
  \returns Returns the current store source objects flag's value.
  */
  bool getStoreSourceObjects() const;

  /** \details
  Sets a new callback function for filtering element selection.
  \param pCallback [in]  A pointer to the <link OdTvFeedbackForChooseCallback, selection function>.
  */
  void setFeedbackForChooseCallback(OdTvFeedbackForChooseCallback pCallback);

  /** \details
  Retrieves the current callback function for element selection.
  \returns Returns a pointer to the current <link OdTvFeedbackForChooseCallback, callback function>.
  */
  OdTvFeedbackForChooseCallback getFeedbackForChooseCallback() const;

  /** \details
  Sets a new thin lines flag value.
  \param bSet [in] A new flag value.
  */
  void setThinLines(bool bSet);

  /** \details
  Retrieves the current thin lines flag value.
  \returns Returns true if thin lines are used; otherwise the method returns false.
  */
  bool getThinLines() const;

  /** \details
  Sets a new value of the flag that determines whether empty objects should be cleared.
  \param bSet [in] A new flag value.
  */
  void setClearEmptyObjects(bool bSet);

  /** \details
  Retrieves the current value of the clear empty objects flag.
  \returns Returns true if empty objects should be cleared during the import operation; otherwise the method returns false. 
  */  
  bool getClearEmptyObjects() const;

//DOM-IGNORE-BEGIN
protected:

  ODCOLORREF                     m_background;             // Background color. Default: black.
  OdTvDCRect                     m_importRect;             // Output rectangle. Used for correct import of some specific objects (camera, OLE images). The normal way to set the output window size.
  OdUInt8                        m_flags;                  // Different options.
  OdTvFeedbackForChooseCallback  m_pCallback;              // Callback for choose.
//DOM-IGNORE-END  
};


/** \details
This class implements the ODA Visualize SDK parameters for appending BIM file content.
*/
class ODTV_EXPORT OdTvBimAppendParams : public OdTvBimImportParams, public OdTvBaseAppendParams
{
public:

  /** \details 
  Creates a new set of parameters for appending BIM file content. 
  */
  OdTvBimAppendParams() : OdTvBimImportParams() {};
  
  /** \details 
  Destroys the set of parameters for appending BIM file content. 
  */
  virtual ~OdTvBimAppendParams() {}
};


/** \details
This class implements the Visualize SDK parameters for importing NwInterop files.
*/
class ODTV_EXPORT OdTvNwImportParams : public OdTvCustomBaseImportParams
{
public:

  /** \details
  Creates a new set of NwInterop import parameters with default values.
  */
  OdTvNwImportParams();

  /** \details
  Destroys the set of Interop import parameters.
  */
  ~OdTvNwImportParams() {}

  /** \details
  Sets a new device rectangle that is used during the import operation.
  \param rect [in]  A new rectangle object to be set.
  \remarks
  If all components of the rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  void setDCRect(const OdTvDCRect& rect);

  /** \details
  Retrieves the current device rectangle that is used during the import operation.
  \returns Returns the current device rectangle object.
  \remarks
  If all components of the rectangle are equal to zero, the rectangle is not associated with the Visualize device.
  */
  OdTvDCRect  getDCRect() const;

  /** \details
  Sets a new background value for the NwInterop import operation.
  \param background [in] A new background color value.
  */
  void setBackground(ODCOLORREF background);

  /** \details
  Retrieves the current background value.
  \returns Returns the current value of the background color for the NwInterop import operation.
  */
  ODCOLORREF getBackground() const;

  /** \details
  Sets a new store source objects flag value.
  \param bVal [in] A new flag value.
  */
  void setStoreSourceObjects(bool bVal);

  /** \details
  Retrieves the store source objects flag value.
  \returns Returns true if store source objects is available; otherwise the method returns false.
  */
  bool getStoreSourceObjects() const;

  /** \details
  Sets a new change to Nwc flag value.
  \param bVal [in] A new flag value.
  */
  void setChangeToNwc(bool bChange);

  /** \details
  Retrieves the change to Nwc flag value.
  \returns Returns true if change to Nwc is available; otherwise the method returns false.
  */
  bool getChangeToNwc() const;

//DOM-IGNORE-BEGIN
protected:
  OdTvDCRect                     m_importRect;             // Output rectangle. Used for correct import of some specific objects (camera, OLE images). The normal way to set the output window size.
  ODCOLORREF                     m_background;             // Background color.
  bool                           m_bChangeToNwc;
  bool                           m_bStoreSourceObjects;
//DOM-IGNORE-END
};

/** \details
This class implements the Visualize SDK parameters for appending NwInterop file content.
*/
class ODTV_EXPORT OdTvNwAppendParams : public OdTvNwImportParams, public OdTvBaseAppendParams
{
public:

  /** \details 
  Creates a new set of parameters for appending NwInterop file content. 
  */
  OdTvNwAppendParams() : OdTvNwImportParams() {};

  /** \details 
  Destroys the set of parameters for appending NwInterop file content. 
  */
  virtual ~OdTvNwAppendParams() {}
};


/** \details
This class implements the ODA Visualize SDK parameters for importing .ifc files.
*/
class ODTV_EXPORT OdTvIfcImportParams : public OdTvCustomBaseImportParams
{
  OdTvFeedbackForChooseCallback m_pCallback;              // Callback.
  double m_deviation;                                     // Triangulation deviation value.
  OdUInt16 m_minPerCircle;                                // Minimum number of segments per circle if deviation is high.
  OdUInt16 m_maxPerCircle;                                // Maximum number of segments per circle if deviation is low.
  OdUInt8  m_modelerType;                                 // Modeler type for IFC geometry.
  bool m_bUseCustomColor;                                 // Use background color or default gradient background
  ODCOLORREF m_BGColor;                                   // Background color.
public:

  /** \details
  Creates a new set of IFC import parameters with default values.
  */
  OdTvIfcImportParams();

  /** \details
  Destroys the set of IFC import parameters.
  */
  virtual ~OdTvIfcImportParams() {};

  /** \details
  Sets a new value of the background color.
  \param defcolor [in] A new background color value.
  */
  void setBGColor(ODCOLORREF defcolor);

  /** \details
  Retrieves the current background color value.
  \returns Returns the current value of the background color.
  */
  ODCOLORREF getBGColor() const;


  /** \details
  Enables the background color.
  \param defcolor [in] Enable background color value.
  */
  void setEnableBGColor(bool enable);

  /** \details
  Retrieves the current state of background color .
  \returns Returns the current state of the background color.
  */
  bool getEnableBGColor() const;

  /** \details
  Sets the callback function.

  \param pCallback [in]  A pointer to the function.
  */
  void setFeedbackForChooseCallback(OdTvFeedbackForChooseCallback pCallback);

  /** \details
  Retrieves the current callback function for element selection.
  \returns 
  Returns a pointer to the current callback function.
  */
  OdTvFeedbackForChooseCallback getFeedbackForChooseCallback() const;
  
  /** \details
  Retrieves the deviation value that is used during the triangulation process.

  \returns 
  Returns a double value that shows the maximum allowed deviation of a triangulated shape.
  */
  double getDeviation() const;
  
  /** \details
  Sets the maximum allowed deviation value of a triangulated shape.

  \param pCallback [in]  A double value that specifies how much a triangulated shape will deviate from the same shape represented by an equation.
  */
  void setDeviation(double deviation);

  /** \details
  Retrieves how many segments are used to construct a shape if the deviation value is high.

  \returns Returns an OdUInt16 value that represents the quantity of segments used to form a shape when the deviation value is high.
  */
  OdUInt16 getMinPerCircle() const;
  
  /** \details
  Sets the minimum amount of segments required to construct a shape if the deviation value is high.

  \param minPerCircle [in]  Minimum quantity of segments used to form a shape when the deviation value is high.
  */
  void setMinPerCircle(OdUInt16 minPerCircle);

  /** \details
  Retrieves how many segments are used to construct a shape if the deviation value is low.

  \returns Returns an OdUInt16 value that represents the quantity of segments used to form a shape when the deviation value is low.
  */
  OdUInt16 getMaxPerCircle() const;
  
  /** \details
  Sets the minimum amount of segments required to construct a shape if the deviation value is low.

  \param maxPerCircle [in]  Maximum quantity of segments used to form a shape when the deviation value is low.
  */
  void setMaxPerCircle(OdUInt16 maxPerCircle);

  /** \details
  Retrieves modeler type, that will be used to create IFC geometry.

  \returns Returns an OdUInt8 value that represents the quantity of segments used to form a shape when the deviation value is low.
*/

  OdUInt8 getModelerType() const;
  
  /** \details
  Sets the modeler type, that will be used to create IFC geometry.

  \param type [in]  Modeler type for IFC SDK.
*/
  void setModelerType(OdUInt8 type);
};


/** \details
This class implements the ODA Visualize SDK parameters for appending .ifc files.
*/
class ODTV_EXPORT OdTvIfcAppendParams : public OdTvIfcImportParams, public OdTvBaseAppendParams
{
public:

  /** \details
  Creates a new set of parameters for appending content of an .ifc file.
  */
  OdTvIfcAppendParams() : OdTvIfcImportParams() {};

  /** \details
  Destroys the set of parameters for appending content of an .ifc file.
  */
  virtual ~OdTvIfcAppendParams() {};

};

#endif //_ODTV_IMPORT_H_INCLUDED_
