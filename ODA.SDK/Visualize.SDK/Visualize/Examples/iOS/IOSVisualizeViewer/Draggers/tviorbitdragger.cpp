#include "tviorbitdragger.h"
#include "OdaCommon.h"

TviOrbitDragger::TviOrbitDragger(OdTvGsDeviceId &tvDeviceId, OdTvModelId &tvDraggersModelId)
    : OdTvDragger (tvDeviceId, tvDraggersModelId)
{
    m_D = 16.0;
}

eDraggerResult TviOrbitDragger::start(OdTvDraggerPtr pPrevDragger, OdTvExtendedView *pExtendedView)
{
    return  OdTvDragger::start(pPrevDragger, pExtendedView);
}

eDraggerResult TviOrbitDragger::nextpoint(int x, int y)
{
    if ( m_state == kWaiting )
        return kNothingToDo;

    OdTvGsViewPtr pView = getActiveTvViewPtr();
    if ( pView.isNull() )
        return kNothingToDo;

    // calculate click point in WCS
    OdGePoint3d pt = toEyeToWorld(x, y);

    // transfer point to the eye coordinate system
    m_prevPt = pView->viewingMatrix() * pt;

    // calculate mouse move to rotation angle conversion coefficient in base of viewport dc size
    OdGePoint3d pt1;
    OdGePoint2d pt2;
    pView->viewDcCorners((OdGePoint2d&)pt1, pt2);
    pt2.x -= pt1.x;
    pt2.y -= pt1.y;
    double r = odmin(pt2.x, pt2.y);
    m_D = r;

    // here we should to remember the extents since we want to rotate the scene about this point
    if (m_pTvExtendedView)
    {
        OdGeBoundBlock3d cachedExt;
        if (m_pTvExtendedView->getCachedExtents(cachedExt))
        {
            m_viewCenter = cachedExt.center();
            return kNothingToDo;
        }
    }

    OdGeBoundBlock3d extents;
    pView->viewExtents(extents);
    m_viewCenter = extents.center();
    m_viewCenter.transformBy(pView->eyeToWorldMatrix());

    if (m_pTvExtendedView)
    {
        OdGeBoundBlock3d lastExt;
        if (pView->getLastViewExtents(lastExt))
            m_pTvExtendedView->setViewExtentsForCaching(&lastExt);
    }

    return kNothingToDo;
}

eDraggerResult TviOrbitDragger::drag(int x, int y)
{
    if ( m_state == kWaiting )
        return kNothingToDo;

    OdTvGsViewPtr pView = getActiveTvViewPtr();
    if ( pView.isNull() )
        return kNothingToDo;

    // calculate click point in WCS
    OdGePoint3d pt = toEyeToWorld(x, y);

    // transfer point to the eye coordinate system
    OdGePoint3d ptView = pView->viewingMatrix() * pt;

    // calculate the angles for the rotation about appropriate axes
    double distX = ptView.x - m_prevPt.x; // around vertical
    double distY = ptView.y - m_prevPt.y; // around horizontal

    distX *= -OdaPI / m_D;
    distY *= OdaPI / m_D;

    // perform camera orbiting
    if (m_pTvExtendedView)
        m_pTvExtendedView->orbit(distY, distX, &m_viewCenter);

    // store previous click point
    m_prevPt = ptView;

    return kNeedUpdateView;
}

eDraggerResult TviOrbitDragger::nextpointup(int x, int y)
{
    return reset();
}
