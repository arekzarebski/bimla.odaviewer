#ifndef TVIPANDRAGGER_H
#define TVIPANDRAGGER_H

#include "Tools/TvDragger.h"

class TviPanDragger : public OdTvDragger
{
public :
    TviPanDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId)
        : OdTvDragger (tvDeviceId, tvDraggersModelId) {}
    ~TviPanDragger() {}

    virtual eDraggerResult start(OdTvDraggerPtr pPrevDragger, OdTvExtendedView* pExtendedView);
    virtual eDraggerResult nextpoint(int x, int y);
    virtual eDraggerResult drag(int x, int y);
    virtual eDraggerResult nextpointup(int x, int y);

private:

    // last cliked or moved point (WCS)
    OdGePoint3d m_prevPt;

    // last camera position (WCS)
    OdGePoint3d m_pos;
};

#endif // TVIPANDRAGGER_H
