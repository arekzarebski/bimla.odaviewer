#ifndef TVIORBITDRAGGER_H
#define TVIORBITDRAGGER_H

#include "Tools/TvDragger.h"

class TviOrbitDragger : public OdTvDragger
{
public:
    TviOrbitDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId);
    ~TviOrbitDragger() {}

    eDraggerResult start(OdTvDraggerPtr pPrevDragger, OdTvExtendedView* pExtendedView);
    eDraggerResult nextpoint(int x, int y);
    eDraggerResult drag(int x, int y);
    eDraggerResult nextpointup(int x, int y);

  private:

    // last cliked or moved point (ECS)
    OdGePoint3d m_prevPt;

    // mouse move to rotation angle conversion coefficient
    double m_D;

    // center of the scene
    OdGePoint3d     m_viewCenter;
};

#endif // TVIORBITDRAGGER_H
