#ifndef TVIZOOMWINDOWDRAGGER_H
#define TVIZOOMWINDOWDRAGGER_H

#include "Tools/TvDragger.h"
#include "tviview.h"

class TviZoomWindowDragger : public OdTvDragger
{
public:
    TviZoomWindowDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, TviView *pView);
    ~TviZoomWindowDragger() {}

    eDraggerResult start(OdTvDraggerPtr pPrevDragger, OdTvExtendedView* pExtendedView);
    eDraggerResult nextpoint(int x, int y);
    eDraggerResult drag(int x, int y);
    eDraggerResult nextpointup(int x, int y);
    OdTvDraggerPtr finish(eDraggerResult& rc);

  private:
    void updateFrame(bool bCreate);

  private:

    // first clicked point (WCS)
    OdGePoint3d m_firstPt;

    // first clicked point (DCS) pixels
    OdGePoint2d m_firstPt_device;

    // last drag point (WCS)
    OdGePoint3d m_lastDragPt;

    // need to control the ::start called first time or not
    bool m_bJustCreatedObject;

    // temporary geometry
    OdTvEntityId m_entityId;
    OdTvGeometryDataId m_frameId;

    // points for creating the polygon frame
    OdTvPointArray m_pts;

    TviView *m_pTviVeiw;
};

#endif // TVIZOOMWINDOWDRAGGER_H
