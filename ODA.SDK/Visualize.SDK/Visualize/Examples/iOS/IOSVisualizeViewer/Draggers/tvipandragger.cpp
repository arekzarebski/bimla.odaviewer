#include "tvipandragger.h"
#include "OdaCommon.h"

#include <QDebug>

eDraggerResult TviPanDragger::start(OdTvDraggerPtr pPrevDragger, OdTvExtendedView *pExtendedView)
{
    return OdTvDragger::start(pPrevDragger, pExtendedView);
}

eDraggerResult TviPanDragger::nextpoint(int x, int y)
{
    if ( m_state == kWaiting )
        return kNothingToDo;

    OdTvGsViewPtr pView = getActiveTvViewPtr();
    if ( pView.isNull() )
        return kNothingToDo;

    // remember camera current position
    m_pos = pView->position();

    // remember the difference between click point in WCS and camera current position
    m_prevPt = toEyeToWorld(x, y) - m_pos.asVector();

    return kNothingToDo;
}

eDraggerResult TviPanDragger::drag(int x, int y)
{
    if ( m_state == kWaiting )
        return kNothingToDo;

    OdTvGsViewPtr pView = getActiveTvViewPtr();
    if ( pView.isNull() )
        return kNothingToDo;

    // calculate click point in WCS
    OdGePoint3d pt = toEyeToWorld(x, y);

    //obtain delta for dolly
    OdGeVector3d delta = (m_prevPt - (pt - m_pos)).asVector();

    // transform delta to eye
    delta.transformBy(pView->viewingMatrix());

    // perform camera moving
    pView->dolly(delta.x, delta.y, delta.z);

    // remember the difference between click point in WCS and camera previous position
    m_prevPt = pt - m_pos.asVector();

    // remember camera current position
    m_pos = pView->position();

    return kNeedUpdateView;
}

eDraggerResult TviPanDragger::nextpointup(int x, int y)
{
    return reset();
}
