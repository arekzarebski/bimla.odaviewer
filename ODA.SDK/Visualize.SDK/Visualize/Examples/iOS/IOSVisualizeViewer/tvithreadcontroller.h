#ifndef TVITHREADCONTROLLER_H
#define TVITHREADCONTROLLER_H

#include <QObject>
#include <QThread>
// ODA SDK
#include "OdaCommon.h"
// Visualize SDK
#include "TvError.h"

class TviThreadController;

struct TviThreadTask : public QObject {
  Q_OBJECT
public:
  ~TviThreadTask() {}

  virtual void run() = 0;

  TviThreadController* getController() { return m_pController; }

private:
  friend class TviThreadController;

  TviThreadController * m_pController;
};

class TviThreadController : public QThread {
  Q_OBJECT

  void run();

public:
  TviThreadController();
  void setTask(TviThreadTask *task);
  void cancel();

signals:
  void resultReady();
  void error(const QString& err);
  void unexpectedError();

private:
  TviThreadTask *m_pTask;
  bool m_bisCancel;
};

#endif // TVITHREADCONTROLLER_H
