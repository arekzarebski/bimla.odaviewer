#ifndef TVIMEMORYSTATUS_H
#define TVIMEMORYSTATUS_H

typedef unsigned long uint64;

class TviMemoryStatus
{
public:
    TviMemoryStatus();

    uint64 getUsedMemory() const { return  m_iUsedMemory; }
    uint64 getFreeMemory() const { return  m_iFreeMemory; }

private:
    uint64 m_iUsedMemory;
    uint64 m_iFreeMemory;
};

#endif // TVIMEMORYSTATUS_H
