#include "tviutils.h"

QString toQString(const OdString& sStr)
{
  std::wstring wstr((wchar_t*)sStr.c_str());
  QString qsStr(QString::fromStdWString(wstr));
  return qsStr;
}

OdString toOdString(const QString& sStr)
{
  //std::wstring wstr(sStr.toStdWString());
  wchar_t* pChars = new wchar_t[ sStr.length() + 1 ];
  sStr.toWCharArray( pChars );
  pChars[ sStr.length() ] = 0;
  OdString odsStr( pChars );
  delete[] pChars;
  return odsStr;
}
