#include "tvithreadcontroller.h"

#include "tviutils.h"

TviThreadController::TviThreadController()
  : QThread(), m_pTask(NULL), m_bisCancel(false)
{
  //connect(this, SIGNAL(finished), this, SLOT(deleteLater));
}

void TviThreadController::setTask(TviThreadTask *pTask)
{
  m_pTask = pTask;
  m_pTask->m_pController = this;
}

void TviThreadController::cancel()
{
  m_bisCancel = true;
  this->terminate();
}

void TviThreadController::run()
{
  try
  {
    m_pTask->run();
    if (!m_bisCancel)
      emit resultReady();
  }
  catch (OdTvError& err)
  {
    if (!m_bisCancel)
      emit error( toQString(err.description()) );
  }
  catch (OdError& err)
  {
    if (!m_bisCancel)
      emit error( toQString(err.description()) );
  }
  catch (...)
  {
    if (!m_bisCancel)
      emit unexpectedError();
  }
}
