#include "TviAppearanceParams.h"

TviAppearanceParams::TviAppearanceParams(QObject *parent) : QObject(parent)
  , m_bWcsEnabled(true), m_bFpsEnabled(false), m_bAnimationEnabled(true), m_bPartialOpen(false), m_bUseLimitManager(false)
  , m_bUseSceneGraph(false)
{

}
