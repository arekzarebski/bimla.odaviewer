#ifndef TVIFILEOPENTHREADTASK_H
#define TVIFILEOPENTHREADTASK_H

#include "tvithreadcontroller.h"
#include "tvidatabaseinfo.h"
#include "TviAppearanceParams.h"

// Visualize SDK
#include "TvImport.h"
#include "TvDatabase.h"
#include "TvFactory.h"

class TviFileOpenThreadTask : public TviThreadTask
{
    Q_OBJECT

public:
    TviFileOpenThreadTask(OdTvBaseImportParams *pParams, TviDatabaseInfo info, const OdString& filePath, TviAppearanceParams *generalParams, bool bLowMemory);
    ~TviFileOpenThreadTask() {}

    virtual void run();

    OdTvDatabaseId getTvDatabase() const { return  m_TvDatabaseId; }

private:
    OdTvBaseImportParams *m_baseParams;
    TviDatabaseInfo m_dbInfo;
    OdTvDatabaseId m_TvDatabaseId;
    OdString m_filePath;
    TviAppearanceParams *m_pGeneralParams;
    bool m_bLowMemory;
};

#endif // TVIFILEOPENTHREADTASK_H
