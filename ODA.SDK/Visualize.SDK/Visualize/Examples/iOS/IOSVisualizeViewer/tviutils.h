#ifndef TVIUTILS_H
#define TVIUTILS_H

#include "OdaCommon.h"
#include "OdPlatform.h"
#include "QString"
#include "OdString.h"

QString toQString(const OdString& sStr);
OdString toOdString(const QString& sStr);


#endif // TVIUTILS_H
