#include "TviLimitManager.h"

#ifdef __IPHONE_OS_VERSION_MIN_REQUIRED
#include "TviMemoryStatus.h"
#endif

TviLimitManager::TviLimitManager()
{
    m_nMemoryLimit = 50 * 1024 * 1024;
}

TviLimitManager::~TviLimitManager(){ }

OdTvLimitManager::MemoryLimitControlResult TviLimitManager::checkMemoryUsage(OdUInt8 nReason, OdUInt64 nApproximateMemoryUsage) const
{
#ifdef __IPHONE_OS_VERSION_MIN_REQUIRED
    m_mutex.lock();
    TviMemoryStatus memStatus;
    if(memStatus.getFreeMemory() > m_nMemoryLimit) {
        m_mutex.unlock();
        return OdTvLimitManager::kPassed;
    }
    m_mutex.unlock();
    return OdTvLimitManager::kNotPassed;
#endif
}


