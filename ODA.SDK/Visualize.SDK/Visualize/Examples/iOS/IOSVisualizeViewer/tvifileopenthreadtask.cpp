#include "tvifileopenthreadtask.h"

#include "tvidatabaseinfo.h"

#include <QElapsedTimer>

#include "tviutils.h"

TviFileOpenThreadTask::TviFileOpenThreadTask(OdTvBaseImportParams *params, TviDatabaseInfo info, const OdString& filePath
                                             , TviAppearanceParams *generalParams, bool bLowMemory)
    : m_baseParams(params), m_dbInfo(info), m_filePath(filePath), m_pGeneralParams(generalParams), m_bLowMemory(bLowMemory)
{
}

void TviFileOpenThreadTask::run()
{
    try
    {
        OdTvResult rc = tvOk;

        if(m_dbInfo.getType() == TviDatabaseInfo::kFromFile)
        {

            QElapsedTimer timer;
            timer.start();

            m_TvDatabaseId = odTvGetFactory().readFile(m_dbInfo.getFilePath(), !m_pGeneralParams->getPartialOpen(), m_pGeneralParams->getPartialOpen(), &rc);

            // fill profiling info
            if (m_baseParams->getProfiling())
              m_baseParams->getProfiling()->setImportTime(OdInt64(timer.elapsed()));

            if (rc != tvOk)
            {
                OdString msg = "Reading of file failed";
                getController()->error( toQString(msg) );
            }
        }
        else
        {
            m_baseParams->setFilePath(m_filePath);
            m_dbInfo.setFilePath(m_filePath);

            if ( !m_bLowMemory )
              m_TvDatabaseId = odTvGetFactory().importFile(m_baseParams, &rc);
            else
            {
              rc = odTvGetFactory().lowMemoryImportFile(m_baseParams, m_dbInfo.getFilePath() + L".vsf");
              if ( rc == tvOk )
                m_TvDatabaseId = odTvGetFactory().readFile(m_dbInfo.getFilePath() + L".vsf", false, true, &rc);
            }

            if (rc != tvOk)
            {
                OdString msg;
                if (rc == tvMissingFilerModule)
                    msg = "Missing filer module.";
                else
                    msg = "Error during open file.";
                getController()->error( toQString(msg) );
            }
        }

    }
    catch (OdTvError &err)
    {
        getController()->error( toQString(err.description()) );
    }

}


