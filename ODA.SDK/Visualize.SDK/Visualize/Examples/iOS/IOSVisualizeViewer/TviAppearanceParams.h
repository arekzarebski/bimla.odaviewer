#ifndef TVIAPPEARANCEPARAMS_H
#define TVIAPPEARANCEPARAMS_H

#include <QObject>

class TviAppearanceParams : public QObject
{
    Q_OBJECT
public:
    explicit TviAppearanceParams(QObject *parent = NULL);

    Q_PROPERTY(bool wcsEnabled MEMBER m_bWcsEnabled NOTIFY wcsEnabledChanged)
    Q_PROPERTY(bool fpsEnabled MEMBER m_bFpsEnabled NOTIFY fpsEnabledChanged)
    Q_PROPERTY(bool animationEnabled MEMBER m_bAnimationEnabled NOTIFY animationEnabledChanged)
    Q_PROPERTY(bool partialOpen MEMBER m_bPartialOpen NOTIFY partialOpenChanged)
    Q_PROPERTY(bool useLimitManager MEMBER m_bUseLimitManager NOTIFY useLimitManagerChanged)
    Q_PROPERTY(bool useSceneGraph MEMBER m_bUseSceneGraph NOTIFY useSceneGraphChanged)

    bool getWcsEnabled() const { return m_bWcsEnabled; }
    bool getFpsEnabled() const { return m_bFpsEnabled; }
    bool getAnimationEnabled() const { return  m_bAnimationEnabled; }
    bool getPartialOpen() const { return m_bPartialOpen; }
    bool getUseLimitManager() const { return m_bUseLimitManager; }
    bool getUseSceneGraph() const { return m_bUseSceneGraph; }

signals:
    void wcsEnabledChanged();
    void fpsEnabledChanged();
    void animationEnabledChanged();
    void partialOpenChanged();
    void useLimitManagerChanged();
    void useSceneGraphChanged();

private:
    bool m_bWcsEnabled;
    bool m_bFpsEnabled;
    bool m_bAnimationEnabled;
    bool m_bPartialOpen;
    bool m_bUseLimitManager;
    bool m_bUseSceneGraph;
};

#endif // TVIAPPEARANCEPARAMS_H
