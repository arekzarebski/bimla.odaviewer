#ifndef TVILIMITMANAGER_H
#define TVILIMITMANAGER_H

#include "OdaCommon.h"
#include "TvFactory.h"

class TviLimitManager : public OdTvLimitManager
{
public:
    TviLimitManager();
    ~TviLimitManager();

    virtual OdTvLimitManager::MemoryLimitControlResult checkMemoryUsage(OdUInt8 nReason, OdUInt64 nApproximateMemoryUsage = 0) const;
    void setMemoryLimit( OdUInt64 nLimit ) { m_nMemoryLimit = nLimit; }
    OdUInt64 memoryLimit() const { return m_nMemoryLimit; }

protected:
    OdUInt64 m_nMemoryLimit;
    mutable OdMutex m_mutex;
};

#endif // TVILIMITMANAGER_H
