# DO NOT EDIT
# This makefile makes sure all linkable targets are
# up-to-date with anything they link to
default:
	echo "Do not invoke directly"

# Rules to remove targets that are older than anything to which they
# link.  This forces Xcode to relink the targets from scratch.  It
# does not seem to check these dependencies itself.
PostBuild.iVisualizeViewer.Debug:
/Users/arekzarebski/dev/ODA.SDK/Visualize.SDK/Visualize_iOS_13.0simdbg/Visualize/Examples/iOS/iVisualizeViewer/build_ios/Debug${EFFECTIVE_PLATFORM_NAME}/iVisualizeViewer.app/iVisualizeViewer:
	/bin/rm -f /Users/arekzarebski/dev/ODA.SDK/Visualize.SDK/Visualize_iOS_13.0simdbg/Visualize/Examples/iOS/iVisualizeViewer/build_ios/Debug${EFFECTIVE_PLATFORM_NAME}/iVisualizeViewer.app/iVisualizeViewer


PostBuild.iVisualizeViewer.Release:
/Users/arekzarebski/dev/ODA.SDK/Visualize.SDK/Visualize_iOS_13.0simdbg/Visualize/Examples/iOS/iVisualizeViewer/build_ios/Release${EFFECTIVE_PLATFORM_NAME}/iVisualizeViewer.app/iVisualizeViewer:
	/bin/rm -f /Users/arekzarebski/dev/ODA.SDK/Visualize.SDK/Visualize_iOS_13.0simdbg/Visualize/Examples/iOS/iVisualizeViewer/build_ios/Release${EFFECTIVE_PLATFORM_NAME}/iVisualizeViewer.app/iVisualizeViewer


PostBuild.iVisualizeViewer.MinSizeRel:
/Users/arekzarebski/dev/ODA.SDK/Visualize.SDK/Visualize_iOS_13.0simdbg/Visualize/Examples/iOS/iVisualizeViewer/build_ios/MinSizeRel${EFFECTIVE_PLATFORM_NAME}/iVisualizeViewer.app/iVisualizeViewer:
	/bin/rm -f /Users/arekzarebski/dev/ODA.SDK/Visualize.SDK/Visualize_iOS_13.0simdbg/Visualize/Examples/iOS/iVisualizeViewer/build_ios/MinSizeRel${EFFECTIVE_PLATFORM_NAME}/iVisualizeViewer.app/iVisualizeViewer


PostBuild.iVisualizeViewer.RelWithDebInfo:
/Users/arekzarebski/dev/ODA.SDK/Visualize.SDK/Visualize_iOS_13.0simdbg/Visualize/Examples/iOS/iVisualizeViewer/build_ios/RelWithDebInfo${EFFECTIVE_PLATFORM_NAME}/iVisualizeViewer.app/iVisualizeViewer:
	/bin/rm -f /Users/arekzarebski/dev/ODA.SDK/Visualize.SDK/Visualize_iOS_13.0simdbg/Visualize/Examples/iOS/iVisualizeViewer/build_ios/RelWithDebInfo${EFFECTIVE_PLATFORM_NAME}/iVisualizeViewer.app/iVisualizeViewer




# For each target create a dummy ruleso the target does not have to exist
