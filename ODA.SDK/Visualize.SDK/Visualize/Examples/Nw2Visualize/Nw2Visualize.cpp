/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _WIN32_WCE
#include <sys/stat.h>
#endif

#include <fstream>

#include "OdaCommon.h"
#include "RxDynamicModule.h"
#include "RxVariantValue.h"
#include "ThumbnailImage.h"

// Brep
#include "Br/BrBrep.h"
#include "BrepRenderer/BrepRendererImpl.h"

// Tv
#include "TvGeomCollector.h"
#include "TvFilerTimer.h"
#include "TvImport.h"
#include "TvDatabaseUtils.h"

// Nw
#include "Nw2Visualize.h"
#include "NwMaterial.h"

#include "NwGeometryEllipticalShape.h"
#include "NwGeometryLineSet.h"
#include "NwGeometryMesh.h"
#include "NwGeometryPointSet.h"
#include "NwGeometryText.h"
#include "NwGeometryTube.h"

#include "NwComponent.h"
#include "NwFragment.h"
#include "NwModelItem.h"

#include "NwBackgroundElement.h"
#include "NwModel.h"
#include "NwTexture.h"
#include "NwColor.h"
#include "Ge/GeQuaternion.h"
#include "NwUnitUtils.h"
#include "NwDistantLight.h"
#include "NwSpotLight.h"
#include "NwPointLight.h"
#include "NwVariant.h"

#include "TvDatabaseCleaner.h"
#include "../Extensions/ExVisualizeDevice/GsVisualizeVectorizer.h"
#include "Ge/GeMatrix3d.h"


//#define NW_DRAW_MESH_AS_POINTSET
#define NW_USE_TRIANGULATION

#define START_TV_NW_TIMER                            \
  if (pTvTimer)                                       \
    pTvTimer->start();

#define END_TV_NW_TIMER                              \
  if (pTvTimer)                                       \
  {                                                   \
    nTotalTvTime += pTvTimer->permanentSec();         \
    pTvTimer->stop();                                 \
  }

static OdString nwSanitizePath(const OdString& path);
static OdString nwExtractFileName(const OdString& path);
static OdString nwExtractFileDir(const OdString& path);
static OdResult findPathToFile(const OdString& sCurrentPath, OdString &sFileToFind);

class OdTvVisualizeNwFilerSourceFromDb : public OdTvVisualizeNwFilerDbSource
{
  OdDbBaseDatabase *mDatabase;

public:
  OdTvVisualizeNwFilerSourceFromDb(OdDbBaseDatabase *pDatabase) : mDatabase(pDatabase) {};
  virtual OdNwDatabasePtr   getDb() { return mDatabase; };
  virtual bool              odWasInitialized() const { return false; }
  virtual OdString          getFilename()
  {
    OdNwDatabasePtr pDb = mDatabase;
    if (!pDb.isNull())
    {
      OdString fileName = pDb->getFilename();
      if (!fileName.isEmpty())
        return fileName;
    }

    return OD_T("NoNameNwDatabase");
  }
};

class OdTvVisualizeNwFilerSourceFromFile : public OdTvVisualizeNwFilerDbSource
{
  OdStaticRxObject<OdTvNw2VisService> svcs;
  OdString mFilePath;

protected:
  double         m_initTime;

public:
  OdTvVisualizeNwFilerSourceFromFile(const OdString& filePath, OdTvFilerTimeProfiling* pProfileRes, const OdTvVisualizeNwFilerPropertiesPtr pProperties) { initialize(filePath, pProfileRes, pProperties); };
  virtual OdNwDatabasePtr getDb() 
  {
    return svcs.readFile(mFilePath); 
  };
  virtual double          getInitTime() const { return m_initTime; }
  virtual OdString        getFilename()
  {
    //generate name for the stream file
    return OdTvDatabaseUtinls::getFileNameFromPath(mFilePath);
  };

protected:
  OdTvVisualizeNwFilerSourceFromFile() {};
  void initialize(const OdString& filePath, OdTvFilerTimeProfiling* pProfileRes, const OdTvVisualizeNwFilerPropertiesPtr pProperties)
  {
    mFilePath = filePath;

    OdTvFilerTimer timing(needTimer(pProfileRes));
    timing.startTotal();

    svcs.setTextureDirectoryPath(nwExtractFileDir(mFilePath) + OD_T("/"), false);

    //load module
    ::odrxDynamicLinker()->loadModule(OdNwDbModuleName, false);

    timing.endTotal();
    m_initTime = timing.getTotalTime();
  };
};



/** \details
class for storing the information about the opened file
*/
class OdTvViewerDatabaseInfo : public OdTvFilerTimeProfiling
{
public:

  enum Type
  {
    kNew = 0,
    kBuiltIn = 1,
    kFromFile = 2,
    kImport = 4,
    kImportRevisions = 5,
  };

  OdTvViewerDatabaseInfo() : m_iLoadTime(0), m_iVectorizingTime(0), m_iTvCreationTime(0), m_iFirstUpdateTime(0), m_iCDACreationTime(0), m_type(kNew) {}
  ~OdTvViewerDatabaseInfo() {}

  // returns the time of file loading (in milliseconds)
  virtual OdInt64 getImportTime() const { return m_iLoadTime; }

  // returns the total vectorizing time (in milliseconds)
  virtual OdInt64 getVectorizingTime() const { return m_iVectorizingTime; }

  // returns the total tv calling time (in milliseconds)
  virtual OdInt64 getTvTime() const { return m_iTvCreationTime; }

  // returns  the total time of the CDA tree (and properties cache) creation
  virtual OdInt64 getCDATreeCreationTime() const { return m_iCDACreationTime; }

  // return the time of calling (in milliseconds)
  OdInt64 getFirstUpdateTime() const { return m_iFirstUpdateTime; }

  OdInt64 getTotalTime() const { return m_iLoadTime + m_iVectorizingTime + m_iTvCreationTime + m_iFirstUpdateTime; }

  // set the type
  Type getType() const { return m_type; }

  //set file path
  OdString getFilePath() const { return m_strPath; }

  // set the time of file loading (in milliseconds)
  virtual void setImportTime(OdInt64 time) { m_iLoadTime = time; }

  // set the time of vectorizing (in milliseconds)
  virtual void setVectorizingTime(OdInt64 time) { m_iVectorizingTime = time; }

  // set the time of calling (in milliseconds)
  virtual void setTvTime(OdInt64 time) { m_iTvCreationTime = time; }

  // set the time of calling (in milliseconds)
  void setFirstUpdateTime(OdInt64 time) { m_iFirstUpdateTime = time; }

  //Sets a new time occupied by the CDA tree (and properties cache) creation
  void setCDATreeCreationTime(OdInt64 time) { m_iCDACreationTime = time; }

  // set the type
  void setType(Type type) { m_type = type; }

  //set file path
  void setFilePath(const OdString& path) { m_strPath = path; }
private:

  OdInt64   m_iLoadTime;
  OdInt64   m_iVectorizingTime;
  OdInt64   m_iTvCreationTime;
  OdInt64   m_iCDACreationTime;
  OdInt64   m_iFirstUpdateTime;
  Type      m_type;
  OdString  m_strPath;
};

OdTvVisualizeNwFilerProperties::OdTvVisualizeNwFilerProperties()
  : m_importRect()
  , m_background(ODRGB(0, 0, 0))
  , m_defaultColor(ODRGB(191, 191, 191))
  , m_flags(0)
  , m_appendTransform(OdGeMatrix3d::kIdentity)
{
}

OdRxDictionaryPtr OdTvVisualizeNwFilerProperties::createObject()
{
  return OdRxObjectImpl<OdTvVisualizeNwFilerProperties, OdRxDictionary>::createObject();
}

OdTvVisualizeNwFilerProperties::~OdTvVisualizeNwFilerProperties()
{
}

    ODRX_DECLARE_PROPERTY(DCRect)
    ODRX_DECLARE_PROPERTY(BackgroundColor)
    ODRX_DECLARE_PROPERTY(DefaultNwColor)
    ODRX_DECLARE_PROPERTY(AppendTransform)
    ODRX_DECLARE_PROPERTY(NeedCDATree)
    ODRX_DECLARE_PROPERTY(NeedCollectPropertiesInCDA)
    ODRX_DECLARE_PROPERTY(ChangeToNwc)
    ODRX_DECLARE_PROPERTY(StoreSourceObjects)

    ODRX_BEGIN_DYNAMIC_PROPERTY_MAP(OdTvVisualizeNwFilerProperties);
    ODRX_GENERATE_PROPERTY(DCRect)
    ODRX_GENERATE_PROPERTY(BackgroundColor)
    ODRX_GENERATE_PROPERTY(DefaultNwColor)
    ODRX_GENERATE_PROPERTY(AppendTransform)
    ODRX_GENERATE_PROPERTY(NeedCDATree)
    ODRX_GENERATE_PROPERTY(NeedCollectPropertiesInCDA)
    ODRX_GENERATE_PROPERTY(ChangeToNwc)
    ODRX_GENERATE_PROPERTY(StoreSourceObjects)
    ODRX_END_DYNAMIC_PROPERTY_MAP(OdTvVisualizeNwFilerProperties);

    ODRX_DEFINE_PROPERTY_METHODS(DCRect, OdTvVisualizeNwFilerProperties, getDCRect, setDCRect, getIntPtr);
    ODRX_DEFINE_PROPERTY_METHODS(BackgroundColor, OdTvVisualizeNwFilerProperties, getBackgroundColor, setBackgroundColor, getIntPtr);
    ODRX_DEFINE_PROPERTY_METHODS(DefaultNwColor, OdTvVisualizeNwFilerProperties, getDefaultNwColor, setDefaultNwColor, getIntPtr);
    ODRX_DEFINE_PROPERTY_METHODS(AppendTransform, OdTvVisualizeNwFilerProperties, getAppendTransform, setAppendTransform, getIntPtr);
    ODRX_DEFINE_PROPERTY_METHODS(NeedCDATree, OdTvVisualizeNwFilerProperties, getNeedCDATree, setNeedCDATree, getBool);
    ODRX_DEFINE_PROPERTY_METHODS(NeedCollectPropertiesInCDA, OdTvVisualizeNwFilerProperties, getNeedCollectPropertiesInCDA, setNeedCollectPropertiesInCDA, getBool);
    ODRX_DEFINE_PROPERTY_METHODS(ChangeToNwc, OdTvVisualizeNwFilerProperties, getChangeToNwc, setChangeToNwc, getBool);
    ODRX_DEFINE_PROPERTY_METHODS(StoreSourceObjects, OdTvVisualizeNwFilerProperties, getStoreSourceObjects, setStoreSourceObjects, getBool);

void OdTvVisualizeNwFilerProperties::setDCRect(OdIntPtr rect)
{
  OdTvDCRect* pRect = (OdTvDCRect*)(rect);
  if (!pRect)
  {
    ODA_ASSERT(false);
  }

  m_importRect = *pRect;
}

OdIntPtr OdTvVisualizeNwFilerProperties::getDCRect() const
{
  return (OdIntPtr)(&m_importRect);
}

void OdTvVisualizeNwFilerProperties::setBackgroundColor(OdIntPtr ptr)
{
  ODCOLORREF* pColor = (ODCOLORREF*)(ptr);
  if (!pColor)
  {
    ODA_ASSERT(false);
  }
  m_background = *pColor;
}

OdIntPtr OdTvVisualizeNwFilerProperties::getBackgroundColor() const
{
  return (OdIntPtr)(&m_background);
}

void OdTvVisualizeNwFilerProperties::setDefaultNwColor(OdIntPtr ptr)
{
  ODCOLORREF* pColor = (ODCOLORREF*)(ptr);
  if (!pColor)
  {
    ODA_ASSERT(false);
  }
  m_defaultColor = *pColor;
}

OdIntPtr OdTvVisualizeNwFilerProperties::getDefaultNwColor() const
{
  return (OdIntPtr)(&m_defaultColor);
}

void OdTvVisualizeNwFilerProperties::setAppendTransform(OdIntPtr pTransform)
{
  const OdTvMatrix* pAppendTransform = (const OdTvMatrix*)(pTransform);

  if (pAppendTransform)
  {
    m_appendTransform = *pAppendTransform;
  }
  else
  {
    m_appendTransform = OdTvMatrix::kIdentity;
  }
}

OdIntPtr OdTvVisualizeNwFilerProperties::getAppendTransform() const
{
  return (OdIntPtr)(&m_appendTransform);
}

//***************************************************************************//
// 'OdTvVisualizeNwFiler' methods implementation
//***************************************************************************//

OdTvMaterialId OdTvVisualizeNwFiler::importMaterial(const OdTvDatabaseId& tvDbId, OdNwMaterialPtr pNwMtl) const
{
  if (pNwMtl.isNull())
    return OdTvMaterialId();
  OdTvResult rc;
  OdTvMaterialId materialId;
  OdTvDatabasePtr pTvDb = tvDbId.openObject(OdTv::kForWrite);

  OdString mtlNameGen = pNwMtl->getName() + OdString().format(OD_T("_Generated_%u"), pNwMtl->getInt64StableId());
  materialId = pTvDb->findMaterial(mtlNameGen, &rc);

  if (rc != eOk)
    materialId = pTvDb->createMaterial(mtlNameGen, &rc);

  if (materialId.isNull())
    return materialId;

  OdTvMaterialPtr pMaterial = materialId.openObject(OdTv::kForWrite);

  OdNwTexturePtr pNwTxt;
  if (pNwMtl->isA() == OdNwTexture::desc())
    pNwTxt = pNwMtl;

  OdNwColor tempColor;
  if (pNwMtl->getDiffuse(tempColor) == eOk)
  {
    OdTvMaterialMap materialMap;
    if (!pNwTxt.isNull())
    {
      OdString sTextureFName = pNwTxt->getStringValue(NwTextureValueType::diffuse_path);
      OdTvTextureMapper txtMap;
      NwModelUnits::Enum DbUnits = pNwMtl->objectId().database()->getUnits();
      if (!sTextureFName.isEmpty())
      {
        txtMap.setSampleSize(OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::diffuse_scale_x_value), pNwTxt->getUnitValue(NwTextureValueType::diffuse_scale_x_unit), DbUnits),
                             OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::diffuse_scale_y_value), pNwTxt->getUnitValue(NwTextureValueType::diffuse_scale_y_unit), DbUnits));
        txtMap.setOffset(OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::diffuse_offset_x_value), pNwTxt->getUnitValue(NwTextureValueType::diffuse_offset_x_unit), DbUnits),
                         OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::diffuse_offset_y_value ), pNwTxt->getUnitValue(NwTextureValueType::diffuse_offset_y_unit), DbUnits));
      }
      else
      {
        sTextureFName = pNwTxt->getStringValue(NwTextureValueType::pattern_path);

        if (!sTextureFName.isEmpty())
        {
          txtMap.setSampleSize(OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::pattern_scale_x_value), pNwTxt->getUnitValue(NwTextureValueType::pattern_scale_x_unit), DbUnits),
                               OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::pattern_scale_y_value), pNwTxt->getUnitValue(NwTextureValueType::pattern_scale_y_unit), DbUnits));
          txtMap.setOffset(OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::pattern_offset_x_value), pNwTxt->getUnitValue(NwTextureValueType::pattern_offset_x_unit), DbUnits),
                           OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::pattern_offset_y_value), pNwTxt->getUnitValue(NwTextureValueType::pattern_offset_y_unit), DbUnits));
        }
        else
        {
          sTextureFName = pNwTxt->getStringValue(NwTextureValueType::bump_path);

          if (!sTextureFName.isEmpty())
          {
            txtMap.setSampleSize(OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::bump_scale_x_value), pNwTxt->getUnitValue(NwTextureValueType::bump_scale_x_unit), DbUnits),
                                 OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::bump_scale_y_value), pNwTxt->getUnitValue(NwTextureValueType::bump_scale_y_unit), DbUnits));
            txtMap.setOffset(OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::bump_offset_x_value), pNwTxt->getUnitValue(NwTextureValueType::bump_offset_x_unit), DbUnits),
                             OdNwUnitUtils::convert(pNwTxt->getDoubleValue(NwTextureValueType::bump_offset_y_value), pNwTxt->getUnitValue(NwTextureValueType::bump_offset_y_unit), DbUnits));
          }
        }
      }

      if (sTextureFName.find((OdChar)'.') > 0)
      {
        OdString sFName = nwExtractFileName(sTextureFName);
        OdTvRasterImageId rasterImageId = pTvDb->findRasterImage(sFName, &rc);
        if (rc != eOk)
        {
          OdGiRasterImagePtr pRI = pNwTxt->getRasterImageValue(NwTextureValueType::diffuse_raster_image);
          if (pRI.isNull())
            pRI = pNwTxt->getRasterImageValue(NwTextureValueType::pattern_raster_image);

          if (!pRI.isNull())
          {
            OdBinaryData bufData;
            bufData.resize(pRI->pixelHeight() * pRI->scanLineSize());
            pRI->scanLines(const_cast<OdUInt8*>(bufData.asArrayPtr()), 0, pRI->pixelHeight());

            if (pRI->numColors() == 0)
            {
              OdTvRasterImage::Format riFrmt;
              if (pRI->pixelFormat().isRGBA())
                riFrmt = OdTvRasterImage::kRGBA;
              else if (pRI->pixelFormat().isRGB())
                riFrmt = OdTvRasterImage::kRGB;
              else if (pRI->pixelFormat().isBGRA())
                riFrmt = OdTvRasterImage::kBGRA;
              else if (pRI->pixelFormat().is16bitBGR())
                riFrmt = OdTvRasterImage::kBGR_16;
              else if (pRI->pixelFormat().isBGR())
                riFrmt = OdTvRasterImage::kBGR;

              rasterImageId = pTvDb->createRasterImage(sFName, riFrmt, pRI->pixelWidth(), pRI->pixelHeight(), const_cast<OdUInt8*>(bufData.asArrayPtr()), pRI->scanLinesAlignment(), &rc);
            }
            else if (pRI->numColors() == 2 && pRI->colorDepth() == 1)
            {
              ODCOLORREF color1 = pRI->color(0);
              ODCOLORREF color2 = pRI->color(1);
              bool bInverted = false;
              if (30 * ODGETRED(color1) + 59 * ODGETGREEN(color1) + 11 * ODGETBLUE(color1) >
                  30 * ODGETRED(color2) + 59 * ODGETGREEN(color2) + 11 * ODGETBLUE(color2))
                bInverted = false;
              else
                bInverted = true;

              rasterImageId = pTvDb->createRasterImage(sFName, pRI->pixelWidth(), pRI->pixelHeight(), const_cast<OdUInt8*>(bufData.asArrayPtr()), bInverted, pRI->scanLinesAlignment(), &rc);
            }
            else
            {
              OdTvRasterImage::Format riFrmt;
              if (pRI->pixelFormat().isRGBA())
                riFrmt = OdTvRasterImage::kRGBA;
              else if (pRI->pixelFormat().isRGB())
                riFrmt = OdTvRasterImage::kRGB;
              else if (pRI->pixelFormat().isBGRA())
                riFrmt = OdTvRasterImage::kBGRA;
              else if (pRI->pixelFormat().is16bitBGR())
                riFrmt = OdTvRasterImage::kBGR_16;
              else if (pRI->pixelFormat().isBGR())
                riFrmt = OdTvRasterImage::kBGR;

              OdBinaryData bufPaletteData;
              bufPaletteData.resize(pRI->paletteDataSize());
              pRI->paletteData(const_cast<OdUInt8*>(bufPaletteData.asArrayPtr()));

              OdTvRasterImage::BitPerIndex bpi = OdTvRasterImage::kEightBits;
              if (pRI->colorDepth() != 8)
                bpi = OdTvRasterImage::kFourBits;

              rasterImageId = pTvDb->createRasterImage(sFName, riFrmt, pRI->paletteDataSize(), const_cast<OdUInt8*>(bufPaletteData.asArrayPtr()), bpi, pRI->pixelWidth(), pRI->pixelHeight(), const_cast<OdUInt8*>(bufData.asArrayPtr()), pRI->scanLinesAlignment(), &rc);
            }
          }
        }

        if (!rasterImageId.isNull())
          materialMap.setSourceRasterImage(rasterImageId);
        else
          materialMap.setSourceFileName(sTextureFName);
      }

      materialMap.setMapper(txtMap);
    }

    double p1 = tempColor.R();
    double p2 = tempColor.G();
    double p3 = tempColor.B();

    //VAS: in texture's object there is double type parameter OdNwTexture::m_DiffuseIntensity which may need to use somehow

    OdTvMaterialColor color(OdTvColorDef(p1 * 255.0, p2 * 255.0, p3 * 255.0));

    pMaterial->setDiffuse(color, materialMap);
  }

  {
    //VAS:: in texture's object there is string type parameter OdNwTexture::m_OpacityMapPath which may need to use
    if (pNwMtl->getTransparency() > 1)
      pMaterial->setOpacity(1 - pNwMtl->getTransparency()/100);
    else
      pMaterial->setOpacity(1 - pNwMtl->getTransparency());
  }

  if (pNwMtl->getAmbient(tempColor) == eOk)
  {
    double p1 = tempColor.R();
    double p2 = tempColor.G();
    double p3 = tempColor.B();

    //VAS: in texture's object there is double type parameter OdNwTexture::m_ReflectIntensity which may need to use somehow
    //most likely ReflectIntensity is ambient intensity

    OdTvMaterialColor color(OdTvColorDef(p1 * 255.0, p2 * 255.0, p3 * 255.0));

    pMaterial->setAmbient(color);
  }

  if (pNwMtl->getSpecular(tempColor) == eOk)
  {
    double p1 = tempColor.R();
    double p2 = tempColor.G();
    double p3 = tempColor.B();

    // NB: check if shininness is gloss factor
    pMaterial->setSpecular(OdTvMaterialColor(OdTvColorDef(p1 * 255.0, p2 * 255.0, p3 * 255.0)), pNwMtl->getShininess());
    if (!pNwTxt.isNull())
      pMaterial->setSpecularGloss(pNwTxt->getSpecularIntensity());
  }

  if (pNwMtl->getEmissive(tempColor) == eOk)
  {
    double p1 = tempColor.R();
    double p2 = tempColor.G();
    double p3 = tempColor.B();
    pMaterial->setEmission(OdTvMaterialColor(OdTvColorDef(p1 * 255.0, p2 * 255.0, p3 * 255.0)));
  }

  return materialId;
}

OdTvVisualizeNwFiler::OdTvVisualizeNwFiler() : m_properties(OdTvVisualizeNwFilerProperties::createObject())
{

}

OdTvDatabaseId OdTvVisualizeNwFiler::loadFrom(const OdString& filePath, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  if (rc)
    *rc = tvOk;

  //
  // prepare Nw Importer timing object
  //
  OdTvFilerTimer timing(NULL != pProfileRes);
  timing.startTotal();

  // load Nw database
  OdStaticRxObject<OdTvNw2VisService> svcs;

  OdRxModulePtr pBimNvModule;
  try
  {
    pBimNvModule = ::odrxDynamicLinker()->loadModule(OdNwDbModuleName, false);
  }
  catch (...)
  {
    if (rc)
      *rc = tvInternal;

    timing.endTotal();
  }

  svcs.setTextureDirectoryPath(nwExtractFileDir(filePath) + OD_T("/"), false);
  OdNwDatabasePtr pNwDb = svcs.readFile(filePath);

  if (pNwDb.isNull())
  {
    if (rc)
      *rc = tvFilerEmptyInternalDatabase;

    timing.endTotal();
    return OdTvDatabaseId();
  }

  OdTvDatabaseId tvDbId = pNwDb->isComposite() ? loadNwf(pNwDb, timing, pProfileRes) : loadNwd(pNwDb, timing, pProfileRes);

  pBimNvModule.release();
  ::odrxDynamicLinker()->unloadModule(OdNwDbModuleName);

  return tvDbId;
}

static bool isFileReadable(const OdString& pcFilename)
{
  int mode = Oda::kFileRead;
#if defined(_MSC_VER) && !defined(_WIN32_WCE)
  // SetErrorMode() function is used to avoid the message box
  // if there is no floppy disk in the floppy drive (CR 2122).
  int oldErrorMode = SetErrorMode(SEM_FAILCRITICALERRORS);
  bool res = (_waccess(pcFilename,
    (GETBIT(mode, Oda::kFileRead) ? 0x04 : 0x00) |
    (GETBIT(mode, Oda::kFileWrite) ? 0x02 : 0x00)) == 0);
  SetErrorMode(oldErrorMode);
  if (res)
  {
    struct _stat st;
    _wstat(pcFilename, &st);
    if (st.st_mode & _S_IFDIR)
      return false;
  }
  return res;
#elif defined(sgi)
  // Use this version, since try/catch below results in memory leaks on sgi (OdError destructor doesn't get called).
  OdString  sMode = (mode == Oda::kFileRead) ? OD_T("rb") : OD_T("wb");
  FILE* fp = fopen((const char*)pcFilename, (const char*)sMode);
  if (fp)
  {
    fclose(fp);
    return true;
  }
  return false;
#elif defined(__linux__)
  bool res = false;
  if (GETBIT(mode, Oda::kFileRead) != 0x00) {
    std::ifstream ifile(OdAnsiString(pcFilename).c_str());
    res = (bool)ifile;
  }
  if (res && (GETBIT(mode, Oda::kFileWrite) != 0x00)) {
    std::ofstream ofile(OdAnsiString(pcFilename).c_str());
    res = (bool)ofile;
  }
  return res;
#elif defined(__APPLE__)
  struct stat buf;
  return (stat(OdAnsiString(pcFilename).c_str(), &buf) == 0);
#else
  ODA_ASSERT(false);
#endif
}

static OdString nwSanitizePath(const OdString& path)
{
  OdString sToFindSanitized = path;
  sToFindSanitized.replace('\\', '/');    // win to lin
  return sToFindSanitized;
}

static OdString nwExtractFileName(const OdString& path)
{
  // get filename with extension
  OdString sToFindSanitized = nwSanitizePath(path);
  const int lastDelimiter = sToFindSanitized.reverseFind('/');

  if (-1 == lastDelimiter)
    return path;                   // no delimiter

  return sToFindSanitized.mid(lastDelimiter + 1);
}

static OdString nwExtractFileDir(const OdString& path)
{
  // get filename with extension
  OdString sToFindSanitized = nwSanitizePath(path);
  const int lastDelimiter = sToFindSanitized.reverseFind('/');

  if (-1 == lastDelimiter)
    return L"";                   // no folder

  return sToFindSanitized.left(lastDelimiter);
}

static OdResult findPathToFile(const OdString& sCurrentPath, OdString &sFileToFind)
{
  // if original file exists return it
  if (isFileReadable(sFileToFind))
    return eOk;

  // get filename with extension
  OdString sToFindSanitized = nwSanitizePath(sFileToFind);
  const int lastDelimiter = sToFindSanitized.reverseFind('/');


  if (-1 == lastDelimiter)
    return eFileAccessErr;                   // no delimiter - no file

  // At this moment there is no file in current folder
  // gte name ie. test.txt
  OdString sStrippedName = nwExtractFileName(sToFindSanitized);

  if (sStrippedName.isEmpty())
    return eFileAccessErr;

  OdString sFileInCurrentPath = (sCurrentPath);
  if (sFileInCurrentPath.isEmpty()
    || ('/' != sFileInCurrentPath[sFileInCurrentPath.getLength() - 1]))
  {
    sFileInCurrentPath += "/";
  }

  sFileInCurrentPath += sStrippedName;
  const bool bFound = isFileReadable(sFileInCurrentPath);
  if (bFound)
  {
    sFileToFind = sFileInCurrentPath;
  }
  return bFound ? eOk : eFileAccessErr;
}

OdResult OdTvVisualizeNwFiler::getGeometryNodesByRoot(OdNwModelItemPtr pRoot, OdArray<OdNwModelItemPtr>& aHasGeomNodes) const
{
  OdArray<OdNwModelItemPtr> aRootChildren;
  OdResult res = pRoot->getChildren(aRootChildren);
  if (res!= eOk)
    return res;
  if (pRoot->hasGeometry())
    aHasGeomNodes.push_back(pRoot);
  for (OdArray<OdNwModelItemPtr>::const_iterator itRootChildren = aRootChildren.begin(); itRootChildren != aRootChildren.end(); ++itRootChildren)
  {
    res = getGeometryNodesByRoot(*itRootChildren, aHasGeomNodes);
    if (res != eOk)
      return res;
  }

  return eOk;
}

static OdString convertFormatToNwc(const OdString& pathToFile)
{
  OdString res;

  if (pathToFile.find('.') > 0)
    res = pathToFile.left(pathToFile.reverseFind('.')) + OD_T(".nwc");

  return res;
}

OdTvDatabaseId OdTvVisualizeNwFiler::loadNwf(OdNwDatabasePtr pNwDb, OdTvFilerTimer &timing, OdTvFilerTimeProfiling* pProfileRes) const
{
  OdTvDatabaseId tvDbId;

  //
  // prepare Visualize timing object
  //
  OdPerfTimerBase* pTvTimer = 0;
  double nTotalTvTime = 0.;
  bool bUseTimeProfiling = (NULL != pProfileRes);
  if (bUseTimeProfiling)
    pTvTimer = OdPerfTimerBase::createTiming();

  START_TV_NW_TIMER

  // create tv database
  OdTvFactoryId tvFactoryId = odTvGetFactory();
  tvDbId = tvFactoryId.createDatabase();

  //create tv model
  OdTvDatabasePtr pTvDb = tvDbId.openObject(OdTv::kForWrite);

  OdNwViewpointPtr pCurView = pNwDb->getCurrentView();
  OdTvGsViewPtr pTvView;
  if (!pCurView.isNull())
  {
    OdTvGsViewBackgroundId bkgId = setupBackground(pNwDb->getBackgroundElement(), tvDbId);
    OdTvGsDeviceId tvDeviceId = pTvDb->createDevice(L"Window_NW");
    OdTvGsViewId tvViewId = setupCurrentView(pCurView, tvDeviceId, OdTvModelId(), bkgId);
    pTvView = tvViewId.openObject(OdTv::kForWrite);
    pTvView->enableDefaultLighting(false);
  }

  //2. Prepare profiling structure
  OdTvViewerDatabaseInfo databaseInfo;
  databaseInfo.setFilePath(pNwDb->getFilename().c_str());
  databaseInfo.setType(OdTvViewerDatabaseInfo::kImport);

  END_TV_NW_TIMER

  OdArray<OdNwModelPtr> aModels;
  pNwDb->getModels(aModels);

  OdString strBaseFileDir = nwExtractFileDir(pNwDb->getFilename());
  OdTvModelId lastTvModelId;
  NwModelUnits::Enum databaseUnits = pNwDb->getUnits();

  for (OdArray<OdNwModelPtr>::const_iterator it = aModels.begin();
    it != aModels.end(); ++it)
  {
    OdNwModelPtr pModel = (*it);
    if (!pModel.isNull())
    {
      OdGeMatrix3d trf = pModel->getTransform();
      //if (pModel->getUnits() != databaseUnits)
      //  OdNwUnitUtils::convertMatrix3d(trf, databaseUnits, pModel->getUnits());
      trf(3, 3) = 1.0;  //  element must be 1!
      OdTvDCRect rectDC = *(OdTvDCRect*) m_properties->getDCRect();
      OdString strSubFilePath = pModel->getPath();
      NwModelType::Enum modelType = pModel->getType();
      bool bFileIsFound = (eOk == findPathToFile(strBaseFileDir, strSubFilePath));
      bool bIsChangeToNwc = false;
      if (!bFileIsFound)
      {
        strSubFilePath = convertFormatToNwc(strSubFilePath);
        bFileIsFound = (eOk == findPathToFile(strBaseFileDir, strSubFilePath));
        if (bFileIsFound)
        {
          modelType = NwModelType::nw_model;
          bIsChangeToNwc = true;
        }
      }
      ODA_ASSERT(bFileIsFound);
      if (bFileIsFound)
      {
        OdTvResult res = tvOk;
        switch (modelType)
        {
        case NwModelType::dwg_model:
        {
          OdTvDwgAppendParams oImportParams;
          oImportParams.setTransform(trf);
          // fill base fields in param
          oImportParams.setFilePath(strSubFilePath.c_str());
          oImportParams.setProfiling(&databaseInfo);
          oImportParams.setDCRect(rectDC);
          oImportParams.setNeedCDATree(m_properties->getNeedCDATree());
          oImportParams.setNeedCollectPropertiesInCDA(m_properties->getNeedCollectPropertiesInCDA());
          lastTvModelId = pTvDb->append(&oImportParams, &res);
        }
        break;
        case NwModelType::revit_model:
        {
          OdTvBimAppendParams oImportParams;
          oImportParams.setTransform(trf);
          oImportParams.setFilePath(strSubFilePath.c_str());
          oImportParams.setProfiling(&databaseInfo);
          oImportParams.setDCRect(rectDC);
          oImportParams.setNeedCDATree(m_properties->getNeedCDATree());
          oImportParams.setNeedCollectPropertiesInCDA(m_properties->getNeedCollectPropertiesInCDA());
          lastTvModelId = pTvDb->append(&oImportParams, &res);
        }
        break;
        case NwModelType::dgn_model:
        {
          OdTvDgnAppendParams oImportParams;
          oImportParams.setTransform(trf);
          oImportParams.setFilePath(strSubFilePath.c_str());
          oImportParams.setProfiling(&databaseInfo);
          oImportParams.setDCRect(rectDC);
          oImportParams.setNeedCDATree(m_properties->getNeedCDATree());
          oImportParams.setNeedCollectPropertiesInCDA(m_properties->getNeedCollectPropertiesInCDA());
          lastTvModelId = pTvDb->append(&oImportParams, &res);
        }
        break;
        case NwModelType::nw_model:
        {
          OdTvNwAppendParams oImportParams;
          oImportParams.setTransform(trf);
          oImportParams.setFilePath(strSubFilePath.c_str());
          oImportParams.setProfiling(&databaseInfo);
          oImportParams.setDCRect(rectDC);
          oImportParams.setNeedCDATree(m_properties->getNeedCDATree());
          oImportParams.setNeedCollectPropertiesInCDA(m_properties->getNeedCollectPropertiesInCDA());
          oImportParams.setChangeToNwc(bIsChangeToNwc);
          lastTvModelId = pTvDb->append(&oImportParams, &res);
        }
        break;
        default:
          break;
        }
        ODA_ASSERT(tvOk == res);
      } // if (isFileFound)
    }
    if (!pTvView.isNull() && !lastTvModelId.isNull())
      pTvView->addModel(lastTvModelId);
  }
  
  return tvDbId;
}

OdTvDatabaseId OdTvVisualizeNwFiler::loadNwd(OdNwDatabasePtr pNwDb, OdTvFilerTimer &timing, OdTvFilerTimeProfiling* pProfileRes) const
{
  OdTvDatabaseId tvDbId;

  timing.startVectorizing();

  // get default color from properties
  ODCOLORREF* pDefColor = (ODCOLORREF*)m_properties->getDefaultNwColor();
  OdUInt8 r, g, b;
  r = ODGETRED(*pDefColor);
  g = ODGETGREEN(*pDefColor);
  b = ODGETBLUE(*pDefColor);

  OdNwModelItemPtr pModelItemRoot = pNwDb->getModelItemRoot();

  //generate model name
  OdString modelName = OD_T("model1"); // TODO: get Model Name
  if (!pModelItemRoot.isNull())
  {
    if (!pModelItemRoot->getDisplayName().isEmpty())
      modelName = pModelItemRoot->getDisplayName();
    else if (!pModelItemRoot->getClassDisplayName().isEmpty())
      modelName = pModelItemRoot->getClassDisplayName();
    else if (!pModelItemRoot->getClassName().isEmpty())
      modelName = pModelItemRoot->getClassName();
  }
  modelName.replace('.', '_');

  //
  // prepare Visualize timing object
  //
  OdPerfTimerBase* pTvTimer = 0;
  double nTotalTvTime = 0.;
  bool bUseTimeProfiling = (NULL != pProfileRes);
  if (bUseTimeProfiling)
    pTvTimer = OdPerfTimerBase::createTiming();

  START_TV_NW_TIMER

  // create tv database
  OdTvFactoryId tvFactoryId = odTvGetFactory();
  tvDbId = tvFactoryId.createDatabase();

  //create tv model
  OdTvDatabasePtr pTvDb = tvDbId.openObject(OdTv::kForWrite);
  OdTvModelId tvModelId = pTvDb->createModel(modelName);

  END_TV_NW_TIMER

  OdTvGeomCollector wd(bUseTimeProfiling);

  setupGeometry(pModelItemRoot, tvModelId, pTvTimer, nTotalTvTime);

  OdArray<OdNwCommonLightPtr> aLights;
  if (pNwDb->getLightElements(aLights) == eOk)
    setupLights(tvModelId, aLights);

  START_TV_NW_TIMER

  //here we also will create a device and view, becuase we want the Shaded initial mode
  OdTvGsDeviceId tvDeviceId = pTvDb->createDevice(L"Window_NW");

  OdTvGsViewBackgroundId bkgId = setupBackground(pNwDb->getBackgroundElement(), tvDbId);
  setupCurrentView(pNwDb->getCurrentView(), tvDeviceId, tvModelId, bkgId);

  END_TV_NW_TIMER

  timing.startMisc();

  if (tvDbId.isValid()/* && m_properties->getClearEmptyObjects()*/)
    OdTvDatabaseCleaner::cleanTvDatabase(tvDbId);

  timing.endMisc();

  timing.endVectorizing();

  if (m_properties->getNeedCDATree())
  {
    timing.startMisc();

    createCommonDataAccessTree(pTvDb, pNwDb, pNwDb->getFilename() + OD_T(".nwd"));

    timing.endMisc();

    if (pProfileRes)
      pProfileRes->setCDATreeCreationTime(OdInt64((timing.getMiscTime()) * 1000.));
  }

  //unload BimNv modules
  //here we will unload all including Visualize device
  odrxDynamicLinker()->unloadUnreferenced();

  timing.endTotal();

  if (pProfileRes)
  {
    pProfileRes->setImportTime(OdInt64((timing.getTotalTime() - timing.getVectorizingTime()) * 1000.));
    pProfileRes->setVectorizingTime(OdInt64((timing.getVectorizingTime()) * 1000.));
#if !defined(__APPLE__)
    pProfileRes->setTvTime(OdInt64(nTotalTvTime * 1000.));
#endif
  }

  return tvDbId;
}

OdTvDatabaseId OdTvVisualizeNwFiler::loadFrom(OdStreamBuf* pBuffer, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  if (rc)
    *rc = tvNotImplementedYet;

  return OdTvDatabaseId();
}

OdTvDatabaseId OdTvVisualizeNwFiler::loadFrom(OdDbBaseDatabase* pDatabase, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  if (rc)
    *rc = tvNotImplementedYet;

  return OdTvDatabaseId();
}


OdTvModelId OdTvVisualizeNwFiler::appendFrom(const OdTvDatabaseId& databaseId, OdTvVisualizeNwFilerDbSource *pNwDatabaseSource, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  OdTvModelId tvModelId;

  if (rc)
    *rc = tvOk;

  OdTvGsDeviceId activeTvGsDeviceId;
  {
    OdTvDatabasePtr pTvDb = databaseId.openObject();
    OdTvDevicesIteratorPtr devicesIteratorPtr = pTvDb->getDevicesIterator();
    while (!devicesIteratorPtr->done())
    {
      activeTvGsDeviceId = devicesIteratorPtr->getDevice();
      if (activeTvGsDeviceId.openObject()->getActive())
        break;
    }
  }

  //get palette, background and device size
  ODCOLORREF background = ODRGB(0, 0, 0);
  OdTvDCRect rect(0, 0, 0, 0);
  OdArray<ODCOLORREF, OdMemoryAllocator<ODCOLORREF> > pPalCpy;

  if (!activeTvGsDeviceId.isNull())
  {
    OdTvGsDevicePtr pTvDevice = activeTvGsDeviceId.openObject();

    background = pTvDevice->getBackgroundColor();
    pTvDevice->getSize(rect);

    int nColors;
    const ODCOLORREF* pPalette = pTvDevice->getLogicalPalette(nColors);
    if (nColors >= 256)
      pPalCpy.insert(pPalCpy.begin(), pPalette, pPalette + 256);
    else
    {
      pPalCpy.insert(pPalCpy.begin(), pPalette, pPalette + nColors);
      pPalCpy.insert(pPalCpy.begin() + nColors, 256 - nColors, ODRGB(0, 0, 0));
    }
  }

  //collcet current models and blocks
  std::set<OdTvGsViewId> foreignViews;
  std::set<OdTvModelId> foreignModels;
  std::set<OdTvBlockId> foreignBlocks;
  OdTvDatabaseUtinls::collectViewsModelsAndBlocks(databaseId, foreignViews, foreignModels, foreignBlocks);

  //check that time profiling is need
  bool bUseTimeProfiling = false;
  if (pProfileRes)
    bUseTimeProfiling = true;
  double internalTiming = 0.;
  double externalTiming = 0;

  //prepare timing object
  OdTvFilerTimer timing(bUseTimeProfiling);
  timing.startTotal();

  // Generate model name
  OdString modelName = OdTvDatabaseUtinls::generateModelName(databaseId, pNwDatabaseSource->getFilename());

  //store visualize device module name
  OdString moduleName;
  try
  {
    OdNwDatabasePtr pNwDb = pNwDatabaseSource->getDb();

    if (!pNwDb.isNull())
    {
      timing.startVectorizing();

      OdNwModelItemPtr pModelItemRoot = pNwDb->getModelItemRoot();

      //
      // prepare Visualize timing object
      //
      OdPerfTimerBase* pTvTimer = 0;
      double nTotalTvTime = 0.;
      bool bUseTimeProfiling = (NULL != pProfileRes);
      if (bUseTimeProfiling)
        pTvTimer = OdPerfTimerBase::createTiming();

      START_TV_NW_TIMER
      
      //create tv model
      OdTvDatabasePtr pTvDb = databaseId.openObject(OdTv::kForWrite);
      tvModelId = pTvDb->createModel(modelName);
      
      END_TV_NW_TIMER

      OdTvGeomCollector wd(bUseTimeProfiling);

      setupGeometry(pModelItemRoot, tvModelId, pTvTimer, nTotalTvTime);

      //OdArray<OdNwCommonLightPtr> aLights;
      //if (pNwDb->getLightElements(aLights) == eOk)
      //  setupLights(tvModelId, aLights);

      START_TV_NW_TIMER

      if (activeTvGsDeviceId.isNull())
      {
        OdTvDatabasePtr bTvDb = databaseId.openObject();
        activeTvGsDeviceId = bTvDb->createDevice(OD_T("Window_NW"));
      }

      {
        OdTvGsDevicePtr pTvDevice = activeTvGsDeviceId.openObject(OdTv::kForWrite);
        if (pTvDevice->numViews() == 0)
        {
          OdTvGsViewBackgroundId bkgId = setupBackground(pNwDb->getBackgroundElement(), databaseId);
          setupCurrentView(pNwDb->getCurrentView(), activeTvGsDeviceId, tvModelId, bkgId);
        }
      }

      END_TV_NW_TIMER

      //pTvDevice->update(&rect);

      if (databaseId.isValid())
      {
        timing.startMisc();

        OdTvDatabaseId& tvDvId = const_cast<OdTvDatabaseId&>(databaseId);
        OdTvDatabaseCleaner::cleanTvDatabaseForAppend(tvDvId, foreignViews, foreignModels, foreignBlocks);

        //apply transform if need 
        OdTvMatrix* pTransfrom = (OdTvMatrix*)m_properties->getAppendTransform();
        if (pTransfrom)
        {
          OdTvDatabaseId& tvDvId = const_cast<OdTvDatabaseId&>(databaseId);
          if (m_properties->getChangeToNwc())
            OdTvDatabaseUtinls::applyTransformToTheModel(tvDvId, modelName, (*pTransfrom) * pNwDb->getModelTransform().invert());
          else
            OdTvDatabaseUtinls::applyTransformToTheModel(tvDvId, modelName, *pTransfrom);
        }

        timing.endMisc();
      }

      // save file name to database user data
      OdTvDatabaseUtinls::writeFileNameToTvDatabase(databaseId, pNwDatabaseSource->getFilename());

      //initialize cache and switch it to the "write only" mode
      ExGsVisualizeDeviceCache deviceCache;
      deviceCache.m_bApplyCacheData = false;

      externalTiming = timing.getMiscTime();

      timing.endVectorizing();

      if (m_properties->getNeedCDATree())
      {
        timing.startMisc();

        OdTvDatabasePtr pTvDb = databaseId.openObject();
        createCommonDataAccessTree(pTvDb, pNwDb, pNwDatabaseSource->getFilename() + OD_T(".nwd"));

        timing.endMisc();

        if (pProfileRes)
          pProfileRes->setCDATreeCreationTime(OdInt64((timing.getMiscTime()) * 1000.));
      }

    }
    else
    {
      if (rc)
        *rc = tvFilerEmptyInternalDatabase;
    }
  }
  catch (...)
  {
    if (rc)
      *rc = tvInternal;

    timing.endVectorizing();
  }

  //unload Dgn modules (try to emulate the OdUninitialized for Nw)
  //here we will unload all including Visualize device
  odrxDynamicLinker()->unloadUnreferenced();

  timing.endTotal();

  if (pProfileRes)
  {
    pProfileRes->setImportTime(OdInt64((timing.getTotalTime() - timing.getVectorizingTime() + pNwDatabaseSource->getInitTime()) * 1000.));
    pProfileRes->setVectorizingTime(OdInt64(timing.getVectorizingTime() * 1000.));
#if !defined(__APPLE__)
    pProfileRes->setTvTime(OdInt64((internalTiming + externalTiming) * 1000.));
#endif
  }

  return tvModelId;
}

void OdTvVisualizeNwFiler::setupGeometry(OdNwModelItemPtr pModelItemRoot, const OdTvModelId& tvModelId, OdPerfTimerBase* pTvTimer, double& nTotalTvTime) const
{
  // get default color from properties
  ODCOLORREF* pDefColor = (ODCOLORREF*)m_properties->getDefaultNwColor();
  OdUInt8 r, g, b;
  r = ODGETRED(*pDefColor);
  g = ODGETGREEN(*pDefColor);
  b = ODGETBLUE(*pDefColor);

  //OdTvGeomCollector wd(bUseTimeProfiling);
  OdArray<OdNwModelItemPtr> aHasGeometryNodes;
  getGeometryNodesByRoot(pModelItemRoot, aHasGeometryNodes);

  OdTvModelPtr pTvModel = tvModelId.openObject(OdTv::kForWrite);
  OdTvDatabaseId tvDbId = pTvModel->getDatabase();
  OdTvDatabasePtr pTvDb = tvDbId.openObject(OdTv::kForWrite);

  bool alreadyExist = false;
  OdTvRegAppId regAppId = pTvDb->registerAppName(OD_T("ExGsVisualizeDevice"), alreadyExist);

  if (m_properties->getNeedCDATree() || m_properties->getStoreSourceObjects())
    ::odrxDynamicLinker()->loadModule(L"NwProperties");

  for (OdArray<OdNwModelItemPtr>::iterator itHasGeomNode = aHasGeometryNodes.begin();
    itHasGeomNode != aHasGeometryNodes.end();
    ++itHasGeomNode)
  {
    OdNwModelItemPtr pNwModelItem = (*itHasGeomNode);
    OdNwComponentPtr pComp = (*itHasGeomNode)->getGeometryComponent();

    if (pComp.isNull())
      continue;

    OdArray<OdNwModelItemPtr> aAncestors;
    (*itHasGeomNode)->getAncestorsAndSelf(aAncestors);
    OdString sEntityName;
    for (OdArray<OdNwModelItemPtr>::iterator itAncestors = aAncestors.begin();
      itAncestors != aAncestors.end() && (*itAncestors) != pModelItemRoot;
      ++itAncestors)
    {
      if (itAncestors != aAncestors.begin())
        sEntityName = OD_T(" -> ") + sEntityName;

      if (!(*itAncestors)->getDisplayName().isEmpty())
        sEntityName = (*itAncestors)->getDisplayName() + sEntityName;
      else
        sEntityName = (*itAncestors)->getClassDisplayName() + sEntityName;
    }

    OdTvEntityId entId = pTvModel->appendEntity(sEntityName);

    OdTvEntityPtr entPtr = entId.openObject(OdTv::kForWrite);

    // Add native handle to user data
    if (m_properties->getNeedCDATree() || m_properties->getStoreSourceObjects())
    {
      OdRxPropertyPtr pHandleProp = OdRxMemberQueryEngine::theEngine()->find(pNwModelItem, L"ODAUniqueID");
      OdRxValue valHandle;
      if (!pHandleProp.isNull() && eOk == pHandleProp->getValue(pNwModelItem, valHandle))
      {
        OdUInt64 uinic_ID = *rxvalue_cast<OdUInt64>(&valHandle);
        OdTvByteUserData* data = new OdTvByteUserData(&uinic_ID, sizeof(OdUInt64), OdTvByteUserData::kCopyOwn, true);
        entPtr->appendUserData(data, regAppId);
      }
    }

    entPtr->setColor(OdTvColorDef(r, g, b));

    OdNwMaterialPtr pMaterial = pComp->getMaterial();
    OdTvMaterialId matId = importMaterial(pTvModel->getDatabase(), pMaterial);
    entPtr->setMaterial(matId);

    OdArray<OdNwFragmentPtr> CompFrags;
    pComp->getFragments(CompFrags);
    OdUInt32 fragInd = 0;
    for (OdArray<OdNwFragmentPtr>::const_iterator itFrag = CompFrags.begin();
      itFrag != CompFrags.end();
      ++itFrag, ++fragInd)
    {
      OdNwFragmentPtr pFrag = *itFrag;
      OdGeMatrix3d trMat = pFrag->getTransformation();
      entPtr->setModelingMatrix(trMat);
      OdTvMapperDef matMap = entPtr->getMaterialMapper();

      if (pFrag->isLineSet())
      {
        OdNwGeometryLineSetPtr pGeomertyLineSet = pFrag->getLineSet();
        ODA_ASSERT(!pGeomertyLineSet.isNull());
        const OdGePoint3dArray& Vertexes = pGeomertyLineSet->getVertexes();
        const OdArray<OdUInt16>& VertexPerLine = pGeomertyLineSet->getVertexCountPerLine();
      
        OdInt32 nCurrentLineVertex = 0;
        for (OdArray<OdUInt16>::const_iterator it = VertexPerLine.begin();
          it != VertexPerLine.end(); ++it)
        {
          // vertex count for current line
          const OdInt16 nVertexPerLine = *it;
          entPtr->appendPolyline(nVertexPerLine, &Vertexes[nCurrentLineVertex]);

          // shift vertex index to the start position of next line
          nCurrentLineVertex += nVertexPerLine;
        }
      }
      else if (pFrag->isEllipse())
      {
        START_TV_NW_TIMER
          OdNwGeometryEllipticalShapePtr pGeometryEllipticalShape = pFrag->getEllipse();

          // TODO: check this equation!
          entPtr->appendEllipse(pGeometryEllipticalShape->getOrigin(),
          pGeometryEllipticalShape->getXVector().asPoint() * pGeometryEllipticalShape->getXRadius(),
          pGeometryEllipticalShape->getYVector().asPoint() * pGeometryEllipticalShape->getYRadius());

          matMap.setProjection(OdTvMapperDef::kSphere);

        END_TV_NW_TIMER
      }
      else if(pFrag->isMesh())
      {
#ifdef NW_DRAW_MESH_AS_POINTSET
        START_TV_NW_TIMER
          OdNwGeometryMeshPtr pGeometryMesh;
          pFrag->getMesh(pGeometryMesh);
          const OdGePoint3dArray& vertices = pGeometryMesh->getVertexes();
          entPtr->appendPointCloud(vertices.size(), vertices.asArrayPtr());
        END_TV_NW_TIMER
#endif
                
        START_TV_NW_TIMER
             
        OdNwGeometryMeshPtr pGeometrMesh = pFrag->getMesh();

        const OdGePoint3dArray& vertices = pGeometrMesh->getVertexes();
        const OdArray<OdUInt16>& vertexPerFace = pGeometrMesh->getVertexCountPerFace();
        OdInt32Array faces;

#ifdef NW_USE_TRIANGULATION

        OdArray<OdArray<OdInt32> > prep_faces;
        pGeometrMesh->prepareFaces(prep_faces);
        for (OdArray<OdArray<OdInt32> >::const_iterator itFaces = prep_faces.begin()
          ; itFaces != prep_faces.end(); ++itFaces)
        {
          const OdArray<OdInt32>& triangulated_face = *itFaces;

          faces.push_back(triangulated_face.size());
          for (OdArray<OdInt32>::const_iterator itTriangFaces = triangulated_face.begin();
            itTriangFaces != triangulated_face.end(); ++itTriangFaces)
          {
            faces.push_back(*itTriangFaces - 1);
          }
        }
#else
              OdArray<OdArray<OdInt32> > faces2;
              pGeometrMesh->prepareFaces(faces2);

              OdArray<OdUInt16>& aIndexes = pGeometrMesh->getIndexes();

              if (0 == aIndexes.size())
              {
                OdInt32 nFacesArraySize = 0;
                for (OdArray<OdUInt16>::const_iterator it = vertexPerFace.begin(); it != vertexPerFace.end(); ++it)
                {
                  nFacesArraySize += *it;
                }
                ODA_ASSERT(nFacesArraySize == vertices.size());
                nFacesArraySize += vertexPerFace.size();
                faces.reserve(nFacesArraySize);

                OdInt32 currentIndex = 0;
                for (OdArray<OdUInt16>::const_iterator it = vertexPerFace.begin(); it != vertexPerFace.end(); ++it)
                {
                  const OdUInt16 nVertexCount = *it;
                  ODA_ASSERT_ONCE(currentIndex + nVertexCount <= vertices.size());

                  if (currentIndex + nVertexCount > vertices.size())
                    break;

                  faces.push_back(nVertexCount);


                  // 3-1-0-2 strategy
                  OdInt16 del = nVertexCount % 2 ? 2 : 1;
                  for (OdInt16 nFaceVertex = nVertexCount - del; nFaceVertex > 0; nFaceVertex -= 2)
                  {
                    faces.push_back(currentIndex + nFaceVertex);
                  }
                  for (OdInt16 nFaceVertex = 0; nFaceVertex < nVertexCount; nFaceVertex += 2)
                  {
                    faces.push_back(currentIndex + nFaceVertex);
                  }
                  currentIndex += nVertexCount;
                }
              }
              else
              {
                // check indexes size and faces size
                OdInt32 nFacesArraySize = 0;
                for (OdArray<OdUInt16>::const_iterator it = vertexPerFace.begin(); it != vertexPerFace.end(); ++it)
                {
                  nFacesArraySize += *it;
                }
                ODA_ASSERT(nFacesArraySize == aIndexes.size());
                
                faces.reserve(aIndexes.size() + vertexPerFace.size());
                OdInt32 currentIndex = 0;

                for (OdArray<OdUInt16>::const_iterator it = vertexPerFace.begin(); it != vertexPerFace.end(); ++it)
                {
                  const OdUInt16 nVertexCount = *it;

                  ODA_ASSERT(currentIndex + nVertexCount <= aIndexes.size());
                  if (currentIndex + nVertexCount <= aIndexes.size())
                  {
                    faces.push_back(nVertexCount);

                    // 3-1-0-2 strategy
                    OdInt16 del = nVertexCount % 2 ? 2 : 1;
                    // odd
                    for (OdInt16 nFaceVertex = nVertexCount - del; nFaceVertex > 0; nFaceVertex -= 2)
                    {
                      const OdUInt16 nIndex = aIndexes[currentIndex + nFaceVertex];
                      if (nIndex < vertices.size())
                        faces.push_back(nIndex);
                    }
                    // even
                    for (OdInt16 nFaceVertex = 0; nFaceVertex < nVertexCount; nFaceVertex += 2)
                    {
                      const OdUInt16 nIndex = aIndexes[currentIndex + nFaceVertex];
                      if (nIndex < vertices.size())
                        faces.push_back(nIndex);
                    }
                  }
                  
                  currentIndex += nVertexCount; 
                }
                ODA_ASSERT(currentIndex == aIndexes.size());

              }
#endif // !NW_USE_TRIANGULATION
        OdTvGeometryDataId shellId = entPtr->appendShell(vertices.size(), vertices.asArrayPtr(), faces.size(), faces.asArrayPtr());

        matMap.setProjection(OdTvMapperDef::kBox);

        //set normals to the shell
        OdTvShellDataPtr shellPtr = shellId.openAsShell();

        const OdArray<OdGeVector3d>& aNormales = pGeometrMesh->getNormales();
        if (aNormales.size() == vertices.size())
        {
          shellPtr->setVertexNormalsViaRange(0, (OdInt32)aNormales.size(), aNormales.getPtr());
        }
        else
        {
          // TODO: Calculate normals by visualize or something else
          ;;
        }
        END_TV_NW_TIMER
      }
      else if (pFrag->isPointSet())
      {
        START_TV_NW_TIMER
          OdNwGeometryPointSetPtr pGeometryPointSet = pFrag->getPointSet();
          const OdGePoint3dArray& vertices = pGeometryPointSet->getVertexes();
          entPtr->appendPointCloud(vertices.size(), vertices.asArrayPtr());
        END_TV_NW_TIMER
      }
      else if(pFrag->isText())
      {
        START_TV_NW_TIMER
          OdNwGeometryTextPtr pGeometrText = pFrag->getText();
          // TODO: NwGeometryText has more params than "appendText" method receives
          entPtr->appendText(pGeometrText->getLeftPoint(), pGeometrText->getText());
        END_TV_NW_TIMER
      }
      else if (pFrag->isTube())
      {
        // TODO: implement
      }
      else
      {
        ODA_ASSERT(false);
      }

      entPtr->setMaterialMapper(matMap);
    }
  }

  if (m_properties->getNeedCDATree() || m_properties->getStoreSourceObjects())
    ::odrxDynamicLinker()->unloadModule(L"NwProperties");
}

OdTvGsViewBackgroundId OdTvVisualizeNwFiler::setupBackground(OdNwBackgroundElementPtr pBackGround, const OdTvDatabaseId& tvDbId) const
{
  OdTvGsViewBackgroundId tvBckId;

  if (pBackGround.isNull())
    return tvBckId;

  OdTvDatabasePtr pTvDb = tvDbId.openObject(OdTv::kForWrite);

  switch(pBackGround->getBackgroundType())
  {
    case NwBackgroundType::HORIZON:
    {
      tvBckId = pTvDb->createBackground(OD_T("HorizonColor"), OdTvGsViewBackgroundId::kGradient);
      if (!tvBckId.isNull())
      {
        OdTvGsViewGradientBackgroundPtr pGradientBackground = tvBckId.openAsGradientBackground(OdTv::kForWrite);
        OdNwColor skyColor, skyHorizonColor, groundColor, groundHorizonColor;
        pBackGround->getHorizonColor(skyColor, skyHorizonColor, groundHorizonColor, groundColor);

        pGradientBackground->setColorTop(OdTvColorDef(ODGETRED(skyColor.ToColor()), ODGETGREEN(skyColor.ToColor()), ODGETBLUE(skyColor.ToColor())));
        pGradientBackground->setColorMiddle(OdTvColorDef(ODGETRED(groundHorizonColor.ToColor()), ODGETGREEN(groundHorizonColor.ToColor()), ODGETBLUE(groundHorizonColor.ToColor())));
        pGradientBackground->setColorBottom(OdTvColorDef(ODGETRED(groundColor.ToColor()), ODGETGREEN(groundColor.ToColor()), ODGETBLUE(groundColor.ToColor())));
        pGradientBackground->setHeight(0.33);
        pGradientBackground->setHorizon(0.5);
      }
      break;
    }
    case NwBackgroundType::GRADUATED:
    {
      tvBckId = pTvDb->createBackground(OD_T("GraduatedColor"), OdTvGsViewBackgroundId::kGradient);
      if (!tvBckId.isNull())
      {
        OdTvGsViewGradientBackgroundPtr pGradientBackground = tvBckId.openAsGradientBackground(OdTv::kForWrite);
        OdNwColor topColor, bottomColor, plainColor;
        pBackGround->getGraduatedColor(topColor, bottomColor);
        pBackGround->getPlainColor(plainColor);

        pGradientBackground->setColorTop(OdTvColorDef(ODGETRED(topColor.ToColor()), ODGETGREEN(topColor.ToColor()), ODGETBLUE(topColor.ToColor())));
        pGradientBackground->setColorMiddle(OdTvColorDef(ODGETRED(plainColor.ToColor()), ODGETGREEN(plainColor.ToColor()), ODGETBLUE(plainColor.ToColor())));
        pGradientBackground->setColorBottom(OdTvColorDef(ODGETRED(bottomColor.ToColor()), ODGETGREEN(bottomColor.ToColor()), ODGETBLUE(bottomColor.ToColor())));
        pGradientBackground->setHeight(0.33);
        pGradientBackground->setHorizon(0.5);
      }
      break;
    }
    case NwBackgroundType::PLAIN:
    {
      tvBckId = pTvDb->createBackground(OD_T("PlainColor"), OdTvGsViewBackgroundId::kSolid);
      if (!tvBckId.isNull())
      {
        OdTvGsViewSolidBackgroundPtr pGradientBackground = tvBckId.openAsSolidBackground(OdTv::kForWrite);
        OdNwColor plainColor;
        pBackGround->getPlainColor(plainColor);

        pGradientBackground->setColorSolid(OdTvColorDef(ODGETRED(plainColor.ToColor()), ODGETGREEN(plainColor.ToColor()), ODGETBLUE(plainColor.ToColor())));
      }
      break;
    }
    default:
      break;
  }

  return tvBckId;
}

OdTvGsViewId OdTvVisualizeNwFiler::setupCurrentView(OdNwViewpointPtr pCurView, const OdTvGsDeviceId& tvDeviceId, const OdTvModelId& tvModelId, const OdTvGsViewBackgroundId& backgroundId) const
{
  OdTvGsDevicePtr pTvDevice = tvDeviceId.openObject(OdTv::kForWrite);

  if (pCurView.isNull())
  {
    OdTvGsViewId tvViewId = pTvDevice->createView(OD_T("View_NW"));
    pTvDevice->addView(tvViewId);

    //setup view
    OdTvGsViewPtr pView = tvViewId.openObject(OdTv::kForWrite);
    if (!tvModelId.isNull())
      pView->addModel(tvModelId);

    pView->setMode(OdTvGsView::kGouraudShaded);
    pView->setView(OdTvPoint(0., 0., 1.), OdTvPoint(0., 0., 0.), OdTvVector(0., 1., 0.), 1., 1.);
    pView->setActive(true);

    // since the GS is not setup yet,we can call empty zoomtoextens to mark the view.
    // The action will be performed inside first setupGs
    OdTvPoint minPt, maxPt;
    pView->zoomExtents(minPt, maxPt);
    return tvViewId;
  }

  OdString sName = pCurView->getName();
  OdTvGsViewId tvViewId = pTvDevice->createView(sName.isEmpty() ? OdString(OD_T("View_NW")) : sName);
  pTvDevice->addView(tvViewId);

  //setup view
  OdTvGsViewPtr pView = tvViewId.openObject(OdTv::kForWrite);
  if (!tvModelId.isNull())
    pView->addModel(tvModelId);

  OdTvGsView::RenderMode tvRenderMode = OdTvGsView::k2DOptimized;
  switch (pCurView->getRenderStyle())
  {
  case NwModeType::FULL_RENDER:
  case NwModeType::SHADED:
    tvRenderMode = OdTvGsView::kGouraudShaded;
    break;
  case NwModeType::WIREFRAME:
    tvRenderMode = OdTvGsView::kWireframe;
    break;
  case NwModeType::HIDDEN_LINE:
    tvRenderMode = OdTvGsView::kHiddenLine;
    break;
  }
  pView->setMode(tvRenderMode);

  OdTvGsView::Projection tvProjection = OdTvGsView::kParallel;
  switch (pCurView->getProjection())
  {
  case NwViewType::PERSPECTIVE:
    tvProjection = OdTvGsView::kPerspective;
    break;
  case NwViewType::ORTHOGRAPHIC:
    tvProjection = OdTvGsView::kParallel;
    break;
  }

  // calculate target point
  OdGeMatrix3d matrix = pCurView->getRotation().getMatrix();
  matrix.invert();

  OdGeVector3d targetVector = OdGeVector3d::kZAxis;
  targetVector.transformBy(matrix);

  // there are files with zero focaldistance 
  if (pCurView->hasFocalDistance())
    targetVector *= pCurView->getFocalDistance();

  OdGePoint3d targetPoint = (pCurView->getPosition() - targetVector);
  pView->setView(pCurView->getPosition(), targetPoint, pCurView->getWorldUpVector(), 1., 1., tvProjection);
  pView->setActive(true);
  pView->setDefaultLightingIntensity(1.);
  if (!backgroundId.isNull())
    pView->setBackground(backgroundId);

  // since the GS is not setup yet,we can call empty zoomtoextens to mark the view.
  // The action will be performed inside first setupGs
  OdTvPoint minPt, maxPt;
  pView->zoomExtents(minPt, maxPt);

  return tvViewId;
}

void OdTvVisualizeNwFiler::setupLights(const OdTvModelId& tvModelId, const OdArray<OdNwCommonLightPtr>& aLights) const
{
  for (OdUInt32 i = 0; i < aLights.size(); ++i)
  {
    OdNwCommonLightPtr pLight = aLights[i];
    if (pLight.isNull())
      continue;
    OdString sName = pLight->getName();
    OdTvModelPtr pModel = tvModelId.openObject(OdTv::kForWrite);
    OdTvEntityId tvLightId = pModel->appendLight(sName.isEmpty() ? OdString().format(OD_T("Light_%d"), i) : sName);
    OdTvLightPtr pTvLight = tvLightId.openObjectAsLight(OdTv::kForWrite);
    pTvLight->setOn(pLight->isLightOn());
    {
      if (pLight->getValue(NwLightValueType::light_filter_color).type() == OdNwVariant::kColor)
        pTvLight->setLightColor(pLight->getDiffuse().Multiply(pLight->getValue(NwLightValueType::light_filter_color).getColor()).ToColor());
      else
        pTvLight->setLightColor(pLight->getDiffuse().ToColor());
      pTvLight->setIntensity(pLight->getValue(NwLightValueType::light_intensity_value).getDouble());
    }
    if (pLight->isKindOf(OdNwDistantLight::desc()))
    {
      OdNwDistantLightPtr pDistLight = pLight;
      pTvLight->setLightType(OdTvLight::kDistantLight);
      pTvLight->setLightDirection(pDistLight->getDirection());
    }
    else if (pLight->isKindOf(OdNwPointLight::desc()))
    {
      OdNwPointLightPtr pPointLight = pLight;
      pTvLight->setLightType(OdTvLight::kPointLight);
      pTvLight->setPosition(pPointLight->getPosition());
    }
    else if (pLight->isKindOf(OdNwSpotLight::desc()))
    {
      OdNwSpotLightPtr pSpotLight = pLight;
      pTvLight->setLightType(OdTvLight::kSpotLight);
      pTvLight->setPosition(pSpotLight->getPosition());
      pTvLight->setLightDirection(pSpotLight->getDirection());
      pTvLight->setHotspotAndFalloff(pSpotLight->getDropoffRate(), pSpotLight->getCutoffAngle());
    }
  }
}

OdTvDatabaseId OdTvVisualizeNwFiler::generate(OdTvFilerTimeProfiling* pProfileRes) const
{
  OdTvDatabaseId tvDbId;

  // does nothing
  return tvDbId;
}

OdTvModelId OdTvVisualizeNwFiler::appendFrom(const OdTvDatabaseId& databaseId, const OdString& filePath, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  OdTvVisualizeNwFilerSourceFromFile dl(filePath, pProfileRes, m_properties);
  return appendFrom(databaseId, &dl, pProfileRes, rc);
}

OdTvModelId OdTvVisualizeNwFiler::appendFrom(const OdTvDatabaseId& databaseId, OdStreamBuf* pBuffer, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  if (rc)
    *rc = tvNotImplementedYet;

  return OdTvModelId();
}

OdTvModelId OdTvVisualizeNwFiler::appendFrom(const OdTvDatabaseId& databaseId, OdDbBaseDatabase* pDatabase, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  OdTvVisualizeNwFilerSourceFromDb dl(pDatabase);
  return appendFrom(databaseId, &dl, pProfileRes, rc);
}

OdTvResult OdTvVisualizeNwFiler::startActionsWithNativeProperties(const OdString& sFilePath, bool bPartial)
{
  if (!m_pDatabaseForNativeProp.isNull() && m_pDatabaseForNativeProp->getFilename() != sFilePath)
    m_pDatabaseForNativeProp.release();
  else if (!m_pDatabaseForNativeProp.isNull())
    return tvOk;

  OdTvResult rc = tvOk;
  const_cast<OdTvVisualizeNwFiler*>(this)->m_pDl = new OdTvVisualizeNwFilerSourceFromFile(sFilePath, NULL, m_properties);

  {
    if (m_pRxPropertiesModule.isNull())
      m_pRxPropertiesModule = ::odrxDynamicLinker()->loadModule(L"RxProperties");

    if (m_pNwPropertiesModule.isNull())
      m_pNwPropertiesModule = ::odrxDynamicLinker()->loadModule(L"NwProperties");

    if (m_pNwDatabaseModule.isNull())
      m_pNwDatabaseModule = ::odrxDynamicLinker()->loadModule(OdNwDbModuleName);
    
    try
    {
      m_pDatabaseForNativeProp = m_pDl->getDb();
    }
    catch (...)
    {
      return tvFilerEmptyInternalDatabase;
    }

  }

  return rc;
}

bool OdTvVisualizeNwFiler::isActionsWithNativePropertiesStarted(const OdString& sFilePath)
{
  if (!m_pDatabaseForNativeProp.isNull() && m_pDatabaseForNativeProp->getFilename() == sFilePath)
    return true;

  return false;
}

OdTvResult OdTvVisualizeNwFiler::endActionsWithNativeProperties()
{
  if (!m_pDatabaseForNativeProp.isNull())
    m_pDatabaseForNativeProp.release();

  if (!m_pNwPropertiesModule.isNull())
  {
    m_pRxPropertiesModule.release();
    m_pNwPropertiesModule.release();
    m_pNwDatabaseModule.release();
    ::odrxDynamicLinker()->unloadModule(L"RxProperties");
    ::odrxDynamicLinker()->unloadModule(L"NwProperties");
    ::odrxDynamicLinker()->unloadModule(OdNwDbModuleName);
    ::odrxDynamicLinker()->unloadUnreferenced();

    if (m_pDl)
    {
      delete m_pDl;
      m_pDl = NULL;
    }
  }

  return tvOk;
}

OdRxMemberIteratorPtr OdTvVisualizeNwFiler::getNativePropertiesIterator(OdUInt64 dbHandle, OdTvResult* rc)
{
  if (m_pDatabaseForNativeProp.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedDatabase;
    return OdRxMemberIteratorPtr();
  }

  OdRxMemberIteratorPtr pIter;
  if (dbHandle == 0)
    pIter = OdRxMemberQueryEngine::theEngine()->newMemberIterator(m_pDatabaseForNativeProp);
  else
  {
    OdNwObjectId objectId = m_pDatabaseForNativeProp->getObjectId(dbHandle);
    if (objectId.isNull())
    {
      if (rc)
        *rc = tvNativePropMissedObject;
      return OdRxMemberIteratorPtr();
    }
    pIter = OdRxMemberQueryEngine::theEngine()->newMemberIterator(objectId.getObject());
  }

  if (pIter.isNull())
  {
    if (rc)
      *rc = tvInternal;
    return OdRxMemberIteratorPtr();
  }

  if (rc)
    *rc = tvOk;

  return pIter;
}
OdRxValue OdTvVisualizeNwFiler::getNativePropertyValue(OdUInt64 dbHandle, const OdRxPropertyPtr& pProperty, bool* bReadOnly, OdTvResult* rc)
{
  if (pProperty.isNull())
  {
    if (rc)
      *rc = tvInvalidInput;
    return OdRxValue();
  }

  if (m_pDatabaseForNativeProp.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedDatabase;
    return OdRxValue();
  }

  OdRxObjectPtr pElement;
  if (dbHandle == 0)
    pElement = m_pDatabaseForNativeProp;
  else
  {
    OdNwObjectId objectId = m_pDatabaseForNativeProp->getObjectId(dbHandle);
    pElement = objectId.getObject();
  }

  if (pElement.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedObject;
    return OdRxValue();
  }

  OdRxValue value;
  OdResult odRes = pProperty->getValue(pElement, value);
  if (bReadOnly)
    *bReadOnly = pProperty->isReadOnly(pElement);

  if (odRes != eOk)
  {
    if (rc)
      *rc = tvInternal;

    return OdRxValue();
  }

  if (rc)
    *rc = tvOk;

  return value;
}
OdRxValueIteratorPtr OdTvVisualizeNwFiler::getNativeCollectionPropertyIterator(OdUInt64 dbHandle, const OdRxCollectionPropertyPtr& pCollectionProperty, bool* bReadOnly, OdTvResult* rc)
{
  if (m_pDatabaseForNativeProp.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedDatabase;
    return OdRxValueIteratorPtr();
  }

  if (pCollectionProperty.isNull())
  {
    if (rc)
      *rc = tvInvalidInput;
    return OdRxValueIteratorPtr();
  }

  OdRxValueIteratorPtr pValIter;
  if (dbHandle == 0)
  {
    pValIter = pCollectionProperty->newValueIterator(m_pDatabaseForNativeProp);

    if (bReadOnly)
      *bReadOnly = pCollectionProperty->isReadOnly(m_pDatabaseForNativeProp);
  }
  else
  {
    OdNwObjectId objectId = m_pDatabaseForNativeProp->getObjectId(dbHandle);
    if (objectId.isNull())
    {
      if (rc)
        *rc = tvNativePropMissedObject;
      return OdRxValueIteratorPtr();
    }
    pValIter = pCollectionProperty->newValueIterator(objectId.getObject());

    if (bReadOnly)
      *bReadOnly = pCollectionProperty->isReadOnly(objectId.getObject());
  }


  if (rc)
    *rc = tvOk;

  return pValIter;
}

OdTvResult OdTvVisualizeNwFiler::setNativePropertyValue(OdUInt64 dbHandle, OdRxPropertyPtr& pProperty, const OdRxValue& value)
{
  if (pProperty.isNull())
    return tvInvalidInput;

  if (m_pDatabaseForNativeProp.isNull())
    return tvNativePropMissedDatabase;

  OdRxObjectPtr pElement;
  if (dbHandle == 0)
    pElement = m_pDatabaseForNativeProp;
  else
  {
    OdNwObjectId objectId = m_pDatabaseForNativeProp->getObjectId(dbHandle);
    pElement = objectId.getObject();
  }

  if (pElement.isNull())
    return tvNativePropMissedObject;

  pProperty->setValue(pElement, value);

  return tvOk;
}

OdDbBaseDatabase* OdTvVisualizeNwFiler::getNativeDatabase(OdTvResult* rc) const
{
  if (m_pDatabaseForNativeProp.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedDatabase;
    return NULL;
  }

  return (OdDbBaseDatabase*)m_pDatabaseForNativeProp.get();
}

void OdTvVisualizeNwFiler::createCommonDataAccessTree(OdTvDatabasePtr pTvDb, OdDbBaseDatabase *pDatabase, const OdString& strTreeName) const
{
  ::odrxDynamicLinker()->loadModule(L"NwProperties");

  //Create CDA tree
  OdTvCDATreePtr pTree = OdTvCDATree::createObject();
  pTree->createDatabaseHierarchyTree(pDatabase, m_properties->getNeedCollectPropertiesInCDA());

  //Add tree to the Tv database
  OdTvResult rc;
  OdTvCDATreeStorageId cdaTreeId = pTvDb->addCDATreeStorage(strTreeName, pTree, &rc);
  if (rc == tvAlreadyExistSameName)
  {
    OdUInt32 i = 1;
    while (rc != tvOk && i < MAX_CDATREENAME_GENERATION_ATTEMPTS)
    {
      OdString str;
      str.format(L"%s_%d", strTreeName.c_str(), i++); //not to fast but it is not a "bottle neck"
      cdaTreeId = pTvDb->addCDATreeStorage(str, pTree, &rc);
    }
  }

  //Add new CDA tree to the appropriate models
  OdTvModelsIteratorPtr modelsIterPtr = pTvDb->getModelsIterator();
  if (!modelsIterPtr->done())
  {
    OdTvModelPtr pModel = modelsIterPtr->getModel().openObject();
    if (!pModel.isNull())
    {
      pModel->setCDATreeStorage(cdaTreeId);
    }
  }
}
//***************************************************************************//
// 'OdTvVisualizeRcsFilerModule' methods implementation
//***************************************************************************//

ODRX_DEFINE_DYNAMIC_MODULE(OdTvVisualizeNwFilerModule);

void OdTvVisualizeNwFilerModule::initApp()
{
  // initialize the Visualize SDK
  odTvInitialize();
}

void OdTvVisualizeNwFilerModule::uninitApp()
{
  // Uninitialize the Visualize SDK
  odTvUninitialize();
}

OdTvVisualizeFilerPtr OdTvVisualizeNwFilerModule::getVisualizeFiler() const
{
  OdTvVisualizeFilerPtr pFiler = new OdTvVisualizeNwFiler();

  return pFiler;
}
