/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef OD_TV_VIEWERVISUALSTLESDLG_H
#define OD_TV_VIEWERVISUALSTLESDLG_H

//QT
#include <QDialog>
#include <QListWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QComboBox>
#include <QScrollArea>

//ODA Visualize Viewer
#include "TvDatabase.h"
#include "OdTvUIBaseView.h"

class OdTvUIBaseView;

/** \details
This object uses for storing the controls which should be enabled via condition
*/
class OdTvViewerVisualStylesGuidedCtrls
{
public:
  enum ConditionType
  {
    kUnknown = -1,
    kBit = 0,
    kValues = 1,
  };

  /** \details
  Constructor
  */
  OdTvViewerVisualStylesGuidedCtrls();

  /** \details
  Constructor
  */
  OdTvViewerVisualStylesGuidedCtrls(ConditionType type);

  /** \details
  Destructor
  */
  virtual ~OdTvViewerVisualStylesGuidedCtrls();

  /** \details
  Set type
  */
  void setType(ConditionType type) { m_type = type; m_bit = 0; m_controlledValues.clear(); }

  /** \details
  Add control
  */
  void addControl(QWidget* widget) { m_ctrlList.append(widget); }

  /** \details
  Set bit for check
  */
  void setBit(OdUInt32 bit){ if (m_type != kBit) return; m_bit = bit; }

  /** \details
  Append value for check
  */
  void addValue(OdUInt32 val){ if (m_type != kValues) return; m_controlledValues.append(val); }

  /** \details
  Perform enable/disable of the controls based on the value
  */
  void updateState(OdUInt32 val);

protected:
  QList<QWidget*> m_ctrlList;
  ConditionType   m_type;
  QList<OdUInt32> m_controlledValues;
  OdUInt32        m_bit;
};

/** \details
This is an implemetation of the preview widget for the visual style editor
*/
class OdTvViewerVisualStylesPreview : public QWidget
{
  Q_OBJECT

public:
  OdTvViewerVisualStylesPreview(OdTvUIBaseView* pAppActiveView, QWidget* parent = 0);
  virtual ~OdTvViewerVisualStylesPreview();

  /** \details
  Set visual style
  */
  void setVisualStyle(const OdTvVisualStyleId& id);

  /** \details
  Get visual style
  */
  OdTvVisualStyleId getVisualStyleId() const { return m_TvVisualStyleId; }

  /** \details
  Update preview
  */
  void updatePreview(bool bDueToChanges = false);

  /** \details
  Get preview database
  */
  OdTvDatabaseId getTvDatabaseId() const { return m_TvDatabaseId; }

  /** \details
  Get list of changed visual styles names
  */
  QStringList getChangedVisualStylesList() const { return m_visualStylesList; }

  /** \details
  Get flag that show enabled scene graph or not
  */
  bool isSceneGraphEnabled() const { return m_bSceneGraphEnadled; }

protected:

  virtual QPaintEngine* paintEngine() const;
  virtual void paintEvent(QPaintEvent* e);
  bool eventFilter(QObject *obj, QEvent *ev);
  void initialize();

private:

  //local visualize database
  OdTvDatabaseId m_TvDatabaseId;

  //local visualize moel
  OdTvModelId m_TvModelId;

  //local visualize device 
  OdTvGsDeviceId m_TvDeviceId;

  //local visualize view
  OdTvGsViewId m_TvViewId;

  // current visual style
  OdTvVisualStyleId m_TvVisualStyleId;

  // current active application view
  OdTvUIBaseView * m_pActiveView;

  // list of changed visual styles names
  QStringList m_visualStylesList;

  // background color
  ODCOLORREF m_iBgColor;

  //means that the object was already initialized (first paint already was)
  bool m_bWasInitialized;

  // flag that show enable scene graph or nor
  bool m_bSceneGraphEnadled;
};


/** \details
This is an implemetation of the dialog for working with Visual Style properties
*/
class OdTvViewerVisualStylesDlg : public QDialog
{
  Q_OBJECT

public:
  OdTvViewerVisualStylesDlg(OdTvDatabaseId appBbId, OdTvUIBaseView* pAppActiveView);
  virtual ~OdTvViewerVisualStylesDlg();

public slots:
  void okBtnClicked();
  void addVisualStyle();
  void removeVisualStyle();
  void applyVisualStyleToApp(QListWidgetItem * item);
  void changeVisualStyle(QListWidgetItem *current, QListWidgetItem *previous);
protected:
  void updateAppView();
  bool applyChangesToApp();     //return true if it is need to update the app view

  void createControls(OdTvVisualStyleId visualStyleId);
  void updateControls(OdTvVisualStyleId visualStyleId);

  void updateComboboxes(OdTvVisualStyleId visualStyleId);
  void updateIntLineEdits(OdTvVisualStyleId visualStyleId);
  void updateDoubleLineEdits(OdTvVisualStyleId visualStyleId);
  void updateCheckBoxes(OdTvVisualStyleId visualStyleId);
  void updateColors(OdTvVisualStyleId visualStyleId);

  void updateFaceModifiers(OdTvVisualStyleId visualStyleId);
  void updateEdgeStyles(OdTvVisualStyleId visualStyleId);
  void updateEdgeModifiers(OdTvVisualStyleId visualStyleId);
  void updateDisplayStyles(OdTvVisualStyleId visualStyleId);

  void addFaceModifiers(const OdTvVisualStyleId& visualStyleId, QGridLayout *lt, int row);  
  void addEdgeStyles(const OdTvVisualStyleId& visualStyleId, QGridLayout *lt, int row);
  void addEdgeModifiers(const OdTvVisualStyleId& visualStyleId, QGridLayout *lt, int row);
  void addDisplayStyles(const OdTvVisualStyleId& visualStyleId, QGridLayout *lt, int row);

  void generateComboBoxParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, const QStringList& list, QGridLayout *lt, int row, int startInd = 0);
  void generateLineEditDoubleParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, QGridLayout *lt, int row);
  void generateColorComboBoxParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, QGridLayout *lt, int row);
  void generateLineEditIntParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, QGridLayout *lt, int row);
  void generateCheckBoxParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, QGridLayout *lt, int row);

  // methods for create different types of objects
  QObject* createCheckBox(bool flag, bool readOnly = false);
  QObject* createCombobox(QStringList items, int currentIndex, bool readOnly = false);
  QObject* createCombobox(QStringList items, const QString& curText, bool readOnly = false); 
  QObject* createLineEdit(const QString& text, bool readOnly = false, QWidget* parent = NULL);
  QObject* createColorComboBox(const OdTvColorDef& color);

  // methods for work with color
  QColor getQColorFromDef(const OdTvColorDef& color) const;
  // method for add colors from list to combobox
  void fillColorNamesList(QComboBox *cb);
  // method for setting current color in combobox and method for return old color to combobox(if selected color was canceled)
  void getColorFromDef(QComboBox *cb, const OdTvColorDef& color);
  void getOldColor(QComboBox *cb, const OdTvColorDef& oldColor);
  // add or set color for combobox
  void setColorForComboBox(QComboBox *cb, const QColor& color);

  void setComboBoxDropDownListWidth(QComboBox * cb, int pixmapWidth = 0);

  void updateAllGuidedControls();
  void updateGuidedControls(OdTvVisualStyleOptions::Options opt);

 private slots:
  // Visual style properties slots
  void applyComboBox(const QString& text);
  void applyDouble();
  void applyInt();
  void applyCheckBox(int state);
  void applyFaceModifiers(int state);
  void applyEdgeStyles(int state);
  void applyEdgeModifiers(int state);
  void applyDisplayStyles(int state);
  void applyColorComboBox(const QString& text);

private:
  
  // App database id
  OdTvDatabaseId m_dbId;

  // current active application view
  OdTvUIBaseView * m_pActiveView;

  // List with visual styles
  QListWidget* m_pVisualStylesList;

  //string list with the colors for color control
  QStringList m_colorNamesList;

  // Main layout
  QPushButton* m_pRemoveBtn;

  // Main layout
  QGridLayout* m_pMainLayout;

  // Scroll area for visual style parameters
  QScrollArea* m_pScrollArea;

  // Layout for visual style parameters
  QVBoxLayout* m_pRightColumnVBoxLayout;

  // Current visual style
  OdTvVisualStyleId m_visualStyleId;

  // Current visual style
  OdTvVisualStyleId m_curAppVisualStyleId;

  // Preview
  OdTvViewerVisualStylesPreview *m_pPreview;

  // for update widgets
  QList<QComboBox *> m_comboboxList;
  QList<QComboBox *> m_colorComboboxList;
  QList<QCheckBox *> m_checkboxList;
  QList<QLineEdit *> m_iLineEditList;
  QList<QLineEdit *> m_dLineEditList;
  QList<QCheckBox *> m_faceModifers;
  QList<QCheckBox *> m_edgeStyles;
  QList<QCheckBox *> m_edgeModifers;
  QList<QCheckBox *> m_displayStyles;

  // controls for dynamic on\off
  QList<OdTvViewerVisualStylesGuidedCtrls> m_faceModifiersGuidedCtrls;
  QList<OdTvViewerVisualStylesGuidedCtrls> m_faceColorModeGuidedCtrls;
  QList<OdTvViewerVisualStylesGuidedCtrls> m_edgeStylesGuidedCtrls;
  QList<OdTvViewerVisualStylesGuidedCtrls> m_edgeModifiersGuidedCtrls;
  QList<OdTvViewerVisualStylesGuidedCtrls> m_edgeModelsGuidedCtrls;

  //special flag
  bool m_bChanged;

  // Icon for the eidt button
  QIcon m_editBtnIcn;
};

#endif //OD_TV_VIEWERVISUALSTLESDLG_H
