/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef OD_TV_VIEWERCONSTRUCTDRAGGERS_H
#define OD_TV_VIEWERCONSTRUCTDRAGGERS_H

//ODA Visualize Viewer
#include "OdTvUIDraggers.h"

/** \details
    This class implements the dragger for construct the line
*/
class OdTvViewerBaseConstructDragger : public OdTvUIBaseDragger
{
protected:

  enum ClickState
  {
    kWaitingInfiniteClick = -1,
    kWaitingFirstClick    = 0,
    kWaitingSecondClick   = 1,
    kWaitingThirdClick    = 2,
    kWaitingFourthClick   = 3,
    kWaitingFifthClick    = 4
  };

public :
  OdTvViewerBaseConstructDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerBaseConstructDragger(){}

  // reimplementation of OdTvViewerDragger
  eDraggerResult start(OdTvDraggerPtr pPrevDragger, QCursor activeCusor, OdTvExtendedView* pExtendedView);
  eDraggerResult nextpoint(int x, int y);
  eDraggerResult drag(int x, int y);
  OdTvDraggerPtr finish(eDraggerResult& rc);
  bool canFinish();

  //own virtual method
  virtual void transferResultToActiveModel();

protected:

  // this method should be called to update or create the new geometry
  virtual void updateGeometry(bool bCreate, bool bFromNextPoint);
  bool updateCursor();

  /** \details
    Return the distance over or under the UCS (currently WCS) plane
  */
  double getDistance(OdGePoint3d& pt1, OdGePoint3d& pt2) const;

protected:

  //model for adding created objects
  OdTvModelId m_tvActiveModelId;
  
  // clicked points
  OdGePoint3dArray m_clickedPts;

  // local state of the dragger
  ClickState m_ClickState;

  // last state of the dragger (after it the dragger should be finished or restarted)
  ClickState m_LastState;

  // true if and only if dragger should be restarted after end of work
  bool m_bDraggerShouldBeRestarted;

  // need to control the ::start called first time or not
  bool m_bFirstStart;

  // new geometry
  OdTvEntityId m_entityId;
  OdTvGeometryDataId m_newGeometryId;

  // need for add entity id to the list for regeneration
  OdTvUIBaseView* m_pView;

  //indicate that the result should be transfered to the active model
  bool m_bNeedTransferToActive;
};


/** \details
    This class implements the dragger for construct the line
*/
class OdTvViewerLineDragger : public OdTvViewerBaseConstructDragger
{
public :
  OdTvViewerLineDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerLineDragger(){}

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
};


/** \details
    This class implements the dragger for construct the circle via center and radius
*/
class OdTvViewerCircleRadiusDragger : public OdTvViewerBaseConstructDragger
{
public :
  OdTvViewerCircleRadiusDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerCircleRadiusDragger(){}

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
};


/** \details
    This class implements the dragger for construct the circle via 3 points
*/
class OdTvViewerCircle3PointsDragger : public OdTvViewerBaseConstructDragger
{
public :
  OdTvViewerCircle3PointsDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerCircle3PointsDragger(){}

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
};

/** \details
      This class implements the dragger for construct circle arc via 3 points
*/
class OdTvViewerCircleArcDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerCircleArcDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerCircleArcDragger(){}

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
};

/** \details
This class implements the dragger for construct the circle wedge via 3 points
*/
class OdTvViewerCircleWedgeDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerCircleWedgeDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerCircleWedgeDragger(){}

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
};

/** \details
This class implements the dragger for construct the ellipse via 3 points
*/
class OdTvViewerEllipseDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerEllipseDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModeId, OdTvModelId& tvActiveModeId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerEllipseDragger() {}

  virtual OdTvDraggerPtr finish(eDraggerResult& rc);

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);

private:
  // temporary geometry ( line from center to the cursor )
  OdTvEntityId m_tempEntityId;
  OdTvGeometryDataId m_tempGeometryId;
};

/** \details
    This class implements the dragger for construct the box
*/
class OdTvViewerBoxDragger : public OdTvViewerBaseConstructDragger
{
public :
  OdTvViewerBoxDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerBoxDragger(){}

  virtual OdTvDraggerPtr finish(eDraggerResult& rc);

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);

private:
  // temporary geometry ( line from center to the cursor )
  OdTvEntityId m_tempEntityId;
  OdTvGeometryDataId m_tempGeometryId;
};


/** \details
    This class implements the dragger for construct the sphere
*/
class OdTvViewerSphereDragger : public OdTvViewerBaseConstructDragger
{
public :
  OdTvViewerSphereDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerSphereDragger(){}

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
};

/** \details
  This class implements the dragger for construct the cylinder
*/
class OdTvViewerCylinderDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerCylinderDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerCylinderDragger() {}

  virtual OdTvDraggerPtr finish(eDraggerResult& rc);

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);

private:
  // temporary geometry ( line from center to the cursor )
  OdTvEntityId m_tempEntityId;
  OdTvGeometryDataId m_tempGeometryId;
};

/** \details
This class implements the dragger for ray
*/
class OdTvViewerRayDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerRayDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerRayDragger() {}

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
};

/** \details
This class implements the dragger for xline
*/
class OdTvViewerXlineDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerXlineDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerXlineDragger() {}

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
};

/** \details
This class implements the dragger for elliptic arc
*/
class OdTvViewerEllipticArcDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerEllipticArcDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerEllipticArcDragger() {}

  virtual OdTvDraggerPtr finish(eDraggerResult& rc);

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);

  // specific member functions used for defined the angle pos an the ellipse based on the current ray
  double calibrateAngle(double val, double input);
  double paramFromAngle(double angle, double radiusRatio);
  double angleToCCW(const OdTvVector& mineVect, const OdTvVector& vect) const;

private:  
  // temporary geometry ( line from center to the cursor )
  OdTvEntityId m_tempEntityId;
  OdTvGeometryDataId m_tempGeometryId;
};


/** \details
This class implements the dragger for construct the nurbs
*/
class OdTvViewerNurbsDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerNurbsDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerNurbsDragger() {}

  virtual eDraggerResult nextpoint(int x, int y);
  virtual OdTvDraggerPtr finish(eDraggerResult& rc);
  virtual eDraggerResult processEnter();

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);
  void updateKnotsAndDegree(unsigned int nPoints);

private:
  // temporary geometry ( from previous point to the next )
  OdTvEntityId m_tempEntityId;
  OdTvGeometryDataId m_tempGeometryId;

  OdDoubleArray m_knots;
  unsigned int m_degree;
};


/** \details
This class implements the dragger for construct the raster image
*/
class OdTvViewerRasterImageDragger : public OdTvViewerBaseConstructDragger
{
public:
  OdTvViewerRasterImageDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView);
  virtual ~OdTvViewerRasterImageDragger() {}

  eDraggerResult nextpoint(int x, int y);
  virtual OdTvDraggerPtr finish(eDraggerResult& rc);

  bool loadImage(QString& imagePath);

private:
  void updateGeometry(bool bCreate, bool bFromNextPoint);

private:
  // imagedefenition object
  OdTvRasterImageId m_imageDef;
  // temporary geometry ( line from center to the cursor )
  OdTvEntityId m_tempEntityId;
  OdTvGeometryDataId m_tempGeometryId;

};


#endif //OD_TV_VIEWERCONSTRUCTDRAGGERS_H
