/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#include "OdaCommon.h"
#include "RxInit.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QDesktopWidget>

#include "TvViewerMainWindow.h"

#if defined(_MSC_VER)
  inline void attuneLocale()
  {
    setlocale(LC_NUMERIC, "C"); // saving doubles to dxf
    setlocale(LC_TIME, ""); // correct stvtime formatting
  }
#else
 #define attuneLocale()
#endif

void ODASdkActivate()
{
  static const char* ActInfo[] = {
#ifdef TEIGHA_TRIAL
    "", ""
#else
    //"UserInfo", "UserSignature" 

    // Before compiling, a ODA SDK activation file should be placed in a location that a compiler can access, 
    // otherwise you get a compiler error such as "Kernel/Extensions/ExServices/ExSystemServices.h:43:10: fatal error: 'OdActivationInfo' file not found". 
    // To learn about ODA SDK activation, see the activation guide at https://docs.opendesign.com/tkernel/oda_activation.html    
#include "OdActivationInfo"
#endif
  };

  odActivate(ActInfo[0], ActInfo[1]);
}

void ODASdkDeactivate()
{
  odCleanUpStaticData();
}

int main(int argc, char *argv[])
{
  attuneLocale();

  int xDpi = 0, yDpi = 0;
  {
    QApplication app0(argc, argv);
    xDpi = app0.desktop()->physicalDpiX();
    yDpi = app0.desktop()->physicalDpiY();
  }

  if (xDpi > 110 && yDpi > 110)
  {
#if QT_VERSION >= QT_VERSION_CHECK(5,6,0)
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#else
    qputenv("QT_DEVICE_PIXEL_RATIO", QByteArray("1"));
#endif // QT_VERSION
  }

  //activate ODA SDK
  ODASdkActivate();

  // initialize the Visualize SDK
  odTvInitialize();
  int res = 0;
  {
    // create the Qt aplication
    QApplication app(argc, argv);

    // load files from the command string  //TODO TV_VIEWER
    QCoreApplication::setApplicationVersion(QT_VERSION_STR);
    // fill data for writing in register
    QCoreApplication::setOrganizationName(ORGANIZATION_NAME);
    QCoreApplication::setOrganizationDomain(ORGANIZATION_DOMAIN);
    QCoreApplication::setApplicationName(APPLICATION_NAME);

    QCommandLineParser parser;
    parser.setApplicationDescription("ODA Visualize Viewer");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("file", "The file to open.");
    parser.process(app);

    // create app main window
    OdTvViewerMainWindow mainWin;
    foreach(const QString &fileName, parser.positionalArguments())
      mainWin.openFile(fileName, true, false, false, 0);

    //show window
    mainWin.show();

    // run application event
#if !(defined(ANDROID) || defined(__IPHONE_OS_VERSION_MIN_REQUIRED))
    res = app.exec();
#else
#if defined(ANDROID)
    QStyle* android = QStyleFactory::create("Android");
    app.setStyle(android);
#endif
    Q_ASSERT(!mainWin.isClosed());
    do {
      res = app.exec();
      if (!mainWin.isClosed())
      {
        if (!mainWin.isVisible())
          mainWin.show();
        if (!mainWin.isBrowserClosed())
          QTimer::singleShot(100, &mainWin, SLOT(clickedBrowserCloseButton()));
      }

      //
      // http://sourceforge.net/p/necessitas/tickets/76/
      // http://comments.gmane.org/gmane.comp.lib.qt.android/1874
      // Back button handling is not work ! When i press back my app close.
    } while (!mainWin.isClosed()); // should be closed via closeEvent
#endif
  }

  // Uninitialize the ODA Visualize SDK
  odTvUninitialize();

  ODASdkDeactivate();

  return res;
}

