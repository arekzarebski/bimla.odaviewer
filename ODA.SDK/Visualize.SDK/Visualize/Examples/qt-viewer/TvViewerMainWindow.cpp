/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////
#include "OdaCommon.h"
#include "DynamicLinker.h"
#include "RxVariantValue.h"

#ifdef _MSC_VER
#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif
#endif

#include <QtWidgets>
#include <QUrl>

#include "TvViewerMainWindow.h"
#include "TvModuleNames.h"

#include "TvFactory.h"
#include "TvError.h"
#include "OdTvUINavigationHandler.h"
#include "OdTvUIDraggers.h"
#include "OdTvUIBaseView.h"
#include "OdTvUIProgressMeter.h"
#include "OdTvUICuttingPlaneParamsDlg.h"

#ifdef ODA_WINDOWS
#include <psapi.h>
#endif

OdTvViewerLimitManager::OdTvViewerLimitManager()
{
#ifdef ODA_WINDOWS
  m_processId = GetCurrentProcess();
#endif
  m_nMemoryLimit = 0;
}

OdTvViewerLimitManager::~OdTvViewerLimitManager()
{
}

OdTvLimitManager::MemoryLimitControlResult OdTvViewerLimitManager::checkMemoryUsage( OdUInt8 nReason, OdUInt64 nApproximateMemoryUsage ) const
{
  if( m_nMemoryLimit == 0 ) return OdTvLimitManager::kPassed;
#ifdef ODA_WINDOWS
  PROCESS_MEMORY_COUNTERS pmc;
  m_mutex.lock();
  if( GetProcessMemoryInfo( m_processId, &pmc, sizeof( pmc ) ) )
  {
    OdUInt64 workingSetBytes = pmc.WorkingSetSize;
    m_mutex.unlock();
    if( workingSetBytes + nApproximateMemoryUsage < m_nMemoryLimit ) 
    {
      return OdTvLimitManager::kPassed;
    }
    return OdTvLimitManager::kNotPassed;
  }
  else
  {
    ODA_FAIL();
  }
  m_mutex.unlock();
  return OdTvLimitManager::kPassed;
  /* Allocator-based
  m_mutex.lock();
  OdUInt64 memUsage = GetMemoryUsageInternalInfo( 0 );
  m_mutex.unlock();
  if( memUsage + nApproximateMemoryUsage < m_nMemoryLimit ) 
  {
    return OdTvLimitManager::kPassed;
  }
  return OdTvLimitManager::kNotPassed;
  */
#else
  return OdTvLimitManager::kPassed;
#endif //ODA_WINDOWS
}

QString getAppName()
{
  static QString s_qsAppName;
  if (s_qsAppName.isEmpty())
  {
    s_qsAppName = QFileInfo(QCoreApplication::applicationFilePath()).baseName();
    if (   !s_qsAppName.indexOf(APPLICATION_NAME, Qt::CaseInsensitive) )
    {
      s_qsAppName = s_qsAppName.mid(!s_qsAppName.indexOf(APPLICATION_NAME) ? 19 : 2).trimmed();
      if (!s_qsAppName.isEmpty())
        s_qsAppName = " " + s_qsAppName;
      s_qsAppName = QObject::tr(APPLICATION_NAME) + s_qsAppName;
      qApp->setApplicationName(s_qsAppName);
    }
  }
  return s_qsAppName;
}

std::wstring GetExtensionFromFile(const QString& fileName)
{
  //std::wstring strFName(fileName.toStdWString());
  QString2WString q2w( fileName );
  size_t pos = q2w.wstr().find_last_of(L".");
  if (pos == std::wstring::npos)
    return std::wstring();

  std::wstring strExt = q2w.wstr().substr(pos + 1, q2w.wstr().length() - pos);
  transform(strExt.begin(), strExt.end(), strExt.begin(), toupper);
  return strExt;
}

QString importFormats[] = {
  "Drawings files(*.dwg *.dxf)"
  ,"DGN files(*.dgn)"
  ,"STL(*.stl)"
  ,"OBJ(*.obj)"
  ,"PRC(*.prc)"
  ,"U3D(*.u3d)"
  ,"BIM(*.rfa *.rvt)"
  ,"Point Cloud(*.rcs)"
  ,"Point Cloud Project(*.rcp)"
#ifdef ACIS2VISUALIZE_ENABLED
  ,"ACIS(*.sat *.sab)"
#endif
#ifdef NW2VISUALIZE_ENABLED
  ,"NwInterop (*.nwd *.nwc *.nwf)"
#endif
#ifdef IFC2VISUALIZE_ENABLED
  ,"IFC(*.ifc)"
#endif
#ifdef REPLAY2VISUALIZE_ENABLED
  ,"ODA SDK Replay(*.replay)"
#endif
};

QString getImportFormatsString(bool bForPartial)
{
  static int numImportFormats = sizeof(importFormats) / sizeof(importFormats[0]);

  QString strAllSupported = "*.vsf";
  QString strRes = "Open Design Visualize Stream (*.vsf)";
  if (numImportFormats > 0 && !bForPartial)
  {
    for (int i = 0; i < numImportFormats; ++i)
    {
      int iFirst = importFormats[i].lastIndexOf("(") + 1;
      int iLast = importFormats[i].lastIndexOf(")");
      if (iFirst < iLast)
      {
        strAllSupported += " " + importFormats[i].mid(iFirst, iLast - iFirst);
        strRes += ";; " + importFormats[i];
      }
    }
    strRes += ";; All Supported files(" + strAllSupported + ")";
  }
  return strRes;
}

QString getAllSupportedFormatsString()
{
  static int numImportFormats = sizeof(importFormats) / sizeof(importFormats[0]);

  QString strAllSupported = "*.vsf";
  if (numImportFormats > 0)
  {
    for (int i = 0; i < numImportFormats; ++i)
    {
      int iFirst = importFormats[i].lastIndexOf("(") + 1;
      int iLast = importFormats[i].lastIndexOf(")");
      if (iFirst < iLast)
        strAllSupported += " " + importFormats[i].mid(iFirst, iLast - iFirst);
    }
  }
  return strAllSupported;
}

void customParamsToFilerProperties(const OdTvBaseImportParams* pParams, OdRxDictionaryPtr pProperties, const OdString& strExt)
{
#ifdef REPLAY2VISUALIZE_ENABLED
  if (strExt == OD_T("REPLAY"))
  {
    const OdTvCustomBaseImportParams *pBaseCustomParams = dynamic_cast<const OdTvCustomBaseImportParams *>(pParams);
    if (pBaseCustomParams)
    {
      ODCOLORREF color = pBaseCustomParams->getDefaultColor();
      if (pProperties.get())
      {
        if (pProperties->has(OD_T("DefaultColor")))
          pProperties->putAt(OD_T("DefaultColor"), OdRxVariantValue((OdIntPtr)&color));
      }
    }
  }
#endif
#ifdef ACIS2VISUALIZE_ENABLED
  if (strExt == OD_T("SAT") || strExt == OD_T("SAB"))
  {
    const OdTvUIAcisImportParams *pAcisParams = dynamic_cast<const OdTvUIAcisImportParams *>(pParams);
    if (pAcisParams)
    {
      ODCOLORREF color = pAcisParams->getDefaultColor();
      OdTvDCRect dcRect = pAcisParams->getDCRect();
      if (pProperties.get())
      {
        if (pProperties->has(OD_T("FacetRes")))
          pProperties->putAt(OD_T("FacetRes"), OdRxVariantValue(pAcisParams->getFacetRes()));

        if (pProperties->has(OD_T("ImportBrepAsBrep")))
          pProperties->putAt(OD_T("ImportBrepAsBrep"), OdRxVariantValue(pAcisParams->getImportBrepAsBrep()));

        if (pProperties->has(OD_T("DefaultColor")))
          pProperties->putAt(OD_T("DefaultColor"), OdRxVariantValue((OdIntPtr)&color));

        if (pProperties->has(OD_T("DCRect")))
          pProperties->putAt(OD_T("DCRect"), OdRxVariantValue((OdIntPtr)&dcRect));
      }
    }
  }
#endif
}

OdTvDatabaseId importCustomFormat(const OdTvBaseImportParams* pParams, OdTvResult* rc)
{
  OdTvDatabaseId newId;

  if (pParams == 0)
  {
    if (rc)
      *rc = tvInvalidInput;
    return newId;
  }

  //get the file path from params
  OdString fileName = pParams->getFilePath();

  //get file extension
  OdString strExt = fileName.mid(fileName.reverseFind('.') + 1);
  if (strExt.isEmpty())
  {
    if (rc)
      *rc = tvInvalidFilePath;
    return newId;
  }

  strExt.makeUpper();

  //load appropriate extension module
  OdTvVisualizeFilerModulePtr pVisualizeFilerModule;
#ifdef REPLAY2VISUALIZE_ENABLED
  if (strExt == OD_T("REPLAY"))
  {
    pVisualizeFilerModule = odrxDynamicLinker()->loadApp(OdTvReplay2VisualizeModuleName);
  }
  else
#endif
#ifdef ACIS2VISUALIZE_ENABLED
  if (strExt == OD_T("SAT") || strExt == OD_T("SAB"))
  {
    pVisualizeFilerModule = odrxDynamicLinker()->loadApp(OdTvAcis2VisualizeModuleName);
  }
  else
#endif
  {
    if (rc)
      *rc = tvInvalidFileType;
    return newId;
  }

  if (pVisualizeFilerModule.isNull())
  {
    if (rc)
      *rc = tvMissingFilerModule;
    return newId;
  }

  OdString strModuleName = pVisualizeFilerModule->moduleName();

  // Load database
  {
    OdTvVisualizeFilerPtr pFiler = pVisualizeFilerModule->getVisualizeFiler();
    if (pFiler.isNull())
    {
      if (rc)
        *rc = tvMissingFiler;
    }
    else
    {
      // get properties
      OdRxDictionaryPtr pProperties = pFiler->properties();

      //transfer import properties
      customParamsToFilerProperties(pParams, pProperties, strExt);

      // load database from file  
      newId = pFiler->loadFrom(fileName, pParams->getProfiling(), rc);
    }
  }

  pVisualizeFilerModule.release();

  //unload extension module
  if (!strModuleName.isEmpty())
    odrxDynamicLinker()->unloadModule(strModuleName);

  return newId;
}

//**************************************************************************************
// OdTvViewerMainWindow methods implementation                                               //
//**************************************************************************************

OdTvViewerMainWindow::OdTvViewerMainWindow()
  : m_bClosed(false), m_pDockPanel(NULL), m_bBrowserIsEnabled(true)
  , m_bPropertiesIsEnabled(true), m_pFileTabs(NULL), m_pActiveView(NULL)
{
  // hotkeys
  QShortcut *pCtrlO = new QShortcut(this);
  pCtrlO->setKey(Qt::CTRL + Qt::Key_O);
  connect(pCtrlO, SIGNAL(activated()), this, SLOT(open()));

  QShortcut *pCtrlN = new QShortcut(this);
  pCtrlN->setKey(Qt::CTRL + Qt::Key_N);
  connect(pCtrlN, SIGNAL(activated()), this, SLOT(newFile()));

  QShortcut *pCtrlS = new QShortcut(this);
  pCtrlS->setKey(Qt::CTRL + Qt::Key_S);
  connect(pCtrlS, SIGNAL(activated()), this, SLOT(save()));

  QShortcut *pCtrlShftS = new QShortcut(this);
  pCtrlShftS->setKey(Qt::CTRL + Qt::SHIFT + Qt::Key_S);
  connect(pCtrlShftS, SIGNAL(activated()), this, SLOT(saveAs()));

  QShortcut *pF5 = new QShortcut(this);
  pF5->setKey(Qt::Key_F5);
  connect(pF5, SIGNAL(activated()), this, SLOT(regenAll()));

  //load global device and appearance properties from register
  m_performanceOptions.loadFromRegister(QString());
  m_appearanceOptions.loadFromRegister();
  m_gridOptions.loadFromRegister();
  m_generalOptions.loadFromRegister(QString());

  // initialize Visualize SDK
  m_Tvfactory = odTvGetFactory();

  setWindowTitle(getAppName());
#ifndef __IPHONE_OS_VERSION_MIN_REQUIRED
  QPixmap pixmap(QLatin1String(":odalogo.png"));
  ODA_ASSERT_ONCE(!pixmap.isNull());
  pixmap.setMask(pixmap.createHeuristicMask());
  setWindowIcon(QIcon(pixmap));
#endif

  //update some import parameters
  m_importParams.dwgImportParam.setObjectNaming(true);
  m_importParams.dwgImportParam.setMultithreading(true);
  m_importParams.dgnImportParam.setObjectNaming(true);
  m_importParams.dgnImportParam.setUseIsolinesFor3DObjects(true);
  m_importParams.prcImportParam.setObjectNaming(true);
  m_importParams.bimImportParam.setObjectNaming(true);

  //update some append parameters
  m_appendParams.dwgAppendParam.setObjectNaming(true);
  m_appendParams.dwgAppendParam.setMultithreading(true);
  m_appendParams.dgnAppendParam.setObjectNaming(true);
  m_appendParams.dgnAppendParam.setUseIsolinesFor3DObjects(true);
  m_appendParams.prcAppendParam.setObjectNaming(true);
  m_appendParams.bimAppendParam.setObjectNaming(true);

  m_pCentralWidget = new QWidget(this);
  QVBoxLayout *mainLayout = new QVBoxLayout;
  m_pCentralWidget->setLayout(mainLayout);
  setCentralWidget(m_pCentralWidget);

  m_pMdiArea = new QMdiArea(m_pCentralWidget);
  m_pMdiArea->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  m_pMdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  m_pMdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
  m_pMdiArea->setStyleSheet("background-color:rgb(215, 215, 215);");

  m_pFileTabs = new OdTvUIFileTabBar(m_pMdiArea);
  connect(m_pFileTabs, SIGNAL(fileTabChangedSg(OdTvUIBaseView*)), this, SLOT(fileTabChangedSl(OdTvUIBaseView*)));

  QHBoxLayout *tabLt = new QHBoxLayout(m_pMdiArea);
  tabLt->setMargin(0);
  tabLt->setSpacing(0);
  m_pCentralStackedWid = new QStackedWidget;
  m_pCentralStackedWid->addWidget(m_pFileTabs);
  tabLt->addWidget(m_pCentralStackedWid);
  m_pMdiArea->setLayout(tabLt);

  m_pMainSplitter = new QSplitter(m_pCentralWidget);
  m_pMainSplitter->setOpaqueResize(false);

  // create dock panels with palettes
  m_pDockPanel = new OdTvUIPalettePanel(m_pCentralWidget);
  m_pDockPanel->getExplorerItem()->getContainer()->setVisible(m_bBrowserIsEnabled);
  m_pDockPanel->getPropertiesItem()->getContainer()->setVisible(m_bPropertiesIsEnabled);
  m_pDockPanel->getExplorerItem()->getContainer()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
  m_pDockPanel->getPropertiesItem()->getContainer()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  getObjectExplorer()->setPerSession(m_generalOptions.getIsolateHidePerSession());

  m_pMainSplitter->addWidget(m_pDockPanel->getExplorerItem()->getContainer());
  m_pMainSplitter->addWidget(m_pMdiArea);
  m_pMainSplitter->addWidget(m_pDockPanel->getPropertiesItem()->getContainer());

  m_pMainSplitter->setStretchFactor(0, 0);
  m_pMainSplitter->setStretchFactor(1, 1);
  m_pMainSplitter->setStretchFactor(2, 0);

  m_pMainSplitter->setCollapsible(0, true);
  m_pMainSplitter->setCollapsible(1, false);
  m_pMainSplitter->setCollapsible(2, true);

  // add widgets to main layout
  m_pMenuToolbar = new OdTvUIMenuToolbar(m_pCentralWidget);
  m_pItemPanel = new OdTvUIItemPanel(m_pCentralWidget);
  // add visual styles dialog to styles panel
  if (m_generalOptions.getGsDeviceName() == OdTvGsDevice::kOpenGLES2 && m_generalOptions.getUseVisualStyles())
    m_pItemPanel->addVisualStylesToStylePanel();

  mainLayout->addWidget(m_pMenuToolbar->getContainer());
  mainLayout->addWidget(m_pItemPanel->getContainer());
  mainLayout->addSpacing(2);
  mainLayout->addWidget(m_pMainSplitter);
  mainLayout->setSpacing(0);
  mainLayout->setMargin(0);

  OdTvUIStyleManager styleManager;
  styleManager.init(":/style.txt");
  styleManager.setEfects(m_pMenuToolbar->getContainer());
  styleManager.setEfects(m_pItemPanel->getContainer());
  styleManager.setEfects(m_pDockPanel->getPropertiesItem()->getContainer());
  styleManager.setEfects(m_pDockPanel->getExplorerItem()->getContainer());

  styleManager.setStyle(this);

  //add style for the progress bar
  QString strsSyleSheet = styleSheet();
  setStyleSheet(strsSyleSheet + "QProgressDialog>QLabel{font-family: Open Sans; font-size: 12px; color: rgb(63, 69, 115);} QProgressBar {color: rgb(0,0,0); border: 1px solid rgb(153,153,153); text-align: center;} QProgressBar::chunk {background: qlineargradient(x1 : 0, y1 : 0.5, x2 : 0, y2 :0.51,  stop : 1 rgb(111,119,176), stop : 0 rgb(148,154,197));}");

  m_pSliderMenu = new OdTvUISliderMenu(this);
  m_pSliderMenu->setSetting(m_pMenuToolbar->getContainer()->height(), m_pSliderMenu->getWidth(), 200);
  m_pSliderMenu->hide();

  m_pRecentMenu = new OdTvViewerOpenRecentMenu(this);
  m_pRecentMenu->setupFiles(m_pSliderMenu->getRecentList());
  m_pSliderMenu->addChild(m_pRecentMenu);

  m_pExportMenu = new OdTvViewerExportMenu(this);
  m_pSliderMenu->addChild(m_pExportMenu);

  m_pExampleMenu = new OdTvViewerExamplesMenu(this);
  m_pSliderMenu->addChild(m_pExampleMenu);
  m_pSliderMenu->hide();

  m_pPlotsMenu = new OdTvViewerPlotsMenu(this);
  m_pExampleMenu->addChild(m_pPlotsMenu);

  m_pMaterialsMenu = new OdTvViewerMaterialsMenu(this);
  m_pExampleMenu->addChild(m_pMaterialsMenu);

#ifdef _DEBUG
  m_pDebugExamplesMenu = new OdTvViewerDebugExamplesMenu(this);
  m_pExampleMenu->addChild(m_pDebugExamplesMenu);

  m_pDbgShellAttributesMenu = new OdTvViewerDbgShellAttributes(this);
  m_pDebugExamplesMenu->addChild(m_pDbgShellAttributesMenu);

  m_pDbgMeshAttributesMenu = new OdTvViewerDbgMeshAttributes(this);
  m_pDebugExamplesMenu->addChild(m_pDbgMeshAttributesMenu);

  m_pDbgResetTraitsMenu = new OdTvViewerDbgResetTraitsMenu(this);
  m_pDebugExamplesMenu->addChild(m_pDbgResetTraitsMenu);
#endif // _DEBUG

  connectSignals();

  // set start panels width
  m_pDockPanel->adjustPanelWidth(width());
  QList<int> list;
  list.append(width() * 0.1);
  list.append(width() * 0.1);
  list.append(width() * 0.75);
  list.append(width() * 0.15);
  m_pMainSplitter->setSizes(list);

  m_pBgImageLbl = new QLabel();
  m_pBgImageLbl->setAlignment(Qt::AlignCenter);
  m_bgImg = QPixmap(":/images/background.png");
  m_pCentralStackedWid->addWidget(m_pBgImageLbl);

  // read settings about the last window state
  restoreWidgetPlacement(this, "", 1000, 600);
  showMaximized();

  //set the special flag
  setUnifiedTitleAndToolBarOnMac(true);

  // allow drop files
  setAcceptDrops(true);

  updateButtonsState();
}

// FILE METHODS

bool OdTvViewerMainWindow::openFile(const QString &fileName, bool bIsMT, bool bIsPartial, bool bIsMemLimit, OdUInt32 nMemLimit)
{
  OdTvFileOptions opts( fileName, bIsMT, bIsPartial );
  opts.bUseMemoryLimit = bIsMemLimit && bIsPartial;
  opts.nMemoryLimit = nMemLimit;
  return openFile( opts );
}

bool OdTvViewerMainWindow::openFile(OdTvFileOptions& opt)
{
  QString fileName = opt.fileName;

  QFileInfo info(fileName);
  if (fileName.isEmpty() || (info.exists() && !info.isReadable()))
  {
    QMessageBox::warning(this, getAppName(),
      tr("File \"%1\" is not accessible to read")
      .arg(fileName));
    return false;
  }

  // transform to OdString
  //std::wstring strWName(fileName.toStdWString());
  QString2WString q2w( fileName );

  //0. get file extension 
  std::wstring strExt = GetExtensionFromFile(fileName);
  OdString odStrExt = OdString(strExt.c_str());
  //1. get appropriate import parameters
  //
  m_importParams.dwgImportParam.setFilePath( toOdString( fileName ) );
  if (odStrExt == OD_T("NWD") || odStrExt == OD_T("NWC") || odStrExt == OD_T("NWF"))
    m_importParams.nwImportParam.setFilePath(toOdString(fileName));

  bool bNeedLowMemoryImport = false;
  OdString vsfDumpFile;
  OdTvBaseImportParams* pImportParam = getImportParameters(strExt.c_str(), opt.bIsCallImportDlg, opt.bIsSetFeedback, bNeedLowMemoryImport, vsfDumpFile);
  if (pImportParam == 0)
    return false;

  // sync params
  m_generalOptions.loadFromRegister(toQString(strExt.c_str()));
  m_performanceOptions.loadFromRegister(toQString(strExt.c_str()));

  // override cursor
  QApplication::setOverrideCursor(Qt::WaitCursor);
  QCoreApplication::processEvents();

  // If the “CommonParams” parameter was turned on, after showing the dialog with the import parameters
  // , need to disable the call dialogs later on.
  if (opt.bIsCommonParams && opt.bIsCallImportDlg)
    opt.bIsCallImportDlg = false;

  //2. Prepare profiling structure
  OdTvUIDatabaseInfo databaseInfo;
  databaseInfo.setFilePath(q2w.wstr().c_str());
  if (strExt == L"VSF")
    databaseInfo.setType(OdTvUIDatabaseInfo::kFromFile);
  else
    databaseInfo.setType(OdTvUIDatabaseInfo::kImport);

  //3. fill base fields in param
  pImportParam->setFilePath(q2w.wstr().c_str());
  pImportParam->setProfiling(&databaseInfo);

  //4. open file and return appropriate model Id
  LoadMode loadMode = (strExt == L"VSF" ? kRead : kImport);
#ifdef REPLAY2VISUALIZE_ENABLED
  if (loadMode == kImport && strExt == L"REPLAY")
    loadMode = kCustom;
#endif
#ifdef ACIS2VISUALIZE_ENABLED
  if (loadMode == kImport && (strExt == L"SAT" || strExt == L"SAB"))
    loadMode = kCustom;
#endif
  //SEA enable limit manager if need
  if( loadMode == kRead || (loadMode == kImport && strExt == L"DWG") )
  {
     m_Tvfactory.setLimitManager( (opt.bUseMemoryLimit) ? &m_limitator : NULL );
  }
  m_limitator.setMemoryLimit( opt.nMemoryLimit * 1024 * 1024 );

  OdTvModelId tvModelId = loadDatabase(m_Tvfactory, pImportParam, loadMode, opt.bIsMT, opt.bIsPartial, bNeedLowMemoryImport, vsfDumpFile);
  if (tvModelId.openObject().isNull())
  {
    QApplication::restoreOverrideCursor();
    return false;
  }

  //4. create window for each device
  OdTvGsDeviceId deviceId;
  OdTvDatabaseId tvDbId = tvModelId.openObject()->getDatabase();
  OdTvDevicesIteratorPtr pDevicesIterator = tvDbId.openObject()->getDevicesIterator();
  OdTvDevicesIteratorPtr pDevIt = tvDbId.openObject()->getDevicesIterator();
  if (pDevicesIterator->done() || countViewsInDevices(pDevIt) == 0)
  {
    onOpeningError();
    return false;
  }

  QFileInfo fInfo = QFileInfo(toQString(pImportParam->getFilePath()));
  int fileInd = m_pFileTabs->addFileTab(fInfo.fileName());
  OdTvUIDeviceTabBar *devTabBar = m_pFileTabs->getDeviceTabBar(fileInd);
  connect(devTabBar, SIGNAL(deviceTabChangeSg(OdTvUIBaseView*)), this, SLOT(tabChangedSl(OdTvUIBaseView*)));

  int indOfActiveDevice = 0;
  unsigned int nDevices = 0;
  while (!pDevicesIterator->done())
  {
    deviceId = pDevicesIterator->getDevice();
    OdTvGsDevicePtr pDevice = deviceId.openObject();
    if (pDevice->getActive())
      indOfActiveDevice = nDevices;

    OdTvGsViewId activeViewId = pDevice->getActiveView();

    if (activeViewId.isNull())
      continue;

    //create MDI window
    m_pActiveView = createBaseView(databaseInfo, activeViewId.openObject(OdTv::kForRead)->modelAt(0), deviceId, true);
    devTabBar->blockSignals(true);
    devTabBar->addTab(m_pActiveView, toQString(pDevice->getName()));
    devTabBar->blockSignals(false);
    m_pActiveView->setWindowFlags(Qt::FramelessWindowHint);
    m_pActiveView->showMaximized();
    if (loadMode == kRead)
      m_pActiveView->setCurrentFile(fInfo.filePath());

    nDevices++;
    pDevicesIterator->step();
  }

    if (nDevices > 1)
      m_pMdiArea->tileSubWindows();

  // initialization for tabs
  m_pFileTabs->setCurrentIndex(fileInd);
  if (devTabBar->currentIndex() != indOfActiveDevice)
    devTabBar->setCurrentIndex(indOfActiveDevice);
  else
    devTabBar->tabSelected(indOfActiveDevice);

  m_pActiveView = devTabBar->getActiveView();

  updateButtonsState();
  updateBrowserTree();

  QApplication::restoreOverrideCursor();

  addFileToRecent(fileName);

  return true;
}

bool OdTvViewerMainWindow::appendFile(const QString &fileName)
{
  QFileInfo info(fileName);
  if (fileName.isEmpty() || (info.exists() && !info.isReadable()))
  {
    QMessageBox::warning(this, getAppName(),
      tr("File \"%1\" is not accessible to read")
      .arg(fileName));
    return false;
  }

  // transform to OdString
  //std::wstring strWName(fileName.toStdWString());
  QString2WString q2w( fileName );

  //0. get file extension 
  std::wstring strExt = GetExtensionFromFile(fileName);

  //1. get appropriate import parameters
  OdTvBaseImportParams* pImportParam = getAppendParameters(strExt.c_str());
  if (pImportParam == 0)
    return false;

  // override cursor
  QApplication::setOverrideCursor(Qt::WaitCursor);

  //2. Prepare profiling structure
  OdTvUIDatabaseInfo databaseInfo;
  databaseInfo.setFilePath(q2w.wstr().c_str());
  if (strExt == L"VSF")
    databaseInfo.setType(OdTvUIDatabaseInfo::kFromFile);
  else
    databaseInfo.setType(OdTvUIDatabaseInfo::kImport);

  //3. fill base fields in param
  pImportParam->setFilePath(q2w.wstr().c_str());
  pImportParam->setProfiling(&databaseInfo);

  bool bDatabaseExisits = (m_pActiveView && !m_pActiveView->getTvDatabaseId().isNull());

  OdTvDatabaseId databaseId;
  if (m_pActiveView)
  {
    databaseId = m_pActiveView->getTvDatabaseId();
  }
  else
  {
    OdTvFactoryId tvFactoryId = odTvGetFactory();
    databaseId = tvFactoryId.createDatabase();
  }

  OdTvDatabasePtr pTvDb = databaseId.openObject(OdTv::kForWrite);

  OdTvModelId lastModel;
  try
  {
    OdTvResult res;
    lastModel = pTvDb->append(pImportParam, &res);

    if (res != tvOk)
    {
      QApplication::restoreOverrideCursor();
      return false;
    }
  }
  catch (OdTvError& e)
  {
    OdString descr = e.description();
    QMessageBox::warning(this, getAppName(), QObject::tr("Appending of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(toQString(descr)));

    QApplication::restoreOverrideCursor();
    return false;
  }
  catch (const OdError& e)
  {
    OdString descr = e.description();
    QMessageBox::warning(this, getAppName(), QObject::tr("Appending of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(toQString(descr)));

    QApplication::restoreOverrideCursor();
    return false;
  }
  catch (...)
  {
    QMessageBox::warning(this, getAppName(), QObject::tr("Appending of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(QString("Unknown exception is caught...")));

    QApplication::restoreOverrideCursor();
    return false;
  }

  if (!bDatabaseExisits)
  {
    QFileInfo *fInfo = new QFileInfo(toQString(pImportParam->getFilePath()));
    int fileInd = m_pFileTabs->addFileTab(fInfo->fileName());
    OdTvUIDeviceTabBar *devTabBar = m_pFileTabs->getDeviceTabBar(fileInd);
    connect(devTabBar, SIGNAL(deviceTabChangeSg(OdTvUIBaseView*)), this, SLOT(tabChangedSl(OdTvUIBaseView*)));

    int indOfActiveDevice = 0;
    unsigned int nDevices = 0;
    OdTvDevicesIteratorPtr pDevicesIterator = pTvDb->getDevicesIterator();

    OdTvGsDeviceId deviceId;

    OdTvDevicesIteratorPtr deviceIterPtr = pTvDb->getDevicesIterator();
    while (!deviceIterPtr->done())
    {
      deviceId = deviceIterPtr->getDevice();

      if (deviceId.openObject()->getActive())
        indOfActiveDevice = nDevices;

      nDevices++;
      deviceIterPtr->step();
    }

    //create MDI window
    OdTvUIBaseView *view = createBaseView(databaseInfo, lastModel, deviceId, true);
    devTabBar->addTab(view, toQString(deviceId.openObject()->getName()));
    view->setWindowFlags(Qt::FramelessWindowHint);
    view->showMaximized();

    m_pFileTabs->setCurrentIndex(fileInd);
    devTabBar->tabSelected(indOfActiveDevice);

    m_pActiveView = devTabBar->getActiveView();
  }
  else
    m_pActiveView->fileAppended( toQString(strExt.c_str()) );

  m_pActiveView->getTvDeviceId().openObject(OdTv::kForWrite)->invalidate();
  m_pActiveView->update();

  // restore cursor
  QApplication::restoreOverrideCursor();

  return true;
}

bool OdTvViewerMainWindow::isClosed() const // by closeEvent
{
  return m_bClosed;
}

bool OdTvViewerMainWindow::showUrl(const QString& qsUrl,
  const QString& cqsTitle, // = "%1"
  bool bModal) // = true
{
  QUrl url(qsUrl);
  if (!url.isValid())
    return false;
  QString qsScheme = url.scheme();

  QString qsLocal = url.toLocalFile();
  if (!qsLocal.isEmpty() && !QFileInfo(qsLocal).exists())
    return false;

  if (qsScheme.isEmpty() || qsScheme == "qrc")
  {
    QFileInfo info(QString(qsUrl).replace("qrc:", ":"));
    if (!info.exists())
      return false;
  }

  QString qsTitle;
  if (!cqsTitle.isEmpty()) {
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
    qsTitle = QObject::tr(QString(cqsTitle).arg(getAppName()).toAscii().data());
#else
    qsTitle = QObject::tr(QString(cqsTitle).arg(getAppName()).toLatin1().data());
#endif
  }

  if (bModal)
  {
    // Workaround for : macx : QTextBrowser close button unavailable on modal window
    QDialog dlg(this);
    if (!qsTitle.isEmpty())
      dlg.setWindowTitle(qsTitle);
    restoreWidgetPlacement(&dlg, "modal", 600, 240);
    // dlg.setWindowState(Qt::WindowMaximized);

    QVBoxLayout* pLayout = new QVBoxLayout(&dlg);

    OdSharedPtr<QTextBrowser> sv_pBrowser = m_pBrowser;

    m_pBrowser = new QTextBrowser(&dlg);
    m_pBrowser->setFrameStyle(QFrame::NoFrame);
    m_pBrowser->setReadOnly(true);
    m_pBrowser->setAttribute(Qt::WA_DeleteOnClose, false);

    connect(m_pBrowser, SIGNAL(sourceChanged(QUrl)),
      this, SLOT(changedBrowserSource(QUrl)), Qt::QueuedConnection);
    connect(m_pBrowser, SIGNAL(anchorClicked(QUrl)),
      this, SLOT(clickedBrowserAnchor(QUrl)));

    m_pBrowser->setSource(url);
    pLayout->addWidget(m_pBrowser);

    QPushButton* pCloseButton = new QPushButton(tr("OK"));
    pCloseButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    pCloseButton->setMinimumSize(100, pCloseButton->minimumSize().height());
    connect(pCloseButton, SIGNAL(clicked()),
      this, SLOT(clickedBrowserCloseButton()));
    pLayout->addWidget(pCloseButton,
      0, //stretch
      Qt::AlignHCenter); // alignment
                         //  dlg.adjustSize();
    dlg.exec();

    saveWidgetPlacement(&dlg, "modal");
    dlg.close();

    m_pBrowser = sv_pBrowser;
    return true;
  }

  if (m_pBrowser.isNull())
  {
    m_pBrowser = new QTextBrowser(this);
    m_pBrowser->setWindowIcon(windowIcon());
    m_pBrowser->setReadOnly(true);
    restoreWidgetPlacement(m_pBrowser, "browser", 770, 575); // size at first start
  }
  m_pBrowser->setWindowModality(Qt::NonModal);

  if (!cqsTitle.isEmpty())
    m_pBrowser->setWindowTitle(qsTitle);

  m_pBrowser->setParent(NULL, Qt::Dialog);

  connect(m_pBrowser, SIGNAL(sourceChanged(QUrl)),
    this, SLOT(changedBrowserSource(QUrl)), Qt::QueuedConnection);
  connect(m_pBrowser, SIGNAL(anchorClicked(QUrl)),
    this, SLOT(clickedBrowserAnchor(QUrl)));
  m_pBrowser->setSource(url);

  m_pBrowser->show();
  return true;
}

bool OdTvViewerMainWindow::isBrowserClosed() const
{
  if (m_pBrowser.isNull())
    return true;

  QDialog* pParent = qobject_cast<QDialog*>(m_pBrowser->parent());
  Q_ASSERT(pParent || m_pBrowser->windowModality() == Qt::NonModal);
  if (!pParent || !pParent->isVisible())
    return true;

  return false;
}

// get active view ptr
OdTvGsViewPtr OdTvViewerMainWindow::getActiveTvViewPtr()
{
  return m_pActiveView->getActiveTvViewPtr();
}

// PROTECTED METHODS
void OdTvViewerMainWindow::closeEvent(QCloseEvent *event)
{
  if (m_pFileTabs)
    m_pFileTabs->closeAll();

  m_pMdiArea->closeAllSubWindows();
  if (m_pMdiArea->currentSubWindow())
  {
    event->ignore();
  }
  else
  {
    saveWidgetPlacement(this);
    saveWidgetPlacement(m_pBrowser.get(), "browser");
    event->accept();
  }

  if ( m_mapDbNumberOfViews.size() > 0)
  {
    ODA_ASSERT_ONCE(false);
  }
 
  m_mapDbNumberOfViews.clear();

  m_bClosed = true;
}

void OdTvViewerMainWindow::mouseReleaseEvent(QMouseEvent *event)
{
  int rx = event->pos().rx();
  int ry = event->pos().ry();
  if ( (rx > 210 || ry > 470) && m_pSliderMenu->isShowed() )
  {
    m_pSliderMenu->hide();
  }
  QMainWindow::mouseReleaseEvent(event);
}

void OdTvViewerMainWindow::resizeEvent(QResizeEvent *event)
{
  m_pDockPanel->adjustPanelWidth(this->width());
  m_pBgImageLbl->setPixmap(m_bgImg.scaled(size() * 0.6, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

void OdTvViewerMainWindow::keyPressEvent(QKeyEvent *event)
{
  if (m_pActiveView)
    QApplication::sendEvent(m_pActiveView, event);
}

void OdTvViewerMainWindow::callChooseDialog(OdTvFilerFeedbackForChooseObject& object)
{
  QApplication::setOverrideCursor(Qt::ArrowCursor);

  OdTvUIViewsChooseDialog* dlg = new OdTvUIViewsChooseDialog(object, QApplication::activeWindow());
  QDialog::DialogCode dlgCode = (QDialog::DialogCode)dlg->exec();

  QApplication::restoreOverrideCursor();

  if (dlgCode == QDialog::Rejected)
  {
    object.getFilerFeedbackItemForChooseArrayPtr()->clear();
  }

  delete dlg;
}

void OdTvViewerMainWindow::dragEnterEvent(QDragEnterEvent *event)
{
  if (event->mimeData()->hasUrls())
    event->acceptProposedAction();
}

void OdTvViewerMainWindow::dropEvent(QDropEvent *event)
{
  const QMimeData *pMimeData = event->mimeData();
  // check for our needed mime type, here a file or a list of files
  if (!pMimeData->hasUrls())
    return;

  QStringList pathList;
  QList<QUrl> urlList = pMimeData->urls();

  // extract the local paths of the files
  for (int i = 0; i < urlList.size() && i < 20; i++)
    pathList.append(urlList.at(i).toLocalFile());

  // validation of files
  QString allSupportedStr = getAllSupportedFormatsString();
  QString firstExt = "";
  bool bIsSameFormat = true;
  for (int i = 0; i < pathList.size(); i++)
  {
    QString path = pathList[i];
    QFileInfo fInfo(path);
    QString ext = fInfo.suffix();
    if(!allSupportedStr.contains(ext))
    {
      QMessageBox::warning(this, getAppName(), QObject::tr("Format of file \"%1\" is not supported.").arg(path));
      pathList.removeAt(i--);
      continue;
    }
    // remember first extension
    if (firstExt.isEmpty())
      firstExt = ext;
    // compare extensions
    if (ext != firstExt)
      bIsSameFormat = false;
  }

  // focus on app after drop files
  this->activateWindow();

  OdTvFileOptions opt("", true, false, pathList.size() == 1 || bIsSameFormat, pathList.size() == 1, bIsSameFormat);
  for (int i = 0; i < pathList.size(); i++)
  {
    opt.fileName = pathList[i];
    openFile(opt);
  }
}

// SLOTS
// on menu button click slot
void OdTvViewerMainWindow::menuButtonClick(const QString& btnName)
{
  if (m_pSliderMenu->getRecentList().contains(btnName))
  {
    recentFileClick(btnName);
    return;
  }

  m_pSliderMenu->hide();

  if (!btnName.compare("Open"))
    open();
  else if (!btnName.compare("Exit"))
    onCloseClick();
  else if (!btnName.compare("New"))
    newFile();
  else if (!btnName.compare("Append"))
    append();
  else if (!btnName.compare("Save"))
    save();
  else if (!btnName.compare("SaveAs"))
    saveAs();
  else if (!btnName.compare("Options"))
    createGlobalOptionsDlg();
  else if (!btnName.compare("PDF"))
    exportToPDF();
  else if (!btnName.compare(".dwg"))
    exportToDWG();
  else if (!btnName.compare("OBJ"))
    exportToOBJ();
  else if (!btnName.compare("XML"))
    exportToXML();
  else if (!btnName.compare("Soccer"))
    generateDatabase(m_Tvfactory, kSoccer);
  else if (!btnName.compare("AllEntities"))
    generateDatabase(m_Tvfactory, kAllEntities);
  else if (!btnName.compare("2DPlot"))
    generateDatabase(m_Tvfactory, kPlot2D);
  else if (!btnName.compare("SurfacePlot"))
    runSurfPlotSample();
  else if (!btnName.compare("CAE"))
    generateDatabase(m_Tvfactory, kPlotCAE);
  else if (!btnName.compare("Sword"))
    generateDatabase(m_Tvfactory, kMaterials);
  else if (!btnName.compare("SphericalMapping"))
    generateDatabase(m_Tvfactory, kMaterialSphereMap);
  else if (!btnName.compare("CylindricalMapping"))
    generateDatabase(m_Tvfactory, kMaterialCylindricalMap);
  else if (!btnName.compare("Lights"))
    generateDatabase(m_Tvfactory, kMaterialLightTeapot);
  else if (!btnName.compare("Opacity"))
    generateDatabase(m_Tvfactory, kMaterialOpacityMap);
  else if (!btnName.compare("Bump"))
    generateDatabase(m_Tvfactory, kMaterialBumpMap);
  else if (!btnName.compare("CircEllipArc"))
    generateDatabase(m_Tvfactory, kCircleEllipse);
  else if (!btnName.compare("Linetypes"))
    generateDatabase(m_Tvfactory, kLinetypes);
  else if (!btnName.compare("Layers"))
    generateDatabase(m_Tvfactory, kLayers);
  else if (!btnName.compare("Transparency"))
    generateDatabase(m_Tvfactory, kTransparency);
  else if (!btnName.compare("Light"))
    generateDatabase(m_Tvfactory, kLight);
  else if (!btnName.compare("Materials"))
    generateDatabase(m_Tvfactory, kDebugMaterials);
  else if (!btnName.compare("ShellFaces"))
    generateDatabase(m_Tvfactory, kShellFacesAttributes);
  else if (!btnName.compare("ShellEdges"))
    generateDatabase(m_Tvfactory, kShellEdgesAttributes);
  else if (!btnName.compare("Cylinder"))
    generateDatabase(m_Tvfactory, kCylinderAttributes);
  else if (!btnName.compare("Sphere"))
    generateDatabase(m_Tvfactory, kSphereAttributes);
  else if (!btnName.compare("Box"))
    generateDatabase(m_Tvfactory, kBoxAttributes);
  else if (!btnName.compare("MeshFaces"))
    generateDatabase(m_Tvfactory, kMeshFacesAttributes);
  else if (!btnName.compare("MeshEdges"))
    generateDatabase(m_Tvfactory, kMeshEdgesAttributes);
  else if (!btnName.compare("ResetColor"))
    generateDatabase(m_Tvfactory, kResetColor);
  else if (!btnName.compare("ResetLineweight"))
    generateDatabase(m_Tvfactory, kResetLineWeight);
  else if (!btnName.compare("ResetLinetype"))
    generateDatabase(m_Tvfactory, kResetLinetype);
  else if (!btnName.compare("ResetLinetypeScale"))
    generateDatabase(m_Tvfactory, kResetLinetypeScale);
  else if (!btnName.compare("ResetLayer"))
    generateDatabase(m_Tvfactory, kResetLayer);
  else if (!btnName.compare("ResetVisibility"))
    generateDatabase(m_Tvfactory, kResetVisibility);
}

void OdTvViewerMainWindow::recentFileClick(const QString& filePath)
{
  m_pSliderMenu->hide();
  if (filePath.isEmpty())
    return;
  
  if (m_pFileTabs->count() > 0)
  {
    QFileInfo fInfo(filePath);
    QString fileName = fInfo.fileName();
    for (int i = 0; i < m_pFileTabs->count(); i++)
    {
      if (!fileName.compare(m_pFileTabs->tabText(i)))
      {
        m_pFileTabs->setCurrentIndex(i);
        return;
      }
    }
  }

  openFile(filePath, m_generalOptions.getPartialOpenEnabled() ? false : m_generalOptions.getMTOpenEnabled(), m_generalOptions.getPartialOpenEnabled(), m_generalOptions.getEnableMemoryLimit(), m_generalOptions.getMemoryLimit());
}

// update enabled\disabled buttons slot
void OdTvViewerMainWindow::updateButtonsState()
{
  bool bExist = m_pActiveView;
  setButtonsBarEnabled(bExist);
  m_pSliderMenu->getElementByName("Save")->setEnabled(bExist);
  m_pSliderMenu->getElementByName("SaveAs")->setEnabled(bExist);
  m_pSliderMenu->getElementByName("Export")->setEnabled(bExist);
  if (!bExist && m_pSliderMenu->isShowed())
    m_pSliderMenu->hide();
  updateViewButtonState();
  updateProjectionButtonState();
  updateRenderModeButtonState();
  updateAppearanceButtonState();
  updatePanelsButtonState();
  updateSectioningButtonState();
}

// hide expanded menu slot
void OdTvViewerMainWindow::onMenuClicked()
{
  if (m_pSliderMenu->isShowed()) 
    m_pSliderMenu->hide();
  else 
    m_pSliderMenu->show();
}

void OdTvViewerMainWindow::onRecentExpand()
{
  if (m_pRecentMenu->isShowed())
    m_pRecentMenu->hide();
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pRecentMenu->setSetting(btn->geometry().top() + m_pMenuToolbar->getContainer()->height(), m_pRecentMenu->getWidth(), 200, m_pSliderMenu->getContainer()->width());
    m_pSliderMenu->hideChilds();
    m_pRecentMenu->show();
  }
}

// slots for expand nested menus
void OdTvViewerMainWindow::onExportExpand()
{
  if (m_pExportMenu->isShowed())
    m_pExportMenu->hide();
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pExportMenu->setSetting(btn->geometry().top() + m_pMenuToolbar->getContainer()->height(), 200, 200, m_pSliderMenu->getContainer()->width());
    m_pSliderMenu->hideChilds();
    m_pExportMenu->show();
  }
}

void OdTvViewerMainWindow::onExamplesExpand()
{
  if (m_pExampleMenu->isShowed())
  {
    m_pExampleMenu->hide();
  }
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pExampleMenu->setSetting(btn->geometry().top() + m_pMenuToolbar->getContainer()->height(), 150, 200, m_pSliderMenu->getContainer()->width());
    m_pSliderMenu->hideChilds();
    m_pExampleMenu->show();
  }
}

void OdTvViewerMainWindow::onPlotsExpand()
{
  if (m_pPlotsMenu->isShowed())
    m_pPlotsMenu->hide();
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pPlotsMenu->setSetting(btn->geometry().top() + m_pExampleMenu->getTopOffset(), 150, 200, m_pSliderMenu->getContainer()->width() + m_pExampleMenu->getContainer()->width());
    m_pExampleMenu->hideChilds();
    m_pPlotsMenu->show();
  }
}

void OdTvViewerMainWindow::onMaterialsExpand()
{
  if (m_pMaterialsMenu->isShowed())
    m_pMaterialsMenu->hide();
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pMaterialsMenu->setSetting(btn->geometry().top() + m_pExampleMenu->getTopOffset(), 200, 200, m_pSliderMenu->getContainer()->width() + m_pExampleMenu->getContainer()->width());
    m_pExampleMenu->hideChilds();
    m_pMaterialsMenu->show();
  }
}

void OdTvViewerMainWindow::onDebugExamplesExpand()
{
  if (m_pDebugExamplesMenu->isShowed())
    m_pDebugExamplesMenu->hide();
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pDebugExamplesMenu->setSetting(btn->geometry().bottom() + m_pExampleMenu->getTopOffset() - 9*40, 220, 200, m_pSliderMenu->getContainer()->width() + m_pExampleMenu->getContainer()->width());
    m_pExampleMenu->hideChilds();
    m_pDebugExamplesMenu->show();
  }
}

void OdTvViewerMainWindow::onDbgShellAttrExpand()
{
  if (m_pDbgShellAttributesMenu->isShowed())
    m_pDbgShellAttributesMenu->hide();
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pDbgShellAttributesMenu->setSetting(btn->geometry().top() + m_pDebugExamplesMenu->getTopOffset(), 150, 200, m_pSliderMenu->getContainer()->width()
        + m_pExampleMenu->getContainer()->width() + m_pDebugExamplesMenu->getContainer()->width());
    m_pDebugExamplesMenu->hideChilds();
    m_pDbgShellAttributesMenu->show();
  }
}

void OdTvViewerMainWindow::onDbgMeshAttrExpand()
{
  if (m_pDbgMeshAttributesMenu->isShowed())
    m_pDbgMeshAttributesMenu->hide();
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pDbgMeshAttributesMenu->setSetting(btn->geometry().top() + m_pDebugExamplesMenu->getTopOffset(), 150, 200, m_pSliderMenu->getContainer()->width()
        + m_pExampleMenu->getContainer()->width() + m_pDebugExamplesMenu->getContainer()->width());
    m_pDebugExamplesMenu->hideChilds();
    m_pDbgMeshAttributesMenu->show();
  }
}

void OdTvViewerMainWindow::onDbgResetTraitsExpand()
{
  if (m_pDbgResetTraitsMenu->isShowed())
    m_pDbgResetTraitsMenu->hide();
  else
  {
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if (btn)
      m_pDbgResetTraitsMenu->setSetting(btn->geometry().top() + m_pDebugExamplesMenu->getTopOffset(), 200, 200, m_pSliderMenu->getContainer()->width()
        + m_pExampleMenu->getContainer()->width() + m_pDebugExamplesMenu->getContainer()->width());
    m_pDebugExamplesMenu->hideChilds();
    m_pDbgResetTraitsMenu->show();
  }
}

// slide to clicked panel (navigation, style, projection, etc.)
void OdTvViewerMainWindow::onItemPanelChoose(int ind)
{
  if (m_pItemPanel->getContainer()->currentIndex() == ind)
  {
    m_pItemPanel->setVisibility(true);
    return;
  }

  if (ind < 0)
  {
    m_pItemPanel->setVisibility(false);
    return;
  }

  m_pMenuToolbar->setButtonsDisable();
  m_pItemPanel->getContainer()->slideInIdx(ind);

  if (m_pItemPanel->getSectioningPanel() && m_pItemPanel->getSectioningPanel() == m_pItemPanel->getContainer()->widget(ind))
    onAppearSectioningPanel(true);
  else
    onAppearSectioningPanel(false);
}

// slots for clicked buttons
void OdTvViewerMainWindow::onChangeActionSl(const QString& str)
{
  if (!m_pActiveView)
    return;
  if (!str.compare("pan"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kPan);
  else if (!str.compare("orbit"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kOrbit);
  else if (!str.compare("freeOrbit"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kFreeOrbit);
  else if (!str.compare("collide"))
  {
    if (!m_pActiveView->runCollideAction())
    {
      QWidget* pWidget = m_pItemPanel->getElementByName("Collide");
      if (pWidget)
        pWidget->setStyleSheet("border: none;");
    }
  }
  else if (!str.compare("collideDialog"))
  {
    if (m_pDockPanel->getActivePropertiesPalette())
      m_pDockPanel->getActivePropertiesPalette()->initCollisionDetectionResults(true);
  }
  else if (!str.compare("zoomIn"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kZoomIn);
  else if (!str.compare("zoomOut"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kZoomOut);
  else if (!str.compare("zoomWindow"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kZoomWindow);
  else if (!str.compare("zoomExtents"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kZoomExtents);
  else if (!str.compare("rotation"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kRotation);
  else if (!str.compare("rotationSettings"))
    m_pActiveView->runNavigationAction(OdTvUIBaseView::kRotationSettings);
  else if (!str.compare("markupRect"))
    m_pActiveView->runMarkupAction(OdTvUIBaseView::kRectangle);
  else if (!str.compare("markupCircle"))
    m_pActiveView->runMarkupAction(OdTvUIBaseView::kCircle);
  else if (!str.compare("markupHandle"))
    m_pActiveView->runMarkupAction(OdTvUIBaseView::kHandle);
  else if (!str.compare("markupCloud"))
    m_pActiveView->runMarkupAction(OdTvUIBaseView::kCloud);
  else if (!str.compare("markupText"))
    m_pActiveView->runMarkupAction(OdTvUIBaseView::kText);
  else if (!str.compare("markupSave"))
    m_pActiveView->runMarkupAction(OdTvUIBaseView::kSave);
  else if (!str.compare("markupLoad"))
    m_pActiveView->runMarkupAction(OdTvUIBaseView::kLoad);
}

void OdTvViewerMainWindow::onChangeViewSl(const QString& str)
{
  if (!str.compare("Top"))
    m_pActiveView->set3DView(OdTvExtendedView::kTop);
  else if (!str.compare("Bottom"))
    m_pActiveView->set3DView(OdTvExtendedView::kBottom);
  else if (!str.compare("Left"))
    m_pActiveView->set3DView(OdTvExtendedView::kLeft);
  else if (!str.compare("Right"))
    m_pActiveView->set3DView(OdTvExtendedView::kRight);
  else if (!str.compare("Front"))
    m_pActiveView->set3DView(OdTvExtendedView::kFront);
  else if (!str.compare("Back"))
    m_pActiveView->set3DView(OdTvExtendedView::kBack);
  else if (!str.compare("SW"))
    m_pActiveView->set3DView(OdTvExtendedView::kSW);
  else if (!str.compare("SE"))
    m_pActiveView->set3DView(OdTvExtendedView::kSE);
  else if (!str.compare("NE"))
    m_pActiveView->set3DView(OdTvExtendedView::kNE);
  else if (!str.compare("NW"))
    m_pActiveView->set3DView(OdTvExtendedView::kNW);
}

void OdTvViewerMainWindow::onChangeRenderModeSl(const QString& str)
{
  if (!str.compare("2dWireframe"))
    m_pActiveView->runRenderMode(OdTvGsView::k2DOptimized);
  else if (!str.compare("Wireframe"))
    m_pActiveView->runRenderMode(OdTvGsView::kWireframe);
  else if (!str.compare("Hidden"))
    m_pActiveView->runRenderMode(OdTvGsView::kHiddenLine);
  else if (!str.compare("Shaded"))
    m_pActiveView->runRenderMode(OdTvGsView::kFlatShaded);
  else if (!str.compare("Gouraud"))
    m_pActiveView->runRenderMode(OdTvGsView::kGouraudShaded);
  else if (!str.compare("Shaded with edges"))
    m_pActiveView->runRenderMode(OdTvGsView::kFlatShadedWithWireframe);
  else if (!str.compare("GouraudWithEdges"))
    m_pActiveView->runRenderMode(OdTvGsView::kGouraudShadedWithWireframe);
}

void OdTvViewerMainWindow::onChangeProjectionSl(const QString& str)
{
  if (!m_pActiveView)
    return;
  OdTvGsView::Projection proj = OdTvGsView::kParallel;
  if (!str.compare("Perspective"))
    proj = OdTvGsView::kPerspective;
  OdTvGsViewPtr pView = m_pActiveView->getActiveTvViewPtr();
  if (pView.isNull())
    return;
  bool isPersp = pView->isPerspective();
  if (proj != (int)isPersp)
  {
    pView->setView(pView->position(), pView->target(), pView->upVector(), pView->fieldWidth(), pView->fieldHeight(), proj);
    m_pActiveView->update();
  }
}

void OdTvViewerMainWindow::onDrawGeometrySl(const QString& str)
{
  if (!m_pActiveView)
    return;
  
  if (!str.compare("Line"))
    m_pActiveView->drawGeometry(OdTv::kPolyline);
  else if (!str.compare("Ray"))
    m_pActiveView->drawGeometry(OdTv::kInfiniteLine, OdTvInfiniteLineData::kRay);
  else if (!str.compare("Xline"))
    m_pActiveView->drawGeometry(OdTv::kInfiniteLine, OdTvInfiniteLineData::kLine);
  else if (!str.compare("Nurbs"))
    m_pActiveView->drawGeometry(OdTv::kNurbs);
  else if (!str.compare("CircleRadius"))
    m_pActiveView->drawGeometry(OdTv::kCircle, 0);
  else if (!str.compare("Circle3Point"))
    m_pActiveView->drawGeometry(OdTv::kCircle, 1);
  else if (!str.compare("Ellipse"))
    m_pActiveView->drawGeometry(OdTv::kEllipse);
  else if (!str.compare("CircArc"))
    m_pActiveView->drawGeometry(OdTv::kCircularArc);
  else if (!str.compare("CircWedge"))
    m_pActiveView->drawGeometry(OdTv::kCircleWedge);
  else if (!str.compare("EllipArc"))
    m_pActiveView->drawGeometry(OdTv::kEllipticArc);
  else if (!str.compare("Image"))
    m_pActiveView->drawGeometry(OdTv::kRasterImage);
  else if (!str.compare("Box"))
    m_pActiveView->drawGeometry(OdTv::kBox);
  else if (!str.compare("Sphere"))
    m_pActiveView->drawGeometry(OdTv::kSphere);
  else if (!str.compare("Cylinder"))
    m_pActiveView->drawGeometry(OdTv::kCylinder);
}

void OdTvViewerMainWindow::onSetRegen(const QString& str)
{
  if (!str.compare("Regen All"))
    m_pActiveView->runRegenAction(OdTvUIBaseView::kAll);
  else if (!str.compare("Regen Active View"))
    m_pActiveView->runRegenAction(OdTvUIBaseView::kView);
  else if (!str.compare("Regen Visible"))
    m_pActiveView->runRegenAction(OdTvUIBaseView::kVisible);
}

void OdTvViewerMainWindow::updateStateRemoveCuttingPlanesButtonSl()
{
  if (!m_pActiveView || m_pActiveView->getActiveTvViewId().isNull())
    return;

  OdTvGsViewPtr pActiveView = m_pActiveView->getActiveTvViewId().openObject();
  if (!pActiveView.isNull())
  {
    // Enable/disable remove button
    m_pItemPanel->getElementByName("Remove cutting planes")->setEnabled(pActiveView->numCuttingPlanes() > 0);
  }
}

void OdTvViewerMainWindow::resetRotationBtnSl()
{
  m_pItemPanel->getEventManager()->resetWidgetUnderlined(m_pItemPanel->getElementByName("Rotation"));
}

void OdTvViewerMainWindow::onSectioningChangeSl(const QString& str)
{
  if (!m_pActiveView || m_pActiveView->getActiveTvViewId().isNull())
    return;

  OdTvVector viewVector = m_pActiveView->getActiveTvViewPtr()->position() - m_pActiveView->getActiveTvViewPtr()->target();

  if (!str.compare("CuttingPlane"))
    showCuttingPlanes();
  else if (!str.compare("AddCuttingPlaneByX"))
  {
    double xDot = viewVector.dotProduct(OdTvVector::kXAxis);
    if (OdZero(xDot))
      xDot = 1.;

    OdTvVector axis = OdTvVector::kXAxis;
    axis *= -xDot;
    axis.normalize();
    addCuttingPlane(axis);
  }
  else if (!str.compare("AddCuttingPlaneByY"))
  {
    double yDot = viewVector.dotProduct(OdTvVector::kYAxis);
    if (OdZero(yDot))
      yDot = 1.;

    OdTvVector axis = OdTvVector::kYAxis;
    axis *= -yDot;
    axis.normalize();
    addCuttingPlane(axis);
  }
  else if (!str.compare("AddCuttingPlaneByZ"))
  {
    double zDot = viewVector.dotProduct(OdTvVector::kZAxis);
    if (OdZero(zDot))
      zDot = 1.;

    OdTvVector axis = OdTvVector::kZAxis;
    axis *= -zDot;
    axis.normalize();
    addCuttingPlane(axis);
  }
  else if (!str.compare("CuttingPlaneProperties"))
    cuttingPlaneProperties();
  else if (!str.compare("RemoveCuttingPlane"))
    removeCuttingPlanes();
}

void OdTvViewerMainWindow::onChangeAppearanceSl(const QString& str)
{
  if (!m_pActiveView)
    return;

  if (!str.compare("WCS"))
    onOffWcs();
  else if (!str.compare("FPS"))
    onOffFps();
  else if (!str.compare("Grid"))
    onOffGrid();
  else if (!str.compare("Background"))
    setBgColor();
}

// reset current dragger
void OdTvViewerMainWindow::resetDraggerButtonStateSl()
{
  if (!m_pActiveView || !m_pActiveView->getActiveDragger())
    return;
  m_pItemPanel->getEventManager()->setBorderForAction();

  OdTvDragger *dragger = m_pActiveView->getActiveDragger();
  OdTvUIPanDragger *panDragger = dynamic_cast<OdTvUIPanDragger*>(dragger);
  if (panDragger)
  {
    setWidgetUnderlined(m_pItemPanel->getElementByName("Pan"));
    return;
  }
  OdTvUIOrbitDragger *orbitDragger = dynamic_cast<OdTvUIOrbitDragger*>(dragger);
  if (orbitDragger)
  {
    setWidgetUnderlined(m_pItemPanel->getElementByName("Orbit"));
    return;
  }
  OdTvUIFreeOrbitDragger *freeOrbitDragger = dynamic_cast<OdTvUIFreeOrbitDragger*>(dragger);
  if (freeOrbitDragger)
  {
    setWidgetUnderlined(m_pItemPanel->getElementByName("Free Orbit"));
    return;
  }
  OdTvUIZoomWindowDragger *zoomWindowDragger = dynamic_cast<OdTvUIZoomWindowDragger*>(dragger);
  if (zoomWindowDragger)
  {
    setWidgetUnderlined(m_pItemPanel->getElementByName("Zoom window"));
    return;
  }
  OdTvUICollideDragger *collideDragger = dynamic_cast<OdTvUICollideDragger*>(dragger);
  if (collideDragger)
  {
    setWidgetUnderlined(m_pItemPanel->getElementByName("Collide"));
    return;
  }

}

// slot for update view buttons
void OdTvViewerMainWindow::updateViewStateSl()
{
  updateViewButtonState();
}

// create dialog with options
void OdTvViewerMainWindow::createGlobalOptionsDlg()
{
  m_pSliderMenu->hide();

  OdTvUIBaseView* pActiveWnd = m_pActiveView;
  OdTvUICommonParamsDlg *dlg = NULL;
  if (!pActiveWnd)
    dlg = new OdTvUICommonParamsDlg(m_performanceOptions, m_gridOptions, m_appearanceOptions, m_generalOptions, &m_importParams, false, this);
  else
    dlg = new OdTvUICommonParamsDlg(pActiveWnd->getPerformanceOptions(), pActiveWnd->getGridOptions(), pActiveWnd->getAppearanceOptions(), pActiveWnd->getGeneralOptions(), &m_importParams, false, this);

  if (!pActiveWnd)
  {
    connect(dlg->getPerformancePage(), SIGNAL(okBtnDownSg()), this, SLOT(setPerformanceGlobal()));
    connect(dlg->getGridPage(), SIGNAL(okBtnSg()), this, SLOT(setGridGlobal()));
    connect(dlg->getAppearancePage(), SIGNAL(okSg()), this, SLOT(setAppearanceGlobal()));
    connect(dlg->getGeneralPage(), SIGNAL(okBtnSg()), this, SLOT(setGeneralGlobal()));
  }
  else
  {
    connect(dlg->getPerformancePage(), SIGNAL(okBtnDownSg()), this, SLOT(setPerformance()));
    connect(dlg->getGridPage(), SIGNAL(okBtnSg()), this, SLOT(setGrid()));
    connect(dlg->getAppearancePage(), SIGNAL(okSg()), this, SLOT(setAppearance()));
    connect(dlg->getGeneralPage(), SIGNAL(okBtnSg()), this, SLOT(setGeneral()));
  }

  int dlgCode = dlg->exec();

}

void OdTvViewerMainWindow::onAppearSectioningPanel(bool bAppear)
{
  if (!m_pActiveView)
    return;

  m_pActiveView->onAppearSectioningPanel(bAppear);
}

void OdTvViewerMainWindow::onOpeningError()
{
  // restore override cursor
  QApplication::restoreOverrideCursor();

  QMessageBox::warning(this, getAppName(), tr("Error during open file."));
}

OdUInt64 OdTvViewerMainWindow::countViewsInDevices(OdTvDevicesIteratorPtr& pDevicesIterator) const
{
  OdUInt64 res = 0;

  while (!pDevicesIterator->done())
  {
    OdTvGsDeviceId deviceId = pDevicesIterator->getDevice();
    OdTvGsDevicePtr pDevice = deviceId.openObject();

    if (!pDevice->getActiveView().isNull())
      res++;

    pDevicesIterator->step();
  }

  return res;
}

// extras buttons slots
void OdTvViewerMainWindow::showCuttingPlanes()
{
  if (!m_pActiveView)
    return;

  bool bOldShowState = m_pActiveView->getSectioningOptions()->getShown();

  //transfer changes to the view
  m_pActiveView->setShowCuttingPlanes(!bOldShowState);
}

void OdTvViewerMainWindow::addCuttingPlane(const OdTvVector& axis)
{
  if (!m_pActiveView)
    return;

  OdTvResult res = tvOk;
  if (!m_pActiveView->addCuttingPlane(axis))
  {
    if (res == tvOk)
    {
      QMessageBox::warning(this, getAppName(),
        QObject::tr("There are can not be more than %1 cutting planes.")
        .arg(ODTVUI_CUTTINGPLANESMAXNUM));
      return;
    }
  }
}

void OdTvViewerMainWindow::cuttingPlaneProperties()
{
  if (!m_pActiveView)
    return;

  OdTvUICuttingPlaneParamsDlg* dlg = new OdTvUICuttingPlaneParamsDlg(m_pActiveView, this);

  int dlgCode = dlg->exec();

  delete dlg;
}

void OdTvViewerMainWindow::removeCuttingPlanes()
{
  if (!m_pActiveView)
    return;

  m_pActiveView->removeCuttingPlanes();
}

// appearance buttons slots
void OdTvViewerMainWindow::onOffWcs()
{
  if (!m_pActiveView)
    return;
  m_generalOptions.setEnableWcs(!m_generalOptions.getEnableWcs());
  m_pActiveView->setGeneralOptions(m_generalOptions);
  updateAppearanceButtonState();
}

void OdTvViewerMainWindow::setBgColor()
{
  ODCOLORREF backColor;
  if (!m_pActiveView)
    backColor = m_appearanceOptions.getBackground(); // it is impossible but for safety
  else
    backColor = m_pActiveView->getAppearanceOptions().getBackground();

  QColor initColor = QColor(ODGETRED(backColor), ODGETGREEN(backColor), ODGETBLUE(backColor));

  QColor color = QColorDialog::getColor(initColor);
  if (!color.isValid())
    return;
  OdUInt32 iColor = ODRGB(color.red(), color.green(), color.blue());
  if (m_pActiveView)
  {
    m_pActiveView->setBgColor(iColor);
    m_appearanceOptions.setBackground(iColor);
    m_pActiveView->setAppearanceOptions(m_appearanceOptions);
    updateAppearanceButtonState();
  }
}

void OdTvViewerMainWindow::onOffGrid()
{
  if (!m_pActiveView)
    return;
  m_generalOptions.setEnableGrid(!m_generalOptions.getEnableGrid());
  m_pActiveView->setGeneralOptions(m_generalOptions);
  updateAppearanceButtonState();
}

void OdTvViewerMainWindow::onOffFps()
{
  if (!m_pActiveView)
    return;
  m_generalOptions.setEnableFPS(!m_generalOptions.getEnableFPS());
  m_pActiveView->setGeneralOptions(m_generalOptions);
  updateAppearanceButtonState();
}

void OdTvViewerMainWindow::updateBrowserTree()
{
  if (m_pActiveView)
  {
    getObjectExplorer()->setActiveView(m_pActiveView);
    getPropertiesPalette()->setActiveView(m_pActiveView);

    if (getCDANodesProperties())
      getCDANodesProperties()->setActiveView(m_pActiveView);
#ifdef SUPPORTNATIVEPALETTE
    if (getObjectNativeProperties())
      getObjectNativeProperties()->setActiveView(m_pActiveView);
#endif
    getObjectExplorer()->fill(m_pActiveView->getTvDatabaseId());
  }
}

// enable or disable model browser and properties slots
void OdTvViewerMainWindow::onOffModelBrowser()
{
  m_bBrowserIsEnabled = !m_bBrowserIsEnabled;
  m_pDockPanel->getExplorerItem()->getContainer()->setVisible(m_bBrowserIsEnabled);
  updatePanelsButtonState();
}

void OdTvViewerMainWindow::onOffProperties()
{
  m_bPropertiesIsEnabled = !m_bPropertiesIsEnabled;
  m_pDockPanel->getPropertiesItem()->getContainer()->setVisible(m_bPropertiesIsEnabled);
  updatePanelsButtonState();
}

// create view
OdTvUIBaseView * OdTvViewerMainWindow::createBaseView(const OdTvUIDatabaseInfo& databaseInfo, OdTvModelId tvmodelId, OdTvGsDeviceId tvDeviceId /*= OdTvGsDeviceId()*/
  , bool bLoadFile /*= false*/)
{
  OdTvUIBaseView *view = new OdTvUIBaseView(databaseInfo, tvmodelId, tvDeviceId, bLoadFile, m_performanceOptions, m_gridOptions,
                                            m_appearanceOptions, m_generalOptions, this);
  view->resize(m_pMdiArea->size() * devicePixelRatio());

  // update database - view map
  OdTvDatabaseId tvDatabaseId = view->getTvDatabaseId();
  if (m_mapDbNumberOfViews.contains(tvDatabaseId))
  {
    m_mapDbNumberOfViews[tvDatabaseId]++;
  }
  else
    m_mapDbNumberOfViews.insert(tvDatabaseId, 1);

  connect(view, SIGNAL(escPressedSg()), SLOT(resetDraggerButtonStateSl()));
  connect(view, SIGNAL(updateViewStateSg()), SLOT(updateViewStateSl()));
  connect(view, SIGNAL(hideSliderMenuSg()), SLOT(hideSliderMenuSl()));
  connect(view, SIGNAL(closeBrowserModel()), SLOT(clearObjectExplorer()));
  connect(view, SIGNAL(closeView(OdTvUIBaseView *)), SLOT(onCloseBaseView(OdTvUIBaseView *)));
  connect(view, SIGNAL(resetRotatingBtnSg()), SLOT(resetRotationBtnSl()));

  m_appearanceOptions = view->getAppearanceOptions();
  m_gridOptions = view->getGridOptions();
  m_performanceOptions = view->getPerformanceOptions();

  return view;
}

// slot for receive the messages about the view was destroyed
void OdTvViewerMainWindow::onCloseBaseView(OdTvUIBaseView *view)
{
  m_pDockPanel->getObjectProperties()->setActiveView(NULL);
  // update database - view map
  OdTvDatabaseId tvDatabaseId = view->getTvDatabaseId();
  if (m_mapDbNumberOfViews.contains(tvDatabaseId))
  {
    //not optimal but for a few databases it is not important
    unsigned int dbReferences = m_mapDbNumberOfViews[tvDatabaseId];
    if (dbReferences <= 1)
    {
      m_mapDbNumberOfViews.remove(tvDatabaseId);
      //remove database
      OdTvFactoryId odtvfactory = odTvGetFactory();
      odtvfactory.removeDatabase(tvDatabaseId);
    }
    else
      m_mapDbNumberOfViews[tvDatabaseId]--;
  }

  if (m_pFileTabs->count() < 2)
    m_pCentralStackedWid->setCurrentWidget(m_pBgImageLbl);
}

// set active sub window
void OdTvViewerMainWindow::setActiveSubWindow(QWidget *window)
{
  if (!window)
    return;

//   m_pMdiArea->setActiveSubWindow(qobject_cast<QMdiSubWindow *>(window));
}

// browser actions command
void OdTvViewerMainWindow::changedBrowserSource(const QUrl & url)
{
  //QString qsPath = url.path();
  QString qsText = m_pBrowser->toHtml();
  QString qsCopyrightDD = toQString(TD_COPYRIGHT_W);
  qsCopyrightDD.replace("Copyright ", "Copyright"); // remove useless space
  QString qsCompanyNameDD = qsCopyrightDD;
  qsCompanyNameDD.remove(" ('Open Design')", Qt::CaseInsensitive);
  qsText.replace("$(TD_COMPANY_NAME)", qsCompanyNameDD, Qt::CaseInsensitive);
  //qsCopyrightDD.replace("\xA9", "&copy;");
  //qsCopyrightDD.replace(" Inc. ('Open Design')", ".");
  //qsCopyrightDD.replace("2012", "2013"); // temp solution // TODO: remove after realese v3.? !!!
  qsText.replace("$(TD_COPYRIGHT)", qsCopyrightDD, Qt::CaseInsensitive);

  qsText.replace("$(TD_PRODUCT_NAME)", toQString(TD_PRODUCT_NAME_W), Qt::CaseInsensitive);
  qsText.replace("$(TD_PRODUCT_NAME_START)", toQString(TD_PRODUCT_NAME_START_W), Qt::CaseInsensitive);
  // In the About box, the TM symbol for ODA needs to change to . // From: Allison Angus February 21, 2011
  //qsText.replace("$(TD_PRODUCT_NAME)", QString(TD_PRODUCT_NAME_S).replace("ODA_SDK", "ODA_SDK&reg;"), Qt::CaseInsensitive);
  //qsText.replace("$(TD_PRODUCT_NAME_START)", QString(TD_PRODUCT_NAME_START_S).replace("ODA_SDK", "ODA_SDK&reg;"), Qt::CaseInsensitive);

  //QString qsVerTD =  TD_PRODUCT_VER_STR_S; // TD_SHORT_STRING_VER_S // "3.02"
  //qsVerTD.replace(", ", "."); // for common style with Qt (same with DD short ver style)
  QString qsVerTD = QString("%1.%2.%3").arg(TD_MAJOR_VERSION).arg(TD_MINOR_VERSION).arg(TD_MAJOR_BUILD);
#if TD_MINOR_BUILD > 0
  qsVerTD += QString(".%1").arg(TD_MINOR_BUILD)
#endif
    qsText.replace("$(TD_VER_STR)", qsVerTD, Qt::CaseInsensitive);
  qsText.replace("$(TD_SHORT_VER_STR)", TD_SHORT_STRING_VER_S, Qt::CaseInsensitive);
  qsText.replace("$(QT_VER_STR)", QT_VERSION_STR, Qt::CaseInsensitive); // "4.5.3"

                                                                        // set invisible background color
                                                                        // (unfortunately m_pBrowser->setTextBackgroundColor is not usable for it)
  QPalette palette = qApp->palette();
  QColor colorSysGrey = palette.color(QPalette::Normal, QPalette::Window); //Base);
  int red = colorSysGrey.red(), // 236
    green = colorSysGrey.green(), // 233
    blue = colorSysGrey.blue(); // 216
  QString qsSysGrey = QString("bgcolor=\"#%1%2%3\"").arg(red, 2, 16, QLatin1Char('0'))
    .arg(green, 2, 16, QLatin1Char('0'))
    .arg(blue, 2, 16, QLatin1Char('0'));
  qsSysGrey = qsSysGrey.toUpper();
  qsText.replace("bgcolor=\"#ffffff\"", qsSysGrey, Qt::CaseInsensitive);
  m_pBrowser->setHtml(qsSysGrey);

  m_pBrowser->setHtml(qsText);
}

void OdTvViewerMainWindow::clickedBrowserAnchor(const QUrl& url)
{
  QString qsScheme = url.scheme();
  QString qsLocal = url.toLocalFile();

  if (!qsLocal.isEmpty() || qsScheme.isEmpty() || qsScheme == "qrc")
  {
    QString qsUrl = qsLocal;
    if (qsUrl.isEmpty())
    {
      qsUrl = url.toString();
      qsUrl.replace("qrc:", ":");
      m_pBrowser->setSource(url);
    }
    if (QFileInfo(qsUrl).exists())
      return;
  }
  if (qsScheme == "http")
  {
    // open with standard OS browser
    if (!QDesktopServices::openUrl(url))
    {
      ODA_FAIL_ONCE();
    }
  }
  m_pBrowser->setSource(m_pBrowser->source());
}

void OdTvViewerMainWindow::clickedBrowserCloseButton()
{
  ODA_ASSERT_ONCE(!m_pBrowser.isNull());

  // ways to close modal dialog on mac
  QDialog* pParent = qobject_cast<QDialog*>(m_pBrowser->parent());
  ODA_ASSERT_ONCE(pParent || m_pBrowser->windowModality() == Qt::NonModal);
  if (pParent)
    pParent->close();
  else
    m_pBrowser->close();
}

void OdTvViewerMainWindow::unlockCommandViaBrowser()
{
  m_qsBrowserLastCommand.clear();
}

// device change slots
void OdTvViewerMainWindow::setPerformance()
{
  OdTvUIPerformanceParamsPage *pOpt = qobject_cast<OdTvUIPerformanceParamsPage*>(sender());
  if (!pOpt || !m_pActiveView)
    return;

  m_performanceOptions = pOpt->getOptions();
  m_pActiveView->setPerformanceOptions(pOpt->getOptions());
  m_performanceOptions.writeInRegister(QString());
}

void OdTvViewerMainWindow::setPerformanceGlobal()
{
  OdTvUIPerformanceParamsPage *pOpt = qobject_cast<OdTvUIPerformanceParamsPage*>(sender());
  if (!pOpt)
    return;

  m_performanceOptions = pOpt->getOptions();
  m_performanceOptions.writeInRegister(QString());
}

// grid change slots
void OdTvViewerMainWindow::setGrid()
{
  OdTvUIGridParamsPage *pOpt = qobject_cast<OdTvUIGridParamsPage*>(sender());
  if (!pOpt || !m_pActiveView)
    return;

  m_pActiveView->setGridOptions(pOpt->getOptions());
  m_gridOptions.writeInRegister();
}

void OdTvViewerMainWindow::setGridGlobal()
{
  OdTvUIGridParamsPage *pOpt = qobject_cast<OdTvUIGridParamsPage*>(sender());
  if (!pOpt)
    return;

  m_gridOptions = pOpt->getOptions();
  m_gridOptions.writeInRegister();
}

// appearance change slots
void OdTvViewerMainWindow::setAppearance()
{
  OdTvUIAppearanceParamsPage *pOpt = qobject_cast<OdTvUIAppearanceParamsPage*>(sender());
  if (!pOpt || !m_pActiveView)
    return;

  m_appearanceOptions = pOpt->getOptions();
  m_pActiveView->setAppearanceOptions(pOpt->getOptions());
  m_appearanceOptions.writeInRegister();

  updateAppearanceButtonState();
}

void OdTvViewerMainWindow::setAppearanceGlobal()
{
  OdTvUIAppearanceParamsPage *pOpt = qobject_cast<OdTvUIAppearanceParamsPage*>(sender());
  if (!pOpt)
    return;

  m_appearanceOptions = pOpt->getOptions();
  m_appearanceOptions.writeInRegister();
}

void OdTvViewerMainWindow::setGeneral()
{
  OdTvUIGeneralParamsPage *pOpt = qobject_cast<OdTvUIGeneralParamsPage*>(sender());
  if (!pOpt || !m_pActiveView)
    return;

  m_generalOptions = pOpt->getOptions();
  m_pActiveView->setGeneralOptions(pOpt->getOptions());
  m_generalOptions.writeInRegister(QString());

  if (getObjectExplorer()->getPerSession() != pOpt->getOptions().getIsolateHidePerSession())
    getObjectExplorer()->setPerSession(pOpt->getOptions().getIsolateHidePerSession());

  OdTvUISelectDragger* pSelectDrager = m_pActiveView->getSelectionDragger();
  if (pSelectDrager)//we can set without checking the difference
    pSelectDrager->setPerSession(pOpt->getOptions().getIsolateHidePerSession());

  updateAppearanceButtonState();
}

void OdTvViewerMainWindow::setGeneralGlobal()
{
  OdTvUIGeneralParamsPage *pOpt = qobject_cast<OdTvUIGeneralParamsPage*>(sender());
  if (!pOpt)
    return;

  m_generalOptions = pOpt->getOptions();
  m_generalOptions.writeInRegister(QString());

  if (getObjectExplorer()->getPerSession() != pOpt->getOptions().getIsolateHidePerSession())
    getObjectExplorer()->setPerSession(pOpt->getOptions().getIsolateHidePerSession());
}

// clear object explorer
void OdTvViewerMainWindow::clearObjectExplorer()
{
  if (getObjectExplorer())
    getObjectExplorer()->setEmpty();
}

// get the appropriate import parameters by extension
OdTvBaseImportParams* OdTvViewerMainWindow::getImportParameters(const OdString& strExt, bool bCallImportDlg, bool bSetFeedBack, bool& bNeedLowMemoryImport, OdString& vsfDumpFile)
{
  QSettings settings(ORGANIZATION_NAME, APPLICATION_NAME);

  bNeedLowMemoryImport = false;
  vsfDumpFile = OdString();

  if (strExt == OD_T("DWG") || strExt == OD_T("DXF") || strExt == OD_T("DGN") || strExt == OD_T("PRC") || strExt == OD_T("U3D")
    || strExt == OD_T("RFA") || strExt == OD_T("RVT")
    || strExt == OD_T("NWF") || strExt == OD_T("NWD") || strExt == OD_T("NWC")
    || strExt == OD_T("SAT") || strExt == OD_T("SAB"))
  {
    //as a DC rect we will be use the whole MDI area
    QSize baseSize = m_pMdiArea->size();
    OdTvDCRect dcRect(0, baseSize.width(), baseSize.height(), 0);

    m_importParams.dwgImportParam.setDCRect(dcRect);
    m_importParams.dgnImportParam.setDCRect(dcRect);
    m_importParams.prcImportParam.setDCRect(dcRect);
    m_importParams.bimImportParam.setDCRect(dcRect);
    m_importParams.nwImportParam.setDCRect(dcRect);
#ifdef ACIS2VISUALIZE_ENABLED
    m_importParams.acisImportParam.setDCRect(dcRect);
#endif
  }

  if (strExt == OD_T("VSF"))
  {
    return &m_importParams.vsfReadParam;
  }
  else if ((strExt == OD_T("RCS")) || (strExt == OD_T("RCP")))
  {
    if (bCallImportDlg && !GETBIT(settings.value(SETTINGS_RCS_FLAG, 0).toUInt(), OdTvUIRcsImportParameters::kDontShow))
    {
      if (strExt == OD_T("RCS"))
      {
        if (showImportParamsDlg("RCS") == 0)
          return NULL;
      }
      else
      {
        if (showImportParamsDlg("RCP") == 0)
          return NULL;
      }
    }
    return &m_importParams.rcsImportParam;
  }
  else if (strExt == OD_T("DWG") || strExt == OD_T("DXF"))
  {
    if (bCallImportDlg && !GETBIT(settings.value(SETTINGS_DWG_FLAG, 0).toUInt(), OdTvUIDwgImportParameters::kDontShow))
    {
      if (showImportParamsDlg("DRW", &bNeedLowMemoryImport, &vsfDumpFile) == 0)
        return NULL;
    }
    if(bSetFeedBack)
      m_importParams.dwgImportParam.setFeedbackForChooseCallback(&OdTvViewerMainWindow::callChooseDialog);
    else
      m_importParams.dwgImportParam.setFeedbackForChooseCallback(NULL);

    return &m_importParams.dwgImportParam;
  }
  else if (strExt == OD_T("DGN"))
  {
    if (bCallImportDlg && !GETBIT(settings.value(SETTINGS_DGN_FLAG, 0).toUInt(), OdTvUIDgnImportParameters::kDontShow))
    {
      if (showImportParamsDlg("DGN") == 0)
        return NULL;
    }
    if (bSetFeedBack)
      m_importParams.dgnImportParam.setFeedbackForChooseCallback(&OdTvViewerMainWindow::callChooseDialog);
    else
      m_importParams.dgnImportParam.setFeedbackForChooseCallback(NULL);

    return &m_importParams.dgnImportParam;
  }
  else if (strExt == OD_T("STL"))
  {
    if (bCallImportDlg && !GETBIT(settings.value(SETTINGS_STL_FLAG, (OdTvUIStlImportParameters::kFixNormals | OdTvUIStlImportParameters::kUnifyDuplicatedVertices)).toUInt()
      , OdTvUIStlImportParameters::kDontShow))
    {
      if (showImportParamsDlg("STL") == 0)
        return NULL;
    }
    return &m_importParams.stlImportParam;
  }
  else if (strExt == OD_T("OBJ"))
  {
    if (bCallImportDlg && !GETBIT(settings.value(SETTINGS_OBJ_FLAG, 0).toUInt(), OdTvUIStlImportParameters::kDontShow))
    {
      if (showImportParamsDlg("OBJ") == 0)
        return NULL;
    }
    return &m_importParams.objImportParam;
  }
  else if (strExt == OD_T("PRC") || strExt == OD_T("U3D"))
  {
    if (bCallImportDlg && !GETBIT(settings.value(SETTINGS_PRC_FLAG, 0).toUInt(), OdTvUIPrcImportParameters::kDontShow))
    {
      if (showImportParamsDlg("PRC") == 0)
        return NULL;
    }
    return &m_importParams.prcImportParam;
  }
  else if (strExt == OD_T("RFA") || strExt == OD_T("RVT"))
  {
    if (bCallImportDlg && !GETBIT(settings.value(SETTINGS_BIM_FLAG, 0).toUInt(), OdTvUIBimImportParameters::kDontShow))
    {
      if (showImportParamsDlg("BIM") == 0)
        return NULL;
    }
    if (bSetFeedBack)
      m_importParams.bimImportParam.setFeedbackForChooseCallback(&OdTvViewerMainWindow::callChooseDialog);
    else
      m_importParams.bimImportParam.setFeedbackForChooseCallback(NULL);

    return &m_importParams.bimImportParam;
  }
  else if (strExt == OD_T("IFC"))
  {
    if (!settings.value(SETTINGS_IFC_DONTSHOW, false).toBool())
    {
      if (showImportParamsDlg("IFC") == 0)
        return NULL;
    }
    m_importParams.ifcImportParam.setFeedbackForChooseCallback(&OdTvViewerMainWindow::callChooseDialog);
    return &m_importParams.ifcImportParam;
  }
#ifdef ACIS2VISUALIZE_ENABLED
  else if (strExt == OD_T("SAT") || strExt == OD_T("SAB"))
  {
    if (bCallImportDlg && !GETBIT(settings.value(SETTINGS_ACIS_FLAG, 0).toUInt(), OdTvUIAcisImportParameters::kDontShow))
    {
      if (showImportParamsDlg("ACIS") == 0)
        return NULL;
    }
    return &m_importParams.acisImportParam;
  }
#endif
#ifdef REPLAY2VISUALIZE_ENABLED
  else if (strExt == OD_T("REPLAY"))
  {
    /*if (bCallImportDlg && !settings.value(SETTINGS_REPLAY_DONTSHOW, false).toBool())
    {
      if (showImportParamsDlg("Replay") == 0)
        return NULL;
    }*/
    return &m_importParams.replayImportParam;
  }
#endif
#ifdef NW2VISUALIZE_ENABLED
  else if (strExt == OD_T("NWD") || strExt == OD_T("NWC") || strExt == OD_T("NWF"))
  {
    if (bCallImportDlg && !settings.value(SETTINGS_NW_DONTSHOW, false).toBool())
    {
      if (showImportParamsDlg("NW") == 0)
        return NULL;
    }
    return &m_importParams.nwImportParam;
  }
#endif

  return 0;
}

// get the appropriate append parameters by extension
OdTvBaseImportParams* OdTvViewerMainWindow::getAppendParameters(const OdString& strExt)
{
  QSettings settings(ORGANIZATION_NAME, APPLICATION_NAME);

  bool bDatabaseExisits = (m_pActiveView && !m_pActiveView->getTvDatabaseId().isNull());
  if (!bDatabaseExisits &&
    (strExt == OD_T("DWG") || strExt == OD_T("DXF") || strExt == OD_T("DGN") || strExt == OD_T("PRC") || strExt == OD_T("U3D")
      || strExt == OD_T("RFA") || strExt == OD_T("RVT")
      || strExt == OD_T("NWD") || strExt == OD_T("NWC") || strExt == OD_T("NWF")))
  {
    //as a DC rect we will be use the whole MDI area
    QSize baseSize = m_pMdiArea->size();
    OdTvDCRect dcRect(0, baseSize.width(), baseSize.height(), 0);

    m_appendParams.dwgAppendParam.setDCRect(dcRect);
    m_appendParams.dgnAppendParam.setDCRect(dcRect);
    m_appendParams.prcAppendParam.setDCRect(dcRect);
    m_appendParams.bimAppendParam.setDCRect(dcRect);
    m_appendParams.nwAppendParam.setDCRect(dcRect);
  }

  if (strExt == OD_T("RCS"))
  {
    if (!GETBIT(settings.value(SETTINGS_RCS_FLAG, 0).toUInt(), OdTvUIRcsImportParameters::kDontShow))
    {
      if (showAppendParamsDlg("RCS", !bDatabaseExisits) == 0)
        return NULL;
    }
    return &m_appendParams.rcsAppendParam;
  }
  else if (strExt == OD_T("DWG") || strExt == OD_T("DXF"))
  {
    if (!GETBIT(settings.value(SETTINGS_DWG_FLAG, 0).toUInt(), OdTvUIDwgImportParameters::kDontShow))
    {
      if (showAppendParamsDlg("DRW", !bDatabaseExisits) == 0)
        return NULL;
    }
    return &m_appendParams.dwgAppendParam;
  }
  else if (strExt == OD_T("DGN"))
  {
    if (!GETBIT(settings.value(SETTINGS_DGN_FLAG, 0).toUInt(), OdTvUIDgnImportParameters::kDontShow))
    {
      if (showAppendParamsDlg("DGN", !bDatabaseExisits) == 0)
        return NULL;
    }
    return &m_appendParams.dgnAppendParam;
  }
  else if (strExt == OD_T("STL"))
  {
    if (!GETBIT(settings.value(SETTINGS_STL_FLAG, (OdTvUIStlImportParameters::kFixNormals | OdTvUIStlImportParameters::kUnifyDuplicatedVertices)).toUInt()
      , OdTvUIStlImportParameters::kDontShow))
    {
      if (showAppendParamsDlg("STL", !bDatabaseExisits) == 0)
        return NULL;
    }
    return &m_appendParams.stlAppendParam;
  }
  else if (strExt == OD_T("OBJ"))
  {
    if (!GETBIT(settings.value(SETTINGS_OBJ_FLAG, 0).toUInt(), OdTvUIStlImportParameters::kDontShow))
    {
      if (showAppendParamsDlg("OBJ", !bDatabaseExisits) == 0)
        return NULL;
    }
    return &m_appendParams.objAppendParam;
  }
  else if (strExt == OD_T("PRC") || strExt == OD_T("U3D"))
  {
    if (!GETBIT(settings.value(SETTINGS_PRC_FLAG, 0).toUInt(), OdTvUIPrcImportParameters::kDontShow))
    {
      if (showAppendParamsDlg("PRC", !bDatabaseExisits) == 0)
        return NULL;
    }
    return &m_appendParams.prcAppendParam;
  }
  else if (strExt == OD_T("RFA") || strExt == OD_T("RVT"))
  {
    if (!GETBIT(settings.value(SETTINGS_BIM_FLAG, 0).toUInt(), OdTvUIBimImportParameters::kDontShow))
    {
      if (showAppendParamsDlg("BIM", !bDatabaseExisits) == 0)
        return NULL;
    }
    return &m_appendParams.bimAppendParam;
  }
  else if (strExt == OD_T("IFC"))
  {
    if (!settings.value(SETTINGS_IFC_DONTSHOW, false).toBool())
    {
      if (showAppendParamsDlg("IFC", !bDatabaseExisits) == 0)
        return NULL;
    }
    return &m_appendParams.ifcAppendParam;
  }

  return 0;
}

// hide expand menu
void OdTvViewerMainWindow::hideSliderMenuSl()
{
  if (m_pSliderMenu->isShowed())
    m_pSliderMenu->hide();
}

// open visual styles dialog
void OdTvViewerMainWindow::visualStyles()
{
  if (m_pActiveView)
    m_pActiveView->runVisualStylesDialog();
}

// enable item panel buttons after end animation
void OdTvViewerMainWindow::enableItemPanelBtnSl()
{
  if(m_pActiveView)
    m_pMenuToolbar->setButtonsEnable();
}

void OdTvViewerMainWindow::tabChangedSl(OdTvUIBaseView *view)
{
  if (!m_pActiveView)
    return;
  QSize prevSz = m_pActiveView->size();
  view->resize(prevSz);
  m_pActiveView = view;
}

void OdTvViewerMainWindow::fileTabChangedSl(OdTvUIBaseView* view)
{
  m_pActiveView = view;
  updateBrowserTree();
  if (m_pFileTabs->count() < 1)
  {
    m_pMenuToolbar->getNavBar()->setActiveItem(kPanels);
    m_pItemPanel->getContainer()->slideInIdx(kPanels);
    m_pMenuToolbar->getNavBar()->reset();
  }
  updateButtonsState();
  OdTvUIDynamicComboBox::reset();
}

// PRIVATE METHODS

// file methods
void OdTvViewerMainWindow::newFile()
{
  m_pMenuToolbar->setButtonsDisable();
  generateDatabase(m_Tvfactory, kEmpty);
}

void OdTvViewerMainWindow::open()
{
  open(m_generalOptions.getPartialOpenEnabled() ? false : m_generalOptions.getMTOpenEnabled(), m_generalOptions.getPartialOpenEnabled(), m_generalOptions.getEnableMemoryLimit(), m_generalOptions.getMemoryLimit());
}

void OdTvViewerMainWindow::append()
{
  QString DEFAULT_DIR_KEY("default_dir");
  QString DEFAULT_FILTER_KEY("default_filter");

  QSettings settings(ORGANIZATION_NAME, APPLICATION_NAME);
  QString initial_Dir = settings.value(DEFAULT_DIR_KEY, QDir::currentPath()).toString();
  QString selfilter = settings.value(DEFAULT_FILTER_KEY, tr("Open Design Visualize Stream (*.vsf)")).toString();

  QString fileName = QFileDialog::getOpenFileName(this, "Open file", initial_Dir, getImportFormatsString(false), &selfilter);
  if (!fileName.isEmpty())
  {
    QDir curDir = QFileInfo(fileName).dir();
    QString dirPathForSave = curDir.absolutePath();
    settings.setValue(DEFAULT_DIR_KEY, dirPathForSave);
    settings.setValue(DEFAULT_FILTER_KEY, selfilter);
    
    if (appendFile(fileName))
    {
      statusBar()->showMessage(tr("File appended"), 2000);
      updateBrowserTree();
      updateButtonsState();
    }
  }
}

void OdTvViewerMainWindow::save()
{
  if (!m_pActiveView)
    return;
  if (m_pActiveView && m_pActiveView->save())
    statusBar()->showMessage(tr("File saved"), 2000);
}

void OdTvViewerMainWindow::saveAs()
{
  if (!m_pActiveView)
    return;
  if (m_pActiveView && m_pActiveView->saveAs())
    statusBar()->showMessage(tr("File saved"), 2000);
}

void OdTvViewerMainWindow::regenAll()
{
  if (!m_pActiveView)
    return;
  m_pActiveView->runRegenAction(OdTvUIBaseView::kAll);
}

void OdTvViewerMainWindow::exportToOBJ()
{
  if (m_pActiveView)
  {
    int ind = m_pFileTabs->property("activeTab").toInt();
    m_pActiveView->exportToOBJ(m_pFileTabs->tabText(ind));
  }
}

void OdTvViewerMainWindow::exportToDWG()
{
  if (m_pActiveView)
  {
    int ind = m_pFileTabs->property("activeTab").toInt();
    m_pActiveView->exportToDWG(m_pFileTabs->tabText(ind));
  }
}

void OdTvViewerMainWindow::exportToXML()
{
  if (m_pActiveView)
  {
    int ind = m_pFileTabs->property("activeTab").toInt();
    m_pActiveView->exportToXML(m_pFileTabs->tabText(ind));
  }
}

void OdTvViewerMainWindow::onCloseClick()
{
  close();
}

void OdTvViewerMainWindow::exportToPDF()
{
  if (m_pActiveView)
  {
    int ind = m_pFileTabs->property("activeTab").toInt();
    m_pActiveView->exportToPDF(m_pFileTabs->tabText(ind));
  }
}

//open file
void OdTvViewerMainWindow::open(bool bIsMT, bool bIsPartial, bool bUseMemLim, OdUInt32 nMemLim )
{
  QString DEFAULT_DIR_KEY("default_dir");
  QString DEFAULT_FILTER_KEY("default_filter");

  QSettings settings(ORGANIZATION_NAME, APPLICATION_NAME);
  QString initial_Dir = settings.value(DEFAULT_DIR_KEY, QDir::currentPath()).toString();
  QString selfilter = settings.value(DEFAULT_FILTER_KEY, tr("Open Design Visualize Stream (*.vsf)")).toString();

  QString fileName = QFileDialog::getOpenFileName(this, "Open file", initial_Dir, getImportFormatsString(false), &selfilter);
  if (!fileName.isEmpty())
  {
    QDir curDir = QFileInfo(fileName).dir();
    QString dirPathForSave = curDir.absolutePath();
    settings.setValue(DEFAULT_DIR_KEY, dirPathForSave);
    settings.setValue(DEFAULT_FILTER_KEY, selfilter);

    if (openFile(fileName, bIsMT, bIsPartial, bUseMemLim, nMemLim))
    {
      statusBar()->showMessage(tr("File loaded"), 2000);
    }
  }
}

//generate database
void OdTvViewerMainWindow::generateDatabase(OdTvFactoryId& id, SampleModels sample)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
  OdTvDatabaseId dbId;
  OdTvUIDatabaseInfo databaseInfo;

  //try to load visualize filer module
  {
    OdTvVisualizeFilerModulePtr pVisualizeFilerModule = ::odrxDynamicLinker()->loadApp(OdTvModelsGeneratorModuleName);
    if (pVisualizeFilerModule.isNull())
      return;

    //get the path to the exe folder 
    OdString strPath = ::toOdString(QCoreApplication::applicationDirPath().toUtf8().constData());

    {
      // Generate database
      OdTvVisualizeFilerPtr pFiler = pVisualizeFilerModule->getVisualizeFiler();
      if (pFiler.isNull())
        return;

      // get properties
      OdRxDictionaryPtr pProperties = pFiler->properties();
      if (pProperties.get())
      {
        OdUInt16 sampleID = 0;
        switch (sample)
        {
        case kSoccer:
          sampleID = 1;
          break;
        case kAllEntities:
          sampleID = 2;
          strPath += OD_T("\\images\\");
          break;
        case kPlot2D:
          sampleID = 3;
          break;
        case kPlot3D:
          sampleID = 4;
          break;
        case kPlotCAE:
          sampleID = 5;
          break;
        case kMaterials:
          sampleID = 6;
          break;
        case kMaterialSphereMap:
          sampleID = 7;
          break;
        case kMaterialCylindricalMap:
          sampleID = 8;
          break;
        case kMaterialLightTeapot:
          sampleID = 9;
          break;
        case kMaterialOpacityMap:
          sampleID = 10;
          break;
        case kMaterialBumpMap:
          sampleID = 11;
          break;
        case kCircleEllipse:
          sampleID = 1001;
          break;
        case kLinetypes:
          sampleID = 1002;
          break;
        case kLayers:
          sampleID = 1003;
          break;
        case kTransparency:
          sampleID = 1004;
          break;
        case kLight:
          sampleID = 1005;
          break;
        case kDebugMaterials:
          sampleID = 1006;
          break;
        case kShellFacesAttributes:
          sampleID = 1007;
          break;
        case kShellEdgesAttributes:
          sampleID = 1008;
          break;
        case kCylinderAttributes:
          sampleID = 1009;
          break;
        case kSphereAttributes:
          sampleID = 1010;
          break;
        case kBoxAttributes:
          sampleID = 1011;
          break;
        case kMeshFacesAttributes:
          sampleID = 1012;
          break;
        case kMeshEdgesAttributes:
          sampleID = 1013;
          break;
        case kResetColor:
          sampleID = 1014;
          break;
        case kResetLineWeight:
          sampleID = 1015;
          break;
        case kResetLinetype:
          sampleID = 1016;
          break;
        case kResetLinetypeScale:
          sampleID = 1017;
          break;
        case kResetLayer:
          sampleID = 1018;
          break;
        case kResetVisibility:
          sampleID = 1019;
          break;
        }
        if (pProperties->has(OD_T("SampleID")))
          pProperties->putAt(OD_T("SampleID"), OdRxVariantValue(sampleID));

        if (pProperties->has(OD_T("ResourceFolder")))
          pProperties->putAt(OD_T("ResourceFolder"), OdRxVariantValue(strPath));
      }

      QElapsedTimer *timer = new QElapsedTimer;
      timer->start();

      // generate sample model
      dbId = pFiler->generate(&databaseInfo);

      databaseInfo.setTvTime(OdInt64(timer->elapsed()));
      if (sample > kEmpty)
        databaseInfo.setType(OdTvUIDatabaseInfo::kBuiltIn);
    }
  }

  ::odrxDynamicLinker()->unloadModule(OdTvModelsGeneratorModuleName);

  //get active model (now it is always the first model)
  if (dbId.isValid())
  {
    OdTvDatabasePtr pDb = dbId.openObject();

    if (!pDb.isNull())
    {
      OdTvModelsIteratorPtr pModelsIterator = pDb->getModelsIterator();
      OdTvModelId tvModelId = pModelsIterator->getModel();

      OdTvDevicesIteratorPtr pDevicesIterator = pDb->getDevicesIterator();
      OdTvGsDeviceId tvDeviceId = pDevicesIterator->getDevice();

      // create file tabs
      int fileInd = m_pFileTabs->addFileTab(toQString(pModelsIterator->getModel().openObject()->getName()));
      OdTvUIDeviceTabBar *devTabBar = m_pFileTabs->getDeviceTabBar(fileInd);
      connect(devTabBar, SIGNAL(deviceTabChangeSg(OdTvUIBaseView*)), this, SLOT(tabChangedSl(OdTvUIBaseView*)));

      // create subwindow
      OdTvUIBaseView *view = createBaseView(databaseInfo, tvModelId, tvDeviceId);
      devTabBar->blockSignals(true);
      devTabBar->addTab(view, "");
      devTabBar->blockSignals(false);
      view->setWindowFlags(Qt::FramelessWindowHint);
      view->showMaximized();
      // set device name to tab
      devTabBar->tabBar()->setTabText(0, toQString(pDevicesIterator->getDevice().openObject()->getName()));

      m_pFileTabs->setCurrentIndex(fileInd);
      devTabBar->tabSelected(0);

      m_pActiveView = devTabBar->getActiveView();
      updateButtonsState();
      updateBrowserTree();
    }
  }

  QApplication::restoreOverrideCursor();
}

//load database from file
OdTvModelId OdTvViewerMainWindow::loadDatabase(OdTvFactoryId& id, OdTvBaseImportParams* pImportParam, LoadMode loadMode, bool bIsMT, bool bIsPartial,
                                               bool bNeedLowMemoryImport, const OdString& vsfDumpFile)
{
  OdTvModelId modelId;
  OdTvDatabaseId databaseId;

  if (loadMode == kRead)
  {
    //read database
    OdTvResult rc = tvOk;

    try
    {
      QElapsedTimer timer;
      timer.start();

      // read the file
      databaseId = id.readFile(pImportParam->getFilePath(), bIsMT, bIsPartial, &rc);

      // fill profiling info
      if (pImportParam->getProfiling())
        pImportParam->getProfiling()->setImportTime(OdInt64(timer.elapsed()));
    }
    catch (OdTvError& e)
    {
      OdString descr = e.description();
      QMessageBox::warning(this, getAppName(), QObject::tr("Reading of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(toQString(descr)));

      return modelId;
    }
    catch (const OdError& e)
    {
      OdString descr = e.description();
      QMessageBox::warning(this, getAppName(), QObject::tr("Reading of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(toQString(descr)));

      return modelId;
    }
    catch (...)
    {
      QMessageBox::warning(this, getAppName(), QObject::tr("Reading of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(QString("Unknown exception is caught...")));

      return modelId;
    }
  }
  else
    if (loadMode == kImport)
    {
      //import to database
      OdTvResult rc = tvOk;

      try
      {
        //import database
        if ( !bNeedLowMemoryImport )
          databaseId = id.importFile(pImportParam, &rc);
        else
        {
          //create and set progress meter
          OdTvUIProgressMeter* pProgressMeter = new OdTvUIProgressMeter("Low memory import", this);
          pImportParam->setProgressMeter(pProgressMeter);

          //perform main call
          rc = id.lowMemoryImportFile(pImportParam, vsfDumpFile);

          //reset and remove progress meter
          pImportParam->setProgressMeter(NULL);
          delete pProgressMeter;

          if ( rc == tvOk )
            databaseId = id.readFile(vsfDumpFile, false, true, &rc);
        }

        if (rc != tvOk)
        {
          QMessageBox msg;
          msg.setWindowTitle("Error");
          msg.setIcon(QMessageBox::Warning);

          if (rc == tvMissingFilerModule)
            msg.setText("Missing filer module.");
          else if (rc == tvFilerEmptyInternalDatabase)
          {
            if (bNeedLowMemoryImport)
              msg.setText(".vsf file was not created.");
            else
              return modelId;
          }
          else
            msg.setText("Error during open file.");

          msg.exec();
          return modelId;
        }

      }
      catch (OdTvError& e)
      {
        OdString descr = e.description();
        QMessageBox::warning(this, getAppName(), QObject::tr("Importing of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(toQString(descr)));

        return modelId;
      }
      catch (const OdError& e)
      {
        OdString descr = e.description();
        QMessageBox::warning(this, getAppName(), QObject::tr("Importing of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(toQString(descr)));

        return modelId;
      }
      catch (...)
      {
        QMessageBox::warning(this, getAppName(), QObject::tr("Importing of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(QString("Unknown exception is caught...")));

        return modelId;
      }
    }
    else
      if (loadMode == kCustom)
      {
        //import to database
        OdTvResult rc = tvOk;
        try
        {
          //import custom database
          databaseId = importCustomFormat(pImportParam, &rc);

          if (rc != tvOk)
          {
            QMessageBox msg;
            msg.setWindowTitle("Error");
            if (rc == tvMissingFilerModule)
              msg.setText("Missing filer module.");
            else
              msg.setText("Error during open file.");
            msg.setIcon(QMessageBox::Warning);
            msg.exec();
          }

        }
        catch (OdTvError& e)
        {
          OdString descr = e.description();
          QMessageBox::warning(this, getAppName(), QObject::tr("Importing of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(toQString(descr)));

          return modelId;
        }
        catch (const OdError& e)
        {
          OdString descr = e.description();
          QMessageBox::warning(this, getAppName(), QObject::tr("Importing of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(toQString(descr)));

          return modelId;
        }
        catch (...)
        {
          QMessageBox::warning(this, getAppName(), QObject::tr("Importing of file \"%1\" was failed (%2).").arg(toQString(pImportParam->getFilePath())).arg(QString("Unknown exception is caught...")));

          return modelId;
        }
      }

  //get active model (now it is always the first model)
  if (databaseId.isValid())
  {
    OdTvDatabasePtr pDb = databaseId.openObject();

#ifdef SUPPORTNATIVEPALETTE
    emit enableVisualizePropertiesTabSg();

    if (m_pDockPanel->getObjectNativeProperties()->isActive())
      m_pDockPanel->getObjectNativeProperties()->onDatabaseChanged(databaseId, pImportParam->getFilePath());
#endif

    if (!pDb.isNull())
    {
      OdTvModelsIteratorPtr pModelsIterator = pDb->getModelsIterator();
      modelId = pModelsIterator->getModel();
    }
  }

  return modelId;
}

// run surf plot sample
void OdTvViewerMainWindow::runSurfPlotSample()
{
  generateDatabase(m_Tvfactory, kPlot3D);

  QElapsedTimer *timer = new QElapsedTimer;
  timer->start();

  OdTvUIBaseView *pWindow = m_pActiveView;
  //Add coordinate system
  if (pWindow)
  {
    OdTvPoint pointXYZ = OdTvPoint(-0.75, -0.75, -0.5), pointXY = OdTvPoint(0.75, 0.75, pointXYZ.z), pointXZ = OdTvPoint(pointXYZ.x, pointXYZ.y, 0.5);

    OdUInt32 xNum = 10, yNum = 5, zNum = 9;
    double startXMark = 1., startYMark = 0., startZMark = -2.;
    double endXMark = 10., endYMark = 20., endZMark = 2.;
    double startX = 2., endX = 10., startY = 1., endY = 20.;
    OdUInt32 nDiv = 70;

    OdTvDatabaseId baseId = pWindow->getTvDatabaseId();
    OdTvModelId modelId = pWindow->getTvMainModel();
    OdTv3DCoordinateSystem* coordinateSystem = new OdTv3DCoordinateSystem(-1, baseId, modelId, pointXYZ, pointXY, pointXZ);
    coordinateSystem->setStartXMark(startXMark);
    coordinateSystem->setEndXMark(endXMark);
    coordinateSystem->setStartYMark(startYMark);
    coordinateSystem->setEndYMark(endYMark);
    coordinateSystem->setStartZMark(startZMark);
    coordinateSystem->setEndZMark(endZMark);
    coordinateSystem->setXNum(xNum);
    coordinateSystem->setYNum(yNum);
    coordinateSystem->setZNum(zNum);

    pWindow->addListenerToRotationListenersList(coordinateSystem);

    OdTvUIDatabaseInfo databaseInfo = pWindow->getDatabaseInfo();
    databaseInfo.setTvTime(databaseInfo.getTvTime() + OdInt64(timer->elapsed()));
    pWindow->setDatabaseInfo(databaseInfo);
  }
}

// window base methods
void OdTvViewerMainWindow::createStatusBar()
{
  statusBar()->showMessage(tr("Ready"));
}

void OdTvViewerMainWindow::saveWidgetPlacement(QWidget* pWidget, const QString cqsKey) const // = "" // for QMainWindow
{
  if (!pWidget)
    return;

  QSettings settings(ORGANIZATION_NAME, APPLICATION_NAME);

  QString qsKey(cqsKey);
  if (!qsKey.isEmpty())
    qsKey += "/";
  else
    ODA_ASSERT_ONCE(pWidget == this);

  QRect rect = pWidget->geometry();
  settings.setValue(qsKey + "w", rect.width());
  settings.setValue(qsKey + "h", rect.height());
  settings.setValue(qsKey + "x", rect.x());
  settings.setValue(qsKey + "y", rect.y());
}

void OdTvViewerMainWindow::restoreWidgetPlacement(QWidget* pWidget, const QString cqsKey,  // = "" // for QMainWindow
                                           int widthDefault, // = 600
                                           int heightDefault) const // = 200
{
  if (!pWidget)
    return;

  QDesktopWidget* pDesktop = NULL;
  QSize sizeParentWnd = size();
  QPoint posParentWnd = pos();

  QString qsKey(cqsKey);
  if (!cqsKey.isEmpty())
    qsKey += "/";
  else
  {
    ODA_ASSERT_ONCE(pWidget == this);

    pDesktop = QApplication::desktop();
    sizeParentWnd = pDesktop->screenGeometry().size(); // primary screen
    posParentWnd = QPoint();
  }

  QSettings settings(ORGANIZATION_NAME, APPLICATION_NAME);

  QSize size(settings.value(qsKey + "w", widthDefault).toInt(), 
             settings.value(qsKey + "h", heightDefault).toInt());

  QPoint pos((sizeParentWnd.width() - size.width()) / 2,
             (sizeParentWnd.height() - size.height()) / 2);
  pos += posParentWnd;

  pos.setX(settings.value(qsKey + "x", pos.x()).toInt());
  pos.setY(settings.value(qsKey + "y", pos.y()).toInt());

  if (cqsKey.isEmpty())
  {
    ODA_ASSERT_ONCE(pDesktop);
    QSize sizeParentVirtualWnd = pDesktop->geometry().size(); // full virtual size of screens

    if (   size.width() < 400 || size.height() < 300 || pos.x() < 0 || pos.y() < 0
        || (pos.x() + size.width()) > sizeParentVirtualWnd.width() 
        || (pos.y() + size.height()) > sizeParentVirtualWnd.height())
    {
      // restore default application placement
      ODA_ASSERT_ONCE(pDesktop);
      sizeParentWnd = pDesktop->screenGeometry().size(); // primary screen
      size.setWidth(widthDefault);
      size.setHeight(heightDefault);
      pos.setX((sizeParentWnd.width() - widthDefault) / 2);
      pos.setY((sizeParentWnd.height() - heightDefault) / 2);
    }
  }

  pWidget->setGeometry(pos.x(), pos.y(), size.width(), size.height());
}

//create tool button with menu pop-up
QToolButton* OdTvViewerMainWindow::createToolButtonWithPopup(QList<QAction*>::const_iterator& posList, int nActionsToAdd)
{
  QToolButton* pToolButton = NULL;

  if (nActionsToAdd < 2)
    return NULL;

  pToolButton = new QToolButton;
  pToolButton->setDefaultAction(*posList);
  pToolButton->setPopupMode(QToolButton::MenuButtonPopup);
  posList++;
  nActionsToAdd--;

  while (nActionsToAdd > 0)
  {
    pToolButton->addAction(*posList);
    posList++;
    nActionsToAdd--;
  }

  connect(pToolButton, SIGNAL(triggered(QAction*)), pToolButton, SLOT(setDefaultAction(QAction*)));

  return pToolButton;
}

// show dialogs with import parameters for different files
int OdTvViewerMainWindow::showImportParamsDlg(const QString& str, bool* bNeedLowMemoryImport, OdString* vsfDumpFile)
{
  OdTvUIImportParameters *dlg = NULL;
  if (!str.compare("STL"))
    dlg = new OdTvUIStlImportParameters(m_importParams.stlImportParam, false, false, this);
  else if ((!str.compare("RCS")) || (!str.compare("RCP")))
    dlg = new OdTvUIRcsImportParameters(m_importParams.rcsImportParam, false, this);
  else if (!str.compare("OBJ"))
    dlg = new OdTvUIObjImportParameters(m_importParams.objImportParam, false, false, this);
  else if (!str.compare("DRW"))
    dlg = new OdTvUIDwgImportParameters(m_importParams.dwgImportParam, false, false, this);
  else if (!str.compare("DGN"))
    dlg = new OdTvUIDgnImportParameters(m_importParams.dgnImportParam, false, false, this);
  else if (!str.compare("PRC") || !str.compare("U3D"))
    dlg = new OdTvUIPrcImportParameters(m_importParams.prcImportParam, false, this);
  else if (!str.compare("BIM"))
    dlg = new OdTvUIBimImportParameters(m_importParams.bimImportParam, false, false, this);
  else if (!str.compare("NW"))
    dlg = new OdTvUINwImportParameters(m_importParams.nwImportParam, false, this);
  else if (!str.compare("IFC"))
    dlg = new OdTvUIIfcImportParameters(m_importParams.ifcImportParam, false, false, this);
#ifdef ACIS2VISUALIZE_ENABLED
  else if (!str.compare("ACIS"))
    dlg = new OdTvUIAcisImportParameters(m_importParams.acisImportParam, this);
#endif
#ifdef REPLAY2VISUALIZE_ENABLED
  else if (!str.compare("Replay"))
    dlg = new OdTvUIReplayImportParameters(m_importParams.replayImportParam, this);
#endif

  int iRes = dlg->exec();
  if (bNeedLowMemoryImport && vsfDumpFile && !str.compare("DRW"))
  {
    OdTvUIDwgImportParameters* pDlg = dynamic_cast<OdTvUIDwgImportParameters*>(dlg);
    if (pDlg)
    {
      *bNeedLowMemoryImport = pDlg->getLowMemoryImport();
      *vsfDumpFile = pDlg->getPartialImportFileName();
    }
  }

  return iRes;
}

// show dialogs with append parameters for different files
int OdTvViewerMainWindow::showAppendParamsDlg(const QString& str, bool bFirstDevice /*= true*/)
{
  OdTvUIImportParameters *dlg = NULL;
  if (!str.compare("STL"))
    dlg = new OdTvUIStlAppendParameters(m_appendParams.stlAppendParam, bFirstDevice, this);
  else if (!str.compare("RCS"))
    dlg = new OdTvUIRcsAppendParameters(m_appendParams.rcsAppendParam, bFirstDevice, this);
  else if (!str.compare("OBJ"))
    dlg = new OdTvUIObjAppendParameters(m_appendParams.objAppendParam, bFirstDevice, this);
  else if (!str.compare("DRW"))
    dlg = new OdTvUIDwgAppendParameters(m_appendParams.dwgAppendParam, false, bFirstDevice, this);
  else if (!str.compare("DGN"))
    dlg = new OdTvUIDgnAppendParameters(m_appendParams.dgnAppendParam, false, bFirstDevice, this);
  else if (!str.compare("PRC") || !str.compare("U3D"))
    dlg = new OdTvUIPrcAppendParameters(m_appendParams.prcAppendParam, bFirstDevice, this);
  else if (!str.compare("BIM"))
    dlg = new OdTvUIBimAppendParameters(m_appendParams.bimAppendParam, false, bFirstDevice, this);
  else if (!str.compare("IFC"))
    dlg = new OdTvUIIfcAppendParameters(m_appendParams.ifcAppendParam, false, bFirstDevice, this);

  return dlg->exec();
}

// connect signals and slots
void OdTvViewerMainWindow::connectSignals()
{
  // menu items connect
  connect(m_pMenuToolbar->getElementByName("Menu"), SIGNAL(clicked()),
    this, SLOT(onMenuClicked()));
  connect(m_pMenuToolbar->getNavBar(), SIGNAL(onActive(int)),
    this, SLOT(onItemPanelChoose(int)));
  connect(m_pSliderMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
  connect(m_pRecentMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(recentFileClick(QString)));
  connect(m_pExportMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
  connect(m_pExampleMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
  connect(m_pPlotsMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
  connect(m_pMaterialsMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
#ifdef _DEBUG
  connect(m_pDebugExamplesMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
  connect(m_pDbgShellAttributesMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
  connect(m_pDbgMeshAttributesMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
  connect(m_pDbgResetTraitsMenu->getSignalMapper(), SIGNAL(mapped(QString)), SLOT(menuButtonClick(QString)));
#endif

  connect(m_pSliderMenu->getElementByName("OpenRecent"), SIGNAL(clicked()),
    this, SLOT(onRecentExpand()));
  connect(m_pSliderMenu->getElementByName("Export"), SIGNAL(clicked()),
    this, SLOT(onExportExpand()));
  connect(m_pSliderMenu->getElementByName("Examples"), SIGNAL(clicked()),
    this, SLOT(onExamplesExpand()));
  connect(m_pExampleMenu->getElementByName("Plots"), SIGNAL(clicked()),
    this, SLOT(onPlotsExpand()));
  connect(m_pExampleMenu->getElementByName("Materials"), SIGNAL(clicked()),
    this, SLOT(onMaterialsExpand()));
#ifdef _DEBUG
  connect(m_pExampleMenu->getElementByName("Debug"), SIGNAL(clicked()),
    this, SLOT(onDebugExamplesExpand()));
  connect(m_pDebugExamplesMenu->getElementByName("ShellsAttributes"), SIGNAL(clicked()),
    this, SLOT(onDbgShellAttrExpand()));
  connect(m_pDebugExamplesMenu->getElementByName("MeshAttributes"), SIGNAL(clicked()),
    this, SLOT(onDbgMeshAttrExpand()));
  connect(m_pDebugExamplesMenu->getElementByName("ResetTraits"), SIGNAL(clicked()),
    this, SLOT(onDbgResetTraitsExpand()));
#endif

  connect(m_pItemPanel->getEventManager(), SIGNAL(onChangeAction(const QString&)), this, SLOT(onChangeActionSl(const QString&)));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onChangeView(const QString&)), this, SLOT(onChangeViewSl(const QString&)));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onSetProjection(const QString&)), this, SLOT(onChangeProjectionSl(const QString&)));
  connect(m_pItemPanel->getEventManager(), SIGNAL(setRenderMode(const QString&)), this, SLOT(onChangeRenderModeSl(const QString&)));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onDrawGeometry(const QString&)), this, SLOT(onDrawGeometrySl(const QString&)));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onSetRegen(const QString&)), this, SLOT(onSetRegen(const QString&)));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onSectioningChange(const QString&)), this, SLOT(onSectioningChangeSl(const QString&)));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onAppearanceChange(const QString&)), this, SLOT(onChangeAppearanceSl(const QString&)));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onChangeVisibleObjectPanel()), this, SLOT(onOffModelBrowser()));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onChangeVisiblePropertiesPanel()), this, SLOT(onOffProperties()));
  connect(m_pItemPanel->getEventManager(), SIGNAL(onVisualStyleDlgOpen()), this, SLOT(visualStyles()));
  connect(m_pItemPanel->getContainer(), SIGNAL(animationFinished()), this, SLOT(enableItemPanelBtnSl()));

  connect(m_pDockPanel->getExplorerItem(), SIGNAL(refreshBtnSg()), this, SLOT(updateBrowserTree()));
#ifdef SUPPORTNATIVEPALETTE
  QObject::connect(this, SIGNAL(enableVisualizePropertiesTabSg()), m_pDockPanel->getObjectProperties(), SLOT(onEnableVisualizePropertiesTab()));
#endif
}

// enable panel with buttons if file exist
void OdTvViewerMainWindow::setButtonsBarEnabled(bool bEnable)
{
  bEnable ? m_pMenuToolbar->setButtonsEnable() : m_pMenuToolbar->setButtonsDisable();
  resetDraggerButtonStateSl();
  m_pItemPanel->setVisibility(bEnable);
  m_pMenuToolbar->getNavBar()->setMarkerVisibility(bEnable);
  m_pFileTabs->setVisible(bEnable);

  if (bEnable)
    m_pCentralStackedWid->setCurrentWidget(m_pFileTabs);
  else
    m_pCentralStackedWid->setCurrentWidget(m_pBgImageLbl);

  statusBar()->setVisible(bEnable);
}

// update buttons state(view, projection, style, appearance buttons)
void OdTvViewerMainWindow::updateViewButtonState()
{
  if (!m_pActiveView)
    return;
  m_pItemPanel->getEventManager()->setBorderForView();
  switch (m_pActiveView->getActiveTvExtendedView()->getViewType())
  {
  case OdTvExtendedView::kTop:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Top"));
    break;
  case OdTvExtendedView::kBottom:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Bottom"));
    break;
  case OdTvExtendedView::kLeft:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Left"));
    break;
  case OdTvExtendedView::kRight:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Right"));
    break;
  case OdTvExtendedView::kFront:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Front"));
    break;
  case OdTvExtendedView::kBack:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Back"));
    break;
  case OdTvExtendedView::kSW:
    setWidgetUnderlined(m_pItemPanel->getElementByName("SW"));
    break;
  case OdTvExtendedView::kSE:
    setWidgetUnderlined(m_pItemPanel->getElementByName("SE"));
    break;
  case OdTvExtendedView::kNE:
    setWidgetUnderlined(m_pItemPanel->getElementByName("NE"));
    break;
  case OdTvExtendedView::kNW:
    setWidgetUnderlined(m_pItemPanel->getElementByName("NW"));
    break;
}
}

void OdTvViewerMainWindow::updateProjectionButtonState()
{
  if (!m_pActiveView)
    return;
  m_pItemPanel->getEventManager()->setBorderForProjection();
  if (m_pActiveView->getActiveTvViewPtr()->isPerspective())
    setWidgetUnderlined(m_pItemPanel->getElementByName("Perspective"));
  else
    setWidgetUnderlined(m_pItemPanel->getElementByName("Isometric"));
}

void OdTvViewerMainWindow::updateRenderModeButtonState()
{
  if (!m_pActiveView)
    return;
  m_pItemPanel->getEventManager()->setBorderForStyle();
  switch (m_pActiveView->getRenderMode())
  {
  case OdTvGsView::k2DOptimized:
    setWidgetUnderlined(m_pItemPanel->getElementByName("2D Wireframe"));
    break;
  case OdTvGsView::kWireframe:
    setWidgetUnderlined(m_pItemPanel->getElementByName("3D Wireframe"));
    break;
  case OdTvGsView::kHiddenLine:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Hidden line"));
    break;
  case OdTvGsView::kFlatShaded:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Shaded"));
    break;
  case OdTvGsView::kGouraudShaded:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Gouraud shaded"));
    break;
  case OdTvGsView::kFlatShadedWithWireframe:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Shaded with edges"));
    break;
  case OdTvGsView::kGouraudShadedWithWireframe:
    setWidgetUnderlined(m_pItemPanel->getElementByName("Gouraud shaded with edges"));
    break;
  }
}

void OdTvViewerMainWindow::updateAppearanceButtonState()
{
  if (!m_pActiveView)
    return;
  m_pItemPanel->getEventManager()->setBorderForAppearance();
  m_appearanceOptions = m_pActiveView->getAppearanceOptions();
  m_generalOptions = m_pActiveView->getGeneralOptions();

  if (m_generalOptions.getEnableWcs())
    setWidgetUnderlined(m_pItemPanel->getElementByName("WCS"));
  if (m_generalOptions.getEnableFPS())
    setWidgetUnderlined(m_pItemPanel->getElementByName("FPS"));
  if (m_generalOptions.getEnableGrid())
    setWidgetUnderlined(m_pItemPanel->getElementByName("Grid"));
  QString style = "border-bottom: 2px solid rgb(%1, %2, %3); border-top: 0px; border-right: 0px; border-left: 0px; ";
  OdUInt32 color = m_appearanceOptions.getBackground();
  m_pItemPanel->getElementByName("Background")->setStyleSheet(style.arg(ODGETRED(color)).arg(ODGETGREEN(color)).arg(ODGETBLUE(color)));
}

void OdTvViewerMainWindow::updatePanelsButtonState()
{
  m_pItemPanel->getEventManager()->setBorderForPanels();

  if(m_bBrowserIsEnabled)
    setWidgetUnderlined(m_pItemPanel->getElementByName("Object"));
  if(m_bPropertiesIsEnabled)
    setWidgetUnderlined(m_pItemPanel->getElementByName("Properties"));
}

void OdTvViewerMainWindow::updateSectioningButtonState()
{
  m_pItemPanel->getElementByName("Show cutting planes")->setStyleSheet("border: none;");

  if (m_pActiveView)
  {
    if (m_pActiveView->getSectioningOptions()->getShown())
    {
      setWidgetUnderlined(m_pItemPanel->getElementByName("Show cutting planes"));
      QToolButton* pElement = dynamic_cast<QToolButton*>(m_pItemPanel->getElementByName("Show cutting planes"));
      if (pElement)
      {
        QIcon icon(":/images/CuttingPlane.png");
        pElement->setIcon(icon);
      }
    }

    if (!m_pActiveView->getActiveTvViewId().isNull())
    {
      OdTvGsViewPtr pTvGsView = m_pActiveView->getActiveTvViewId().openObject();
      if (pTvGsView->numCuttingPlanes() != 0)
      {
        ODCOLORREF color = 0;
        bool bFillingEnabled = pTvGsView->getCuttingPlaneFillEnabled(color);

        OdTvGsView::CuttingPlaneFillStyle fillingPatternStyle = OdTvGsView::kCheckerboard;
        ODCOLORREF fillingPatternColor = 0;
        bool bFillingPatternEnabled = pTvGsView->getCuttingPlaneFillPatternEnabled(fillingPatternStyle, fillingPatternColor);

        m_pActiveView->getSectioningOptions()->setFilled(bFillingEnabled);
        m_pActiveView->getSectioningOptions()->setFillingColor(color);

        m_pActiveView->getSectioningOptions()->setFillingPatternEnabled(bFillingPatternEnabled);
        m_pActiveView->getSectioningOptions()->setFillingPatternStyle(fillingPatternStyle);
        m_pActiveView->getSectioningOptions()->setFillingPatternColor(fillingPatternColor);
      }
    }

    OdTvGsViewPtr pActiveView = m_pActiveView->getActiveTvViewId().openObject();
    if (!pActiveView.isNull())
    {
      // Enable/disable buttons for cutting planes adding
      bool bAddingButtnsEnabled = (pActiveView->numCuttingPlanes() < ODTVUI_CUTTINGPLANESMAXNUM);
      m_pItemPanel->getElementByName("Add cutting plane by X axis")->setEnabled(bAddingButtnsEnabled);
      m_pItemPanel->getElementByName("Add cutting plane by Y axis")->setEnabled(bAddingButtnsEnabled);
      m_pItemPanel->getElementByName("Add cutting plane by Z axis")->setEnabled(bAddingButtnsEnabled);

      // Enable/disable remove button
      m_pItemPanel->getElementByName("Remove cutting planes")->setEnabled(pActiveView->numCuttingPlanes() > 0);
    }
  }
}

// set bottom border for widget
void OdTvViewerMainWindow::setWidgetUnderlined(QWidget *widget)
{
  if(widget)
    widget->setStyleSheet("border-bottom: 2px solid rgb(100, 100, 155); border-top: 0px; border-right: 0px; border-left: 0px; ");
}

void OdTvViewerMainWindow::addFileToRecent(const QString& fileName)
{
  if (m_pSliderMenu->addToRecent(fileName))
  {
    m_pSliderMenu->initializeControls();
    m_pRecentMenu->setupFiles(m_pSliderMenu->getRecentList());
    // controls was removed and created, need reconnection
    connect(m_pSliderMenu->getElementByName("OpenRecent"), SIGNAL(clicked()), this, SLOT(onRecentExpand()));
    connect(m_pSliderMenu->getElementByName("Export"), SIGNAL(clicked()), this, SLOT(onExportExpand()));
    connect(m_pSliderMenu->getElementByName("Examples"), SIGNAL(clicked()), this, SLOT(onExamplesExpand()));
  }
}

