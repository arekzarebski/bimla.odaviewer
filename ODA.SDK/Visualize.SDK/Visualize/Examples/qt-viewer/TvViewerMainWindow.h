/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef OD_TV_VIEWERMAINWINDOW_H
#define OD_TV_VIEWERMAINWINDOW_H

#include <QMainWindow>
#include <QTextBrowser>
#include <QToolButton>

#include "OdTvUIVisualizeObjectExplorer.h"
#include "OdTvUICDAObjectTreeExplorer.h"
#include "OdTvUIVisualizeObjectProperties.h"
#include "OdTvUIImportParameters.h"
#include "OdTvUICommonParamsDlg.h"
#include "OdTvUISliderMenu.h"
#include "OdTvUIMenuToolbar.h"
#include "OdTvUIItemPanel.h"
#include "OdTvUIStyleManager.h"
#include "OdTvUIAppUtils.h"
#include "OdTvUITabPanels.h"
#include "OdTvUIPalettePanels.h"
#include "OdTvImportParametersContainer.h"

class OdTvUIBaseView;
class OdTvUIPerformanceParamsPage;

QT_BEGIN_NAMESPACE
  class QMdiArea;
  class QSignalMapper;
  class QUrl;
QT_END_NAMESPACE

typedef OdVector<OdTvGsDeviceId> OdTvDeviceArray;

class OdTvViewerLimitManager : public OdTvLimitManager
{
public:
  OdTvViewerLimitManager();
  virtual ~OdTvViewerLimitManager();
  virtual OdTvLimitManager::MemoryLimitControlResult checkMemoryUsage( OdUInt8 nReason, OdUInt64 nApproximateMemoryUsage = 0 ) const;
  void setMemoryLimit( OdUInt64 nLimit ) { m_nMemoryLimit = nLimit; }
  OdUInt64 memoryLimit() const { return m_nMemoryLimit; }

protected:
#ifdef ODA_WINDOWS
  HANDLE m_processId;
#endif // ODA_WINDOWS
  OdUInt64 m_nMemoryLimit;
  mutable OdMutex m_mutex;
};

class OdTvViewerMainWindow : public QMainWindow
{
  Q_OBJECT

  enum SampleModels
  {
    kEmpty = 0,
    kSoccer = 1,
    kAllEntities = 2,
    kPlot2D = 3,
    kPlot3D = 4,
    kPlotCAE = 5,
    kMaterials = 6,
    kMaterialSphereMap = 7,
    kMaterialCylindricalMap = 8,
    kMaterialLightTeapot = 9,
    kMaterialOpacityMap = 10,
    kMaterialBumpMap = 11,
    kCircleEllipse = 1001,
    kLinetypes = 1002,
    kLayers = 1003,
    kTransparency = 1004,
    kLight = 1005,
    kDebugMaterials = 1006,
    kShellFacesAttributes = 1007,
    kShellEdgesAttributes = 1008,
    kCylinderAttributes = 1009,
    kSphereAttributes = 1010,
    kBoxAttributes = 1011,
    kMeshFacesAttributes = 1012,
    kMeshEdgesAttributes = 1013,
    kResetColor = 1014,
    kResetLineWeight = 1015,
    kResetLinetype = 1016,
    kResetLinetypeScale = 1017,
    kResetLayer = 1018,
    kResetVisibility = 1019
  };

  enum LoadMode
  {
    kRead = 0,
    kImport = 1,
    kCustom = 2
  };

public:

  OdTvViewerMainWindow();
  // file methods
  bool openFile(const QString &fileName, bool bIsMT, bool bIsPartial, bool bIsMemLimit, OdUInt32 nMemLimit);
  bool appendFile(const QString &fileName);
  bool isClosed() const; // by closeEvent

  bool showUrl(const QString& qsUrl, const QString& qsTitle = "%1", /* by app name*/ bool bModal = true);
  bool isBrowserClosed() const;

  // get active view ptr
  OdTvGsViewPtr getActiveTvViewPtr();

  OdTvUIBaseObjectExplorer *    getObjectExplorer() const { return m_pDockPanel->getObjectExplorer(); }
  OdTvUIVisualizeObjectProperties *      getPropertiesPalette() const { return m_pDockPanel->getObjectProperties(); }
  OdTvUICDANodesProperties*     getCDANodesProperties() { return m_pDockPanel->getCDANodesProperties(); }
#ifdef SUPPORTNATIVEPALETTE
  OdTvUIObjectNativeProperties* getObjectNativeProperties() { return m_pDockPanel->getObjectNativeProperties(); }
#endif


protected:
  void closeEvent(QCloseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  virtual void resizeEvent(QResizeEvent *event);
  virtual void keyPressEvent(QKeyEvent *event);

  static void callChooseDialog(OdTvFilerFeedbackForChooseObject& object);

  // override drop event
  virtual void dragEnterEvent(QDragEnterEvent *event);
  virtual void dropEvent(QDropEvent *event);

  //called when the sectioning panels appears or disaapears
  void onAppearSectioningPanel(bool bAppear);

  // Show error message
  virtual void onOpeningError();

  // Count views in devices
  OdUInt64 countViewsInDevices(OdTvDevicesIteratorPtr& pDevicesIterator) const;

public slots:
  void updateStateRemoveCuttingPlanesButtonSl();
  void resetRotationBtnSl();

private slots :
  // on menu button click slot
  void menuButtonClick(const QString& btnName);
  // recent file click
  void recentFileClick(const QString& filePath);
  // update enabled\disabled buttons
  void updateButtonsState();
  // hide expanded menu
  void onMenuClicked();
  // slots for expand nested menus
  void onRecentExpand();
  void onExportExpand();
  void onExamplesExpand();
  void onPlotsExpand();
  void onMaterialsExpand();
  void onDebugExamplesExpand();
  void onDbgShellAttrExpand();
  void onDbgMeshAttrExpand();
  void onDbgResetTraitsExpand();
  // slide to clicked panel (navigation, style, projection, etc.)
  void onItemPanelChoose(int ind);
  // slots for clicked buttons
  void onChangeActionSl(const QString& str);
  void onChangeViewSl(const QString& str);
  void onChangeRenderModeSl(const QString& str);
  void onChangeProjectionSl(const QString& str);
  void onDrawGeometrySl(const QString& str);
  void onSetRegen(const QString& str);
  void onSectioningChangeSl(const QString& str);
  void onChangeAppearanceSl(const QString& str);
  // reset current dragger
  void resetDraggerButtonStateSl();
  // slot for update view buttons
  void updateViewStateSl();
  // create dialog with options
  void createGlobalOptionsDlg();
  // extras buttons slots
  void showCuttingPlanes();
  void addCuttingPlane(const OdTvVector& axis);
  void cuttingPlaneProperties();
  void removeCuttingPlanes();
  // appearance buttons slots
  void onOffWcs();
  void setBgColor();
  void onOffGrid();
  void onOffFps();

  // update object explorer
  void updateBrowserTree();

  // enable or disable model browser and properties
  void onOffModelBrowser();
  void onOffProperties();

  // create view
  OdTvUIBaseView *createBaseView(const OdTvUIDatabaseInfo& databaseInfo, OdTvModelId tvmodelId, OdTvGsDeviceId tvDeviceId = OdTvGsDeviceId(), bool bLoadFile = false);
  // slot for receive the messages about the view was destroyed
  void onCloseBaseView(OdTvUIBaseView *view);
  // set active sub window
  void setActiveSubWindow(QWidget *window);

  // browser actions command
  void changedBrowserSource(const QUrl& url);
  void clickedBrowserAnchor(const QUrl& url);
  void clickedBrowserCloseButton();
  void unlockCommandViaBrowser();

  // performance change
  void setPerformance();
  void setPerformanceGlobal();

  // grid change
  void setGrid();
  void setGridGlobal();

  // appearance change
  void setAppearance();
  void setAppearanceGlobal();

  // general change
  void setGeneral();
  void setGeneralGlobal();

  // clear object explorer
  void clearObjectExplorer();

  // get the appropriate import parameters by extension
  OdTvBaseImportParams* getImportParameters(const OdString& strExt, bool bCallImportDlg, bool bSetFeedBack, bool& bNeedLowMemoryImport/*out*/, OdString& vsfDumpFile/*out*/);

  // get the appropriate append parameters by extension
  OdTvBaseImportParams* getAppendParameters(const OdString& strExt);

  // hide expand menu
  void hideSliderMenuSl();
  // open visual styles dialog
  void visualStyles();
  // enable item panel buttons after end animation
  void enableItemPanelBtnSl();

  void tabChangedSl(OdTvUIBaseView *view);
  void fileTabChangedSl(OdTvUIBaseView* view);

  void newFile();
  void open();
  void save();
  void saveAs();
  void regenAll();

signals:
  // Make visualize properties tab active
  void enableVisualizePropertiesTabSg();

private:

  struct OdTvFileOptions
  {
    QString fileName;
    bool bIsMT;
    bool bIsPartial;
    bool bIsCallImportDlg;
    bool bIsSetFeedback;
    bool bIsCommonParams;
    bool bUseMemoryLimit;
    OdUInt32 nMemoryLimit;

    OdTvFileOptions() : fileName(""), bIsMT(true), bIsPartial(false), bIsCallImportDlg(true), bIsSetFeedback(true), bIsCommonParams(false), bUseMemoryLimit( false ), nMemoryLimit( 0 )
    {}

    OdTvFileOptions(const QString& str, bool bMT, bool bPartial)
      : fileName(str), bIsMT(bMT), bIsPartial(bPartial), bIsCallImportDlg(true), bIsSetFeedback(true), bIsCommonParams(false), bUseMemoryLimit( false ), nMemoryLimit( 0 )
    {}

    OdTvFileOptions(const QString& str, bool bMT, bool bPartial, bool bCallImportDlg, bool bSetFeedback, bool bCommonParams)
      : fileName(str), bIsMT(bMT), bIsPartial(bPartial), bIsCallImportDlg(bCallImportDlg), bIsSetFeedback(bSetFeedback), bIsCommonParams(bCommonParams), bUseMemoryLimit( false ), nMemoryLimit( 0 )
    {}
  };

  bool openFile(OdTvFileOptions& opt);

  // file methods
  void append();
  void exportToPDF();
  void exportToOBJ();
  void exportToDWG();
  void exportToXML();
  // close window
  void onCloseClick();
  //open file
  void open(bool bIsMT, bool bIsPartial, bool bUseMemLim, OdUInt32 nMemLim);
  //generate database
  void generateDatabase(OdTvFactoryId& id, SampleModels sample);
  //load database from file
  OdTvModelId loadDatabase(OdTvFactoryId& id, OdTvBaseImportParams* pImportParam, LoadMode loadMode, bool bIsMT, bool bIsPartial, bool bNeedLowMemoryImport, const OdString& vsfDumpFile);

  //examples
  void runSurfPlotSample();

  // window base methods
  void createStatusBar();
  void saveWidgetPlacement(QWidget* pWidget, const QString qsKey = "") const;  // for QMainWindow
  void restoreWidgetPlacement(QWidget* pWidget, const QString qsKey = "", /*for QMainWindow*/int widthDefault = 600, int heightDefault = 200) const;

  //create tool button with menu pop-up
  QToolButton* createToolButtonWithPopup(QList<QAction*>::const_iterator& posList, int nActionsToAdd);

  // show dialogs with import parameters for different files
  int showImportParamsDlg(const QString& str, bool* bNeedLowMemoryImport = NULL/*out*/, OdString* vsfDumpFile = NULL/*out*/);
  // show dialogs with append parameters for different files
  int showAppendParamsDlg(const QString& str, bool bFirstDevice = true);

  // connect signals and slots
  void connectSignals();
  // enable panel with buttons if file exist
  void setButtonsBarEnabled(bool bEnable);
  // update buttons state(view, projection, style, appearance buttons)
  void updateViewButtonState();
  void updateProjectionButtonState();
  void updateRenderModeButtonState();
  void updateAppearanceButtonState();
  void updatePanelsButtonState();
  void updateSectioningButtonState();
  // set bottom border for widget
  void setWidgetUnderlined(QWidget *widget);
  void addFileToRecent(const QString& fileName);

private:
  OdTvUIMenuToolbar *m_pMenuToolbar;
  OdTvUIItemPanel   *m_pItemPanel;

  // central widget
  QWidget   *m_pCentralWidget;
  QSplitter *m_pMainSplitter;

  // expand menus
  OdTvUISliderMenu              *m_pSliderMenu;
  OdTvViewerOpenRecentMenu      *m_pRecentMenu;
  OdTvViewerExportMenu          *m_pExportMenu;
  OdTvViewerExamplesMenu        *m_pExampleMenu;
  OdTvViewerPlotsMenu           *m_pPlotsMenu;
  OdTvViewerMaterialsMenu       *m_pMaterialsMenu;
  OdTvViewerDebugExamplesMenu   *m_pDebugExamplesMenu;
  OdTvViewerDbgShellAttributes  *m_pDbgShellAttributesMenu;
  OdTvViewerDbgMeshAttributes   *m_pDbgMeshAttributesMenu;
  OdTvViewerDbgResetTraitsMenu  *m_pDbgResetTraitsMenu;

  // smart pointer to the browser object
  OdSharedPtr<QTextBrowser> m_pBrowser;

  // TV factory Id
  OdTvFactoryId m_Tvfactory;
  // Tv limitator
  OdTvViewerLimitManager m_limitator;

  // is used to block duplicate commands via Url 
  // (cleared via unlockCommandViaBrowser by QTimer::singleShot)
  QString m_qsBrowserLastCommand; 

  //provides an area in which MDI windows are displayed
  QMdiArea      *m_pMdiArea;

  //provides the possibility to send the triggred event to one slot
  QSignalMapper *m_pWindowMapper;

  // need under Android platform for controlling the real closing (with browser window) of app
  bool m_bClosed;

  // global grid properties
  OdTvUIGridOptions               m_gridOptions;
  //global device properties
  OdTvUIPerformanceOptions             m_performanceOptions;
  // global grid properties
  OdTvUIAppearanceOptions         m_appearanceOptions;
  // global general properties
  OdTvUIGeneralOptions            m_generalOptions;

  // a set of objects with properties for import from different file formats
  OdTvImportParametersContainer   m_importParams;

  // a set of objects with properties for append from different file formats
  OdTvAppendParametersContainer   m_appendParams;

  //a map of databases and appropriate views
  QMap<OdTvDatabaseId, unsigned int>  m_mapDbNumberOfViews;

  // enable or disable model browser and properties
  bool m_bBrowserIsEnabled;
  bool m_bPropertiesIsEnabled;

  // dock panel with object explorer and properties palette
  OdTvUIPalettePanel  *m_pDockPanel;
  // tab bar for opened files
  OdTvUIFileTabBar    *m_pFileTabs;
  // active sub window
  OdTvUIBaseView      *m_pActiveView;
  QLabel              *m_pBgImageLbl;
  QStackedWidget      *m_pCentralStackedWid;
  QPixmap             m_bgImg;
};

#endif //OD_TV_VIEWERMAINWINDOW_H
