/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

// ODA Platform
#include "OdaCommon.h"

//ODA Visualize Viewer
#include "TvViewerViewDependentObject.h"

//**********************************************************************************************************************************
//OdTv3DCoordinateSystem implementation
//**********************************************************************************************************************************

OdTv3DCoordinateSystem::OdTv3DCoordinateSystem(int iViewport, OdTvDatabaseId& databaseId, OdTvModelId& modelId, const OdTvPoint& pointXYZ, const OdTvPoint& pointXY, const OdTvPoint& pointXZ)
  :OdTvViewerViewDependentObject(iViewport)
{
  m_databaseId = databaseId;
  m_modelId = modelId;
  OdTvModelPtr pModel = m_modelId.openObject(OdTv::kForWrite);
  m_axisEntityId = pModel->appendEntity(OD_T("Axises"));
  m_cellEntityId = pModel->appendEntity(OD_T("Cell"));
  m_textMarkEntityId = pModel->appendEntity(OD_T("TextMark"));
  m_textAxisEntityId = pModel->appendEntity(OD_T("TextAxis"));
  OdTvEntityPtr cellEntityPtr = m_cellEntityId.openObject(OdTv::kForWrite);
  cellEntityPtr->setLinetype(OdTvLinetype::kDotted);
  cellEntityPtr->setLinetypeScale(0.1);

  m_pointXYZ = pointXYZ;
  m_pointXY = pointXY;
  m_pointXZ = pointXZ;

  m_startXMark = 0.;
  m_endYMark = 10.;
  m_startYMark = 0.;
  m_endYMark = 10.;
  m_startZMark = 0.;
  m_endZMark = 10.;
  m_xNum = 5;
  m_yNum = 5;
  m_zNum = 5;

  OdTvDatabasePtr pTvDb = m_databaseId.openObject();
  m_midRtextStyle = pTvDb->createTextStyle(OD_T("kCoordSystem"));
  {
    OdString typeface = L"Arial";
    OdInt16 charset = 0;
    OdInt16 family = 12;
    bool bold = false;
    bool italic = false;

    OdTvTextStylePtr pTextStyle = m_midRtextStyle.openObject(OdTv::kForWrite);
    pTextStyle->setFont(typeface, bold, italic, charset, family);
    pTextStyle->setAlignmentMode(OdTvTextStyle::kMiddleRight);
    pTextStyle->setTextSize(0.02);
  }
  
  OdTvEntityPtr pTextMark = m_textMarkEntityId.openObject(OdTv::kForWrite);
  pTextMark->setTextStyle(m_midRtextStyle);
  pTextMark->setAutoRegen(true);
  OdTvEntityPtr pTextAxis = m_textAxisEntityId.openObject(OdTv::kForWrite);
  pTextAxis->setTextStyle(m_midRtextStyle);
  pTextAxis->setAutoRegen(true);
}

void OdTv3DCoordinateSystem::setStartXMark(double startMark)
{
  m_startXMark = startMark;
}

void OdTv3DCoordinateSystem::setEndXMark(double endMark)
{
  m_endXMark = endMark;
}

void OdTv3DCoordinateSystem::setStartYMark(double startMark)
{
  m_startYMark = startMark;
}

void OdTv3DCoordinateSystem::setEndYMark(double endMark)
{
  m_endYMark = endMark;
}

void OdTv3DCoordinateSystem::setStartZMark(double startMark)
{
  m_startZMark = startMark;
}

void OdTv3DCoordinateSystem::setEndZMark(double endMark)
{
  m_endZMark = endMark;
}

void OdTv3DCoordinateSystem::setXNum(OdUInt32 num)
{
  m_xNum = num;
}

void OdTv3DCoordinateSystem::setYNum(OdUInt32 num)
{
  m_yNum = num;
}

void OdTv3DCoordinateSystem::setZNum(OdUInt32 num)
{
  m_zNum = num;
}

OdTvEntityPtr OdTv3DCoordinateSystem::resetEntity(OdTvEntityId& entityId)
{
  OdTvEntityPtr entityPtr = entityId.openObject(OdTv::kForWrite);
  entityPtr->clearGeometriesData();
  return entityPtr;
}

void OdTv3DCoordinateSystem::updatePointsByLongitude(double longitude, OdTvVector& posTargetToX)
{
  m_xMarkLinesSizeX = 0.03;
  m_xMarkLinesSizeZ = 0.03;
  m_yMarkLinesSizeY = 0.03;
  m_yMarkLinesSizeZ = 0.03;

  if (longitude >= 0. && longitude <= OdaPI / 2. && posTargetToX.x <= 0.)
  {
    m_xAxisA1 = m_xAxisA = m_pointXYZ;
    m_xAxisB1 = m_xAxisB = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_xAxisA2 = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_xAxisB2 = m_pointXY;

    m_yAxisA1 = m_yAxisA = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_yAxisB1 = m_yAxisB = m_pointXY;
    m_yAxisA2 = m_pointXYZ;
    m_yAxisB2 = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);

    m_zAxisA = m_pointXYZ;
    m_zAxisB = m_pointXZ;
    m_zAxisA1 = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_zAxisB1 = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXZ.z);
    m_zAxisA2 = m_pointXY;
    m_zAxisB2 = OdTvPoint(m_pointXY.x, m_pointXY.y, m_pointXZ.z);

    m_xMarkLinesSizeX *= -1.;
  }
  else if (longitude > OdaPI / 2 && longitude <= OdaPI && posTargetToX.x <= 0.)
  {
    m_xAxisA1 = m_xAxisA = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_xAxisB1 = m_xAxisB = m_pointXY;
    m_xAxisA2 = m_pointXYZ;
    m_xAxisB2 = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);

    m_yAxisA1 = m_yAxisA = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_yAxisB1 = m_yAxisB = m_pointXY;
    m_yAxisA2 = m_pointXYZ;
    m_yAxisB2 = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);

    m_zAxisA = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_zAxisB = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXZ.z);
    m_zAxisA1 = m_pointXYZ;
    m_zAxisB1 = m_pointXZ;
    m_zAxisA2 = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_zAxisB2 = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXZ.z);
  }
  else if (longitude >= 0. && longitude <= OdaPI / 2. && posTargetToX.x > 0.)
  {
    m_xAxisA1 = m_xAxisA = m_pointXYZ;
    m_xAxisB1 = m_xAxisB = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_xAxisA2 = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_xAxisB2 = m_pointXY;

    m_yAxisA1 = m_yAxisA = m_pointXYZ;
    m_yAxisB1 = m_yAxisB = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_yAxisA2 = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_yAxisB2 = m_pointXY;

    m_zAxisA = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_zAxisB = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXZ.z);
    m_zAxisA1 = m_pointXY;
    m_zAxisB1 = OdTvPoint(m_pointXY.x, m_pointXY.y, m_pointXZ.z);
    m_zAxisA2 = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_zAxisB2 = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXZ.z);

    m_xMarkLinesSizeX *= -1.;
    m_yMarkLinesSizeY *= -1.;
  }
  else if (longitude > OdaPI / 2 && longitude <= OdaPI && posTargetToX.x > 0.)
  {
    m_xAxisA1 = m_xAxisA = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_xAxisB1 = m_xAxisB = m_pointXY;
    m_xAxisA2 = m_pointXYZ;
    m_xAxisB2 = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);

    m_yAxisA1 = m_yAxisA = m_pointXYZ;
    m_yAxisB1 = m_yAxisB = OdTvPoint(m_pointXYZ.x, m_pointXY.y, m_pointXYZ.z);
    m_yAxisA2 = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_yAxisB2 = m_pointXY;

    m_zAxisA = m_pointXY;
    m_zAxisB = OdTvPoint(m_pointXY.x, m_pointXY.y, m_pointXZ.z);
    m_zAxisA1 = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXYZ.z);
    m_zAxisB1 = OdTvPoint(m_pointXY.x, m_pointXYZ.y, m_pointXZ.z);
    m_zAxisA2 = m_pointXYZ;
    m_zAxisB2 = m_pointXZ;

    m_yMarkLinesSizeY *= -1.;
  }
}

void OdTv3DCoordinateSystem::updateMarksData(double longitude, double latityde, OdTvVector& posTargetToX, OdUInt32& xNum, OdUInt32& yNum, OdUInt32& zNum)
{
  m_zMarkLinesSizeX = 0.03;
  m_zMarkLinesSizeY = 0.03;


  if (longitude >= 0. && longitude <= OdaPI / 6.)
  {
    m_zMarkLinesSizeX *= -1.;
    m_zMarkLinesSizeY = 0;

    yNum = yNum / 2 + 1;
  }
  if (longitude > OdaPI / 6. && longitude <= OdaPI / 4.)
  {
    m_zMarkLinesSizeX *= -1.;
    m_zMarkLinesSizeY = 0;
  }
  if (longitude > OdaPI / 4. && longitude <= OdaPI / 3.)
  {
    if (posTargetToX.x < 0.)
    {
      m_zMarkLinesSizeX = 0.;
      m_zMarkLinesSizeY *= -1.;
    }
    else
    {
      m_zMarkLinesSizeX *= -1.;
      m_zMarkLinesSizeY = 0.;
    }
  }
  if (longitude > OdaPI / 3. && longitude <= OdaPI / 2.)
  {
    m_zMarkLinesSizeX = 0.;
    if (posTargetToX.x < 0.)
      m_zMarkLinesSizeY *= -1.;

    xNum = xNum / 2 + 1;
  }
  if (longitude > OdaPI / 2. && longitude <= 2 * OdaPI / 3.)
  {
    m_zMarkLinesSizeX = 0.;
    if (posTargetToX.x < 0.)
      m_zMarkLinesSizeY *= -1.;

    xNum = xNum / 2 + 1;
  }
  if (longitude > 2 * OdaPI / 3. && longitude <= 3 * OdaPI / 4.)
  {
    if (posTargetToX.x < 0.)
    {
      m_zMarkLinesSizeX = 0.;
      m_zMarkLinesSizeY *= -1.;
    }
    else
      m_zMarkLinesSizeY = 0.;
  }
  if (longitude > 3 * OdaPI / 4. && longitude <= 5 * OdaPI / 6.)
  {
    m_zMarkLinesSizeY = 0;
  }
  if (longitude > 5 * OdaPI / 6. && longitude <= OdaPI)
  {
    m_zMarkLinesSizeY = 0;

    yNum = yNum / 2 + 1;
  }

  if (latityde < OdaPI / 3. || latityde > 2 * OdaPI / 3.)
  {
    m_xMarkLinesSizeZ = 0.;
    m_yMarkLinesSizeZ = 0.;
  }
  else if (latityde > OdaPI / 3. && latityde <= OdaPI / 2.)
  {
    m_xMarkLinesSizeX = 0.;
    m_yMarkLinesSizeY = 0.;
  }
  else if (latityde > OdaPI / 2. && latityde < 2 * OdaPI / 3.)
  {
    m_xMarkLinesSizeX = 0.;
    m_yMarkLinesSizeY = 0.;

    m_xMarkLinesSizeZ *= -1.;
    m_yMarkLinesSizeZ *= -1.;
  }

  if ((latityde >= 0 && latityde < OdaPI / 6.) || (latityde >= 5 * OdaPI / 6. && latityde <= OdaPI))
    zNum = zNum / 2 + 1;
}

void OdTv3DCoordinateSystem::updatePointsByLatityde(double latityde)
{
  if (latityde < OdaPI / 2)
  {
    m_xAxisA1.z = m_pointXZ.z;
    m_xAxisB1.z = m_pointXZ.z;
    m_xAxisA2.z = m_pointXZ.z;
    m_xAxisB2.z = m_pointXZ.z;
    m_yAxisA1.z = m_pointXZ.z;
    m_yAxisB1.z = m_pointXZ.z;
    m_yAxisA2.z = m_pointXZ.z;
    m_yAxisB2.z = m_pointXZ.z;
  }
}

OdString OdTv3DCoordinateSystem::generateMark(double mark) const
{
  OdString markStr;
  
  markStr.format(OD_T("%.1f"), mark);

  return markStr;
}

void OdTv3DCoordinateSystem::fillDataForX(OdTvPoint& pointOne, OdTvPoint& pointTwo, OdTvPoint& pointThree, OdTvPoint& pointFour, OdTvPoint& pointFive, OdTvPoint& pointSix, OdTvPoint& pointSeven, double& posStep, double& markStep, OdUInt32 num)
{
  pointOne = OdTvPoint(0., m_xAxisA1.y + 2 * m_xMarkLinesSizeX, m_xAxisA1.z + 2 * m_xMarkLinesSizeZ);
  pointTwo = OdTvPoint(0., m_xAxisA1.y, m_xAxisA1.z);
  pointThree = OdTvPoint(0., m_xAxisA2.y, m_xAxisA2.z);
  pointFour = OdTvPoint(0., m_xAxisB2.y, m_zAxisB.z);
  pointFive = OdTvPoint(0., m_xAxisB2.y, m_xAxisB.z);
  pointSix = OdTvPoint(0., m_xAxisA1.y + m_xMarkLinesSizeX, m_xAxisA1.z + m_xMarkLinesSizeZ);
  pointSeven = OdTvPoint((m_xAxisA1.x + m_xAxisB1.x) / 2., m_xAxisA1.y + 6 * m_xMarkLinesSizeX, m_xAxisA1.z + 5 * m_xMarkLinesSizeZ);

  posStep = fabs(m_xAxisA.x - m_xAxisB.x) / (num - 1);
  markStep = fabs(m_endXMark - m_startXMark) / (num - 1);
}

void OdTv3DCoordinateSystem::fillDataForY(OdTvPoint& pointOne, OdTvPoint& pointTwo, OdTvPoint& pointThree, OdTvPoint& pointFour, OdTvPoint& pointFive, OdTvPoint& pointSix, OdTvPoint& pointSeven, double& posStep, double& markStep, OdUInt32 num)
{
  pointOne = OdTvPoint(m_yAxisA1.x + 2 * m_yMarkLinesSizeY, 0., m_yAxisA1.z + 2 * m_yMarkLinesSizeZ);
  pointTwo = OdTvPoint(m_yAxisA1.x, 0., m_yAxisA1.z);
  pointThree = OdTvPoint(m_yAxisA2.x, 0., m_yAxisA2.z);
  pointFour = OdTvPoint(m_yAxisA2.x, 0., m_zAxisB.z);
  pointFive = OdTvPoint(m_yAxisA2.x, 0., m_xAxisB.z);
  pointSix = OdTvPoint(m_yAxisA1.x + m_yMarkLinesSizeY, 0., m_yAxisA1.z + m_yMarkLinesSizeZ);
  pointSeven = OdTvPoint(m_yAxisA1.x + 6 * m_yMarkLinesSizeY, (m_yAxisA1.y + m_yAxisB1.y)/2., m_yAxisA1.z + 5 * m_yMarkLinesSizeZ);

  posStep = fabs(m_yAxisA.y - m_yAxisB.y) / (num - 1);
  markStep = fabs(m_endYMark - m_startYMark) / (num - 1);
}

void OdTv3DCoordinateSystem::fillDataForZ(OdTvPoint& pointOne, OdTvPoint& pointTwo, OdTvPoint& pointThree, OdTvPoint& pointFour, OdTvPoint& pointFive, OdTvPoint& pointSix, OdTvPoint& pointSeven, double& posStep, double& markStep, OdUInt32 num)
{
  pointOne = OdTvPoint(m_zAxisA.x + 3 * m_zMarkLinesSizeX, m_zAxisA.y + 3 * m_zMarkLinesSizeY, 0.);
  pointTwo = OdTvPoint(m_zAxisA.x, m_zAxisA.y, 0.);
  pointThree = OdTvPoint(m_zAxisA1.x, m_zAxisA1.y, 0.);
  pointFour = pointFive = OdTvPoint(m_zAxisA2.x, m_zAxisA2.y, 0.);
  pointSix = OdTvPoint(m_zAxisA.x + m_zMarkLinesSizeX, m_zAxisA.y + m_zMarkLinesSizeY, 0.);
  pointSeven = OdTvPoint(m_zAxisA.x + 5 * m_zMarkLinesSizeX, m_zAxisA.y + 5 * m_zMarkLinesSizeY, (m_zAxisA1.z + m_zAxisB1.z)/2.);

  posStep = fabs(m_zAxisA.z - m_zAxisB.z) / (num - 1);
  markStep = fabs(m_endZMark - m_startZMark) / (num - 1);
}

OdTvPoint& OdTv3DCoordinateSystem::updatePointForX(OdTvPoint& point, double value)
{
  point.x = value;
  return point;
}

OdTvPoint& OdTv3DCoordinateSystem::updatePointForY(OdTvPoint& point, double value)
{
  point.y = value;
  return point;
}

OdTvPoint& OdTv3DCoordinateSystem::updatePointForZ(OdTvPoint& point, double value)
{
  point.z = value;
  return point;
}

void OdTv3DCoordinateSystem::drawCoordinateCells(OdTvEntityPtr& pGraphEntity, OdTvEntityPtr& pCellEntity, double latityde,
  double startMark, OdUInt32 num, double startPos, const OdString& axisText,
  void(OdTv3DCoordinateSystem::*fillData)(OdTvPoint&, OdTvPoint&, OdTvPoint&, OdTvPoint&, OdTvPoint&, OdTvPoint&, OdTvPoint&, double&, double&, OdUInt32),
  OdTvPoint& (OdTv3DCoordinateSystem::*updatePoint)(OdTvPoint&, double))
{
  OdTvPoint pointOne, pointTwo, pointThree, pointFour, pointFive, pointSix, poinstSeven;
  double posStep = 0., markStep = 0.;
  (this->*fillData)(pointOne, pointTwo, pointThree, pointFour, pointFive, pointSix, poinstSeven, posStep, markStep, num);

  m_textMarkEntityId.openObject(OdTv::kForWrite)->setColor(OdTvColorDef(0, 0, 0));
  m_textAxisEntityId.openObject(OdTv::kForWrite)->setColor(OdTvColorDef(0, 0, 0));

  double pos = startPos; double mark = startMark;
  for (OdUInt32 i = 0; i < num; i++)
  {
    OdTvGeometryDataId textMarkId = m_textMarkEntityId.openObject(OdTv::kForWrite)->appendText((this->*updatePoint)(pointOne, pos), generateMark(mark));
    OdTvTextDataPtr pTextMark = textMarkId.openAsText();
    pTextMark->setAlignmentMode(OdTvTextStyle::kMiddleCenter);
    pTextMark->setNonRotatable(true);
    mark += markStep;

    pCellEntity->appendPolyline((this->*updatePoint)(pointTwo, pos), (this->*updatePoint)(pointThree, pos)); // lines on the floor
    if (latityde >= OdaPI / 2)
      pCellEntity->appendPolyline((this->*updatePoint)(pointThree, pos), (this->*updatePoint)(pointFour, pos)); // lines on wall
    else
      pCellEntity->appendPolyline((this->*updatePoint)(pointThree, pos), (this->*updatePoint)(pointFive, pos)); // lines on wall
    pGraphEntity->appendPolyline((this->*updatePoint)(pointTwo, pos), (this->*updatePoint)(pointSix, pos)); // mark lines

    pos += posStep;
  }
  OdTvGeometryDataId textMarkId = m_textAxisEntityId.openObject(OdTv::kForWrite)->appendText(poinstSeven, axisText);
  OdTvTextDataPtr pTextMark = textMarkId.openAsText();
  pTextMark->setNonRotatable(true);
}

void OdTv3DCoordinateSystem::update(const OdTvGsViewId& tvView)
{
  // Extract main view parameters
  OdTvGsViewPtr pView = tvView.openObject();

  OdTvEntityPtr pNewGraphEntity = resetEntity(m_axisEntityId);
  OdTvEntityPtr pNewCellEntity = resetEntity(m_cellEntityId);
  pNewCellEntity->setLinetype(OdTvLinetype::kDotted);
  pNewCellEntity->setLinetypeScale(0.1);
  OdTvEntityPtr pNewMarkEntity = resetEntity(m_textMarkEntityId);
  pNewMarkEntity->setTextStyle(m_midRtextStyle);
  pNewMarkEntity->setAutoRegen(true);
  OdTvEntityPtr pNewAxisEntity = resetEntity(m_textAxisEntityId);
  pNewAxisEntity->setTextStyle(m_midRtextStyle);
  pNewAxisEntity->setAutoRegen(true);

  OdTvPoint target = pView->target();
  OdTvPoint position = pView->position();

  OdTvVector posTargetVector = pView->target() - pView->position();
  double latityde = posTargetVector.angleTo(OdTvVector::kZAxis);
  double longitude = posTargetVector.angleTo(OdTvVector::kYAxis);
  OdTvVector normalPosTargetVector = posTargetVector;
  normalPosTargetVector.normalize();
  OdTvVector posTargetToX = OdTvVector::kXAxis * normalPosTargetVector.dotProduct(OdTvVector::kXAxis);

  // Local nums depend on longitude
  OdUInt32 xNum = m_xNum;
  OdUInt32 yNum = m_yNum;
  OdUInt32 zNum = m_zNum;

  // Update base points
  updatePointsByLongitude(longitude, posTargetToX);

  // Update points by latityde
  updatePointsByLatityde(latityde);

  // Update mark lines sizes and nums
  updateMarksData(longitude, latityde, posTargetToX, xNum, yNum, zNum);

  // Draw axis
  pNewGraphEntity->appendPolyline(m_xAxisA1, m_xAxisB1);
  pNewGraphEntity->appendPolyline(m_yAxisA1, m_yAxisB1);
  pNewGraphEntity->appendPolyline(m_zAxisA, m_zAxisB);

  drawCoordinateCells(pNewGraphEntity, pNewCellEntity, latityde, m_startXMark, xNum, m_xAxisA.x, OD_T("X"), &OdTv3DCoordinateSystem::fillDataForX, &OdTv3DCoordinateSystem::updatePointForX);
  drawCoordinateCells(pNewGraphEntity, pNewCellEntity, latityde, m_startYMark, yNum, m_yAxisA.y, OD_T("Y"), &OdTv3DCoordinateSystem::fillDataForY, &OdTv3DCoordinateSystem::updatePointForY);
  drawCoordinateCells(pNewGraphEntity, pNewCellEntity, latityde, m_startZMark, zNum, m_zAxisA.z, OD_T("Z"), &OdTv3DCoordinateSystem::fillDataForZ, &OdTv3DCoordinateSystem::updatePointForZ);
}
