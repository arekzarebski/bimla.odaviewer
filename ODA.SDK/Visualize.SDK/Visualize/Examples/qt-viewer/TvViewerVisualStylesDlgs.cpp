/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#include "OdaCommon.h"

//QT
#include <QApplication>
#include <QHBoxLayout>
#include <QStackedWidget>
#include <QLineEdit>
#include <QCheckBox>
#include <QLabel>
#include <QScrollArea>
#include <QGroupBox>
#include <QDoubleSpinBox>
#include <QSignalMapper>
#include "QInputDialog"
#include "QMessageBox"
#include <QMdiArea>
#include <QGridLayout>
#include <QFont>

#include "TvViewerVisualStylesDlgs.h"
#include "OdTvUIVisualizeObjectProperties.h"
#include "TvFactory.h"

//***************************************************************************//
// 'OdTvViewerVisualStylesGuidedCtrls' methods implementation
//***************************************************************************//
OdTvViewerVisualStylesGuidedCtrls::OdTvViewerVisualStylesGuidedCtrls()
 : m_type(kUnknown)
 , m_bit(0)
{
}

OdTvViewerVisualStylesGuidedCtrls::OdTvViewerVisualStylesGuidedCtrls(ConditionType type)
: m_type(type)
, m_bit(0)
{
}

OdTvViewerVisualStylesGuidedCtrls::~OdTvViewerVisualStylesGuidedCtrls()
{
  m_ctrlList.clear();
  m_controlledValues.clear();
}

void OdTvViewerVisualStylesGuidedCtrls::updateState(OdUInt32 val)
{
  bool bEnable = false;

  if ( m_type == kBit )
    bEnable = GETBIT(val, m_bit);
  else if ( m_type == kValues )
  {
    foreach(OdUInt32 iValue, m_controlledValues)
    {
      if ( val == iValue )
      {
        bEnable = true;
        break;
      }
    }
  }
  else
    return;

  foreach(QWidget *pWidg, m_ctrlList)
    pWidg->setEnabled(bEnable);

  return;
}

//***************************************************************************//
// 'OdTvViewerVisualStylesPreview' methods implementation
//***************************************************************************//
OdTvViewerVisualStylesPreview::OdTvViewerVisualStylesPreview(OdTvUIBaseView* pAppActiveView, QWidget* parent /*= 0*/) : QWidget(parent), m_bSceneGraphEnadled(false)
{
  m_bWasInitialized = false;
  setAttribute(Qt::WA_PaintOnScreen);
  setBackgroundRole(QPalette::NoRole);

  m_pActiveView = pAppActiveView;
  OdTvGsDeviceId tvDeviceId = pAppActiveView->getTvDeviceId();
  if (!tvDeviceId.isNull())
  {
    m_iBgColor = tvDeviceId.openObject()->getBackgroundColor();
    tvDeviceId.openObject()->getOption(OdTvGsDevice::kUseSceneGraph, m_bSceneGraphEnadled);
  }

  m_TvDatabaseId = odTvGetFactory().createDatabase();
}


OdTvViewerVisualStylesPreview::~OdTvViewerVisualStylesPreview()
{
  odTvGetFactory().removeDatabase(m_TvDatabaseId);
}


void OdTvViewerVisualStylesPreview::setVisualStyle(const OdTvVisualStyleId& id)
{
  if (id.isNull())
    return;

  m_TvVisualStyleId = id;

  if (!m_TvViewId.isNull())
    m_TvViewId.openObject(OdTv::kForWrite)->setVisualStyle(m_TvVisualStyleId);
  
  updatePreview();
}


void OdTvViewerVisualStylesPreview::updatePreview(bool bDueToChanges)
{
  if (bDueToChanges)
  {
    OdTvGsDevicePtr pTvDevice = m_TvDeviceId.openObject(OdTv::kForWrite);
    if (!pTvDevice.isNull())
      pTvDevice->invalidate();
  }

  update();

  if (bDueToChanges && !m_visualStylesList.contains(toQString(m_TvVisualStyleId.openObject()->getName())))
    m_visualStylesList << toQString(m_TvVisualStyleId.openObject()->getName());
}

QPaintEngine* OdTvViewerVisualStylesPreview::paintEngine() const
{
  return NULL;
}

bool OdTvViewerVisualStylesPreview::eventFilter(QObject *obj, QEvent *ev)
{
  // filter the paint event
  if (ev->type() == QEvent::Paint)
    return true;
  else
    return QObject::eventFilter(obj, ev);
}

void OdTvViewerVisualStylesPreview::paintEvent(QPaintEvent* e)
{
  if (!m_bWasInitialized)
  {
    installEventFilter(this);

    // perform initialization
    initialize();

    // deinstall the event filter. Now paint event again can come
    removeEventFilter(this);
    m_bWasInitialized = true;
  }
  else
    setAttribute(Qt::WA_NoSystemBackground);

  OdTvGsDevicePtr pTvDevice = m_TvDeviceId.openObject(OdTv::kForWrite);
  pTvDevice->update();
}

void OdTvViewerVisualStylesPreview::initialize()
{
  //create model
  OdString strModelName(OD_T("Model"));
  m_TvModelId = m_TvDatabaseId.openObject(OdTv::kForWrite)->createModel(strModelName, OdTvModel::kMain, false);
  OdTvModelPtr pModel = m_TvModelId.openObject(OdTv::kForWrite);

  //prepare rotation matrix
  OdTvMatrix matr = OdGeMatrix3d::rotation(-OdaPI / 4, OdGeVector3d::kXAxis) * OdGeMatrix3d::rotation(-OdaPI / 4, OdGeVector3d::kZAxis);

  // create sphere
  OdString strEntityName(OD_T("Entity_Sphere"));
  OdTvEntityId sphereEntityId = pModel->appendEntity(strEntityName);
  OdTvEntityPtr pSphereEntity = sphereEntityId.openObject(OdTv::kForWrite);
  pSphereEntity->setColor(OdTvColorDef(0, 0, 255));
  OdTvGeometryDataId sphereId = pSphereEntity->appendSphere(OdTvPoint(-0.14, 0., 0.), 0.24);

  //add transform to sphere
  pSphereEntity->setModelingMatrix(matr);

  // create box
  strEntityName = OD_T("Entity_Cube");
  OdTvEntityId cubeEntityId = pModel->appendEntity(strEntityName);
  OdTvEntityPtr pCubeEntity = cubeEntityId.openObject(OdTv::kForWrite);
  pCubeEntity->setColor(OdTvColorDef(255, 0, 0));
  pCubeEntity->appendBox(OdTvPoint(0.2175, 0.1715, 0.0425), 0.285, 0.297, 0.385);

  //add transfrom to box
  matr.setTranslation(OdGeVector3d(-0.1, 0.0, 0.0));
  pCubeEntity->setModelingMatrix(matr);

  //create device
  OdTvGsDevice::Name gsDeviceName = OdTvGsDevice::kOpenGLES2;
  OdString strDeviceName(OD_T("Device"));
  OdTvDCRect rect(0, size().width() * this->devicePixelRatio(), size().height() * this->devicePixelRatio(), 0);

  m_TvDeviceId = m_TvDatabaseId.openObject(OdTv::kForWrite)->createDevice(strDeviceName, (OSWindowHandle)winId(), rect, gsDeviceName, NULL);
  if (m_TvDeviceId.isNull() )
  {
    ODA_ASSERT(FALSE);
    return;
  }

  //setup device
  OdTvGsDevicePtr pTvDevice = m_TvDeviceId.openObject(OdTv::kForWrite);
  pTvDevice->setBackgroundColor(m_iBgColor);
  pTvDevice->setOption(OdTvGsDevice::kUseVisualStyles, true);
  pTvDevice->setOption(OdTvGsDevice::kUseSceneGraph, m_bSceneGraphEnadled);
  pTvDevice->setOption(OdTvGsDevice::kUseCompositeMetafiles, true);
  //create view
  OdString strViewName(OD_T("Device"));
  m_TvViewId = pTvDevice->createView(strViewName, false);
  pTvDevice->addView(m_TvViewId);
  
  //setup view
  OdTvGsViewPtr viewPtr = m_TvViewId.openObject(OdTv::kForWrite);
  viewPtr->setView(OdTvPoint(0., 0., 1.), OdTvPoint::kOrigin, OdTvVector::kYAxis, 1., 0.6);
  viewPtr->addModel(m_TvModelId);
  viewPtr->setActive(true);
  viewPtr->zoom(1.22);
  if (!m_TvVisualStyleId.isNull())
    viewPtr->setVisualStyle(m_TvVisualStyleId);
}


//***************************************************************************//
// 'OdTvViewerVisualStylesDlg' methods implementation
//***************************************************************************//

QStringList operationList = (QStringList() << "Invalid operation" << "Inherit" << "Set" << "Disable" << "Enable");

QStringList facelightingModelList = (QStringList() << "Invisible" << "Constant" << "Phong" << "Gooch");
QStringList faceLightingQualityList = (QStringList() << "No lighting" << "Per face lighting" << "Per vertex lighting" << "Per pixel lighting");
QStringList faceColorModeList = (QStringList() << "No color mode" << "Object color" << "Background color" << "Mono" << "Tint" << "Desaturate");
QStringList faceModifierlList = (QStringList() << "Opacity" << "Specular");

QStringList edgeModelList = (QStringList() << "No edges" << "Isolines" << "Facet edges");
QStringList edgeStylesList = (QStringList() << "Silhouette" << "Obscured" << "Intersection");
QStringList edgeLinePattern = (QStringList() << "Solid" << "Dashed line" << "Dotted" << "Short dash"
                                             << "Medium dash" << "Long dash" << "DoubleShort dash" << "DoubleMedium dash" << "Double long dash"
                                             << "Medium long dash" << "Sparse dot");
QStringList edgeModifiersList = (QStringList() << "Overhang" << "Jitter" << "Width"<< "Color" << "Halo gap" << "Always on top" << "Opacity");
QStringList edgeJitterAmount = (QStringList() << "Low" << "Medium" << "High");
QStringList displayStylesList = (QStringList() << "Backgrounds" << "Materials" << "Textures");

OdTvViewerVisualStylesDlg::OdTvViewerVisualStylesDlg(OdTvDatabaseId appBbId, OdTvUIBaseView* pAppActiveView)
  : QDialog(pAppActiveView)
  , m_bChanged(false)
{
  this->setMinimumSize(QSize(700, 600));

  m_editBtnIcn = QIcon(":/browserModel/apply.png");
  m_dbId = appBbId;
  m_pActiveView = pAppActiveView;

  // fill colors list for color controls
  OdTvGsDeviceId tvAppDeviceId = pAppActiveView->getTvDeviceId();
  if (!tvAppDeviceId.isNull())
  {
    OdTvGsDevicePtr pAppDevice = tvAppDeviceId.openObject();
    int numColors;
    const OdUInt32 * colors = pAppDevice->getLogicalPalette(numColors);

    m_colorNamesList << "None" << "ByBlock" << "ByLayer" << "Select color...";
    for (int i = 0; i < numColors; i++)
      m_colorNamesList << QString("%1").arg(i);
  }

  //get current visual style
  m_curAppVisualStyleId = m_pActiveView->getVisualStyle();

  m_pMainLayout = new QGridLayout;
  m_pMainLayout->setColumnStretch(0, 0);
  m_pMainLayout->setColumnStretch(1, 1);
  m_pMainLayout->setRowStretch(0, 1);

  // Create left side
  QVBoxLayout* pLeftColumnVBoxLayout = new QVBoxLayout;

  // Create Add/Remove buttons
  QPushButton* addBtn = new QPushButton("Add");
  m_pRemoveBtn = new QPushButton("Remove");
  QHBoxLayout* pButtonsHBoxLayout = new QHBoxLayout;
  pButtonsHBoxLayout->setAlignment(Qt::AlignRight);
  pButtonsHBoxLayout->addWidget(addBtn);
  pButtonsHBoxLayout->addWidget(m_pRemoveBtn);

  QVBoxLayout *leftBoxLt = new QVBoxLayout;
  leftBoxLt->addLayout(pLeftColumnVBoxLayout);
  leftBoxLt->addLayout(pButtonsHBoxLayout);

  m_pMainLayout->addLayout(leftBoxLt, 0, 0);

  // Create preview
  m_pPreview = new OdTvViewerVisualStylesPreview(m_pActiveView, this);
  m_pPreview->setFixedSize(QSize(250, 250));
  QHBoxLayout *previewLt = new QHBoxLayout;
  previewLt->setAlignment(Qt::AlignCenter);
  previewLt->addWidget(m_pPreview);
  pLeftColumnVBoxLayout->addLayout(previewLt);

  // Create list of visual styles
  m_pVisualStylesList = new QListWidget(this);
  pLeftColumnVBoxLayout->addWidget(m_pVisualStylesList);

  // previewDatabase
  OdTvDatabasePtr pPreviewDb = m_pPreview->getTvDatabaseId().openObject(OdTv::kForWrite);

  // Fill visual styles list
  OdTvVisualStylesIteratorPtr iter = m_dbId.openObject(OdTv::kForRead)->getVisualStylesIterator();
  QListWidgetItem* pActiveItem = NULL;
  OdTvVisualStyleId activeVSId = m_curAppVisualStyleId;
  while (!iter->done())
  {
    OdTvVisualStyleId visualStyleId = iter->getVisualStyle();
    iter->step();

    if (visualStyleId.isNull())
      continue;

    OdString name = visualStyleId.openObject()->getName();
    if (toQString(name).contains("$"))
      continue;

    QListWidgetItem* item = new QListWidgetItem(tr(name), m_pVisualStylesList);
    if (visualStyleId == m_curAppVisualStyleId)
      pActiveItem = item;

    if (activeVSId.isNull())
      activeVSId = visualStyleId;

    // copy default visual styles from main database to the preview database
    OdTvVisualStyleId id = pPreviewDb->findVisualStyle(name);
    if (!id.isNull())
      id.openObject(OdTv::kForWrite)->copyFrom(visualStyleId);
  } // eo while...

  // add copy of the current app visual style to the preview database
  if (!m_curAppVisualStyleId.isNull())
  {
    OdTvVisualStyleId prevAppId = pPreviewDb->findVisualStyle(m_curAppVisualStyleId.openObject()->getName());
    if (prevAppId.isNull())
      pPreviewDb->createVisualStyle(m_curAppVisualStyleId.openObject()->getName(), m_curAppVisualStyleId);
  }

  if (pActiveItem)
  {
    QFont font = pActiveItem->font();
    font.setBold(true);
    pActiveItem->setFont(font);
  }
  else
    pActiveItem = m_pVisualStylesList->item(0);

  m_pVisualStylesList->setCurrentItem(pActiveItem);
  pActiveItem->setSelected(true);

  m_pVisualStylesList->setFocus();

  QWidget* pRight = new QWidget;
  m_pScrollArea = new QScrollArea;
  m_pScrollArea->setWidget(pRight);
  m_pScrollArea->setWidgetResizable(true);
  QVBoxLayout *rvLt = new QVBoxLayout;
  rvLt->addWidget(m_pScrollArea);

  QPushButton *okBtn = new QPushButton("Ok");
  QPushButton *cancelBtn = new QPushButton("Cancel");
  QHBoxLayout *btnLt = new QHBoxLayout;
  btnLt->setAlignment(Qt::AlignRight);
  btnLt->addWidget(okBtn);
  btnLt->addWidget(cancelBtn);
  rvLt->addLayout(btnLt);

  m_pMainLayout->addLayout(rvLt, 0, 1);
  m_pRightColumnVBoxLayout = new QVBoxLayout(pRight);

  setLayout(m_pMainLayout);
  setWindowTitle("Visual styles");

  OdString activeVSName = activeVSId.openObject()->getName();
  OdTvVisualStyleId prevAppIdForActiveView = pPreviewDb->findVisualStyle(activeVSName);
  createControls(prevAppIdForActiveView);
  updateAllGuidedControls();

  connect(m_pVisualStylesList,  SIGNAL(currentItemChanged(QListWidgetItem *, QListWidgetItem *)),   SLOT(changeVisualStyle(QListWidgetItem *, QListWidgetItem *)));
  connect(m_pVisualStylesList,  SIGNAL(itemDoubleClicked(QListWidgetItem *)),                       SLOT(applyVisualStyleToApp(QListWidgetItem *)));
  connect(m_pVisualStylesList,  SIGNAL(itemDoubleClicked(QListWidgetItem *)),                       SLOT(accept()));
  connect(addBtn,               SIGNAL(clicked()),                                                  SLOT(addVisualStyle()));
  connect(m_pRemoveBtn,         SIGNAL(clicked()),                                                  SLOT(removeVisualStyle()));
  connect(okBtn,                SIGNAL(clicked()),                                                  SLOT(okBtnClicked()));
  connect(okBtn,                SIGNAL(clicked()),                                                  SLOT(accept()));
  connect(cancelBtn,            SIGNAL(clicked()),                                                  SLOT(reject()));

  QPaintEvent ev(this->rect());
  QApplication::sendEvent(m_pPreview, &ev);
}

OdTvViewerVisualStylesDlg::~OdTvViewerVisualStylesDlg()
{
  m_colorNamesList.clear();

  m_comboboxList.clear();
  m_colorComboboxList.clear();
  m_checkboxList.clear();
  m_iLineEditList.clear();
  m_dLineEditList.clear();
  m_faceModifers.clear();
  m_edgeStyles.clear();
  m_edgeModifers.clear();
  m_displayStyles.clear();

  delete m_pVisualStylesList;
  
  delete m_pPreview;

  delete m_pRemoveBtn;
}

void OdTvViewerVisualStylesDlg::updateAppView()
{
  if (m_pActiveView)
  {
   OdTvGsDeviceId deviceId = m_pActiveView->getTvDeviceId();
    if (!deviceId.isNull())
      deviceId.openObject()->invalidate();
  }
}

bool OdTvViewerVisualStylesDlg::applyChangesToApp()
{
  bool bRes = false;

  if ( m_pPreview == NULL )
    return bRes;

  OdTvDatabasePtr pPreviewDb = m_pPreview->getTvDatabaseId().openObject(OdTv::kForWrite);
  OdTvDatabasePtr pAppDb = m_dbId.openObject(OdTv::kForWrite);

  //copy visual style parameters
  QStringList list = m_pPreview->getChangedVisualStylesList();
  for (int i = 0; i < list.size(); i++)
  {
    QString name = list[i];
    OdTvVisualStyleId curVs = pAppDb->findVisualStyle(toOdString(name));
    OdTvVisualStyleId previewVs = pPreviewDb->findVisualStyle(toOdString(name));
    if (previewVs.isNull())
      continue;
    if (curVs.isNull())
      m_dbId.openObject(OdTv::kForWrite)->createVisualStyle(toOdString(name), previewVs);
    else
    {
      curVs.openObject(OdTv::kForWrite)->copyFrom(previewVs);
      if (curVs == m_curAppVisualStyleId)
        bRes = true;
    }
  }

  return bRes;
}

void OdTvViewerVisualStylesDlg::okBtnClicked()
{
  if (applyChangesToApp())
    updateAppView();
}

void OdTvViewerVisualStylesDlg::applyVisualStyleToApp(QListWidgetItem * item)
{
  QListWidgetItem* currentItem = item;
  if (currentItem == NULL)
    return;

  bool bNeedUpdate = applyChangesToApp();

  OdTvResult rc = tvOk;
  OdTvVisualStyleId visualStyleId = m_dbId.openObject(OdTv::kForWrite)->findVisualStyle(toOdString(currentItem->text()), &rc);
  if (rc == tvOk && visualStyleId != m_curAppVisualStyleId)
  {
    m_pActiveView->setVisualStyle(visualStyleId);

    bNeedUpdate = false; // invalidate already will be performed inside the setVisualStyle
  }

  if (bNeedUpdate)
    updateAppView();
}

void OdTvViewerVisualStylesDlg::changeVisualStyle(QListWidgetItem *current, QListWidgetItem *previous)
{
  QListWidgetItem* currentItem = current;

  if (currentItem == NULL)
    return;

  OdString name = toOdString(currentItem->text());
  OdTvVisualStyleId visualStyleId = m_pPreview->getTvDatabaseId().openObject(OdTv::kForWrite)->findVisualStyle(name);
  if (visualStyleId.isNull())
  {
    OdTvVisualStyleId mainDbVsId = m_dbId.openObject(OdTv::kForWrite)->findVisualStyle(name);
    visualStyleId = m_pPreview->getTvDatabaseId().openObject(OdTv::kForWrite)->createVisualStyle(name, mainDbVsId);
  }

  updateControls(visualStyleId);
  updateAllGuidedControls();
}

void OdTvViewerVisualStylesDlg::createControls(OdTvVisualStyleId visualStyleId)
{
  m_visualStyleId = visualStyleId;
  if (m_pPreview)
    m_pPreview->setVisualStyle(visualStyleId);

  OdTvVisualStylePtr pVisualStyle = visualStyleId.openObject(OdTv::kForWrite);
  m_pRemoveBtn->setEnabled(!pVisualStyle->getDefault());

  // Face properties
  QGroupBox* pFaceGroupBox = new QGroupBox(tr("Face properties"));
  pFaceGroupBox->setStyleSheet("QGroupBox::title {color: blue; }");
  QGridLayout* pFaceLayout = new QGridLayout;
  pFaceLayout->setColumnStretch(0, 1);
  pFaceLayout->setColumnStretch(1, 1);

  int row = 0;
  generateComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kFaceLightingModel,   OD_T("Lighting model"),    facelightingModelList,       pFaceLayout, row++);
  generateComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kFaceLightingQuality, OD_T("Lighting quality"),  faceLightingQualityList,     pFaceLayout, row++);
  generateComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kFaceColorMode,       OD_T("Color mode"),        faceColorModeList,           pFaceLayout, row++);
  addFaceModifiers(visualStyleId, pFaceLayout, row++);
  generateLineEditDoubleParam(visualStyleId, OdTvVisualStyleOptions::kFaceOpacityAmount, OD_T("Opacity"), pFaceLayout, row++);
  generateLineEditDoubleParam(visualStyleId, OdTvVisualStyleOptions::kFaceSpecularAmount, OD_T("Specular"), pFaceLayout, row++);
  generateColorComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kFaceMonoColor, OD_T("Mono color"), pFaceLayout, row++);
  pFaceGroupBox->setLayout(pFaceLayout);

  // Edge properties
  QGroupBox* pEdgeGroupBox = new QGroupBox(tr("Edge properties"));
  pEdgeGroupBox->setStyleSheet("QGroupBox::title {color: blue; }");
  QGridLayout* pEdgeBoxLayout = new QGridLayout;
  pEdgeBoxLayout->setColumnStretch(0, 1);
  pEdgeBoxLayout->setColumnStretch(1, 1);

  row = 0;
  generateComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kEdgeModel, OD_T("Model"), edgeModelList, pEdgeBoxLayout, row++);
  addEdgeStyles(visualStyleId, pEdgeBoxLayout, row++);
  if(m_pPreview->isSceneGraphEnabled())
    generateColorComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kEdgeIntersectionColor, OD_T("Intersection color"), pEdgeBoxLayout, row++);
  generateColorComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kEdgeObscuredColor, OD_T("Obscured color"), pEdgeBoxLayout, row++);
  generateComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kEdgeObscuredLinePattern, OD_T("Obscured line pattern"),         edgeLinePattern, pEdgeBoxLayout, row++, 1);
  if (m_pPreview->isSceneGraphEnabled())
    generateComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kEdgeIntersectionLinePattern, OD_T("Intersection line pattern"), edgeLinePattern, pEdgeBoxLayout, row++, 1);
  generateLineEditDoubleParam(visualStyleId, OdTvVisualStyleOptions::kEdgeCreaseAngle, OD_T("Crease angle"), pEdgeBoxLayout, row++);
  addEdgeModifiers(visualStyleId, pEdgeBoxLayout, row++);
  generateColorComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kEdgeColorValue, OD_T("Color"), pEdgeBoxLayout, row++);
  generateLineEditDoubleParam(visualStyleId, OdTvVisualStyleOptions::kEdgeOpacityAmount, OD_T("Opacity"), pEdgeBoxLayout, row++);
  generateLineEditIntParam(visualStyleId, OdTvVisualStyleOptions::kEdgeWidthAmount, OD_T("Width"), pEdgeBoxLayout, row++);
  generateLineEditIntParam(visualStyleId, OdTvVisualStyleOptions::kEdgeOverhangAmount, OD_T("Overhang"), pEdgeBoxLayout, row++);
  generateComboBoxParam(visualStyleId, OdTvVisualStyleOptions::kEdgeJitterAmount, OD_T("Jitter amount"), edgeJitterAmount, pEdgeBoxLayout, row++, 1);
  generateLineEditIntParam(visualStyleId, OdTvVisualStyleOptions::kEdgeSilhouetteWidth, OD_T("Silhouette width"), pEdgeBoxLayout, row++);
  generateLineEditIntParam(visualStyleId, OdTvVisualStyleOptions::kEdgeHaloGapAmount, OD_T("Edge halo gap"), pEdgeBoxLayout, row++);
  pEdgeGroupBox->setLayout(pEdgeBoxLayout);

  // Display properties
  QGroupBox* pDisplayGroupBox = new QGroupBox(tr("Display properties"));
  pDisplayGroupBox->setStyleSheet("QGroupBox::title {color: blue; }");
  QGridLayout* pDisplayBoxLayout = new QGridLayout;
  pDisplayBoxLayout->setColumnStretch(0, 1);
  pDisplayBoxLayout->setColumnStretch(1, 1);
  row = 0;
  addDisplayStyles(visualStyleId, pDisplayBoxLayout, row++);
  generateCheckBoxParam(visualStyleId, OdTvVisualStyleOptions::kUseDrawOrder, OD_T("Use draw order"), pDisplayBoxLayout, row++);
  pDisplayGroupBox->setLayout(pDisplayBoxLayout);
  
  for (int i = 0; i < row; i++)
    pDisplayBoxLayout->setRowMinimumHeight(i, 18);

  m_pRightColumnVBoxLayout->addWidget(pFaceGroupBox);
  m_pRightColumnVBoxLayout->addWidget(pEdgeGroupBox);
  m_pRightColumnVBoxLayout->addWidget(pDisplayGroupBox);
}

void OdTvViewerVisualStylesDlg::updateControls(OdTvVisualStyleId visualStyleId)
{
  if (visualStyleId.isNull())
    return;

  m_bChanged = true;
  m_pRemoveBtn->setEnabled(!visualStyleId.openObject()->getDefault());
  m_visualStyleId = visualStyleId;

  if (m_pPreview)
    m_pPreview->setVisualStyle(visualStyleId);

  updateComboboxes(visualStyleId);
  updateIntLineEdits(visualStyleId);
  updateDoubleLineEdits(visualStyleId);
  updateCheckBoxes(visualStyleId);
  updateColors(visualStyleId);

  updateFaceModifiers(visualStyleId);
  updateEdgeStyles(visualStyleId);
  updateEdgeModifiers(visualStyleId);
  updateDisplayStyles(visualStyleId);

  m_bChanged = false;
}

void OdTvViewerVisualStylesDlg::updateComboboxes(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_comboboxList.size(); i++)
  {
    QComboBox *cb = m_comboboxList[i];
    if (cb)
    {
      QVariant var = cb->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = visualStyleId.openObject(OdTv::kForWrite);
      OdInt32 val = 0;
      OdTvResult rc = pVisualStyle->getOption((OdTvVisualStyleOptions::Options)var.toInt(), val);

      OdTvVisualStyleOptions::Options opt = (OdTvVisualStyleOptions::Options)var.toInt();
      if (opt == OdTvVisualStyleOptions::kEdgeJitterAmount || opt == OdTvVisualStyleOptions::kEdgeIntersectionLinePattern || opt == OdTvVisualStyleOptions::kEdgeObscuredLinePattern)
        val -= 1;

      if (rc == tvOk)
        cb->setCurrentIndex(val);
    }
  }
}

void OdTvViewerVisualStylesDlg::updateIntLineEdits(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_iLineEditList.size(); i++)
  {
    QLineEdit *le = m_iLineEditList[i];
    if (le)
    {
      QVariant var = le->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = visualStyleId.openObject(OdTv::kForWrite);
      OdInt32 oldVal = 0;
      OdTvResult rc = pVisualStyle->getOption((OdTvVisualStyleOptions::Options)var.toInt(), oldVal);
      if (rc == tvOk)
        le->setText(QString("%1").arg(oldVal));
    }
  }
}

void OdTvViewerVisualStylesDlg::updateDoubleLineEdits(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_dLineEditList.size(); i++)
  {
    QLineEdit *le = m_dLineEditList[i];
    if (le)
    {
      QVariant var = le->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = visualStyleId.openObject(OdTv::kForWrite);
      double oldVal = 0.;
      OdTvResult rc = pVisualStyle->getOption((OdTvVisualStyleOptions::Options)var.toInt(), oldVal);
      if (rc == tvOk)
        le->setText(QString("%1").arg(oldVal));
    }
  }
}

void OdTvViewerVisualStylesDlg::updateCheckBoxes(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_checkboxList.size(); i++)
  {
    QCheckBox* pChb = m_checkboxList[i];
    if (pChb)
    {
      OdString name = visualStyleId.openObject()->getName();
      QVariant var = pChb->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
      bool oldVal = false;
      OdTvVisualStyleOptions::Options opt = (OdTvVisualStyleOptions::Options)var.toInt();
      OdTvResult rc = pVisualStyle->getOption(opt, oldVal);
      if (rc == tvOk)
        pChb->setCheckState(oldVal ? Qt::Checked : Qt::Unchecked);
    }
  }
}

void OdTvViewerVisualStylesDlg::updateColors(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_colorComboboxList.size(); i++)
  {
    QComboBox *cb = m_colorComboboxList[i];
    if (cb)
    {
      QVariant var = cb->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = visualStyleId.openObject(OdTv::kForWrite);
      OdTvColorDef oldVal;
      OdTvResult rc = pVisualStyle->getOption((OdTvVisualStyleOptions::Options)var.toInt(), oldVal);
      if (rc == tvOk)
        getColorFromDef(cb, oldVal);
    }
  }
}

void OdTvViewerVisualStylesDlg::updateFaceModifiers(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_faceModifers.size(); i++)
  {
    QCheckBox* pChb = m_faceModifers[i];
    if (pChb)
    {
      QVariant var = pChb->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
      OdInt32 oldVal = 0;
      OdTvResult rc = pVisualStyle->getOption(OdTvVisualStyleOptions::kFaceModifiers, oldVal);
      if (rc == tvOk)
        pChb->setCheckState(GETBIT(oldVal, (OdTvVisualStyleOptions::FaceModifiers)var.toInt()) ? Qt::Checked : Qt::Unchecked);
    }
  }
}

void OdTvViewerVisualStylesDlg::updateEdgeStyles(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_edgeStyles.size(); i++)
  {
    QCheckBox* pChb = m_edgeStyles[i];
    if (pChb)
    {
      QVariant var = pChb->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
      OdInt32 oldVal = 0;
      OdTvResult rc = pVisualStyle->getOption(OdTvVisualStyleOptions::kEdgeStyles, oldVal);
      if (rc == tvOk)
        pChb->setCheckState(GETBIT(oldVal, (OdTvVisualStyleOptions::EdgeStyles)var.toInt()) ? Qt::Checked : Qt::Unchecked);
    }
  }
}

void OdTvViewerVisualStylesDlg::updateEdgeModifiers(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_edgeModifers.size(); i++)
  {
    QCheckBox* pChb = m_edgeModifers[i];
    if (pChb)
    {
      QVariant var = pChb->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
      OdInt32 oldVal = 0;
      OdTvResult rc = pVisualStyle->getOption(OdTvVisualStyleOptions::kEdgeModifiers, oldVal);
      if (rc == tvOk)
        pChb->setCheckState(GETBIT(oldVal, (OdTvVisualStyleOptions::EdgeModifiers)var.toInt()) ? Qt::Checked : Qt::Unchecked);
    }
  }
}

void OdTvViewerVisualStylesDlg::updateDisplayStyles(OdTvVisualStyleId visualStyleId)
{
  for (int i = 0; i < m_displayStyles.size(); i++)
  {
    QCheckBox* pChb = m_displayStyles[i];
    if (pChb)
    {
      QVariant var = pChb->property("PropertyName");
      OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
      OdInt32 oldVal = 0;
      OdTvResult rc = pVisualStyle->getOption(OdTvVisualStyleOptions::kDisplayStyles, oldVal);
      if (rc == tvOk)
        pChb->setCheckState(GETBIT(oldVal, (OdTvVisualStyleOptions::DisplayStyles)var.toInt()) ? Qt::Checked : Qt::Unchecked);
    }
  }
}

void OdTvViewerVisualStylesDlg::addFaceModifiers(const OdTvVisualStyleId& visualStyleId, QGridLayout *lt, int row)
{
  OdInt32 value = 0;
  if (visualStyleId.openObject(OdTv::kForWrite)->getOption(OdTvVisualStyleOptions::kFaceModifiers, value) != tvOk)
    return;

  QGroupBox *faceModifiersGb = new QGroupBox("Face modifiers");
  faceModifiersGb->setStyleSheet("QGroupBox::title {color: black; }");
  QGridLayout *grid = new QGridLayout;
  faceModifiersGb->setLayout(grid);
  for (int i = 0; i < faceModifierlList.size(); i++)
  {
    QCheckBox *cb = qobject_cast<QCheckBox*>(createCheckBox(GETBIT(value, i+1), false));
    cb->setText(faceModifierlList[i]);
    cb->setProperty("PropertyName", QVariant(i + 1));
    connect(cb, SIGNAL(stateChanged(int)), SLOT(applyFaceModifiers(int)));
    grid->addWidget(cb, 0, i);
    m_faceModifers.append(cb);
  }
  lt->addWidget(faceModifiersGb, row, 0, 1, 2);
}

void OdTvViewerVisualStylesDlg::addEdgeStyles(const OdTvVisualStyleId& visualStyleId, QGridLayout *lt, int row)
{
  OdInt32 value = 0;
  if (visualStyleId.openObject(OdTv::kForWrite)->getOption(OdTvVisualStyleOptions::kEdgeStyles, value) != tvOk)
    return;

  QGroupBox *estylesGb = new QGroupBox("Edge styles");
  estylesGb->setStyleSheet("QGroupBox::title {color: black; }");
  QGridLayout *grid = new QGridLayout;
  estylesGb->setLayout(grid);
  for (int i = 0; i < edgeStylesList.size(); i++)
  {
    if (!m_pPreview->isSceneGraphEnabled() && !edgeStylesList[i].compare("Intersection"))
      continue;
    QCheckBox *cb = qobject_cast<QCheckBox*>(createCheckBox(GETBIT(value, 1 << i), false));
    cb->setText(edgeStylesList[i]);
    cb->setProperty("PropertyName", QVariant(1 << i));
    connect(cb, SIGNAL(stateChanged(int)), SLOT(applyEdgeStyles(int)));
    grid->addWidget(cb, 0, i);
    m_edgeStyles.append(cb);
  }
  lt->addWidget(estylesGb, row, 0, 1, 2);
}

void OdTvViewerVisualStylesDlg::addEdgeModifiers(const OdTvVisualStyleId& visualStyleId, QGridLayout *lt, int row)
{
  OdInt32 value = 0;
  if (visualStyleId.openObject(OdTv::kForWrite)->getOption(OdTvVisualStyleOptions::kEdgeModifiers, value) != tvOk)
    return;

  QGroupBox *emodGb = new QGroupBox("Edge modifiers");
  emodGb->setStyleSheet("QGroupBox::title {color: black; }");
  QGridLayout *grid = new QGridLayout;
  emodGb->setLayout(grid);
  int r = 0, c = 0;
  for (int i = 0; i < edgeModifiersList.size(); i++)
  {
    QCheckBox *cb = qobject_cast<QCheckBox*>(createCheckBox(GETBIT(value, 1 << i), false));
    cb->setText(edgeModifiersList[i]);
    cb->setProperty("PropertyName", QVariant(1 << i));
    connect(cb, SIGNAL(stateChanged(int)), SLOT(applyEdgeModifiers(int)));

    grid->addWidget(cb, r, c);
    c++;
    if ((i+1) % 3 == 0)
    {
      r++;
      c = 0;
    }
    m_edgeModifers.append(cb);
  }
  lt->addWidget(emodGb, row, 0, 1, 2);
}

void OdTvViewerVisualStylesDlg::addDisplayStyles(const OdTvVisualStyleId& visualStyleId, QGridLayout *lt, int row)
{
  OdInt32 value = 0;
  if (visualStyleId.openObject(OdTv::kForWrite)->getOption(OdTvVisualStyleOptions::kDisplayStyles, value) != tvOk)
    return;

  QGroupBox *dstylesGb = new QGroupBox("Display styles");
  dstylesGb->setStyleSheet("QGroupBox::title {color: black; }");
  QGridLayout *grid = new QGridLayout;
  dstylesGb->setLayout(grid);
  for (int i = 0; i < displayStylesList.size(); i++)
  {
    QCheckBox *cb = qobject_cast<QCheckBox*>(createCheckBox(GETBIT(value, 1 << i), false));
    cb->setText(displayStylesList[i]);
    cb->setProperty("PropertyName", QVariant(1 << i));
    connect(cb, SIGNAL(stateChanged(int)), SLOT(applyDisplayStyles(int)));
    grid->addWidget(cb, 0, i);
    m_displayStyles.append(cb);
  }
  lt->addWidget(dstylesGb, row, 0, 1, 2);
}

void OdTvViewerVisualStylesDlg::generateComboBoxParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, const QStringList& list,
                                                      QGridLayout *lt, int row, int startInd)
{
  OdInt32 value = 0;
  if (visualStyleId.openObject()->getOption(opt, value) != tvOk || value >= list.size())
    return;

  QLabel* pLabel = new QLabel(tr(title));
  QComboBox* pComboBox = qobject_cast<QComboBox*>(createCombobox(list, value - startInd, false));
  pComboBox->setProperty("PropertyName", QVariant((int)opt));
  connect(pComboBox, SIGNAL(activated(const QString&)), SLOT(applyComboBox(const QString&)));

  lt->setRowStretch(row, 0);
  lt->addWidget(pLabel, row, 0);
  lt->addWidget(pComboBox, row, 1);
  m_comboboxList.append(pComboBox);

  OdTvViewerVisualStylesGuidedCtrls ctrls(OdTvViewerVisualStylesGuidedCtrls::kBit);
  ctrls.addControl(pLabel);
  ctrls.addControl(pComboBox);

  if (opt == OdTvVisualStyleOptions::kEdgeObscuredLinePattern)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kObscured);
    m_edgeStylesGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeIntersectionLinePattern)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kIntersection);
    m_edgeStylesGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeJitterAmount)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kEdgeJitter);
    m_edgeModifiersGuidedCtrls.append(ctrls);
  }
}

void OdTvViewerVisualStylesDlg::generateLineEditIntParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, QGridLayout *lt, int row)
{
  OdInt32 value = 0;
  if (visualStyleId.openObject(OdTv::kForWrite)->getOption(opt, value) != tvOk)
    return;

  QLabel* pLabel = new QLabel(tr(title));
  QLineEdit* pLineEdit = qobject_cast<QLineEdit*>(createLineEdit(QString("%1").arg(value)));
  pLineEdit->setValidator(new QIntValidator(-2147483647, 2147483647, this));
  pLineEdit->setProperty("PropertyName", QVariant((int)opt));
  OdTvQPushButton *pApplyBtn = new OdTvQPushButton("", pLineEdit);
  pApplyBtn->setIcon(m_editBtnIcn);
  pApplyBtn->setProperty("PropertyName", QVariant((int)opt));
  connect(pLineEdit, SIGNAL(returnPressed()), pApplyBtn, SIGNAL(clicked()));
  connect(pApplyBtn, SIGNAL(clicked()), SLOT(applyInt()));

  lt->addWidget(pLabel, row, 0);
  QHBoxLayout *hLt = new QHBoxLayout;
  hLt->addWidget(pLineEdit);
  hLt->addWidget(pApplyBtn);
  lt->addLayout(hLt, row, 1);
  m_iLineEditList.append(pLineEdit);

  OdTvViewerVisualStylesGuidedCtrls ctrls(OdTvViewerVisualStylesGuidedCtrls::kBit);
  ctrls.addControl(pLabel);
  ctrls.addControl(pLineEdit);
  ctrls.addControl(pApplyBtn);

  if (opt == OdTvVisualStyleOptions::kEdgeSilhouetteWidth)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kSilhouette);
    m_edgeStylesGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeOverhangAmount)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kEdgeOverhang);
    m_edgeModifiersGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeWidthAmount)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kEdgeWidth);
    m_edgeModifiersGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeHaloGapAmount)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kEdgeHaloGap);
    m_edgeModifiersGuidedCtrls.append(ctrls);
  }
}

void OdTvViewerVisualStylesDlg::generateLineEditDoubleParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, QGridLayout *lt, int row)
{
  double value = 0.;
  if (visualStyleId.openObject(OdTv::kForWrite)->getOption(opt, value) != tvOk)
    return;

  QLabel* pLabel = new QLabel(tr(title));
  QLineEdit* pLineEdit = qobject_cast<QLineEdit*>(createLineEdit(QString("%1").arg(value)));
  pLineEdit->setValidator(new QDoubleValidator(0, 1, 9, this));
  pLineEdit->setProperty("PropertyName", QVariant((int)opt));
  OdTvQPushButton *pApplyBtn = new OdTvQPushButton("", pLineEdit);
  pApplyBtn->setIcon(m_editBtnIcn);
  pApplyBtn->setProperty("PropertyName", QVariant((int)opt));
  connect(pLineEdit, SIGNAL(returnPressed()), pApplyBtn, SIGNAL(clicked()));
  connect(pApplyBtn, SIGNAL(clicked()), SLOT(applyDouble()));

  lt->addWidget(pLabel, row, 0);
  QHBoxLayout* hBoxLayout = new QHBoxLayout;
  hBoxLayout->addWidget(pLineEdit);
  hBoxLayout->addWidget(pApplyBtn);
  lt->addLayout(hBoxLayout, row, 1);
  m_dLineEditList.append(pLineEdit);

  OdTvViewerVisualStylesGuidedCtrls ctrls(OdTvViewerVisualStylesGuidedCtrls::kBit);
  ctrls.addControl(pLabel);
  ctrls.addControl(pLineEdit);
  ctrls.addControl(pApplyBtn);

  if (opt == OdTvVisualStyleOptions::kFaceOpacityAmount)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kFaceOpacityFlag);
    m_faceModifiersGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kFaceSpecularAmount)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kSpecularFlag);
    m_faceModifiersGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeCreaseAngle)
  {
    ctrls.setType(OdTvViewerVisualStylesGuidedCtrls::kValues);
    ctrls.addValue(OdTvVisualStyleOptions::kFacetEdges);
    m_edgeModelsGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeOpacityAmount)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kEdgeOpacity);
    m_edgeModifiersGuidedCtrls.append(ctrls);
  }
}

void OdTvViewerVisualStylesDlg::generateColorComboBoxParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, QGridLayout *lt, int row)
{
  OdTvColorDef color;
  if (visualStyleId.openObject(OdTv::kForWrite)->getOption(opt, color) != tvOk)
    return;

  QLabel* pLabel = new QLabel(tr(title));
  QComboBox *colorCb = qobject_cast<QComboBox*>(createColorComboBox(color));
  colorCb->setProperty("PropertyName", QVariant((int)opt));
  connect(colorCb, SIGNAL(activated(const QString&)), SLOT(applyColorComboBox(const QString&)));

  lt->addWidget(pLabel, row, 0);
  lt->addWidget(colorCb, row, 1);
  m_colorComboboxList.append(colorCb);

  OdTvViewerVisualStylesGuidedCtrls ctrls(OdTvViewerVisualStylesGuidedCtrls::kBit);
  ctrls.addControl(pLabel);
  ctrls.addControl(colorCb);

  if (opt == OdTvVisualStyleOptions::kEdgeObscuredColor)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kObscured);
    m_edgeStylesGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeIntersectionColor)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kIntersection);
    m_edgeStylesGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kEdgeColorValue)
  {
    ctrls.setBit(OdTvVisualStyleOptions::kEdgeColor);
    m_edgeModifiersGuidedCtrls.append(ctrls);
  }
  else if (opt == OdTvVisualStyleOptions::kFaceMonoColor)
  {
    ctrls.setType(OdTvViewerVisualStylesGuidedCtrls::kValues);
    ctrls.addValue(OdTvVisualStyleOptions::kMono);
    ctrls.addValue(OdTvVisualStyleOptions::kTint);
    m_faceColorModeGuidedCtrls.append(ctrls);
  }
}

void OdTvViewerVisualStylesDlg::generateCheckBoxParam(const OdTvVisualStyleId& visualStyleId, OdTvVisualStyleOptions::Options opt, const OdString& title, QGridLayout *lt, int row)
{
  bool value = false;
  if (visualStyleId.openObject(OdTv::kForWrite)->getOption(opt, value) != tvOk)
    return;

  QLabel* pLabel = new QLabel(tr(title));
  QCheckBox* pCheckBox = qobject_cast<QCheckBox*>(createCheckBox(value));
  pCheckBox->setProperty("PropertyName", QVariant((int)opt));
  connect(pCheckBox, SIGNAL(stateChanged(int)), SLOT(applyCheckBox(int)));

  lt->addWidget(pLabel, row, 0);
  lt->addWidget(pCheckBox, row, 1);
  m_checkboxList.append(pCheckBox);
}

QObject* OdTvViewerVisualStylesDlg::createCheckBox(bool flag, bool readOnly)
{
  QCheckBox *cb = new QCheckBox;
  cb->setCheckState(flag ? Qt::Checked : Qt::Unchecked);
  if (readOnly)
  {
    cb->setAttribute(Qt::WA_TransparentForMouseEvents);
    cb->setFocusPolicy(Qt::NoFocus);
  }
  return cb;
}

QObject* OdTvViewerVisualStylesDlg::createCombobox(QStringList items, int currentIndex, bool readOnly)
{
  QComboBox *cb = new QComboBox(this);
  cb->setFocusPolicy(Qt::NoFocus);
  cb->addItems(items);
  cb->setCurrentIndex(currentIndex);
  if (readOnly)
  {
    cb->setStyleSheet("QComboBox::drop-down {border-width: 0px;} QComboBox::down-arrow {image: url(noimg); border-width: 0px;}");
    QLineEdit *le = new QLineEdit;
    cb->setLineEdit(le);
    cb->lineEdit()->setReadOnly(true);
  }
  setComboBoxDropDownListWidth(cb);
  return cb;
}

QObject* OdTvViewerVisualStylesDlg::createCombobox(QStringList items, const QString& curText, bool readOnly)
{
  QComboBox *cb = new QComboBox(this);
  cb->setFocusPolicy(Qt::NoFocus);
  cb->addItems(items);
  cb->setCurrentText(curText);
  if (readOnly)
  {
    cb->setStyleSheet("QComboBox::drop-down {border-width: 0px;} QComboBox::down-arrow {image: url(noimg); border-width: 0px;}");
    QLineEdit *le = new QLineEdit;
    cb->setLineEdit(le);
    cb->lineEdit()->setReadOnly(true);
  }
  setComboBoxDropDownListWidth(cb);
  return cb;
}

QObject* OdTvViewerVisualStylesDlg::createLineEdit(const QString& text, bool readOnly, QWidget * parent)
{
  QLineEdit* pEdit = new QLineEdit(parent);
  pEdit->setReadOnly(readOnly);
  pEdit->setText(text);
  pEdit->setCursorPosition(0);
  pEdit->setStyleSheet(QString("border: 1px solid"));
  return pEdit;
}

QObject* OdTvViewerVisualStylesDlg::createColorComboBox(const OdTvColorDef& color)
{
  QComboBox *comboBox = new QComboBox(this);
  comboBox->setFocusPolicy(Qt::NoFocus);
  int size = comboBox->style()->pixelMetric(QStyle::PM_SmallIconSize);
  QPixmap pixmap(size, size - 5);
  fillColorNamesList(comboBox);
  getColorFromDef(comboBox, color);

  // adjust drop-down list width for combobox
  setComboBoxDropDownListWidth(comboBox, pixmap.width());

  return comboBox;
}

QColor OdTvViewerVisualStylesDlg::getQColorFromDef(const OdTvColorDef& color) const
{
  OdUInt8 r, g, b;
  color.getColor(r, g, b);
  QColor qColor;
  qColor.setRgb(r, g, b);
  return qColor;
}

// method for add colors from list to combobox
void OdTvViewerVisualStylesDlg::fillColorNamesList(QComboBox *cb)
{
  int size = cb->style()->pixelMetric(QStyle::PM_SmallIconSize);
  QPixmap pixmap(size, size - 5);

  int con = 0;
  foreach(const QString &colorName, m_colorNamesList)
  {
    if (con == 4)
      cb->insertSeparator(con);

    cb->addItem(colorName);

    if (!colorName.compare("None") || !colorName.compare("ByBlock") || !colorName.compare("ByLayer") || !colorName.compare("Select color..."))
    {
      con++;
      continue;
    }

    QColor col = QColor(colorName);
    if (!col.isValid())
    {
      // parse string
      QRegExp rx("[,]");
      QStringList list = colorName.split(rx, QString::SkipEmptyParts);
      if (list.size() >= 3)
      {
        OdUInt8 r = list.at(0).toUInt(), g = list.at(1).toUInt(), b = list.at(2).toUInt();
        QColor newColor = QColor(r, g, b);
        if (newColor.isValid())
        {
          pixmap.fill(newColor);
          cb->setItemData(con, pixmap, Qt::DecorationRole);
          cb->setItemData(con, newColor, Qt::UserRole);
        }
      }
      else if (colorName.compare("ByBlock") && colorName.compare("ByLayer") && !colorName.isEmpty())
      {
        OdTvGsDeviceId tvAppDeviceId = m_pActiveView->getTvDeviceId();
        if (!tvAppDeviceId.isNull())
        {
          OdTvGsDevicePtr pAppDevice = tvAppDeviceId.openObject();
          int numColors = 0;
          const OdUInt32 * colors = pAppDevice->getLogicalPalette(numColors);
          int iColor = colorName.toInt();
          OdUInt32 incoloror = colors[iColor];
          QColor clr = QColor(ODGETRED(incoloror), ODGETGREEN(incoloror), ODGETBLUE(incoloror));
          if (clr.isValid())
          {
            pixmap.fill(clr);
            int ind = cb->findText(colorName);
            cb->setItemData(ind, pixmap, Qt::DecorationRole);
          }
        }
      }

      con++;
      continue;
    }

    pixmap.fill(col);
    cb->setItemData(con, pixmap, Qt::DecorationRole);
    cb->setItemData(con, col, Qt::UserRole);
    con++;
  }
}

// method for setting current color in combobox
void OdTvViewerVisualStylesDlg::getColorFromDef(QComboBox *cb, const OdTvColorDef& color)
{
  int size = cb->style()->pixelMetric(QStyle::PM_SmallIconSize);
  QPixmap pixmap(size, size - 5);

  switch (color.getType())
  {
  case OdTvColorDef::kDefault:
    cb->setCurrentText("None");
    break;
  case OdTvColorDef::kColor:
  {
    QColor clr = getQColorFromDef(color);
    int n = cb->findData(clr, int(Qt::UserRole));
    if (n < 0)
    {
      QString colorRgbStr = QString("%1,%2,%3").arg(clr.red()).arg(clr.green()).arg(clr.blue());
      if (!m_colorNamesList.contains(colorRgbStr))
      {
        m_colorNamesList.push_back(colorRgbStr);
        m_colorNamesList.swap(m_colorNamesList.size() - 1, m_colorNamesList.size() - 2);
        // index after none, byBlock, byLayer and before SelectColor
        int insertIndex = 3;
        cb->insertItem(insertIndex, colorRgbStr);
      }

      pixmap.fill(clr);
      int ind = cb->findText(colorRgbStr);
      cb->setItemData(ind, pixmap, Qt::DecorationRole);
      cb->setItemData(ind, clr, Qt::UserRole);
      cb->setCurrentIndex(ind);
    }
    else
      cb->setCurrentIndex(n);

    break;
  }
  case OdTvColorDef::kInherited:
  {
    if (color.getInheritedColor() == OdTv::kByBlock)
      cb->setCurrentText("ByBlock");
    else
      cb->setCurrentText("ByLayer");
    break;
  }
  case OdTvColorDef::kIndexed:
  {
    QString str = QString("%1").arg(color.getIndexedColor());
    int n = cb->findText(str);
    if (n < 0)
    {
      if (!m_colorNamesList.contains(str))
      {
        m_colorNamesList.push_back(str);
        m_colorNamesList.swap(m_colorNamesList.size() - 1, m_colorNamesList.size() - 2);
        cb->insertItem(cb->count() - 2, str);
      }

      OdTvGsDeviceId tvAppDeviceId = m_pActiveView->getTvDeviceId();
      if (!tvAppDeviceId.isNull())
      {
        OdTvGsDevicePtr pAppDevice = tvAppDeviceId.openObject();
        int numColors;
        const OdUInt32 * colors = pAppDevice->getLogicalPalette(numColors);
        OdUInt32 incoloror = colors[color.getIndexedColor()];
        QColor clr = QColor(ODGETRED(incoloror), ODGETGREEN(incoloror), ODGETBLUE(incoloror));
        pixmap.fill(clr);

        int ind = cb->findText(str);
        cb->setItemData(ind, pixmap, Qt::DecorationRole);
        cb->setItemData(ind, clr, Qt::UserRole);
        cb->setCurrentIndex(ind);
      }
    }
    else
    {
      cb->setCurrentIndex(n);
    }
    break;
  }
  }
}

// method for return old color to combobox(if selected color was canceled)
void OdTvViewerVisualStylesDlg::getOldColor(QComboBox *cb, const OdTvColorDef& oldColor)
{
  switch (oldColor.getType())
  {
  default:
    break;
  case OdTvColorDef::kDefault:
    cb->setCurrentText("None");
    break;
  case OdTvColorDef::kColor:
  {
    QColor color = getQColorFromDef(oldColor);
    int ind = cb->findText(QString("%1,%2,%3").arg(color.red()).arg(color.green()).arg(color.blue()));
    if (ind < 0)
    {
      for (int i = 0; i < cb->count(); i++)
        if (!color.name().compare(QColor(cb->itemText(i)).name()))
        {
          cb->setCurrentIndex(i);
          break;
        }
    }
    else
      cb->setCurrentIndex(ind);

    break;
  }
  case OdTvColorDef::kInherited:
  {
    if (oldColor.getInheritedColor() == OdTv::kByBlock)
      cb->setCurrentText("ByBlock");
    else
      cb->setCurrentText("ByLayer");

    break;
  }
  case OdTvColorDef::kIndexed:
  {
    cb->setCurrentText(QString("%1").arg(oldColor.getIndexedColor()));
    break;
  }
  }
}

void OdTvViewerVisualStylesDlg::setColorForComboBox(QComboBox *cb, const QColor& color)
{
  int n = cb->findData(color, int(Qt::UserRole));
  int size = cb->style()->pixelMetric(QStyle::PM_SmallIconSize);
  QPixmap pixmap(size, size - 5);
  if (n < 0)
  {
    QString colorRgbStr = QString("%1,%2,%3").arg(color.red()).arg(color.green()).arg(color.blue());
    if (!m_colorNamesList.contains(colorRgbStr))
    {
      m_colorNamesList.push_back(colorRgbStr);
      m_colorNamesList.swap(m_colorNamesList.size() - 1, m_colorNamesList.size() - 2);
      // index after none, byBlock, byLayer and before SelectColor
      int insertIndex = 3;
      cb->insertItem(insertIndex, colorRgbStr);
    }
    pixmap.fill(color);
    int ind = cb->findText(colorRgbStr);
    cb->setItemData(ind, pixmap, Qt::DecorationRole);
    cb->setItemData(ind, color, Qt::UserRole);
    cb->setCurrentIndex(ind);
  }
  else
    cb->setCurrentIndex(n);
}

void OdTvViewerVisualStylesDlg::setComboBoxDropDownListWidth(QComboBox * cb, int pixmapWidth)
{
  // adjust drop-down list width for combobox
  int scroll = cb->count() <= cb->maxVisibleItems() ? 0 :
    QApplication::style()->pixelMetric(QStyle::PM_ScrollBarExtent);
  int max = 0;
  for (int i = 0; i < cb->count(); i++)
  {
    int width = cb->view()->fontMetrics().width(cb->itemText(i));
    if (max < width)
      max = width;
  }

  int curWidth = cb->width();
  int newWidth = max + scroll + cb->iconSize().width() + pixmapWidth;

  if (curWidth < newWidth)
    cb->view()->setFixedWidth(newWidth);
}

void OdTvViewerVisualStylesDlg::updateAllGuidedControls()
{
  updateGuidedControls(OdTvVisualStyleOptions::kFaceModifiers);
  updateGuidedControls(OdTvVisualStyleOptions::kFaceColorMode);
  updateGuidedControls(OdTvVisualStyleOptions::kEdgeStyles);
  updateGuidedControls(OdTvVisualStyleOptions::kEdgeModifiers);
  updateGuidedControls(OdTvVisualStyleOptions::kEdgeModel);
}

void OdTvViewerVisualStylesDlg::updateGuidedControls(OdTvVisualStyleOptions::Options opt)
{
  if (m_visualStyleId.isNull())
    return;

  OdTvVisualStylePtr pV = m_visualStyleId.openObject();

  OdInt32 iVal;
  pV->getOption(opt, iVal);

  QList<OdTvViewerVisualStylesGuidedCtrls> *pList = NULL;
  switch (opt)
  {
  case OdTvVisualStyleOptions::kFaceModifiers:
    pList = &m_faceModifiersGuidedCtrls;
    break;
  case OdTvVisualStyleOptions::kFaceColorMode:
    pList = &m_faceColorModeGuidedCtrls;
    break;
  case OdTvVisualStyleOptions::kEdgeStyles:
    pList = &m_edgeStylesGuidedCtrls;
    break;
  case OdTvVisualStyleOptions::kEdgeModifiers:
    pList = &m_edgeModifiersGuidedCtrls;
    break;
  case OdTvVisualStyleOptions::kEdgeModel:
    pList = &m_edgeModelsGuidedCtrls;
    break;
  }

  foreach(OdTvViewerVisualStylesGuidedCtrls ctrls, *pList)
    ctrls.updateState(iVal);
}

void OdTvViewerVisualStylesDlg::applyComboBox(const QString& text)
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  QComboBox *cb = qobject_cast<QComboBox*>(sender());
  if (cb)
  {
    QVariant var = cb->property("PropertyName");
    OdTvVisualStyleOptions::Options opt = (OdTvVisualStyleOptions::Options)var.toInt();

    OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
    OdInt32 oldVal = 0;
    OdTvResult rc = pVisualStyle->getOption(opt, oldVal);

    OdInt32 curInd = (OdInt32)cb->currentIndex();
    if (opt == OdTvVisualStyleOptions::kEdgeJitterAmount || opt == OdTvVisualStyleOptions::kEdgeIntersectionLinePattern || opt == OdTvVisualStyleOptions::kEdgeObscuredLinePattern)
      curInd += 1;

    if (rc != tvOk || curInd == oldVal)
      return;

    pVisualStyle->setOption(opt, curInd);
    m_pPreview->updatePreview(true);

    if (opt == OdTvVisualStyleOptions::kEdgeModel)
      updateGuidedControls(opt);
    else if (opt == OdTvVisualStyleOptions::kFaceColorMode)
      updateGuidedControls(opt);
  }
}

void OdTvViewerVisualStylesDlg::applyDouble()
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  OdTvQPushButton* pBtn = qobject_cast<OdTvQPushButton*>(sender());
  if (pBtn)
  {
    QVariant var = pBtn->property("PropertyName");
    OdTvVisualStyleOptions::Options opt = (OdTvVisualStyleOptions::Options)var.toInt();

    OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
    double oldVal = 0;
    OdTvResult rc = pVisualStyle->getOption(opt, oldVal);
    if (rc != tvOk || pBtn->getValue() == oldVal)
      return;

    pVisualStyle->setOption(opt, pBtn->getValue());
    m_pPreview->updatePreview(true);
  }
}

void OdTvViewerVisualStylesDlg::applyInt()
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  OdTvQPushButton* pBtn = qobject_cast<OdTvQPushButton*>(sender());
  if (pBtn)
  {
    QVariant var = pBtn->property("PropertyName");
    OdTvVisualStyleOptions::Options opt = (OdTvVisualStyleOptions::Options)var.toInt();

    OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
    OdInt32 oldVal = 0;
    OdTvResult rc = pVisualStyle->getOption(opt, oldVal);
    if (rc != tvOk || (OdInt32)pBtn->getValue() == oldVal)
      return;

    pVisualStyle->setOption(opt, (OdInt32)pBtn->getValue());
    m_pPreview->updatePreview(true);
  }
}

void OdTvViewerVisualStylesDlg::applyCheckBox(int state)
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  QCheckBox* pChb = qobject_cast<QCheckBox*>(sender());
  if (pChb)
  {
    QVariant var = pChb->property("PropertyName");
    OdTvVisualStyleOptions::Options opt = (OdTvVisualStyleOptions::Options)var.toInt();

    OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
    bool oldVal = false;
    OdTvResult rc = pVisualStyle->getOption(opt, oldVal);
    if (rc != tvOk || (state == Qt::Checked) == oldVal)
      return;

    pVisualStyle->setOption(opt, state == Qt::Checked);
    m_pPreview->updatePreview(true);
  }
}

void OdTvViewerVisualStylesDlg::applyFaceModifiers(int state)
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  QCheckBox* pChb = qobject_cast<QCheckBox*>(sender());
  if (pChb)
  {
    QVariant var = pChb->property("PropertyName");
    OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
    OdInt32 oldVal = 0;
    OdTvResult rc = pVisualStyle->getOption(OdTvVisualStyleOptions::kFaceModifiers, oldVal);
    if (rc != tvOk)
      return;

    SETBIT(oldVal, (OdTvVisualStyleOptions::FaceModifiers)var.toInt(), state == Qt::Checked);

    pVisualStyle->setOption(OdTvVisualStyleOptions::kFaceModifiers, oldVal);
    m_pPreview->updatePreview(true);
    updateGuidedControls(OdTvVisualStyleOptions::kFaceModifiers);
  }
}

void OdTvViewerVisualStylesDlg::applyEdgeStyles(int state)
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  QCheckBox* pChb = qobject_cast<QCheckBox*>(sender());
  if (pChb)
  {
    QVariant var = pChb->property("PropertyName");
    OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
    OdInt32 oldVal = 0;
    OdTvResult rc = pVisualStyle->getOption(OdTvVisualStyleOptions::kEdgeStyles, oldVal);
    if (rc != tvOk)
      return;

    SETBIT(oldVal, (OdTvVisualStyleOptions::EdgeStyles)var.toInt(), state == Qt::Checked);

    pVisualStyle->setOption(OdTvVisualStyleOptions::kEdgeStyles, oldVal);
    m_pPreview->updatePreview(true);
    updateGuidedControls(OdTvVisualStyleOptions::kEdgeStyles);
  }
}

void OdTvViewerVisualStylesDlg::applyEdgeModifiers(int state)
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  QCheckBox* pChb = qobject_cast<QCheckBox*>(sender());
  if (pChb)
  {
    QVariant var = pChb->property("PropertyName");
    OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
    OdInt32 oldVal = 0;
    OdTvResult rc = pVisualStyle->getOption(OdTvVisualStyleOptions::kEdgeModifiers, oldVal);
    if (rc != tvOk)
      return;

    SETBIT(oldVal, (OdTvVisualStyleOptions::EdgeModifiers)var.toInt(), state == Qt::Checked);

    pVisualStyle->setOption(OdTvVisualStyleOptions::kEdgeModifiers, oldVal);
    m_pPreview->updatePreview(true);
  }
  updateGuidedControls(OdTvVisualStyleOptions::kEdgeModifiers);
}

void OdTvViewerVisualStylesDlg::applyDisplayStyles(int state)
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  QCheckBox* pChb = qobject_cast<QCheckBox*>(sender());
  if (pChb)
  {
    QVariant var = pChb->property("PropertyName");
    OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
    OdInt32 oldVal = 0;
    OdTvResult rc = pVisualStyle->getOption(OdTvVisualStyleOptions::kDisplayStyles, oldVal);
    if (rc != tvOk)
      return;

    SETBIT(oldVal, (OdTvVisualStyleOptions::DisplayStyles)var.toInt(), state == Qt::Checked);

    pVisualStyle->setOption(OdTvVisualStyleOptions::kDisplayStyles, oldVal);
    m_pPreview->updatePreview(true);
  }
}

void OdTvViewerVisualStylesDlg::applyColorComboBox(const QString& text)
{
  if (!m_pPreview || m_pPreview->getVisualStyleId().isNull() || m_bChanged)
    return;

  QComboBox *cb = qobject_cast<QComboBox*>(sender());
  if (!cb)
    return;

  QVariant var = cb->property("PropertyName");
  OdTvVisualStyleOptions::Options opt = (OdTvVisualStyleOptions::Options)var.toInt();

  OdTvVisualStylePtr pVisualStyle = m_pPreview->getVisualStyleId().openObject(OdTv::kForWrite);
  OdTvColorDef oldColor;
  OdTvResult rc = pVisualStyle->getOption(opt, oldColor);
  if (rc != tvOk)
    return;

  if (!text.compare("Select color..."))
  {
    QColor color = QColorDialog::getColor();
    if (!color.isValid())
    {
      // color not valid need return previous color
      getOldColor(cb, oldColor);
      return;
    }
    OdTvColorDef newColor(color.red(), color.green(), color.blue());
    if (newColor == oldColor)
      return;

    pVisualStyle->setOption(opt, newColor);
    // add color to checkbox and colorNameList
    setColorForComboBox(cb, color);
  }
  else if (!text.compare("None"))
  {
    OdTvColorDef newColor;
    newColor.setDefault();
    pVisualStyle->setOption(opt, newColor);
  }
  else if (!text.compare("ByBlock"))
  {
    if (oldColor.getInheritedColor() == OdTv::kByBlock)
      return;

    pVisualStyle->setOption(opt, (OdTvColorDef)OdTv::kByBlock);
  }
  else if (!text.compare("ByLayer"))
  {
    if (oldColor.getInheritedColor() == OdTv::kByLayer)
      return;

    pVisualStyle->setOption(opt, (OdTvColorDef)OdTv::kByLayer);
  }
  else
  {
    QColor color = QColor(text);
    if (color.isValid())
    {
      OdTvColorDef newColor(color.red(), color.green(), color.blue());
      if (newColor == oldColor)
        return;

      pVisualStyle->setOption(opt, newColor);
    }
    else
    {
      // parse string
      QRegExp rx("[,]");
      QStringList list = text.split(rx, QString::SkipEmptyParts);
      if (list.size() >= 3)
      {
        OdUInt8 r = list.at(0).toUInt(), g = list.at(1).toUInt(), b = list.at(2).toUInt();
        OdTvColorDef newColor(r, g, b);
        if (newColor == oldColor)
          return;

        pVisualStyle->setOption(opt, newColor);
      }
      else
      {
        int ind = text.toInt();
        OdTvColorDef newColor(ind);
        if (newColor == oldColor)
          return;

        pVisualStyle->setOption(opt, newColor);
      }
    }
  }

  m_pPreview->updatePreview(true);
}

void OdTvViewerVisualStylesDlg::addVisualStyle()
{
  QListWidgetItem* currentItem = m_pVisualStylesList->currentItem();

  if (currentItem == NULL)
    return;

  OdTvResult rc = tvOk;
  OdTvVisualStyleId visualStyleForCopyId = m_dbId.openObject(OdTv::kForWrite)->findVisualStyle(toOdString(currentItem->text()), &rc);

  // Try get name while in would not be correct
  bool bNeedInputName = true;
  while (bNeedInputName)
  {
    bool ok;
    QString qName = QInputDialog::getText(this, tr("Add visual style"), tr("Enter visual style name"), QLineEdit::Normal, "", &ok);

    if (ok)
    {
      // Try add new visual style
      OdTvResult res = tvInternal;
      OdTvVisualStyleId newVisualStyleId;
      if (!qName.isEmpty())
      {
        newVisualStyleId = m_pPreview->getTvDatabaseId().openObject(OdTv::kForWrite)->createVisualStyle(toOdString(qName), visualStyleForCopyId, &res);
      }

      if (res == tvOk)
      {
        bNeedInputName = false;
        QListWidgetItem* newItem = new QListWidgetItem(qName);
        m_pVisualStylesList->addItem(newItem);
        m_pVisualStylesList->setCurrentItem(newItem);
        m_visualStyleId = newVisualStyleId;
        m_pPreview->updatePreview(true);
      }
      else if (res != tvOk || qName.isEmpty()) // Show alert if was incorrect or empty name
      {
        QMessageBox alert;
        alert.setText(tr("Incorrect visual style name"));
        alert.exec();
      }
 
    }
    else if (!ok)
    {
      bNeedInputName = false;
    }
  }
}

void OdTvViewerVisualStylesDlg::removeVisualStyle()
{
  m_pActiveView->setVisualStyle(OdTvVisualStyleId());

  if (!m_visualStyleId.isNull())
  {
    OdString name = m_pPreview->getVisualStyleId().openObject()->getName();
    OdTvDatabasePtr pDb = m_dbId.openObject(OdTv::kForWrite);
    OdTvVisualStyleId removeVsId = pDb->findVisualStyle(name);
    if (!removeVsId.isNull())
      pDb->removeVisualStyle(removeVsId);
    m_pPreview->getTvDatabaseId().openObject(OdTv::kForWrite)->removeVisualStyle(m_pPreview->getVisualStyleId());
  }

  m_pVisualStylesList->takeItem(m_pVisualStylesList->currentRow());
}
