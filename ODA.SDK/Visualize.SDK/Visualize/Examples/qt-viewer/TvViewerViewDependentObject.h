/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef OD_TV_VIEWDEPENDENTOBJECT_H
#define OD_TV_VIEWDEPENDENTOBJECT_H

#include "Tv.h"
#include "TvEntity.h"
#include "TvDatabase.h"

#include "StringArray.h"

/** \details
This interface for objects which do some actions after changing view parameters
*/
class OdTvViewerViewDependentObject
{
public:

  OdTvViewerViewDependentObject(int iViewport) : m_iParentViewport(iViewport) {}
  virtual void update(const OdTvGsViewId& tvView) {};

  int getViewport() { return m_iParentViewport; }


private:
  //index of the parent viewport (-1) means that the caller should decide about the view
  int m_iParentViewport;

};
typedef OdVector<OdTvViewerViewDependentObject*, OdMemoryAllocator<OdTvViewerViewDependentObject*> > OdTvViewerViewDependentObjectArray;

/** \details
This class implements the 3D coordinate system which depends in view
*/
class OdTv3DCoordinateSystem : public OdTvViewerViewDependentObject
{
public:
  OdTv3DCoordinateSystem(int iViewport, OdTvDatabaseId& databaseId, OdTvModelId& modelId, const OdTvPoint& pointXYZ, const OdTvPoint& pointXY, const OdTvPoint& pointXZ);
  virtual ~OdTv3DCoordinateSystem() {};

  void setStartXMark(double startMark);
  void setEndXMark(double endMark);
  void setStartYMark(double startMark);
  void setEndYMark(double endMark);
  void setStartZMark(double startMark);
  void setEndZMark(double endMark);

  void setXNum(OdUInt32 num);
  void setYNum(OdUInt32 num);
  void setZNum(OdUInt32 num);

  OdTvEntityPtr resetEntity(OdTvEntityId& entityId);
  void updatePointsByLongitude(double longitude, OdTvVector& posTargetToX);
  void updateMarksData(double longitude, double latityde, OdTvVector& posTargetToX, OdUInt32& xNum, OdUInt32& yNum, OdUInt32& zNum);
  void updatePointsByLatityde(double latityde);
  OdString generateMark(double mark) const;

  void fillDataForX(OdTvPoint& pointOne, OdTvPoint& pointTwo, OdTvPoint& pointThree, OdTvPoint& pointFour, OdTvPoint& pointFive, OdTvPoint& pointSix, OdTvPoint& pointSeven, double& posStep, double& markStep, OdUInt32 num);
  void fillDataForY(OdTvPoint& pointOne, OdTvPoint& pointTwo, OdTvPoint& pointThree, OdTvPoint& pointFour, OdTvPoint& pointFive, OdTvPoint& pointSix, OdTvPoint& pointSeven, double& posStep, double& markStep, OdUInt32 num);
  void fillDataForZ(OdTvPoint& pointOne, OdTvPoint& pointTwo, OdTvPoint& pointThree, OdTvPoint& pointFour, OdTvPoint& pointFive, OdTvPoint& pointSix, OdTvPoint& pointSeven, double& posStep, double& markStep, OdUInt32 num);

  OdTvPoint& updatePointForX(OdTvPoint& point, double value);
  OdTvPoint& updatePointForY(OdTvPoint& point, double value);
  OdTvPoint& updatePointForZ(OdTvPoint& point, double value);

  void drawCoordinateCells(OdTvEntityPtr& pGraphEntity, OdTvEntityPtr& pCellEntity, double latityde,
    double startMark, OdUInt32 num, double startPos, const OdString& axisText,
    void (OdTv3DCoordinateSystem::*fillData)(OdTvPoint&, OdTvPoint&, OdTvPoint&, OdTvPoint&, OdTvPoint&, OdTvPoint&, OdTvPoint&, double&, double&, OdUInt32),
    OdTvPoint& (OdTv3DCoordinateSystem::*updatePoint)(OdTvPoint&, double));

  virtual void update(const OdTvGsViewId& tvView);


private:
  OdTvDatabaseId m_databaseId;
  OdTvModelId m_modelId;
  OdTvEntityId m_axisEntityId;
  OdTvEntityId m_cellEntityId;
  OdTvEntityId m_textMarkEntityId;
  OdTvEntityId m_textAxisEntityId;
  OdTvPoint m_pointXYZ;
  OdTvPoint m_pointXY;
  OdTvPoint m_pointXZ;
  double m_startXMark;
  double m_endXMark;
  double m_startYMark;
  double m_endYMark;
  double m_startZMark;
  double m_endZMark;
  OdUInt32 m_xNum;
  OdUInt32 m_yNum;
  OdUInt32 m_zNum;

  OdTvTextStyleId m_midRtextStyle;

  // Base points
  OdTvPoint m_xAxisA, m_xAxisA1, m_xAxisA2;
  OdTvPoint m_xAxisB, m_xAxisB1, m_xAxisB2;

  OdTvPoint m_yAxisA, m_yAxisA1, m_yAxisA2;
  OdTvPoint m_yAxisB, m_yAxisB1, m_yAxisB2;

  OdTvPoint m_zAxisA, m_zAxisA1, m_zAxisA2;
  OdTvPoint m_zAxisB, m_zAxisB1, m_zAxisB2;


  // Mark lines sizes
  double m_xMarkLinesSizeX;
  double m_xMarkLinesSizeZ;
  double m_yMarkLinesSizeY;
  double m_yMarkLinesSizeZ;
  double m_zMarkLinesSizeX;
  double m_zMarkLinesSizeY;
};

typedef OdTvSmartPtr<OdTv3DCoordinateSystem> OdTv3DCoordinateSystemPtr;

#endif //OD_TV_VIEWDEPENDENTOBJECT_H
