# OdVisualizeViewer

# In case QT is available for requested platform
# QT4 or QT5 should be defined in configuration file.
# Depends on linux version different version of QT will be built.

# Note that this sample is included only when QT5 var is set in the system
# See tcomponent.directories
SET(QT5 1)

add_definitions(-DOD_VISUALIZE_VIEWER)

if(VISUALIZE_TESTS)
  if(TCOMPONENTS_BREPMODELER_TESTS)
    add_definitions(-DREPLAY2VISUALIZE_ENABLED)
  endif(TCOMPONENTS_BREPMODELER_TESTS)
endif(VISUALIZE_TESTS)

if(TNW_ROOT)
  add_definitions(-DNW2VISUALIZE_ENABLED)
endif(TNW_ROOT)

if(IFC_ROOT)
add_definitions(-DIFC2VISUALIZE_ENABLED)
endif(IFC_ROOT)

if(QT4)
  FIND_PACKAGE(Qt4 COMPONENTS QtCore QtGui REQUIRED)
  INCLUDE(${QT_USE_FILE})
elseif(QT5)
  set(CMAKE_AUTOMOC ON)
  find_package (Qt5Core REQUIRED)
  find_package (Qt5Widgets REQUIRED)
endif(QT4)

set( OdVisualizeViewer_SRCS
    main.cpp
	../../Common/qtUIComponents/OdTvUIApplicationDef.h
	../../Common/qtUIComponents/OdTvUIAppUtils.h
	../../Common/qtUIComponents/OdTvUIAppUtils.cpp
	../../Common/qtUIComponents/OdTvUIBaseView.h
	../../Common/qtUIComponents/OdTvUIBaseView.cpp
	../../Common/qtUIComponents/OdTvUIDatabaseInfo.h
	../../Common/qtUIComponents/OdTvUITabPanels.h
	../../Common/qtUIComponents/OdTvUITabPanels.cpp
    TvViewerMainWindow.cpp
	TvViewerViewDependentObject.cpp
	TvViewerMainWindow.h
	TvViewerViewDependentObject.h
)

set (OdVisualizeViewer_SRCS_markup
../../Common/qtUIComponents/OdTvUIMarkupDlgs.cpp
)
SOURCE_GROUP ("Source Files\\Markup" FILES ${OdVisualizeViewer_SRCS_markup})

set (OdVisualizeViewer_SRCS_properties
../../Common/qtUIComponents/OdTvUIBaseObjectExplorer.cpp
../../Common/qtUIComponents/OdTvUIVisualizeObjectExplorer.cpp
../../Common/qtUIComponents/OdTvUIBaseObjectProperties.cpp
../../Common/qtUIComponents/OdTvUIVisualizeObjectProperties.cpp
../../Common/qtUIComponents/OdTvUIColorComboBox.cpp
../../Common/qtUIComponents/OdTvUIObjectNativeProperties.cpp
../../Common/qtUIComponents/OdTvUINonCOMPropertyControl.cpp
../../Common/qtUIComponents/OdTvUINonCOMPropertyDictionary.cpp
../../Common/qtUIComponents/OdTvUICDANodesProperties.cpp
TvViewerVisualStylesDlgs.cpp
../../Common/qtUIComponents/OdTvUIPalettePanels.cpp
../../Common/qtUIComponents/OdTvUICollisionDetectionResults.cpp
../../Common/qtUIComponents/OdTvUICDAObjectTreeExplorer.cpp
)
SOURCE_GROUP ("Source Files\\Properties" FILES ${OdVisualizeViewer_SRCS_properties})

set (OdVisualizeViewer_SRCS_draggers
../../Common/qtUIComponents/OdTvUIDraggers.cpp
../../Common/qtUIComponents/OdTvUIAxisControl.cpp
../../Common/qtUIComponents/OdTvUICutPlaneDragger.cpp
TvViewerConstructDraggers.cpp
)
SOURCE_GROUP ("Source Files\\Draggers" FILES ${OdVisualizeViewer_SRCS_draggers})

set (OdVisualizeViewer_SRCS_options
../../Common/qtUIComponents/OdTvUIPerformanceParamsPage.cpp
../../Common/qtUIComponents/OdTvUIGridParamsPage.cpp
../../Common/qtUIComponents/OdTvUIAppearanceParamsPage.cpp
../../Common/qtUIComponents/OdTvUIImportParameters.cpp
../../Common/qtUIComponents/OdTvUICommonParamsDlg.cpp
../../Common/qtUIComponents/OdTvUIGeneralParamsPage.cpp
../../Common/qtUIComponents/OdTvUIExportParameters.cpp
)
SOURCE_GROUP ("Source Files\\Options" FILES ${OdVisualizeViewer_SRCS_options})

set (OdVisualizeViewer_SRCS_UIComponents
../../Common/qtUIComponents/OdTvUIItemPanel.cpp
../../Common/qtUIComponents/OdTvUINavigationHandler.cpp
../../Common/qtUIComponents/OdTvUIMenuToolbar.cpp
../../Common/qtUIComponents/OdTvUISliderMenu.cpp
../../Common/qtUIComponents/OdTvUIProgressMeter.cpp
../../Common/qtUIComponents/OdTvUICuttingPlaneParamsDlg.cpp
)
SOURCE_GROUP ("Source Files\\UIComponents" FILES ${OdVisualizeViewer_SRCS_UIComponents})

set (OdVisualizeViewer_HDR_markup
../../Common/qtUIComponents/OdTvUIMarkupDlgs.h
)
SOURCE_GROUP ("Header Files\\Markup" FILES ${OdVisualizeViewer_HDR_markup})

set (OdVisualizeViewer_HDR_properties
../../Common/qtUIComponents/OdTvUIBaseObjectExplorer.h
../../Common/qtUIComponents/OdTvUIVisualizeObjectExplorer.h
../../Common/qtUIComponents/OdTvUIBaseObjectProperties.h
../../Common/qtUIComponents/OdTvUIVisualizeObjectProperties.h
../../Common/qtUIComponents/OdTvUIColorComboBox.h
../../Common/qtUIComponents/OdTvUIObjectNativeProperties.h
../../Common/qtUIComponents/OdTvUINonCOMPropertyControl.h
../../Common/qtUIComponents/OdTvUINonCOMPropertyDictionary.h
../../Common/qtUIComponents/OdTvUICDANodesProperties.h
TvViewerVisualStylesDlgs.h
../../Common/qtUIComponents/OdTvUIPalettePanels.h
../../Common/qtUIComponents/OdTvUICollisionDetectionResults.h
../../Common/qtUIComponents/OdTvUICDAObjectTreeExplorer.h
)
SOURCE_GROUP ("Header Files\\Properties" FILES ${OdVisualizeViewer_HDR_properties})

set (OdVisualizeViewer_HDR_draggers
../../Common/qtUIComponents/OdTvUIDraggers.h
../../Common/qtUIComponents/OdTvUIAxisControl.h
../../Common/qtUIComponents/OdTvUICutPlaneDragger.h
TvViewerConstructDraggers.h
)
SOURCE_GROUP ("Header Files\\Draggers" FILES ${OdVisualizeViewer_HDR_draggers})

set (OdVisualizeViewer_HDR_options
../../Common/qtUIComponents/OdTvImportParametersContainer.h
../../Common/qtUIComponents/OdTvUIPerformanceParamsPage.h
../../Common/qtUIComponents/OdTvUIGridParamsPage.h
../../Common/qtUIComponents/OdTvUIAppearanceParamsPage.h
../../Common/qtUIComponents/OdTvUIImportParameters.h
../../Common/qtUIComponents/OdTvUICommonParamsDlg.h
../../Common/qtUIComponents/OdTvUIGeneralParamsPage.h
../../Common/qtUIComponents/OdTvUIExportParameters.h
)
SOURCE_GROUP ("Header Files\\Options" FILES ${OdVisualizeViewer_HDR_options})

set (OdVisualizeViewer_HDR_UIComponents
../../Common/qtUIComponents/OdTvBaseUIElement.h
../../Common/qtUIComponents/OdTvUIItemPanel.h
../../Common/qtUIComponents/OdTvUINavigationHandler.h
../../Common/qtUIComponents/OdTvUIMenuToolbar.h
../../Common/qtUIComponents/OdTvUISliderMenu.h
../../Common/qtUIComponents/OdTvUIStyleManager.h
../../Common/qtUIComponents/OdTvUIProgressMeter.h
../../Common/qtUIComponents/OdTvUICuttingPlaneParamsDlg.h
)
SOURCE_GROUP ("Header Files\\UIComponents" FILES ${OdVisualizeViewer_HDR_UIComponents})


if (MSVC)
  visualize_sources(OdVisualizeViewer
    data/TvVisualizeViewerQt.rc
  )
  remove_definitions(-D_CRTDBG_MAP_ALLOC)
endif(MSVC)

set( OdVisualizeViewer_MOC_HDRS
    TvViewerMainWindow.h
)

if (MACOS_X86 OR MACOS_X64)
  add_definitions(-DQ_OS_MAC)
endif()

SET( OdVisualizeViewer_RESOURCES_RCS
  data/TvVisualizeViewerQt.qrc
)

# Add the include directories for the Qt 5 Widgets module to
# the compile lines.
include_directories(${VISUALIZE_ROOT}/Include
					${TKERNEL_ROOT}/Extensions/ExServices
					${TCOMPONENTS_ROOT}/OBJToolkit/Include
					${VISUALIZE_ROOT}/Common/qtUIComponents
					${TKERNEL_ROOT}/DevInclude/root
					${TKERNEL_ROOT}/DevInclude/DbRoot					
					${TKERNEL_ROOT}/Include/Gi
					${TKERNEL_ROOT}/Source/DbRoot
					${TDRAWING_ROOT}/Include
)

if (QT4)
  # this command will generate rules that will run rcc on all files from SAMPLE_RCS
  # in result SAMPLE_RC_SRCS variable will contain paths to files produced by rcc
  qt4_add_resources(OdVisualizeViewer_RC_SRCS ${OdVisualizeViewer_RESOURCES_RCS})

  # run moc
  if(MACOS_X64 OR MACOS_X86)
    qt4_wrap_cpp(OdVisualizeViewer_MOC_SRCS ${OdVisualizeViewer_MOC_HDRS} OPTIONS -DQ_OS_MAC)
  else()
    qt4_wrap_cpp(OdVisualizeViewer_MOC_SRCS ${OdVisualizeViewer_MOC_HDRS})
  endif()

  visualize_sources(OdVisualizeViewer
					 ${OdVisualizeViewer_SRCS}
					 ${OdVisualizeViewer_SRCS_markup}
					 ${OdVisualizeViewer_SRCS_properties}
					 ${OdVisualizeViewer_SRCS_draggers}
					 ${OdVisualizeViewer_SRCS_options}
					 ${OdVisualizeViewer_SRCS_UIComponents}
					 ${OdVisualizeViewer_HDR_markup}
					 ${OdVisualizeViewer_HDR_properties}
					 ${OdVisualizeViewer_HDR_draggers}
					 ${OdVisualizeViewer_HDR_options}
					 ${OdVisualizeViewer_HDR_UIComponents}
					 ${OdVisualizeViewer_MOC_SRCS}
					 ${OdVisualizeViewer_RC_SRCS})
  if (MSVC)
    set(QT_LIBRARIES ${QT_LIBRARIES} ${QT_QTMAIN_LIBRARY})
  endif (MSVC)

elseif (QT5)

  qt5_add_resources(OdVisualizeViewer_RC_SRCS ${OdVisualizeViewer_RESOURCES_RCS})
  visualize_sources(OdVisualizeViewer
					 ${OdVisualizeViewer_SRCS}
					 ${OdVisualizeViewer_SRCS_markup}
					 ${OdVisualizeViewer_SRCS_properties}
					 ${OdVisualizeViewer_SRCS_draggers}
					 ${OdVisualizeViewer_SRCS_options}
					 ${OdVisualizeViewer_SRCS_UIComponents}
					 ${OdVisualizeViewer_HDR_markup}
					 ${OdVisualizeViewer_HDR_properties}
					 ${OdVisualizeViewer_HDR_draggers}
					 ${OdVisualizeViewer_HDR_options}
					 ${OdVisualizeViewer_HDR_UIComponents}
					 ${OdVisualizeViewer_MOC_HDRS}
					 ${OdVisualizeViewer_RC_SRCS})
  set(QT_LIBRARIES ${QT_LIBRARIES} ${Qt5Widgets_LIBRARIES} ${Qt5Core_LIBRARIES} )
  if (MSVC)
    set(QT_LIBRARIES ${QT_LIBRARIES} Qt5::WinMain )
  endif (MSVC)

endif ()

if(ANDROID)
  set(ADDITIONAL_GL_LIBS GLESv2 EGL android)
  visualize_module( OdVisualizeViewer ${TD_GE_LIB} ${TV_VISUALIZE_LIB} ${TV_VISUALIZETOOLS_LIB} ${QT_LIBRARIES} ${ODA_PLATFORM_LIBS} ${ODA_OPENGL_LIBS} ${ADDITIONAL_GL_LIBS} android log gcc
  )
  oda_qt_set_definition(OdVisualizeViewer "TD_USE_QT_LIB")
  oda_qt_set_cxxflags(OdVisualizeViewer "")
elseif(MSVC)
  visualize_executable(OdVisualizeViewer ${TD_EXLIB}  ${TD_GE_LIB} ${QT_LIBRARIES} ${TV_VISUALIZE_LIB}  ${TV_VISUALIZETOOLS_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB} ${TD_DBROOT_LIB} ${TD_RX_CDA_LIB} Psapi.lib
  )
else(ANDROID)
  visualize_executable(OdVisualizeViewer ${TD_EXLIB}  ${TD_GE_LIB} ${QT_LIBRARIES} ${TV_VISUALIZE_LIB}  ${TV_VISUALIZETOOLS_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB}
  )
endif(ANDROID)

if (QT5)
  qt5_use_modules(OdVisualizeViewer Core Widgets)
endif(QT5)

if(MSVC)
  set_target_properties(OdVisualizeViewer PROPERTIES WIN32_EXECUTABLE 1)
  to_relative_path(SRC_REL_PATH ${CMAKE_CURRENT_SOURCE_DIR})
  to_relative_path(DST_REL_PATH ${ODA_BIN_DIR})
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\odalogo.png" "${DST_REL_PATH}\\odalogo.png"  /Y COMMENT "Copying 'odalogo.png' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\ball.png" "${DST_REL_PATH}\\textures\\ball.png" /Y COMMENT "Copying 'ball.png' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\handle.jpg" "${DST_REL_PATH}\\textures\\handle.jpg" /Y COMMENT "Copying 'handle.jpg' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\metal.jpg" "${DST_REL_PATH}\\textures\\metal.jpg" /Y COMMENT "Copying 'metal.jpg' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\marble.png" "${DST_REL_PATH}\\textures\\marble.png" /Y COMMENT "Copying 'marble.png' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\wood.png" "${DST_REL_PATH}\\textures\\wood.png" /Y COMMENT "Copying 'wood.png' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\pepsi.jpg" "${DST_REL_PATH}\\textures\\pepsi.jpg" /Y COMMENT "Copying 'pepsi.jpg' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\texture_for_opacity.png" "${DST_REL_PATH}\\textures\\texture_for_opacity.png" /Y COMMENT "Copying 'texture_for_opacity.png' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\opacity.png" "${DST_REL_PATH}\\textures\\opacity.png" /Y COMMENT "Copying 'opacity.png' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\diffuse.png" "${DST_REL_PATH}\\textures\\diffuse.png" /Y COMMENT "Copying 'diffuse.png' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\textures\\bump.png" "${DST_REL_PATH}\\textures\\bump.png" /Y COMMENT "Copying 'bump.png' to binary folder")
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND echo F|xcopy "${SRC_REL_PATH}\\data\\samples\\pepsi_can.vsf" "${DST_REL_PATH}\\samples\\pepsi_can.vsf" /Y COMMENT "Copying 'pepsi_can.vsf' to binary folder")
  if (NOT QT4)
  set_target_properties(OdVisualizeViewer PROPERTIES COMPILE_FLAGS_REMOVE "/Zc:wchar_t-")
endif (NOT QT4)
elseif (MACOS_X86 OR MACOS_X64)
  oda_set_macos_bundle_properties(OdVisualizeViewer odalogo "${CMAKE_CURRENT_SOURCE_DIR}/../how_to_build_qt_mac_xcode/data")
elseif (ANDROID)
  add_custom_command (TARGET OdVisualizeViewer POST_BUILD COMMAND cp "${CMAKE_CURRENT_SOURCE_DIR}/data/odalogo.png" "${CMAKE_CURRENT_SOURCE_DIR}/data/android-sources/res/drawable-mdpi")
  oda_qt_create_apk(OdVisualizeViewer)
endif(MSVC)

visualize_project_group(OdVisualizeViewer "Visualize/Examples")

#UNSET(QTDIR_PATH)
#UNSET(CMAKE_PREFIX_PATH)