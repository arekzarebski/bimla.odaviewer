/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

// ODA Platform
#include "OdaCommon.h"
#include "TvViewerConstructDraggers.h"
#include "OdRound.h"

//***************************************************************************//
// 'OdTvViewerBaseConstructDragger' methods implementation
//***************************************************************************//
OdTvViewerBaseConstructDragger::OdTvViewerBaseConstructDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvUIBaseDragger(tvDeviceId, tvDraggersModelId)
{
  m_tvActiveModelId = tvActiveModelId;
  m_ClickState = kWaitingFirstClick;
  m_bFirstStart = true;
  m_bNeedFreeDrag = true;
  m_clickedPts.resize(5);
  m_bNeedTransferToActive = false;

  m_LastState = kWaitingSecondClick;

  m_bDraggerShouldBeRestarted = false;

  m_pView = pBaseView;

  m_bHaveDrawableTemporaryGeometry = true;

  m_draggerCursor = QCursor();
}

eDraggerResult OdTvViewerBaseConstructDragger::start(OdTvDraggerPtr pPrevDragger, QCursor activeCusor, OdTvExtendedView* pExtendedView)
{
  eDraggerResult rc = kNothingToDo;

  // create temporary geometry in the case when we restarted from the previous state
  if (!m_bFirstStart)
  {
    updateGeometry(true, false);
    rc = kNeedUpdateView;
  }

  m_draggerCursor = activeCusor;
  m_lastAppActiveCursor = activeCusor;

  addDraggersModelToView();
  rc = rc | OdTvUIBaseDragger::start(pPrevDragger, activeCusor, pExtendedView);

  updateBaseColor();

  if (!m_bFirstStart)
    m_state = kWorking;

  m_bFirstStart = false;

  return rc;
}

eDraggerResult OdTvViewerBaseConstructDragger::nextpoint(int x, int y)
{
  CHECK_DRAGGER

  if (m_ClickState == kWaitingFirstClick)
  {
    // remember the click
    m_clickedPts[(int)m_ClickState] = toEyeToWorld(x, y);
    toUcsToWorld(m_clickedPts[(int)m_ClickState]);

    //increase click state
    m_ClickState = (ClickState)((int)m_ClickState + 1);
  }
  else
  {
    // remember the click
    m_clickedPts[(int)m_ClickState] = toEyeToWorld(x, y);
    toUcsToWorld(m_clickedPts[(int)m_ClickState]);

    // update created geometry
    updateGeometry(false, true);

    eDraggerResult rc = kNothingToDo;
    if (m_ClickState == m_LastState)
    {
      // if it was a dragger with view - we should add the result to the auto regeneration map
      if (m_pView != NULL && !m_entityId.isNull())
        m_pView->addEntityIdToAutoRegenList(m_entityId);

      // release current entity (means final adding to the database)
      m_entityId.setNull();

      // restart or finish the dragger
      if (m_bDraggerShouldBeRestarted)
      {
        transferResultToActiveModel();
        m_clickedPts[0] = m_clickedPts[(int)m_ClickState];
        m_ClickState = kWaitingSecondClick;
      }
      else
      {
        m_bNeedTransferToActive = true;
        rc = kNeedFinishDragger;
      }
    }
    else
      //increase click state
      m_ClickState = (ClickState)((int)m_ClickState + 1);

    return rc | kNeedUpdateView;
  }

  return kNothingToDo;
}

eDraggerResult OdTvViewerBaseConstructDragger::drag(int x, int y)
{
  CHECK_DRAGGER

  if (m_ClickState == kWaitingFirstClick)
    return kNothingToDo;

  // remember the drag
  m_clickedPts[(int)m_ClickState] = toEyeToWorld(x, y);
  toUcsToWorld(m_clickedPts[(int)m_ClickState]);

  // create temporary geometry if need
  if (m_entityId.isNull())
    updateGeometry(true, false);
  else // update created geometry
    updateGeometry(false, false);

  return kNeedUpdateView;
}

OdTvDraggerPtr OdTvViewerBaseConstructDragger::finish(eDraggerResult& rc)
{
  //transfer last created entity to the main active model
  if (m_bNeedTransferToActive)
  {
    transferResultToActiveModel();

    //reset cached extents
    if (m_pTvExtendedView)
      m_pTvExtendedView->setViewExtentsForCaching(NULL);
  }

  eDraggerResult rcFinish;
  OdTvDraggerPtr retFinish = OdTvDragger::finish(rcFinish);

  rc = rcFinish | kNeedUpdateView;
  return retFinish;
}

bool OdTvViewerBaseConstructDragger::canFinish()
{
  return true;
}

bool OdTvViewerBaseConstructDragger::updateCursor()
{
  if (m_state != kFinishing)
    m_draggerCursor = QCursor(Qt::CrossCursor);
  else
    m_draggerCursor = m_lastAppActiveCursor;

  return true;
}

void OdTvViewerBaseConstructDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  return;
}

double OdTvViewerBaseConstructDragger::getDistance(OdGePoint3d& pt1, OdGePoint3d& pt2) const
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return 0.0;

  //1. Calculate the UCS normal in the device space
  OdGeVector3d vUcsNormal = OdGeVector3d::kZAxis; // currently UCS = WCS
  vUcsNormal.transformBy(pView->worldToDeviceMatrix());

  //2. Calculate the UCS normal in the eye space
  vUcsNormal.normalize();
  vUcsNormal = OdGeVector3d(vUcsNormal.x, -vUcsNormal.y, vUcsNormal.z);

  //3. Calculate the UCS normal in UCS plane
  OdGePoint3d ptScrO(0., 0., 0);
  OdGePoint3d ptScrV(vUcsNormal.x, vUcsNormal.y, vUcsNormal.z);

  ptScrO.transformBy(pView->eyeToWorldMatrix());
  ptScrV.transformBy(pView->eyeToWorldMatrix());

  toUcsToWorld(ptScrO);
  toUcsToWorld(ptScrV);

  vUcsNormal = (ptScrV - ptScrO).normalize();

  //4. Project the vector between two clicked points into the UCS normal in UCS plane
  double dist = (pt2 - pt1).dotProduct(vUcsNormal);

  //5. Calculate special coefficient
  OdGeVector3d vUcsDirection = OdGeVector3d::kZAxis; // currently UCS = WCS
  OdGeVector3d vDirection = pView->position() - pView->target();

  // is in perpendicular plane to UCS one
  vUcsDirection.normalize();
  vDirection.normalize();
  double dang = vDirection.angleTo(vUcsDirection);
  double dcoef = tan(dang);
  if (!OdZero(dcoef))
    dist /= dcoef;

  //6. Sometimes it is need to change the sign
  OdGeVector3d vTst = OdGeVector3d::kXAxis + OdGeVector3d::kYAxis;
  double scalar = vDirection.dotProduct(vTst);
  if ( OdZero(scalar) )
    vTst = 0.9*OdGeVector3d::kXAxis + 1.1*OdGeVector3d::kYAxis;

  if ((vDirection.dotProduct(vTst) < 0.0) == ((vUcsNormal).dotProduct(vTst) < 0.0))
    dist = -dist;

  return dist;
}

void OdTvViewerBaseConstructDragger::transferResultToActiveModel()
{
  OdTvModelPtr model = m_tvActiveModelId.openObject(OdTv::kForWrite);
  OdTvEntityId entityId = model->appendEntity();
  OdTvEntityPtr pEn = entityId.openObject(OdTv::kForWrite);

  switch (m_newGeometryId.getType())
  {
  case OdTv::kPolyline:
  {
    OdTvPolylineDataPtr pLine = m_newGeometryId.openAsPolyline();
    OdTvPointArray arr;
    pLine->getPoints(arr);
    pEn->appendPolyline(arr);
    break;
  }
  case OdTv::kCircle:
  {
    OdTvCircleDataPtr pCirc = m_newGeometryId.openAsCircle();
    OdTvPoint center;
    double radius = 0.;
    OdTvVector normal;
    pCirc->get(center, radius, normal);
    pEn->appendCircle(center, radius, normal);
    break;
  }
  case OdTv::kCircularArc:
  {
    OdTvCircleArcDataPtr pCircArc = m_newGeometryId.openAsCircleArc();
    OdTvPoint start, middle, end;
    pCircArc->get(start, middle, end);
    pEn->appendCircleArc(start, middle, end);
    break;
  }
  case OdTv::kCircleWedge:
  {
    OdTvCircleWedgeDataPtr pWedge = m_newGeometryId.openAsCircleWedge();
    OdTvPoint start, middle, end;
    pWedge->get(start, middle, end);
    pEn->appendCircleWedge(start, middle, end);
    break;
  }
  case OdTv::kEllipse:
  {
    OdTvEllipseDataPtr pEllipse = m_newGeometryId.openAsEllipse();
    OdTvPoint center, major, minor;
    pEllipse->get(center, major, minor);
    pEn->appendEllipse(center, major, minor);
    break;
  }
  case OdTv::kEllipticArc:
  {
    OdTvEllipticArcDataPtr pEllipArc = m_newGeometryId.openAsEllipticArc();
    OdTvPoint center, major, minor;
    double start = 0., end = 0.;
    pEllipArc->get(center, major, minor, start, end);
    pEn->appendEllipticArc(center, major, minor, start, end);
    break;
  }
  case OdTv::kShell:
  {
    OdTvShellDataPtr pShell = m_newGeometryId.openAsShell();
    OdTvPointArray points;
    OdInt32Array faces;
    pShell->getParam(points, faces);
    pEn->appendShell(points, faces);
    break;
  }
  case OdTv::kPolygon:
  {
    OdTvPolygonDataPtr pPol = m_newGeometryId.openAsPolygon();
    OdTvPointArray points;
    pPol->getPoints(points);
    pEn->appendPolygon(points);
    break;
  }
  case OdTv::kSphere:
  {
    OdTvSphereDataPtr pSphere = m_newGeometryId.openAsSphere();
    OdTvPoint center;
    double radius = 0.;
    OdTvVector axis, primeMeridian;
    pSphere->getParam(center, radius, axis, primeMeridian);
    pEn->appendSphere(center, radius, axis, primeMeridian);
    break;
  }
  case OdTv::kCylinder:
  {
    OdTvCylinderDataPtr pCyl = m_newGeometryId.openAsCylinder();
    OdTvPointArray pnts;
    OdDoubleArray radii;
    OdTvCylinderData::Capping cap;
    pCyl->getParam(pnts, radii, cap);
    pEn->appendCylinder(pnts, radii, cap);
    break;
  }
  case OdTv::kInfiniteLine:
  {
    OdTvInfiniteLineDataPtr pInf = m_newGeometryId.openAsInfiniteLine();
    pEn->appendInfiniteLine(pInf->getFirst(), pInf->getSecond(), pInf->getType());
    break;
  }
  case OdTv::kNurbs:
  {
    OdTvNurbsDataPtr pNurbs = m_newGeometryId.openAsNurbs();
    unsigned int degree = 0;
    OdTvPointArray pnts;
    OdDoubleArray weights, knots;
    double start = 0., end = 0.;
    pNurbs->get(degree, pnts, weights, knots, start, end);
    pEn->appendNurbs(degree, pnts, weights, knots, start, end);
    break;
  }
  case OdTv::kRasterImage:
  {
    OdTvRasterImageDataPtr pImg = m_newGeometryId.openAsRasterImage();
    OdTvPoint origin;
    OdTvVector u, v;
    pImg->getOrientation(origin, u, v);
    pEn->appendRasterImage(pImg->getImageId(), origin, u, v);
    break;
  }
  case OdTv::kBox:
  {
    OdTvBoxDataPtr pBox = m_newGeometryId.openAsBox();
    OdTvPoint centerPoint;
    double dLength, dWidth, dHeight;
    OdTvVector baseNormal, lengthDirection;
    pBox->getParam(centerPoint, dLength, dWidth, dHeight, baseNormal, lengthDirection);
    pEn->appendBox(centerPoint, dLength, dWidth, dHeight, baseNormal, lengthDirection);
    break;
  }
  }
}

//***************************************************************************//
// 'OdTvViewerLineDragger' methods implementation
//***************************************************************************//
OdTvViewerLineDragger::OdTvViewerLineDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  m_bDraggerShouldBeRestarted = true;
}

void OdTvViewerLineDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  //update or create entity
  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);
      //create line
      m_newGeometryId = entityNewPtr->appendPolyline(m_clickedPts[0], m_clickedPts[1]);
    }
  }
  else
  {
    OdTvGeometryDataPtr geometryPtr = m_newGeometryId.openObject();
    if (geometryPtr.isNull() || geometryPtr->getType() != OdTv::kPolyline)
      return;

    OdTvPolylineDataPtr linePtr = geometryPtr->getAsPolyline();
    if (!linePtr.isNull())
      linePtr->setPoints(m_clickedPts[0], m_clickedPts[1]);
  }

  return;
}


//***************************************************************************//
// ' OdTvViewerCircleRadiusDragger' methods implementation
//***************************************************************************//
OdTvViewerCircleRadiusDragger::OdTvViewerCircleRadiusDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  
}

void  OdTvViewerCircleRadiusDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  OdGeVector3d radius = (m_clickedPts[1] - m_clickedPts[0]);
  double circleRadius = radius.length();

  //update or create entity
  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);
      //create circle
      m_newGeometryId = entityNewPtr->appendCircle(m_clickedPts[0], circleRadius, OdGeVector3d::kZAxis);
    }
  }
  else
  {
    OdTvGeometryDataPtr geometryPtr = m_newGeometryId.openObject();
    if (geometryPtr.isNull() || geometryPtr->getType() != OdTv::kCircle)
      return;

    OdTvCircleDataPtr circlePtr = geometryPtr->getAsCircle();
    if (!circlePtr.isNull())
      circlePtr->setRadius(circleRadius);
  }

  return;
}

//***************************************************************************//
// ' OdTvViewerCircle3PointsDragger' methods implementation
//***************************************************************************//
OdTvViewerCircle3PointsDragger::OdTvViewerCircle3PointsDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  m_LastState = kWaitingThirdClick;
}


void  OdTvViewerCircle3PointsDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  OdGePoint3d lastPoint = m_clickedPts[1];
  if (m_ClickState == kWaitingThirdClick)
  {
    lastPoint = m_clickedPts[2];

    // need to reload the geometry
    OdTvGeometryDataPtr geometryPtr = m_newGeometryId.openObject();
    if (!geometryPtr.isNull() && geometryPtr->getType() != OdTv::kCircle)
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      modelPtr->removeEntity(m_entityId);
      bCreate = true;
    }
  }

  //update or create entity
  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);

      if (m_ClickState == kWaitingThirdClick)
        //create circle
        m_newGeometryId = entityNewPtr->appendCircle(m_clickedPts[0], m_clickedPts[1], lastPoint);
      else
      {
        //create line
        m_newGeometryId = entityNewPtr->appendPolyline(m_clickedPts[0], lastPoint);
        entityNewPtr->setColor(OdTvColorDef(150, 156, 154));
      }

    }
  }
  else
  {
    OdTvGeometryDataPtr geometryPtr = m_newGeometryId.openObject();
    if (geometryPtr.isNull())
      return;

    if (m_ClickState == kWaitingThirdClick)
    {
      OdTvCircleDataPtr circlePtr = geometryPtr->getAsCircle();
      if (!circlePtr.isNull())
        circlePtr->set(m_clickedPts[0], m_clickedPts[1], lastPoint);
    }
    else
    {
      OdTvPolylineDataPtr polylinePtr = geometryPtr->getAsPolyline();
      if (!polylinePtr.isNull())
        polylinePtr->setPoints(m_clickedPts[0], lastPoint);
    }
  }

  return;
}

//***************************************************************************//
// ' OdTvViewerCircleArcDragger' methods implementation
//***************************************************************************//
OdTvViewerCircleArcDragger::OdTvViewerCircleArcDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  m_LastState = kWaitingThirdClick;
}


void OdTvViewerCircleArcDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{

  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  OdGePoint3d lastPoint = m_clickedPts[1];
  if (m_ClickState == kWaitingThirdClick)
  {
    lastPoint = m_clickedPts[2];

    // need to reload the geometry
    OdTvGeometryDataPtr geometryPtr = m_newGeometryId.openObject();
    if (!geometryPtr.isNull() && geometryPtr->getType() != OdTv::kCircularArc)
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      modelPtr->removeEntity(m_entityId);
      bCreate = true;
    }
  }

  //update or create entity
  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);

      if (m_ClickState == kWaitingThirdClick)
        //create circle arc
        m_newGeometryId = entityNewPtr->appendCircleArc(m_clickedPts[0], m_clickedPts[1], lastPoint);
      else
      {
        //create line
        m_newGeometryId = entityNewPtr->appendPolyline(m_clickedPts[0], lastPoint);
        entityNewPtr->setColor(OdTvColorDef(150, 156, 154));
      }

    }
  }
  else
  {
    OdTvGeometryDataPtr geometryPtr = m_newGeometryId.openObject();
    if (geometryPtr.isNull())
      return;

    if (m_ClickState == kWaitingThirdClick)
    {
      OdTvCircleArcDataPtr circleArcPtr = geometryPtr->getAsCircleArc();
      if (!circleArcPtr.isNull())
        circleArcPtr->setEnd(lastPoint);
    }
    else
    {
      OdTvPolylineDataPtr polylinePtr = geometryPtr->getAsPolyline();
      if (!polylinePtr.isNull())
        polylinePtr->setPoints(m_clickedPts[0], lastPoint);
    }
  }

  return;
}

//***************************************************************************//
// ' OdTvViewerCircleWedgeDragger' methods implementation
//***************************************************************************//
OdTvViewerCircleWedgeDragger::OdTvViewerCircleWedgeDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  m_LastState = kWaitingThirdClick;
}

void  OdTvViewerCircleWedgeDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  OdGePoint3d lastPoint = m_clickedPts[1];
  if (m_ClickState == kWaitingThirdClick)
  {
    lastPoint = m_clickedPts[2];

    // need to reload the geometry
    OdTvGeometryDataPtr geometryPtr = m_newGeometryId.openObject();
    if (!geometryPtr.isNull() && geometryPtr->getType() != OdTv::kCircleWedge)
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      modelPtr->removeEntity(m_entityId);
      bCreate = true;
    }
  }

  //update or create entity
  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);

      if (m_ClickState == kWaitingThirdClick)
        //create circle wedge
        m_newGeometryId = entityNewPtr->appendCircleWedge(m_clickedPts[0], lastPoint, m_clickedPts[1]);
      else
      {
        //create line
        m_newGeometryId = entityNewPtr->appendPolyline(m_clickedPts[0], lastPoint);
        entityNewPtr->setColor(OdTvColorDef(150, 156, 154));
      }

    }
  }
  else
  {
    OdTvGeometryDataPtr geometryPtr = m_newGeometryId.openObject();
    if (geometryPtr.isNull())
      return;

    if (m_ClickState == kWaitingThirdClick)
    {
      OdTvCircleWedgeDataPtr circleWedgePtr = geometryPtr->getAsCircleWedge();
      if (!circleWedgePtr.isNull())
        circleWedgePtr->setMiddle(lastPoint);
    }
    else
    {
      OdTvPolylineDataPtr polylinePtr = geometryPtr->getAsPolyline();
      if (!polylinePtr.isNull())
        polylinePtr->setPoints(m_clickedPts[0], lastPoint);
    }
  }

  return;
}

//***************************************************************************//
// ' OdTvViewerEllipseDragger' methods implementation
//***************************************************************************//
OdTvViewerEllipseDragger::OdTvViewerEllipseDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModeId, OdTvModelId& tvActiveModeId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModeId, tvActiveModeId, pBaseView)
{
  m_LastState = kWaitingThirdClick;
}

OdTvDraggerPtr OdTvViewerEllipseDragger::finish(eDraggerResult& rc)
{
  //remove temporary help entity
  OdTvModelPtr model = m_tvDraggersModelId.openObject(OdTv::kForWrite);
  if (!model.isNull() && !m_tempEntityId.isNull())
  {
    model->removeEntity(m_tempEntityId);
  }

  return OdTvViewerBaseConstructDragger::finish(rc);
}

void OdTvViewerEllipseDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  //define color for the temp geometry 
  OdTvColorDef clTemp(150, 156, 154);

  //define the center of the ellipse (always first point)
  OdTvPoint ptCenter = m_clickedPts[0];

  //define the last drag point
  OdGePoint3d ptLast = m_clickedPts[(OdUInt16)m_ClickState];

  //1. Create or update temporary line
  if (m_tempEntityId.isNull())
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_tempEntityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr pTempEntity = m_tempEntityId.openObject(OdTv::kForWrite);
      m_tempGeometryId = pTempEntity->appendPolyline(ptCenter, ptLast);
      pTempEntity->setColor(clTemp);
    }
  }
  else
  {
    OdTvPolylineDataPtr pTempGeometry = m_tempGeometryId.openAsPolyline();
    if (!pTempGeometry.isNull())
    {
      pTempGeometry->setPoints(ptCenter, ptLast);
    }
  }

  //2. Create or update the ellipse
  if (m_ClickState == kWaitingThirdClick)
  {
    // define the ellipse parameters
    OdGeVector3d minorAxis = (m_clickedPts[1] - ptCenter).crossProduct(OdGeVector3d::kZAxis).normalize();
    OdGePoint3d thirdPoint = ptCenter + (ptLast - ptCenter).length() * minorAxis;

    if (bCreate)
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      m_entityId = modelPtr->appendEntity();
      {
        //create ellipse
        OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
        entityNewPtr->setColor(m_baseColor);
        m_newGeometryId = entityNewPtr->appendEllipse(ptCenter, m_clickedPts[1], thirdPoint);
      }
    }
    else
    {
      OdTvEllipseDataPtr pEllipsePtr = m_newGeometryId.openAsEllipse();
      if (!pEllipsePtr.isNull())
      {
        if (thirdPoint != ptCenter)
          pEllipsePtr->setMinor(thirdPoint);
      }
    }
  }
}

//***************************************************************************//
// 'OdTvViewerBoxDragger' methods implementation
//***************************************************************************//
OdTvViewerBoxDragger::OdTvViewerBoxDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  m_LastState = kWaitingThirdClick;
}

OdTvDraggerPtr OdTvViewerBoxDragger::finish(eDraggerResult& rc)
{
  //remove temporary help entity
  OdTvModelPtr model = m_tvDraggersModelId.openObject(OdTv::kForWrite);
  if (!model.isNull() && !m_tempEntityId.isNull())
  {
    model->removeEntity(m_tempEntityId);
  }

  return OdTvViewerBaseConstructDragger::finish(rc);
}

void  OdTvViewerBoxDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  //define color for the temp geometry 
  OdTvColorDef clTemp(150, 156, 154);

  //define the base point
  OdGePoint3d ptBase = m_clickedPts[1];

  //define the last drag point
  OdGePoint3d ptLast = m_clickedPts[(OdUInt16)m_ClickState];

  // check the view direction
  bool bWorldView = false;
  OdTvVector eyeVector = pView->position() - pView->target();
  if (eyeVector.isEqualTo(OdTvVector::kZAxis) || eyeVector.isEqualTo(-OdTvVector::kZAxis))
    bWorldView = true;

  //1. Create or update temporary line
  if (bWorldView && m_ClickState == m_LastState)
  {
    if (m_tempEntityId.isNull())
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      m_tempEntityId = modelPtr->appendEntity();
      {
        OdTvEntityPtr pTempEntity = m_tempEntityId.openObject(OdTv::kForWrite);
        m_tempGeometryId = pTempEntity->appendPolyline(ptBase, ptLast);
        pTempEntity->setColor(clTemp);
      }
    }
    else
    {
      OdTvPolylineDataPtr pTempGeometry = m_tempGeometryId.openAsPolyline();
      if (!pTempGeometry.isNull())
      {
        pTempGeometry->setPoints(ptBase, ptLast);
      }
    }
  }

  //2. Update the geometry and calculate the height of the cylinder
  double height = 0.001;
  if (m_ClickState == kWaitingThirdClick)
  {
    // remove polygon if need
    OdTvPolygonDataPtr pPolygon = m_newGeometryId.openAsPolygon();
    if (!pPolygon.isNull())
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      modelPtr->removeEntity(m_entityId);
      bCreate = true;
    }

    if (!bWorldView)
      height = getDistance(ptBase, ptLast);
    else
      height = (ptLast - ptBase).length();
  }

  //3. Create or update the geometry of the shell
  OdTvPointArray pts;
  pts.resize(4);

  pts[0] = m_clickedPts[0];
  pts[2] = ptBase;

  pts[1].x = pts[0].x;
  pts[3].x = pts[2].x;
  pts[1].y = pts[2].y;
  pts[3].y = pts[0].y;
  pts[1].z = pts[3].z = pts[0].z;

  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);

      if (m_ClickState == kWaitingThirdClick)
      {
        OdTvPoint centerPoint = OdTvPoint(m_clickedPts[0].x + (ptBase.x - m_clickedPts[0].x) / 2.,
          m_clickedPts[0].y + (ptBase.y - m_clickedPts[0].y) / 2., 0.);
        m_newGeometryId = m_entityId.openObject(OdTv::kForWrite)->appendBox(centerPoint, m_clickedPts[0].x - ptBase.x, 
          m_clickedPts[0].y - ptBase.y, 0.);
      }
      else
      {
        m_newGeometryId = entityNewPtr->appendPolygon(pts);
      }
    }
  }
  else
  {
    if (m_ClickState == kWaitingThirdClick)
    {
      OdTvBoxDataPtr boxPtr = m_newGeometryId.openAsBox();
      if (!boxPtr.isNull())
      {
        OdTvPoint centerPoint = boxPtr->getCenterPoint();
        boxPtr->setHeight(height);
        boxPtr->setCenterPoint(OdTvPoint(centerPoint.x, centerPoint.y, height / 2.));
      }
    }
    else if (m_ClickState == kWaitingSecondClick)
    {
      OdTvPolygonDataPtr polygonPtr = m_newGeometryId.openAsPolygon();
      if (!polygonPtr.isNull())
        polygonPtr->setPoints(pts);
    }
  }

  return;
}

//***************************************************************************//
// 'OdTvViewerSphereDragger' methods implementation
//***************************************************************************//
OdTvViewerSphereDragger::OdTvViewerSphereDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
}

void  OdTvViewerSphereDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  // calculate radius
  OdGeVector3d radius = (m_clickedPts[1] - m_clickedPts[0]);
  double sphereRadius = radius.length();

  // set sphere basis vectors
  OdTvVector axis, primeMeridian;
  axis.set(0., 1., 0);
  primeMeridian.set(1., 0., 0.);

  //update or create entity
  if (bCreate)
  {
    if (m_entityId.isNull())
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      m_entityId = modelPtr->appendEntity();
    }
   
    OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
    entityNewPtr->setColor(m_baseColor);

    m_newGeometryId = entityNewPtr->appendSphere(m_clickedPts[0], sphereRadius, axis, primeMeridian);
    m_newGeometryId.openObject()->setTargetDisplayMode(OdTvGeometryData::kEveryWhere);
  }
  else
  {
    OdTvSphereDataPtr pSphere = m_newGeometryId.openAsSphere();
    if (!pSphere.isNull())
    {
      pSphere->setParam(m_clickedPts[0], sphereRadius, axis, primeMeridian);
    }
  }
  return;
}

//***************************************************************************//
// 'OdTvViewerCylinderDragger' methods implementation
//***************************************************************************//

OdTvViewerCylinderDragger::OdTvViewerCylinderDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  m_LastState = kWaitingThirdClick;
}

OdTvDraggerPtr OdTvViewerCylinderDragger::finish(eDraggerResult& rc)
{
  //remove temporary help entity
  OdTvModelPtr model = m_tvDraggersModelId.openObject(OdTv::kForWrite);
  if (!model.isNull() && !m_tempEntityId.isNull())
  {
    model->removeEntity(m_tempEntityId);
  }

  return OdTvViewerBaseConstructDragger::finish(rc);
}

void  OdTvViewerCylinderDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  //define color for the temp geometry 
  OdTvColorDef clTemp(150, 156, 154);

  //define the center
  OdGePoint3d ptCenter = m_clickedPts[0];

  //define the last drag point
  OdGePoint3d ptLast = m_clickedPts[(OdUInt16)m_ClickState];

  // calculate radius
  double dRadius = (m_clickedPts[1] - ptCenter).length();

  // check the view direction
  bool bWorldView = false;
  OdTvVector eyeVector = pView->position() - pView->target();
  if (eyeVector.isEqualTo(OdTvVector::kZAxis) || eyeVector.isEqualTo(-OdTvVector::kZAxis))
    bWorldView = true;

  //1. Create or update temporary line
  if (bWorldView && m_ClickState == m_LastState)
  {
    if (m_tempEntityId.isNull())
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      m_tempEntityId = modelPtr->appendEntity();
      {
        OdTvEntityPtr pTempEntity = m_tempEntityId.openObject(OdTv::kForWrite);
        m_tempGeometryId = pTempEntity->appendPolyline(m_clickedPts[0], ptLast);
        pTempEntity->setColor(clTemp);
      }
    }
    else
    {
      OdTvPolylineDataPtr pTempGeometry = m_tempGeometryId.openAsPolyline();
      if (!pTempGeometry.isNull())
      {
        pTempGeometry->setPoints(ptCenter, ptLast);
      }
    }
  }

  //2. Update the geometry and calculate the height of the cylinder
  double height = 0.001;
  if (m_ClickState == kWaitingThirdClick)
  {
    // remove circle if need
    OdTvCircleDataPtr pCircle = m_newGeometryId.openAsCircle();
    if (!pCircle.isNull())
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      modelPtr->removeEntity(m_entityId);
      bCreate = true;
    }

    if (!bWorldView)
      height = getDistance(ptCenter, ptLast);
    else
      height = (ptLast - ptCenter).length();
  }

  OdTvPoint ptCylinderLastPoint = ptCenter + OdTvVector::kZAxis * height;

  //3. Create or update the geometry of the cylinder
  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);

      if (m_ClickState == kWaitingThirdClick)
        m_newGeometryId = entityNewPtr->appendCylinder(ptCenter, ptCylinderLastPoint, dRadius);
      else
        m_newGeometryId = entityNewPtr->appendCircle(ptCenter, dRadius, OdGeVector3d::kZAxis);
    }
  }
  else
  {
    if (m_ClickState == kWaitingThirdClick)
    {
      OdTvCylinderDataPtr cylinderPtr = m_newGeometryId.openAsCylinder();
      if (!cylinderPtr.isNull())
      {
        OdTvPointArray points;
        points.append(ptCenter);
        points.append(ptCylinderLastPoint);
        cylinderPtr->setPoints(points);
      }
    }
    else if (m_ClickState == kWaitingSecondClick)
    {
      OdTvCircleDataPtr circlePtr = m_newGeometryId.openAsCircle();
      if (!circlePtr.isNull())
        circlePtr->setRadius(dRadius);
    }
  }

  return;
}

//***************************************************************************//
// 'OdTvViewerRayDragger' methods implementation
//***************************************************************************//

OdTvViewerRayDragger::OdTvViewerRayDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
}

void OdTvViewerRayDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  //update or create entity
  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);
      //create ray
      m_newGeometryId = entityNewPtr->appendInfiniteLine(m_clickedPts[0], m_clickedPts[1], OdTvInfiniteLineData::kRay);
      // marks that this entity should be autoregenerated
      entityNewPtr->setAutoRegen(true);
    }
  }
  else
  {
    OdTvInfiniteLineDataPtr rayPtr = m_newGeometryId.openAsInfiniteLine();
    if (!rayPtr.isNull())
      rayPtr->setSecond(m_clickedPts[1]);
  }
}

//***************************************************************************//
// 'OdTvViewerXlineDragger' methods implementation
//***************************************************************************//

OdTvViewerXlineDragger::OdTvViewerXlineDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{

}

void OdTvViewerXlineDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  //update or create entity
  if (bCreate)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
      entityNewPtr->setColor(m_baseColor);
      //create xline
      m_newGeometryId = entityNewPtr->appendInfiniteLine(m_clickedPts[0], m_clickedPts[1], OdTvInfiniteLineData::kLine);
    }
  }
  else
  {
    OdTvInfiniteLineDataPtr xlinePtr = m_newGeometryId.openAsInfiniteLine();
    if (!xlinePtr.isNull())
      xlinePtr->setSecond(m_clickedPts[1]);
  }
}

//***************************************************************************//
// 'OdTvViewerEllipticArcDragger' methods implementation
//***************************************************************************//

OdTvViewerEllipticArcDragger::OdTvViewerEllipticArcDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  m_LastState = kWaitingFifthClick;
}

OdTvDraggerPtr OdTvViewerEllipticArcDragger::finish(eDraggerResult& rc)
{
  //remove temporary help entity
  OdTvModelPtr model = m_tvDraggersModelId.openObject(OdTv::kForWrite);
  if (!model.isNull())
  {
    model->removeEntity(m_tempEntityId);
  }

  return OdTvViewerBaseConstructDragger::finish(rc);
}

void OdTvViewerEllipticArcDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  //define color for the temp geometry 
  OdTvColorDef clTemp(150, 156, 154);

  //define the center of the ellipse (always first point)
  OdTvPoint ptCenter = (m_clickedPts[0] + m_clickedPts[1].asVector()) / 2.;

  //define the last drag point
  OdGePoint3d ptLast = m_clickedPts[(OdUInt16)m_ClickState];

  //1. Create or update temporary line
  if (m_tempEntityId.isNull())
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_tempEntityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr pTempEntity = m_tempEntityId.openObject(OdTv::kForWrite);
      m_tempGeometryId = pTempEntity->appendPolyline(m_clickedPts[0], ptLast);
      pTempEntity->setColor(clTemp);
    }
  }
  else
  {
    OdTvPolylineDataPtr pTempGeometry = m_tempGeometryId.openAsPolyline();
    if (!pTempGeometry.isNull())
    {
      if (m_ClickState > kWaitingSecondClick)
      {
        pTempGeometry->setPoints(ptCenter, ptLast);
      }
      else
      {
        pTempGeometry->setPoints(m_clickedPts[0], ptLast);
      }
    }
  }

  //2. Create or update the elliptic arc
  if (m_ClickState == kWaitingThirdClick) // in this case we have deal with simple ellipse
  {
    // define the ellipse parameters
    OdGeVector3d minorAxis = (m_clickedPts[1] - ptCenter).crossProduct(OdGeVector3d::kZAxis).normalize();
    OdGePoint3d thirdPoint = ptCenter + (ptLast - ptCenter).length() * minorAxis;

    if (bCreate)
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      m_entityId = modelPtr->appendEntity();
      {
        //create ellipse
        OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
        entityNewPtr->setColor(m_baseColor);
        m_newGeometryId = entityNewPtr->appendEllipse(ptCenter, m_clickedPts[1], thirdPoint);
      }
    }
    else
    {
      OdTvEllipseDataPtr pEllipsePtr = m_newGeometryId.openAsEllipse();
      if (!pEllipsePtr.isNull())
      {
        if (thirdPoint != ptCenter)
          pEllipsePtr->setMinor(thirdPoint);
      }
    }
  }
  else if (m_ClickState == m_LastState) // in this case we have deal already with an elliptic arc
  {
    // check that we have an ellipse and should change it to an elliptic arc
    OdTvEllipseDataPtr pEllipsePtr = m_newGeometryId.openAsEllipse();
    if (!pEllipsePtr.isNull())
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      modelPtr->removeEntity(m_entityId);
      bCreate = true;
    }

    // define the elliptic arc parameters
    OdTvVector majorAxisNorm = (m_clickedPts[1] - ptCenter).crossProduct(OdTvVector::kZAxis).normalize();
    OdGePoint3d thirdPoint = ptCenter + (m_clickedPts[2] - ptCenter).length() * majorAxisNorm;

    OdTvVector majorAxis = (m_clickedPts[1] - ptCenter);
    OdTvVector minorAxis = (thirdPoint - ptCenter);
    OdTvVector currentAxis = (ptLast - ptCenter);

    double radiusRatio = minorAxis.length() / majorAxis.length();
    currentAxis.normalizeGetLength();

    double angle = paramFromAngle(angleToCCW(currentAxis, majorAxis), radiusRatio);

    if (bCreate)
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      m_entityId = modelPtr->appendEntity();
      {
        //create an elliptic arc
        OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
        m_newGeometryId = entityNewPtr->appendEllipticArc(ptCenter, m_clickedPts[1], thirdPoint, angle, 0.);
      }
    }
    else
    {
      OdTvEllipticArcDataPtr pEllipseArcPtr = m_newGeometryId.openAsEllipticArc();
      if (!pEllipseArcPtr.isNull())
      {
        pEllipseArcPtr->setEnd(angle);
      }
    }
  }
}

double OdTvViewerEllipticArcDragger::calibrateAngle(double val, double input)
{
  if (input > val)
  {
    if ((input - val) < OdaPI)
      return val;
    else
      return val + OdRound((input - val) / Oda2PI)*Oda2PI;
  }
  else
  {
    if ((val - input) < OdaPI)
      return val;
    else
      return val - OdRound((val - input) / Oda2PI)*Oda2PI;
  }
}

double OdTvViewerEllipticArcDragger::paramFromAngle(double angle, double radiusRatio)
{
  if (OdZero(angle))
    return 0.;
  if (OdZero(angle - Oda2PI))
    return Oda2PI;

  return calibrateAngle(OD_ATAN2(sin(angle), radiusRatio*cos(angle)), angle);
}

double OdTvViewerEllipticArcDragger::angleToCCW(const OdTvVector& mineVect, const OdTvVector& vect) const
{
  return mineVect.convert2d().crossProduct(vect.convert2d()) > 0.0 ? mineVect.angleTo(vect) : -mineVect.angleTo(vect);
}

//***************************************************************************//
// 'OdTvViewerNurbsDragger' methods implementation
//***************************************************************************//
OdTvViewerNurbsDragger::OdTvViewerNurbsDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
  m_LastState = kWaitingInfiniteClick;
  m_degree = 2;
  m_knots.resize(3);
  m_clickedPts.resize(2);
}

eDraggerResult OdTvViewerNurbsDragger::nextpoint(int x, int y)
{
  eDraggerResult rc = OdTvViewerBaseConstructDragger::nextpoint(x, y);

  // resize m_clickedPts and m_knots (in case if we have already 2 points)
  if (m_ClickState > kWaitingSecondClick)
  {
    m_clickedPts.resize((int)m_ClickState + 1);

    updateKnotsAndDegree(m_clickedPts.length());
  }

  return rc;
}

OdTvDraggerPtr OdTvViewerNurbsDragger::finish(eDraggerResult& rc)
{
  //remove temporary help entity
  OdTvModelPtr model = m_tvDraggersModelId.openObject(OdTv::kForWrite);
  if (!model.isNull())
  {
    model->removeEntity(m_tempEntityId);
  }

  return OdTvViewerBaseConstructDragger::finish(rc);
}

eDraggerResult OdTvViewerNurbsDragger::processEnter()
{
  // need remove last point
  OdTvNurbsDataPtr pNurbs = m_newGeometryId.openAsNurbs();
  if (!pNurbs.isNull())
  {
    updateKnotsAndDegree(m_clickedPts.length()-1);

    pNurbs->set(m_degree, m_clickedPts.length() - 1, m_clickedPts.asArrayPtr(), NULL, m_knots.length(), m_knots.asArrayPtr());
  }

  //transfer last created entity to the main active model
  transferResultToActiveModel();
  m_bNeedTransferToActive = false;

  // release current entity (means final adding to the database)
  m_entityId.setNull();

  return kNeedFinishDragger;
}

void OdTvViewerNurbsDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  //define color for the temp geometry 
  OdTvColorDef clTemp(150, 156, 154);

  //1. Create or update temporary line
  if (m_tempEntityId.isNull())
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_tempEntityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr pTempEntity = m_tempEntityId.openObject(OdTv::kForWrite);
      m_tempGeometryId = pTempEntity->appendPolyline(m_clickedPts[0], m_clickedPts[1]);
      pTempEntity->setColor(clTemp);
    }
  }
  else
  {
    OdTvPolylineDataPtr pTempGeometry = m_tempGeometryId.openAsPolyline();
    if (!pTempGeometry.isNull())
    {
      pTempGeometry->setPoints(m_clickedPts.size(), m_clickedPts.asArrayPtr());
    }
  }

  //2. Create or update the nurbs
  if (m_ClickState > kWaitingSecondClick)
  {
    if (bCreate)
    {
      OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
      m_entityId = modelPtr->appendEntity();
      {
        OdTvEntityPtr entityNewPtr = m_entityId.openObject(OdTv::kForWrite);
        entityNewPtr->setColor(m_baseColor);
        //create nurbs
        m_newGeometryId = entityNewPtr->appendNurbs(m_degree, m_clickedPts.length(), m_clickedPts.asArrayPtr(), NULL, m_knots.length(), m_knots.asArrayPtr());
      }
    }
    else
    {
      OdTvNurbsDataPtr pNurbs = m_newGeometryId.openAsNurbs();
      if ( !pNurbs.isNull() )
      {
        pNurbs->set(m_degree, m_clickedPts.length(), m_clickedPts.asArrayPtr(), NULL, m_knots.length(), m_knots.asArrayPtr());
      }
    }
  }
}

void OdTvViewerNurbsDragger::updateKnotsAndDegree(unsigned int nPoints)
{
  m_degree = 1;
  if (nPoints > 2)
    m_degree = 2;
  if (nPoints > 3)
    m_degree = 3;

  m_knots.resize(nPoints + m_degree + 1);
  for (OdUInt32 i = 0; i < (OdUInt32)m_degree + 1; i++)
    m_knots[i] = 1.;
  for (OdUInt32 i = m_knots.length() - 1; i >= m_knots.length() - ((OdUInt32)m_degree + 1); i--)
    m_knots[i] = m_knots[m_knots.length() - (m_degree + 2)] + 1.;
}

//***************************************************************************//
// ' OdTvViewerRasterImageDragger' methods implementation
//***************************************************************************//
OdTvViewerRasterImageDragger::OdTvViewerRasterImageDragger(OdTvGsDeviceId& tvDeviceId, OdTvModelId& tvDraggersModelId, OdTvModelId& tvActiveModelId, OdTvUIBaseView *pBaseView)
  : OdTvViewerBaseConstructDragger(tvDeviceId, tvDraggersModelId, tvActiveModelId, pBaseView)
{
}

eDraggerResult OdTvViewerRasterImageDragger::nextpoint(int x, int y)
{
  eDraggerResult res = OdTvViewerBaseConstructDragger::nextpoint(x, y);

  if (res & kNeedFinishDragger)
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_entityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr pEntity = m_entityId.openObject(OdTv::kForWrite);

      OdTvPoint temp(m_clickedPts[0].x, m_clickedPts[1].y, m_clickedPts[0].z);
      OdTvPoint temp1(m_clickedPts[1].x, m_clickedPts[0].y, m_clickedPts[0].z);
      
      m_newGeometryId = pEntity->appendRasterImage(m_imageDef, m_clickedPts[0], temp1 - m_clickedPts[0], temp - m_clickedPts[0]);
    }
  }

  // release current entity (means final adding to the database)
  m_entityId.setNull();

  return res;
}

OdTvDraggerPtr OdTvViewerRasterImageDragger::finish(eDraggerResult& rc)
{
  //remove temporary help entity
  OdTvModelPtr model = m_tvDraggersModelId.openObject(OdTv::kForWrite);
  if (!model.isNull())
  {
    model->removeEntity(m_tempEntityId);
  }

  return OdTvViewerBaseConstructDragger::finish(rc);
}

bool OdTvViewerRasterImageDragger::loadImage(QString& imagePath)
{
  static int sequenceImageNumber = 1;

  // generate image name
  QString strCurTitle = QObject::tr("Image%1").arg(sequenceImageNumber++);
  //std::wstring wstrCurTitle(strCurTitle.toStdWString());
  QString2WString q2wTitle( strCurTitle );

  //try to get the database
  OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
  OdTvDatabaseId dbId = modelPtr->getDatabase();
  OdTvDatabasePtr dbPtr = dbId.openObject(OdTv::kForWrite);
  if (dbPtr.isNull())
    return false;

  //create the image definition
  //std::wstring wImagePath(imagePath.toStdWString());
  QString2WString q2wPath( imagePath );
  OdTvResult rc;
  m_imageDef = dbPtr->createRasterImage(q2wTitle.wstr().c_str(), q2wPath.wstr().c_str(), true, &rc);

  if (rc != tvOk)
    return false;

  return true;
}

void  OdTvViewerRasterImageDragger::updateGeometry(bool bCreate, bool bFromNextPoint)
{
  OdTvGsViewPtr pView = getActiveTvViewPtr();
  if (pView.isNull())
    return;

  OdTvPointArray pts;
  pts.resize(5);

  pts[0] = m_clickedPts[0];
  pts[1].x = m_clickedPts[0].x;   pts[1].y = m_clickedPts[1].y;   pts[1].z = m_clickedPts[0].z;
  pts[2] = m_clickedPts[1];
  pts[3].x = m_clickedPts[1].x;   pts[3].y = m_clickedPts[0].y;   pts[3].z = m_clickedPts[0].z;
  pts[4] = m_clickedPts[0];

  //1. Create or update temporary rectangle
  if (m_tempEntityId.isNull())
  {
    OdTvModelPtr modelPtr = m_tvDraggersModelId.openObject(OdTv::kForWrite);
    m_tempEntityId = modelPtr->appendEntity();
    {
      OdTvEntityPtr pTempEntity = m_tempEntityId.openObject(OdTv::kForWrite);
      pTempEntity->setColor(m_baseColor);
      m_tempGeometryId = pTempEntity->appendPolyline(pts);
    }
  }
  else
  {
    OdTvPolylineDataPtr pTempGeometry = m_tempGeometryId.openAsPolyline();
    if (!pTempGeometry.isNull())
    {
      pTempGeometry->setPoints(pts);
    }
  }

  return;
}
