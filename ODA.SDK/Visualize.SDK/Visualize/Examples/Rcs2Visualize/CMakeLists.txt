#
# Rcs2Visualize library
#

visualize_sources(${TV_RCS2VISUALIZE_LIB}
           Rcs2Visualize.cpp
           Rcs2Visualize.h
           Rcs2VisualizeDef.h
           ../Common/TvGeomCollector.cpp
           ../Common/TvGeomCollector.h
		   ../Common/TvFilerTimer.cpp
           ../Common/TvFilerTimer.h
		   ../Common/TvDatabaseUtils.cpp
           ../Common/TvDatabaseUtils.h		   
)

include_directories(
           ${VISUALIZE_ROOT}/Include
           ../Common
           ${TDRAWING_ROOT}/Include
                   )

if(ODA_SHARED AND MSVC)
visualize_sources(${TV_RCS2VISUALIZE_LIB}
                  Rcs2Visualize.rc
                  )
endif(ODA_SHARED AND MSVC)
add_definitions(-DTV_RCS2VISUALIZE_EXPORTS)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

visualize_tx(${TV_RCS2VISUALIZE_LIB} ${TD_GE_LIB} ${TV_VISUALIZE_LIB} ${TD_ROOT_LIB} ${TD_ALLOC_LIB})

visualize_project_group(Rcs2Visualize "Visualize/Examples")
