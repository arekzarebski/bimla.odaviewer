#
# Stl2Visualize library
#

visualize_sources(${TV_STL2VISUALIZE_LIB}
           Stl2Visualize.cpp
           Stl2Visualize.h
           Stl2VisualizeDef.h
		   StlProperties.cpp
		   StlProperties.h
           ../Common/TvFilerTimer.cpp
           ../Common/TvFilerTimer.h
		   ../Common/TvDatabaseUtils.cpp
           ../Common/TvDatabaseUtils.h		   
)

include_directories(
        ${VISUALIZE_ROOT}/Include
		    ${TCOMPONENTS_ROOT}/Imports/STLImport/Include
        ../Common
                   )

if(ODA_SHARED AND MSVC)
visualize_sources(${TV_STL2VISUALIZE_LIB}
                  Stl2Visualize.rc
                  )
endif(ODA_SHARED AND MSVC)
add_definitions(-DTV_STL2VISUALIZE_EXPORTS)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

visualize_tx(${TV_STL2VISUALIZE_LIB} 
	${TD_RX_CDA_LIB}
	${TD_GE_LIB} 
	${TV_VISUALIZE_LIB} 
	${TD_ROOT_LIB} 
	${TD_ALLOC_LIB}
	${TD_STL_IMPORT_LIB}
)

visualize_project_group(Stl2Visualize "Visualize/Examples")
