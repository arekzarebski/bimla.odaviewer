/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _TV_IFC2VISUALIZE_H_
#define _TV_IFC2VISUALIZE_H_


#include "TD_PackPush.h"

#include "RxDispatchImpl.h"
#include "RxObject.h"

#include "ExSystemServices.h"
#include "ExIfcHostAppServices.h"
#include "IfcCore.h"
#include "IfcModel.h"

#include "OdFileBuf.h"
#include "IfcGsManager.h"

#include "TvVisualizeFiler.h"

namespace IFC2Visualize {
  /** \details
  This class implements the properties of the ifc loader
  */
  class OdTvVisualizeIfcFilerProperties : public OdRxDispatchImpl<>
  {
    enum ParamFlags
    {
      kObjectNaming = 1,                // Give the names for the tv entities according to the file objects name (like AcDbCircle etc). If object names exist
      kStoreSource = 2,                 // Store source objects (OdDbEntities)
      kClearEmptyObjects = 4,           // Clear empty objects
      kRearrangeObjects = 8,            // Rearrange objects
      kNeedCDATree      = 16,           // Need create CDA tree
      kNeedCollectPropertiesInCDA = 32, // Need collect native properties in CDA nodes
      kUseCustomBgColor = 64            // Set custom backround color
    };

  public:
    OdTvVisualizeIfcFilerProperties();
    virtual ~OdTvVisualizeIfcFilerProperties();

    ODRX_DECLARE_DYNAMIC_PROPERTY_MAP(OdTvVisualizePrcFilerProperties);
    static OdRxDictionaryPtr createObject();

    void      setDefaultColor(OdIntPtr pDefColor);
    OdIntPtr  getDefaultColor() const;

    void      setBgColor(OdIntPtr pDefColor);
    OdIntPtr  getBgColor() const;

    void      setPalette(OdIntPtr palette); //palette should have 256 colors
    OdIntPtr  getPalette() const;

    void      setDCRect(OdIntPtr rect);   //should be pointer to OdTvDCRect
    OdIntPtr  getDCRect() const;

    void      setObjectNaming(bool bSet) { SETBIT(m_flags, kObjectNaming, bSet); }
    bool      getObjectNaming() const { return GETBIT(m_flags, kObjectNaming); }

    void      setStoreSourceObjects(bool bSet) { SETBIT(m_flags, kStoreSource, bSet); }
    bool      getStoreSourceObjects() const { return GETBIT(m_flags, kStoreSource); }

    void      setClearEmptyObjects(bool bSet) { SETBIT(m_flags, kClearEmptyObjects, bSet); }
    bool      getClearEmptyObjects() const { return GETBIT(m_flags, kClearEmptyObjects); }

    void      setRearrangeObjects(bool bSet) { SETBIT(m_flags, kRearrangeObjects, bSet); }
    bool      getRearrangeObjects() const { return GETBIT(m_flags, kRearrangeObjects); }

    void      setNeedCDATree(bool bSet) { SETBIT(m_flags, kNeedCDATree, bSet); }
    bool      getNeedCDATree() const { return GETBIT(m_flags, kNeedCDATree); }

    void      setNeedCollectPropertiesInCDA(bool bSet) { SETBIT(m_flags, kNeedCollectPropertiesInCDA, bSet); }
    bool      getNeedCollectPropertiesInCDA() const { return GETBIT(m_flags, kNeedCollectPropertiesInCDA); }

    void      setUseCustomBgColor(bool bSet) { SETBIT(m_flags, kUseCustomBgColor, bSet); }
    bool      getUseCustomBgColor() const { return GETBIT(m_flags, kUseCustomBgColor); }

    void      setAppendTransform(OdIntPtr pTransform);
    OdIntPtr  getAppendTransform() const;

    void      setFeedbackForChooseCallback(OdIntPtr pCallback);
    OdIntPtr  getFeedbackForChooseCallback() const;

    void setDeviation(double deviation);
    double getDeviation() const;

    void setMinPerCircle(OdUInt16 minPerCircle);
    OdUInt16 getMinPerCircle() const;

    void setMaxPerCircle(OdUInt16 maxPerCircle);
    OdUInt16 getMaxPerCircle() const;

    void setModelerType(OdUInt8 type);
    OdUInt8 getModelerType() const;

  protected:

    ODCOLORREF              m_defaultColor;           // default color which is set to the entity
    ODCOLORREF              m_bgColor;                // background color which is set to the scene
    const ODCOLORREF*       m_pPalette;               // Palette to be used. If NULL, one of two default palettes will be used depending on background color. Should have 256 colors
    OdTvDCRect              m_importRect;             // Output rectangle. Used for correct import of some specific objects (OLE image, camera). The normal way is to set the output window size
    OdUInt8                 m_flags;                  // Different options
    OdTvMatrix              m_appendTransform;        // Transform for the append
    OdTvFeedbackForChooseCallback  m_pCallback;       // Callback for choose

    double                  m_deviation;
    OdUInt16                m_minPerCircle;
    OdUInt16                m_maxPerCircle;
    OdUInt8                 m_modelerType;
  };
  typedef OdSmartPtr<OdTvVisualizeIfcFilerProperties> OdTvVisualizeIfcFilerPropertiesPtr;
}

class TvRxSystemServicesImpl : public OdRxSystemServices
{
public:
  TvRxSystemServicesImpl() {};

  OdInt64 getFileCTime(const OdString& filename) { return 0; };
  OdInt64 getFileMTime(const OdString& filename) { return 0; };
  OdInt64 getFileSize(const OdString& filename) { return 0; };

  OdString formatMessage(unsigned int formatId, va_list* argList = 0) { return OD_T(""); };

  /** \details
  Returns the system code page.

  \remarks
  On Windows platforms system code page is initialized based on computer's
  Regional Settings. On other platforms with CP_UNDEFINED.
  It can be altered by setSystemCodePage()
  */
  OdCodePageId systemCodePage() const { return CP_UNDEFINED; };

  OdResult getEnvVar(const OdString &varName, OdString &value)
  {
    return eNotImplemented;
  };

  OdResult setEnvVar(const OdString &varName, const OdString &newValue)
  {
    return eNotImplemented;
  };
};

/** \details
This class is service for getting database for ifc file
*/
class OdTvIfc2VisService : public OdIfcHostAppServices, public OdDbHostAppProgressMeter, public TvRxSystemServicesImpl
{
public:
  virtual void start(const OdString& displayString = OdString::kEmpty) { };
  virtual void stop() { };
  virtual void meterProgress() { };
  virtual void setLimit(int max) { };

  virtual OdHatchPatternManager* patternManager() { return NULL; }

protected:
  ODRX_USING_HEAP_OPERATORS(TvRxSystemServicesImpl);

};

/** \details
This class wraps ifc system services used inside the import process
*/
class OdTvVisualizeIfcFilerDbSource
{
protected:
  OdStaticRxObject<OdTvIfc2VisService> m_svcs;

public:
  virtual OdIfcFilePtr readFile(const OdString &file);
};

/** \details
This class is ifc loader (to the Visualize database)
*/
class OdTvVisualizeIfcFiler : public OdTvVisualizeFiler
{
public:

  OdTvVisualizeIfcFiler();

  virtual OdRxDictionaryPtr properties() { return m_properties; }
  virtual OdTvDatabaseId loadFrom(const OdString& filePath, OdTvFilerTimeProfiling* pProfileRes = NULL, OdTvResult* rc = NULL) const;
  virtual OdTvDatabaseId loadFrom(OdStreamBuf* pBuffer, OdTvFilerTimeProfiling* pProfileRes = NULL, OdTvResult* rc = NULL) const;
  virtual OdTvDatabaseId loadFrom(OdDbBaseDatabase*  pDatabase, OdTvFilerTimeProfiling* pProfileRes = NULL, OdTvResult* rc = NULL) const;
  virtual OdTvDatabaseId generate(OdTvFilerTimeProfiling* pProfileRes = NULL) const;
  virtual OdTvModelId appendFrom(const OdTvDatabaseId& databaseId, const OdString& filePath, OdTvFilerTimeProfiling* pProfileRes = NULL, OdTvResult* rc = NULL) const;
  virtual OdTvModelId appendFrom(const OdTvDatabaseId& databaseId, OdStreamBuf* pBuffer, OdTvFilerTimeProfiling* pProfileRes = NULL, OdTvResult* rc = NULL) const;
  virtual OdTvModelId appendFrom(const OdTvDatabaseId& databaseId, OdDbBaseDatabase*  pDatabase, OdTvFilerTimeProfiling* pProfileRes = NULL, OdTvResult* rc = NULL) const;

//#define ENABLE_IFC2VIS_NATIVE_PROPERTIES
#ifdef ENABLE_IFC2VIS_NATIVE_PROPERTIES
  //Native properties support
  virtual bool                    hasNativePropertiesSupport() const { return true; }
  virtual OdTvResult              startActionsWithNativeProperties(const OdString& sFilePath, bool bPartial);
  virtual bool                    isActionsWithNativePropertiesStarted(const OdString& sFilePath);
  virtual OdTvResult              endActionsWithNativeProperties();
  virtual OdRxMemberIteratorPtr   getNativePropertiesIterator(OdUInt64 dbHandle, OdTvResult* rc = NULL);
  virtual OdRxValue               getNativePropertyValue(OdUInt64 dbHandle, const OdRxPropertyPtr& pProperty, bool* bReadOnly = NULL, OdTvResult* rc = NULL);
  virtual OdRxValueIteratorPtr    getNativeCollectionPropertyIterator(OdUInt64 dbHandle, const OdRxCollectionPropertyPtr& pCollectionProperty, bool* bReadOnly = NULL, OdTvResult* rc = NULL);
  virtual OdTvResult              setNativePropertyValue(OdUInt64 dbHandle, OdRxPropertyPtr& pProperty, const OdRxValue& value);
  virtual OdDbBaseDatabase*       getNativeDatabase(OdTvResult* rc = NULL) const;
#endif

  int getChoosedRepresentationContexts(OdIfcFilePtr pIfcFile, OdTvFilerFeedbackForChooseObject & filerFeedbackForChoose, bool& bCanceled) const;

protected:

  mutable OdDAIObjectIds m_selContexts;

  OdTvModelId import(OdTvDatabaseId &tvDbId, OdStreamBuf* pBuffer, OdTvFilerTimeProfiling* pProfileRes /*= NULL*/, OdTvResult* rc /*= NULL*/) const;

private:

  OdTvGsDeviceId  getActiveTvDevice(OdTvDatabaseId& tvDbId, int idDevice) const;
  int             getActiveViewId(OdGsIfcLayoutHelper* pLayoutHelper) const;
  OdTvGsViewId    getActiveTvView(OdTvGsDeviceId& tvDeviceId, int iActViewViewInd) const;
  OdTvGsViewId    getActiveTvView(OdTvDatabaseId& dbId, int iActViewViewInd) const;

  void            createCommonDataAccessTree(OdTvDatabasePtr pTvDb, OdDbBaseDatabase *pDatabase, const OdString& strTreeName) const;
  int             odSelectRepresentationContextsByDefault(OdIfcModelPtr pModel) const;

private:

  IFC2Visualize::OdTvVisualizeIfcFilerPropertiesPtr m_properties;

  OdIfcFilePtr m_pIfcFile;
  OdIfcModelPtr m_pIfcModel;
  OdRxModulePtr m_pRxPropertiesModule;
  OdRxModulePtr m_pIfcCoreModule;
  OdSharedPtr<OdTvVisualizeIfcFilerDbSource> m_pFilerPtr;
};

/** \details
This class is ifc loader module implementation
*/
class OdTvVisualizeIfcFilerModule : public OdTvVisualizeFilerModule
{
public:
  virtual OdTvVisualizeFilerPtr getVisualizeFiler() const;

  void initApp();
  void uninitApp();
};

#include "TD_PackPop.h"

#endif // _TV_IFC2VISUALIZE_H_
