#
# Ifc2Visualize library
#
if(IFC_ROOT)

visualize_sources(${TV_IFC2VISUALIZE_LIB}

  Ifc2Visualize.h
  Ifc2Visualize.cpp
  Ifc2VisualizeDef.h
  ../Common/TvFilerTimer.cpp
  ../Common/TvFilerTimer.h
  ../Common/TvDatabaseCleaner.cpp
  ../Common/TvDatabaseCleaner.h
  ../Common/TvDatabaseUtils.cpp
  ../Common/TvDatabaseUtils.h
)

include_directories(

  ${VISUALIZE_ROOT}/Include
  ../Common

  ${IFC_ROOT}/Include
  ${IFC_ROOT}/Include/sdai
  ${IFC_ROOT}/Include/Common
  ${IFC_ROOT}/Extensions/ExServices
  ${IFC_ROOT}/Examples/Common/

  ${TDRAWING_ROOT}/Include
  ${TKERNEL_ROOT}/Include
  ${TKERNEL_ROOT}/Extensions/ExServices
)

if(ODA_SHARED AND MSVC)
tcomponents_sources(${TV_IFC2VISUALIZE_LIB}
  Ifc2Visualize.rc
)
endif(ODA_SHARED AND MSVC)

add_definitions(-DTV_IFC2VISUALIZE_EXPORTS)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

visualize_tx(${TV_IFC2VISUALIZE_LIB}

  ${IFCGEOM_LIB}

  ${TV_VISUALIZE_LIB}
  ${IFCCORE_LIB}
  ${SPF_LIB}
  ${SDAI_LIB}
  ${TD_GI_LIB}
  ${TD_GS_LIB}
  ${TD_GE_LIB}
  ${TD_ROOT_LIB}
  ${TD_DBROOT_LIB}
  ${TD_KEY_LIB}
  ${TD_ALLOC_LIB}
)

visualize_project_group(Ifc2Visualize "Visualize/Examples")

else(IFC_ROOT)

message("Warning: IFC_ROOT undefined, Ifc2Visualize disabled")

endif(IFC_ROOT)
