/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

// ODA Platform
#include "OdaCommon.h"
#include "RxDynamicModule.h"
#include "RxVariantValue.h"
#include "RxObject.h"
#include "RxInit.h"

#include "HatchPatternManager.h"

#include "TvFactory.h"
#include "TvFilerTimer.h"
#include "TvModuleNames.h"
#include "TvDatabaseCleaner.h"
#include "TvDatabaseUtils.h"
#include "ColorMapping.h"

#include "Ifc2Visualize.h"
#include "IfcGiContext.h"
#include "IfcAttributesEnum.h"
#include "IfcDeviationParams.h"
#include "IfcModelContext.h"
#include "IfcFile.h"
#include "Entities/IfcProject.h"

using namespace OdIfc;
using namespace IFC2Visualize;

//***************************************************************************//
// 'OdTvVisualizePrcFilerProperties' methods implementation
//***************************************************************************//

OdTvVisualizeIfcFilerProperties::OdTvVisualizeIfcFilerProperties()
  : m_flags(0)
  , m_pPalette(0)
  , m_pCallback(NULL)
  , m_minPerCircle(8)
  , m_maxPerCircle(128)
  , m_deviation(0.5)
  , m_modelerType(0)
{
  m_importRect.xmax = 0;
  m_importRect.xmin = 0;
  m_importRect.ymax = 0;
  m_importRect.ymin = 0;
}

OdRxDictionaryPtr OdTvVisualizeIfcFilerProperties::createObject()
{
  return OdRxObjectImpl<OdTvVisualizeIfcFilerProperties, OdRxDictionary>::createObject();
}

void OdTvVisualizeIfcFilerProperties::setDefaultColor(OdIntPtr pDefColor)
{
  ODCOLORREF* pColor = (ODCOLORREF*)(pDefColor);
  if (!pColor)
  {
    ODA_ASSERT(false);
  }
  m_defaultColor = *pColor;
}

OdIntPtr OdTvVisualizeIfcFilerProperties::getDefaultColor() const
{
  return (OdIntPtr)(&m_defaultColor);
}

void OdTvVisualizeIfcFilerProperties::setPalette(OdIntPtr palette)
{
  const ODCOLORREF* pPalette = (const ODCOLORREF*)(palette);

  m_pPalette = pPalette;
}

void OdTvVisualizeIfcFilerProperties::setBgColor(OdIntPtr pDefColor)
{
  ODCOLORREF* pColor = (ODCOLORREF*)(pDefColor);
  if (!pColor)
  {
    ODA_ASSERT(false);
  }
  m_bgColor = *pColor;
}

OdIntPtr OdTvVisualizeIfcFilerProperties::getBgColor() const
{
  return (OdIntPtr)(&m_bgColor);
}

OdIntPtr OdTvVisualizeIfcFilerProperties::getPalette() const
{
  return (OdIntPtr)(m_pPalette);
}

void OdTvVisualizeIfcFilerProperties::setDeviation(double deviation)
{
  m_deviation = deviation;
}

double OdTvVisualizeIfcFilerProperties::getDeviation() const
{
  return m_deviation;
}

void OdTvVisualizeIfcFilerProperties::setMinPerCircle(OdUInt16 minPerCircle)
{
  m_minPerCircle = minPerCircle;
}

OdUInt16 OdTvVisualizeIfcFilerProperties::getMinPerCircle() const
{
  return m_minPerCircle;
}

void OdTvVisualizeIfcFilerProperties::setMaxPerCircle(OdUInt16 maxPerCircle)
{
  m_maxPerCircle = maxPerCircle;
}

OdUInt16 OdTvVisualizeIfcFilerProperties::getMaxPerCircle() const
{
  return m_maxPerCircle;
}

OdUInt8 OdTvVisualizeIfcFilerProperties::getModelerType() const
{
  return m_modelerType;
}

void OdTvVisualizeIfcFilerProperties::setModelerType(OdUInt8 type)
{
  m_modelerType = type;
}

OdTvVisualizeIfcFilerProperties::~OdTvVisualizeIfcFilerProperties()
{
}

namespace IFC2Visualize {

  ODRX_DECLARE_PROPERTY(Palette)
  ODRX_DECLARE_PROPERTY(DCRect)
  ODRX_DECLARE_PROPERTY(ObjectNaming)
  ODRX_DECLARE_PROPERTY(StoreSourceObjects)
  ODRX_DECLARE_PROPERTY(ClearEmptyObjects)
  ODRX_DECLARE_PROPERTY(RearrangeObjects)
  ODRX_DECLARE_PROPERTY(AppendTransform)
  ODRX_DECLARE_PROPERTY(FeedbackForChooseCallback)
  ODRX_DECLARE_PROPERTY(DefaultEntityColor)
  ODRX_DECLARE_PROPERTY(Deviation)
  ODRX_DECLARE_PROPERTY(MinPerCircle)
  ODRX_DECLARE_PROPERTY(MaxPerCircle)
  ODRX_DECLARE_PROPERTY(NeedCDATree)
  ODRX_DECLARE_PROPERTY(ModelerType)
  ODRX_DECLARE_PROPERTY(NeedCollectPropertiesInCDA)
  ODRX_DECLARE_PROPERTY(UseCustomBgColor)
  ODRX_DECLARE_PROPERTY(BgEntityColor)

  ODRX_BEGIN_DYNAMIC_PROPERTY_MAP(OdTvVisualizeIfcFilerProperties);
    ODRX_GENERATE_PROPERTY(Palette)
    ODRX_GENERATE_PROPERTY(DCRect)
    ODRX_GENERATE_PROPERTY(ObjectNaming)
    ODRX_GENERATE_PROPERTY(StoreSourceObjects)
    ODRX_GENERATE_PROPERTY(ClearEmptyObjects)
    ODRX_GENERATE_PROPERTY(RearrangeObjects)
    ODRX_GENERATE_PROPERTY(AppendTransform)
    ODRX_GENERATE_PROPERTY(FeedbackForChooseCallback)
    ODRX_GENERATE_PROPERTY(DefaultEntityColor)
    ODRX_GENERATE_PROPERTY(Deviation)
    ODRX_GENERATE_PROPERTY(MinPerCircle)
    ODRX_GENERATE_PROPERTY(MaxPerCircle)
    ODRX_GENERATE_PROPERTY(NeedCDATree)
    ODRX_GENERATE_PROPERTY(ModelerType)
    ODRX_GENERATE_PROPERTY(NeedCollectPropertiesInCDA)
    ODRX_GENERATE_PROPERTY(UseCustomBgColor)
    ODRX_GENERATE_PROPERTY(BgEntityColor)
  ODRX_END_DYNAMIC_PROPERTY_MAP(OdTvVisualizeIfcFilerProperties);

  ODRX_DEFINE_PROPERTY_METHODS(Palette, OdTvVisualizeIfcFilerProperties, getPalette, setPalette, getIntPtr);
  ODRX_DEFINE_PROPERTY_METHODS(DCRect, OdTvVisualizeIfcFilerProperties, getDCRect, setDCRect, getIntPtr);
  ODRX_DEFINE_PROPERTY_METHODS(ObjectNaming, OdTvVisualizeIfcFilerProperties, getObjectNaming, setObjectNaming, getBool);
  ODRX_DEFINE_PROPERTY_METHODS(StoreSourceObjects, OdTvVisualizeIfcFilerProperties, getStoreSourceObjects, setStoreSourceObjects, getBool);
  ODRX_DEFINE_PROPERTY_METHODS(ClearEmptyObjects, OdTvVisualizeIfcFilerProperties, getClearEmptyObjects, setClearEmptyObjects, getBool);
  ODRX_DEFINE_PROPERTY_METHODS(RearrangeObjects, OdTvVisualizeIfcFilerProperties, getRearrangeObjects, setRearrangeObjects, getBool);
  ODRX_DEFINE_PROPERTY_METHODS(AppendTransform, OdTvVisualizeIfcFilerProperties, getAppendTransform, setAppendTransform, getIntPtr);
  ODRX_DEFINE_PROPERTY_METHODS(FeedbackForChooseCallback, OdTvVisualizeIfcFilerProperties, getFeedbackForChooseCallback, setFeedbackForChooseCallback, getIntPtr);
  ODRX_DEFINE_PROPERTY_METHODS(DefaultEntityColor, OdTvVisualizeIfcFilerProperties, getDefaultColor, setDefaultColor, getIntPtr);
  ODRX_DEFINE_PROPERTY_METHODS(Deviation, OdTvVisualizeIfcFilerProperties, getDeviation, setDeviation, getDouble);
  ODRX_DEFINE_PROPERTY_METHODS(MinPerCircle, OdTvVisualizeIfcFilerProperties, getMinPerCircle, setMinPerCircle, getIntPtr)
  ODRX_DEFINE_PROPERTY_METHODS(MaxPerCircle, OdTvVisualizeIfcFilerProperties, getMaxPerCircle, setMaxPerCircle, getIntPtr)
  ODRX_DEFINE_PROPERTY_METHODS(NeedCDATree, OdTvVisualizeIfcFilerProperties, getNeedCDATree, setNeedCDATree, getBool)
  ODRX_DEFINE_PROPERTY_METHODS(ModelerType, OdTvVisualizeIfcFilerProperties, getModelerType, setModelerType, getIntPtr)
  ODRX_DEFINE_PROPERTY_METHODS(NeedCollectPropertiesInCDA, OdTvVisualizeIfcFilerProperties, getNeedCollectPropertiesInCDA, setNeedCollectPropertiesInCDA, getBool);
  ODRX_DEFINE_PROPERTY_METHODS(UseCustomBgColor, OdTvVisualizeIfcFilerProperties, getUseCustomBgColor, setUseCustomBgColor, getBool);
  ODRX_DEFINE_PROPERTY_METHODS(BgEntityColor, OdTvVisualizeIfcFilerProperties, getBgColor, setBgColor, getIntPtr);
}

void OdTvVisualizeIfcFilerProperties::setDCRect(OdIntPtr rect)
{
  OdTvDCRect* pRect = (OdTvDCRect*)(rect);
  if (!pRect)
  {
    ODA_ASSERT(false);
  }

  m_importRect = *pRect;
}

OdIntPtr OdTvVisualizeIfcFilerProperties::getDCRect() const
{
  return (OdIntPtr)(&m_importRect);
}

void OdTvVisualizeIfcFilerProperties::setAppendTransform(OdIntPtr pTransform)
{
  const OdTvMatrix* pAppendTransform = (const OdTvMatrix*)(pTransform);

  if (pAppendTransform)
  {
    m_appendTransform = *pAppendTransform;
  }
  else
  {
    m_appendTransform = OdTvMatrix::kIdentity;
  }
}

OdIntPtr OdTvVisualizeIfcFilerProperties::getAppendTransform() const
{
  return (OdIntPtr)(&m_appendTransform);
}

void OdTvVisualizeIfcFilerProperties::setFeedbackForChooseCallback(OdIntPtr pCallback)
{
  m_pCallback = (OdTvFeedbackForChooseCallback)pCallback;
}

OdIntPtr OdTvVisualizeIfcFilerProperties::getFeedbackForChooseCallback() const
{
  return (OdIntPtr)m_pCallback;
}


//***************************************************************************//
// 'OdTvVisualizeIfcFiler' methods implementation
//***************************************************************************//

OdTvVisualizeIfcFiler::OdTvVisualizeIfcFiler()
  : m_properties(OdTvVisualizeIfcFilerProperties::createObject())
{
}

int OdTvVisualizeIfcFiler::getChoosedRepresentationContexts(OdIfcFilePtr pIfcFile, OdTvFilerFeedbackForChooseObject & filerFeedbackForChoose, bool& bCanceled) const
{
  m_selContexts.clear();

  OdIfcModelPtr pModel = pIfcFile->getModel();

  if (pModel.isNull())
  {
    ODA_ASSERT_ONCE(pModel.get());
    return -1;
  }

  if (!m_properties->has(OD_T("FeedbackForChooseCallback")) ||
    m_properties->getFeedbackForChooseCallback() == NULL)
    return odSelectRepresentationContextsByDefault(pModel);

  OdTvFilerFeedbackItemForChooseArray* pFilerFeedbackForChooseArray = filerFeedbackForChoose.getFilerFeedbackItemForChooseArrayPtr();
  if (pFilerFeedbackForChooseArray == NULL)
    return odSelectRepresentationContextsByDefault(pModel);

  OdDAIObjectIds selContexts;

  OdIfc::OdIfcEntityPtr pProject = pIfcFile->getProjectId().openObject();
  if (!pProject.isNull())
  {
    OdIfcEntityPtr pCtx;
    OdDAIObjectIds contexts;
    pProject->getAttr("representationcontexts") >> contexts;
    for (OdDAIObjectIds::iterator it = contexts.begin(), end = contexts.end(); it != end; ++it)
    {
      pCtx = it->openObject();
      if (pCtx->isKindOf(kIfcGeometricRepresentationContext))
      {
        OdAnsiString strContextType;
        if (pCtx->getAttr("contexttype") >> strContextType)
        {
          OdTvFilerFeedbackItemForChoose filerFeedbackForChoose(strContextType, strContextType.compare("Model") == 0 ? true : false);
          pFilerFeedbackForChooseArray->append(filerFeedbackForChoose);
          selContexts.append(*it);

          OdDAIObjectIds hasSubContexts;
          if (pCtx->getAttr("hassubcontexts") >> hasSubContexts && hasSubContexts.size() > 0)
          {
            OdIfcEntityPtr pSubCtx;
            for (OdDAIObjectIds::iterator it = hasSubContexts.begin(), end = hasSubContexts.end(); it != end; ++it)
            {
              pSubCtx = it->openObject();
              if (!pSubCtx.isNull() && pSubCtx->isInstanceOf(kIfcGeometricRepresentationSubContext))
              {
                OdAnsiString strContextIdentifier;
                if (pSubCtx->getAttr("contextidentifier") >> strContextIdentifier)
                {
                  OdTvFilerFeedbackItemForChoose filerFeedbackForChoose(strContextType + "\\" + strContextIdentifier, strContextIdentifier.compare("Body") == 0 ? true : false);
                  pFilerFeedbackForChooseArray->append(filerFeedbackForChoose);
                  selContexts.append(*it);
                }
              }
            }
          }
        }
      }
    }
  }

  unsigned int size = pFilerFeedbackForChooseArray->size();
  if (size > 0)
  {
    if (m_properties->has(OD_T("FeedbackForChooseCallback")) &&
      m_properties->getFeedbackForChooseCallback() != NULL && size > 1)
    {
      ((OdTvFeedbackForChooseCallback)m_properties->getFeedbackForChooseCallback())(filerFeedbackForChoose);
      if (filerFeedbackForChoose.getFilerFeedbackItemForChooseArrayPtr()->length() == 0)
      {
        bCanceled = true;
        return -1;
      }

      for (unsigned int i = 0; i < size; ++i)
      {
        if (pFilerFeedbackForChooseArray->at(i).m_bChosen)
          m_selContexts.append(selContexts[i]);
      }
    }
    else
    {
      m_selContexts = selContexts;
    }
  }

  return m_selContexts.size();
}

OdTvDatabaseId OdTvVisualizeIfcFiler::loadFrom(const OdString& filePath, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  OdStreamBufPtr pStreamBuf = odrxSystemServices()->createFile(filePath);
  if (pStreamBuf.isNull())
  {
    if (rc)
      *rc = tvInvalidFilePath;
    return OdTvDatabaseId();
  }

  return loadFrom(pStreamBuf, pProfileRes, rc);
}

void OdTvVisualizeIfcFiler::createCommonDataAccessTree(OdTvDatabasePtr pTvDb, OdDbBaseDatabase *pDatabase, const OdString& strTreeName) const
{
  OdIfcFilePtr pIfcFile = OdIfcFile::cast(pDatabase);
  if (pIfcFile.isNull())
  {
      ODA_ASSERT_ONCE("Wrong database class!");
      return;
  }

  OdIfcModelPtr pModel = pIfcFile->getModel();
  if (pModel.isNull())
  {
    ODA_ASSERT_ONCE("Can not create CDA tree for empty IfcFile!");
    return;
  }

  ::odrxDynamicLinker()->loadModule(L"RxProperties");

  //Create CDA tree
  OdTvCDATreePtr pTree = OdTvCDATree::createObject();
  pTree->createDatabaseHierarchyTree(pModel.get(), m_properties->getNeedCollectPropertiesInCDA());

  //Add tree to the Tv database
  OdTvResult rc;
  OdTvCDATreeStorageId cdaTreeId = pTvDb->addCDATreeStorage(strTreeName, pTree, &rc);
  if (rc == tvAlreadyExistSameName)
  {
    OdUInt32 i = 1;
    while (rc != tvOk && i < MAX_CDATREENAME_GENERATION_ATTEMPTS)
    {
      OdString str;
      str.format(L"%s_%d", strTreeName.c_str(), i++); //not to fast but it is not a "bottle neck"
      cdaTreeId = pTvDb->addCDATreeStorage(str, pTree, &rc);
    }
  }

  //Add new CDA tree to the appropriate models
  OdTvModelsIteratorPtr modelsIterPtr = pTvDb->getModelsIterator();
  if (!modelsIterPtr->done())
  {
    OdTvModelPtr pModel = modelsIterPtr->getModel().openObject();
    if (!pModel.isNull())
    {
      pModel->setCDATreeStorage(cdaTreeId);
    }
  }
}

int OdTvVisualizeIfcFiler::odSelectRepresentationContextsByDefault(OdIfcModelPtr pModel) const
{
  OdDAIObjectIds itReprCtx = pModel->getEntityExtent("IfcGeometricRepresentationContext");
  for (OdDAIObjectIds::iterator it = itReprCtx.begin(), end = itReprCtx.end(); it != end; ++it)
  {
    OdIfcEntityPtr pEnt = it->openObject();

    OdAnsiString strContextType;
    if (pEnt->getAttr("ContextType") >> strContextType)
    {
      m_selContexts.append(*it);      

      OdDAIObjectIds hasSubContexts;
      if (pEnt->getAttr("HasSubContexts") >> hasSubContexts)
      {
        unsigned int s = hasSubContexts.size();
        for (unsigned int i = 0; i < s; ++i)
        {
          OdIfcEntityPtr pSubCtx = hasSubContexts[i].openObject();
          if (!pSubCtx.isNull() && pSubCtx->isInstanceOf(kIfcGeometricRepresentationSubContext))
          {
            OdAnsiString strContextIdentifier;
            if (pSubCtx->getAttr("ContextIdentifier") >> strContextIdentifier)
            {
              if (strContextIdentifier.compare("Body") == 0)
                m_selContexts.append(hasSubContexts[i]);
            }
          }
        }
      }
    }
  }
  return m_selContexts.size();
}

OdTvGsViewId OdTvVisualizeIfcFiler::getActiveTvView(OdTvGsDeviceId& tvDeviceId, int iActViewViewInd) const
{
  OdTvGsDevicePtr pTvDevice = tvDeviceId.openObject();
  if (!pTvDevice.isNull())
  {
    return pTvDevice->viewAt(iActViewViewInd);
  }
  return OdTvGsViewId();
}

OdTvGsDeviceId OdTvVisualizeIfcFiler::getActiveTvDevice(OdTvDatabaseId& tvDbId, int idDevice) const
{
  OdTvDevicesIteratorPtr pDevicesIterator = tvDbId.openObject()->getDevicesIterator();
  unsigned iDeviceInd = 0;
  if (!pDevicesIterator.isNull() && !pDevicesIterator->done())
  {
    if (iDeviceInd == idDevice)
      return pDevicesIterator->getDevice();

    pDevicesIterator->step();
  }

  return OdTvGsDeviceId();
}

OdTvModelId OdTvVisualizeIfcFiler::import(OdTvDatabaseId &tvDbId, OdStreamBuf* pBuffer, OdTvFilerTimeProfiling* pProfileRes /*= NULL*/, OdTvResult* rc /*= NULL*/) const
{
  if (rc)
    *rc = tvOk;

  OdTvModelId idModel;

  OdString fileName = OdTvDatabaseUtinls::getFileNameFromPath(pBuffer->fileName());
  if (fileName.isEmpty())
    fileName = OD_T("StreamBuffData");
  OdString modelName;

  OdTvGsDeviceId activeTvGsDeviceId;
  if (tvDbId.isNull())
  {
    //
    // Create empty tv database.
    //
    OdTvFactoryId tvFactoryId = odTvGetFactory();
    tvDbId = tvFactoryId.createDatabase();
    OdTvDatabasePtr pTvDb = tvDbId.openObject(OdTv::kForWrite);
  }
  else
  {
    //
    // Check that we are in the real append mode
    //
    {
      OdTvDatabasePtr pTvDb = tvDbId.openObject();
      OdTvDevicesIteratorPtr devicesIteratorPtr = pTvDb->getDevicesIterator();
      while (!devicesIteratorPtr->done())
      {
        activeTvGsDeviceId = devicesIteratorPtr->getDevice();
        if (activeTvGsDeviceId.openObject()->getActive())
          break;
      }
    }

    modelName = OdTvDatabaseUtinls::generateModelName(tvDbId, fileName);
  }

  //
  // Check if time profiling is needed
  //
  bool bUseTimeProfiling = false;
  if (pProfileRes)
    bUseTimeProfiling = true;
  double internalTiming = 0.;
  double externalTiming = 0.;
  double fileOpenTiming = 0.;
  double composeTiming = 0.;
  double contextSelectionTiming = 0.;
  double CDATiming = 0.;

  OdStaticRxObject<OdTvIfc2VisService> svcs;

  //
  // Start total timing
  //
  OdTvFilerTimer timing(bUseTimeProfiling);
  timing.startTotal();

  ModelerType type = (ModelerType)m_properties->getModelerType();
  odIfcInitialize(true, type);

  //
  // Store visualize device module name
  //
  OdString moduleName;
  try
  {
    //
    // Create OdIfcModel context
    //
    ODCOLORREF* pDefColor = (ODCOLORREF*)m_properties->getDefaultColor();
    OdUInt8 r, g, b;
    r = ODGETRED(*pDefColor);
    g = ODGETGREEN(*pDefColor);
    b = ODGETBLUE(*pDefColor);
    OdCmEntityColor color(r, g, b);
    OdIfcDeviationParams deviationParams(m_properties->getDeviation(), m_properties->getMinPerCircle(), m_properties->getMaxPerCircle());
    OdIfcModelContext context(deviationParams, color);

    timing.startMisc();
    OdIfcFilePtr pIfcFile = svcs.readFile(pBuffer);
    timing.endMisc();
    fileOpenTiming = timing.getMiscTime();

    if (!pIfcFile.isNull())
    {
      //
      // Interrupt total timing for ask what representation contexts should be drawn
      //
      timing.startMisc();
      OdTvFilerFeedbackForChooseObject filerFeedbackForChooseObject(OD_T("Geometric contexts for import"));
      bool bCanceled = false;
      int nSelContexts = getChoosedRepresentationContexts(pIfcFile, filerFeedbackForChooseObject, bCanceled);
      timing.endMisc();

      if (bCanceled)
      {
        if (rc)
          *rc = tvFilerEmptyInternalDatabase;
        odTvGetFactory().removeDatabase(tvDbId);
        tvDbId.setNull();

        pIfcFile = NULL;

        //uninitialize IFC modules
        odIfcUninitialize();
        // Here we will unload everything including Visualize device
        odrxDynamicLinker()->unloadUnreferenced();

        return idModel;
      }
      contextSelectionTiming = timing.getMiscTime();

      if (nSelContexts > 0)
      {
        timing.startMisc();
        pIfcFile->setContextSelection(m_selContexts);
        pIfcFile->setContext(context);
        pIfcFile->composeEntities();
        timing.endMisc();
        composeTiming = timing.getMiscTime();

        //
        // Vectorizing
        //
        timing.startVectorizing();
        OdGsModulePtr pGs = ::odrxDynamicLinker()->loadModule(OdTvVisualizeDeviceModuleName, false);
        if (!pGs.isNull())
        {
          moduleName = pGs->moduleName();

          //
          // Create vectorizer device
          //
          OdGsDevicePtr pDevice = pGs->createDevice();

          OdGiContextForIfcDatabasePtr pIfcContext = OdGiContextForIfcDatabase::createObject();
          pIfcContext->setDatabase(pIfcFile);
          pIfcContext->enableGsModel(true);

          OdRxDictionaryPtr pProperties = pDevice->properties();
          if (pProperties.get())
          {
            if (pProperties->has(OD_T("DisplayViaGLES2")))
              pProperties->putAt(OD_T("DisplayViaGLES2"), OdRxVariantValue(false));
            if (pProperties->has(OD_T("AllowNonPersistentObjects")))
              pProperties->putAt(OD_T("AllowNonPersistentObjects"), OdRxVariantValue(false));
#if !defined(__APPLE__)
            if (pProperties->has(OD_T("EnableTiming")))
              pProperties->putAt(OD_T("EnableTiming"), OdRxVariantValue(bUseTimeProfiling));
#endif
            if (pProperties->has(OD_T("TvDatabaseID")))
              pProperties->putAt(OD_T("TvDatabaseID"), OdRxVariantValue((OdIntPtr)(&tvDbId)));

            if (pProperties->has(OD_T("WriteUserData")))
              pProperties->putAt(OD_T("WriteUserData"), OdRxVariantValue(true /*m_properties->getStoreSourceObjects()*/));
            if (pProperties->has(OD_T("GenerateEntityNames")))
              pProperties->putAt(OD_T("GenerateEntityNames"), OdRxVariantValue(true /*m_properties->getObjectNaming()*/));
            if (pProperties->has(OD_T("CompareUnnamedImages")))
              pProperties->putAt(OD_T("CompareUnnamedImages"), OdRxVariantValue(true));

            if (!activeTvGsDeviceId.isNull())
            {
              if (pProperties->has(OD_T("IgnoreFlags")))
                pProperties->putAt(OD_T("IgnoreFlags"), OdRxVariantValue(OdUInt16(1)/*DeviceIgnoreFlags::kIgnoreViewInfoFlags*/));
              if (pProperties->has(OD_T("ModelName")))
                pProperties->putAt(OD_T("ModelName"), OdRxVariantValue(modelName));
              if (pProperties->has(OD_T("NamePrefix")))
                pProperties->putAt(OD_T("NamePrefix"), OdRxVariantValue(modelName));
              if (pProperties->has(OD_T("TvDeviceDAM")))
                pProperties->putAt(OD_T("TvDeviceDAM"), OdRxVariantValue((OdIntPtr)(&activeTvGsDeviceId)));

              OdTvGsViewId viewId = activeTvGsDeviceId.openObject()->getActiveView();
              OdString name = viewId.openObject()->getName();
              if (pProperties->has(OD_T("TvViewDAM")))
                pProperties->putAt(OD_T("TvViewDAM"), OdRxVariantValue((OdIntPtr)(&viewId)));
            }
          }

          //
          // Setup the device and view
          //
          OdGsIfcLayoutHelperPtr pLayoutHelper = OdIfcGsManager::setupActiveLayoutViews(pDevice.get(), pIfcContext);
          pLayoutHelper->activeView()->setMode(OdGsView::kGouraudShaded);

          //
          // Set the palette
          //
          const ODCOLORREF* palette = (const ODCOLORREF*)m_properties->getPalette();
          bool bDefaultPalette = false;
          if (palette == 0)
          {
            bDefaultPalette = true;
            palette = odcmAcadPalette(ODRGB(255, 255, 255));
          }

          OdArray<ODCOLORREF, OdMemoryAllocator<ODCOLORREF> > pPalCpy;
          pPalCpy.insert(pPalCpy.begin(), palette, palette + 256);
          pDevice->setLogicalPalette(pPalCpy.asArrayPtr(), 256);

          pDevice->setBackgroundColor(ODRGB(192, 192, 192));
          pIfcContext->setPaletteBackground(ODRGB(192, 192, 192));

          //
          // Call onsize
          //
          OdTvDCRect* pRect = (OdTvDCRect*)m_properties->getDCRect();
          if (pRect && (pRect->xmax > 0 || pRect->xmin > 0 || pRect->ymax > 0 || pRect->ymin > 0))
          {
            OdGsDCRect  gsRect(pRect->xmin, pRect->xmax, pRect->ymin, pRect->ymax);
            pDevice->onSize(gsRect);
          }

          //
          // Call draw to the Visualize database
          //
          pLayoutHelper->update();

          if (pProperties->has(OD_T("TvDatabaseID")))
            tvDbId = *(OdTvDatabaseId*)OdRxVariantValue(pProperties->getAt(OD_T("TvDatabaseID")).get())->getIntPtr();
          if (bUseTimeProfiling)
          {
#if !defined(__APPLE__)
            if (pProperties->has(OD_T("TvElapsedTime")))
            {
              internalTiming = OdRxVariantValue(pProperties->getAt(OD_T("TvElapsedTime")).get())->getDouble();
            }
#endif
          }

          //
          // Setup active view
          //
          timing.startMisc();

          if (tvDbId.isValid())
          {
            OdTvDatabasePtr pTvDb = tvDbId.openObject(OdTv::kForWrite);

            if (activeTvGsDeviceId.isNull())
            {
              int iActViewViewInd = getActiveViewId(pLayoutHelper.get());
              if (iActViewViewInd >= 0)
              {
                //get current tv device
                OdTvGsDeviceId tvDeviceId = getActiveTvDevice(tvDbId, 0);
                OdTvGsViewId tvViewId = getActiveTvView(tvDeviceId, iActViewViewInd);
                if (!tvDeviceId.isNull() && !tvViewId.isNull())
                {
                  OdTvGsViewPtr pActiveTvView = tvViewId.openObject(OdTv::kForWrite);
                  pActiveTvView->setActive(true);

                  // mark other views sibling
                  OdTvGsDevicePtr pTvDevice = tvDeviceId.openObject();
                  if (!pTvDevice.isNull())
                  {
                    for (int i = 0; i < pTvDevice->numViews(); i++)
                    {
                      if (i != iActViewViewInd)
                        continue;

                      OdTvGsViewId viewId = pTvDevice->viewAt(i);
                      if(viewId != tvViewId)
                        pActiveTvView->addSibling(viewId);

                      //set background to the active view
                      OdTvGsViewBackgroundId::BackgroundTypes bgType = m_properties->getUseCustomBgColor() ? OdTvGsViewBackgroundId::kSolid : OdTvGsViewBackgroundId::kGradient;
                      OdTvGsViewBackgroundId bkgId = pTvDb->createBackground(OD_T("IFC2VISUALIZE"), bgType);
                      if (!bkgId.isNull())
                      {
                        {
                          if (bgType == OdTvGsViewBackgroundId::kSolid)
                          {
                            OdTvGsViewSolidBackgroundPtr pSolidBackground = bkgId.openAsSolidBackground(OdTv::kForWrite);
                            ODCOLORREF* pBgColor = (ODCOLORREF*)m_properties->getBgColor();
                            r = ODGETRED(*pBgColor);
                            g = ODGETGREEN(*pBgColor);
                            b = ODGETBLUE(*pBgColor);

                            pSolidBackground->setColorSolid(OdTvColorDef(r, g, b));
                          }
                          else if (bgType == OdTvGsViewBackgroundId::kGradient)
                          {
                            OdTvGsViewGradientBackgroundPtr pGradientBackground = bkgId.openAsGradientBackground(OdTv::kForWrite);

                            pGradientBackground->setColorTop(OdTvColorDef(33, 108, 170));
                            pGradientBackground->setColorMiddle(OdTvColorDef(109, 158, 200));
                            pGradientBackground->setColorBottom(OdTvColorDef(184, 208, 230));
                            pGradientBackground->setHeight(0.33);
                            pGradientBackground->setHorizon(0.5);
                          }
                          else
                          {
                            ODA_FAIL_M_ONCE("Ifc2Visualize background type not implemented");
                          }
                        }

                        pActiveTvView->setBackground(bkgId);
                      }

                      //set more lighting
                      pActiveTvView->setDefaultLightingIntensity(1.);

                      // Zoom to extents
                      OdTvPoint minPt, maxPt;
                      pActiveTvView->zoomExtents(minPt, maxPt);
                    }
                  }
                }
              }

              //
              // Store filename to database user data
              //
              OdTvDatabaseUtinls::writeFileNameToTvDatabase(tvDbId, fileName);

              //
              // Rename the model (here we suppose that database contain only one model)
              //
              OdTvModelsIteratorPtr pModelsIterator = tvDbId.openObject()->getModelsIterator();
              if (!pModelsIterator.isNull() && !pModelsIterator->done())
              {
                OdTvModelId modelId = pModelsIterator->getModel();
                OdTvModelPtr pIvModel = modelId.openObject(OdTv::kForWrite);
                if (!pIvModel.isNull())
                {
                  pIvModel->setName(fileName);
                  OdTvSelectabilityDef selectability;
                  selectability.setGeometries(true);
                  pIvModel->setSelectability(selectability);
                }
              }

              if (m_properties->getNeedCDATree())
              {
#if !defined(__APPLE__)
                OdPerfTimerBase* pCDATiming = OdPerfTimerBase::createTiming();
                pCDATiming->start();
#endif

                OdTvDatabasePtr pTvDb = tvDbId.openObject(OdTv::kForWrite);
                createCommonDataAccessTree(pTvDb, pIfcFile, fileName + OD_T(".ifc"));

#if !defined(__APPLE__)
                pCDATiming->stop();
                CDATiming += pCDATiming->countedSec();
                if (pProfileRes)
                  pProfileRes->setCDATreeCreationTime(OdInt64((CDATiming) * 1000.));
#endif
              }
            }
            else
            {
              //
              // Collect current models and blocks
              //
              std::set<OdTvGsViewId> foreignViews;
              std::set<OdTvModelId> foreignModels;
              std::set<OdTvBlockId> foreignBlocks;
              OdTvDatabaseUtinls::collectViewsModelsAndBlocks(tvDbId, foreignViews, foreignModels, foreignBlocks);

              {
                OdTvDatabasePtr pTvDb = tvDbId.openObject();
                idModel = pTvDb->findModel(modelName);
              }

              OdTvDatabaseId& tvDvId = const_cast<OdTvDatabaseId&>(tvDbId);
              OdTvDatabaseCleaner::cleanTvDatabaseForAppend(tvDvId, foreignViews, foreignModels, foreignBlocks);

              //
              // Apply transform if needed
              //
              OdTvMatrix* pTransfrom = (OdTvMatrix*)m_properties->getAppendTransform();
              if (pTransfrom)
              {
                OdTvDatabaseId& tvDvId = const_cast<OdTvDatabaseId&>(tvDbId);
                OdTvDatabaseUtinls::applyTransformToTheModel(tvDvId, modelName, *pTransfrom);
              }
            }
          }

          if (tvDbId.isValid() && m_properties->getClearEmptyObjects())
            OdTvDatabaseCleaner::cleanTvDatabase(tvDbId);

          timing.endMisc();
          externalTiming += timing.getMiscTime();
        }
        else
        {
          if (rc)
            *rc = tvMissingVisualizeDeviceModule;
        }

        timing.endVectorizing();
      }
    }
    else
    {
      //
      // File open error
      //
      if (rc)
        *rc = tvFilerEmptyInternalDatabase;
    }
  }
  catch (...)
  {
    if (rc)
      *rc = tvInternal;

    timing.endVectorizing();
  }

  //unload vectorizing device
  if (!moduleName.isEmpty())
    odrxDynamicLinker()->unloadModule(moduleName);

  //uninitialize IFC modules
  odIfcUninitialize();

  //
  // Here we will unload everything including Visualize device
  //
  odrxDynamicLinker()->unloadUnreferenced();

  timing.endTotal();

  if (pProfileRes)
  {
    pProfileRes->setImportTime(OdInt64((timing.getTotalTime() - timing.getVectorizingTime() - contextSelectionTiming - CDATiming) * 1000.));
    pProfileRes->setVectorizingTime(OdInt64((timing.getVectorizingTime() - CDATiming) * 1000.));
#if !defined(__APPLE__)
    pProfileRes->setTvTime(OdInt64((internalTiming + externalTiming - CDATiming) * 1000.));
#endif
  }

  //set the selectability level
  if (!idModel.isNull())
  {
    OdTvModelPtr pTvModel = idModel.openObject(OdTv::kForWrite);
    OdTvSelectabilityDef selectability;
    selectability.setGeometries(true);
    pTvModel->setSelectability(selectability);
  }

  return idModel;
}

OdTvDatabaseId OdTvVisualizeIfcFiler::loadFrom(OdStreamBuf* pBuffer, OdTvFilerTimeProfiling* pProfileRes /*= NULL*/, OdTvResult* rc /*= NULL*/) const
{
  OdTvDatabaseId idDb;
  import(idDb, pBuffer, pProfileRes, rc);
  return idDb;
}

OdTvDatabaseId OdTvVisualizeIfcFiler::loadFrom(OdDbBaseDatabase*  pDatabase, OdTvFilerTimeProfiling* pProfileRes /*= NULL*/, OdTvResult* rc /*= NULL*/) const
{
  OdTvDatabaseId tvDbId;

  // does nothing
  return tvDbId;
}

OdTvDatabaseId OdTvVisualizeIfcFiler::generate(OdTvFilerTimeProfiling* pProfileRes) const
{
  OdTvDatabaseId tvDbId;

  // does nothing
  return tvDbId;
}

OdTvModelId OdTvVisualizeIfcFiler::appendFrom(const OdTvDatabaseId& databaseId, const OdString& filePath, OdTvFilerTimeProfiling* pProfileRes, OdTvResult* rc) const
{
  OdStreamBufPtr pStreamBuf = odrxSystemServices()->createFile(filePath);
  if (pStreamBuf.isNull())
  {
    if (rc)
      *rc = tvInvalidFilePath;
    return OdTvModelId();
  }

  return appendFrom(databaseId, pStreamBuf, pProfileRes, rc);

  //
  // Get palette, background and device size
  //
  /*ODCOLORREF background = ODRGB(0, 0, 0);
  OdTvDCRect rect(0, 0, 0, 0);
  OdArray<ODCOLORREF, OdMemoryAllocator<ODCOLORREF> > pPalCpy;

  if (!activeTvGsDeviceId.isNull())
  {
    OdTvGsDevicePtr pTvDevice = activeTvGsDeviceId.openObject();

    background = pTvDevice->getBackgroundColor();
    pTvDevice->getSize(rect);

    int nColors;
    const ODCOLORREF* pPalette = pTvDevice->getLogicalPalette(nColors);
    if (nColors >= 256)
      pPalCpy.insert(pPalCpy.begin(), pPalette, pPalette + 256);
    else
    {
      pPalCpy.insert(pPalCpy.begin(), pPalette, pPalette + nColors);
      pPalCpy.insert(pPalCpy.begin() + nColors, 256 - nColors, ODRGB(0, 0, 0));
    }
  }
  else
  {
    background = ODRGB(171, 200, 232);
    OdTvDCRect* pRect = (OdTvDCRect*)m_properties->getDCRect();
    if (pRect)
      rect = *pRect;

    //set the palette
    const ODCOLORREF* palette = (const ODCOLORREF*)m_properties->getPalette();
    bool bDefaultPalette = false;
    if (palette == 0)
    {
      bDefaultPalette = true;
      palette = odcmAcadPalette(ODRGB(255, 255, 255));
    }

    pPalCpy.insert(pPalCpy.begin(), palette, palette + 256);
  }*/
}

OdTvModelId OdTvVisualizeIfcFiler::appendFrom(const OdTvDatabaseId& databaseId, OdStreamBuf* pBuffer, OdTvFilerTimeProfiling* pProfileRes /*= NULL*/, OdTvResult* rc /*= NULL*/) const
{
  OdTvDatabaseId idTvDb = (OdTvDatabaseId)databaseId;
  return import(idTvDb, pBuffer, pProfileRes, rc);
}

OdTvModelId OdTvVisualizeIfcFiler::appendFrom(const OdTvDatabaseId& databaseId, OdDbBaseDatabase*  pDatabase, OdTvFilerTimeProfiling* pProfileRes /*= NULL*/, OdTvResult* rc /*= NULL*/) const
{
  OdTvModelId tvModelId;

  // does nothing
  return tvModelId;
}

int OdTvVisualizeIfcFiler::getActiveViewId(OdGsIfcLayoutHelper* pLayoutHelper) const
{
  if (!pLayoutHelper)
    return -1;

  OdGsViewPtr pActiveView = pLayoutHelper->activeView();

  int num = pLayoutHelper->numViews();
  int iActViewViewInd = 0;
  for (int i = 0; i < num; ++i)
  {
    OdGsView* pView = pLayoutHelper->viewAt(i);

    if (pView == pActiveView.get())
    {
      iActViewViewInd = i;
      break;
    }
  }

  return iActViewViewInd;
}

OdTvGsViewId OdTvVisualizeIfcFiler::getActiveTvView(OdTvDatabaseId& dbId, int iActViewViewInd) const
{
  OdTvDevicesIteratorPtr pDevicesIterator = dbId.openObject()->getDevicesIterator();
  if (!pDevicesIterator.isNull() && !pDevicesIterator->done())
  {
    OdTvGsDevicePtr pTvDevice = pDevicesIterator->getDevice().openObject();
    if (!pTvDevice.isNull())
    {
      return pTvDevice->viewAt(iActViewViewInd);
    }
  }

  return OdTvGsViewId();
}

//***************************************************************************//
// Native properties methods implementation
//***************************************************************************//

#ifdef ENABLE_IFC2VIS_NATIVE_PROPERTIES

OdTvResult OdTvVisualizeIfcFiler::startActionsWithNativeProperties(const OdString& sFilePath, bool bPartial)
{

  if (!m_pIfcFile.isNull())
  {
    if (m_pIfcFile->getFileName() != sFilePath)
      m_pIfcFile.release();
    else
      return tvOk;
  }

  if (m_pIfcCoreModule.isNull())
  {
    m_pIfcCoreModule = ::odrxDynamicLinker()->loadModule(OdIfcCoreModuleName);
  }

  OdTvResult rc = tvOk;
  if (m_pFilerPtr.isNull())
  {
    m_pFilerPtr = new OdTvVisualizeIfcFilerDbSource;
  }

  OdIfcFilePtr pIfcFile = m_pFilerPtr->readFile(sFilePath);
  if (pIfcFile.isNull())
    return tvFilerEmptyInternalDatabase;

  //OdTvVisualizeDwgFilerSourceFromFile dl;
  //if (!dl.initialize(sFilePath, NULL, &rc))

  {
    if (m_pRxPropertiesModule.isNull())
      m_pRxPropertiesModule = ::odrxDynamicLinker()->loadModule(L"RxProperties");

    try
    {
      m_pIfcFile = pIfcFile;
      m_pIfcModel = m_pIfcFile->getModel();
    }
    catch (...)
    {
      return tvFilerEmptyInternalDatabase;
    }
  }

  return rc;
}

bool OdTvVisualizeIfcFiler::isActionsWithNativePropertiesStarted(const OdString& sFilePath)
{
  if (!m_pIfcFile.isNull() && m_pIfcFile->getFileName() == sFilePath)
    return true;
  return false;
}

OdTvResult OdTvVisualizeIfcFiler::endActionsWithNativeProperties()
{
  m_pFilerPtr = NULL;

  m_pIfcModel.release();
  m_pIfcFile.release();
  m_pIfcCoreModule.release();
  m_pRxPropertiesModule.release();

  ::odrxDynamicLinker()->unloadModule(L"RxProperties");
  ::odrxDynamicLinker()->unloadModule(OdIfcCoreModuleName);
  ::odrxDynamicLinker()->unloadUnreferenced();

  return tvOk;
}

OdRxMemberIteratorPtr OdTvVisualizeIfcFiler::getNativePropertiesIterator(OdUInt64 dbHandle, OdTvResult* rc /*= NULL*/)
{
  if (m_pIfcModel.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedDatabase;
    return OdRxMemberIteratorPtr();
  }

  OdRxMemberIteratorPtr pIter;
  if (dbHandle == 0)
    pIter = OdRxMemberQueryEngine::theEngine()->newMemberIterator(m_pIfcFile.get());
  else
  {
    OdDAIObjectId id = m_pIfcModel->getEntityInstance(dbHandle);
    if (id.isNull())
    {
      if (rc)
        *rc = tvNativePropMissedObject;
      return OdRxMemberIteratorPtr();
    }

    OdDAI::EntityInstancePtr pInst = id.openObject();
    pIter = OdRxMemberQueryEngine::theEngine()->newMemberIterator(pInst.get());
  }

  if (pIter.isNull())
  {
    if (rc)
      *rc = tvInternal;
    return OdRxMemberIteratorPtr();
  }

  if (rc)
    *rc = tvOk;
  return pIter;
}

OdRxValue OdTvVisualizeIfcFiler::getNativePropertyValue(OdUInt64 dbHandle, const OdRxPropertyPtr& pProperty, bool* bReadOnly /*= NULL*/, OdTvResult* rc /*= NULL*/)
{
  if (pProperty.isNull())
  {
    if (rc)
      *rc = tvInvalidInput;
    return OdRxValue();
  }

  if (m_pIfcModel.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedDatabase;
    return OdRxValue();
  }

  OdRxObjectPtr pInst;
  if (dbHandle == 0)
    pInst = m_pIfcFile;
  else
  {
    OdDAIObjectId id = m_pIfcModel->getEntityInstance(dbHandle);
    pInst = id.openObject();
  }

  if (pInst.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedObject;
    return OdRxValue();
  }

  OdRxValue value;
  OdResult odRes = pProperty->getValue(pInst, value);
  if (bReadOnly)
    *bReadOnly = pProperty->isReadOnly(pInst);

  if (odRes != eOk)
  {
    if (rc)
      *rc = tvInternal;

    return OdRxValue();
  }

  if (rc)
    *rc = tvOk;

  return value;
}

OdRxValueIteratorPtr OdTvVisualizeIfcFiler::getNativeCollectionPropertyIterator(OdUInt64 dbHandle, const OdRxCollectionPropertyPtr& pCollectionProperty, bool* bReadOnly /*= NULL*/, OdTvResult* rc /*= NULL*/)
{
  return OdRxValueIteratorPtr();
}

OdTvResult OdTvVisualizeIfcFiler::setNativePropertyValue(OdUInt64 dbHandle, OdRxPropertyPtr& pProperty, const OdRxValue& value)
{
  return tvOk;
}

OdDbBaseDatabase* OdTvVisualizeIfcFiler::getNativeDatabase(OdTvResult* rc) const
{
  if (m_pIfcModel.isNull() || m_pIfcFile.isNull())
  {
    if (rc)
      *rc = tvNativePropMissedDatabase;
    return NULL;
  }

  return (OdDbBaseDatabase*)m_pIfcFile.get();
}
#endif

//***************************************************************************//
// 'OdTvVisualizeIfcFilerModule' methods implementation
//***************************************************************************//

ODRX_DEFINE_DYNAMIC_MODULE(OdTvVisualizeIfcFilerModule);

void OdTvVisualizeIfcFilerModule::initApp()
{
  // initialize the Visualize SDK
  odTvInitialize();
}

void OdTvVisualizeIfcFilerModule::uninitApp()
{
  // Uninitialize the Visualize SDK
  odTvUninitialize();
}

OdTvVisualizeFilerPtr OdTvVisualizeIfcFilerModule::getVisualizeFiler() const
{
  OdTvVisualizeFilerPtr pDwgFiler = new OdTvVisualizeIfcFiler();

  return pDwgFiler;
}

//***************************************************************************//
// 'OdTvVisualizeIfcFilerDbSource' methods implementation
//***************************************************************************//

OdIfcFilePtr OdTvVisualizeIfcFilerDbSource::readFile(const OdString &file)
{
  return m_svcs.readFile(file);
}
