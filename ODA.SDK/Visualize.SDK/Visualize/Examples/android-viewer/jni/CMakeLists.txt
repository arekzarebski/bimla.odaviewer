set( jni_lib od_jni_for_vis )

SET(ANDROID_SDK $ENV{ANDROID_SDK})
SET(ANDROID_NDK $ENV{ANDROID_NDK})
SET(ANT_HOME $ENV{ANT_HOME})

# Enable GLES2 by this definition
SET (ANDROID_GLES2 1)

if(ANDROID_GLES2)
    Message ("-- GLES2 for Android Enabled")
    add_definitions(-DANDROID_GLES2)
    set( gl_libs ${TD_TXV_GLES2_LIBS} )
    set( gl_sdk_libs android EGL GLESv2 )
else(ANDROID_GLES2)
    set( gl_libs ${TD_TXV_OPENGL_LIB} ${TD_OPENGL_LIB} )
    set( gl_sdk_libs GLESv1_CM )
endif(ANDROID_GLES2)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

tcomponents_sources(${jni_lib}
        PreFileLoader.h
        PreFileLoader.cpp
        AndroidAppServices.h
        AndroidAppServices.cpp
        com_opendesign_android_TeighaDWGJni.cpp
        com_opendesign_android_TeighaDWGJni.h
        Trackers.h
        Trackers.cpp
        ObjectExplorerCreator.cpp
        ObjectExplorerCreator.h
        ${TDRAWING_ROOT}/Extensions/ExServices/ExHostAppServices.cpp
)

include_directories(${ODA_SOURCE_ROOT}
                    ${TKERNEL_ROOT}/Include/Tr
                    ${TKERNEL_ROOT}/Include
                    ${TKERNEL_ROOT}/DevInclude/DbRoot
                    ${TDRAWING_ROOT}/Include
                    ${TKERNEL_ROOT}/DevInclude/Ge
                    ${TKERNEL_ROOT}/Include/Gi
                    ${TKERNEL_ROOT}/Include/Gs
                    ${TKERNEL_ROOT}/Source/Gs
                    ${TDRAWING_ROOT}/Include/Gfx
                    ${TKERNEL_ROOT}/Examples/Common
                    ${TKERNEL_ROOT}/DevInclude/root
                    ${TKERNEL_ROOT}/DevInclude/
                    ${TDRAWING_ROOT}/Examples/Common
                    ${TKERNEL_ROOT}/Source
                    ${TKERNEL_ROOT}/Source/Gi
                    ${TKERNEL_ROOT}/Include/Si
                    ${TKERNEL_ROOT}/Source/Si
                    ${TKERNEL_ROOT}/Source/DbRoot
                    ${TKERNEL_ROOT}/Source/root
                    ${TKERNEL_ROOT}/Source/database
                    ${TKERNEL_ROOT}/Extensions/RxFs
                    ${TKERNEL_ROOT}/Extensions/ExRender
                    ${TDRAWING_ROOT}/Extensions/ExServices
                    ${TDRAWING_ROOT}/Source/TxHostLib
                    ${TDRAWING_ROOT}/Extensions/RevisionControl
                    ${TKERNEL_ROOT}/Source/Tr
                    ${TKERNEL_ROOT}/Source/Tr/sc
                    ${TKERNEL_ROOT}/Source/Tr/render
                    ${TKERNEL_ROOT}/Include/Tr/vec
                    ${TH_ROOT}/DWFToolkit-7.7
                    ${TDRAWING_ROOT}/Source/database
                    ${TDRAWING_ROOT}/Source/database/DwgFiler
                    ${TKERNEL_ROOT}/DevInclude/License
                    ${TKERNEL_ROOT}/Extensions/ExServices
                    ${VISUALIZE_ROOT}/Include
                    ${VISUALIZE_ROOT}/Source
                    ${TKERNEL_ROOT}/Source/OdCryptoServices/CryptoServices/
                    ${VISUALIZE_ROOT}/Examples/Common

                    ${ANDROID_NDK}/platforms/android-16/arch-arm/usr/include/android

                    viewerCore
                    revisions
)



tcomponents_module(${jni_lib}
  ${TV_VISUALIZE_LIB}
  
  ${TV_MODELSGENERATOR_LIB}
  ${EX_VISUALIZEDEVICE_LIB}
  ${TD_RDIMBLK_LIB}
  
  ${OBJToolkit_LIB}
  ${TD_ISM_LIB}
  ${TD_DYNBLOCKS_LIB}

  ${TP_ROOT_LIB}

  ${TD_OPENGL_LIB} 

  ${gl_libs}

  ${TD_MODELER_LIB}

  ${TCOMPONENTS_BREPMODELER_LIB}

  ${TD_DbCryptModule_LIB}

  ${TD_ACISBLDR_LIB}
  ${TD_BREPBUILDER_LIB}
  ${TD_BREPBUILDERFILLER_LIB}
  ${TD_BREPRENDERER_LIB} 
  ${TD_DBENT_LIB}

  ${TD_BR_LIB}
  ${TD_SPATIALINDEX_LIB}
  ${TD_SCENEOE_LIB}

  ${TD_TF_LIB}
  ${TD_DBCORE_LIB} 
  ${TD_DBIO_LIB}
  ${ODA_OPENGL_LIBS}
  ${TD_GS_LIB}
  ${TD_RX_CDA_LIB}
  ${TD_GI_LIB}
  ${TD_SPATIALINDEX_LIB}
  ${TD_TFCORE_LIB}
  ${TD_GE_LIB} 
  ${TD_DBROOT_LIB}

  ${TD_DB_LIB}
  ${TD_RASTERPROC_LIB}
  ${TD_RASTER_LIB}
  ${TD_ROOT_LIB}
  ${TD_ALLOC_LIB}
  ${TD_KEY_LIB}
  ${TD_EXLIB}

  ${TH_CONDITIONAL_LIBCRYPTO}
  ${TH_THIRDPARTYRASTER_LIB}
  ${TH_ZLIB_LIB}
  ${TH_UTF_LIB}
  ${TH_FT_LIB}
  ${TH_LIBSSL_LIB}
  ${TH_TINYXML_LIB}
  ${gl_sdk_libs}
  log)
