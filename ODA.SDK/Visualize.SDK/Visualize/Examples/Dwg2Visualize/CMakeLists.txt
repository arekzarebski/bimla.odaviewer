#
# Dwg2Visualize library
#

visualize_sources(${TV_DWG2VISUALIZE_LIB}
           Dwg2Visualize.cpp
           Dwg2Visualize.h
           Dwg2VisualizeDef.h
           ../Common/TvFilerTimer.cpp
           ../Common/TvFilerTimer.h
           ../Common/TvDatabaseCleaner.cpp
           ../Common/TvDatabaseCleaner.h
		   ../Common/TvDatabaseUtils.cpp
           ../Common/TvDatabaseUtils.h		   
		   ${TKERNEL_ROOT}/Extensions/ExServices/OdFileBuf.cpp
		   ${TKERNEL_ROOT}/Extensions/ExServices/OdFileBuf.h
		   ${TKERNEL_ROOT}/Extensions/ExServices/RxSystemServicesImpl.cpp
           Dwg2Visualize_Partial.cpp
           PartialImportNotifier.h
)

include_directories(
           ${VISUALIZE_ROOT}/Include
           ../Common
           ${TDRAWING_ROOT}/Include
		   ${TKERNEL_ROOT}/Extensions/ExServices
                   )

if(ODA_SHARED AND MSVC)
visualize_sources(${TV_DVG2VISUALIZE_LIB}
                  Dwg2Visualize.rc
                  )
endif(ODA_SHARED AND MSVC)
add_definitions(-DTV_DWG2VISUALIZE_EXPORTS)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

visualize_tx(${TV_DWG2VISUALIZE_LIB} ${TV_VISUALIZE_LIB} ${TD_GI_LIB} ${TD_GS_LIB} ${TD_EXLIB} ${TD_DR_EXLIB} ${TD_GE_LIB} ${TD_RX_CDA_LIB} ${TD_ROOT_LIB} ${TD_DBROOT_LIB} ${TD_KEY_LIB} ${TD_DB_LIB} ${TD_ALLOC_LIB})

visualize_project_group(Dwg2Visualize "Visualize/Examples")
