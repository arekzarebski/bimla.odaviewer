#
# Visualize2Pdf library
#
visualize_sources(${TV_VISUALIZE2PDF_LIB}
           Visualize2Pdf.cpp
           Visualize2Pdf.h 
)

include_directories(
       ${VISUALIZE_ROOT}/Include
		   ${TKERNEL_ROOT}/Exports/PdfExport/Include
       ${TKERNEL_ROOT}/Extensions/ExServices
       ${TDRAWING_ROOT}/Include/
       ${TDRAWING_ROOT}/Extensions/ExServices
       )

if(ODA_SHARED AND MSVC)
visualize_sources(${TV_VISUALIZE2PDF_LIB}
                  Visualize2Pdf.rc
                  )
endif(ODA_SHARED AND MSVC)

add_definitions(-DTV_VISUALIZE2PDF_EXPORTS)

if(NOT WINCE)
add_definitions(-DODA_LINT)
endif(NOT WINCE)

visualize_tx(${TV_VISUALIZE2PDF_LIB} ${TD_PDF_EXPORT_LIB} ${TD_DR_EXLIB} ${TD_EXLIB} ${TD_GE_LIB} ${TV_VISUALIZE_LIB} ${TD_ROOT_LIB} ${TD_DBCORE_LIB} ${TD_DBROOT_LIB} ${TD_ALLOC_LIB})

visualize_project_group(Visualize2Pdf "Visualize/Examples")

