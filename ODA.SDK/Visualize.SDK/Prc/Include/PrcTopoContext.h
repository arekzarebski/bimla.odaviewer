/////////////////////////////////////////////////////////////////////////////// 
// Copyright (C) 2002-2019, Open Design Alliance (the "Alliance"). 
// All rights reserved. 
// 
// This software and its documentation and related materials are owned by 
// the Alliance. The software may only be incorporated into application 
// programs owned by members of the Alliance, subject to a signed 
// Membership Agreement and Supplemental Software License Agreement with the
// Alliance. The structure and organization of this software are the valuable  
// trade secrets of the Alliance and its suppliers. The software is also 
// protected by copyright law and international treaty provisions. Application  
// programs incorporating this software must include the following statement 
// with their copyright notices:
//   
//   This application incorporates Open Design Alliance software pursuant to a license 
//   agreement with Open Design Alliance.
//   Open Design Alliance Copyright (C) 2002-2019 by Open Design Alliance. 
//   All rights reserved.
//
// By use of this software, its documentation or related materials, you 
// acknowledge and accept the above terms.
///////////////////////////////////////////////////////////////////////////////

#ifndef _PRCTOPOCONTEXT_INCLUDED_
#define _PRCTOPOCONTEXT_INCLUDED_ 
 

#include "PrcBase.h"

SMARTPTR(OdPrcTopoContext);

/** \details 
<group PRC_Topology_Classes> 

Class representing a set of geometry and topology. It contains topological bodies that 
are represented as entry elements and points to geometry and topological items. 
  */
class PRC_TOOLKIT OdPrcTopoContext : public OdPrcBase
{
public:
  //DOM-IGNORE-BEGIN
  ODPRC_DECLARE_MEMBERS_PRCBASE(OdPrcTopoContext);
  //DOM-IGNORE-END
 
  /** \details
  Sets a new behavior value for a body.

  \param behaviour [in] A new behavior value to be set.
    */
  void setBehaviour(OdInt8 behaviour);
  
  /** \details
  Returns the current body's behavior value. 
  */
  OdInt8 behaviour() const;
  
  /** \details
  Sets a new minimum size of an edge.

  \param granularity [in] A new minimum size to be set.
  */
  void setGranularity(double granularity);
  
  /** \details
  Returns the current minimum size of an edge.
  */
  double granularity() const;
  
  /** \details
  Sets a new tolerance value used for topological elements.

  \param tolerance [in] A new tolerance value.
  */
  void setTolerance(double tolerance);
  
  /** \details
  Returns the current tolerance value.
  */
  double tolerance() const;
  
  /** \details
  Sets a new smallest face thickness value.

  \param smallest_thickness [in] A new value of the smallest face thickness to be set.
  */
  void setSmallestThickness(double smallest_thickness);
  
  /** \details
  Returns the current smallest face thickness value.
  */
  double smallestThickness() const;
  
  /** \details
  Sets a new scale used to interpret context data.

  \param scale [in] A new scale value to be set.
  */
  void setScale(double scale);
  
  /** \details
  Returns the current scale value used to interpret context data.
  */
  double scale() const;

  /** \details
  Sets the current scale value to 1.
  */
  void clearScale();

  /** \details
  Returns the current value of the scale flag.
  The scale flag indicates whether a scale is used (true) or is not used (false).
  */
  bool haveScale() const;
  
  /** \details
  Sets the smallest face thickness value value to (granularity * 100).
  */
  void clearSmallestFaceThickness();

  /** \details
  Returns the current value of the smallest face thickness flag.
  The smallest face thickness flag indicates whether the smallest face thickness is used (true) or is not used (false).
  */
  bool haveSmallestFaceThickness() const;

  /** \details
  Clears topological context data.
  */
  void clear();

  /** \details
  Returns the current topological body identifiers array.
  The topological body identifiers array is returned as a reference to an OdPrcObjectIdArray object, therefore it can be used to set a new identifiers array.
  */
  OdPrcObjectIdArray &bodies();

  /** \details
  Returns the current topological body identifiers array.
  */
  const OdPrcObjectIdArray &bodies() const;
};

#endif // _PRCTOPOCONTEXT_INCLUDED_

